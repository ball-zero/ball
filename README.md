### About

This repository contains "BALL" the first prototype implementation of
the Askemos concept and a few simple example applications.

Askemos is a space of communicating blockchains. It is an operating
system for the new level in computer memory hierarchy where units are
blockchains. Their addresses are hashcodes derived from their genesis
block proofing their initial integrity. Those blockchains may be
updated according to a smart contract held in the genesis block when
transaction messages are processed. There is exactly one contract per
blockchain for minimal interference between independent
contracts. Blockchains may create new chains and send transaction
messages to other chains upon update. Note that there is no currency
build into the core as currencies can readily be implemented in smart
contracts.

BALL presents the Askemos concept in the form of a scriptable
webserver.  Eating our own dog food, it is used among other things to
host the documentation at the test network here:
[http://ball.askemos.org](http://ball.askemos.org)

The example applications installed in the demo below contain a
showcase how currencies can be defined and transfered at application
level.

### Branches of Interest

* chicken-argvector: "works for developer", still sometime hangs;
  requires recent [Chicken](https://call-cc.org)

* fuse-av: should be least difference from chicken-argvector to support `fuse` mounts.

* stable: Code in production; **requires** custom chicken from ball.askemos.org!

### This Branch

Branch name: chicken-argvector

Description: Closest to "stable".  Changes to support newer Chicken's
way of argument passing.

State: working in development

###  Installation

See [ball/INSTALL](ball/INSTALL) how to compile and run a first test.

### First Steps

There are short tutorials detailing the setup of the example
applications:

* Find by step instructions to setup a demo network and establish
   "Alice and Bob" in
   [Setup4Local](ball/doc/admin/0120-Setup4Local.md)

* The "metaview":  How to explore the whole thing.

    The "meta view" is a debug facility for exploring the system at
    low level.  It targets developers allowing to browse the rep using
    really simple HTML forms.  No JavaScript, plain HTML ready to be
    scripted.

    The [meta view is documented here](http://ball.askemos.org/?_v=search&_id=2384).

* To learn how to code for BALL start reading with
    [latedeliveryandpenalty](ball/app/latedeliveryandpenalty.xml) This
    showcases the ["shebang
    language"](http://ball.askemos.org/?_v=search&_id=756) used for
    the examples.  BAIL is a blend of XSLT (plus its predecessor DSSSL
    extended with some Scheme and recast in an optional Python-style
    whitespace sensitive syntax) and
    [XSQL](http://askemos.org/A0cd6168e9408c9c095f700d7c6ec3224?_v=search&_id=2356)
    to access the portion of the state stored in the SQLite database
    in the head of the blockchain for each place.

* TBD: Installing  other applications manually using the command line.

### Alternative, Old Setup

Using the [alternative, old setup](ball/doc/admin/0020-Setup4Local.md)
a different contract is used for the entry points.  This one has
additional aplications installed,

* Example Applications

    * Yet another chat

    * The wallet demo: Five steps to issue a test "ICO" and do first
        transfer in you newly created currency.

    * "Clipboard and publish" Upload stuff via Browser or WebDAV.

    The [ExampleApplication tutorial is here](ball/doc/admin/0040-ExampleWallet.md).


