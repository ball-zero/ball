#!/bin/sh -ex

# This was written using the xfce4-terminal
XT=xfce4-terminal
SEP=--window

# Expecting to be run in the build directory we include the output dir:
PATH=`pwd`/.tmp:$PATH

T1="Rep 1"
H1=r1
T2="Rep 2"
H2=r2
T3="Rep 3"
H3=r3
T4="Rep 4"
H4=r4

## A bad idea; leave this to make as it is by now.
#WAKEUP=mechanism/wakeup.scm
#BASE_SETUP=mechanism/setup.scm
#USER_SETUP=app/setup.scm
#
#STARTUP=$WAKEUP $BASE_SETUP $USER_SETUP
#
#if [ -d "$H1" -o -d "$H2" -o -d "$H3" -o -d "$H4" ]; then
#    echo "Found existing rep.  Not using any startup scripts."
#    unset STARTUP
#fi

if [ ! -d "$H1" -o ! -d "$H2" -o ! -d "$H3" -o ! -d "$H4" ]; then
    echo "Did not find all reps.  Exiting."
    exit 1
fi

NAME=askemos
USER=`whoami`
BINARY0=${NAME}-chicken
BINARY=${NAME}-chicken.bin

go () {
  HOSTNAME=$1
  shift  
  # echo ${BINARY} -:S "$HOSTNAME" $*
  echo askemos -u ${USER} -:S -start "$HOSTNAME"
}

CMD1=`go "$H1" $STARTUP`
CMD2=`go "$H2" $STARTUP`
CMD3=`go "$H3" $STARTUP`
CMD4=`go "$H4" $STARTUP`

$XT -T "$T1" -e "bash -c '"PATH=$PATH" $CMD1'" --geometry +0+0 $SEP -T "$T2" -e "bash -c '"PATH=$PATH" $CMD2'" --geometry +820+0 $SEP -T "$T3" -e "bash -c '"PATH=$PATH" $CMD3'" --geometry +0+500 $SEP -T "$T4" -e "bash -c '"PATH=$PATH" $CMD4'" --geometry +820+500

echo PID $$
