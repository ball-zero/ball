#!/bin/sh -ex

## Preface

# Command line parameters:

#  The directory to be created to hold the representative.
REP=$1

# The axiomatic paralegal preconditions valid for the representative.
CNSTTTN=$2

# The login name to be installed.  (Scope: local to this rep; unique)
LOGNAME=$3

# The CN (Common Name) (eg., for X509 certs; Scope: global; may conflict).
CN=$4

# The passord for the CA, "system" and $LOGNAME are initially all the same.
PASSWD=$5

# Either unset/empty for the old default or a file name to a CoreAPI
# expression to create a fresh object.

LOGNAME_LOAD=$6

BIN=askemos-chicken.bin

############## Checking the Environment #################
bailout () {
    echo $*
    exit 1
}

test -z $CNSTTTN && echo "usage $0 rep precondition"

needcmd () {
    bailout "could not find executable" $1
}

test -x `which $BIN` || needcmd $BIN
test -x `which sslmgr` || needcmd sslmgr
############## Check Password ###########################
[ X$PASSWD != X ]] || bailout "Password must not be emtpy."
[ $PASSWD != changethis ]] || bailout "Password must be 'changethis'."
#########################################################

## 1. Initial Contract

# -patch 1 -- backward compatible mode: initial contract changes state during setup.
$BIN -init $REP $CNSTTTN # -patch 0


# The steps 1.1 and 1.2 are optional.  In theory.

## 1.1 System Control Panel

# The control panel is in principle optional; without however, all
# administration must be done on the command line.

# Compile the .setup file for use in next step.

$BIN -offline $REP load -o .tmp/system.core policy/setup-system.setup

# Link the channel (a.k.a. entry point, a.k.a user accout) "system" to
# the result of evaluating the file .tmp/system.core as CoreAPI expression.

$BIN -offline $REP channel -link system .tmp/system.core

# Set the initial password for "system".

$BIN -offline $REP channel secret set system "$PASSWD"

## 1.2 The First User Becomes The Owner

# If this step is skipped, the rep can still function as a pure
# notary.  Without this user, it will not be able to send requests in
# the name of a particular person.

if [ "${LOGNAME_LOAD}" = "" ]; then
   # Another way to create a new entry.
   $BIN -offline $REP channel -new "$LOGNAME" system/xslt-method app/xslt-user.xml text/xml
else
   $BIN -offline $REP channel -link "$LOGNAME" "${LOGNAME_LOAD}"
fi


## 2. Certify The New Rep.

# Create Certificate Authority, host cert request and sign it.

$BIN -offline $REP tofu "$CN" "$LOGNAME" "$PASSWD"

# Set the initial password for $LOGNAME.

$BIN -offline $REP channel secret set "$LOGNAME" "$PASSWD"

