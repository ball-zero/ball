#!/bin/sh -ex

# Command line parameters:

NAME=$1

# Purpose:

# extend the user "me" from the initial setup under a given name on
# the other reps.

BIN=askemos-chicken.bin

# Expecting to be run in the build directory we include the output dir:
PATH=`pwd`/.tmp:$PATH

############## Checking the Environment #################
bailout () {
    echo $*
    exit 1
}

needcmd () {
    bailout "could not find executable" $1
}

test -x `which $BIN` || needcmd $BIN
test -x `which sslmgr` || needcmd sslmgr
#########################################################

R1=r1
ID1=`$BIN -online $R1 print public` # Query the rep(s) for it's public identifier
ON1=`$BIN -online $R1 print onion-route` # Query the rep(s) for it's onion route
URL1=${ON1:-https://127.0.0.1:9443}

R2=r2
ID2=`$BIN -online $R2 print public`
ON2=`$BIN -online $R1 print onion-route`
URL2=${ON2:-https://127.0.0.1:10443}
R3=r3
ID3=`$BIN -online $R3 print public`
R4=r4
ID4=`$BIN -online $R4 print public`
ON4=`$BIN -online $R1 print onion-route`
URL4=${ON4:-https://127.0.0.1:12443}

echo "Next we establish 'me' (from $R1) everywhere as " $NAME  "."

#sleep 5

# Make 'me' from first rep known elsewhere as lower case '$NAME'.
for i in $R2 $R3 $R4; do
    $BIN -online $i support $ID1 $NAME me || true
done

echo "Next we extend the replica set to cover all four reps."

sleep 5

# Helper to create a request to commission additional notaries for the demo wallet.
commform () {
 cat <<EOF
<form xmlns="http://www.askemos.org/2000/CoreAPI#">
 <action>toggle-replication-state</action>
 <name>$1</name>
</form>
EOF
}

for ID in $ID2 $ID3 $ID4; do
    commform $ID | $BIN -online $R1 via me send -in -
done
