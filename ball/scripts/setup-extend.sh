#!/bin/sh -ex

## Preface

# Command line parameters:

#  The directory to be created to hold the representative.
REP=$1

# The PORTBASE - to be moved together with section three below into a file of its own.
PORTBASE=$2

BIN=askemos-chicken.bin

############## Checking the Environment #################
bailout () {
    echo $*
    exit 1
}

test -z $CNSTTTN && echo "usage $0 rep precondition"

needcmd () {
    bailout "could not find executable" $1
}

test -x `which $BIN` || needcmd $BIN
test -x `which sslmgr` || needcmd sslmgr
#########################################################

# Install some demo code from the 'app' directory

# Compile those into a CoreAPI expression
$BIN -offline $REP load -o .tmp/populate-system.core app/populate-system.setup
# and load it into the "system" account, where they are searched for:
$BIN -offline $REP via "system" send .tmp/populate-system.core

# Eventually change some parameters.

# echo $PORTBASE | $BIN -offline $REP load scripts/change-ports.setup
