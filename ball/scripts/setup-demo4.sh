#!/bin/sh -ex

# Command line parameters:

# None.  Assuming values given in ../Makefile target `reps4demo4`.

BIN=askemos-chicken.bin

# Expecting to be run in the build directory we include the output dir:
PATH=`pwd`/.tmp:$PATH

############## Checking the Environment #################
bailout () {
    echo $*
    exit 1
}

needcmd () {
    bailout "could not find executable" $1
}

test -x `which $BIN` || needcmd $BIN
test -x `which sslmgr` || needcmd sslmgr
#########################################################

R1=r1
ID1=`$BIN -online $R1 print public` # Query the rep(s) for it's public identifier
ON1=`$BIN -online $R1 print onion-route` # Query the rep(s) for it's onion route
URL1=${ON1:-https://127.0.0.1:9443}

R2=r2
ID2=`$BIN -online $R2 print public`
ON2=`$BIN -online $R2 print onion-route`
URL2=${ON2:-https://127.0.0.1:10443}
R3=r3
ID3=`$BIN -online $R3 print public`
R4=r4
ID4=`$BIN -online $R4 print public`
ON4=`$BIN -online $R4 print onion-route`
URL4=${ON4:-https://127.0.0.1:12443}

#  Establish initial connection
for i in $R2 $R3 $R4; do
    $BIN -online $i connect $URL1 &
done

# TBD: This second and third round of connects should not be needed.
# To help with the undiscovered hang we connect again.
for i in $R1 $R3 $R4; do
    $BIN -online $i connect $URL2 &
done
for i in $R1 $R2 $R3; do
    $BIN -online $i connect $URL4
done

echo "Next we establish Alice (from $R3) and Bob (from $R4) everywhere."

sleep 5

# Make capital 'Bob' from fourth rep known elsewhere as lower case 'bob'.
#
# Readers loving Zookos triangle: there is the OID, which is machine
# readable and has global scope.  The human readable name has local
# scope.  "Bob" at $R4 and "bob" elsewhere.
for i in $R1 $R2 $R3; do
    $BIN -online $i support $ID4 bob Bob || true
done

# Similarily make 'Alice' from the third known elsewhere as 'al'.
for i in $R1 $R2 $R4; do
    $BIN -online $i support $ID3 al Alice || true
done

echo "Next we give Alice' and Bobs account their full name."
# This is normally done via browser.  Just that's hard to script.

sleep 5

# Note: this *really* depends on the target place to cooperate.
# Different contracts will nedd different setup.
echo "<form><title>Bob Beispiel</title></form>" | $BIN -online $R4 via Bob send -in -
echo "<form><title>Alice Example</title></form>" | $BIN -online $R3 via Alice send -in -

echo "Next we extend the replica set of Alice and Bob to cover all four reps."

sleep 5

# Helper to create a request to commission additional notaries for the demo wallet.
commform () {
 cat <<EOF
<form>
 <action>commission</action>
 <name>$1</name>
</form>
EOF
}

for ID in $ID1 $ID2 $ID3; do
    commform $ID | $BIN -online $R4 via Bob send -in -
done

for ID in $ID1 $ID2 $ID4; do
    commform $ID | $BIN -online $R3 via Alice send -in -
done

echo "Change authentication mode to 'fixed'.  New users must then be manually verified."

for i in $R1 $R2 $R3 $R4; do
    $BIN -online $i channel auth fixed
done
