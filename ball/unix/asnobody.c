/* (C) 2004 J�rg F. Wittenberger  See http://www.askemos.org

 * This is a very simple wrapper to safely execute system processes.

 * Most system components are huge C level code monstrosities.  We
 * would like to take advantage of the features, but we are concerned
 * about security issues.  Therefore we recomment to lock such
 * processes at least behind another user id, for better protection
 * within using chroot(2) or LD_PRELOAD tricks.  There are several
 * appropriate programms like "super" (comes with setiud) and "sudo"
 * system dependant chroot jails.  It is reccomented that you use one
 * of those for production environments.

 * To easy the initial installation, we provide the featureleast
 * implementation of such a setuid(2) jail here.

 * Compile: cc -o asnobody asnobody.c

 * This programm should be installed setuid as user "nobody" or
 * whoever is supposed to run CGI-alike scripts with untrusted data.
 * Set execution permission such that only the "askemos" group can
 * execute the wrapper.  Finally make sure the user "askemos" is the
 * only user in the askemos group.

 * To execute code as the "nobody" user prefix the command with
 * "asnobody".

 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char** argv){
  if(argc > 1){
    if( execv(argv[1],argv+1) == -1){
     fprintf(stderr,"%s calling %s error%s\n",
               argv[0],argv[1],strerror(errno));
     return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
  }
  fprintf(stderr,"%s called without arguments\n",argv[0]);
  return EXIT_FAILURE;
}
