/* (C) 2003, 2019 Jörg F. Wittenberger <Joerg.Wittenberger@softeyes.net>, BSD3 */

/* Start/restart Askemos kernel daemon, also a (supposed to be)
 * portable and "minimal fuss" way to set user and group id.
 *
 * The BALL kernel as of today still may need some restarts.  This is
 * more an unfortunate truth rather than design.  It is restarted from
 * this script, if it exitcode is anything but zero.
 */

#include <chicken.h> /* This version is for CHICKEN only! */

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>                /* exists but not needed on BSD */
#include <time.h>
#include <signal.h>
#include <string.h>

#define CHECK_PERIOD 10
#define STARTUP_DELAY (CHECK_PERIOD + 9)
#define RESTART_DELAY 3
#define RESTART_RETRY 3
#define USE_FORCED_RESTART 1

static int help(const char *const name) {
  fprintf(stderr, "%.256s [-u user] kernel-options...\n", name);
  fprintf(stderr, "  where\n");
  fprintf(stderr, "     user           : the name of the user to run as\n");
  fprintf(stderr, "     kernel-options : other option fed to kernel\n");
  exit(EXIT_FAILURE);
}

void *xmalloc(size_t s)
{
  void *result=malloc(s);
  if(result) return result;
  else {
    fprintf(stderr,
            "memory problem, malloc returned NULL allocating %d bytes\n",
           (int) s);
    exit(EXIT_FAILURE);
    return NULL;
  }
}

static pid_t the_kernel = 0;

static void sigterm_handler( int x )	/* signal handler */
{
#ifdef DEBUG_SIGNALS
  psignal(x, "heartbeat.c sigterm_handler() invocation on");
#endif
  kill(the_kernel, SIGTERM);
  exit(0);
}

static void sighup_handler( int x )	/* signal handler */
{
#ifdef DEBUG_SIGNALS
  psignal(x, "heartbeat.c sighup_handler() invocation on");
  psignal(SIGUSR1, "heartbeat.c sighup_handler() delegate");
#endif
  kill(the_kernel, SIGUSR1);
}

/*
 * Simple dead/locked/runaway process signal.  It's the kernels
 * watchdog responsibility to send a SIGUSR1 every second.  If it
 * misses two, we kill it.
 *
 * FIXME BSD has a different idea of setting permissions.  We replace
 * SIGUSR1 with SIGCONT for the time until everything has been worked
 * out.
 */

static int the_dead_flag = 1;
#ifdef USE_FORCED_RESTART
static int the_kill_counter = 0;
static void count_kill()
{
  if(the_kill_counter++ > 20) {
    int err;
    char *argv[5];
    argv[0] = "/usr/bin/sudo";
    argv[1] = "-n";
    argv[2] = "/sbin/reboot";
    argv[3] = "-f";
    argv[4]=NULL;
    fprintf(stderr, "Trying reboot\n");
    err = execv(argv[0], (char **const)argv);
    fprintf(stderr, "sudo reboot returned %d\n", err);
    exit(EXIT_FAILURE);
  }
}
#endif

static void sigalarm_handler( int x )     /* signal handler */
{
  if(the_dead_flag == 1) {
    fprintf(stderr, "Kernel did not send alive signal, signaling it.\n");
    kill(the_kernel, SIGUSR2);
    the_dead_flag = 2;
    alarm(STARTUP_DELAY);
  } else if(the_dead_flag == 2) {
    fprintf(stderr, "Kernel did not send alive signal, killing it.\n");
    kill(the_kernel, SIGKILL);
#ifdef USE_FORCED_RESTART
    count_kill();
#endif
    alarm(STARTUP_DELAY);
  } else {
    the_dead_flag = 1;
    alarm(CHECK_PERIOD);
  }
}

static void sigusr1_handler( int x )     /* signal handler */
{
#ifdef DEBUG_SIGNALS
  psignal(x, "heartbeat.c sigusr1_handler() invocation on");
#endif
  the_dead_flag = 0;
#ifdef USE_FORCED_RESTART
  the_kill_counter = 0;
#endif
}

static void set_run_user(const char* USER)
{
  struct passwd *user;

  if( ! (user = getpwnam (USER)) ) {
    fprintf(stderr, "can't find user %s.\n", USER);
    exit(EXIT_FAILURE);
  }

  if( ! geteuid() ) {
    if( initgroups(USER, user->pw_gid) ) {
      fprintf(stderr, "could not initialize groups for \"%s\", error %d\n",
              USER, errno);
      exit(EXIT_FAILURE);
    }

    if( setgid(user->pw_gid) ) {
      fprintf(stderr, "could not set group id %d for \"%s\", error %d.",
              user->pw_gid, USER, errno);
      exit(EXIT_FAILURE);
    }
    if( setuid(user->pw_uid) ) {
      fprintf(stderr, "could not set user id %d for \"%s\", error %d.",
              user->pw_uid, USER, errno);
      exit(EXIT_FAILURE);
    }
  }
}

static void exec_chicken_kernel(int argc, char **argv)
{
  C_word heap, stack, symbols;
  CHICKEN_parse_command_line(argc, argv, &heap, &stack, &symbols);
  CHICKEN_initialize(heap, stack, symbols, C_toplevel);
  CHICKEN_run(C_toplevel);
  fprintf(stderr, "Unexpected termination in CHICKEN toplevel.\n");
  exit(EXIT_FAILURE);
}

static int do_not_use_heartbeat(const char *arg)
{
  return
    // strncmp(arg, "-", 1) == 0 ||
    strcmp(arg, "-init") == 0
    || strcmp(arg, "-bare") == 0
    || strcmp(arg, "-offline") == 0
    || strcmp(arg, "-online") == 0
    || strcmp(arg, "-secret") == 0
    ;
}

int main( int argc, char **argv )
{
  int status=0;
  int run = 1, retry = RESTART_RETRY;
  struct passwd *user;

  if( argc < 2 ) help(argv[0]);
  else if( strcmp(argv[1], "-u") == 0 ) {
    if( argc < 4 ) help(argv[0]);
    else {
      set_run_user(argv[2]);
      argv[2] = argv[0];
      argc -= 2;
      argv += 2;
    }
  }

  if( do_not_use_heartbeat(argv[1])) {
    exec_chicken_kernel(argc, argv);
  }

  signal(SIGHUP, sighup_handler); /* restart kernel, reload config */

  signal(SIGTERM, sigterm_handler); /* terminate */
  signal(SIGINT, sigterm_handler);
  signal(SIGQUIT, sigterm_handler);
  signal(SIGABRT, sigterm_handler);

  signal(SIGALRM, sigalarm_handler); /* timer */

  /* signal(SIGUSR1, sigusr1_handler); */
  signal(SIGCONT, sigusr1_handler); /* kernel alive */

  while( run ) {
    time_t start = time(NULL);
    the_kernel = fork();
    if( the_kernel ) {
      alarm(STARTUP_DELAY);
      while( run ) {
        pid_t p = waitpid(the_kernel, &status, 0);
        if( p == the_kernel ) {
          time_t ran = time(NULL) - start;
          if WIFSIGNALED(status) {
	    psignal(WTERMSIG(status), "heartbeat.c Kernel exit on" );
	  }
          run = ! (WIFEXITED(status) && WEXITSTATUS(status) == 0) ;
          /* clock adjument could cause negative differences here */
          if( ran >= 0 ) {
	    if( ran < STARTUP_DELAY) {
	      fprintf(stderr, "Kernel ran just %ld seconds.\nPerhaps something is broken?\n", (long int) ran);
	      if( ! --retry ) {
		fprintf(stderr, "Exiting now.\n");
		exit(EXIT_FAILURE);
	      }
	    } else {
	      retry = RESTART_RETRY;
	    }
	  }
	  if( run ) sleep(RESTART_DELAY);
          break;
        } else if( p == -1 ) {
	  switch ( errno ) {
	  case ECHILD:
	    fprintf(stderr, "Kernel process %d exited.\n", the_kernel);
	    break;
	  case EINTR: break;
	  default:
	    fprintf(stderr, "Kernel waitpit(3) error: %s\n", strerror(errno));
	    exit(EXIT_FAILURE);
	  }
        }
      }
    } else {

      C_word heap, stack, symbols;

      if( setpgid(0,0) ) {
        fprintf(stderr, "Failed to set new process group for kernel.\n");
        exit(0);
      }

      exec_chicken_kernel(argc, argv);
    }
  }

  return WEXITSTATUS(status);
}
