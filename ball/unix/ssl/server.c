#ifndef USE_SELECT_SCHED
# define USE_POLL_SCHED 1
#endif

#define OPENSSL_ALLOW_CIPHERS "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA HIGH !RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS"

/*
 * TBD: This should be completely rewritten:
 *
 * 1. The whole "framing_mode" is inherited from RScheme, not required
 * and just adds complexity.  Should use a dedicated file descriptor
 * instead.
 *
 * 2. Better have a separate implementation for GNU TLS instead of
 * mixing.  GNU TLS is no longer fully supported by this code anyway.
 */

#if defined(AS_STANDALONE) && defined(AS_EMBEDDED)
#error "AS_STANDALONE and AS_EMBEDDED are mutually exclusive"
#endif

#if defined(AS_STANDALONE) && defined(AS_PTHREAD)
#error "AS_STANDALONE and AS_PTHREAD are mutually exclusive"
#endif

#if !defined(AS_EMBEDDED) && defined(AS_PTHREAD)
#define AS_EMBEDDED
#endif

#ifndef AS_EMBEDDED
#define AS_STANDALONE 1
#else
#undef AS_STANDALONE
#endif

/* begin: deprecated inet_aton only! */
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif
/* end: deprecated inet_aton only! */


#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#ifndef _WIN32
# include <netdb.h>
#endif
#include <signal.h>
#if defined(USE_OPENSSL)
#include <openssl/ssl.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#elif defined(USE_GNUTLS)
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gcrypt.h>		/* for gcry_control */
#endif

#ifndef _WIN32
# if defined(USE_POLL_SCHED)
#  include <poll.h>
# else
# include <sys/select.h>
# endif
#endif

#ifdef HAVE_CAPABILITY_H
#include <sys/capability.h>
#endif
#include <sys/types.h>
#ifdef _WIN32
/**
 * int inet_pton(int af, const char *src, void *dst);
 *
 * https://github.com/dubcanada/inet_pton/blob/master/src/inet_pton.c
 *
 * Compatible with inet_pton just replace inet_pton with xp_inet_pton
 * and you are good to go. Uses WSAStringToAddressA instead of
 * inet_pton, compatible with Windows 2000 +
 */
static int inet_pton(int family, const char *src, void *dst)
{
    int rc;
    struct sockaddr_storage addr;
    int addr_len = sizeof(addr);

    addr.ss_family = family;

    rc = WSAStringToAddressA((char *) src, family, NULL,
        (struct sockaddr*) &addr, &addr_len);
    if (rc != 0) {
        return -1;
    }

    if (family == AF_INET) {
        memcpy(dst, &((struct sockaddr_in *) &addr)->sin_addr,
            sizeof(struct in_addr));
    } else if (family == AF_INET6) {
        memcpy(dst, &((struct sockaddr_in6 *)&addr)->sin6_addr,
            sizeof(struct in6_addr));
    }

    return 1;
}
#else
# include <sys/socket.h>
# include <netinet/in.h>
#include <arpa/inet.h>
#endif


#define SEED_SIZE  (512)

/* framing prococol */

#define CONTROL_TAG     (0xCC000000)
#define DATA_TAG        (0xDD000000)

#define TAG_MASK        (0xFF000000)
#define LENGTH_MASK     (0x0003FFFF)    /* 256K-1 max block size */

#define CTL_SHUTDOWN    ('c')
#define CTL_PASSWORD    ('p')
#define CTL_X509VERIFY  ('v')

#define B_MESHCERT_ELEMENT_LEN_MAX 48


struct ssl_mgr_state {

  int framing_mode;
  int input_fd;
  int output_fd;
  int status_fd;
  int network_socket;
  int verbose;
  int wot_mode;
  int poll_timeout;

  char buffer[LENGTH_MASK+1];

#if defined(USE_OPENSSL)

  SSL_CTX *ctx;
  SSL *cnx;
  X509 *statused_cert;

#elif defined(USE_GNUTLS)

  gnutls_certificate_credentials_t ctx;
  gnutls_session_t cnx;
  char statused_cert[1024];

#define DH_BITS 1024
  gnutls_dh_params_t dh_params;
#endif

  /* local to 'verify_callback' */
  size_t llen, olen;
  char lbuf[B_MESHCERT_ELEMENT_LEN_MAX+1], obuf[B_MESHCERT_ELEMENT_LEN_MAX+1];

};

typedef struct ssl_mgr_state * mgr_state_t;

static int mydata_index = -1;

static void setup_mgr_state(struct ssl_mgr_state *s)
{
  s->framing_mode = 0;
  s->poll_timeout = -1;
  s->input_fd = -1;
  s->output_fd = -1;
  s->status_fd = -1;
  s->network_socket = -1;
  s->verbose = 0;
  s->wot_mode = 0;

  s->ctx = NULL;
  s->cnx = NULL;

#if defined(USE_OPENSSL)

  s->statused_cert = NULL;

#elif defined(USE_GNUTLS)

  s->statused_cert[0] = '\0';

#endif

  /* local to 'verify_callback' */
  s->llen = 0, s->olen = 0;
}

#define DEBUG_TRACE_SSL_MGR(x)
//#define DEBUG_TRACE_SSL_MGR(x) x

//#define MGR_CLOSE(id, key, ref) if((ref)!=-1) { close(ref); ref=-1; }
#define MGR_CLOSE(id, key, ref) if((ref)!=-1) { DEBUG_TRACE_SSL_MGR(fprintf(stderr, "mgr %d close %s fd %d\n", id, key, ref);) close(ref); (ref)=-1; }

static void cleanup_mgr_state(struct ssl_mgr_state *s)
{
  int id = s->network_socket;
  DEBUG_TRACE_SSL_MGR(fprintf(stderr, "cleanup_mgr_state %d\n", id);)

#if defined(USE_OPENSSL)

  if(s->statused_cert) {
    X509_free(s->statused_cert);
    s->statused_cert = NULL;
  }

  if( s->cnx != NULL ) {
    SSL_free(s->cnx);
    s->cnx = NULL;
  }

  SSL_CTX_free(s->ctx);
  s->ctx=NULL;

#elif defined(USE_GNUTLS)

  if(tstate->cnx != NULL) {
    gnutls_deinit(cnx);
    tstate->cnx = NULL;
  }
  s->statused_cert[0] = '\0';

#endif

  MGR_CLOSE(id, "network", s->network_socket);
  if(s->input_fd != s->output_fd) {
    MGR_CLOSE(id, "input", s->input_fd);
  } else {
#ifdef _WIN32
    shutdown(s->input_fd, SD_RECEIVE);
#else
    shutdown(s->input_fd, SHUT_RD);
#endif
  }
  if(s->output_fd != s->status_fd) {
    MGR_CLOSE(id, "output", s->output_fd);
  } else {
#ifdef _WIN32
    shutdown(s->output_fd, SD_SEND);
#else
    shutdown(s->output_fd, SHUT_WR);
#endif
  }
  MGR_CLOSE(id, "status", s->status_fd);
  /* This COULD be the same as status_fd and cleanup migh be called multiple times */
  s->input_fd = -1;
  s->output_fd = -1;
}

#ifndef AS_PTHREAD
static void ssl_mgr_exit(mgr_state_t tstate, int code)
{
  cleanup_mgr_state(tstate);
  DEBUG_TRACE_SSL_MGR(fprintf(stderr, "Exit in ssl_server %d\n", code);)
  exit(code);
}
#else
static void ssl_mgr_exit(mgr_state_t tstate, int code)
{
  cleanup_mgr_state(tstate);
  DEBUG_TRACE_SSL_MGR(fprintf(stderr, "Pthread_Exit in ssl_server %d\n", code);)
  pthread_exit((void*) (size_t) (code));
}
#endif

static unsigned read_frame(mgr_state_t tstate, int fd );

typedef unsigned char u8;
#ifdef _WIN32
 typedef uint32_t u32;
#else
 typedef u_int32_t u32;
#endif
typedef uint64_t u64;

void status(mgr_state_t s, const char *msg, ... )
{
  char *bufptr, *buf, small[8192];
  va_list va;
  size_t n;
  int rc;

  buf = &small[0];
  n = sizeof(small);
  bufptr = buf;

  if (s->framing_mode) {
    bufptr += sizeof(u32);
    n -= sizeof(u32);         /* room for the header */
  } else {
    memcpy( &buf[0], "%[", 2 );
    bufptr = &buf[2];
    n -= 5;                             /* room for the "%[]%\n" */
  }

  va_start( va, msg );
  rc = vsnprintf( bufptr, n, msg, va );
  va_end( va );

  if (rc < 0) {
    perror( "vsnprintf" );
    ssl_mgr_exit( s,  2 );
  }
  bufptr += rc;

  if (rc > n) {
    rc = sprintf( buf, "status-overflow %d", rc );
  }

  if (s->status_fd >= 0) {

    if (s->framing_mode) {
      ((u32 *)buf)[0] = CONTROL_TAG | rc;
      write(s->status_fd, buf, rc + sizeof( u32 ));
    } else {
      memcpy( bufptr, "]%\n", 3 );
      write(s->status_fd, buf, rc + 5);
    }
  }
}

void status_errno(mgr_state_t tstate, const char *msg )
{
  DEBUG_TRACE_SSL_MGR(fprintf(stderr, "error: %d %s: %s\n", tstate->network_socket, msg, strerror(errno));)
  status(tstate, "error: %s: %s", msg, strerror(errno) );
}

/* Enforce keep alive handling */

#ifdef AS_STANDALONE

static int the_dead_flag = 0;
static int the_check_period = 0;
static mgr_state_t the_global_state = NULL;

static void sigalarm_handler( int x )     /* signal handler */
{
  if(the_dead_flag == 1) {
    status(the_global_state, "error: connection appears dead");
    ssl_mgr_exit(the_global_state, 1);
  } else if(the_check_period == 0) {
  } else {
    the_dead_flag = 1;
    alarm(the_check_period);
  }
}

#define SETALIVE {the_dead_flag = 0;}
#else
#define SETALIVE /* nothing */
#endif

#if defined(USE_OPENSSL)
void status_cert(mgr_state_t tstate, const char *tag, X509 *cert )
{
  if (cert) {
    char tmp[1024];

    status( tstate, "%s %s",
            tag,
            X509_NAME_oneline( X509_get_subject_name( cert ),
                               tmp, sizeof(tmp) ) );
  } else {
    status(tstate, "%s -none-", tag );
  }
}

void ssl_choke(mgr_state_t tstate, int rc )
{
  char buf[256];

  while (1) {
    unsigned long err = ERR_get_error();
    if (!err) {
      break;
    }
    status(tstate, "ssl-err %d %s", rc, ERR_error_string( err, buf ) );
    DEBUG_TRACE_SSL_MGR(fprintf(stderr, "ssl-err %d %s", rc, buf);)
  }
  ssl_mgr_exit(tstate, rc);
}

#ifndef _WIN32
static void loadrand( void )
{
  unsigned char buf[SEED_SIZE];
  int fd;

  fd = open( "/dev/urandom", O_RDONLY );
  read( fd, buf, SEED_SIZE );

  RAND_seed( &buf[0], SEED_SIZE );
  memset( &buf[0], 0, SEED_SIZE );
  close( fd );
}
#else
#define RtlGenRandom SystemFunction036
BOOLEAN NTAPI RtlGenRandom(PVOID RandomBuffer,ULONG RandomBufferLength);
static void loadrand( void )
{
  unsigned char buf[SEED_SIZE];
  if(!RtlGenRandom(&buf, sizeof(buf))) {
    perror("RtlGenRandom");
    exit(1);
  }

  RAND_seed( &buf[0], SEED_SIZE );
  memset( &buf[0], 0, SEED_SIZE );
}
#endif

static int verify_callback(int preverify_ok, X509_STORE_CTX *ctx)
{
  int     err, depth;

  X509 *current_cert = X509_STORE_CTX_get_current_cert(ctx);
  SSL *ssl = X509_STORE_CTX_get_ex_data(ctx, SSL_get_ex_data_X509_STORE_CTX_idx());
  mgr_state_t tstate = ssl ? SSL_get_ex_data(ssl, mydata_index) : NULL;

  if( tstate == NULL ) {
    perror("SSL_CTX_get_ex_data(ssl, mydata_index)");
    return 0; /* fail if preconditions are not met */
  }

  err = X509_STORE_CTX_get_error(ctx);
  depth = X509_STORE_CTX_get_error_depth(ctx);

  /*
   * Retrieve the pointer to the SSL of the connection currently treated
   * and the application specific data stored into the SSL object.
   */
  // ssl = X509_STORE_CTX_get_ex_data(ctx, SSL_get_ex_data_X509_STORE_CTX_idx());

  /*
   * Catch a too long certificate chain. The depth limit set using
   * SSL_CTX_set_verify_depth() is by purpose set to "limit+1" so
   * that whenever the "depth>verify_depth" condition is met, we
   * have violated the limit and want to log this error condition.
   * We must do it here, because the CHAIN_TOO_LONG error would not
   * be found explicitly; only errors introduced by cutting off the
   * additional certificates would be logged.
   */
  if (depth > 2) {
    preverify_ok = 0;
    err = X509_V_ERR_CERT_CHAIN_TOO_LONG;
    X509_STORE_CTX_set_error(ctx, err);
    return 0;
  }

  /*
   * At this point, err contains the last verification error. We can use
   * it for something special
   */
  /*
  if (!preverify_ok && (err == X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT))  {
    X509_NAME_oneline(X509_get_issuer_name(ctx->current_cert), nbuf, 256);
    fprintf(stderr, "issuer= %s\n", nbuf);
  }
  */

  if (tstate->wot_mode && depth==1 && (!err || err == X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN)) {
    u32 hdr, n;
    char *pc;
    long pcl;
    BIO *mem = BIO_new(BIO_s_mem());
    EVP_PKEY *pkey = current_cert == NULL ? NULL : X509_get_pubkey(current_cert);
    //status_cert("peer-ca", ctx->current_cert);
    /* We pin the whole public key.  Maybe we better took only a hash. */
    // fprintf(stderr, "WOT verify ONE\n");
    if(pkey == NULL || !PEM_write_bio_PUBKEY(mem, pkey)) {
      DEBUG_TRACE_SSL_MGR(fprintf(stderr, "PEM_write_bio_X509 failed\n");)
      ssl_mgr_exit(tstate, 92);
    }
    pcl=BIO_get_mem_data(mem, &pc);
    status(tstate, "%s %.*s", "peer-cakey", pcl, pc);
    BIO_free(mem);
    // TBD: return result from WoT DB lookup here.
    hdr = read_frame(tstate, tstate->input_fd );

    if ((hdr & TAG_MASK) != CONTROL_TAG) {
      DEBUG_TRACE_SSL_MGR(fprintf(stderr, "error: can't do data while getting cert verification\n");)
      status(tstate, "error: can't do data while getting cert verification" );
      ssl_mgr_exit(tstate, 93);
    }

    n = hdr & LENGTH_MASK;

    if (n == 1 && tstate->buffer[0] == CTL_SHUTDOWN) {
      ssl_mgr_exit(tstate, 0);
    }

    if (n < 2) {
      DEBUG_TRACE_SSL_MGR(fprintf(stderr, "error: needed verify result\n");)
      status(tstate, "error: needed verify result" );
      ssl_mgr_exit(tstate, 94);
    }

    if (tstate->buffer[0] != CTL_X509VERIFY) {
      DEBUG_TRACE_SSL_MGR(fprintf(stderr, "error: can't process non-verify now %s\n", tstate->buffer);)
      status(tstate, "error: can't process non-verify now" );
      ssl_mgr_exit(tstate, 95);
    }

    if ( tstate->buffer[1]!='0') {
      n-=2;
      const char *p0=tstate->buffer+2;
      const char *space = strchr(p0, '/');
      size_t l1, l2;
      assert(space!=NULL);
      l1 = (space-p0) > B_MESHCERT_ELEMENT_LEN_MAX ? B_MESHCERT_ELEMENT_LEN_MAX : (space-p0);
      if ( l1 > 0 ) {
	tstate->llen = l1;
	memcpy(tstate->lbuf, p0, l1);
	tstate->lbuf[l1+1]='\0';
      }
      l2 = space+1-p0;
      l2 = n-l2 > B_MESHCERT_ELEMENT_LEN_MAX ? B_MESHCERT_ELEMENT_LEN_MAX : n-l2;
      if ( l2 > 0 ) {
	tstate->olen = l2;
	memcpy(tstate->obuf, space+1, l2);
	tstate->obuf[l2+1]='\0';
      }
    }
    /* Since we check below, we do not only need to continue here.  We
       must also note the sucessful verification in `ctx`. */
    if ( tstate->buffer[1]!='0' ) X509_STORE_CTX_set_error(ctx, X509_V_OK);
    return tstate->buffer[1]!='0';
  } else if (preverify_ok && tstate->wot_mode && depth==0 && current_cert != NULL) {
    char lb[B_MESHCERT_ELEMENT_LEN_MAX];
    X509_NAME *subj = X509_get_subject_name(current_cert);
    if(subj==NULL) return 0;
    if ( tstate->llen > 0 ) {
      int l2 = X509_NAME_get_text_by_NID(subj, NID_localityName, lb, B_MESHCERT_ELEMENT_LEN_MAX);
      if ( l2>0 && !(l2==tstate->llen && strncmp(tstate->lbuf, lb, l2)==0) ) {
	// fprintf(stderr, "MESH-L match error: llen %zd LBUF: %s certll %d %s\n\n", llen, lbuf, l2, lb);
	return 0;
      }
    }
    if ( tstate->olen > 0 ) {
      int l2 = X509_NAME_get_text_by_NID(subj, NID_organizationName, lb, B_MESHCERT_ELEMENT_LEN_MAX);
      if ( l2>0 && !(l2==tstate->olen && strncmp(tstate->obuf, lb, l2)==0) ) {
	// fprintf(stderr, "MESH-O error: olen %zd OBUF: %s l2 %d %s\n\n", olen, obuf, l2, lb);
	return 0;
      }
    }
    /* TBD: Check possible revocation of this certificate. */

    // fprintf(stderr, "TLS verify OK\n");
    return 1;
  } else {
    // fprintf(stderr, "VERIFY FAIL\n");
    return preverify_ok;
  }
}

#elif defined(USE_GNUTLS)
void ssl_choke(mgr_state_t tstate, int rc )
{
  status(tstate, "ssl-err %d", rc);
  ssl_mgr_exit(tstate, rc);
}
#endif

#define SOCKS_BUFLEN 128

static void socks_setup(mgr_state_t tstate, int fd, char *hf, int pf )
{
  char buffer[SOCKS_BUFLEN+1], *buf=buffer;
  int len = snprintf(buf, SOCKS_BUFLEN + 1,
                     "\x4\x1%c%c00010%s",
                     (char) ((pf & 0xff00) >> 8), /* port MSB */
                     (char) (pf & 0xff), /* port LSB */
                     hf);
  if( len >= SOCKS_BUFLEN ) {
    buf = alloca( len + 1 );  // should never happen
    snprintf(buf, len + 1,
             "\x4\x1%c%c00010%s",
             (char) ((pf & 0xff00) >> 8), /* port MSB */
             (char) (pf & 0xff), /* port LSB */
             hf);
  }
  buf[4]='\0';
  buf[5]='\0';
  buf[6]='\0';
  buf[7]='\1';
  buf[8]='\0';
  if (write( fd, buf, len+1 ) != len+1) {
    status_errno(tstate, "socks server connect request" );
    ssl_mgr_exit(tstate,  3 );
  }
  len = read( fd, buf, 8 );
  if( len != 8 || buf[0] != 0 ) {
    status_errno(tstate, "socks server connect" );
    ssl_mgr_exit(tstate,  3 );
  }
  if( buf[1] != (char) 90 ) {
    char *msg = "unknown code";
    switch( (int) buf[1] ) {
    case 91: msg = "request rejected or failed"; break;
    case 92: msg = "rejected; server cannot connect to identd on the client";
      break;
    case 93: msg = "rejected because the client program and identd report different user-ids";
      break;
    }
    status(tstate, "socks connect %s:%d %s", hf, pf, msg );
    ssl_mgr_exit(tstate,  3 );
  }
}

#define SMODE_CLIENT   (1)
#define SMODE_SERVER   (2)

int get_socket (mgr_state_t tstate,
                const char *socks,      /* socks proxy spec */
                const char *addr,       /* local addr/port spec */
                const char *port,       /* remote addr/port spec (local in server mode) */
                int *mode )
{
  int rc;

  DEBUG_TRACE_SSL_MGR(fprintf(stderr, "getting socket as %s\n", port);)

  if (strncmp( port, "fdsrv:", 6 ) == 0) {
    *mode = SMODE_SERVER;
    return atoi( port+6 );
  } else if (strncmp( port, "fdclient:", 9 ) == 0) {
    *mode = SMODE_CLIENT;
    return atoi( port+9 );
  } else if (strncmp( port, "listen:", 7 ) == 0) {
    /* FIXME: This case is broken, fd is never closed! */
    int fd = socket( PF_INET, SOCK_STREAM, 0 );
    struct sockaddr_in sa, peer;
    socklen_t peer_len;

    *mode = SMODE_SERVER;

    memset( &sa, 0, sizeof(sa) );
    sa.sin_family = AF_INET;
    sa.sin_port = htons( atoi( port+7 ) );
    sa.sin_addr.s_addr = INADDR_ANY;

    if (tstate->verbose) {
      status( tstate, "note: server on port <%d>", ntohs( sa.sin_port ) );
    }

    rc = bind( fd, (struct sockaddr *)&sa, sizeof(sa) );
    if (rc < 0) {
      status_errno(tstate, "bind" );
      ssl_mgr_exit(tstate, 3);
    }

    rc = listen( fd, 3 );
    if (rc < 0) {
      status_errno(tstate, "listen" );
      ssl_mgr_exit(tstate, 3);
    }

    peer_len = sizeof( peer );
    rc = accept( fd, (struct sockaddr *)&peer, &peer_len );
    if (rc < 0) {
      status_errno(tstate, "accept" );
      ssl_mgr_exit(tstate, 3);
    }
    return rc;
  } else if (strncmp( port, "connect:", 8 ) == 0) {
    int fd = -1;
    int ipv6 = 0;
    struct sockaddr_in sa4;
    struct in_addr in4;
    struct sockaddr_in6 sa;
    struct in6_addr in;
    int ip6_bracked_left = port[8] == '[';
    const char *ip6_bracked_right = ip6_bracked_left ? strrchr( port + 8, ']' ) : NULL;
    /* socklen_t peer_len; */
    const char *x = !ip6_bracked_left ? strrchr( port + 8, ':' ) :
      ip6_bracked_right ? strrchr(ip6_bracked_right, ':' ) : NULL;
    char *h, *hf=NULL;
    struct hostent *e;
    int p, pf=0;

    *mode = SMODE_CLIENT;

    if (x && !ip6_bracked_right) {
      h = alloca( x-(port+8) + 1 );
      memcpy( h, port+8, x-(port+8) );
      h[x-(port+8)] = 0;
      x++;
    } else if (x) {
      int len = ip6_bracked_right-(port+9);
      h = alloca( len + 1 );
      memcpy( h, port+9, len );
      h[len] = 0;
      x++;
    } else {
      x = "443";        /* https */
      h = (char *)port+8;
    }
    if (tstate->verbose) {
      status( tstate, "note: client to <%s> port <%s>", h, x );
    }

    p = atoi( x );
    if ((p < 1) || (p > 65535)) {
      status( tstate, "error: bad port number <%s>", x );
      ssl_mgr_exit(tstate,  1 );
    }

    if (socks) {
      hf = h;
      pf = p;
      x = strrchr( socks, ':' );  // FIXME for IPv6
      if (x) {
	h = alloca( x-socks + 1 );
	memcpy( h, socks, x-socks );
	h[x-socks] = 0;
	x++;
	p = atoi( x );
      } else {
	p = 9050;        /* socks */
	h = (char *)socks;
      }
    }

    memset( &sa4, 0, sizeof(sa4) );
    memset( &sa, 0, sizeof(sa) );

    if(inet_pton(AF_INET, h, &in4)) {
      sa4.sin_family = AF_INET;
      sa4.sin_addr = in4;
    } else if(inet_pton(AF_INET6, h, &in)) {
      ipv6 = 1;
      sa.sin6_family = AF_INET6;
      sa.sin6_addr = in;
    } else {
      e = gethostbyname( h );
      if (!e) {
	status( tstate, "error: host name resolution error on <%s>", h );
	ssl_mgr_exit(tstate,  3 );
      }

      if (!e->h_addr_list[0]) {
	status( tstate, "error: host name lookup did not provide address for <%s>", h );
	ssl_mgr_exit(tstate,  1 );
      }
      sa4.sin_addr = *(struct in_addr *)e->h_addr_list[0];
    }


    if(ipv6) {
      sa.sin6_port = htons( p );
      fd = socket( PF_INET6, SOCK_STREAM, 0 );
      do {
        DEBUG_TRACE_SSL_MGR(fprintf( stderr, "connect: %d %s\n", fd, h);)
        rc = fd >= 0 ? connect( fd, (struct sockaddr *)&sa, sizeof(sa) ) : -1;
      } while( fd >= 0 && rc==-1 && (errno==EINTR || errno==EAGAIN) );
    } else {
      sa4.sin_port = htons( p );
      fd = socket( PF_INET, SOCK_STREAM, 0 );
      do {
        rc = fd >= 0 ? connect( fd, (struct sockaddr *)&sa4, sizeof(sa4) ) : -1;
      } while( fd >= 0 && rc==-1 && (errno==EINTR || errno==EAGAIN) );
    }

    tstate->network_socket = fd;  /* register in case connect failed, to be closed in ssl_mgr_exit */

    if (rc < 0) {
      status_errno(tstate, "connect" );
      ssl_mgr_exit(tstate,  3 );
    }

    if (socks != NULL) {
      socks_setup(tstate, fd, hf, pf );
    }

    return fd;
  } else {
    DEBUG_TRACE_SSL_MGR(fprintf( stderr, "bad port spec: %s\n", port );)
    ssl_mgr_exit(tstate,  1 );
    return -1; /* never reached */
  }
}

#if defined(USE_OPENSSL)
void ssl_loop(mgr_state_t tstate, SSL *cnx, int sock, int in, int out );
#elif defined(USE_GNUTLS)
void ssl_loop(mgr_state_t tstate, gnutls_session_t cnx, int sock, int in, int out );
#endif

static int passwd_cb( char *pass, int size, int rwflag, void *userdata )
{
  mgr_state_t tstate = userdata;
  u32 hdr;
  unsigned i, n;

  if (rwflag) {
    return 0;
  }
  status(tstate, "passwd:");

  if (getenv( "PASS" )) {
    strcpy( pass, getenv( "PASS" ) );
    return strlen( getenv( "PASS" ) );
  }

  if (!tstate->framing_mode) {
    status( tstate, "error: can't get password in non-framing mode" );
    ssl_mgr_exit(tstate, 101);
  }

  hdr = read_frame(tstate, tstate->input_fd );

  if ((hdr & TAG_MASK) != CONTROL_TAG) {
    status(tstate, "error: can't do data while getting password" );
    ssl_mgr_exit(tstate, 102);
  }

  n = hdr & LENGTH_MASK;

  if (n == 0) {
    status(tstate, "error: needed password" );
    ssl_mgr_exit(tstate, 103);
  }

  if (tstate->buffer[0] != CTL_PASSWORD) {
    status(tstate, "error: can't process non-CTL_PASSWORD now" );
    ssl_mgr_exit(tstate, 104);
  }

  n--;
  if (n > size) {
    status(tstate, "error: actual password length %d > max length %d", n, size );
    ssl_mgr_exit(tstate, 105);
  }

  tstate->buffer[1+n] = 0;
  /*status(tstate, "password <%s>", buf+1 );*/
  for (i=0; i<n; i++) {
    unsigned char ch = tstate->buffer[i+1];
    if (isupper( ch )) {
      pass[i] = 'A' + ('Z' - ch);
    } else if (islower( ch )) {
      pass[i] = 'a' + ('z' - ch);
    } else {
      pass[i] = ch;
    }
  }
  return n;
}

#ifdef AS_EMBEDDED
// Literally including `px4_getopt` in order to minimize the number of
// changed code chunks when running in pthreads.  The code is at the
// end of the file.  These changes shall be reverted as soon as we
// simply arrange the setup without passing/parsing a command line.
int px4_getopt(int argc, char *argv[], const char *options, int *myoptind, const char **myoptarg);
#define optarg myoptarg
#define getopt(c,v,o) px4_getopt(c,v,o,&myoptind,&myoptarg)
#define __EXPORT
#endif

static int ssl_mgr_main(struct ssl_mgr_state *s, int argc, char * /*const*/ *argv )
{
#if defined(USE_OPENSSL)
  SSL_CTX *ctx = NULL;
  SSL *cnx = NULL;
#elif defined(USE_GNUTLS)
  gnutls_certificate_credentials_t ctx;
  gnutls_priority_t priority_cache;
  gnutls_session_t cnx;
#endif

#ifdef AS_EMBEDDED
  int myoptind = 1;
  const char *myoptarg = NULL;
#endif
  int rc, fd;
  const char *certfile = NULL; /* "server.pem"; */
  const char *pkeyfile = NULL; /* "server.pkey"; */
  const char *useport = "listen:443";
  const char *usesocks = NULL;
  const char *useaddr = NULL;

  int peer_cert_mode = 0;
  int op_mode;
  const char *ca = NULL;
  const char *trust = NULL;
  const char *jail = NULL;

  int retry_restart;
  assert( sizeof(u32) == 4 );

  while (1) {
    switch (getopt( argc, argv, "c:p:k:FeEwA:T:vqJ:i:s:t:" )) {
    case 'q':
      s->status_fd = -1;
      break;
    case 'J':
      jail = optarg;
      break;
    case 'v':
      s->verbose++;
      break;
    case 'c':
      certfile = optarg;
      break;
    case 'A':
      ca = optarg;
      break;
    case 'T':
      trust = optarg;
      break;
    case 'e':
      peer_cert_mode = 1;
      break;
    case 'E':
      peer_cert_mode = 2;
      break;
    case 'w':
      s->wot_mode = 1;
      break;
    case 'F':
      s->framing_mode = 1;
      break;
    case 'k':
      pkeyfile = optarg;
      break;
    case 'p':
      useport = optarg;
      break;
    case 'i':
      useaddr = optarg;
      break;
    case 's':
      usesocks = optarg;
      break;
    case 't':
#ifdef AS_STANDALONE
      the_check_period = atoi(optarg);
      the_global_state = s;
#else
      s->poll_timeout = 1000 * atoi(optarg);
#endif
      break;
    case '?':
      fprintf( stderr, "usage: %s [-c cert] [-k key] [-p port] [-i ip-addr] [-s socks4a-proxy] [-t timeout]\n", argv[0] );
      ssl_mgr_exit(s,  1 );
    case -1:
      goto done;
    }
  }
 done:

  if (s->framing_mode) {
    s->status_fd = s->output_fd;
  }

  if ( s->verbose ) {
    status(s, "sslmgr 1.0 pid=%d", getpid() );
  }

#ifdef AS_STANDALONE
  signal(SIGALRM, sigalarm_handler);
  if( the_check_period ) {
    alarm(the_check_period);
  }
#endif

  fd = get_socket( s, usesocks, useaddr, useport, &op_mode );
  s->network_socket = fd;
  DEBUG_TRACE_SSL_MGR(fprintf(stderr, "FD: %d on: %s\n", s->network_socket, useport);)

#if defined (USE_OPENSSL)

  if ( mydata_index == -1) {
    SSL_load_error_strings();
    SSL_library_init();
    loadrand();
    mydata_index = SSL_get_ex_new_index(0, "mydata index", NULL, NULL, NULL);
  }

  if (op_mode == SMODE_CLIENT) {
    ctx = SSL_CTX_new( TLS_client_method() );
  } else {
    ctx = SSL_CTX_new( TLS_server_method() );
  }

  if (!ctx) {
    status(s, "error: SSL_CTX creation failed" );
    ssl_mgr_exit(s, 30);
  } else {
    s->ctx = ctx;
  }

  // long SSL_CTX_clear_mode(SSL_CTX ctx, long mode);

  if(!SSL_CTX_set_cipher_list(ctx, OPENSSL_ALLOW_CIPHERS)) {
    status(s, "could not set cipher list to %s\n", OPENSSL_ALLOW_CIPHERS);
    ssl_mgr_exit(s, 1);
  }
  SSL_CTX_set_options(ctx, SSL_OP_ALL); /* enable known bug work arounds */
  SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv3);
  SSL_CTX_set_options(ctx, SSL_OP_NO_TLSv1);
  /* prefer cipher order from server */
  SSL_CTX_set_options(ctx, SSL_OP_CIPHER_SERVER_PREFERENCE);

  SSL_CTX_set_default_passwd_cb( ctx, passwd_cb );
  SSL_CTX_set_default_passwd_cb_userdata( ctx, s );

#elif defined(USE_GNUTLS)

  /* to disallow usage of the blocking /dev/random */
  gcry_control (GCRYCTL_ENABLE_QUICK_RANDOM, 0);
  gnutls_global_init();
  gnutls_certificate_allocate_credentials (&ctx);
  if (!ctx) {
    status(s, "error: SSL_CTX creation failed" );
    ssl_mgr_exit(s, 30);
  } else {
    s->ctx = ctx;
  }

#endif

  if (ca) {
#   if defined (USE_OPENSSL)
    X509 *cert = NULL;
    FILE *f = fopen( ca, "r" );
    char tmp[1024];

    if(f==NULL) ssl_choke( s, 23 );

    SSL_CTX_set_default_passwd_cb_userdata( ctx, "ca" );

    cert = PEM_read_X509( f, NULL, NULL, NULL );
    fclose(f);
    if (!cert) {
      ssl_choke( s, 23 );
    }

    if (s->verbose) {
      status( s, "note: ca is %s",
              X509_NAME_oneline( X509_get_subject_name( cert ),
                                 tmp, sizeof(tmp) ) );
    }

    if (!SSL_CTX_add_client_CA( ctx, cert )) {
      ssl_choke( s, 22 );
    }
#   elif defined(USE_GNUTLS)
    /* sets the trusted cas file */
    gnutls_certificate_set_x509_trust_file (ctx, ca, GNUTLS_X509_FMT_PEM);
#   endif
  }

  if (trust) {
#   if defined (USE_OPENSSL)
    if (!SSL_CTX_load_verify_locations( ctx, trust, NULL )) {
      ssl_choke( s, 28 );
    }
#   elif defined(USE_GNUTLS)
#   endif
  }

#if defined(USE_OPENSSL)

  SSL_CTX_set_default_passwd_cb_userdata( ctx, "client" );
  if (certfile != NULL && !SSL_CTX_use_certificate_chain_file( ctx, certfile )) {
    ssl_choke( s, 25 );
  }

  SSL_CTX_set_default_passwd_cb_userdata( ctx, "private" );
  if (pkeyfile != NULL && !SSL_CTX_use_PrivateKey_file( ctx, pkeyfile, SSL_FILETYPE_PEM )) {
    ssl_choke( s, 26 );
  }

  SSL_CTX_set_default_passwd_cb_userdata( ctx, "?" );
  if (pkeyfile != NULL && !SSL_CTX_check_private_key( ctx )) {
    ssl_choke( s, 27 );
  }

#elif defined(USE_GNUTLS)

  if (certfile != NULL && pkeyfile != NULL &&
      !gnutls_certificate_set_x509_key_file (ctx, certfile, pkeyfile, GNUTLS_X509_FMT_PEM)) {
    ssl_choke( s, 28 );
  }

  if (op_mode == SMODE_CLIENT) {
    gnutls_init(&cnx, GNUTLS_CLIENT);
    rc = gnutls_priority_set_direct(cnx, "PERFORMANCE", NULL);
  } else {
    /* Generate Diffie-Hellman parameters - for use with DHE
     * kx algorithms. When short bit length is used, it might
     * be wise to regenerate parameters.
     *
     * Check the ex-serv-export.c example for using static
     * parameters.
     *
     * However: to expensive.
     */
#if 0
    gnutls_dh_params_init(&s->dh_params);
    gnutls_dh_params_generate2(s->dh_params, DH_BITS);
#endif

#ifdef TLS_SESSION_CACHE
#endif
    gnutls_certificate_set_dh_params(ctx, s->dh_params);
    /* gnutls_certificate_set_rsa_export_params(ctx, rsa_params); */

    /* generate_rsa_params (); */

    gnutls_priority_init(&priority_cache, "NORMAL", NULL);
    gnutls_certificate_set_dh_params (ctx, s->dh_params);

    gnutls_init(&cnx, GNUTLS_SERVER);
    gnutls_priority_set(cnx, priority_cache);
    gnutls_credentials_set(cnx, GNUTLS_CRD_CERTIFICATE, ctx);

  }

  gnutls_credentials_set(cnx, GNUTLS_CRD_CERTIFICATE, ctx);

#endif

  if (peer_cert_mode) {
#   if defined(USE_OPENSSL)
    SSL_CTX_set_verify( ctx,
                        SSL_VERIFY_PEER |
                        (peer_cert_mode > 1 ? SSL_VERIFY_FAIL_IF_NO_PEER_CERT
                         : SSL_VERIFY_CLIENT_ONCE),
                        s->wot_mode ? verify_callback : NULL);
#   elif defined(USE_GNUTLS)
    /* request client certificate if any. */
    gnutls_certificate_server_set_request
      (cnx,
       peer_cert_mode > 1 ? GNUTLS_CERT_REQUIRE : GNUTLS_CERT_REQUEST);
#   endif
  }

#if defined(USE_OPENSSL)

  cnx = s->cnx = SSL_new( ctx );
  if (!cnx) {
    status(s, "error: SSL creation failed" );
    ssl_mgr_exit( s,  2 );
  }

  if(s->wot_mode) SSL_set_ex_data(cnx, mydata_index, s);

  rc = SSL_set_fd( cnx, fd );
  if (rc != 1) {
    ssl_choke( s, 2 );
  }

#elif defined(USE_GNUTLS)

  /* Set maximum compatibility mode. This is only suggested on public webservers
   * that need to trade security for compatibility
   */
  gnutls_session_enable_compatibility_mode(cnx);

  gnutls_transport_set_ptr (cnx, (gnutls_transport_ptr_t) fd);

#endif

  /*
   *  We're about to start talking to the peer.  Drop
   *  all capabilities.
   */
  if (jail) {
#ifdef HAVE_CAPABILITY_H
    cap_t c;
    int rc;

    rc = chdir( jail );
    if (rc < 0) {
      perror( jail );
      ssl_mgr_exit(s, 1);
    }

    if (getuid() == 0) {
      rc = chroot( jail );
      if (rc < 0) {
        perror( jail );
        ssl_mgr_exit(s, 1);
      }
    }

    c = cap_init();
    rc = cap_clear( c );
    if (rc < 0) {
      perror( "cap_clear" );
      ssl_mgr_exit(s, 1);
    }

    /* I'm not sure this does anything, since we don't have
     * any of the mentioned capabilities anyway...
     */
    rc = cap_set_proc( c );
    if (rc < 0) {
      perror( "cap_set_proc" );
      ssl_mgr_exit(s, 1);
    }
    cap_free( c );
#else
    perror( "capability support not compiled in" );
#endif
  }

#if defined(USE_OPENSSL)
  retry_restart = 5;
 restart_startup:
  if (op_mode == SMODE_SERVER) {
    if (s->verbose) {
      status( s, "note: accepting" );
    }
    rc = SSL_accept( cnx );
  } else {
    if (s->verbose) {
      status( s, "note: connecting" );
    }
    rc = SSL_connect( cnx );
  }

  if (rc <= 0) {
    int code = SSL_get_error( cnx, rc );
    if ( (rc == -1) &&
	 retry_restart-- &&
	 ( (code == SSL_ERROR_WANT_CONNECT) || (code == SSL_ERROR_WANT_ACCEPT) )
	) {
      goto restart_startup;
    } else if (code == SSL_ERROR_SYSCALL) {
      unsigned long code = ERR_get_error();
      char errbuf[200];
      if(code==0) {
        status( s, op_mode==SMODE_SERVER ? "error: accept failed" : "error: connect failed");
        ssl_choke( s, 3);
      }
      ERR_error_string_n(code, errbuf, sizeof(errbuf));
      status( s, "error: PID %d Code %x SSL_ERROR_SYSCALL proc: %s reason: %s %s", getpid(), code,
	      ERR_func_error_string(code), ERR_reason_error_string(code), errbuf );
      ssl_choke( s, 3 );
    } else {
      status( s, "error: PID %d RC %d Error Code 0x%08x", getpid(), rc, code );
      ssl_choke( s, 3 );
    }
  }
#elif defined(USE_GNUTLS)
  if (op_mode == SMODE_SERVER) {
    if (s->verbose) {
      status( s, "note: accepting" );
    }
  } else {
    if (s->verbose) {
      status( s, "note: connecting" );
    }
  }

  rc = gnutls_handshake(cnx);

  if(rc < 0) {
    status( s, "error: Error  %s", gnutls_strerror(rc));
    ssl_choke( s, 3 );
  }
#endif

  if (s->verbose) {
    if (s->framing_mode) {
      status( s, "note: ready (framing)" );
    } else {
      status( s, "note: ready (passthru)" );
    }
  }

  ssl_loop( s, cnx, fd, s->input_fd, s->output_fd );

#if defined(USE_GNUTLS)
  gnutls_certificate_free_credentials(ctx);
  gnutls_global_deinit();
#endif

  if (s->verbose) {
    status( s, "note: bye" );
  }

  ssl_mgr_exit(s, 0);
  return 1; /* never reached */
}

static void setup_mgr_state012(struct ssl_mgr_state *s)
{
  s->input_fd = 0;
  s->output_fd = 1;
  s->status_fd = 2;
}

#ifdef AS_STANDALONE
int main(int argc, char * /*const*/ *argv)
{
  struct ssl_mgr_state state;
  setup_mgr_state(&state);
  setup_mgr_state012(&state);
  return ssl_mgr_main(&state, argc, argv);
}
#endif

#if 0
static void request_client_cert( mgr_state_t tstate, SSL *cnx )
{
  int rc;

  SSL_set_verify( cnx, SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE, NULL );
  SSL_renegotiate( cnx );
  rc = SSL_do_handshake( cnx );
  if (tstate->verbose) {
    status( tstate, "note: renegotiate %d", rc );
  }
}
#endif

static unsigned read_frame(mgr_state_t tstate, int fd )
{
  u32 hdr;
  int rc;
  unsigned size, offset;

 retry:
  rc = read( fd, &hdr, sizeof(hdr));
  if (rc != sizeof(hdr)) {
    if (rc < 0) {
      switch(errno) {
      case EAGAIN:
      case EINTR:
        DEBUG_TRACE_SSL_MGR(fprintf(stderr, "error %d frame header retry on fd: %d\n", tstate->network_socket, fd);)
          goto retry;
          ;;
      default:
        DEBUG_TRACE_SSL_MGR(fprintf(stderr, "error %d frame header read on fd: %d\n", tstate->network_socket, fd);)
        status_errno( tstate, "frame header read" );
        ssl_mgr_exit(tstate, 2);
      }
    } else if (rc == 0) {
      status( tstate, "error: frame channel EOF" );
      ssl_mgr_exit(tstate, 0);
    } else {
      status( tstate, "error: frame header short read" );
      ssl_mgr_exit(tstate, 2);
    }
  }

  if (!(((hdr & TAG_MASK) == CONTROL_TAG)
        || ((hdr & TAG_MASK) == DATA_TAG))) {
    status( tstate, "illegal frame header %08lx", hdr );
    ssl_mgr_exit(tstate, 2);
  }

  size = hdr & LENGTH_MASK;
  offset = 0;

  while (offset < size) {
    rc = read( fd, tstate->buffer + offset, size - offset );
    if (rc < 0) {
      status_errno( tstate, "frame data read" );
      ssl_mgr_exit(tstate, 2);
    } else if (rc == 0) {
      status( tstate, "error: frame channel EOF (after header)" );
      ssl_mgr_exit(tstate, 2);
    } else {
      offset += rc;
    }
  }
  return hdr;
}

#if defined(USE_OPENSSL)
static void report_peer_cert(mgr_state_t tstate, SSL *cnx )
{
  /*
   *  Report the new peer certificate if it's different than
   *  what we last reported
   */
  X509 *pc = NULL;
  pc = SSL_get_peer_certificate( cnx );
  if (tstate->statused_cert != pc
      && SSL_get_verify_result( cnx ) == X509_V_OK) {
    status_cert( tstate, "peer-cert", pc );
    if(tstate->statused_cert) X509_free(tstate->statused_cert);
    tstate->statused_cert = pc;
  }
  if (pc && pc != tstate->statused_cert) {
    X509_free( pc );
  }
}
#elif defined(USE_GNUTLS)
static void report_peer_cert(mgr_state_t tstate, gnutls_session_t cnx )
{
  const gnutls_datum_t *chain;
  gnutls_x509_crt_t cert;
  unsigned int stat;
  if( tstate->statused_cert[0] != '\0' ) {
    /* status( tstate, "peer-cert %s", statused_cert->data); */
    return;
  } else if( gnutls_certificate_verify_peers2(cnx, &stat) == 0 ) {
    unsigned int n;

    /*
    if (stat & (GNUTLS_CERT_INVALID | GNUTLS_CERT_SIGNER_NOT_FOUND | GNUTLS_CERT_REVOKED)) {
      status("error: The certificate is not trusted.");
      return;
    }
    */

    /* Up to here the process is the same for X.509 certificates and
     * OpenPGP keys. From now on X.509 certificates are assumed. This can
     * be easily extended to work with openpgp keys as well.
     */
    if (gnutls_certificate_type_get (cnx) != GNUTLS_CRT_X509) return;

    if (gnutls_x509_crt_init (&cert) < 0)
    {
      status(tstate, "error: in initialization\n");
      return;
    }

    chain = gnutls_certificate_get_peers(cnx, &n);
    if (chain == NULL) return;

    if (gnutls_x509_crt_import (cert, &chain[0], GNUTLS_X509_FMT_DER) < 0)
    {
      status(tstate, "error: parsing certificate");
      return;
    }

    stat=1024;
    gnutls_x509_crt_get_dn(cert, statused_cert, &stat);
    if (stat & (GNUTLS_CERT_INVALID | GNUTLS_CERT_SIGNER_NOT_FOUND | GNUTLS_CERT_REVOKED)) {
      status(tstate, "error: The certificate %s is not trusted.", statused_cert);
      ssl_mgr_exit(tstate, 2);
    } else {
      status(tstate, "peer-cert %s", statused_cert);
    }
    gnutls_x509_crt_deinit (cert);
  }
}
#endif

# if defined(USE_OPENSSL)
void ssl_close( mgr_state_t tstate )
{
  SSL *cnx = tstate->cnx;
  int rc;

  if(tstate->cnx == NULL) {
    ssl_choke( tstate, 6 ); return;
  }

  rc = SSL_shutdown( cnx );

#if 1
  if (rc == 0) {
    /* we only sent the "close notify" alert...
       haven't received theirs yet though
    */
    rc = SSL_shutdown( cnx );
    if (rc < 0) {
      ssl_choke( tstate, 5 );
    } else if (rc == 0) {
      status( tstate, "error: incomplete shutdown" );
    } else {
      status( tstate, "clean-shutdown" );
      ssl_mgr_exit(tstate,  0 );
    }
  } else if (rc == 1) {
    status( tstate, "clean-shutdown" );
    ssl_mgr_exit( tstate,  0 );
  } else {
    ssl_choke( tstate, 5 );
  }
#endif
}
#elif defined(USE_GNUTLS)
void ssl_close(mgr_state_t tstate)
{
  if(tstate->cnx != NULL) {
    /* do not wait for the peer to close the connection. */
    gnutls_bye (tstate->cnx, GNUTLS_SHUT_WR);
    close((int) gnutls_transport_get_ptr(tstate->cnx));
    gnutls_deinit (tstate->cnx);
    tstate->cnx = NULL;
  }
}
#endif

void ssl_loop(
              mgr_state_t tstate,
#if defined(USE_OPENSSL)
	      SSL *
#elif defined(USE_GNUTLS)
	      gnutls_session_t
#endif
	      cnx, int sock, int in, int out )
{
  int num_fd;

  int restart_count = 1;

#if defined(USE_OPENSSL)
  BIO *wbio = SSL_get_wbio(cnx);

  if( !wbio ) {
    status( tstate, "error: SSL_get_bio no result");
    ssl_mgr_exit(tstate, 1);
  }
#endif

  num_fd = in;
  if (out > num_fd) {
    num_fd = out;
  }
  if (sock > num_fd) {
    num_fd = sock;
  }
  num_fd++;

  report_peer_cert( tstate, cnx );

  while (restart_count > 0) {
    char *p;
    int n, tmo = tstate->poll_timeout;
    time_t t0 = time(NULL);
#if defined(USE_POLL_SCHED)
    struct pollfd poll_fds[3] = { {in, POLLIN, 0}, {sock, POLLIN, 0}, {out, POLLHUP, 0}};
#else
    fd_set r, w;

    FD_ZERO( &r );
    FD_ZERO( &w );

    FD_SET( sock, &r );
    FD_SET( in, &r );
#endif

#if defined(USE_OPENSSL)
#   if defined(USE_POLL_SCHED)
    poll_fds[1].events = SSL_want_write( cnx ) ? POLLIN|POLLOUT : POLLIN;
#   else
    if (SSL_want_write( cnx )) {
      FD_SET( sock, &w );
    }
#   endif

#elif defined(USE_GNUTLS)
    if (gnutls_record_check_pending(cnx)) {
#     if defined(USE_POLL_SCHED)
      poll_fds[1].events = POLLIN|POLLOUT;
#     else
      FD_SET( sock, &w );
#     endif
    }

#endif

  wait_again:

#if defined(USE_POLL_SCHED)
    n = poll(poll_fds, 3, tmo);
#else
    n = select( num_fd, &r, &w, NULL, NULL ); // Note: we ignore the timeout.
#endif

    if (n == 0) {
      status( tstate, "error: timeout waiting on input");
      ssl_mgr_exit(tstate, 31);
    } else if (n < 0) {
      switch (errno) {
      case EINTR:
      case EAGAIN:
        if(tstate->poll_timeout >= 0) {
          tmo -= ((time(NULL)-t0)*1000);
          if(tmo < 0) {
            status( tstate, "error: timeout waiting on input");
            ssl_mgr_exit(tstate, 31);
          }
        }
        goto wait_again;
      default:
	status( tstate, "error: waiting for input errno %d", errno );
	ssl_mgr_exit(tstate, 31);
      }
    }

    if (tstate->verbose) {
      p = &tstate->buffer[0];
#if defined(USE_POLL_SCHED)
      if (poll_fds[1].revents & POLLIN) *p++='<';
      if (poll_fds[1].revents & POLLOUT) *p++='>';
      if (poll_fds[0].revents & POLLIN) *p++='i';
#else
      if (FD_ISSET( sock, &r )) {
        *p++ = '<';
      }
      if (FD_ISSET( sock, &w )) {
        *p++ = '>';
      }
      if (FD_ISSET( in, &r )) {
        *p++ = 'i';
      }
      if (FD_ISSET( out, &r )) {
        *p++ = 'o';
      }
#endif
      *p = 0;
      status( tstate, "note: select returns %d (%s)", n, tstate->buffer );
    }

    if (
#if defined(USE_POLL_SCHED)
	poll_fds[1].revents & POLLIN
#else
	FD_ISSET( sock, &r )
#endif
	) {
#     if defined(USE_OPENSSL)
      n = SSL_read( cnx, tstate->buffer, LENGTH_MASK );
#     elif defined(USE_GNUTLS)
      n = gnutls_record_recv (cnx, tstate->buffer, LENGTH_MASK);
#     endif
      if (n > 0) {

        if (tstate->verbose) {
          status( tstate, "note: read chunk %d", n );
        }

	SETALIVE

        report_peer_cert( tstate, cnx );

        if (tstate->framing_mode) {
          u32 hdr = DATA_TAG | n;
          write( out, &hdr, sizeof(hdr) );
          write( out, tstate->buffer, n );
        } else {
          write( out, tstate->buffer, n );
        }
      } else if (n == 0) {
#       if defined(USE_OPENSSL)
        if (SSL_get_shutdown( cnx ) & SSL_RECEIVED_SHUTDOWN) {
          /* status( tstate, "clean-shutdown" ); */
          ssl_mgr_exit(tstate,  0 );
        } else {
          int code = SSL_get_error( cnx, n );
          status( tstate, "error: read shutdown error (%d)", code );
          ssl_choke( tstate, 4 );
        }
#       elif defined(USE_GNUTLS)
        gnutls_deinit(cnx);
        tstate->cnx = NULL;
	ssl_mgr_exit(tstate, 0);
#       endif
      } else {
        status( tstate, "error: read error %d", n );
        ssl_choke( tstate, 4 );
      }
    }

#   if defined(USE_POLL_SCHED)
    if ( (poll_fds[0].revents & (POLLERR | POLLNVAL)) ||
	 (poll_fds[1].revents & (POLLERR | POLLNVAL))
	 || (poll_fds[2].revents & (POLLHUP | POLLERR | POLLNVAL))
	 ) {
      status(tstate, "error: EOF");
      ssl_mgr_exit(tstate, 0);
    }
#   endif

    if (
#if defined(USE_POLL_SCHED)
	poll_fds[0].revents & POLLIN
#else
	FD_ISSET( in, &r )
#endif
	) {
      int write_rc = 0;
      unsigned write_n = 0;

      if (tstate->framing_mode) {
        u32 hdr;

        DEBUG_TRACE_SSL_MGR(fprintf(stderr, "%d poll read frame from %d %d\n", tstate->network_socket, poll_fds[0].fd, poll_fds[0].revents & POLLIN);)
        hdr = read_frame( tstate, in );
        if ((hdr & TAG_MASK) == CONTROL_TAG) {
          unsigned i;

          for (i=0; i<(hdr & LENGTH_MASK); i++) {
            unsigned char ch = tstate->buffer[i];

            switch (ch) {
            case CTL_SHUTDOWN:
              ssl_close( tstate );
	      ssl_mgr_exit(tstate, 0);
              break;
            default:
              status( tstate, "error: illegal control code 0x%02x", ch );
              ssl_mgr_exit(tstate, 32);
            }
          }
        } else {
          assert( (hdr & TAG_MASK) == DATA_TAG );
          write_n = hdr & LENGTH_MASK;
#         if defined(USE_OPENSSL)
          write_rc = SSL_write( cnx, tstate->buffer, write_n );
#         elif defined(USE_GNUTLS)
	  write_rc = gnutls_record_send(cnx, tstate->buffer, write_n);
#         endif
        }
      } else {
        n = read( in, tstate->buffer, sizeof(tstate->buffer) );
        if (n > 0) {
          write_n = n;
#         if defined(USE_OPENSSL)
          write_rc = SSL_write( cnx, tstate->buffer, write_n );
#         elif defined(USE_GNUTLS)
	  write_rc = gnutls_record_send(cnx, tstate->buffer, write_n);
#         endif
        } else if (n == 0) {
          if (tstate->verbose) {
            status( tstate, "note: EOF local" );
          }
          ssl_mgr_exit(tstate, 0);
        }
      }

      if (write_rc < 0
#         if defined(USE_GNUTLS)
          && gnutls_error_is_fatal(write_rc)
#         endif
	  ) {
#       if defined(USE_OPENSSL)
        int code = SSL_get_error( cnx, write_rc );
#       elif defined(USE_GNUTLS)
        int code = write_rc;
#       endif
        status( tstate, "error: read shutdown error (%d)", code );
        ssl_choke( tstate, 4 );
      }

      if (write_rc != write_n) {
        status( tstate, "error: only wrote %d of %u", write_rc, write_n );
      }

#     if defined(USE_OPENSSL)
      if( !BIO_flush(wbio) ) {
	if(!BIO_should_retry(wbio)) {
	  status( tstate, "error: BIO_flush failed" );
	}
      }
#     endif

      SETALIVE;

    }

  }
}

#ifdef AS_EMBEDDED
/* source: https://raw.githubusercontent.com/PX4/Firmware/master/src/platforms/common/px4_getopt.c */
/* --- %< --- --- %< --- %< --- literal included px4_optarg.c  --- %< --- %< --- */
/****************************************************************************
 *
 * Copyright (c) 2015 Mark Charlebois. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file px4_getopt.cpp
 * Minimal, thread safe version of getopt
 */

// #include <px4_getopt.h>
// #include <stdio.h>

// check if p is a valid option and if the option takes an arg
static char isvalidopt(char p, const char *options, int *takesarg)
{
	int idx = 0;
	*takesarg = 0;

	while (options[idx] != 0 && p != options[idx]) {
		++idx;
	}

	if (options[idx] == 0) {
		return '?';
	}

	if (options[idx + 1] == ':') {
		*takesarg = 1;
	}

	return options[idx];
}

// reorder argv and put non-options at the end
static int reorder(int argc, char **argv, const char *options)
{
	char *tmp_argv[argc];
	char c;
	int idx = 1;
	int tmpidx = 1;
	int takesarg;

	// move the options to the front
	while (idx < argc && argv[idx] != 0) {
		if (argv[idx][0] == '-') {
			c = isvalidopt(argv[idx][1], options, &takesarg);

			if (c == '?') {
				return 1;
			}

			tmp_argv[tmpidx] = argv[idx];
			tmpidx++;

			if (takesarg) {
				if (idx + 1 >= argc) { //Error: option takes an argument, but there is no more argument
					return 1;
				}

				tmp_argv[tmpidx] = argv[idx + 1];
				// printf("tmp_argv[%d] = %s\n", tmpidx, tmp_argv[tmpidx]);
				tmpidx++;
				idx++;
			}
		}

		idx++;
	}

	// Add non-options to the end
	idx = 1;

	while (idx < argc && argv[idx] != 0) {
		if (argv[idx][0] == '-') {
			c = isvalidopt(argv[idx][1], options, &takesarg);

			if (c == '?') {
				return c;
			}

			if (takesarg) {
				idx++;
			}

		} else {
			tmp_argv[tmpidx] = argv[idx];
			tmpidx++;
		}

		idx++;
	}

	// Reorder argv
	for (idx = 1; idx < argc; idx++) {
		argv[idx] = tmp_argv[idx];
	}

	return 0;
}

//
// px4_getopt
//
// returns:
//            the valid option character
//            '?' if any option is unknown
//            -1 if no remaining options
//
// If the option takes an arg, myoptarg will be updated accordingly.
// After each call to px4_getopt, myoptind in incremented to the next
// unparsed arg index.
// Argv is changed to put all options and option args at the beginning,
// followed by non-options.
//
__EXPORT int px4_getopt(int argc, char *argv[], const char *options, int *myoptind, const char **myoptarg)
{
	char *p;
	char c;
	int takesarg;

	if (*myoptind == 1) {
		if (reorder(argc, argv, options) != 0) {
			*myoptind += 1;
			return (int)'?';
		}
	}

	p = argv[*myoptind];

	if (*myoptarg == 0) {
		*myoptarg = argv[*myoptind];
	}

	if (p && options && myoptind && p[0] == '-') {
		c = isvalidopt(p[1], options, &takesarg);

		if (c == '?') {
			*myoptind += 1;
			return (int)c;
		}

		*myoptind += 1;

		if (takesarg) {
			*myoptarg = argv[*myoptind];

			if (!*myoptarg) { //Error: option takes an argument, but there is no more argument
				return -1;
			}

			*myoptind += 1;
		}

		return (int)c;
	}

	return -1;
}
#endif
