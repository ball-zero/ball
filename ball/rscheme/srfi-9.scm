;; (C) 2003, 2006 J�rg F. Wittenberger see http://www.askemos.org

(define-class <record> (<object>))

(define-method write-object ((self <record>) port)
  (format port "#[a ~s]" (class-name (object-class self))))

(define-macro (define-record-type type-name 
                (constructor-name . field-tags)
                predicate-name
                . field-specs)
  (define (fs-name fs) (car fs))
  (define (fs-accessor fs) (cadr fs))
  (define (fs-modifier fs) (and (pair? (cddr fs)) (caddr fs)))
  (define (field-spec fs)
    (list (fs-name fs) getter: (fs-accessor fs) setter: (fs-modifier fs)))
  ;
  `(begin
     (define-class ,type-name (<record>)
       . ,(map field-spec field-specs))
     ;
     (define (,constructor-name ,@field-tags)
       (make ,type-name
         . ,(let loop ((field-tags field-tags))
              (if (pair? field-tags)
                  `(,(string->symbol
                      (string-append (symbol->string (car field-tags)) ":"))
                    ,(car field-tags)
                    . ,(loop (cdr field-tags)))
                  '()))))
                                        ;
     (define (,predicate-name item)
       (instance? item ,type-name))

     (define-method print ((self ,type-name))
       (format #t "~s\n" self)
       . ,(map (lambda (fs)
                 `(format #t "   ~s = ~#*@60s\n" 
                          ',(fs-name fs) 
                          (,(fs-accessor fs) self)))
               field-specs))
     ))

(define-macro (define-atomic-type type-name 
                (constructor-name . field-tags)
		define-update-name
                predicate-name
                . field-specs)
  (define (fs-name fs) (car fs))
  (define (fs-accessor fs) (cadr fs))
  (define (fs-mutable fs) (and (pair? (cddr fs))
			       (eq? (caddr fs) :mutable)))
  (define (field-spec fs)
    (list (fs-name fs) getter: (fs-accessor fs) setter: #f))
  (define object (gensym))
  (define outer-object (gensym))
  (define get-proc (gensym))
  (define exch-proc (gensym))
  (define proc (gensym))
					;
  `(begin
     (define-class ,type-name (<record>)
       . ,(map field-spec field-specs))
					;
     (define (,constructor-name ,@field-tags)
       (make ,type-name
         . ,(let loop ((ft field-tags))
              (if (pair? ft)
                  `(,(string->symbol
                      (string-append (symbol->string (car ft)) ":"))
                    ,(car ft)
                    . ,(loop (cdr ft)))
                  (let loop ((field-specs field-specs))
		    (cond
		     ((null? field-specs) '())
		     ((memq (fs-name (car field-specs)) field-tags)
		      (loop (cdr field-specs)))
		     (else `(,(string->symbol
			       (string-append
				(symbol->string (fs-name (car field-specs))) ":"))
			     #f
			     . ,(loop (cdr field-specs))))))))))

     (define-macro (,define-update-name update-method
		     input-fields update-fields arguments results
		     . body)
       (let* ((updater (gensym))
	      (update-method2 (symbol-append update-method '!))
	      (typename ',type-name)
	      (object ',object)
	      (outer-object ',outer-object)
	      (get-proc ',get-proc)
	      (exch-proc ',exch-proc)
	      (getterlist (let loop ((field-specs ',field-specs))
			    (if (null? field-specs) '()
				(if (memq (car (car field-specs))
					  input-fields)
				    (cons `(,(cadr (car field-specs)) ,object)
					  (loop (cdr field-specs)))
				    (loop (cdr field-specs))))))
	      (makelist (let loop ((field-specs ',field-specs))
			  (if (null? field-specs) '()
			      (let ((name (car (car field-specs))))
				`(,(string->symbol
				    (string-append (symbol->string name) ":"))
				  ,(if (memq name update-fields)
				       name
				       `(,(cadr (car field-specs)) ,object))
				  . ,(loop (cdr field-specs))))))))
	 `(begin
	    (define ,update-method
	      (let ((,updater (lambda (,@input-fields ,@arguments) . ,body)) )
		(define (,update-method ,object ,@arguments)
		  (call-with-values
		      (lambda () (,updater ,@getterlist ,@arguments))
		    (lambda (,@results ,@update-fields)
		      (values
		       ,@results
		       (make ,typename . ,makelist)))))
		,update-method))
	    (define ,update-method2
	      (let ((,updater (lambda (,@input-fields ,@arguments) . ,body)) )
		(define (,update-method2 ,outer-object ,get-proc ,exch-proc ,@arguments)
		  (define ,object (,get-proc ,outer-object))
		  (call-with-values
		      (lambda () (,updater ,@getterlist ,@arguments))
		    (lambda (,@results ,@update-fields)
		      (let ((result (make ,typename . ,makelist)))
			(if (eq? (,exch-proc ,outer-object ,object result) result)
			    (values ,@results result)
			    (begin
			      ;; (format (current-error-port) "~a restart ~a\n"(current-thread) ,update-method2)
			      (,update-method2 ,outer-object ,get-proc ,exch-proc ,@arguments)))))))
		,update-method2)))))

     (define (,predicate-name item)
       (instance? item ,type-name))

     (define-method print ((self ,type-name))
       (format #t "~s\n" self)
       . ,(map (lambda (fs)
                 `(format #t "   ~s = ~#*@60s\n" 
                          ',(fs-name fs) 
                          (,(fs-accessor fs) self)))
               field-specs))
     ))

; (define-macro (define-record-type type-name 
;                 (constructor-name . field-tags)
;                 predicate-name
;                 . field-specs)
;   (define (fs-name fs) (car fs))
;   (define (fs-accessor fs) (cadr fs))
;   (define (fs-modifier fs) (and (pair? (cddr fs)) (caddr fs)))
;   (define (std-modifier fs)
;     (string->symbol (string-append "set-" (symbol->string (fs-name fs)) "!")))
;   ;
;   `(begin
;      (define-class ,type-name (<record>)
;        . ,(map fs-name field-specs))
;      ;
;      (define (,constructor-name ,@field-tags)
;        (make ,type-name
;          . ,(let loop ((field-tags field-tags))
;               (if (pair? field-tags)
;                   `(,(string->symbol
;                       (string-append (symbol->string (car field-tags)) ":"))
;                     ,(car field-tags)
;                     . ,(loop (cdr field-tags)))
;                   '()))))
;                                         ;
;      (define (,predicate-name item)
;        (instance? item ,type-name))
;                                         ;
;      ,@(map (lambda (fs)
;               (if (not (equal? (fs-accessor fs) (fs-name fs)))
;                   `(define (,(fs-accessor fs) (self ,type-name))
;                      (,(fs-name fs) self))
;                   '(begin)))
;             field-specs)
;      ,@(map (lambda (fs)
;               (if (and (fs-modifier fs)
;                        (not (equal? (std-modifier fs) (fs-modifier fs))))
;                   `(define (,(fs-modifier fs) (self ,type-name) value)
;                      (,(std-modifier fs) self value)
;                      (values))
;                   '(begin)))
;             field-specs)

;      (define-method print ((self ,type-name))
;        (format #t "~s\n" self)
;        . ,(map (lambda (fs)
;                  `(format #t "   ~s = ~#*@60s\n" 
;                           ',(fs-name fs) 
;                           (,(fs-accessor fs) self)))
;                field-specs))
;      ))