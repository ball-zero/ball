/* (C) 2003 J�rg F. Wittenberger <Joerg.Wittenberger@pobox.com>, GPL */

/* Start/restart Askemos kernel daemon, also a (supposed to be)
 * portable and "minimal fuss" way to set user and group id.
 *
 * The AskemosServer as of today can need some restarts.  This is more
 * an unfortunate truth rather than design.  It is restarted from this
 * script, if it exitcode is anything but zero.
 */

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>                /* exists but not needed on BSD */
#include <time.h>
#include <signal.h>
#include <string.h>

#define CHECK_PERIOD 10
#define STARTUP_DELAY (CHECK_PERIOD + 9)
#define RESTART_DELAY 3
#define RESTART_RETRY 3
#define USE_FORCED_RESTART 1

static int help(const char *const name) {
  fprintf(stderr, "%.256s user name library-path kernel-options...\n", name);
  fprintf(stderr, "  where\n");
  fprintf(stderr, "     user           : the name of the user to run as\n");
  fprintf(stderr, "     name           : the executable\n");
  fprintf(stderr, "     library-path   : path to <name>.bin and <name>.fas\n");
  fprintf(stderr, "     kernel-options : other option fed to kernel\n");
  exit(EXIT_FAILURE);
  return EXIT_FAILURE;
}

void *xmalloc(size_t s)
{
  void *result=malloc(s);
  if(result) return result;
  else {
    fprintf(stderr,
            "memory problem, malloc returned NULL allocating %d bytes\n",
           (int) s);
    exit(EXIT_FAILURE);
    return NULL;
  }
}

static pid_t the_kernel = 0; 

static void sigterm_handler( int x )	/* signal handler */
{
#ifdef DEBUG_SIGNALS
  psignal(x, "heartbeat.c sigterm_handler() invocation on");
#endif
  kill(the_kernel, SIGTERM);
  exit(0);
}

static void sighup_handler( int x )	/* signal handler */
{
#ifdef DEBUG_SIGNALS
  psignal(x, "heartbeat.c sighup_handler() invocation on");
  psignal(SIGUSR1, "heartbeat.c sighup_handler() delegate");
#endif
  kill(the_kernel, SIGUSR1);
}

/*
 * Simple dead/locked/runaway process signal.  It's the kernels
 * watchdog responsibility to send a SIGUSR1 every second.  If it
 * misses two, we kill it.
 *
 * FIXME BSD has a different idea of setting permissions.  We replace
 * SIGUSR1 with SIGCONT for the time until everything has been worked
 * out.
 */

static int the_dead_flag = 1;
#ifdef USE_FORCED_RESTART
static int the_kill_counter = 0;
static void count_kill()
{
  if(the_kill_counter++ > 100) {
    int err;
    char *argv[5];
    argv[0] = "/usr/bin/sudo";
    argv[1] = "-n";
    argv[2] = "/sbin/reboot";
    argv[3] = "-f";
    argv[4]=NULL;
    fprintf(stderr, "Trying reboot\n");
    err = execv(argv[0], (char **const)argv);
    fprintf(stderr, "sudo reboot returned %d\n", err);
  }
}
#endif

static void sigalarm_handler( int x )     /* signal handler */
{
  if(the_dead_flag == 1) {
    fprintf(stderr, "Kernel did not send alive signal, signaling it.\n");
    kill(the_kernel, SIGUSR2);
    the_dead_flag = 2;
    alarm(STARTUP_DELAY);
  } else if(the_dead_flag == 2) {
    fprintf(stderr, "Kernel did not send alive signal, killing it.\n");
    kill(the_kernel, SIGKILL);
#ifdef USE_FORCED_RESTART
    count_kill();
#endif
    alarm(STARTUP_DELAY);
  } else {
    the_dead_flag = 1;
    alarm(CHECK_PERIOD);
  }
}

static void sigusr1_handler( int x )     /* signal handler */
{
#ifdef DEBUG_SIGNALS
  psignal(x, "heartbeat.c sigusr1_handler() invocation on");
#endif
  the_dead_flag = 0;
#ifdef USE_FORCED_RESTART
  the_kill_counter = 0;
#endif
}

#define USER argv[1]
#define NAME argv[2]
#define LIBR argv[3]
#define BIN  ".bin"
#define BIN_ACCESS X_OK|R_OK
#define FAS  ".fas"
#define FAS_ACCESS R_OK
#define NEW  ".new"

int main( int argc, const char **argv )
{
  char *nshell=NULL, *nheap=NULL, *oshell=NULL, *oheap=NULL,
    *shell=NULL, *heap=NULL;
  int status=0;
  int run = 1, retry = RESTART_RETRY;
  struct passwd *user;

  if( argc < 4 ) return help(argv[0]);

  if( ! (user = getpwnam (USER)) ) {
    fprintf(stderr, "can't find user %s.\n", USER);
    exit(EXIT_FAILURE);
  }

  if( ! geteuid() ) {
    if( initgroups(NAME, user->pw_gid) ) {
      fprintf(stderr, "could not initialize groups for \"%s\", error %d\n",
              USER, errno);
      exit(EXIT_FAILURE);
    }

    if( setgid(user->pw_gid) ) {
      fprintf(stderr, "could not set group id %d for \"%s\", error %d.",
              user->pw_gid, USER, errno);
      exit(EXIT_FAILURE);
    }
    if( setuid(user->pw_uid) ) {
      fprintf(stderr, "could not set user id %d for \"%s\", error %d.",
              user->pw_uid, USER, errno);
      exit(EXIT_FAILURE);
    }
  }

  nshell=xmalloc(1+strlen(LIBR)+strlen(NAME)+strlen(BIN)+strlen(NEW));
  strcpy(nshell, LIBR); strcat(nshell, NAME);
  strcat(nshell, BIN); strcat(nshell, NEW);

  nheap=xmalloc(1+strlen(LIBR)+strlen(NAME)+strlen(FAS)+strlen(NEW));
  strcpy(nheap, LIBR); strcat(nheap, NAME);
  strcat(nheap, FAS); strcat(nheap, NEW);

  oshell=xmalloc(1+strlen(LIBR)+strlen(NAME)+strlen(BIN));
  strcpy(oshell, LIBR); strcat(oshell, NAME); strcat(oshell, BIN);

  oheap=xmalloc(1+strlen(LIBR)+strlen(NAME)+strlen(FAS));
  strcpy(oheap, LIBR); strcat(oheap, NAME); strcat(oheap, FAS);

  signal(SIGHUP, sighup_handler); /* restart kernel, reload config */

  signal(SIGTERM, sigterm_handler); /* terminate */
  signal(SIGINT, sigterm_handler);
  signal(SIGQUIT, sigterm_handler);
  signal(SIGABRT, sigterm_handler);

  signal(SIGALRM, sigalarm_handler); /* timer */

  /* signal(SIGUSR1, sigusr1_handler); */
  signal(SIGCONT, sigusr1_handler); /* kernel alive */

  while( run ) {
    time_t start = time(NULL);
    the_kernel = fork();
    if( the_kernel ) {
      alarm(STARTUP_DELAY);
      while( run ) {
        pid_t p = waitpid(the_kernel, &status, 0);
        if( p == the_kernel ) {
          time_t ran = time(NULL) - start;
          if WIFSIGNALED(status) {
	    psignal(WTERMSIG(status), "heartbeat.c Kernel exit on" );
	  }
          run = ! (WIFEXITED(status) && WEXITSTATUS(status) == 0) ;
          /* clock adjument could cause negative differences here */
          if( ran >= 0 ) {
	    if( ran < STARTUP_DELAY) {
	      fprintf(stderr, "Kernel ran just %ld seconds.\nPerhaps something is broken?\n", (long int) ran);
	      if( ! --retry ) {
		fprintf(stderr, "Exiting now.\n");
		exit(EXIT_FAILURE);
	      }
	    } else {
	      retry = RESTART_RETRY;
	    }
	  }
	  if( run ) sleep(RESTART_DELAY);
          break;
        } else if( p == -1 ) {
	  switch ( errno ) {
	  case ECHILD:
	    fprintf(stderr, "Kernel process %d exited.\n", the_kernel);
	    break;
	  case EINTR: break;
	  default:
	    fprintf(stderr, "Kernel waitpit(3) error: %s\n", strerror(errno));
	    exit(EXIT_FAILURE);
	  }
        }
      }
    } else {

      if( setpgid(0,0) ) {
        fprintf(stderr, "Failed to set new process group for kernel.\n");
        exit(0);
      }

      /* First look for a ".new" version.  Not sure that this is a
         good way.  It should actually be better to unlink/recreate
         those files.  But his way we could even switch back in a
         second. */
      if( !access( nshell, BIN_ACCESS ) ) {
        shell = nshell;
        heap = nheap;
      } else {
        if( access(oshell, BIN_ACCESS) ) {
          fprintf(stderr, "%s : can't execute %s or %s\n", argv[0],
                  oshell, nshell);
          exit(EXIT_FAILURE);
        } else {
          shell = oshell;
          heap = oheap;
        }
      }

#ifdef TEST_FAS_ACCESS
      if( access( heap, FAS_ACCESS )  ) {
        fprintf(stderr, "%s : can't load heap %s %s\n", argv[0], heap,
                strerror(errno));
        exit(EXIT_FAILURE);
      }
#endif

      argv += 1;
      argv[0] = shell;
      argv[1] = "-qimage";
      argv[2] = heap;

      execv(shell, ((char **const) argv));
      exit(errno);
    }
  }

  return WEXITSTATUS(status);
}
