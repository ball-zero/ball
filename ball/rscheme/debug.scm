(define (BUS-test-state)
  (SIGBUS-send)
  (call-with-values SIGBUS-state
    (lambda (envt literal dynamic continuation thread)
      ;;(display (class-name (object-class envt)))         ; closure
      ;;(display (class-name (object-class literal)))      ; template
      ;;(display (class-name (object-class dynamic)))      ; ()
      ;;(display continuation)
      (display (class-name (object-class thread)))
      (display thread)
      )))

;(display (format "envt: ~a \nliteral: ~a\n"  ))

(define-glue (SIGBUS-state)
{ extern obj SIGBUS_state[5];

  REG0 = SIGBUS_state[0];
  REG1 = SIGBUS_state[1];
  REG2 = SIGBUS_state[2];
  REG3 = SIGBUS_state[3];
  REG4 = SIGBUS_state[4];
  RETURN(5);
})

(define-glue (SIGBUS-envt_reg)
{ extern obj SIGBUS_state[5];

  REG0 = SIGBUS_state[0];
  RETURN1();
})

(define-glue (SIGBUS-literals_reg)
{ extern obj SIGBUS_state[5];

  REG0 = SIGBUS_state[1];
  RETURN1();
})

(define-glue (SIGBUS-dynamic_state_reg)
{ extern obj SIGBUS_state[5];

  REG0 = SIGBUS_state[2];
  RETURN1();
})

(define-glue (SIGBUS-continuation_reg)
{ extern obj SIGBUS_state[5];

  REG0 = SIGBUS_state[3];
  RETURN1();
})

(define-glue (SIGBUS-thread_state_reg)
{ extern obj SIGBUS_state[5];

  REG0 = SIGBUS_state[4];
  RETURN1();
})

(define-glue (SIGBUS-send)
  properties: ((other-h-files "<signal.h>"))

{
  REG0=int2fx( raise(SIGBUS));
  RETURN1();
})


;;-------------------------------------------------

(define-safe-glue (set-SIGBUS-handler! (handler <closure>))
  
{ extern obj SIGBUS_handler;

  SIGBUS_handler = handler;
  RETURN0();
})

(define-safe-glue (reset-SIGBUS-handler!)
  
{ extern obj SIGBUS_handler;

  SIGBUS_handler = FALSE_OBJ;
  RETURN0();
})

(define-safe-glue (enable-SIGBUS-handler)
  properties: ((other-c-files "debug_glue.c"))

{ void trace_bus_error( int x );
  extern obj SIGBUS_handler;

  SIGBUS_handler=FALSE_OBJ;
  os_register_signal_handler( SIGBUS, trace_bus_error);
  RETURN0();
})

(cond-expand
 (init-sigbus
  (enable-SIGBUS-handler)
  (set-SIGBUS-handler!
   (lambda ()
     (logerr "\n~a\nrestart on SIGBUS!\n"
	     (with-output-to-string (lambda () (thread-list))))
     (exit 1)))))
