;; (C) 2004 J�rg F. Wittenberger see http://www.askemos.org

(define-class <utf8-error> (<condition>)
  message
  offset)

(define-method display-object ((self <utf8-error>) port)
  (format port "utf8 error ~a ~d\n" (message self) (offset self)))

;; Seek the byte offset for the string index 'pos'.  The 'start' and
;; 'index' parameters initialise the counter.  If there's no knowledge
;; about the offset/index mappings, one passes 0,0 here, otherwise the
;; utf8-seek result and 'pos' parameter of a previous call.

(define-safe-glue (utf8-seek (str <string>)
                             (start <raw-int>) (index <raw-int>)
                             (pos <raw-int>))
  literals: ((& <utf8-error>))
{
  unsigned char *s=string_text(str);
  unsigned char *scan=s+start;
  unsigned char *limit=s+string_length(str);
  if( index < pos) {
    while (index < pos) {
     if( scan >= limit ) {
      raise_error( make3( TLREF(0), NIL_OBJ, make_string("index out of bounds"), int2fx(pos) ) );
     }
     ++index;
     if (*scan < 0x80) scan++;
     else if (*scan < 0xE0) scan+=2;
     else if (*scan < 0xF0) scan+=3;
     else if (*scan < 0xF8) scan+=4;
     else if (*scan < 0xFC) scan+=5;
     else if (*scan < 0xFE) scan+=6;
     else raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad string"), int2fx(scan-s) ) );
    }
    REG0=int2fx(scan-s);
  } else if(index > pos) {
     int size=0;
     while( index > pos ) {
       if( s > limit ) {
        raise_error( make3( TLREF(0), NIL_OBJ, make_string("index out of bounds"), int2fx(pos) ) );
       }
     do {
       size++;
       limit--;
       if( s > limit || size > 6 ) {
        raise_error( make3( TLREF(0), NIL_OBJ, make_string("invalid string"), int2fx(pos) ) );
       }
     } while((*limit >= 0x80) && (*limit < 0xC0));
     index--;
    }
    REG0=int2fx(limit-s);
  } else {
    REG0=int2fx(start);
  }
    RETURN1();
})

;; Tell the string index of a byte offset into an utf8 encoded string.
;; Reverse to utf8-seek.

(define-safe-glue (utf8-tell (str <string>)
                             (start <raw-int>) (index <raw-int>)
                             (pos <raw-int>))
  literals: ((& <utf8-error>))
{
  unsigned char *s=string_text(str);
  unsigned char *scan=s+start;
  unsigned char *limit=scan+string_length(str);
  if( s+pos > limit ) {
   raise_error( make3( TLREF(0), NIL_OBJ, make_string("index out of bounds"), int2fx(pos) ) );
  } else {
   limit = s+pos;
  }
  while ( scan < limit ) {
   ++index;
   if (*scan < 0x80) scan++;
   else if (*scan < 0xE0) scan+=2;
   else if (*scan < 0xF0) scan+=3;
   else if (*scan < 0xF8) scan+=4;
   else if (*scan < 0xFC) scan+=5;
   else if (*scan < 0xFE) scan+=6;
   else raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad string"), int2fx(scan-s) ) );
  }
  REG0=int2fx(index);
  RETURN1();
})

(define (utf8-string-length s) (utf8-tell s 0 0 (string-length s)))

;; Return the character at byte offset 'start' from the source string
;; (e.g., of a string port) and it's length.

(define-safe-glue (utf8-string-getc (str <string>) (start <raw-int>))
  literals: ((& <utf8-error>))
{
  unsigned char *s=string_text(str);
  unsigned char *scan=s+start;
  unsigned char *limit=s+string_length(str);
  unsigned int i, size=1, ch;
  if (*scan < 0x80) ch=*scan;
  else if (*scan < 0xE0) {size=2; ch=*scan & 0x1F;}
  else if (*scan < 0xF0) {size=3; ch=*scan & 0x0F;}
  else if (*scan < 0xF8) {size=4; ch=*scan & 0x07;}
  else if (*scan < 0xFC) {size=5; ch=*scan & 0x3;}     
  else if (*scan < 0xFE) {size=6; ch=*scan & 0x1;}
  else ch=0, raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad character size"), int2fx(scan-s) ) );

  if( scan++ + size > limit )
    raise_error( make3( TLREF(0), NIL_OBJ, make_string("short character"), int2fx(scan-s) ) );
  for(i=size-1; i ;--i) {
    if ((*scan<0x80) || (*scan >= 0xC0))
      raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad byte"), int2fx(scan-s) ) );
    else { ch=(ch<<6) | (*scan++ & 0x3F); }
  }

  REG0 = size==1 ? MAKE_ASCII_CHAR(ch) : MAKE_UNICODE_CHAR(ch);
  REG1 = int2fx(size);
  RETURN(2);
})

(define (utf8-string-ref (str <string>) (index <integer>))
  (call-with-values (lambda ()
                      (utf8-string-getc str (utf8-seek str 0 0 index)))
    (lambda (result size) result)))

(define (utf8-substring str from to)
  (let ((start-offset (utf8-seek str 0 0 from)))
    (substring str start-offset (utf8-seek str start-offset from to))))

;; string ports

(define-class <utf8-string-input-port> (<string-input-port>))

(define-method input-port-read-char ((self <utf8-string-input-port>))
  (let (((contents <string>) (buffered-input-buffer self))
	((i <fixnum>) (buffered-input-posn self)))
    (if (fixnum<? i (string-length contents))
	(receive (ch size) (utf8-string-getc contents i)
                 (set-buffered-input-posn! self (fixnum+ i size))
                 (if (eq? ch #\newline)
                     (increment-line self))
                 ch)
	(let ((more (provide-more-input self)))
	  (if (string? more)
	      (begin
		(set-buffered-input-buffer! self more)
		(set-buffered-input-posn! self 0)
		(input-port-read-char self))
	      $eof-object)))))

(define-method input-port-peek-char ((self <utf8-string-input-port>))
  (let (((contents <string>) (buffered-input-buffer self))
	((i <fixnum>) (buffered-input-posn self)))
    (if (fixnum<? i (string-length contents))
	(receive (ch size) (utf8-string-getc contents i) ch)
	(integer->ascii-char (bvec-ref contents i))
	(let ((more (provide-more-input self)))
	  (if (string? more)
	      (begin
		(set-buffered-input-buffer! self more)
		(set-buffered-input-posn! self 0)
		(input-port-peek-char self))
	      $eof-object)))))

(define (open-utf8-input-string (source <string>))
  (make <utf8-string-input-port>
	input-port-line-number: 1
	buffered-input-buffer: source))

(define (call-with-utf8-input-string str proc)
  (proc (open-utf8-input-string str)))

(define-class <utf8-string-output-port> (<string-output-port>))

(define-method output-port-write-char ((self <utf8-string-output-port>)
                                       (ch <ascii-char>))
  (let (((i <fixnum>) (get-immob-value ch)))
    (if (< i #x0080)
        (string-output-port-write-char self ch)
        (string-output-port-write-string self (integer->utf8string i)))))

(define (open-utf8-output-string)
  (make <utf8-string-output-port>
	current-buffer: (bvec-alloc <byte-vector> 100)
	current-buffer-index: 0
	buffer-overflows: '()))

(define (call-with-utf8-output-string proc)
  (let ((port (open-utf8-output-string)))
    (proc port)
    (close-output-port port)))

;; integer encoding

(define-safe-glue (integer->utf8string (ch <raw-int>))
{
  static unsigned char off[6]={0xFC,0xF8,0xF0,0xE0,0xC0,0x00};
  int size=5; unsigned char buf[7];
  buf[6]='\0';
  if (ch < 0x80) {
    buf[5]=ch;
  } else {
    buf[size--]=(ch&0x3F)|0x80; ch=ch>>6;
    while (ch) { buf[size--]=(ch&0x3F)|0x80; ch=ch>>6; }
    /* Write the size information into the first byte */
    ++size;
    buf[size]=off[size]|buf[size];
  }
  REG0 = bvec_alloc(7-size, string_class );
  memcpy( PTR_TO_DATAPTR(REG0), buf+size, 7-size );
  RETURN1();
})

;; XML normalisation

(define any-but-eol-rs-regex
  (reg-expr->proc `(prefix (+ (not (or #\cr #\newline))))))

(define end-of-line-rs-regex
  (reg-expr->proc
   `(prefix (or (seq #\cr #\newline) (seq #\newline #\cr) #\newline #\cr))))

(define (normalized-length str str-len el)
  (let loop ((i 0) (l 0))
    (if (eqv? i str-len)
        l
        (receive* (s e) (any-but-eol-rs-regex str i)
                  (if s
                      (loop (min e str-len) (fx+ l (fx- (min e str-len) s)))
                      (receive* (s e) (end-of-line-rs-regex str i)
                                (loop e (fx+ el l))))))))

(define (normalize-input str result nl eol)
  (let loop ((i 0) (j 0))
    (if (eqv? j nl)
        result
        (receive* (s e) (end-of-line-rs-regex str i)
                  (if s
                      (let lp ((j j) (p 0))
                        (if (eqv? p (string-length eol))
                            (loop e j)
                            (begin (string-set! result j (string-ref eol p))
                                   (lp (add1 j) (add1 p)))))
                      (begin
                        (string-set! result j (string-ref str i))
                        (loop (add1 i) (add1 j))))))))

(define-macro (iconv-buflen) 30)

;; This code is heavily derived from Donovan's iconv code.  I'm
;; specialising it here towards XML data normalisation for Askemos.
;; This seems only possible, when I accept some confusing code.

(define-method write-bytes-a ((self <output-port>) bvec offset len)
  (do ((i 0 (add1 i))
       (j offset (add1 j)))
      ((eqv? i len) i)
    (output-port-write-char self (string-ref bvec i))))

(define-method write-bytes-b ((self <output-port>) bvec offset len)
  (let ((x (make-string len)))
    (bvec-copy x 0 bvec offset len)
    (write-string self x)))

(define call-write-bytes write-bytes-a)

;; XML normalise line end convention as well

(define normalise-data-utf-8 '("UTF-8" "UTF8" "utf8" "utf-8"))

(define (normalise-data str to-encoding from-encoding eol-string)
  (let ((to-encoding (or to-encoding "UTF-8"))
        (from-encoding (or from-encoding "UTF-8")))
    (cond
     ((or (equal? to-encoding from-encoding)
	  (and (string? to-encoding) (string? from-encoding)
	       (member to-encoding normalise-data-utf-8)
	       (member from-encoding normalise-data-utf-8)))
      (if eol-string
          (let ((nl (normalized-length str (string-length str)
                                       (string-length eol-string))))
            (if (eqv? nl (string-length str))
                str
                (normalize-input str (make-string nl) nl eol-string)))
          str))
     (else
      (let ((cnv (or (iconv-open to-encoding from-encoding)
                     (error "iconv-open ~a ~a" to-encoding from-encoding)))
            (buf (make-string (iconv-buflen))) ; internal buffer
            (eol (or eol-string "\n")))
        ;;
        (call-with-output-string
	 (lambda (o)
	   (let loop ((i 0)
		      (n (string-length str)))
	     (if (= n 0)
		 (iconv-close cnv)
		 (bind ((nx sn dn (iconv-bytes cnv str i n buf 0 (iconv-buflen))))
		       (let* ((bl (- (iconv-buflen) dn))
			      (nl (normalized-length buf bl (string-length eol))))
			 ;; Beware: the conversion works only for the
			 ;; special case of XML, where we know the
			 ;; canonical line delimiter is #\newline at most
			 ;; as long as the one we find in the input stream.
			 (cond
			  ;; BUG this code doesn't even deal with
			  ;; #\return -only line ends.  If that becomes
			  ;; an issue, remove the first case.
					; ((eqv? bl nl) (call-write-bytes o buf 0 nl))
			  ((<= nl bl)
			   (call-write-bytes
			    o (normalize-input buf buf nl eol) 0 nl))
			  (else
			   (call-write-bytes
			    o (normalize-input buf (make-string nl) nl eol)
			    0 nl)))
			 (loop (+ i (- n sn)) sn))))))))))))

;; The rest of the file is a plain copy of iconv from Donovan's stuff.

(define-class <iconv-error> (<condition>)
  errno offset source)

(define-method display-object ((self <iconv-error>) port)
  (format port "(~d) ~a offset ~d in '~a'\n"
          (errno self)
          (errno-message (errno self))
          (offset self)
	  (if (> (string-length (source self)) 200)
	      (substring (source self) 50) (source self))))

(define-class <iconv-desc> (<object>) :bvec)

;;;
;;;  Note the wierd (i.e., backwards) order of the arguments
;;;  to iconv-open.  I left them this way because that is
;;;  the order of arguments to the underlying iconv_open().

(define-safe-glue (iconv-open (to <raw-string>) (from <raw-string>))
  properties: ((other-h-files "<iconv.h>"))
  literals: ((& <iconv-desc>))
{
  iconv_t i;
  obj d;

  i = iconv_open( to, from );
  if (i == (iconv_t)-1) {
    RETURN0();
  } else {
    d = alloc( sizeof( i ), TLREF(0) );
    *((iconv_t *)PTR_TO_DATAPTR(d)) = i;
    REG0 = d;
    RETURN1();
  }
})

(define-safe-glue (iconv-close (self <iconv-desc>))
{
  iconv_close( *((iconv_t *)PTR_TO_DATAPTR(self)) );
  RETURN0();
})

(define-safe-glue (iconv-bytes (self <iconv-desc>) 
                               s (soffset <raw-int>) (slen <raw-int>)
                               d (doffset <raw-int>) (dlen <raw-int>))
  type-handler: (<iconv-desc>
		 (direct-instance? <iconv-desc>)
		 ("iconv_t ~a" "*(iconv_t *)PTR_TO_DATAPTR(~a)"))
  properties: ((other-h-files "<iconv.h>" "<errno.h>"))
  literals: ((& <iconv-error>))
{
  size_t n, sn, dn;
  char *sp, *dp;

  CHECK_BVEC( s );
  CHECK_BVEC( d );

  sp = ((char *)PTR_TO_DATAPTR(s)) + soffset;
  dp = ((char *)PTR_TO_DATAPTR(d)) + doffset;
  sn = slen;
  dn = dlen;

  n = iconv( self, &sp, &sn, &dp, &dn );
  if ((((int)n) < 0) && (errno != E2BIG) && (errno != EINVAL)) {
    raise_error( make4( TLREF(0), NIL_OBJ, int2fx(errno),
                 int2fx(slen-sn), s ) );
  }

  REG0 = int2fx( n );
  REG1 = int2fx( sn );
  REG2 = int2fx( dn );
  RETURN(3);
})

(define (unicode-string->utf8 (str <unicode-string>))
  (let ((cnv (iconv-open "UTF-8" "UCS-2"))
        (buf (make-string (iconv-buflen)))
        (o (open-output-string)))
    ;;
    (let loop ((i 0)
               (n (- (bvec-length str) 2)))
      (if (= n 0)
          (begin
            (iconv-close cnv)
            (close-output-port o))
          (bind ((n sn dn (iconv-bytes cnv str i n buf 0 (iconv-buflen))))
            (call-write-bytes o buf 0 (- (iconv-buflen) dn))
            (loop (+ i (- n sn)) sn))))))

(define (utf8->unicode-string (str <string>))
  (if (string=? str "")
      (let ((x (bvec-alloc <unicode-string> 2)))
        (bvec-write-unsigned-16 x 0 0)
        x)
      ;;
      (let* ((cnv (iconv-open "UCS-2" "UTF-8"))
             (N (* 2 (string-length str)))
             (buf (make-string N))
             (o #f))
        ;;
        (define (finish s #optional n)
          (let* ((n (or n (string-length s)))
                 (x (bvec-alloc <unicode-string> (+ 2 n))))
            (bvec-copy x 0 s 0 n)
            (bvec-write-unsigned-16 x n 0)
            (iconv-close cnv)
            x))
        ;;
        (let loop ((i 0)
                   (n (string-length str)))
          (if (= n 0)
              ;; given the over-estimation of `N', it's not clear
              ;; how we would ever get here, since we already handled
              ;; the empty string case...
              (finish (close-output-port o))
              (bind ((nx sn dn (iconv-bytes cnv str i n buf 0 N)))
                (if (and (= i 0) (= sn 0))
                    ;; did it all in one conversion!
                    (finish buf (- N dn))
                    (begin
                      (if (not o)
                          (set! o (open-output-string)))
                      (call-write-bytes o buf 0 (- N dn))
                      (loop (+ i (- n sn)) sn)))))))))
