;; (C) 2000, 2001, 2002, 2013 J�rg F. Wittenberger see http://www.askemos.org

(define (%node-list-append . nl) (apply append nl))

;;* Data Definition

;; Rscheme could do the data definition (make-*) with classes.  Hm.
;; This would require a little more typing and porting effort if we
;; want to change the underlying scheme.  Leave that as R4RS.

;; Every data object is implemented as a vector, the component at
;; position 0 is a type tag.

;; One thing I feel (no numbers mesured) that it slows down DSSSL by
;; design is, that so much stuff is modeled using strings (e.e., gi's)
;; while they are so simillar to symbols.  We implement it using
;; symbols here.

;; make-xml-attribute is to be used in a named node list (see groves),
;; e.g., the attributes of an element.  Slighly different to sdc and
;; the latter should be updated.

;; 2002-01-29 converting the code
;(define (make-xml-attribute name ns value)
;  (cons  name (vector 'xml-attribute-def ns value)))
;(define (xml-attribute-def-value x) (vector-ref x 2))
;(define (xml-attribute-def-type x) (vector-ref x 1))
;(define (xml-attribute? node)
;  (and (pair? node)
;       (symbol? (car node))
;       (vector? (cdr node))
;       (eq? (vector-ref (cdr node) 0) 'xml-attribute-def)))

;(define (make-xml-attribute name ns value)
;  (vector 'xml-attribute name ns value))

(define-class <xml-attribute> (<object>)
  (xml-attribute-name type: <symbol>)
  xml-attribute-ns
  (xml-attribute-value type: <string>))

(define (make-xml-attribute name ns value)
  (make <xml-attribute>
    xml-attribute-name: name xml-attribute-ns: ns
    xml-attribute-value: value))

(define (xml-attribute? x) (instance? x <xml-attribute>))

;(define (xml-attribute-value x) (vector-ref x 3))
;(define (xml-attribute? node)
;  (and (vector? node) (eq? (vector-ref node 0) 'xml-attribute)))

;;* Namespaces

;; Beware: The data of xml-element definition does NOT mirror the full
;; DOM structure for a reason.  DOM defines the access to the tree
;; structure only, obvoiusly knowing that this could be implemented as
;; static data structure as well as dynamically (lazy) upon tree
;; traversal.  I do intend the latter for memory and performance
;; reasons.  Hence we see only the document axis (see XPath), which
;; are content (not context~) related.

;; I've tried several strategies to implement xml element creation.
;; The first is really R5RS compatible, but allows to break the
;; abtraction boundary by looking at is as a vector.  The second
;; should be replaced by a SRFI-9 compatible construct, however the
;; version here has been contested against the third one, which
;; provides support for kind of a global registration mechanism for
;; new kinds of specially handled xml elements.  While this looks like
;; a desirable feature, test show a considerable cost of ~34% runtime.
;; That's deemed too expensive and hence going to be done at prsing
;; time, if at all.

;; (define (make-xml-element gi ns attributes content)
;;   (vector 'xml-element gi ns attributes content))

(define-class <xml-element> (<object>)
  (name type: <symbol>)
  xml-element-ns
  ;; (xml-element-attributes type: <list>)
  %xml-element-attributes
  %children)

(define (make-xml-element g n a c)
  (make <xml-element>
    name: g xml-element-ns: n
    %xml-element-attributes: a %children: c))


(define (xml-element-attributes element)
  (let ((v (%xml-element-attributes element)))
    (if (procedure? v) (v) v)))


(: xml-element-special (* --> (procedure (&rest) *)))
(define (xml-element-special obj)
  (and (xml-element? obj)
       (let ((v (%xml-element-attributes obj)))
	 (and (procedure? v) v))))


;; (define (make-xml-element gi ns attributes content)
;;   (make <xml-element>
;;     name: gi xml-element-ns: ns
;;     xml-element-attributes: attributes %children: content))

;; (define *make-xml-element-ns-handlers* (make-symbol-table))

;; (define (make-xml-element gi ns attributes content)
;;   (or (and
;;        ns
;;        (and-let* ((entry (table-lookup *make-xml-element-ns-handlers* ns))
;;                   (elcons (if (table? entry) (table-lookup entry gi) entry)))
;;                  (elcons gi ns attributes content)))
;;       (make <xml-element>
;;         name: gi xml-element-ns: ns
;;         xml-element-attributes: attributes %children: content)))

;(define (xml-element? node)
;  (and (vector? node) (eq? (vector-ref node 0) 'xml-element)))
(define (xml-element? node) (instance? node <xml-element>))

;; TODO: rename xml-element-attributes in all code into attributes.
;(define (xml-element-attributes node) (vector-ref node 3))
;(define attributes xml-element-attributes)

(define (attributes node)
  (let ((node (node-list-first node)))
    (and (xml-element? node) (xml-element-attributes node))))

;(define (gi node) (vector-ref node 1))

;; lt. DSSSL gi has an optional node list argument defaulting to
;; current-node.  This is too expensive for the kernel for now.  To
;; comply we would also need to check that node is actually a
;; singleton node list.

(define (gi node)
  (let ((node (node-list-first node)))
    (and (xml-element? node) (name node))))

(define (ns node)
  (let ((node (node-list-first node)))
    (and (xml-element? node) (xml-element-ns node))))

(define dsssl-gi gi)
(define dsssl-ns ns)

(define (dsssl-children obj)
  (cond
   ((not obj) (empty-node-list))
   (else (children obj))))
