(define-class <gcrypt-error> (<simple-error>) code)

(define-class <gcrypt-md-ctx> (<bvec>))

(define-macro (define-gcrypt-glue args . body)
  `(define-safe-glue ,args
     type-handler: (<gcrypt-md-ctx> 
		    (direct-instance? <gcrypt-md-ctx>)
		    ("gcry_md_hd_t ~a"
		     "(*(gcry_md_hd_t *)PTR_TO_DATAPTR(~a))"))
     properties: ((other-h-files "<gcrypt.h>")
		  (other-libs "gcrypt"))
     ,@body))

(define-gcrypt-glue (gcry-md-algo-name (algo <raw-int>))
{
  const char *name=gcry_md_algo_name(algo);
  REG0 = ( strcmp(name, "?") == 0 ) ? FALSE_OBJ : make_string(name);
  RETURN1();
})

(define-gcrypt-glue (gcry-md-get-algo-dlen (algo <raw-int>))
{
  REG0 = gcry_md_get_algo_dlen(algo);
  RETURN1();
})

(define-gcrypt-glue (gcry-md-map-name (algo <string>))
{
  int n=gcry_md_map_name(string_text(algo));
  if(n==0) REG0=FALSE_OBJ;
  else REG0=int2fx(n);
  RETURN1();
})

(define-gcrypt-glue (gcry-md-ctx-create (algo <raw-int>))
  literals: ((& <gcrypt-md-ctx>))
{
  int r;
  REG0 = alloc(sizeof(gcry_md_hd_t), TLREF(0));
  r=gcry_md_open( (gcry_md_hd_t *)PTR_TO_DATAPTR(REG0), algo, 0);
  if( r != 0 ) scheme_error("gcry_md_open failed", 1, int2fx(gcry_err_code(r)));
  mark_as_finalizable(REG0);
  RETURN1();
})

(define-gcrypt-glue (gcry-md-ctx-finalize (ctx <gcrypt-md-ctx>))
{
  gcry_md_close(ctx);
  RETURN0();
})

(define-method finalize ((ctx <gcrypt-md-ctx>))
  (gcry-md-ctx-finalize ctx))

(define-gcrypt-glue (gcry-md-enable (ctx <gcrypt-md-ctx>) (algo <raw-int>))
{
  int r=gcry_md_enable(ctx, algo);
  if(r != 0) scheme_error("gcry_md_enable failed", 1, int2fx(gcry_err_code(r)));
  REG0 = raw_ctx;
  RETURN1();
})

(define-gcrypt-glue (gcry-md-read (ctx <gcrypt-md-ctx>) (algo <raw-int>) (packed <raw-bool>))
{
  static char hex_char[] = "0123456789abcdef";
  unsigned char *md=gcry_md_read(ctx, algo), *d;
  size_t s=gcry_md_get_algo_dlen(algo), di=0 ;
  if(packed==YES) {
    /* REG0 = bvec_alloc( s, byte_vector_class ); */
    REG0 = bvec_alloc( s, string_class );
    REG1 = int2fx(s);
    d = (unsigned char *) string_text(REG0);
    for( ; di < s ; di++ ) d[di] = md[di];
  } else {
    REG0 = bvec_alloc( 2 * s + 1 , string_class );
    REG1 = int2fx(2 * s);
    d = (unsigned char *) string_text(REG0);
    for( ; di < s ; di++ ) {
      *d++ = hex_char[(md[di]>>4) & 0x0f];
      *d++ = hex_char[ md[di]     & 0x0f];
    }
    *d = 0;
  }
  RETURN(2);
})

(define-gcrypt-glue (gcry-md-write (ctx <gcrypt-md-ctx>)
				   (d <raw-string>) 
				   (cnt <raw-int>)
				   (off <raw-int>))
{
  gcry_md_write(ctx, (const void *)&d[off], cnt);
  REG0 = raw_ctx;
  RETURN1();
})

(define-safe-glue (gcry-init)
{
  /* Version check should be the very first call because it
     makes sure that important subsystems are intialized. */
  if (!gcry_check_version (GCRYPT_VERSION)) {
    scheme_error("libgcrypt version mismatch", 1, int2fx(GCRYPT_VERSION));
  }
  /* Tell Libgcrypt that initialization has completed. */
  gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);

 RETURN0();
})

(gcry-init)

(define *string-digest-chunk-size* 512)
(define *port-digest-chunk-size* 4096)

(define (string-digest (data <string>) (algo <integer>) packed)
 (let ((len (string-length data))
       (cs *string-digest-chunk-size*))
   (let loop ((ctx (gcry-md-ctx-create algo))
	      (off 0)
	      (rest len))
     (if (fixnum<? rest cs)
	 (gcry-md-read (gcry-md-write ctx data rest off) algo packed)
	 (loop (gcry-md-write ctx data cs off)
	       (fixnum+ off cs)
	       (fixnum- rest cs))))))

(define (port-digest (port <input-port>) (algo <integer>) packed)
  (let ((cs *port-digest-chunk-size*)
	(ctx (gcry-md-ctx-create algo)))
    (handler-case
     (let loop ((ctx ctx))
       (loop (gcry-md-write ctx (read-string port cs) cs 0)))
     ((<partial-read> condition: c)
      (gcry-md-read 
       (gcry-md-write
	ctx
	(partially-read c)
	(string-length (partially-read c)) 0) algo packed)))))

(define md5-digest
  (let ((algo (gcry-md-map-name "md5")))
    (lambda (x) ((if (string? x) string-digest port-digest) x algo #f))))

(define sha256-digest
  (let ((algo (gcry-md-map-name "sha256")))
    (lambda (x) ((if (string? x) string-digest port-digest) x  algo #f))))

(define sha1-digest
  (let ((algo (gcry-md-map-name "sha1")))
    (lambda (x . packed)
      ((if (string? x) string-digest port-digest) x  algo
       (and (pair? packed) (car packed))))))
