;; New Hashtable API
;;
;; http://srfi.schemers.org/srfi-69/srfi-69.html

(define (make-hash-table equal? . rest)
  (apply make-table equal? rest))

(define (hash-table? obj)
  (instance? obj <table>))

(define (alist->hash-table alist . rest)
  (let ((t (apply make-hash-table rest)))
    (for-each (lambda (e) (table-insert! t (car e) (cdr e))))
    t))

(define hash-table-equivalence-function table-equal-function)

(define hash-table-hash-function table-hash-function)

(define-class <hash-table-lookup-error> (<condition>)
  hash-table
  key)

(define-method write-object ((self <hash-table-lookup-error>) port)
  (format port "#[hash-table-ref error: ~a no key ~s]" (hash-table self) (key self)))

(define-method display-object ((self <hash-table-lookup-error>) port)
  (format port "#[hash-table-ref error: ~a no key ~s]" (hash-table self) (key self)))

(define (hash-table-ref hash-table key . thunk)
  (or (table-lookup hash-table key)
      (if (table-key-present? hash-table key)
	  #f
	  (if (pair? thunk) ((car thunk))
	      (error (make <hash-table-lookup-error> hash-table: hash-table key: key))))))

(define (hash-table-ref/default hash-table key default)
  (or (table-lookup hash-table key)
      (and (not (table-key-present? hash-table key)) default)))

;; BEWARE: the defined return type is "undefined".  We don't clobber
;; rschemes (better) interface here, even though we should, for
;; performance reasons.  Not however to never use the return value
;; from hash-table-set!

(define hash-table-set! table-insert!)

(define hash-table-delete! table-remove!)

(define hash-table-exists? table-key-present?)

(define (hash-table-update! hash-table key function . thunk)
  (let* ((old (table-lookup hash-table key))
	 (input (if (table-key-present? hash-table key)
		    old
		    (if (pair? thunk) ((car thunk))
			(error (make <hash-table-lookup-error>
				 hash-table: hash-table key: key)))))
	 (new (function input))
	 (replaced (if (eq? new old) old (table-insert! hash-table key new))))
    (if (not (eq? old replaced))
	(error "hash-table-update! ~s entry ~s clobbered (from other thread?)"
	       hash-table key))))

(define (hash-table-update!/default hash-table key function default)
  (let* ((old (table-lookup hash-table key))
	 (new (function (if (table-key-present? hash-table key) old default)))
	 (replaced (if (eq? new old) old (table-insert! hash-table key new))))
    (if (not (eq? old replaced))
	(error "hash-table-update!/default ~s entry ~s clobbered (from other thread?)"
	       hash-table key))))

(define hash-table-size table-size)

(define hash-table-keys key-sequence)

(define hash-table-values value-sequence)

(define (hash-table-walk table proc)
  (table-for-each table (lambda (h k v) (proc k v))))

(define (hash-table-fold table combine init)
  ;; we iterate and accumulate the result in "init".
  (table-for-each table (lambda (h k v) (set! init (combine k v init))))
    ;; return the accumulated value.
    init)

(define (hash-table->alist-combine k v i)
  (cons (cons k v) i))

(define (hash-table->alist hash-table)
  (hash-table-fold hash-table hash-table->alist-combine '()))

(define (hash-table-merge!-combine k v i)
  (table-insert! i k v) i)

(define (hash-table-merge! into from)
  (hash-table-fold from hash-table-merge!-combine into))

;; FIXME: the hash function ignore the optional bound argument for
;; now.

(define (hash obj . rest) (hash-code obj))

(define (string-hash obj . rest) (string->hash obj))

(define (string-ci-hash obj . rest) (string-ci->hash obj))

(define (hash-by-identity obj . rest) (hash-code obj))
