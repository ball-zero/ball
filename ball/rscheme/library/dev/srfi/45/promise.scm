(define-class <recursive-promise> (<object>)
  box)

(define (recursive-promise? obj)
  (instance? obj <recursive-promise>))

(define (promise? obj)
  (or (recursive-promise? obj) (instance? obj <promise>)))

(define-method eager-promise? ((self <object>)) #f)

(define-method lazy-promise? ((self <object>)) #f)

(define-method eager-promise? ((self <recursive-promise>))
  (not (car (box self))))

(define-method lazy-promise? ((self <recursive-promise>))
  (car (box self)))

(define-method force ((self <recursive-promise>))
  (let ((content (box self)))
    (if (car content)
	(call-with-values (cdr content)
	  (lambda tail
	    (if (and (pair? tail) (null? (cdr tail))
		     (recursive-promise? (car tail)))
		(let ((content (box self)))
		  (if (car content)
		      (let ((x (box (car tail))))
			(set-car! content (car x))
			(set-cdr! content (cdr x))
			(set-box! (car tail) content)))
		  (force self))
		(apply values tail))))
	(apply values (cdr content)))))

(define-syntax (lazy expr)
  (make <recursive-promise> box: (cons #t (lambda () expr))))

(define (eager x) (make <recursive-promise> box: (list #f x)))

(define-syntax (delay expr)
  (lazy (call-with-values expr eager)))

#|
(with-module repl
  (with-module compiler
    (with-module tables
      (with-module mlink
	(let ((target (top-level-envt (get-module 'user))))
	  (for-each (lambda (new-name old-name)
		      (table-insert! (table target) new-name 
				     (table-lookup (table target) old-name)))
		    '(delay1)
		    '(delay)))))))
|#
