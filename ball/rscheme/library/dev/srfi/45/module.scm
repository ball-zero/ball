(define-module srfi.45 ()
  (&module
   (import usual-inlines)
   (implements SRFI-45 srfi-45)
   ;;
   ;;
   (load "promise.scm")
   ;
   (export
    ;; SRFI 45
    lazy
    eager
    delay
    promise?
    force
    ;; Extras
    lazy-promise?
    eager-promise?
    recursive-promise?)))
