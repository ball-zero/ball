;; Shamelessly imported from
;; http://lists.gnu.org/archive/html/guile-user/2002-10/msg00021.html

;--- srfi/srfi-26.scm: 

(define-macro (cut slot . slots)
  (let loop ((slots     (cons slot slots))
             (params    '())
             (args      '()))
    (if (null? slots)
        `(lambda ,(reverse! params) ,(reverse! args))
      (let ((s    (car slots))
            (rest (cdr slots)))
        (case s
          ((<>)
           (let ((var (gensym)))
             (loop rest (cons var params) (cons var args))))
          ((<...>)
           (if (pair? rest)
               (error "<...> not on the end of cut expression"))
           (let ((var (gensym)))
             `(lambda ,(append! (reverse! params) var)
                (apply ,@(reverse! (cons var args))))))
          (else
           (loop rest params (cons s args))))))))

(define-macro (cute . slots)
  (let ((temp (map (lambda (s) (and (not (memq s '(<> <...>))) (gensym))) 
slots)))
    `(let ,(delq! #f (map (lambda (t s) (and t (list t s))) temp slots))
       (cut ,@(map (lambda (t s) (or t s)) temp slots)))))
