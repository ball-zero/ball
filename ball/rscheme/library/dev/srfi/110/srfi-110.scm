
(define srfi-69-make-hash-table make-hash-table)

;; Kill undefined stuff for now

(define-macro (type? x) #f)

(define-macro (type-of x) #f)

(define-macro (slot-ref t x i) #f)

(define-macro (type-num-slots x) 0)

(define-macro (type-printer x) #f)

(define list->u8vector list->vector)

(define (sweet-read-all port)

  (set-read-mode 'keyword-style-suffix #f)

  (let loop ((expr (sweet-read port)) (result '()))
    (if (eof-object? expr)
	(reverse! result)
	(loop (sweet-read port) (cons expr result)))))
