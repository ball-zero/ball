(define-macro (define-pgsql-glue args . body)
  `(define-safe-glue ,args
     type-handler: (<pgsql-connection>
		    (direct-instance? <pgsql-connection>)
		    ("PGconn *~a"
		     "(PGconn *)OBJ_TO_RAW_PTR(gvec_ref(~a,SLOT(1)))"))
     type-handler: (<pgsql-result>
		    (class-eq? <pgsql-result>)
		    ("PGresult *~a"
		     "(PGresult *)OBJ_TO_RAW_PTR(gvec_ref(~a,SLOT(0)))"))

     properties: ((other-h-files "<libpq-fe.h>")
		  (other-h-files "<string.h>" "<stdlib.h>")
		  (other-libs "pq"))

     ,@body))
