(define-class <pgsql-connection> (<sql-connection>)
  (database type: <string>)
  raw-connection)

(define-class <pgsql-error> (<sql-error>)
  (message type: <string>))

(define-class <pgsql-result> (<sql-result>)
  raw-pgsql-result
  connection
  proc
  )
