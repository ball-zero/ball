(define-method display-object ((self <pgsql-error>) port)
  (format port "<pgsql-error> ~a\n" (message self)))

(define-pgsql-glue (pgsql-connectdb (name <raw-string>)
                                    (conninfo <raw-string>))
 literals: ((& <pgsql-connection>)
	    (& <pgsql-error>))
{
  PGconn *conn = PQconnectdb(conninfo);

  if( !conn ) {
    raise_error( make1(TLREF(1), make_string("connect failed")) );
  }

  if( PQstatus(conn) == CONNECTION_BAD ) {
    obj descr = make_string(PQerrorMessage(conn));
    PQfinish(conn);
    raise_error( make1(TLREF(1), descr) );
  } else if( PQsetnonblocking(conn, 1) ) {
    obj descr = make_string(PQerrorMessage(conn));
    PQfinish(conn);
    raise_error( make1(TLREF(1), descr) );
  } else {
    REG0 = make2( TLREF(0), raw_name, RAW_PTR_TO_OBJ(conn));
  }

  RETURN1();
})

(define (pgsql-connect db host user pass)
  (pgsql-connectdb
   db
   (apply string-append
          (append (list " dbname='" db "' ")
                  (if (list " host='" host "' ") '(""))
                  (if user (list " user='" user "' ") '(""))
                  (if pass (list " password='" pass "' ") '(""))))))

(define-pgsql-glue (pgsql-close (db <pgsql-connection>))
{
  PQfinish(db);
  RETURN0();
})

(define-pgsql-glue (pgsql-send-query (db <pgsql-connection>)
                                     (query <raw-string>))
  literals: ((& <pgsql-error>))
{
  if( !PQsendQuery(db, query) ) {
    raise_error(make1(TLREF(0), make_string(PQerrorMessage(db))) );
  }
  RETURN0();
})

(define-pgsql-glue (pgsql-with-result-tupels (db <pgsql-connection>)
                                             (proc <function>))
  literals: ((& <pgsql-result>)
	     (& <pgsql-error>))
{
  PGresult *res = NULL;

  res = PQgetResult(db);
  if(!res) {
    if( 0 ) { /* might we have to handle anything here? */
      raise_error(make1(TLREF(1), make_string(PQerrorMessage(db))) );
    } else {
      RETURN0();
    }
  }
  REG0 = make3( TLREF(0), RAW_PTR_TO_OBJ(res), RAW_PTR_TO_OBJ(db), proc);
  SAVE_CONT1(done_with_proc);
  REG1 = int2fx( PQntuples(res) );
  REG2 = int2fx( PQnfields(res) );
  APPLY(3, proc);
}
("done_with_proc" {

  PGresult *res = OBJ_TO_RAW_PTR(gvec_ref(PARTCONT_REG(0), SLOT(0)));

  PQclear( res );

  res = PQgetResult(OBJ_TO_RAW_PTR(gvec_ref(PARTCONT_REG(0), SLOT(1))));

  if( !res ) {
    RESTORE_CONT_REG();
    RETURN(arg_count_reg);
  } else {
    REG0 = PARTCONT_REG(0);
    gvec_set(REG0, SLOT(0), RAW_PTR_TO_OBJ(res));
    SAVE_CONT1(done_with_proc);
    REG1 = int2fx( PQntuples(res) );
    REG2 = int2fx( PQnfields(res) );
    APPLY(3, gvec_ref(REG0, SLOT(2)));
  }
}))

(define-pgsql-glue (pgsql-busy? (db <pgsql-connection>))
  literals: ((& <pgsql-error>))
{

  if( PQflush(db) ) {
    raise_error(make1(TLREF(0), make_string(PQerrorMessage(db))) );
  }
  if( !PQconsumeInput(db) ) {
    raise_error(make1(TLREF(0), make_string(PQerrorMessage(db))) );
  }
  REG0 = PQisBusy(db) ? TRUE_OBJ : FALSE_OBJ;
  RETURN1();
})

(define *pgsql-query-time* 10)

(define (set-pgsql-query-time! (n <integer>))
  (set! *pgsql-query-time* n))

(define (pgsql-with-tupels (db <pgsql-connection>)
                           (query <string>)
                           (proc <function>))
  (pgsql-send-query db query)

  (let loop ()
    (if (pgsql-busy? db)
        (begin (thread-sleep-ms *pgsql-query-time*) (loop))))

  (pgsql-with-result-tupels db proc))

(define-pgsql-glue (pgsql-value (result <pgsql-result>)
                                (row <raw-int>)
                                (field <raw-int>))
  literals: ((& <pgsql-result>)
	     (& <pgsql-error>))
{
  if( row < 0 || row >= PQntuples(result) ) {
    raise_error(make1(TLREF(1), make_string("illegal raw number")) );
    RETURN0();
  } else if( PQgetisnull( result, row, field ) ) {
    REG0 = FALSE_OBJ;
  } else {
    int length = PQgetlength( result, row, field);
    REG0 = bvec_alloc( length + 1, string_class );
    memcpy( PTR_TO_DATAPTR(REG0), PQgetvalue( result, row, field ), length );
  }
  RETURN1();
})

(define-pgsql-glue (pgsql-field (result <pgsql-result>)
                                (field <raw-int>))
  literals: ((& <pgsql-result>)
	     (& <pgsql-error>))
{
  if( field < 0 || field >= PQnfields(result) ) {
    raise_error(make1(TLREF(1), make_string("illegal field number")) );
    RETURN0();
  } else {
    REG0 = make_string( PQfname( result, field ) );
  }
  RETURN1();
})

(set! *the-db-drivers* `(("PostgreSQL" . ,pgsql-connect) . ,*the-db-drivers*))

(define-method sql-close ((self <pgsql-connection>))
  (pgsql-close self))

(define-method sql-value ((self <pgsql-result>) row field)
  (pgsql-value self row field))

(define-method sql-field ((self <pgsql-result>) field)
  (pgsql-field self field))

(define-method sql-with-tupels ((self <pgsql-connection>)
                                (query <string>)
                                (proc <function>))
  (pgsql-with-tupels self query proc))
