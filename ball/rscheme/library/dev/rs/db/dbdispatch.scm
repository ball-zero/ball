;; (C) 2003, 2005 J�rg F. Wittenberger -*-Scheme-*-

(define *the-db-drivers* '())

(define (sql-connect (driver <string>)
                     (db <string>)
                     (host <string>)
                     (user <string>)
                     (pass <string>))
  (let ((entry (assoc driver *the-db-drivers*)))
    (if entry
	((cdr entry) db host user pass)
	(error (string-append "sql-connect unsupported driver '"
			      driver
			      "' requested.")))))

(define-method sql-close ((self <sqlite3-database>))
  (sqlite3-close self))

(define-method sql-with-tupels ((self <sqlite3-database>)
                                (query <string>)
                                (proc <function>))
  (let* ((result (sqlite3-exec self query))
	 (rows (vector-length result)))
    (if (eqv? rows 0)
	(proc result 0 0)
	(proc result (sub1 rows) (vector-length (vector-ref result 0))))))

(define (vassoc val vec)
  (do ((i 0 (add1 i)))
      ((or (eqv? i (vector-length vec))
	   (equal? val (vector-ref vec i)))
       (and (fixnum<? i (vector-length vec)) i))))

;; WARNING: sql-index, sql-field and sql-value are experimental.

(define-method sql-index ((self <vector>) field)
  (and (fixnum>? (vector-length self) 0)
       (vassoc field (vector-ref self 0))))

(define-method sql-field ((self <vector>) field)
  (vector-ref (vector-ref self 0) field))

(define-method sql-value ((self <vector>) row field)
  (vector-ref (vector-ref self (add1 row))
	      (or (cond
		   ((integer? field) field)
		   ((string? field) (sql-index self field))
		   ((symbol? field) (sql-index self (symbol->string field)))
		   (else (error "sql-value bad index type ~s" field)))
		  (error "no field ~a in ~a" field
			 (vector-ref self 0)))))

(define-method sql-ref ((self <vector>) row field)
  (cond
   ((and row field) (sql-value self row field))
   (field (sql-index self field))
   (else (sub1 (vector-length self)))))

(define sql-fold/vector
  (letrec ((loop (lambda (kons knil len result i)
                   (if (eqv? i len)
                       knil
                       (loop kons
                             (kons
			      (lambda (x) (sql-value result i x))
			      knil)
                             len result (add1 i))))))
    (lambda (self kons knil)
      (loop kons knil (sub1 (vector-length self)) self 0))))

(define-method sql-fold ((self <vector>) kons nil) (sql-fold/vector self kons nil))

(define (sql-result? obj)
  (or (vector? obj) (instance? obj <sql-result>)))

;; For the time of conversion we have the one-shot versions, which
;; SHOULD be brought to an interating, lazy implementation.

(define-macro (receive vars expr . body)
  `(bind ((,@vars ,expr)) . ,body))

(define (one-shot-sql-tupels%-fold-left db
                                        (query <string>)
                                        (setup-seeds <function>)
                                        (fold-function <function>)
                                        seeds)
  ;; TODO fix the mxsql-driver to actually return the value!
  (sql-with-tupels
   db query
   (lambda (result rows cols)
     (if (eqv? rows 0)
         (apply list->values (append! seeds (setup-seeds result rows cols)))
         (let ((cols (range cols)))
           (let loop ((seeds (append! seeds (setup-seeds result rows cols)))
                      (row 0))
             (if (eqv? row rows)
                 (apply list->values seeds)
                 (receive (proceed . seeds)
                          (apply fold-function
                                 (map (lambda (field)
                                        (sql-value result row field))
                                      cols)
                                 seeds)
                          (if proceed
                              (loop seeds (add1 row))
                              (apply list->values seeds))))))))))
