(define-method display-object ((self <mysql-error>) port)
  (format port "<mysql-error> ~a\n" (message self)))

(define-mysql-glue (mysql-connect (db <raw-string>)
                                  (host <raw-string>)
                                  (user <raw-string>)
                                  (pass <raw-string>))
 literals: ((& <mysql-connection>)
	    (& <mysql-error>))
{
  MYSQL *conn = NULL; /* old: = malloc(sizeof (MYSQL)); */

  conn = mysql_init(conn);

  if( !conn ) {
    raise_error( make2(TLREF(1), NIL_OBJ, make_string("out of memory")) );
  }

  if( !mysql_real_connect(conn, host, user, pass, db, 0, NULL, 0) ) {
    obj descr = make_string(mysql_error(conn));
    mysql_close(conn);  /* old: free(conn); */
    raise_error( make2(TLREF(1), NIL_OBJ, descr) );
  } else {
/* db selection is now in the connect above */
#if 0
    if( mysql_select_db(conn, db) ) {
      obj descr = make_string(mysql_error(conn));
      raise_error( make2(TLREF(1), NIL_OBJ, descr) );
    } else {
#endif
      REG0 = make2( TLREF(0), raw_db, RAW_PTR_TO_OBJ(conn));
#if 0
    }
#endif
  }
  RETURN1();
})

(define-mysql-glue (mysql-close (db <mysql-connection>))
{
  mysql_close(db);
  RETURN0();
})

(define-mysql-glue (mysql-send-query (db <mysql-connection>)
                                     (query <string>))
{
fprintf(stderr, "mysql_real_query %p %s\n", db, string_text(query));
  if( mysql_real_query(db, string_text(query), string_length(query) ) ) {
    raise_error(make2(TLREF(1), NIL_OBJ, make_string(mysql_error(db))) );
  }
  RETURN0();
})

(define-mysql-glue (mysql-with-result-tupels (db <mysql-connection>)
                                             (proc <function>))
  literals: ((& <mysql-result>)
	     (& <mysql-error>))
{
  MYSQL_RES *res = NULL;
  res = mysql_store_result(db);
  if(!res) {
    if( mysql_errno(db) ) {
      raise_error(make2(TLREF(1), NIL_OBJ, make_string(mysql_error(db))) );
    } else {
      RETURN0();
    }
  }
  REG0 = make2( TLREF(0), RAW_PTR_TO_OBJ(res), raw_db);
  SAVE_CONT1(done_with_proc);
  REG1 = int2fx( mysql_num_rows(res) );
  REG2 = int2fx( mysql_num_fields(res) );
  APPLY(3, proc);
}
("done_with_proc" {
  MYSQL_RES *res;

  res = OBJ_TO_RAW_PTR(gvec_ref(PARTCONT_REG(0), SLOT(0)));
  mysql_free_result( res );

  RESTORE_CONT_REG();
  RETURN(arg_count_reg);
}))

;; KLUDGE: I can't find out how to test whether the mysql server is
;; done processing a query.  So we wait for a configurable time.

(define *mysql-query-time* 20)

(define (set-mysql-query-time! (n <integer>))
  (set! *mysql-query-time* n))

(define (mysql-with-tupels (db <mysql-connection>)
                           (query <string>)
                           (proc <function>))
  (mysql-send-query db query)
  (thread-sleep-ms *mysql-query-time*)
  (mysql-with-result-tupels db proc))

(define-mysql-glue (mysql-value (result <mysql-result>)
                                (row <raw-int>)
                                (field <raw-int>))
  literals: ((& <mysql-result>)
	     (& <mysql-error>))
{
  if( row < 0 || row >= mysql_num_rows(result) ) {
    raise_error(make2(TLREF(1), NIL_OBJ, make_string("illegal raw number")) );
    RETURN0();
  } else {
    unsigned long *length;
    MYSQL_ROW data;
    mysql_data_seek(result, row);
    data = mysql_fetch_row(result);
    length = mysql_fetch_lengths(result);
    if( !data ) {
      MYSQL *db = (MYSQL *)OBJ_TO_RAW_PTR( gvec_ref( raw_result, SLOT(1) ) );
      raise_error(make2(TLREF(1), NIL_OBJ, make_string(mysql_error(db))) );
      RETURN0();
    } else if( data[field] ) {
      REG0 = bvec_alloc( length[field] + 1, string_class );
      memcpy( PTR_TO_DATAPTR(REG0), data[field], length[field] );
    } else {
      REG0 = FALSE_OBJ;
    }
    RETURN1();
  }
})

(define-mysql-glue (mysql-field (result <mysql-result>)
                                (field <raw-int>))
  literals: ((& <mysql-result>)
	     (& <mysql-error>))
{
  if( field < 0 || field >= mysql_num_fields(result) ) {
    raise_error(make2(TLREF(1), NIL_OBJ,
                make_string("illegal field number")) );
    RETURN0();
  } else {
    MYSQL_FIELD *f = mysql_fetch_field_direct(result, field);
    if( f ) {
      REG0 = make_string(f->name);
    } else {
      MYSQL *db = (MYSQL *)OBJ_TO_RAW_PTR( gvec_ref( raw_result, SLOT(1) ) );
      raise_error(make2(TLREF(1), NIL_OBJ, make_string(mysql_error(db))) );
      RETURN0();
    }
    RETURN1();
  }
})

;; (mysql_fetch_field((MYSQL_RES *)res))->name;
;; mysql_affected_rows((MYSQL *)db);

(define-mysql-glue (mysql-escape-string (db <mysql-connection>)
                                        (str <raw-string>))
{
  #define BUFLEN 500

  unsigned int len, orig=string_length(raw_str);
  char buf[2*BUFLEN], *tmp;

  tmp = (orig < BUFLEN) ? buf : (char *)malloc((orig<<1)+1);
  len = mysql_real_escape_string(db, tmp, str, orig);
  REG0 = bvec_alloc( len+1, string_class );
  memcpy( PTR_TO_DATAPTR(REG0), tmp, len );
  if(orig >= BUFLEN) {  free(tmp); }
  #undef BUFLEN
  RETURN1();
})

(define-method sql-close ((self <mysql-connection>))
  (mysql-close self))

(define-method sql-with-tupels ((self <mysql-connection>)
                                (query <string>)
                                (proc <function>))
  (mysql-with-tupels self query proc))

(define-method sql-value ((self <mysql-result>) row field)
  (mysql-value self row field))

(define-method sql-field ((self <mysql-result>) field)
  (mysql-field self field))

(set! *the-db-drivers* `(("MySQL" . ,mysql-connect) . ,*the-db-drivers*))

(define-method sql-with-tupels%-fold-left ((db  <mysql-connection>)
                                           (query <string>)
                                           (setup-seeds <function>)
                                           (fold-function <function>)
                                           seed . seeds)
  (one-shot-sql-tupels%-fold-left db query fold-function (cons seed seeds)))
