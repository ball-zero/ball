(define-macro (define-mysql-glue args . body)
  `(define-safe-glue ,args
     type-handler: (<mysql-connection>
		    (direct-instance? <mysql-connection>)
		    ("MYSQL *~a"
		     "(MYSQL *)OBJ_TO_RAW_PTR(gvec_ref(~a,SLOT(1)))"))
     type-handler: (<mysql-result>
		    (class-eq? <mysql-result>)
		    ("MYSQL_RES *~a"
		     "(MYSQL_RES *)OBJ_TO_RAW_PTR(gvec_ref(~a,SLOT(0)))"))
     type-handler: (<mysql-raw>
		    (class-eq? <mysql-raw>)
		    ("MYSQL_ROW *~a"
		     "(MYSQL_RAW *)OBJ_TO_RAW_PTR(gvec_ref(~a,SLOT(0)))"))
     properties: ((other-h-files "<mysql.h>")
		  (other-h-files "<string.h>" "<stdlib.h>")
		  (other-libs "mysqlclient" "z"))
     ,@body))
