(define-class <mysql-connection> (<sql-connection>)
  (database type: <string>)
  raw-connection)

(define-class <mysql-error> (<sql-error>)
  (message type: <string>))

(define-class <mysql-result> (<sql-result>)
  raw-mysql-result
  connection)
