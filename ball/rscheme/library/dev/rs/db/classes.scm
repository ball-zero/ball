;; (C) 2003 J�rg F. Wittenberger -*-Scheme-*-

(define-class <sql-connection> (<object>) :abstract)

(define-class <sql-error> (<error>)
  (message type: <string>))

(define-class <sql-result> (<object>) :abstract)
