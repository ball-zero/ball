(define-sqlite-glue (external-interrupt-filedes)
{
 extern int external_interrupt_filedes();
 REG0 = int2fx(external_interrupt_filedes());
 RETURN1();
})

(define-sqlite-glue (interrupt-callback)
{
 extern obj external_interrupt_callback();
 REG0 = external_interrupt_callback();
 RETURN1();
})

(define-sqlite-glue (interrupt-result)
{
 extern obj external_interrupt_result();
 REG0 = external_interrupt_result();
 RETURN1();
})

(define-sqlite-glue (clear-external-interrupt!)
{
 extern void clear_external_interrupt();
 clear_external_interrupt();
 RETURN0();
})

(define (external-wait)
  (let ((fd (external-interrupt-filedes)))
    (or (eqv? fd 0)
	(thread-start!
	 (make-thread
	  (lambda ()
	    (let ((p (filedes->input-port fd #f)))
	      (do ((c (read-char p) (read-char p)))
		  ((eof-object? c)
		   (display "thread external-wait died\n"
			    (current-error-port)))
		(handler-case
		 (let ((cb (interrupt-callback)))
		   (if cb
		       (let ((r (interrupt-result)))
			 (clear-external-interrupt!)
			 (cb r))))
		 ((<condition> condition: err) #f)))))
	  "external-wait")))))

(define sqlite3server #f)

(define callback-server #f)

(define (start-callback-thread!)
  (set! callback-server
	(let ((mbox (make-mailbox "callback-server")))
	  (thread-start!
	   (make-thread
	    (lambda ()
	      (let loop ()
		(letrec ((thread (make-thread
				  (lambda () (callback-wrapper arg))
				  "callback-request"))
			 (arg #f))
		  (set! arg (receive-message! mbox))
		  (thread-start! thread))
		(loop)))
	    "callback-root"))
	  mbox)))

(define-class <sqlite3-error> (<condition>)
  code
  args)

(define-method display-object ((self <sqlite3-error>) port)
  (format port "SQLITE3:~a: ~s\n"
          (sqlite3-strerror (code self))
          (args self)))

(define-method write-object ((self <sqlite3-error>) port)
  (display-object self port))

(define-class <sqlite3-database> (<object>)
  raw-pointer				; C glue expects it at slot 0
  open-statements
  prep-cache
  callout                               ; first one not access in C glue
  mbox
  name
  callback-args
  callback-ref)

(define-class <sqlite3-statement> (<object>)
  raw-pointer				; C glue expects it at slot 0
  (container type: <sqlite3-database>)
  (name type: <abstract-string>))

(define-sqlite-glue (sqlite3-strerror (rc <raw-int>))
  literals: ('#(SQLITE_OTHER
                SQLITE_OK
                SQLITE_ERROR
                SQLITE_INTERNAL
                SQLITE_PERM
                SQLITE_ABORT
                SQLITE_BUSY
                SQLITE_LOCKED
                SQLITE_NOMEM
                SQLITE_READONLY
                SQLITE_INTERRUPT
                SQLITE_IOERR
                SQLITE_CORRUPT
                SQLITE_NOTFOUND
                SQLITE_FULL
                SQLITE_CANTOPEN
                SQLITE_PROTOCOL
                SQLITE_EMPTY
                SQLITE_SCHEMA
                SQLITE_TOOBIG
                SQLITE_CONSTRAINT
                SQLITE_MISMATCH
                SQLITE_MISUSE
                SQLITE_NOLFS
                SQLITE_AUTH
                SQLITE_ROW
                SQLITE_DONE))
{
  int k;

  switch (rc) {
    case SQLITE_OK:             k =  1; break;
    case SQLITE_ERROR:          k =  2; break;
    case SQLITE_INTERNAL:       k =  3; break;
    case SQLITE_PERM:           k =  4; break;
    case SQLITE_ABORT:          k =  5; break;
    case SQLITE_BUSY:           k =  6; break;
    case SQLITE_LOCKED:         k =  7; break;
    case SQLITE_NOMEM:          k =  8; break;
    case SQLITE_READONLY:       k =  9; break;
    case SQLITE_INTERRUPT:      k = 10; break;
    case SQLITE_IOERR:          k = 11; break;
    case SQLITE_CORRUPT:        k = 12; break;
    case SQLITE_NOTFOUND:       k = 13; break;
    case SQLITE_FULL:           k = 14; break;
    case SQLITE_CANTOPEN:       k = 15; break;
    case SQLITE_PROTOCOL:       k = 16; break;
    case SQLITE_EMPTY:          k = 17; break;
    case SQLITE_SCHEMA:         k = 18; break;
    case SQLITE_TOOBIG:         k = 19; break;
    case SQLITE_CONSTRAINT:     k = 20; break;
    case SQLITE_MISMATCH:       k = 21; break;
    case SQLITE_MISUSE:         k = 22; break;
    case SQLITE_NOLFS:          k = 23; break;
    case SQLITE_AUTH:           k = 24; break;
    case SQLITE_ROW:            k = 25; break;
    case SQLITE_DONE:           k = 26; break;
    default:                    k =  0; break;
  }
  REG0 = gvec_ref( LITERAL(0), SLOT(k) );
  RETURN1();
})

(define (sqlite3-error? obj) (instance? obj <sqlite3-error>))

(define (sqlite3-error-db-locked? obj)
  (and (instance? obj <sqlite3-error>)
       (eq? (sqlite3-strerror (code obj)) 'SQLITE_BUSY)))

(define-sqlite-glue (sqlite3-start-fn s (fn <raw-int>) (c <function>))
{
  typedef int (*askemos_request_function_t)(void *);
  extern void
  start_asynchronous_request(askemos_request_function_t function,
		             void *data, obj callback);
  extern int pthread_sqlite3_open(void *data);
  extern int pthread_sqlite3_prepare(void *data);
  extern int pthread_sqlite3_step(void *data);
  extern int pthread_sqlite3_close(void *data);
  askemos_request_function_t task = NULL;
  switch (fn) {
    case 1: task = pthread_sqlite3_open; break;
    case 2: task = pthread_sqlite3_prepare; break;
    case 3: task = pthread_sqlite3_step; break;
    case 4: task = pthread_sqlite3_close; break;
    default: task = NULL;
  }
  if(task) start_asynchronous_request(task, (void *) s, c);
  REG0 = task ? TRUE_OBJ : FALSE_OBJ;
  RETURN1();
})

(define pthread-pool-load (make-semaphore 'pthread-pool-load 40))

(define (make-sqlite3-callout)
  (let ((mux (make-semaphore 'sqlite3 0))
	(result #f))
    (let ((cb (lambda (x)
		(set! result x)
		(semaphore-signal pthread-pool-load)
		(semaphore-signal mux))))
      (lambda (param fn)
	(semaphore-wait pthread-pool-load)
	(let ((started (sqlite3-start-fn param fn cb)))
	  (if started
	      (values (begin (semaphore-wait mux) result) param)
	      (error "make-sqlite3-callout: unknown function code ~a" fn)))))))

(define-macro (sqlite3-run-fn co param fn) (list co param fn))

(define-sqlite-glue (open-args (arg_dbn <raw-string>) (arg_setup <raw-int>) arg_vfs arg_sm)
{
  struct open_args *a;
  REG0 = bvec_alloc(sizeof(struct open_args), byte_vector_class);
  a = PTR_TO_DATAPTR(REG0);
  a->dbn = arg_dbn;
  a->setup = arg_setup;
  a->vfs = arg_vfs;
  a->sm = arg_sm;
  RETURN1();
})

(define-sqlite-glue (extract-open-result (param <byte-vector>))
{
  struct open_args *a = PTR_TO_DATAPTR(param);
  REG0 = RAW_PTR_TO_OBJ(a->cnx);
  RETURN1();
})

(define (sqlite3-open* dbn setup co vfs sm)
  (or sqlite3server (set! sqlite3server (external-wait)))
  (and vfs (not callback-server) (start-callback-thread!))
  (call-with-values
      (lambda ()
	(sqlite3-run-fn co (open-args dbn setup vfs sm) 1))
    (lambda (result param)
      (if (eq? (sqlite3-strerror result) 'SQLITE_OK)
	  (extract-open-result param)
	  (begin
	    (if sm (free-callback-args sm))
	    (error (make <sqlite3-error> code: result args: dbn)))))))

(define-sqlite-glue (alloc-callback (size <raw-int>) (ref <object>))
{
  struct callback_args *a;
  REG0=bvec_alloc(sizeof(struct callback_args)+size, byte_vector_class);
  a = PTR_TO_DATAPTR(REG0);
  pthread_mutex_init(&a->mux, NULL);  
  pthread_cond_init(&a->cond, NULL);  
  a->op = 0;
  a->ref = ref;
  a->buf = NULL;
  a->size = size;
  RETURN1();
})

(define-sqlite-glue (free-callback-args (cbb <byte-vector>))
{
  struct callback_args *a = PTR_TO_DATAPTR(cbb);
  /* if: we want to be sure that the other side is done with the callback */
  /* this *seems* to be helpful, no rationale here */
/*
  pthread_mutex_lock(&a->mux);
  while(CB_IS_BUSY(a->op)) pthread_cond_wait(&a->cond, &a->mux);
  pthread_mutex_unlock(&a->mux);
*/
  /* normally we would do just this */
  pthread_mutex_destroy(&a->mux);
  pthread_cond_destroy(&a->cond);
  REG0 = FALSE_OBJ;
  RETURN1();
})

(define-sqlite-glue (sqlite3-extract-callback-arguments (cbb <byte-vector>))
{
  INT_64 offset_64;
  struct callback_args *a = PTR_TO_DATAPTR(cbb);
  pthread_mutex_lock(&a->mux);
  while(!CB_IS_CALL(a->op)) pthread_cond_wait(&a->cond, &a->mux);
  REG0 = a->ref;
  REG1 = int2fx(CB_GET_OP(a->op));
  REG2 = RAW_PTR_TO_OBJ(a->buf);
  REG3 = int2fx(a->size);
  REG4 = int2fx(a->amount);
#ifdef HAVE_INT_64
  REG5 = int_64_compact( a->offset );
#else
  offset_64.digits[0] = a->offset >> 48;
  offset_64.digits[1] = a->offset >> 32;
  offset_64.digits[2] = a->offset >> 16;
  offset_64.digits[3] = a->offset;
  REG5 = int_64_compact( offset_64 );
#endif
  RETURN(6);
})

(define-sqlite-glue (sqlite3-set-callback-return! (cbb <byte-vector>) (r <raw-int>) (v <raw-int>))
{
  INT_64 offset_64;
  struct callback_args *a = PTR_TO_DATAPTR(cbb);
  switch(r) {
   case 0: a->op = CB_M_RETURN_OP(SQLITE_OK); break;
   case 1: a->op = CB_M_RETURN_OP(SQLITE_ERROR); break;
   case 2: a->op = CB_M_RETURN_OP(SQLITE_IOERR_SHORT_READ); break;
   case 10: a->op = CB_M_RETURN_OP(SQLITE_IOERR); break;
  }
  a->offset = v;
  RETURN0();
})

(define-sqlite-glue (sqlite3-callback-completed (cbb <byte-vector>))
{
  struct callback_args *a = PTR_TO_DATAPTR(cbb);
  pthread_mutex_unlock(&a->mux);
  REG0 = RAWINT_TO_FIXNUM(pthread_cond_signal(&a->cond));
  RETURN1();
})



(define (make-callback-interface obj)
  (alloc-callback 0 obj))

(define (sqlite3-signal-condition* cbb)
  (sqlite3-signal-condition* (vector-ref cbb 1)))

(define (cleanup-mbox db)
  (let ((mb (mbox db))
	(res (cons #f (make <sqlite3-error> code: 10 ;; SQLITE_INTERRUPT
			    args: (name db)))))
    (set-mbox! db #f)
    (let loop ()
      (or (dequeue-empty? mb)
	  (begin
	    (send-message! (car (receive-message! mb)) res)
	    (loop))))))

(define (sqlite3-serve db)
  (let loop ()
    (let ((request (receive-message! (mbox db))))
      (if
       (handler-case
	(case (cadr request)
	  ((exec)
	   (send-message!
	    (car request)
	    (cons #t (sqlite3-exec! db (caddr request) (cdddr request))))
	   #t)
	  ((proc)
	   (send-message!
	    (car request)
	    (cons #t ((cddr request) (lambda (stmt . args) (sqlite3-exec! db stmt args)))))
	   #t)
	  ((close)
	   (send-message! (car request) (cons #t (sqlite3-close! db)))
	   #f)
	  (else (error "sqlite3-serve illegal request ~a\n" request)))
	((<condition> condition: err)
	 (send-message! (car request) (cons #f err))
	 (not (eq? (cadr request) 'close))))
       (loop)
       (cleanup-mbox db)))))

(define (sqlite3-wrap-server db)
  (thread-start! (make-thread (lambda () (sqlite3-serve db)) "sqlite3"))
  db)

(define (sqlite3-call db op args)
  (let ((rmb (make-mailbox (symbol->string op))))
    (send-message! (mbox db) (cons rmb (cons op args)))
    (let ((reply (receive-message! rmb)))
      (if (car reply) (cdr reply) (error (cdr reply))))))

(define (sqlite3-open (dbn <string>))
  (define co (make-sqlite3-callout))
  (sqlite3-wrap-server
   (make <sqlite3-database>
     name: dbn
     raw-pointer: (sqlite3-open* dbn 1 co #f #f)
     mbox: (make-mailbox dbn)
     callout: co
     callback-args: #f
     callback-ref: #f
     open-statements: (make-object-table)
     prep-cache: (make-string-table))))

(define (sqlite3-open-restricted (dbn <string>) . vfs)
  (define co (make-sqlite3-callout))
  (if (pair? vfs)
      (let ((sm (make-callback-interface (cadr vfs))))
	(sqlite3-wrap-server
	 (make <sqlite3-database>
	   name: dbn
	   raw-pointer: (sqlite3-open* dbn 2 co (car vfs) sm)
	   mbox: (make-mailbox dbn)
	   callout: co
	   callback-args: sm
	   callback-ref: (cadr vfs)
	   open-statements: (make-object-table)
	   prep-cache: (make-string-table))))
      (sqlite3-wrap-server
       (make <sqlite3-database>
	 name: dbn
	 raw-pointer: (sqlite3-open* dbn 2 co #f #f)
	 mbox: (make-mailbox dbn)
	 callout: co
	 callback-args: #f
	 callback-ref: #f
	 open-statements: (make-object-table)
	 prep-cache: (make-string-table)))))

(define (sqlite3-open-restricted-ro (dbn <string>) . vfs)
  (define co (make-sqlite3-callout))
  (if (pair? vfs)
      (let ((sm (make-callback-interface (cadr vfs))))
	(sqlite3-wrap-server
	 (make <sqlite3-database>
	   name: dbn
	   raw-pointer: (sqlite3-open* dbn 3 co (car vfs) sm)
	   mbox: (make-mailbox dbn)
	   callout: co
	   callback-args: sm
	   callback-ref: (cadr vfs)
	   open-statements: (make-object-table)
	   prep-cache: (make-string-table))))
      (sqlite3-wrap-server
       (make <sqlite3-database>
	 name: dbn
	 raw-pointer: (sqlite3-open* dbn 3 co #f #f)
	 mbox: (make-mailbox dbn)
	 callout: co
	 callback-args: #f
	 callback-ref: #f
	 open-statements: (make-object-table)
	 prep-cache: (make-string-table)))))

(define-sqlite-glue (sqlite3-bugworkaround-reset-restrictions (db <sqlite3-database>))
{
 extern void sqlite3_set_authorizer_unrestricted(sqlite3 *cnx);
 sqlite3_set_authorizer_unrestricted(db);
 RETURN0();
})

(define (sqlite3-close! (db <sqlite3-database>))
  (let ((raw (raw-pointer db)))
    (call-with-values
	(lambda ()
	  (set-raw-pointer! db #f)
	  (sqlite3-run-fn (callout db) raw 4))
      (lambda (rc raw)
	(if (callback-args db)
	    (set-callback-args! db (free-callback-args (callback-args db))))
	(if (not (eq? (sqlite3-strerror rc) 'SQLITE_OK))
	    (sqlite3-raise-db-error rc db))))))

(define (sqlite3-close (db <sqlite3-database>))
  (sqlite3-call db 'close '()))

#|
(define-sqlite-glue (sqlite3-close.old (db <sqlite3-database>))
  literals: ((& <sqlite3-error>))
{
  int rc;
  rc = sqlite3_close( db );
  if (rc != SQLITE_OK) {
    obj eo = make3( TLREF(0),
                    NIL_OBJ, 
                    int2fx( rc ),
                    make_string( sqlite3_errmsg( db ) ) );
    raise_error( eo );
  }
  RETURN0();
})
|#

(define-sqlite-glue (sqlite3-changes (db <sqlite3-database>))
{
  REG0 = int2fx( sqlite3_changes( db ) );
  RETURN1();
})

(define-sqlite-glue (sqlite3-reset (s <sqlite3-statement>))
  literals: ((& <sqlite3-error>))
{
  int rc;

  rc = sqlite3_reset( s );
  if (rc != SQLITE_OK) {
    sqlite3 *db = sqlite3_db_handle( s );
    obj eo = make3( TLREF(0), 
                    NIL_OBJ, 
                    int2fx( rc ),
                    make_string( sqlite3_errmsg( db ) ) );
    raise_error( eo );
  }
  RETURN0();
})

(define-sqlite-glue (sqlite3-finalize (s <sqlite3-statement>))
  literals: ((& <sqlite3-error>))
{
  int rc;
  obj stmt_tbl, prep_tbl;
  obj k;

  rc = sqlite3_finalize( s );
  if (rc != SQLITE_OK) {
    sqlite3 *db = sqlite3_db_handle( s );
    obj eo = make3( TLREF(0), 
                    NIL_OBJ, 
                    int2fx( rc ),
                    make_string( sqlite3_errmsg( db ) ) );
    raise_error( eo );
  }

  stmt_tbl = gvec_ref( gvec_ref( raw_s, SLOT(1) ), SLOT(1) );
  prep_tbl = gvec_ref( gvec_ref( raw_s, SLOT(1) ), SLOT(2) );

  objecttable_remove( stmt_tbl, obj_hash( raw_s ), raw_s );
  k = gvec_ref( raw_s, SLOT(2) );
/*
  objecttable_remove( prep_tbl, hash_string( k ), k );
*/
  RETURN0();
})

(define-sqlite-glue (sqlite3-prepare* (db <sqlite3-database>) 
                                     (sql <string>)
                                     (offset <raw-int>))
  literals: ((& <sqlite3-statement>)
             (& <sqlite3-error>)
             (& <substring>))
{
  sqlite3_stmt *stmt;
  int rc;
  obj x;
  const char *tail;
  int n;

  rc = sqlite3_prepare_v2( db,
			   string_text( sql ) + offset,
			   string_length( sql ) - offset,
			   &stmt,
			   &tail );

  if (rc != SQLITE_OK) {
    obj eo = make3( TLREF(1), NIL_OBJ, int2fx( rc ), 
                    cons( make_string( sqlite3_errmsg( db ) ),
                          cons( raw_offset, 
                                cons( sql, NIL_OBJ ) ) ) );
    raise_error( eo );
  }

  if (stmt == NULL) {
    REG0 = FALSE_OBJ;
    REG1 = FALSE_OBJ;
    RETURN(2);
  } else {
    obj tbl;
    obj text;

    n = tail - string_text( sql );

    if ((offset == 0) && (n == string_length( sql ))) {
      text = sql;
    } else {
      obj text_len = int2fx( n - offset );
      text = make4( TLREF(2), sql, raw_offset, text_len, text_len );
    }

    x = make3( TLREF(0), RAW_PTR_TO_OBJ( stmt ), raw_db, text );

    tbl = gvec_ref( raw_db, SLOT(1) );
    objecttable_insert( tbl, obj_hash(x), x, x );

    REG0 = x;
    REG1 = int2fx( n );
    RETURN(2);
  }
})

(define-sqlite-glue (prepare-args (db <sqlite3-database>)
				  (sql <raw-string>) (sqllen <raw-int>)
				  (offset <raw-int>))
{
  struct prepare_args *a;
  REG0 = bvec_alloc(sizeof(struct prepare_args), byte_vector_class);
  a = PTR_TO_DATAPTR(REG0);
  a->stmt = NULL;
  a->db = db;
  a->sql = sql;
  a->sql_len = sqllen;
  a->offset = offset;
  RETURN1();
})

(define-sqlite-glue (extract-prepare-result (param <byte-vector>))
{
  struct prepare_args *a = PTR_TO_DATAPTR(param);
  REG0 = a->stmt != NULL ? RAW_PTR_TO_OBJ(a->stmt) : FALSE_OBJ;
  REG1 = int2fx(a->tail);
  RETURN(2);
})


(define-sqlite-glue (sqlite3-raise-db-error (rc <raw-int>) (db <sqlite3-database>))
  literals: ((& <sqlite3-error>))
{
  obj eo = make3( TLREF(0), NIL_OBJ, int2fx( rc ), 
                  make_string( sqlite3_errmsg( db ) ) );
  raise_error( eo );
  RETURN0();
})

(define-sqlite-glue (sqlite3-raise-error (s <sqlite3-statement>) (rc <raw-int>))
  literals: ((& <sqlite3-error>))
{
  sqlite3 *db = sqlite3_db_handle( s );
  obj eo = make3( TLREF(0), NIL_OBJ, int2fx(rc),
                  make_string( sqlite3_errmsg( db ) ) );
  sqlite3_finalize(s);
  raise_error( eo );
  RETURN0();
})

(define (sqlite3-raise-closed db)
  (error (make <sqlite3-error> code: 10 ;; SQLITE_INTERRUPT
	       args: (name db))))

(define (sqlite3-prepare (db <sqlite3-database>) (sql <string>) (offset <integer>))
  (let ((param (prepare-args db sql (string-length sql) offset)))
    (call-with-values
	(lambda ()
	  (sqlite3-run-fn (callout db) param 2))
      (lambda (result param)
	(if (eq? (sqlite3-strerror result) 'SQLITE_OK)
	    (call-with-values (lambda () (extract-prepare-result param))
	      (lambda (rp n)
		(if rp
		    (let* ((stmt-sql (if (and (eqv? offset 0) (eqv? n (string-length sql)))
					 sql (substring sql offset n)))
			   (stmt (make <sqlite3-statement>
				   raw-pointer: rp container: db
				   name: stmt-sql)))
		      (table-insert! (open-statements db) stmt stmt)
		      (values stmt n))
		    (values #f #f))))
	    (error (make <sqlite3-error> code: result args: (cons offset sql))))))))

(define-sqlite-glue (sqlite3-step (s <sqlite3-statement>))
  literals: ((& <sqlite3-error>))
{
  int rc;

  rc = sqlite3_step( s );
  if (rc == SQLITE_ROW) {
    REG0 = TRUE_OBJ;
    RETURN1();
  } else if (rc != SQLITE_DONE) {
    sqlite3 *db = sqlite3_db_handle( s );
    obj eo = make3( TLREF(0), NIL_OBJ, int2fx( rc ), 
                    make_string( sqlite3_errmsg( db ) ) );
    raise_error( eo );
    RETURN0();
  } else {
    RETURN0();
  }
})

(define-sqlite-glue (sqlite3-bind (s <sqlite3-statement>) #rest r)
  literals: ((& <sqlite3-error>)
             "Wrong number of arguments"
             "Can't handle argument type for binding") 
{
  int i;
  extern int rs_to_sq3( sqlite3_stmt *s, int i, int copy_p, obj sv );

  if (arg_count_reg != (1+sqlite3_bind_parameter_count( s ))) {
    obj eo;
    eo = make3( TLREF(0), NIL_OBJ, int2fx( SQLITE_ERROR ),
                cons( LITERAL(1), 
                      cons( int2fx( arg_count_reg-1 ),
                            cons( int2fx( sqlite3_bind_parameter_count( s ) ),
                                  NIL_OBJ ) ) ) );
    raise_error( eo );
  }

  for (i=1; i<arg_count_reg; i++) {
    int rc;

    /* interestingly, the first BIND parameter in sqlite3 has index 1,
       even though the first COLUMN result has index 0 */

    rc = rs_to_sq3( s, i, 0, reg_ref( i ) );
    if (rc != SQLITE_OK) {
      obj eo;
      sqlite3 *db = sqlite3_db_handle( s );

      eo = make3( TLREF(0), NIL_OBJ, int2fx( SQLITE_ERROR ),
                  cons( (rc == -1) 
                        ? LITERAL(1) 
                        : make_string( sqlite3_errmsg( db ) ),
                        cons( int2fx( i ),
                              cons( reg_ref(i), NIL_OBJ ) ) ) );
      raise_error( eo );
    }
  }
  RETURN0();
})

(define-sqlite-glue (sqlite3-row (st <sqlite3-statement>))
{
  int i, n;
  extern obj sq3_to_rs( sqlite3_stmt *s, int col );

  n = sqlite3_data_count( st );
  if (n == 0) {
    RETURN0();
  } else {
    for (i=0; i<n; i++) {
      obj sv = sq3_to_rs( st, i );
      reg_set( i, sv );
    }
    RETURN(n);
  }
})

(define-sqlite-glue (sqlite3-columns (st <sqlite3-statement>))
{
  int i, n;

  n = sqlite3_column_count( st );
  if (n == 0) {
    RETURN0();
  } else {
    for (i=0; i<n; i++) {
      obj sv = make_string( sqlite3_column_name(st, i) );
      reg_set( i, sv );
    }
    RETURN(n);
  }
})

#|
(define (sqlite3-async-step (s <sqlite3-statement>))
  (call-with-values (lambda () (sqlite3-run-fn (callout (container s)) (raw-pointer s) 3))
    (lambda (result raw)
      (let ((result (sqlite3-strerror result)))
	(case result
	  ((SQLITE_ROW) #t)
	  ((SQLITE_DONE SQLITE_OK) #f)
	  (else (sqlite3-raise-error s result)))))))
|#

(define (sqlite3-async-for-each (s <sqlite3-statement>) (fn <function>))
  (do ((exit #f))
      (exit #t)
    (let* ((rc (sqlite3-run-fn (callout (container s)) (raw-pointer s) 3))
	   (row (sqlite3-strerror rc)))
      (case row
	((SQLITE_ROW) (call-with-values (lambda () (sqlite3-row s)) fn))
	((SQLITE_DONE) (set! exit #t))
	(else (sqlite3-raise-error s rc))))))

#|
(define-sqlite-glue (sqlite3-for-each (s <sqlite3-statement>) (fn <function>))
  literals: ((& <sqlite3-error>))
{
  JUMP( 2, sq4each );
}

("sq4each"
{
  int rc;
  sqlite3_stmt *st;
  extern obj sq3_to_rs( sqlite3_stmt *s, int col );

  st = (*(sqlite3_stmt **)PTR_TO_DATAPTR(REG0));

  rc = sqlite3_step( st );
  if (rc == SQLITE_ROW) {
    int i, n;
    obj the_fn = REG1;

    SAVE_CONT2( sq4next );

    n = sqlite3_data_count( st );
    for (i=0; i<n; i++) {
      obj sv = sq3_to_rs( st, i );
      reg_set( i, sv );
    }
    APPLYF( n, the_fn );
  } else if (rc != SQLITE_DONE) {
    sqlite3 *db = sqlite3_db_handle( st );
    obj eo = make3( TLREF(0), NIL_OBJ, int2fx( rc ), 
                    make_string( sqlite3_errmsg( db ) ) );
    raise_error( eo );
    RETURN0();
  } else {
    sqlite3_reset( st );
    RETURN0();
  }
})

("sq4next"
{
  RESTORE_CONT2();
  BJUMP( 2, sq4each );
}))
|#

(define sqlite3-for-each sqlite3-async-for-each)

;;;
;;;  Return a list of lists
;;;

(define sqlite3-empty-result '#(#()))

#|
(define (sqlite3-exec (db <sqlite3-database>) stmt . args)
  (let loop ((n 0)
	     (r0 sqlite3-empty-result))
    (if (fixnum<? n (string-length stmt))
	(call-with-values (lambda () (sqlite3-prepare db stmt n))
	  (lambda (p n)
	    (if p
		(let ((r '()))
		  ;;
		  (begin
		    (apply sqlite3-bind p args)
		    ;;
		    (sqlite3-for-each
		     p
		     (lambda args
		       (set! r (cons (list->vector args) r))))
		    (set! r (list->vector
			     (cons
			      (call-with-values (lambda ()
						  (sqlite3-columns p))
				vector)
			      (reverse! r))))
		    (sqlite3-finalize p)
		    (loop n r)))
		r0)))
	r0)))
|#

(define (sqlite3-exec! (db <sqlite3-database>) stmt args)
  (let loop ((n 0)
	     (r0 sqlite3-empty-result))
    (if (fixnum<? n (string-length stmt))
	(call-with-values (lambda () (sqlite3-prepare db stmt n))
	  (lambda (p n)
	    (if p
		(let ((r '()))
		  ;;
		  (begin
		    (apply sqlite3-bind p args)
		    ;;
		    (sqlite3-async-for-each
		     p
		     (lambda args
		       (set! r (cons (list->vector args) r))))
		    (set! r (list->vector
			     (cons
			      (call-with-values (lambda ()
						  (sqlite3-columns p))
				vector)
			      (reverse! r))))
		    (sqlite3-finalize p)
		    (loop n r)))
		r0)))
	r0)))

(define (sqlite3-async-exec (db <sqlite3-database>) stmt . args)
  (sqlite3-call db 'exec (cons stmt args)))

(define sqlite3-exec sqlite3-async-exec)

(define (sqlite3-call-with-transaction db proc)
  (sqlite3-call
   db 'proc
   (lambda (sql)
     (handler-case
      (begin
	(sql "BEGIN IMMEDIATE TRANSACTION")
	(let ((r (proc sql)))
	  (sql "COMMIT")
	  r))
      ((<condition> condition: ex)
       (handler-case (sql "ROLLBACK")
		     ((<condition> condition: ex)))
       (error ex))))))

(define (sqlite3-call-test/set db test set fail fail-args)
  (or (sqlite3-call
       db 'proc
       (lambda (sql)
	 (handler-case
	  (begin
	    (sql "BEGIN IMMEDIATE TRANSACTION")
	    (if (test sql)
		(begin
		  (set sql)
		  (sql "COMMIT")
		  #t)
		(begin
		  (sql "ROLLBACK")
		  #f)))
	  ((<condition> condition: ex)
	   (handler-case (sql "ROLLBACK")
			 ((<condition> condition: ex)))
	   (error ex)))))
      (apply fail fail-args)))

(define (callback-wrapper arg)
  (call-with-values (lambda () (sqlite3-extract-callback-arguments arg))
    (lambda (ref op buf size amount offset)
      (define opaque (vector-ref ref 0))
      (handler-case
       (case op
	 ((1) (sqlite3-set-callback-return! arg 0 ((vector-ref ref 1) opaque)))
	 ((2) (sqlite3-set-callback-return! arg 0 ((vector-ref ref 2) opaque)))
	 ((3) (sqlite3-set-callback-return!
	       arg
	       (case ((vector-ref ref 3) opaque buf amount offset)
		 ((SQLITE_OK) 0)
		 ((SQLITE_IOERR_SHORT_READ) 2)
		 (else 10))
	       0))
	 ((4) ((vector-ref ref 4) opaque buf amount offset)
	  (sqlite3-set-callback-return! arg 0 0))
	 ((5) ((vector-ref ref 5) opaque offset)
	  (sqlite3-set-callback-return! arg 0 0))
	 ((6) ((vector-ref ref 6) opaque)
	  (sqlite3-set-callback-return! arg 0 0))
	 (else (error "unknown op code")))
       ((<condition> condition: ex)
	(format (current-error-port) "sqlite3 callback: opcode ~a exn ~a\n" op ex)
	(sqlite3-set-callback-return! arg 10 0)))))
  (sqlite3-callback-completed arg))

(define (callback-sender arg)
  (send-message! callback-server arg))

(define-glue (callback-vfs-init proc)
{
 extern sqlite3_vfs *callback_sqlite3_vfs_init(obj p);
 if (callback_sqlite3_vfs_init(proc) == NULL) {
   obj eo = make3( TLREF(0), 
                   NIL_OBJ, 
                   int2fx(11),
                   make_string("AskemosVFS failed to register!") );
   raise_error( eo );
 }
 RETURN0();
})

(callback-vfs-init callback-sender)

#|
(define d (sqlite3-open ":memory:"))
(sqlite3-step (sqlite3-prepare d "create table foo( x int, y char(30) );" 0))
(sqlite3-step (sqlite3-prepare d "insert into foo values( 3, 'foo' )" 0))
(sqlite3-step (sqlite3-prepare d "insert into foo values( 4, 'bar' )f" 0))

(sqlite3-for-each
 (sqlite3-prepare d "select * from foo" 0)
 (lambda (x y)
   (format #t "==> ~s = ~s\n" x y)))

(define s1 (sqlite3-prepare d "insert into foo(x,y) values( ?, ? )" 0))

(sqlite3-bind s1 33 "foofoo")
(sqlite3-step s1)



|#
