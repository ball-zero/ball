#include <string.h>
#include <rscheme.h>
#include <pthread.h>
#include "sqlite/sqlite3.h"

#define MIN(x,y) ((x)<(y)?(x):(y))
#define MAX(x,y) ((x)>(y)?(x):(y))


int rs_to_sq3( sqlite3_stmt *s, int i, int copy_p, obj sv )
{
  if (FIXNUM_P( sv )) {
    return sqlite3_bind_int( s, i, fx2int( sv ) );
  } else if (LONGFLOAT_P( sv )) {
    return sqlite3_bind_double( s, i, extract_float( sv ) );
  } else if (LONG_INT_P( sv )) {
    long long int dst;
    INT_64 src = extract_int_64( sv );

    dst = ((unsigned long long)src.digits[0] << 48)
      + ((unsigned long long)src.digits[1] << 32)
      + ((unsigned long long)src.digits[2] << 16)
      + ((unsigned long long)src.digits[3] << 0);
    return sqlite3_bind_int64( s, i, dst );
  } else if (STRING_P( sv )) {
    return sqlite3_bind_text( s, i, 
                              string_text( sv ), string_length( sv ),
                              SQLITE_TRANSIENT );
  } else if (SYMBOL_P( sv )) {

    /* avoid the copy */

    sv = symbol_str( sv );
    return sqlite3_bind_text( s, i,
                              string_text( sv ), string_length( sv ),
                              SQLITE_STATIC );
  } else if (BVEC_P( sv )) {
    return sqlite3_bind_blob( s, i,
                              PTR_TO_DATAPTR( sv ), SIZEOF_PTR( sv ),
                              SQLITE_TRANSIENT );
  } else if (EQ( sv, FALSE_OBJ )) {
    return sqlite3_bind_null( s, i );
  } else if (PAIR_P( sv )) {
    obj type_as = pair_car( sv );
    obj data_is = pair_cdr( sv );

    /* the only retyping cases we allow are to treat a bvec as TEXT
       and a string as a BLOB */
    switch (fx2int( type_as )) {
    case 0:
      /* treat a bvec argument as TEXT */
      if (BVEC_P( data_is )) {
        return sqlite3_bind_text( s, i,
                                  PTR_TO_DATAPTR( data_is ), 
                                  SIZEOF_PTR( data_is ),
                                  SQLITE_TRANSIENT );
      }
      return -1;

    case 1:
      /* treat a string argument as a BLOB */
      if (STRING_P( data_is )) {
        return sqlite3_bind_blob( s, i,
                                  string_text( data_is ), 
                                  string_length( data_is ),
                                  SQLITE_TRANSIENT );
      }
      return -1;
      
    default:
      return -1;
    }
  } else {
    return -1;
  }
}

obj sq3_to_rs( sqlite3_stmt *s, int i )
{
  obj sv;

  switch (sqlite3_column_type( s, i )) {

  case SQLITE_INTEGER:
    {
      long long x = sqlite3_column_int64( s, i );
#ifdef HAVE_INT_64
      sv = int_64_compact( x );
#else
      INT_64 u;
      u.digits[0] = x >> 48;
      u.digits[1] = x >> 32;
      u.digits[2] = x >> 16;
      u.digits[3] = x >> 0;
      sv = int_64_compact( u );
#endif
      break;
    }

  case SQLITE_FLOAT:
    {
      sv = make_float( sqlite3_column_double( s, i ) );
      break;
    }

  case SQLITE_NULL:
    {
      sv = FALSE_OBJ;
      break;
    }

  default:
    /* this should not happen... */
    sv = make_string( (const char *)sqlite3_column_text( s, i ) );
    break;

  case SQLITE_TEXT:
    {
      void *src = sqlite3_column_blob( s, i );
      unsigned n = sqlite3_column_bytes( s, i );
      sv = bvec_alloc( n+1, string_class );
      if(n>0) memcpy( PTR_TO_DATAPTR( sv ), src, n );
      break;
    }

  case SQLITE_BLOB:
    {
      void *src = sqlite3_column_blob( s, i );
      unsigned n = sqlite3_column_bytes( s, i );
      sv = bvec_alloc( n+1, string_class );
      if(n>0) memcpy( PTR_TO_DATAPTR( sv ), src, n );
      break;
    }
  }
  return sv;
}

/* ** AUTHORIZATION HANDLING ** */

static int
sq_sqlite3_auth_restricted(void* userdata, int opcode,
			   const char* arg1, const char* arg2,
			   const char* dbname, const char* trigger)
{
  switch(opcode) {
  case SQLITE_CREATE_INDEX:	/* Index Name      Table Name      */
  case SQLITE_REINDEX:
  case SQLITE_CREATE_TABLE:	/* Table Name      NULL            */
  case SQLITE_CREATE_VTABLE:    /* Table Name      Module Name     */
  case SQLITE_ALTER_TABLE:      /* Database Name   Table Name      */
  case SQLITE_CREATE_TEMP_INDEX: /* Index Name      Table Name     */
  case SQLITE_CREATE_TEMP_TABLE: /* Table Name      NULL           */
  case SQLITE_CREATE_TEMP_TRIGGER: /* Trigger Name    Table Name   */
  case SQLITE_CREATE_TEMP_VIEW:	/* View Name       NULL            */
  case SQLITE_CREATE_TRIGGER:	/* Trigger Name    Table Name      */
  case SQLITE_CREATE_VIEW:	/* View Name       NULL            */
  case SQLITE_DELETE:		/* Table Name      NULL            */
  case SQLITE_DROP_INDEX:	/* Index Name      Table Name      */
  case SQLITE_DROP_TABLE:	/* Table Name      NULL            */
  case SQLITE_DROP_VTABLE:      /* Table Name      Module Name     */
  case SQLITE_DROP_TEMP_INDEX:	/* Index Name      Table Name      */
  case SQLITE_DROP_TEMP_TABLE:	/* Table Name      NULL            */
  case SQLITE_DROP_TEMP_TRIGGER: /* Trigger Name    Table Name     */
  case SQLITE_DROP_TEMP_VIEW:   /* View Name       NULL            */
  case SQLITE_DROP_TRIGGER:	/* Trigger Name    Table Name      */
  case SQLITE_DROP_VIEW:	/* View Name       NULL            */
  case SQLITE_INSERT:		/* Table Name      NULL            */
  case SQLITE_PRAGMA:		/* Pragma Name     1st arg or NULL */
  case SQLITE_READ:		/* Table Name      Column Name     */
  case SQLITE_SELECT:		/* NULL            NULL            */
#if SQLITE_VERSION_NUMBER > 3003007
  case SQLITE_FUNCTION:		/* Function Name   NULL            */
#endif
  case SQLITE_TRANSACTION:	/* NULL            NULL            */
  case SQLITE_UPDATE:		/* Table Name      Column Name     */
  case SQLITE_ANALYZE:          /* Table Name      NULL            */
    return SQLITE_OK;
  case SQLITE_ATTACH:		/* Filename        NULL            */
  case SQLITE_DETACH:		/* Database Name   NULL            */
  default:
fprintf(stderr, "auth_restricted deny %d\n", opcode);
    return SQLITE_DENY;
  }
}

/* KLUDGE: FIXME: we need to know what temp tables are, not code them hard */

static int is_temporary_table(void *userdata, const char *table)
{
 if (strcmp(table, "sqlite_temp_master") == 0) return 1;
 if (strcmp(table, "current_message") == 0) return 1;
 return 0;
}

static int
sq_sqlite3_auth_restricted_ro(void* userdata, int opcode,
			      const char* arg1, const char* arg2,
			      const char* dbname, const char* trigger)
{
  switch(opcode) {
  case SQLITE_CREATE_INDEX:	/* Index Name      Table Name      */
  case SQLITE_CREATE_TABLE:	/* Table Name      NULL            */
  case SQLITE_ALTER_TABLE:      /* Database Name   Table Name      */
    return SQLITE_DENY;
  case SQLITE_CREATE_TEMP_INDEX: /* Index Name      Table Name     */
  case SQLITE_CREATE_TEMP_TABLE: /* Table Name      NULL           */
  case SQLITE_CREATE_TEMP_TRIGGER: /* Trigger Name    Table Name   */
  case SQLITE_CREATE_TEMP_VIEW:	/* View Name       NULL            */
    return SQLITE_OK;
  case SQLITE_CREATE_TRIGGER:	/* Trigger Name    Table Name      */
  case SQLITE_CREATE_VIEW:	/* View Name       NULL            */
  case SQLITE_DELETE:		/* Table Name      NULL            */
  case SQLITE_DROP_INDEX:	/* Index Name      Table Name      */
  case SQLITE_DROP_TABLE:	/* Table Name      NULL            */
    return SQLITE_DENY;
  case SQLITE_DROP_TEMP_INDEX:	/* Index Name      Table Name      */
  case SQLITE_DROP_TEMP_TABLE:	/* Table Name      NULL            */
  case SQLITE_DROP_TEMP_TRIGGER: /* Trigger Name    Table Name     */
  case SQLITE_DROP_TEMP_VIEW:   /* View Name       NULL            */
    return SQLITE_OK;
  case SQLITE_DROP_TRIGGER:	/* Trigger Name    Table Name      */
  case SQLITE_DROP_VIEW:	/* View Name       NULL            */
    return SQLITE_DENY;
  case SQLITE_INSERT:		/* Table Name      NULL            */
    if (is_temporary_table(userdata,arg1))
    return SQLITE_OK;
    return SQLITE_DENY;
  case SQLITE_PRAGMA:		/* Pragma Name     1st arg or NULL */
    return SQLITE_DENY;
  case SQLITE_READ:		/* Table Name      Column Name     */
  case SQLITE_SELECT:		/* NULL            NULL            */
#if SQLITE_VERSION_NUMBER > 3003007
  case SQLITE_FUNCTION:		/* Function Name   NULL            */
#endif
    return SQLITE_OK;
  case SQLITE_TRANSACTION:	/* NULL            NULL            */
    return SQLITE_DENY;
  case SQLITE_UPDATE:		/* Table Name      Column Name     */
  /* FIXME: this is somehow needed to select from fts tables. */
#if 0
    if (is_temporary_table(userdata,arg1))
    return SQLITE_OK;
    return SQLITE_DENY;
#else
    return SQLITE_OK;
#endif
  case SQLITE_ATTACH:		/* Filename        NULL            */
  case SQLITE_DETACH:		/* Database Name   NULL            */
  default:
    return SQLITE_DENY;
  }
}

/*  ** misc functions ** */

static void
sqlite3_concat(sqlite3_context* ctx, int argc, sqlite3_value** argv)
{
  int len=0, i=0, j=0;
  char *r = NULL;
  for(;i<argc; ++i) len+=sqlite3_value_bytes(argv[i]);
  r = malloc(len+1);
  for(i=0, j=0; i<argc; ++i) {
    int s = sqlite3_value_bytes(argv[i]);
    strncpy(r+j, (const char*)sqlite3_value_text(argv[i]), s);
    j += s;
  }
  r[j]='\0';
  sqlite3_result_text(ctx, r, len, free);
}

static int
sq_sqlite3_create_functions(sqlite3 *conn)
{
  return sqlite3_create_function(conn, "concat", -1, SQLITE_UTF8,
				 NULL, sqlite3_concat, NULL, NULL);
}

/* bug workaround */

static int rs_sqlite3_auth_unrestricted(void* userdata, int opcode,
					const char* arg1, const char* arg2,
					const char* dbname, const char* trigger)
{
  return SQLITE_OK;
}

void sqlite3_set_authorizer_unrestricted(sqlite3 *cnx)
{
 sqlite3_set_authorizer(cnx, rs_sqlite3_auth_unrestricted, NULL);
}

/* setup function table */

static void sq_sqlite3_setup_full(sqlite3 *cnx)
{
  sq_sqlite3_create_functions(cnx);
}

static void sq_sqlite3_setup_restricted(sqlite3 *cnx)
{
  sq_sqlite3_create_functions(cnx);
  sqlite3_set_authorizer(cnx, sq_sqlite3_auth_restricted, NULL);
}

static void sq_sqlite3_setup_restricted_ro(sqlite3 *cnx)
{
  sq_sqlite3_create_functions(cnx);
  sqlite3_set_authorizer(cnx, sq_sqlite3_auth_restricted_ro, NULL);
}

static void (*setup_table[4])(sqlite3 *) = {
  NULL,
  sq_sqlite3_setup_full,
  sq_sqlite3_setup_restricted,
  sq_sqlite3_setup_restricted_ro
};

void sq_sqlite3_setup(sqlite3 *cnx, int i)
{
  void (*f)(sqlite3 *);
  assert(i<4);
  f=setup_table[i];
  if(f) (*f)(cnx);
}

/* Callback VFS */

static sqlite3_vfs *dflt_vfs=NULL;
static obj callback_callback=FALSE_OBJ;
/*
void callback_sqlite3_vfs_init(obj proc)
{
  dflt_vfs = sqlite3_vfs_find(NULL);
  callback_callback = proc;

  callback_vfs.szOsFile = MAX(callback_vfs.szOsFile, dflt_vfs->szOsFile);
  callback_vfs.mxPathname = MAX(CALLBACK_MAX_PATHNAME, dflt_vfs->mxPathname);

}
*/
