/*
** Maximum pathname length supported by the callback backend.
*/
#define CALLBACK_MAX_PATHNAME 512
#define CB_IO_TYPE_BLKSZ  1
#define CB_IO_TYPE_FSIZE  2
#define CB_IO_TYPE_READ   3
#define CB_IO_TYPE_WRITE  4
#define CB_IO_TYPE_TRUNC  5
#define CB_IO_TYPE_CLOSE  6

#define CB_OP_CALL 2
#define CB_OP_RETURN 1
#define CB_OP_BUSY 3
#define CB_OP_SHIFT 2

#define CB_GET_OP(x) ((x) >> CB_OP_SHIFT)
#define CB_M_RETURN_OP(x) ((x) << CB_OP_SHIFT | CB_OP_RETURN)
#define CB_M_CALL_OP(x) ((x) << CB_OP_SHIFT | CB_OP_CALL)

#define CB_IS_BUSY(x) ((x) & CB_OP_BUSY)
#define CB_IS_CALL(x) ((x) & CB_OP_CALL)
#define CB_IS_RETURN(x) ((x) & CB_OP_RETURN)

struct open_args {
  sqlite3 *cnx;
  char *dbn;
  int setup;
  obj vfs;
  obj sm;
};

struct prepare_args {
  sqlite3_stmt *stmt;
  int tail;
  sqlite3 *db;
  const char *sql;
  int sql_len;
  int offset;
};

struct callback_args {
  obj ref;
  int op;
  const char *buf;
  int size;
  int amount;
  sqlite_int64 offset;
  pthread_mutex_t mux;
  pthread_cond_t cond;
};
