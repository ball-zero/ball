(define-module rs.net.md5 ()
  (&module (import usual-inlines))
  (&module
    (load "md5.scm")

    ;; low-level procedures
    (export make-md5-stream          ;; make an md5-stream
            make-md5-state           ;; make an md5 state
            update-md5-state)        ;; roll in a block of input

    ;; this generic can slurp a <string> or an <input-port>
    (export md5-digest)))
