;; There are three kinds of utility code (referenced by
;; mechanism/util.mcf):

(define-macro (: name . typedeclaration) `(begin))
(define-macro (the type expression) expression)
(define-macro (xthe type expression) expression)

;; - runtime, general => mechanism/util.scm
;; - runtime, rscheme only => mechanism/util-rscheme.scm (should be
;; moved into rscheme directoy)
;; - run- and compiletime => rscheme/compile-support.scm

;;; andmap + ormap:

(define (andmap f first . rest)
  (cond ((null? rest)
         (let loop ((l first))
           (or (null? l)
               (and (f (car l)) (loop (cdr l))))))
        ((null? (cdr rest))
         (let loop ((l1 first) (l2 (car rest)))
           (or (null? l1)
               (and (f (car l1) (car l2)) (loop (cdr l1) (cdr l2))))))
        (else
         (let loop ((first first) (rest rest))
           (or (null? first)
               (and (apply f (car first) (map car rest))
                    (loop (cdr first) (map cdr rest))))))))

(define (ormap f first . rest)
  (cond
   ((null? first) (or))
   ((null? rest) (let loop ((first first))
                   (and (pair? first)
                        (or (f (car first))
                            (loop (cdr first))))))
   (else (let loop ((lists (cons first rest)))
           (and (pair? (car lists))
                (or (apply f (map car lists))
                    (loop (map cdr lists))))))))
