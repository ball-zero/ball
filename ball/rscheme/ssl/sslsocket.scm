(define *debug-ssl* #f)

;(define-syntax (if-debug-ssl . body) (values))
;(define-syntax (if-debug-ssl . body) (begin . body))

(define-macro (if-debug-ssl . body) `(if *debug-ssl* (begin . ,body)))

(define (debug-ssl . flag)
  (if (pair? flag)
      (set! *debug-ssl* (car flag)))
  *debug-ssl*)

(define ssl-keep-alive-timeout
  (let ((t 660))
    (lambda args
      (if (pair? args)
	  (let ((t0 t)) (set! t (car args)) t0)
	  t))))

(define-safe-glue (crack-mux-header (s <string>))
{
  u_int32_t hdr = *(u_int32_t *)PTR_TO_DATAPTR(s);
  REG0 = int2fx( (hdr >> 24) & 0xFF );
  REG1 = int2fx( hdr & 0x3FFFF );
  RETURN(2);
})

(define-safe-glue (make-mux-header (type <raw-int>) (len <raw-int>))
{
  obj s;
  u_int32_t hdr; /* BEWARE: assert(sizeof(hdr) == 4) at the other end!!! */

  if ((type < 0) || (type > 0xFF)) {
    scheme_error( "make-mux-header: type ~s out of range", 1, raw_type );
  }
  if ((len < 0) || (len > 0x3FFFF)) {
    scheme_error( "make-mux-header: length ~s out of range", 1, raw_len );
  }

  hdr = ((type << 24) & 0xFF000000) + (len & 0x3FFFF);
  s = bvec_alloc( sizeof(hdr)+1, string_class );
  *(u_int32_t *)PTR_TO_DATAPTR( s ) = hdr;
  REG0 = s;
  RETURN1();
})


(define-class <ssl-socket> (<object>)
  (properties init-value: '())
  peer
  cert-available
  certificate-value
  plaintext-in
  plaintext-out
  mux-out
  mux-in				; only for "safe" close
  process)

(define-method certificate ((self <ssl-socket>))
  (let ((v (certificate-value self)))
    (if (mutex? v)
	(begin
	  (mutex-lock! v)
	  (if (mutex? (certificate-value self))
	      (mutex-unlock! v (cert-available self))
	      (mutex-unlock! v))
	  (certificate-value self))
	v)))

(define-method set-certificate! ((self <ssl-socket>) cert)
  (set-certificate-value! self cert)
  (condition-variable-broadcast! (cert-available self)))

(define-method input-port ((self <ssl-socket>))
  (plaintext-in self))

(define-method output-port ((self <ssl-socket>))
  (plaintext-out self))

(define $ctl-shutdown (string-append (make-mux-header #xCC 1) "c"))

(define-syntax (control-write self packet)
  (if-debug-ssl (format (current-error-port) "*** >>> CONTROL ~s\n" (string-ref packet 4)))
  (write-string (mux-out self) packet))

(define-method close ((self <ssl-socket>))
  (set-certificate! self #f)
  (handler-case
   (begin
     (control-write self $ctl-shutdown)
     (flush-output-port (mux-out self)))
   ((<condition> condition: err)
    (if-debug-ssl (format (current-error-port) "*** close<ssl-socket> ~a\n" err))
    ))
  ;; this should trigger the subprocess to exit
  (handler-case
   (close-output-port (plaintext-out self))
   ((<condition> condition: err)
    (if-debug-ssl (format (current-error-port) "*** close<ssl-socket> (plaintext-out self) ~a\n" err))
    ))
  (handler-case (close-input-port (mux-in self)) ((<condition>)))
  (handler-case
   (check-exit-status (process self))
   ((<condition> condition: err)
    (if-debug-ssl (format (current-error-port) "*** close<ssl-socket> ~a\n" err))))
  ;; this is closed by the status-in monitoring thread when it sees EOF
  ;; (close-input-port (status-in self))
  (values))

(define-class <ssl-mux-output-port> (<output-port>)
  ;; XXX <ssl-mux-output-port> needs to be buffered, and only
  ;; send a data packet down when we are flushed or overflow
  mux-output)

(define-method close-output-port ((self <ssl-mux-output-port>))
  (close-output-port (mux-output self)))

(define-method flush-output-port ((self <ssl-mux-output-port>))
  ;; XXX TODO: ping the subprocess to make sure everything is flushed
  ;;     not just outside the process, but outside our process _cluster_
  (flush-output-port (mux-output self)))

(define-method output-port-write-char ((self <ssl-mux-output-port>) (ch <char>))
  (if-debug-ssl (format (current-error-port) "*** >>> DATA ~#@*70s\n" ch))
  (qout-write-vec (mux-output self) 
                  (vector (make-mux-header #xDD 1) ch)))

(define full-length-mux-header (make-mux-header #xDD #x3ffff))

(define-method write-string ((self <ssl-mux-output-port>) (str <string>))
  (let-syntax ((write-chunk (syntax-form (arg)
                              (let* (((chunk <string>) arg)
                                     ((n <fixnum>) (string-length chunk)))
                                (if (> n 0)
                                    (qout-write-vec 
                                     (mux-output self) 
                                     (vector
                                      (make-mux-header #xDD n)
                                      chunk)))))))
    (if-debug-ssl (format (current-error-port) "*** >>> DATA ~#@*70s\n" str))
    (if (< (string-length str) 250000)
        (write-chunk str)
        (let loop ((i 0))
          (let ((j (+ i 250000)))
            (if (>= j (string-length str))
                (write-chunk (substring str i))
                (begin
                  (write-chunk (substring str i j))
                  (loop j))))))))

(%strategy bytecode
 (define (file-close fd) (fd-close fd))
)

(define-method write-object ((self <initiator-socket>) port)
  (format port "#[<initiator-socket> ~a]\n" (peer self)))

(define (internal-socket-connect2 (remote <inet-socket-addr>) name local)
  (let (((s <initiator-socket>)
         (make <initiator-socket>
               filedes: (socket-create 
                         (socket-address-family->integer 
                          'address-family/internet)
                         (socket-type->integer 'socket-type/stream)
                         0)
               peer: remote
               name: name)))
    (handler-case
     (begin
       (fd-set-blocking (filedes s) #f)
       (if local
	   (socket-bind/inet-sockaddr (filedes s) local))
       (let ((rc (do-connect s)))
	 (cond
	  ((eq? rc 0) s)
	  ((fixnum? rc)
	   (file-close (filedes s))
	   (signal (make <os-error>
		     error-number: rc
		     system-call: "connect"
		     arguments: (vector (filedes s) remote))))
	  (else
	   (file-close (filedes s))
	   (signal (make <os-error>
		     error-number: (cdr rc)
		     system-call: (case (car rc)
				    ((0) "connect")
				    ((1) "getsockopt")
				    (else "do-connect"))
		     arguments: (vector (filedes s))))))))
     ((<condition> condition: err)
      (file-close (filedes s))
      (error err)))))

(define (ssl-connect (sockaddr <inet-socket-addr>)
                     #key
                     (certinfo default: '())
                     (local default: #f)
                     (passphrase default: #f))
  (let ((s (internal-socket-connect2 sockaddr "ssl" local)))
    (handler-case
     (begin
       ;; sslmgr wants the socket in blocking mode...
       (fd-set-blocking (filedes s) #t)
       ;; create the subprocess and wrapper ports
       (make-sslmgr* (filedes s) sockaddr certinfo #f passphrase))
     ((<condition> condition: err)
      (if s (begin
	      (file-close (filedes s))
	      (set-filedes! s -1)
	      (close s)))
      (error err)))))

(define (make-sslmgr cnx peer certinfo)
  (make-sslmgr* cnx peer certinfo #t #f))

(define *sslmgr-executable* "sslmgr")

(define (make-sslmgr* cnx peer certinfo server? pp)
  (bind ((r0 w0 (pipe))
         (r1 w1 (pipe))
         (rpin wpin (make-internal-pipe)))      ; pin="Plaintext INput"
    ;;
    ;; (format #t "Building SSL: ~s\n" peer)
    ;;
    (let ((proc (run* *sslmgr-executable*
		      `("-F"
			"-t" ,(ssl-keep-alive-timeout)
			,(if server? "-pfdsrv:3"
			     (if (string? cnx) (string-append cnx (to-string peer)) "-pfdclient:3"))
			,@(if *debug-ssl* '("-v") '())
			. ,certinfo) 
                      (vector r0 w1 2 cnx)))
          (mux-in (filedes->input-port r1 #t))
          (mux-out (filedes->output-port w0 #t)))
      ;;
      ;; mark the writable fd as non-blocking, which is (still)
      ;; required (on the write side) to avoid a possible blocking
      ;; situation in the threads system in case the subprocess gets
      ;; jammed up.
      (fd-set-blocking w0 #f)
      ;;
      (file-close r0)
      (file-close w1)
      (if (number? cnx) (file-close cnx))
      ;;
      (let ((ssl (make <ssl-socket>
		       cert-available: (make-condition-variable peer)
                       certificate-value: (make-mutex peer)
                       peer: peer
                       plaintext-in: rpin
                       mux-out: mux-out
		       mux-in: mux-in
                       plaintext-out: (make <ssl-mux-output-port>
                                            mux-output: mux-out)
                       process: proc)))
        ;;
        (if pp
            (begin
              (format (current-error-port) "Setting PASSPHRASE = ~s\n" pp)
              (set-properties! ssl `((passphrase . ,pp) . ,(properties ssl)))))
        ;;
        (thread-resume
         (make-thread
          (lambda ()
            (handler-case
             (let loop ()
               (bind ((l (read-string mux-in 4))
                      (type len (crack-mux-header l))
                      (data (read-string mux-in len)))
                 (case type
                   ((#xCC)
                    (if-debug-ssl
                     (format (current-error-port) "*** <<< CONTROL ~s\n" data))
                    (process-control-packet ssl data))
                   ((#xDD)
                    (if-debug-ssl (format (current-error-port)
                                          "*** <<< DATA [~a] ~#@*70s\n"
                                          (string-length data) data))
                    (if (mutex? (certificate-value ssl))
                        (set-certificate! ssl #f))
                    (write-string wpin data))
                   (else
                    (error "Bad framing header type: ~s" type)))
                 (loop)))
             ((<partial-read> condition: err)
              (if-debug-ssl (format (current-error-port) "*** <<< EOF ~a\n" err))
              (values))
             ((<condition> condition: err)
              (if-debug-ssl
               (format (current-error-port) "*** <<< ERROR ~a\n" err))
              (values)))
            (handler-case (close-output-port wpin) ((<condition>)))
	    (close ssl)
	    (and ssl (exit-status (process ssl))))
          (format #f "ssl[~a].in" (if (number? cnx) cnx peer))))
        ssl))))

(define control-header (reg-expr->proc '(seq (save (+ (not #\space)))
                                             (* #\space))))

(define (process-control-packet (ssl <ssl-socket>) data)
  (bind ((s e tag (control-header data)))
        (cond
         ((string=? tag "peer-cert")
          (let ((cert (substring data e)))
            (set-certificate! ssl (if (string=? cert "-none-") #f (make-mesh-certificate cert)))))
	 ((string=? tag "passwd:private")
	  (let (((p <string>)
		 (let ((p (assq 'ssl (properties ssl))))
		   (and p (cadr p)))))
	    (control-write ssl
			   (string-append 
			    (make-mux-header #xCC (+ 1 (string-length p)))
			    "p"
			    p))
	    (flush-output-port (mux-out ssl))))
	 ((string=? tag "error:")
	  (if-debug-ssl
	   (format (current-error-port) "***sslmgr ~a\n"
		   (substring data e))))
	 ((string=? tag "ssl-err")  ;; ssl_choke
	  (if-debug-ssl
	   (format (current-error-port) "***sslmgr choke ~a\n"
		   (substring data e))))
         (else
          ;; ignore it for Now...
          (values)))))

;;;
;;;  The passphrase needs to be set in munged form...

(define (mini-munge! str)
  (for-each
   (lambda (i)
     (let ((ch (string-ref str i)))
       (string-set! 
        str 
        i
        (cond
         ((char-upper-case? ch)
          (integer->char (+ (char->integer #\A)
                            (- (char->integer #\Z) (char->integer ch)))))
         ((char-lower-case? ch)
          (integer->char (+ (char->integer #\a)
                            (- (char->integer #\z)
                               (char->integer ch)))))
         (else
          ch)))))
   (range (string-length str)))
  str)

#|

Strange: this code was written and tested on chicken/linux and tested
on rscheme/linux but it does not work on rscheme/freebsd!

(define (askemos:open-ssl-client addr ca cert key socks4a verbose)
  (let ((ssl #f))
    (handler-case
     (begin
       (set! ssl (make-sslmgr*
		  (if verbose "-pconnect:" "-qpconnect:") addr
		  `("-c" ,(or cert (error "No certificate to connect to ~a." addr))
		    "-k" ,key
		    ,@(if socks4a `("-s" ,socks4a) '())
		    . ,(if ca
			   (list "-E" "-A" ca "-T" ca)
			   '()))
		  #f #f))
       (values (input-port ssl) (output-port ssl) (certificate ssl)
	       (lambda (c) (if ssl (begin (close ssl) (set! ssl #f))))))
     ((<condition> condition: ex)
      (if ssl (handler-case (close ssl)))
      (signal ex)))))
|#

(define-safe-glue (socks4a-setup! (fd <raw-int>) (h <string>) (pf <raw-int>))
{
#define BUFLEN 10
  char buffer[BUFLEN+1], *buf=buffer, *hf=string_text(h);
  int len = snprintf(buf, BUFLEN + 1,
                     "\x4\x1%c%c00010%s",
                     (char) ((pf & 0xff00) >> 8), /* port MSB */
                     (char) (pf & 0xff), /* port LSB */
                     hf);
  if( len >= BUFLEN ) {
    buf = alloca( len + 1 );
    snprintf(buf, len + 1,
             "\x4\x1%c%c00010%s",
             (char) ((pf & 0xff00) >> 8), /* port MSB */
             (char) (pf & 0xff), /* port LSB */
             hf);
  }
  buf[4]='\0';
  buf[5]='\0';
  buf[6]='\0';
  buf[7]='\1';
  buf[8]='\0';
  if (write( fd, buf, len+1 ) != len+1) {
    os_error( "socks server connect request", 1, h );
    RETURN0();
  }
  len = read( fd, buf, 8 );
  if( len != 8 || buf[0] != 0 ) {
    os_error( "socks server connect", 0);
    RETURN0();
  }
  if( buf[1] != (char) 90 ) {
    char *msg = "unknown code";
    switch( (int) buf[1] ) {
    case 91: msg = "request rejected or failed"; break;
    case 92: msg = "rejected; server cannot connect to identd on the client";
      break;
    case 93: msg = "rejected because the client program and identd report different user-ids";
      break;
    }
    os_error( "socks connect", 3, hf, pf, msg );
  }
  RETURN0();
#undef BUFLEN
})

(define (askemos:open-ssl-client addr ca cert key socks4a verbose)
  (let ((ssl (let ((ci `("-c" ,(or cert (error "No certificate to connect to ~a." addr))
			 "-k" ,key
			 "-t" ,(ssl-keep-alive-timeout)
			 . ,(if ca
				(list "-E" "-A" ca "-T" ca)
				'()))))
	       (if socks4a
		   (let ((s (internal-socket-connect2
			     (if (string? socks4a)
				 (let ((d (string-search socks4a #\:)))
				   (if d
				       (make-inet-socket-addr
					(string->inet-addr (substring socks4a 0 d))
					(string->number
					 (substring socks4a (add1 d) (string-length socks4a))))
				       (make-inet-socket-addr
					(string->inet-addr socks4a)
					9050)))
				 socks4a)
			     "ssl-socks" #f)))
		     (handler-case
		      (call-with-values
			  (lambda ()
			    (if (string? addr)
				(let ((d (string-search addr #\:)))
				  (if d
				      (values (substring addr 0 d)
					      (string->number
					       (substring addr (add1 d) (string-length addr))))
				      (values (substring addr 0 d) 443)))
				(inet-socket-addr-parts addr)))
			(lambda (h p)
			  (socks4a-setup! (filedes s) h p)
			  ;; sslmgr wants the socket in blocking mode...
			  (fd-set-blocking (filedes s) #t)
			  (make-sslmgr* (filedes s) addr ci #f #f)))
		      ((<condition> condition: err)
		       (close s) (error err))))
		   (ssl-connect
		    (if (string? addr)
			(let ((d (string-search addr #\:)))
			  (if d
			      (make-inet-socket-addr
			       (string->inet-addr (substring addr 0 d))
			       (string->number (substring addr (add1 d) (string-length addr))))
			      (make-inet-socket-addr
			       (string->inet-addr addr)
			       443)))
			addr)
		    certinfo: ci)))))
    (values (input-port ssl) (output-port ssl) (certificate ssl)
	    (lambda () (and ssl (not (the-exit-status (process ssl)))))
	    (lambda (c) (begin (if ssl (close ssl)) (set! ssl #f))))))
