;; (C) 2013 J�rg F. Wittenberger see http://www.askemos.org

#;(define-syntax make-xml-attribute
  (syntax-rules ()
    ((_ name ns value)
     (make <xml-attribute>
       xml-attribute-name: name xml-attribute-ns: ns
       xml-attribute-value: value))))

#;(define-macro (make-xml-attribute name ns value)
  (list 'make '<xml-attribute>
	xml-attribute-name: name xml-attribute-ns: ns
	xml-attribute-value: value))

#;(define-syntax xml-attribute?
  (syntax-rules () ((xml-attribute? node) (instance? node <xml-attribute>))))

#;(define-syntax %node-list-cons
  (syntax-rules () ((_ snl nl) (cons snl nl))))

(define-macro (%node-list-cons n nl) (list 'cons n nl))

;(define-macro (%node-list-append . nl) `(append . ,nl))

#;(define-syntax %node-list-append
  (syntax-rules ()
    ((_ . nl) (append . nl))))

;;* Namespaces

#;(define-syntax make-xml-namespace
  (syntax-rules () ((_ local uri) (cons local uri))))
(define-macro (make-xml-namespace local uri) (list 'cons local uri))

#;(define-syntax xml-namespace?
  (syntax-rules ()
    ((_ obj)
     (and (pair? obj) (symbol? (car obj)) (symbol? (cdr obj))))))
(define-macro (xml-namespace? obj)
  (list 'and (list 'pair? obj) (list 'symbol? (list 'car obj)) (list 'symbol? (list 'cdr obj))))

;(define-syntax xml-namespace-local (syntax-rules () ((_ x) (car x))))
(define-macro (xml-namespace-local x) (list 'car x))
(define-macro (xml-namespace-uri x) (list 'cdr x))

#;(define-syntax make-xml-element
  (syntax-rules ()
    ((_ gi ns attributes content)
     (make <xml-element>
       name: gi xml-element-ns: ns
       %xml-element-attributes: attributes %children: content))))

#;(define-macro (make-xml-element g n a c)
  (list 'make '<xml-element>
	name: g xml-element-ns: n
	%xml-element-attributes: a %children: c))

(define-syntax copy-xml-element
  (syntax-rules ()
    ((_ node attributes content)
     (or (and (or attributes content)
	      (make-xml-element (name node)
				(xml-element-ns node)
				(or attributes (%xml-element-attributes node))
				(or content (%children node))))
	 node))))

#;(define-syntax make-xml-literal (syntax-rules () ((_ data) data)))
(define-macro (make-xml-literal data) data)
#;(define-syntax xml-literal-value (syntax-rules () ((_ lit) lit)))
(define-macro (xml-literal-value lit) lit)

#;(define-syntax empty-node-list (syntax-rules () ((_) '()))) ; dsssl 10.2.2
(define-macro (empty-node-list) ''()) ; dsssl 10.2.2

