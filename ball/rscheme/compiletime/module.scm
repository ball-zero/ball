;; -*-scheme-*-
;; (C) 2003, 2007 J�rg F. Wittenberger see http://www.askemos.org

(define-module compiletime ()
  (&module
   (import usual-inlines)
   (import srfi.9)
   (load "../compile-support.scm")
   (load "../../mechanism/notation/Lalr/lalr-gen.scm")
   (load "../../mechanism/notation/Lalr/lalr-driver.scm")
   (load "../match-support.scm")
   (files "../../mechanism/srfi/llrbtree.scm")
   (export : the xthe)	 ; ignored type declarations in chicken syntax
   (export make-llrbtree-code)
   
   (export gen-lalr-parser lr-driver glr-driver)
   (export
    make-source-location
    source-location-input source-location-line source-location-column
    source-location-offset source-location-length
    combine-locations

    make-lexical-token lexical-token-category lexical-token-source lexical-token-value
    )
   (export
    match:expanders match:syntax-err match:runtime-structures
    match:structure-control
    match-error-control
    match:primitive-vector?
    match:disjoint-predicates match:vector-structures
    sys:match-error match:set-error-control ;; igitt
    andmap ormap)))

