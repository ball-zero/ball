(define-class <gnutls-error> (<simple-error>) code)

; (define-class <gcrypt-md-ctx> (<bvec>))

(define-macro (define-gnutls-glue args . body)
  `(define-safe-glue ,args
;;      type-handler: (<gcrypt-md-ctx> 
;; 		    (direct-instance? <gcrypt-md-ctx>)
;; 		    ("gcry_md_hd_t ~a"
;; 		     "(*(gcry_md_hd_t *)PTR_TO_DATAPTR(~a))"))
     properties: ((other-h-files "<gnutls/gnutls.h>" "<gnutls/x509.h>")
		  (other-libs "gnutls"))
     ,@body))

(define-gnutls-glue (x509-text (text <string>))
{
  gnutls_x509_crt_t cert;
  gnutls_datum_t tmp;
  gnutls_x509_crt_init(&cert);
  tmp.data = (unsigned char *) string_text(text);
  tmp.size = string_length(text);
  if(gnutls_x509_crt_import(cert, &tmp, GNUTLS_X509_FMT_PEM) < 0) {
      scheme_error("error: parsing certificate",0 , FALSE_OBJ);
  } else {
   gnutls_x509_crt_print(cert, GNUTLS_CRT_PRINT_FULL, &tmp);
   gnutls_x509_crt_deinit(cert);
   REG0 = make_string((char *) tmp.data);
   gnutls_free(tmp.data);
  }
  RETURN1();
})

(define-gnutls-glue (x509-subject (text <string>))
{
  gnutls_x509_crt_t cert;
  gnutls_datum_t tmp;
  char buf[2025];
  size_t size=1024;
  gnutls_x509_crt_init(&cert);
  tmp.data = (unsigned char *) string_text(text);
  tmp.size = string_length(text);
  if(gnutls_x509_crt_import(cert, &tmp, GNUTLS_X509_FMT_PEM) < 0) {
      scheme_error("error: parsing certificate",0 , FALSE_OBJ);
  } else {
   gnutls_x509_crt_get_dn(cert, buf, &size);
   buf[size]='\0';
   gnutls_x509_crt_deinit(cert);
   REG0 = make_string((char *) buf);
  }
  RETURN1();
})

(define-gnutls-glue (x509-expiration-time (text <string>))
{
  gnutls_x509_crt_t cert;
  gnutls_datum_t tmp;
  gnutls_x509_crt_init(&cert);
  tmp.data = (unsigned char *) string_text(text);
  tmp.size = string_length(text);
  if(gnutls_x509_crt_import(cert, &tmp, GNUTLS_X509_FMT_PEM) < 0) {
      scheme_error("error: parsing certificate",0 , FALSE_OBJ);
  } else {
   time_t t = gnutls_x509_crt_get_expiration_time(cert);
   gnutls_x509_crt_deinit(cert);
   REG0 = t > HALF_WORD_MASK ? make_long_int( int_32_to_int_64(t) ) : int2fx(t);
  }
  RETURN1();
})

(define-safe-glue (gnutls-init)
{
  gnutls_global_init();
  RETURN0();
})

(gnutls-init)
