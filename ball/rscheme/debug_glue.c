#include <stdio.h>
#include <signal.h>
#include <rscheme.h>

obj SIGBUS_state[5];
obj SIGBUS_handler;

void trace_bus_error( int x )
{
  fprintf( stderr, 
	   "Got BUS error signal\n"
	   "envt_reg:          %#.8x\n"
	   "literals_reg:      %#.8x\n"
	   "dynamic_state_reg: %#.8x\n"
	   "continuation_reg:  %#.8x\n"
	   "thread_state_reg:  %#.8x\n",
	   (unsigned int)envt_reg,
	   (unsigned int)literals_reg,
	   (unsigned int)dynamic_state_reg,
	   (unsigned int)continuation_reg,
	   (unsigned int)thread_state_reg);
  fflush( stderr );

  SIGBUS_state[0] = envt_reg;
  SIGBUS_state[1] = literals_reg;
  SIGBUS_state[2] = dynamic_state_reg;
  SIGBUS_state[3] = continuation_reg;
  SIGBUS_state[4] = thread_state_reg;

  if( EQ(SIGBUS_handler, FALSE_OBJ)) {
    /* let askemos restart */
    c_signal_catcher(SIGUSR1);
  } else {
    rscheme_intr_call0(SIGBUS_handler);
  }
}


  /*

  extern obj rscheme_global[60];
  raise_exception( 7, 1, ( make1 ( rscheme_global[36], //condition_class,
				   make2( rscheme_global[8], //pair_class
					  make2( rscheme_global[8],
						 lookup_symbol( "stack" ),
						 make_exception_stack() ),
					  NIL_OBJ ))));

  */
