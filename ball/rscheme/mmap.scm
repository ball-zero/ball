(define-class <mmap-error> (<condition>)
  filename
  message)

(define-class <mmap-string> (<object>) :abstract
  (sha256 type: <string> init-value: #f)
  (size type: <fixnum>)
  (count-value type: <fixnum> init-value: #f)
  region)

(define-class <mmap-ro-string> (<mmap-string>))

(define-method finalize ((self <mmap-ro-string>))
  (munmap (region self) (size self)))

(define-safe-glue (munmap region (size <fixnum>))
{
  if(munmap(OBJ_TO_RAW_PTR(region), fx2int(size))) {
    raise_error( make3( TLREF(0), NIL_OBJ,
	                FALSE_OBJ, make_string(strerror(errno)) ) );
  }
  RETURN0();
})

(define-safe-glue (mmap-ro-file* (file <string>))
  literals: ((& <mmap-error>))
  properties: ((other-h-files "<sys/mman.h>" "<sys/types.h>" "<sys/stat.h>"
			      "<fcntl.h>" "<unistd.h>" "<errno.h>" "<string.h>"))
{
  int fd = 0;
  size_t size;
  void *region = NULL;
  struct stat sb;

  if (stat( string_text(file), &sb ) < 0) {
    raise_error( make3( TLREF(0), NIL_OBJ,
	                file, make_string(strerror(errno)) ) );
  }

  size = sb.st_size;

  if( (fd = open(string_text(file), O_RDONLY)) == 0 ) {
    raise_error( make3( TLREF(0), NIL_OBJ,
	                file, make_string(strerror(errno)) ) );
  }

  if( (region = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0)) == NULL ) {
    raise_error( make3( TLREF(0), NIL_OBJ,
	                file, make_string(strerror(errno)) ) );
  }

  close(fd); /* ??? will not unmap the region, will it hurt? */

  REG2=size > 5 && strncmp(region, "<?xml", 5) ? TRUE_OBJ : FALSE_OBJ;
  REG1=int2fx(size);
  REG0=RAW_PTR_TO_OBJ(region);
  RETURN(3);
})

(define (mmap-ro-file name)
  (call-with-values (lambda () (mmap-ro-file* name))
    (lambda (region size xml)
      (let ((result (make <mmap-ro-string>
		      size: size region: region
		      count-value:
		      (if xml
			  (delay (let loop ((start 0) (index 0))
				   (if (eqv? start size) index
				       (receive
					(index end)
					(mmap:utf8-tell
					 region size start index
					 (min size (+ index 16384)))
					(loop end index)))))
			  size))))
	(register-for-finalization result)
	result))))

(define-glue (mmap-substring region offset length)
{
  size_t len = fx2int(length);
  obj result = bvec_alloc( len+1, string_class );
  char *dst = ((char *)PTR_TO_DATAPTR(result));
  memcpy( dst, fx2int(offset) + (char *)OBJ_TO_RAW_PTR(region), len );
  dst[len]='\0';
  REG0=result;
  RETURN1();
})

(define-safe-glue (mmap-copy-string! region
				     (str <string>)
				     (roffset <raw-int>)
				     (soffset <raw-int>)
				     (length  <raw-int>))
{
  memcpy( roffset + string_text(str),
	  soffset + (char *)OBJ_TO_RAW_PTR(region),
          length );
  REG0=str;
  RETURN1();
})

;; Questionable, but the same as with ordinary strings.  We should
;; return the 'count' instead for UTF-8.

(define-method string-length ((self <mmap-ro-string>))
  (size self))

(define-safe-glue (mmap:utf8-tell region (size <raw-int>)
				  (start <raw-int>) (index <raw-int>)
				  (pos <raw-int>))
  literals: ((& <utf8-error>))
{
  unsigned char *s=(char *)OBJ_TO_RAW_PTR(region);
  unsigned char *scan=s+start;
  unsigned char *limit=scan+size;
  if( s+pos > limit ) {
   raise_error( make3( TLREF(0), NIL_OBJ, make_string("index out of bounds"), int2fx(pos) ) );
  } else {
   limit = s+pos;
  }
  while ( scan < limit ) {
   ++index;
   if (*scan < 0x80) scan++;
   else if (*scan < 0xE0) scan+=2;
   else if (*scan < 0xF0) scan+=3;
   else if (*scan < 0xF8) scan+=4;
   else if (*scan < 0xFC) scan+=5;
   else if (*scan < 0xFE) scan+=6;
   else raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad string"), int2fx(scan-s) ) );
  }
  REG1=int2fx(scan-s);
  REG0=int2fx(index);
  RETURN(2);
})

(define-method string-count ((self <mmap-string>))
  (force (count-value self)))

(define-safe-glue (mmap:utf8-string-getc* region (size <raw-int>) (start <raw-int>))
  literals: ((& <utf8-error>))
{
  unsigned char *s=(char *)OBJ_TO_RAW_PTR(region);
  unsigned char *scan=s+start;
  unsigned char *limit=s+size;
  unsigned int i, size=1, ch;
  if (*scan < 0x80) ch=*scan;
  else if (*scan < 0xE0) {size=2; ch=*scan & 0x1F;}
  else if (*scan < 0xF0) {size=3; ch=*scan & 0x0F;}
  else if (*scan < 0xF8) {size=4; ch=*scan & 0x07;}
  else if (*scan < 0xFC) {size=5; ch=*scan & 0x3;}     
  else if (*scan < 0xFE) {size=6; ch=*scan & 0x1;}
  else ch=0, raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad character size"), int2fx(scan-s) ) );

  if( scan++ + size > limit )
    raise_error( make3( TLREF(0), NIL_OBJ, make_string("short character"), int2fx(scan-s) ) );
  for(i=size-1; i ;--i) {
    if ((*scan<0x80) || (*scan >= 0xC0))
      raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad byte"), int2fx(scan-s) ) );
    else { ch=(ch<<6) | (*scan++ & 0x3F); }
  }

  REG0 = size==1 ? MAKE_ASCII_CHAR(ch) : MAKE_UNICODE_CHAR(ch);
  REG1 = int2fx(size);
  RETURN(2);
})

(define-method mmap:utf8-string-getc ((self <mmap-string>) start)
  (mmap:utf8-string-getc* (region self) (size self) start))

;; (define-method substring ((self <mmap-ro-string>) offset length)
;;   (mmap-substring (region self) offset length))

;; In fortunate coincidence we use a string buffer of the maximum size
;; sslsockets will pass through without rebuffering.

(define-method display-object ((self <mmap-ro-string>) port)
  (if (<= (size self) #x3ffff)
      (write-string port (mmap-substring (region self) 0 (size self)))
      (let ((buffer (make-string #x3ffff)))
	(do ((i 0 (+ i 16384)))
	    ((>= i (- (size self) #x3ffff))
	     (write-string
	      port
	      (mmap-substring (region self) i (- (size self) i))))
	  (write-string
	   port
	   (mmap-copy-string!
	    (region self) buffer i 0 #x3ffff))))))
