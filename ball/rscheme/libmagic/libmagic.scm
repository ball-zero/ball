;; (C) 2005 Peter Hochgemuth see http://www.askemos.org

(define-class <magic-desc> (<object>) :bvec)

(define-safe-glue (magic-open)
  properties: ((other-h-files "<magic.h>")
	       (other-libs "magic"))
  literals: ((& <magic-desc>))
{
  magic_t i;
  obj d;

  i = magic_open(MAGIC_MIME  /*| MAGIC_CONTINUE*/);
  if (i == NULL) {
    REG0=FALSE_OBJ;		  
    RETURN1();
  } else {
    magic_load(i, NULL);
    d = alloc( sizeof( i ), TLREF(0) );
    *((magic_t *)PTR_TO_DATAPTR(d)) = i;
    REG0 = d;
    RETURN1();
  }
})

(define-safe-glue (magic-close (self <magic-desc>))
{
  magic_close( *((magic_t *)PTR_TO_DATAPTR(self)) );
  RETURN0();
})

(define-safe-glue (magic-buffer (self <magic-desc>) (buffer <string>))
  properties: ((other-h-files "<magic.h>"))
{
  magic_t i = *((magic_t *)PTR_TO_DATAPTR(self));
  size_t len;
  const char *buf, *res;

  buf = string_text(buffer);
  len = string_length(buffer);

  res = magic_buffer( i, buf, len);
  if( res == NULL )
    REG0 = FALSE_OBJ;
  else
    REG0 = make_string(res);
  RETURN1();
})

(define magic-cookie #f)

(define (mime-type-of-string str)
  (if (not magic-cookie) (set! magic-cookie (magic-open)))
  (and magic-cookie (magic-buffer magic-cookie str)))

;;
;; experimental implementation to use shared-mime-info
;;
(define *mime-glob-ext-table* #f)
(define *mime-glob-regex-list* '())

(define (load-mime-database)
  (let loop ((d '("/usr/share/mime"
		  "/usr/local/share/mime"
		  ".local/share/mime")))
    (cond 
     ((null? d) #t)
     (else
      (load-globs (string-append (car d) "/globs"))
      ;(load-magic (string-append (car d) "/magic"))
      (loop (cdr d))))))

(define (load-globs path)
  (if (not *mime-glob-ext-table*)
      (set! *mime-glob-ext-table* (make-string-table)))
  (guard
   (e (else (logerr " E: load-mime-database: can't open ~a\n" path) #f))
   (call-with-input-file path
     (lambda (port)
       (do ((line (read-line port) (read-line port)))
	   ((eof-object? line) #t)
	 (let ((match ((pcre->proc "^([^#:][^:]*):(.+)$") line)))
	   (if match
	       (cond
		((or ((pcre->proc "^\\*(\\.[^][*?]+)$") (caddr match))
		     ((pcre->proc "^([^][*?]*)$") (caddr match))) =>
		 (lambda (ext)
		   (table-insert! 
		    *mime-glob-ext-table* (cadr ext) (cadr match))))
		(else
		 (set! 
		  *mime-glob-regex-list*
		  (cons
		   (cons 
		    (pcre->proc
		     (apply
		      string-append
		      (cons
		       "^"
		       (append
			(map 
			 (lambda (c) (cond ((eq? c #\?) ".")
					   ((eq? c #\*) ".*")
					   ((eq? c #\.) "\\.")
					   (else (string c))))
			 (string->list (caddr match)))
			'("$")))))
		    (cadr match))
		   *mime-glob-regex-list*)))))))))))

(define (glob-match name)
  (or
   (table-lookup *mime-glob-ext-table* name)
   (let loop ((ext ((pcre->proc "^[^.]*(\\..+)$") name)))
     (and ext
	  (or (table-lookup *mime-glob-ext-table* (cadr ext))
	      (loop ((pcre->proc "^\\.[^.]*(\\..+)$") (cadr ext))))))
   (let loop ((match *mime-glob-regex-list*))
     (cond ((null? match) #f)
	   (((caar match) name) (cdar match))
	   (else (loop (cdr match)))))))
