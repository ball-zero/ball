;; (C) 2003, Joerg F. Wittenberger see http://www.askemos.org

;; This was originally just an own hopefully PLT compatible socket
;; interface.  Much of the code went into rscheme.  The module should
;; actually be abandoned.

;; However while slowly restructuring the source code to clean up
;; remaining Scheme implementation specifica, I'm using it as a
;; collection spot for those things.  This would incure repeting
;; renames with a lot of version control problems.  Hence we stick
;; with the bad name until it's gone alttogether.

;; First some old code from the Askemos implementation.  This is
;; beeing replaced with the new functions based on 0.7.3.3 build 9.

;; (define-class <socket-input-port> (<input-port>)
;;   (underlying-peer-socket type: <mbox-input-port>)
;;   write-side
;;   peer)

;; (define-class <socket-output-port> (<output-port>)
;;   (underlying-output-port type: <queued-output-port>)
;;   read-side
;;   peer)


;; (define-delegation ((self <socket-input-port>) (underlying-input-port self))
;;   (input-port-read-char self)
;;   (input-port-peek-char self)
;;   (input-port-read-line self)
;;   (input-port-read self)
;;   (input-port-scan-token self))

;; (define-delegation ((self <socket-output-port>) (underlying-output-port self))
;;   (flush-output-port self)
;;   (output-port-write-char self char)
;;   (write-string self string))


;; (define-method close-input-port ((self <socket-input-port>))
;;   (close-input-port (underlying-input-port self))
;;   (if (write-side self)
;;       (set-read-side! (write-side self) #f)
;;       (file-close (fd (underlying-input-port self)))))

;; (define-method finalize ((self <socket-input-port>))
;;   (close-input-port self))

;; (define-method close-output-port ((self <socket-output-port>))
;;   (close-output-port (underlying-output-port self))
;;   (if (read-side self)
;;       (set-write-side! (read-side self) #f)
;;       (file-close (fd (underlying-output-port self)))))

;; (define-method finalize ((self <socket-output-port>))
;;   (close-output-port self))

;; (define-class <socket-input-port> (<input-port>)
;;   (initiator type: <initiator-socket>))

;; (define-class <socket-output-port> (<output-port>)
;;   (initiator type: <initiator-socket>))

;; (define-delegation ((self <socket-input-port>) (initiator self))
;;   (input-port-read-char self)
;;   (input-port-peek-char self)
;;   (input-port-read-line self)
;;   (input-port-read self)
;;   (input-port-scan-token self))

;; (define-delegation ((self <socket-output-port>) (initiator self))
;;   (flush-output-port self)
;;   (output-port-write-char self char)
;;   (write-string self string))


;; (define-method close-input-port ((self <socket-input-port>))
;;   (let ((result (close-input-port (input-port (initiator self)))))
;;     (set-input-port! (initator self) #f)
;;     (or (output-port (initator self)) (close self))
;;     result))

;; (define-method finalize ((self <socket-input-port>))
;;   (close-input-port self))

;; (define-method close-output-port ((self <socket-output-port>))
;;   (let ((result (close-output-port (output-port (initator self)))))
;;     (set-output-port! (initator self) #f)
;;     (or (input-port self) (close self))
;;     result))

;; (define-method finalize ((self <socket-output-port>))
;;   (close-output-port self))

;;

;; (define (tcp-listen port)
;;  (make-service (inet-server port)))
;;
;; (define (tcp-accept listener)
;;   (receive
;;    (fd peer) (get-next-client listener)
;;    (fd-set-blocking fd #f)
;;    (let ((in (make <socket-input-port>
;;                underlying-input-port: (open-mbox-input-port fd)
;;                write-side: #f peer: peer))
;;          (out (make <socket-output-port>
;;                underlying-output-port: (open-queued-output fd)
;;                read-side: #f peer: peer)))
;;      (set-write-side! in out)
;;      (set-read-side! out in)
;;      (values in out))))
;;
;; (define (tcp-connect host port)
;;   (let ((fd (inet-client host port)))
;;     (let ((in (make <socket-input-port>
;;                 underlying-input-port: (open-mbox-input-port fd)
;;                 write-side: #f peer: #f))
;;           (out (make <socket-output-port>
;;                  underlying-output-port: (open-queued-output fd)
;;                  read-side: #f peer: #f)))
;;       (set-write-side! in out)
;;       (set-read-side! out in)
;;       (values in out))))

(define (eof-condition-reason obj)
  (cond ((eof-object? obj) 'eof)
	((broken-pipe-error? obj) 'EPIPE)
	((partial-read-error? obj) 'partial-read)
	(else #f)))

(define (eof-condition? obj)
  (or (eof-object? obj)
      (broken-pipe-error? obj)
      (partial-read-error? obj)))

(define (broken-pipe-error? ex)
  ;; EPIPE: try to write to a socket/pipe fd while its reading end is closed
  (and (instance? ex <os-error>)
       ;(equal? (system-call ex) "write")
       (eq?    (error-number ex) 32)))

(define (partial-read-error? obj)
  (instance? obj <partial-read>))

(%strategy bytecode
(define (set-socket-noblock! socket)
  (if (< (setsockopt-int (filedes socket)
			 (local-socket-level
			  (cadr (assq 'level/tcp $portable-sockopt-levels)))
			 1			; TCP_NODELAY
			 1)
	 0)
      #f)				; FIXME raise error
  socket))

(define (tcp-listen port . backlog+host)
  (if (pair? backlog+host)
      (let ((backlog (car backlog+host)) ; TODO don't ignore
	    (host (and (pair? (cdr backlog+host)) (cadr backlog+host))))
	(if host
	    (open-server-socket port local: host)
	    (open-server-socket port)))
      (open-server-socket port)))

(define (tcp-accept listener)
  (let ((rs (accept-client listener)))
    (set-socket-option (filedes rs) 'level/socket 'socket/keep-alive #t)
    (set-socket-noblock! rs)
    (values (input-port rs) (output-port rs))))

(define (tcp-connect host port)
  (let ((cs (open-client-socket host port)))
    (set-socket-noblock! cs)
    (values (input-port cs) (output-port cs))))

(define (ssl-accept listener certinfo)
  (bind ((cnx peer (get-next-client (service listener)))
         (ssl (make-sslmgr cnx peer certinfo)))
        (values (input-port ssl) (output-port ssl))))

(define (string-match regexp str . start)
  (bind ((start end #rest x (apply regexp str start)))
        (and start (cons (cons start end) x))))

(define etc-resolve.conf-nameserver-line-regex
  (reg-expr->proc
   '(seq "nameserver" (+ (or #\space #\tab))
         (save (seq (+ digit) #\. (+ digit) #\.
                    (+ digit) #\. (+ digit))))))
