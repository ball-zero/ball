(define-macro (make-stack . lst)
  (if (pair? lst)
      `(let ((result (make-dequeue)))
	 (for-each (lambda (e) (dequeue-push-back! result e) (values)) ,(car lst))
	 result)
      '(make-dequeue)))

(define-macro (clear-stack s)
  `(begin
    (set-state! ,s (make-vector 5 #f))
    (set-front! ,s 0)
    (set-back! ,s 0)
    (values)))

(define-macro (stack? s) (instance ,s <dequeue>))

(define-macro (stack-empty? s) `(dequeue-empty? ,s))

(define-macro (stack-length s)
  `(dequeue-count ,s))

(define-macro (stack-depth s)
  `(sub1 (dequeue-count ,s)))

(define-macro (stack-peek s . index)
  (let ((i (gensym))
	(j (gensym)))
    `(let* ((,i ,(if (pair? index) (car index) 0))
	    (,j (if (eq? (add1 ,i) (vector-length (state ,s)))
		    0
		    (add1 ,i))))
       (if (fixnum>? ,j (back ,s)) (error "stack-peek index ~a out of range" ,j))
       (vector-ref (state ,s) ,j))))

(define-macro (stack-poke! s value . index)
  (let ((i (gensym))
	(j (gensym)))
    `(let* ((,i ,(if (pair? index) (car index) 0))
	    (,j (if (eq? (add1 ,i) (vector-length (state ,s)))
		    0
		    (add1 ,i))))
       (if (fixnum>? ,j (back ,s)) (error "stack-peek index ~a out of range" ,j))
       (vector-set (state ,s) ,j ,value)
       ,s)))

(define-macro (stack-push! s v . more)
  `(begin
     ,@(map
	(lambda (e)
	  (list 'dequeue-push-front! s e))
	(cons v more))
     ,s))

(define-macro (stack-pop*! s) `(dequeue-pop-front! ,s))

(define (stack-pop! x) (dequeue-pop-front! x))

(define (stack-cut! stack start-depth . end-depth)
  (define (dequeue+ state a b)
    (let* ((max (vector-length state))
	   (r (fixnum+ a b)))
      (cond
       ((fixnum>=? r max) (fixnum- r max))
       ((fixnum<? r 0) (fixnum+ r max))
       (else r))))
  (let ((end-depth (if (pair? end-depth) (car end-depth) start-depth))
	(state (state stack)))
    (if (fixnum>? start-depth (stack-depth stack))
	(error "stack-cut! start-depth ~a out of range" start-depth))
    (if (or (fixnum<? end-depth start-depth)
	    (fixnum>? end-depth (stack-depth stack)))
	(error "stack-cut! end-depth ~a out of range" end-depth))
    (let loop ((i (dequeue+ state (front stack) start-depth))
	       (n (add1 (fixnum- end-depth start-depth))))
      (if (eqv? n 0)
	  (begin
	    (if (fixnum>? start-depth 0)
		(let loop ((i i)
			   (n (sub1 start-depth)))
		  (if (eqv? n -1)
		      (set-front! stack i)
		      (let ((f (dequeue+ state i -1)))
			(vector-set!
			 state
			 f
			 (vector-ref state (dequeue+ state (front stack) n)))
			(loop f (sub1 n)))))
		(set-front! stack i))
	    '())
	  (let ((e (vector-ref state i)))
	    (vector-set! state i #f)
	    (cons e (loop (dequeue+ state i 1) (sub1 n))))))))

(define (stack->list* dq)
  (let (((f <fixnum>) (front dq))
	((l <fixnum>) (vector-length (state dq)))
	((v <vector>) (state dq)))
    ;;
    (let loop (((i <fixnum>) (sub1 (back dq)))
	       (r '()))
      (if (eq? i f)
	  (cons (vector-ref v i) r)
	  (if (eq? i 0)
	      (loop (sub1 (vector-length v)) (cons (vector-ref v i) r))
	      (loop (sub1 i) (cons (vector-ref v i) r)))))))

(define-macro (stack->list s) `(stack->list* ,s))

(define (stack-for-each* dq proc)
  (let (((f <fixnum>) (front dq))
	((l <fixnum>) (vector-length (state dq)))
	((v <vector>) (state dq)))
    ;;
    (let loop (((i <fixnum>) (sub1 (back dq)))
	       (r '()))
      (if (eq? i f)
	  (proc (vector-ref v i))
	  (if (eq? i 0)
	      (loop (sub1 (vector-length v)) (proc (vector-ref v i)))
	      (loop (sub1 i) (proc (vector-ref v i))))))))

(define-macro (stack-for-each stack proc) `(stack-for-each* ,stack ,proc))

(define-macro (print-stack stack) `(stack-for-each ,stack display))
;;;-----

(define-class <dynvector> (<object>)
  vect dflt cnt)

(define (dynvector? x) (instance? x <dynvector>))

(define (dynvector-tabulate n f . rest)
  (let* ((dflt (if (pair? rest) (car rest) #f))
	 (vect (vector-unfold f n))
	 (dflt (if dflt dflt (vector-ref vect 0))))
     (make <dynvector> vect: vect dflt: dflt cnt: n)))

(define (list->dynvector l . rest)
  (let* ((dflt (if (pair? rest) (car rest) #f))
	 (vect (list->vector l))
	 (dflt (if dflt dflt (vector-ref vect 0))))
    (make <dynvector> vect: vect dflt: dflt cnt: (vector-length vect))))

(define (make-dynvector n dflt)
  (make <dynvector> vect: (make-vector n dflt) dflt: dflt cnt: 0))

(define (dynvector-clear! dv n) 
    (set-vect! dv (make-vector n (dflt dv)))
    (set-cnt! dv n))

(define (dynvector-length dv)
  (cnt dv))

(define (dynvector-ref dv i)
  (handler-case
   (vector-ref (vect dv) i)
   ((<condition>) (dflt dv))))

(define (dynvector-set! dv i e)
  (let ((vect (vect dv))
	(n    (cnt dv)))
    (handler-case
     (begin
       (vector-set! vect i e)
       (set-cnt! dv (max n (fixnum+ 1 i))))
     ((<condition>)
      (let* ((n1    (max (fixnum* 2 n) (fixnum+ 1 i) 16))
	     (vect1 (if (eqv? 0 (vector-length vect))
			(make-vector n1 (dflt dv))
			(vector-copy vect 0 n1 (dflt dv)))))
	(vector-set! vect1 i e)
	(set-vect! dv vect1)
	(set-cnt! dv (fixnum+ 1 i)))))))

(define (dynvector-expand! dv n)
  (dynvector-set! dv (- n 1) (dflt dv)))

(define (dynvector-for-each f dv . rest)
  (let ((vect+n (map (lambda (dv) (list (vect dv) (cnt dv))) (cons dv rest))))
    (receive
     (vect n) (unzip2 vect+n)
     (let ((min-n  (apply min n)))
       (apply vector-for-each 
	      (cons (lambda (i v . rest) (apply f (cons i rest)))
		    (cons (make-vector min-n #f) vect)))))))

(define (dynvector-map f dv . rest)
  (let ((vect+n (map (lambda (dv) (list (vect dv) (cnt dv))) (cons dv rest))))
    (receive
     (vect n) (unzip2 vect+n)
     (let ((min-n  (apply min n)))
       (let ((vect1 (apply vector-map 
			   (cons (lambda (i v . rest) (apply f (cons i rest)))
				 (cons (make-vector min-n #f) vect)))))
	 (make <dynvector> vect: vect1 dflt: (vector-ref vect1 0) cnt: min-n))))))

(define (dynvector-copy dv)
  (make <dynvector> vect: (vector-copy (vect dv)) dflt: (dflt dv) cnt: (cnt dv)))

(define (dynvector-fold f init dv . rest)
  (let ((vect+n (map (lambda (dv) (list (vect dv) (cnt dv))) (cons dv rest))))
    (receive
     (vect n)  (unzip2 vect+n)
     (let ((min-n  (apply min n)))
       (apply vector-fold 
	      (cons (lambda (i state v . rest) (apply f (cons i (cons state rest))))
		    (cons init (cons (make-vector min-n #f) vect))))))))

(define (dynvector-fold-right f init dv . rest)
  (let ((vect+n (map (lambda (dv) (list (vect dv) (cnt dv))) (cons dv rest))))
    (receive
     (vect n)  (unzip2 vect+n)
     (let ((min-n  (apply min n)))
       (apply vector-fold-right 
	      (cons (lambda (i state v . rest) (apply f (cons i (cons state rest))))
		    (cons init (cons (make-vector min-n #f) vect))))))))

(define (dynvector-index pred? dv . rest)
  (let ((vect+n (map (lambda (dv) (list (vect dv) (cnt dv))) (cons dv rest))))
    (receive
     (vect n)  (unzip2 vect+n)
     (let ((min-n  (apply min n)))
       (apply vector-index 
	      (cons (lambda (v . rest) (apply pred? rest))
		    (cons (make-vector min-n #f) vect)))))))

(define (dynvector-any pred? dv . rest)
  (let ((vect+n (map (lambda (dv) (list (vect dv) (cnt dv))) (cons dv rest))))
    (receive
     (vect n)  (unzip2 vect+n)
     (let ((min-n  (apply min n)))
       (apply vector-any 
	      (cons (lambda (v . rest) (apply pred? rest))
		    (cons (make-vector min-n #f) vect)))))))

(define (dynvector-every pred? dv . rest)
  (let ((vect+n (map (lambda (dv) (list (vect dv) (cnt dv))) (cons dv rest))))
    (receive
     (vect n)  (unzip2 vect+n)
     (let ((min-n  (apply min n)))
       (apply vector-every 
	      (cons (lambda (v . rest) (apply pred? rest))
		    (cons (make-vector min-n #f) vect)))))))

(define (dynvector->list dv)
  (vector->list (vect dv) 0 (cnt dv)))

;;; http://www.call-with-current-continuation.org/eggs/datatype.html

;;;; datatype.scm - Variant record types from "Essentials of Programming Languages" - felix
;
; Copyright (c) 2000-2003, Felix L. Winkelmann
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following
; conditions are met:
;
;   Redistributions of source code must retain the above copyright notice, this list of conditions and the following
;     disclaimer. 
;   Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
;     disclaimer in the documentation and/or other materials provided with the distribution. 
;   Neither the name of the author nor the names of its contributors may be used to endorse or promote
;     products derived from this software without specific prior written permission. 
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
; AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
; OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;
; Send bugs, suggestions and ideas to: 
;
; felix@call-with-current-continuation.org
;
; Felix L. Winkelmann
; Steinweg 1A
; 37130 Gleichen, OT Weissenborn
; Germany


(define-macro (define-datatype typename x . rest)
  ;; (##sys#check-syntax 'define-datatype typename 'symbol)
  `(begin
     (define-class ,typename (<object>))
     ,@(map (lambda (variant)
	      ;; (##sys#check-syntax 'define-datatype variant '(symbol . #((symbol _) 0)))
	      (let ((variantname (car variant))
		    (variantclass
		     (string->symbol
		      (string-append "<" (symbol->string (car variant)) ">")))
		    (fieldnames (map car (cdr variant)))
		    (fieldpreds (map cadr (cdr variant))) )
		`(begin
		   (define-class ,variantclass (,typename)
		     . ,(map (lambda (n) (list n getter: n setter: #f))
			     fieldnames))
		   (define (,variantname ,@fieldnames)
		     (make
			 ,variantclass
		       . ,(apply
			   append
			   (map
			    (lambda (name pred)
			      `(,(string->symbol (string-append
						  (symbol->string name) ":"))
				(if (,pred ,name)
				    ,name
				    (error
				     "bad argument ~s type to variant constructor ~a ~a"
				     ,name ',variantname ',name) )) )
			    fieldnames fieldpreds)) ) )) ) )
	    (if (symbol? x) 
		rest
		(cons x rest) ) )
     ,@(if (symbol? x)
	   `((define (,x x) (instance? x ,typename)))
	   '() ) ) )

(define-macro (cases typename exp . clauses)
  ;; (##sys#check-syntax 'cases typename 'symbol)
  (let ((tmp (gensym)) )
    `(let ((,tmp ,exp))
       (if (instance? ,tmp ,typename)
	   (cond ,@(map (lambda (clause)
			  (if (eq? 'else (car clause))
			      `(else (let () ,@(cdr clause)))
			      (begin
				;; (##sys#check-syntax 'cases clause '(symbol #(symbol 0) . #(_ 1)))
				(let ((variantname (car clause))
				      (variantclass
				       (string->symbol
					(string-append "<" (symbol->string (car clause)) ">")))
				      (fields (cadr clause)) )
				  `((instance? ,tmp ,variantclass)
				    (call-with-values
					(lambda ()
					  (values . ,(map (lambda (f) (list f tmp))
							  fields)))
				      (lambda ,fields . ,(cddr clause))) ) ) ) ) )
			clauses) )
	   (error "bad argument type ~s to `cases' ~a" ,tmp ',typename ) ) ) ) )

(define (stack-peek2 stack) 
  (values (stack-peek stack 0)
	  (stack-peek stack 1)))

;; pair split
(define (psplit2 lst) (values (car lst) (cdr lst)))

(define (intpair? x)
  (and (pair? x)
       (fixnum? (car x))
       (fixnum? (cdr x))))


; Datatype: diffop
;
; A representation of the three diff operations; insert, remove, change. 
;
; TARGET is the line or range of lines that is being operated on
;
; SOURCE is the range of lines that is used as input of the insert and
; change commands.  
;
; DATA, DATAIN, DATAOUT is a sequence of the
; elements (e.g. lines) that are being inserted or replaced.
;
; CONTEXT, CONTEXTIN, CONTEXTOUT is optional context; these are pairs
; in which the car is a list of elements preceding the operation, and
; the cdr is a list of elements following the operation.
;
(define-datatype diffop diffop?
  (Insert   (target fixnum?) (source intpair?)
	    (seq list?) (context (lambda (x) (or (not x) (list? x)))) )
  (Remove   (target intpair?)
	    (seq list?) (context (lambda (x) (or (not x) (list? x)))) )
  (Change   (target intpair?)
	    (source intpair?)
	    (seqin list?)
	    (seqout list?)
	    (contextin (lambda (x) (or (not x) (list? x))))
	    (contextout (lambda (x) (or (not x) (list? x))))))

(define-method display-object  ((self diffop) out)
  (cases diffop self
	 (Insert (target source seq context)
		 (format out "#(Insert target=~a source=~a context=~a)\n"
			 target source context))	 
	 (Remove (target seq context)
		 (format out "#(Remove target=~a seq=~a context=~a)\n"
			 target seq context))
	 
	 (Change (target source seqin seqout contextin contextout)
		 (format
		  out
		  "#(Change target=~a source=~a seqin=~a seqout=~a contextin=~a contextout=~a)\n"
		  target source seqin seqout contextin contextout))))

(define-macro (s32vector-set! x i v) `(vector-set! ,x ,i ,v))
(define-macro (s32vector-ref x i) `(vector-ref ,x ,i))
(define-macro (make-s32vector n d) `(make-vector ,n ,d))

;;
;;
;;  S. Wu, U. Manber, and E. Myers. An O(NP) sequence comparison
;;  algorithm. In Information Processing Letters, volume 35, pages
;;  317--323, September 1990.
;;
;;
(define (make-npdiff equal? is-ref is-length hunks) 
  (lambda (A B . rest)
   (let ((context-len (if (pair? rest) (car rest) 0)))
    (define css (make-stack))

    (let ((M (is-length A))
	  (N (is-length B)))

      (receive
       (A B M N swap) 
       (if (fixnum>? M N) (values B A N M #t)
	   (values A B M N #f))
       (begin

	 ;; The algorithm outlined in the paper calls for the creation
	 ;; of an array that contains the furthest paths, and that is
	 ;; defined as [-(M+1),(N+1)].

	 ;; Since the vector library in Scheme does not support negative
	 ;; array indices, we are going to have to bump everything by
	 ;; offset M+1 whenever accessing array FP

	 (define (compare delta offset fp p) 
	   
	   (define (update k)
	     (s32vector-set! fp (fixnum+ k offset)
			     (snake k (max (fixnum+ 1 (s32vector-ref fp (fixnum+ offset (fixnum- k 1))))
					   (s32vector-ref fp (fixnum+ offset (fixnum+ 1 k)))))))
	   
	   (define (lowerloop k)
	     (if (fixnum<=? k (fixnum- delta 1))
		 (begin
		   (update k)
		   (lowerloop (fixnum+ 1 k)))))
	   
	   (define (upperloop k)
	     (if (fixnum>=? k (fixnum+ 1 delta))
		 (begin
		   (update k)
		   (upperloop (fixnum- k 1)))))
	   
	   (let ((p (fixnum+ p 1)))
	     (lowerloop (fixnum* -1 p))
	     (upperloop (fixnum+ delta p))
	     (update delta)
	     (if (not (eqv? N (s32vector-ref fp (fixnum+ offset delta))))
		 (compare delta offset fp p))))
	 
	 
	 (define (snake k y)
	   (let ((a (fixnum- y k))
		 (b y))
	     (receive
	      (x y) 
	      (let loop ((x  a)  (y  b))
		(if (and (fixnum<? x M) (fixnum<? y N)
			 (equal? (is-ref A x) (is-ref B y)))
		    (loop (fixnum+ 1 x) (fixnum+ y 1))
		    (values x y)))
	      (if (or (not (eqv? a x)) (not (eqv? b y)))
		  (receive
		   (lasta lastb)
		   (if (stack-empty? css) (values -1 -1)
		       (psplit2 (stack-peek css 1)))
		   (if (and (fixnum<? lasta a) (fixnum<? lastb b))
		       ;; we have found a common substring; push the end
		       ;; and start pairs onto the common substring stack
		       (if swap
			   (begin
			     (stack-push! css (cons b a))
			     (stack-push! css (cons y x)))
			   (begin
			     (stack-push! css (cons a b))
			     (stack-push! css (cons x y)))))))
	      y)))


	 (let ((offset (fixnum+ 1 M))
	       (fp     (make-s32vector (fixnum+ 3 (fixnum+ M N)) -1))
	       (delta  (fixnum- N M))
	       (p       -1))
	   (compare delta offset fp p)
	   (if swap 
	       (values (hunks B A css context-len) B A)
	       (values (hunks A B css context-len) A B)))))))))




;;  Pop matching pairs from the given stack, and fill in the gaps
;;  between them with insert/change/remove hunks.
;;
;;  This function expects the following stack layout:
;;
;; 	endpair n
;; 	startpair n
;; 	endpair n-1
;;      startpair n-1
;; 	.
;; 	.
;; 	.
;; 	endpair 1
;; 	startpair 1
;;			 
;;  i.e. the one constructed by function `npdiff' above. endpair
;;  marks the end of a common substring. startpair marks the beginning
;;  of a common substring. Each pair has the form (x,y) where x is a
;;  line number in text A, and y is a line number in text B.
;;
;;  If substring n (i.e. the one at the top of the stack) does not
;;  reach the last line of text A (its endpair does NOT have the last
;;  line number in A as x coordinate) that means we have some extra
;;  lines at the end of text A that need to be removed, so we make a
;;  remove hunk for them. If instead the y component does not reach
;;  the end of B, we make an insert hunk.
;;
;;  If substring 1 (i.e. the one at the bottom of the stack) does not
;;  start from the first line of text A (its endpair does NOT have 0
;;  as y coordinate) that means we have some extra lines at the
;;  beginning of text B that need to be inserted, so we make an insert
;;  hunk for them. If instead the x component is non-zero, we make a
;;  remove hunk.
;;
;;  For all other cases, we make change hunks that fill in the gaps
;;  between any two common substrings.  
(define (make-hunks is-ref is-length is-slice)
  
  (lambda (A B css . rest)
    (let ((context-len (if (pair? rest) (car rest) 0)))
     (let ((M (is-length A))
	   (N (is-length B))
	   (context? (fixnum>? context-len 0)))
       
       (define (make-context seq len start end)
	 (if (or (fixnum>? start len) (fixnum<? end start)) (list)
	     (let ((start (if (fixnum<? start 0) 0 start))
		   (end   (if (fixnum<? len end) len end)))
		 (is-slice seq start end))))

       (define (loop css hunks)
	 (if (stack-empty? css) hunks
	    ;; make a change hunk and recurse
	    (receive
	     (endpair startpair)  (stack-peek2 css)
	     (let ((k (stack-depth css)))
	       (bind ((x y  (psplit2 startpair))
		      (w z  (psplit2 endpair)))
		     ;; are these the the last two elements of the stack?
		     (if (eqv? 1 k)
			 (cond ((and (eqv? 0 x) (eqv? 0 y))   hunks)

			       ((eqv? 0 x) (cons (Insert x (cons 0 y) (is-slice B 0 y) 
							 (and context? (cons (list) (make-context B N y (fixnum+ y context-len)))))
						 hunks))

			       ((eqv? 0 y) (cons (Remove (cons 0 x) (is-slice A 0 x)
							 (cons (list) (make-context A M x (fixnum+ x context-len))))
						 hunks))

			       (else (cons (Change (cons 0 x) (cons 0 y)
						   (is-slice B 0 y) (is-slice A 0 x) 
						   (and context? (cons (list) (make-context B N y (fixnum+ y context-len))))
						   (and context? (cons (list) (make-context A M x (fixnum+ x context-len)))))
					   hunks)))
			 (begin
			   (stack-pop! css)
			   (stack-pop! css)
			   (bind ((w z (values x y))
				  (x y (psplit2 (stack-peek css))))
				 (let ((newhunk  (cond ((eqv? y z)  (Remove (cons x w) (is-slice A x w)
									    (and context? (cons (make-context A M (fixnum- x context-len) x)
												(make-context A M x (fixnum+ x context-len))))))

						       ((eqv? x w)  (Insert x (cons y z) (is-slice B y z)
									    (and context? (cons (make-context B N (fixnum- y context-len) y)
												(make-context B N z (fixnum+ z context-len))))))

						       (else (Change (cons x w) (cons y z)
								     (is-slice B y z ) (is-slice A x w)
								     (and context? (cons (make-context B N (fixnum- y context-len) y)
											 (make-context B N z (fixnum+ z context-len))))
								     (and context? (cons (make-context A M (fixnum- x context-len) x)
											 (make-context A M w (fixnum+ w context-len)))))))))
				   ;;; (match hunks
;;; 					;				     ((h . rest)  (loop css (merge newhunk h rest)))
;;; 					  (_   (loop css (if newhunk (cons newhunk hunks) hunks))))
				   (loop css (if newhunk (cons newhunk hunks) hunks)))))))))))

      (if (stack-empty? css)
	  ;; the two sequences are completely different (or empty)
	  (list (Change (cons 1 M) (cons 1 N)
			(is-slice B 0 N)
			(is-slice A 0 M)))
	  (receive
	   (endpair startpair)  (stack-peek2 css)
	   (let ((k (stack-depth css)))
	     (bind ((x y  (psplit2 startpair))
		    (w z  (psplit2 endpair)))
		   (cond ((and (eqv? w M) (eqv? z N))
			  (loop css (list)))

			 ((eqv? z N)
			  (loop css (list (Remove (cons w M) (is-slice A w M)
						  (and context? (cons (make-context A M w (fixnum- w context-len))
								      (list)))))))

			 ((eqv? w M)
			  (loop css (list (Insert w (cons z N) (is-slice B z N)
						  (and context? (cons (make-context B N (fixnum- z context-len) z)
								      (list)))))))

			 (else (loop css (list (Change (cons w M) (cons z N)
						       (is-slice B z N )
						       (is-slice A w M)
						       (and context? (cons (make-context B N (fixnum- z context-len) z)
									   (list)))
						       (and context? (cons (make-context A M (fixnum- w context-len) w)
									   (list))))))))))))))))



