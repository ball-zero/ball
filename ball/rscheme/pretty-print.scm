;; FIXME: RScheme's pretty-print "pp" seems broken.
;; TRY: (pp '(1 . 2))
(define (pretty-print obj port)
  (with-output-to-port port
    (lambda ()
      (display "Pretty-print in RScheme is broken! ") (display (random)) (newline)
      (pp obj))))
