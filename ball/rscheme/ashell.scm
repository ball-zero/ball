,(use paths tables)

(define $default-fas-name "ashell")

(define $verbose #f)

(define (debug fmt . args)
  (and $verbose (apply format #t fmt args)))

(define *known-modules* 
  (let ((t (make-symbol-table)))
    (for-each (lambda (mp)
		(table-insert! t (car mp) 'builtin))
	      (with-module mlink (installed-modules)))
    t))

(define *extra-libs* (make-symbol-table))

(define (add-extra-libs! libs)
  (let loop ((libs libs))
    (and (pair? libs)
	 (let ((k (string->symbol (car libs))))
	   (if (not (table-lookup *extra-libs* k))
	       (table-insert! *extra-libs* k (string-append "-l" (car libs))))
	   (loop (cdr libs))))))

(define *linkfiles* (make-string-table))
(define (add-linkfile path)
  (let ((k (pathname->os-path path)))
    (if (not (table-lookup *linkfiles* k))
	(table-insert! *linkfiles* k #f))))

(define (module-name->os-name name)
  (list->string
   (map (lambda (ch)
	  (cond ((or (eq? ch #\.)
		     (eq? ch #\-)
		     (eq? ch #\*)) #\_)
		(else ch)))
	(string->list
	 (if (symbol?  name) (symbol->string name) name)))))

(define rs-install-dir
  (string->dir (or (getenv "RS_INSTALL_DIR") "[install]")))

(define resource/modules (string->dir "resource/modules"))

(define (mx-file-in dir name)
  (make <file-name> 
    filename: (module-name->os-name name)
    extension: "mx"
    file-directory: (append-dirs dir resource/modules)))

(define (mif-file-in dir name)
  (make <file-name> 
    filename: (module-name->os-name name)
    extension: "mif"
    file-directory: (append-dirs dir resource/modules)))

(define (obj-file-in dir name)
  (make <file-name> 
    filename: (module-name->os-name name)
    extension: "o"
    file-directory: (append-dirs dir (string->dir "lib"))))

(define ignore-objects #t)

(define (register-module base name)
  (let ((obj-file (lambda(name) (obj-file-in base name)))
	(mx-data (and (file-exists? (mx-file-in base name))
		      (call-with-input-file
			  (pathname->os-path
			   (mx-file-in base name)) read))))
    (cond ((not mx-data)
	   (debug " *D* register: no mif for ~a in ~a\n"
		  name (mx-file-in base name))
	   #f)
	  ((table-lookup *known-modules* (car mx-data)) =>
	   (lambda (e)
	     (cond ((eq? e 'builtin)
		    (debug " *D* register: builtin module ~a\n" name)
		    #t)
		   ((and (pair? e) (equal? base (caddr e)))
		    (debug " *D* register: called twize for ~a\n" name)
		    #t)
		   (else 
		    (format #t "*** Warning: duplicate module ~a ignore ~aresource/module/~a.mx use ~aresource/module/~a.mx\n"
			    (car mx-data) base (module-name->os-name name)
			    (caddr e) (cadr (cadr e)))
		    #t))))
	  ((not (file-exists? (mif-file-in base name)))
	   (format #t "*** missing mif file for ~a in ~aresource/modules/!\n"
		   (car mx-data) base)
	   #f)
	  (else
	   (let loop ((f (map obj-file (caddr mx-data))))
	     (cond ((and (pair? f) (or ignore-objects (file-exists? (car f))))
		    (loop (cdr f)))
		   ((null? f)
		    (table-insert! *known-modules*
				   (car mx-data)
				   (list (null? (list-ref mx-data 4)) mx-data base))
		    (debug " *D* register: ~a registerd~a\n"
			   (car mx-data)
			   (if (null? (list-ref mx-data 4)) "!"
			       (format #f " depends on ~a modules!"
				       (length (list-ref mx-data 4)))))
		    (add-extra-libs! (list-ref mx-data 5))
		    (map add-linkfile (map obj-file (caddr mx-data)))
		    #t)
		   (else
		    (format #t "*** c-link file missed for ~a: ~a!\n"
			    (car mx-data) (car f))
		    #f)))))))

(define (register-module* dirs name rs-dir)
  (let loop ((dirs dirs))
    (cond ((pair? dirs)
	   (or (register-module (car dirs) name)
	       (loop (cdr dirs))))
	  ((and rs-dir (register-module rs-dir name)) #t)
	  (else
	   (format #t "*** Error: missing requested module: ~a\n" name)
	   (process-exit 2)))))

(define (register-all-modules base)
  (let ((port (open-input-process
	       (format #f "ls ~a" (pathname->os-path (append-dirs base resource/modules))))))
    (let loop ((line (read-line port)))
      (cond ((eof-object? line)
	     (close-input-port port))
	    (else
	     (let* ((file (string->file line)))
	       (and (string=? (extension file) "mx")
		    (register-module base (filename file)))
	       (loop (read-line port))))))))

(define (register-all-modules* dirs)
  (let loop ((dirs dirs))
    (cond ((pair? dirs)
	   (register-all-modules (car dirs))
	   (loop (cdr dirs)))
	  (else #t))))

(define (check-depend! module)
  (let ((entry (table-lookup *known-modules* module)))
    (cond ((eq? entry 'builtin) '())
	  ((and (pair? entry) (car entry)) '())
	  ;; try sys modules in rs-install-dir
	  ((and (not entry) (or (debug " *D* search ~a in rs-install-dir\n"
				       module) #t)
		(register-module rs-install-dir module))
	   (check-depend! module))
	  ((and (pair? entry)
		(let loop ((dep (list-ref (cadr entry) 4)))
		  (cond ((null? dep)
			 (set-car! entry #t)
			 (list module))
			(else 
			 (append (check-depend! (car dep))
				 (loop (cdr dep))))))))
	(else
	  (format #t "*** Error: missing dependent module: ~a\n" module)
	  (process-exit 2)))))

(define (sort-depend*!)
  (let loop ((all (key-sequence *known-modules*)))
    (if (null? all)
	'()
	(append (check-depend! (car all)) (loop (cdr all))))))

(define (search-modules dirs modules rs-dir)
  (let ((dirs (if (null? dirs) (list (current-directory)) (reverse dirs))))
   (let loop ((modules modules))
    (if (string=? (car modules) "all")
	(register-all-modules* dirs)
	(register-module* dirs (car modules) rs-dir))
    (and (pair? (cdr modules)) (loop (cdr modules)))))
  (let ((sl (sort-depend*!)))
    (debug " *D* search-modules: sorted: ~a\n" sl)
    sl))

(define (help)
  (display "synopsis\n"
	   *console-output-port*)
  (display "genfasl [--help] [-o name] [-I base]... [all | module ...]\n"
	   *console-output-port*)
  (display " -o name           defines the name prefix and path used for the boot, package and option file\n"
	   *console-output-port*)
  (display " -I base           include base as a search path for modules\n"
	   *console-output-port*)
  (display "                   genfasl searches in <base>/resource/modules/ for modules\n"
	   *console-output-port*)
  (display "                   and <base>/lib/ for support libraries\n"
	   *console-output-port*)
  (display "                   dependend modules are always searched relative to \"[install]\"\n"
	   *console-output-port*)
  (display "                   or (if set) the RS_INSTALL_PATH environment variable\n"
	   *console-output-port*)
  (display " all | module...   include the named modules or all genfasl can find\n\n"
	   *console-output-port*))

(define (argerror msg)
  (display "genfasl: " *console-output-port*)
  (display msg *console-output-port*)
  (newline *console-output-port*)
  (newline *console-output-port*)
  (display "see genfasl -- help for more" *console-output-port*)
  (newline *console-output-port*)
  (process-exit 2))

(define (Xmain args)
  (handler-bind 
   (<condition> death)
   (do-it args)))

(define (main args)
  (let ((rs-dir rs-install-dir) ;; search system modules in INSTALL_DIR or [install]
	(name $default-fas-name)
	(shell #f)
	(boot  #f)
	(force #f))
    (let loop ((args args)
	       (dirs '())
	       (modules '()))
      (cond
       ((null? args)
	(if (null? modules) (argerror "no modules specified"))
	(let* ((mods (search-modules dirs modules rs-dir))
	       (file (string->file name))
	       (base (make <file-name> 
		       filename: (if (string=? (filename file) "")
				     $default-fas-name (filename file))
		       extension: #f
		       file-directory: (file-directory file))))
	  (and shell (build-shell-setup base mods force))
	  (and boot  (build-boot-setup  base mods force))))
       ((string=? (car args) "--help")
	(help)
	(process-exit 0))
       ((string=? (car args) "-I")
	(if (pair? (cdr args))
	    (loop (cddr args) (cons (string->dir (cadr args)) dirs) modules)
	    (argerror "-I needs a value!")))
       ((string=? (car args) "-o")
	(if (pair? (cdr args))
	    (begin (set! name (cadr args))
		   (loop (cddr args) dirs modules))
	    (argerror "-o needs a value!")))
       ((string=? (car args) "-no-rs-dir")
	(set! rs-dir #f)
	(loop (cdr args) dirs modules))
       ((string=? (car args) "-v")
	(set! $verbose #t)
	(loop (cdr args) dirs modules))
       ((string=? (car args) "-f")
	(set! force #t)
	(loop (cdr args) dirs modules))
       ((string=? (car args) "-boot")
	(set! boot #t)
	(loop (cdr args) dirs modules))
       ((string=? (car args) "-shell")
	(set! shell #t)
	(set! ignore-objects #f)
	(loop (cdr args) dirs modules))
       (else
	(loop (cdr args) dirs (cons (car args) modules)))))))

(define (death (err <condition>) next-handler)
  (display "--- internal error occurred\n" *console-error-port*)
  (display err *console-error-port*)
  (newline *console-error-port*)
  (process-exit 2))

(define (update-file name val force)
  (if (and (not force) (file-exists? name) (string=? (file->string name) val))
      (and $verbose (format #t "~a is up to date\n" name))
      (with-output-to-file (relative-file name)
	(lambda () (format #t "~a" val)))))

(define (build-shell-setup base modules force)
  (update-file
   (extension-related-path base "c")
   (with-output-to-string
     (lambda () (emit-module-setup (filename base) modules)))
   force)
  (update-file
   (extension-related-path base "lnk")
   (string-join " " (append (key-sequence *linkfiles*) 
			    (value-sequence *extra-libs*)))
   force))

(define (build-boot-setup base modules force)
  (update-file 
   (extension-related-path base "scm")
   (with-output-to-string
     (lambda () (emit-boot-setup base modules)))
   force))

(define (emit-module-setup name modules)
  (let* ((entries (map (lambda(m) (table-lookup *known-modules* m)) modules))
	 (mxs (map cadr entries))
	 (dirs (map caddr entries)))
    (format #t 
"/* module setup file for ~a */

#include <stdlib.h>
#include <string.h>
#include <rscheme/api.h>
#include <rscheme/osglue.h>
#include <rscheme/stdmodul.h>
#include <rscheme/rlseconf.h>

/* public interfaces for rscheme modules */
" name)
    (for-each
     (lambda (mx dir)
       (and (equal? dir rs-install-dir)
	    (pair?  (caddr mx))
	    (format #t "#include <rscheme/pkgs/~a.h>\n" (caaddr mx))))
     mxs dirs)
    (format #t "
/* public interfaces for project modules */
")
    (for-each
     (lambda (mx dir)
       (and (not (equal? dir rs-install-dir))
	    (pair?  (caddr mx))
	    (format #t "#include \"~a/~a.h\"\n" (caaddr mx) (caaddr mx))))
     mxs dirs)
    (format #t "

/* module descriptors */
struct module_descr *(std_modules[]) = {
")
    (for-each
     (lambda (mx dir)
       (and (equal? dir rs-install-dir)
	    (pair?  (caddr mx))
	    (format #t "\t&module_~a,\n" (caaddr mx))))
     mxs dirs)
    (for-each
     (lambda (mx dir)
       (and (not (equal? dir rs-install-dir))
	    (pair?  (caddr mx))
	    (format #t "\t&module_~a,\n" (caaddr mx))))
     mxs dirs)
    (format #t
"\tSTD_MODULES_DECL };

#define DEFAULT_IMG \"~a.fas\"

#ifdef INSTALL_DIR
#else
  #define INSTALL_DIR \"/usr/lib/~a/\"
#endif

void askemos_pre_init();

int main( int argc, const char **argv )
{
  char temp[1024];

  rs_install_dir = getenv( \"RS_INSTALL_DIR\" );
  if (!rs_install_dir)
      rs_install_dir = INSTALL_DIR;

  sprintf( temp, \"%s\" DEFAULT_IMG, rs_install_dir );
  if (!os_file_exists_p( temp ) && strstr( temp, \".fas\" ))
    {
      /*  if the default image isn't there, look for an `.orig' image
       *  as well.  This allows us to easily replace the default
       *  fasl image with preloaded material
       */
       strcpy( strstr( temp, \".fas\" ), \".orig.fas\" );
    }

  askemos_pre_init();

  return rscheme_std_main( argc, argv, std_modules, temp );
}
" name name)))

(define (emit-boot-setup base modules)
  (let* ((entries (map (lambda(m) (table-lookup *known-modules* m)) modules))
	 (mxs (map cadr entries))
	 (dirs (map caddr entries)))
    (for-each
     (lambda (mx dir)
       (format #t ";; ~s\n" (car mx))
       (pp `(with-module
	     mlink
	     (with-module
	      paths
	      (format #t "--- Linking in: ~a\n" ',(car mx))
	      (link-load-module 
	       ',(car mx) 
	       (string->file
		,(if (equal? dir rs-install-dir)
		     (format #f "[resource]/modules/~a.mif" (cadr mx))
		     (format
		      #f "~a/resource/modules/~a.mif" 
		      (pathname->os-path (dir-from-to (file-directory base) dir))
		      (cadr mx))))))))
       (newline))
     mxs dirs)))

  
