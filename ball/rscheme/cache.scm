;; (C) 2008, 2010 Joerg F. Wittenberger see http://www.askemos.org

(cond-expand
 (debug-lock
  (define-record-type <cache-entry>
    (%make-cache-entry color left right key/s key
		       state mutex avail thread thunk cont results)
     cache-entry?
     (color cache-entry-color cache-entry-color-set!)
     (left cache-entry-left cache-entry-left-set!)
     (right cache-entry-right cache-entry-right-set!)
     (key/s cache-entry-key/s cache-entry-key/s-set!)
     (key cache-entry-key)
     (state cache-entry-state cache-entry-state-set!)
     (mutex cache-entry-mutex)
     (avail cache-entry-avail)
     (thread cache-entry-thread cache-entry-thread-set!)
     (thunk cache-entry-thunk cache-entry-thunk-set!)
     (cont cache-entry-cont cache-entry-cont-set!)
     (results cache-entry-values cache-entry-values-set!)))
 (else
  (define-class <cache-entry> (<object>)
    color left right key/s key
    state mutex avail thread
    thunk cont results)
  (define-macro
    (%make-cache-entry color left right key/s key
		       state mutex avail thread thunk cont results)
    `(make-gvec <cache-entry>
		,color ,left ,right ,key/s ,key
		,state ,mutex ,avail ,thread ,thunk ,cont ,results))
  (define-macro (cache-entry-color n) `(gvec-ref ,n 0))
  (define-macro (cache-entry-color-set! n v) `(gvec-set! ,n 0 ,v))
  (define-macro (cache-entry-left n) `(gvec-ref ,n 1))
  (define-macro (cache-entry-left-set! n v) `(gvec-set! ,n 1 ,v))
  (define-macro (cache-entry-right n) `(gvec-ref ,n 2))
  (define-macro (cache-entry-right-set! n v) `(gvec-set! ,n 2 ,v))
  (define-macro (cache-entry-key/s n) `(gvec-ref ,n 3))
  (define-macro (cache-entry-key/s-set! n v) `(gvec-set! ,n 3 ,v))
  (define-macro (cache-entry-key n) `(gvec-ref ,n 4))

  (define-macro (cache-entry-state n) `(gvec-ref ,n 5))
  (define-macro (cache-entry-state-set! n v) `(gvec-set! ,n 5 ,v))
  (define-macro (cache-entry-mutex n) `(gvec-ref ,n 6))
  (define-macro (cache-entry-avail n) `(gvec-ref ,n 7))
  (define-macro (cache-entry-thread n) `(gvec-ref ,n 8))
  (define-macro (cache-entry-thread-set! n v) `(gvec-set! ,n 8 ,v))
  (define-macro (cache-entry-thunk n) `(gvec-ref ,n 9))
  (define-macro (cache-entry-thunk-set! n v) `(gvec-set! ,n 9 ,v))
  (define-macro (cache-entry-cont n) `(gvec-ref ,n 10))
  (define-macro (cache-entry-cont-set! n v) `(gvec-set! ,n 10 ,v))
  (define-macro (cache-entry-values n) `(gvec-ref ,n 11))
  (define-macro (cache-entry-values-set! n v) `(gvec-set! ,n 11 ,v))
  ))

(define-llrbtree-code
   (ordered)
   ((node . args)
    `(let ((node ,node))
       . ,(let loop ((args args))
	    (if (null? args)
		'(node)
		(cons
		 (case (car args)
		   ((color:) `(cache-entry-color-set! node ,(cadr args)))
		   ((left:) `(cache-entry-left-set! node ,(cadr args)))
		   ((right:) `(cache-entry-right-set! node ,(cadr args)))
		   (else (error args)))
		 (loop (cddr args)))))))
   cache-node-init!    ;; defined
   cache-node-lookup	   ;; defined
   #f			   ;; cache-min not defined
   cache-node-fold	   ;; defined
   #f ;; cache-node-for-each	   ;; undefined
   cache-node-insert!	   ;; defined
   cache-node-delete!	   ;; defined
   #f			   ;; cache-node-delete-min! not defined
   cache-empty?	   ;; defined
   ((key node) ;; key-node-eq?
    ;; don't use just "string=?" - in chicken it will miss the (most common) a=b case
    `(string=? ,key (cache-entry-key/s ,node)))
   ((key node) ;; key-node-<? ordering function
    `(string<? ,key (cache-entry-key/s ,node)))
   ((node1 node2) ;; before? node ordering function
    `(string<? (cache-entry-key/s ,node1) (cache-entry-key/s ,node2)))
   cache-entry-left cache-entry-left-set!
   cache-entry-right cache-entry-right-set!
   cache-entry-color cache-entry-color-set!
   #f
   )

(define-inline (make-cache-root)
  (cache-node-init!
   (%make-cache-entry
    #f #f #f  #f ;; color left right, key/s
    #f #f	 ;; key state
    #f #f #f     ;; mutex avail thread
    #f #f #f	 ;; thunk cont values
    )))

(define-inline (make-cache-entry es key/s key thunk)
  (let ((nm (dbgname key "~a-entry")))
    (%make-cache-entry
     #f #f #f  key/s ;; color left right, key
     key es	     ;; state
     (make-mutex nm) (make-condition-variable nm) #f
     thunk #f #f)))

(define-record-type <cache>
  (%make-cache name key2string mutex index state miss hit fulfil valid? delete)
  cache?
  (name cache-name)
  (key2string cache-key2string)
  (mutex cache-mutex)
  (index cache-index)
  (state cache-state)
  (miss cache-miss)
  (hit cache-hit)
  (fulfil cache-fulfil)
  (valid? cache-valid?)
  (delete cache-delete))

(define (default-hit-handler cache value-state) #f)

(define (default-fulfil-handler cache value-state values-or-false) #f)

(define (default-delete-handler cache-state value-state) #f)

;;(define (identity x) x)

(define (make-cache name eq state miss hit fulfil valid? delete)
  (assert (procedure? miss))
  (assert (procedure? valid?))
  (%make-cache
   name
   (cond
    ((eq? eq eq?) symbol->string)
    ((eq? eq string=?) identity)
    (else (error "make-cache: only symbols and strings as key so far")))
   (make-mutex name)
   (make-cache-root)
   state miss
   (or hit default-hit-handler)
   (or fulfil default-fulfil-handler)
   valid?
   (or delete default-delete-handler)))

(: cache-size ((struct <cache>) -> fixnum))
(define (cache-size cache) -1)		; currently not supported

(define-inline (cache-lookup cache key)
  (cache-node-lookup (cache-index cache) ((cache-key2string cache) key)))

(define-inline (cache-value-fulfiled! cache entry values)
  ((cache-fulfil cache) cache (cache-entry-state entry) values))

(define-inline (set-entry-value! entry new avail)
  (cache-entry-values-set! entry new)
  (cache-entry-cont-set! entry avail)
  (cache-entry-thread-set! entry #f)
  (condition-variable-broadcast! (cache-entry-avail entry)))

;; Compute the value, run trigger and signal completion.

(define-macro (with-cache-entry entry . body)
  `(retain-mutex (cache-entry-mutex ,entry) . ,body))

(define-macro (with-cache-entry-owned entry owner . body)
  `(retain-mutex (cache-entry-mutex ,entry) (if (eq? ,owner (cache-entry-thread ,entry)) (begin . ,body))))

(: cache-entry-force! ((struct <cache>) * :ce: (struct thread) -> undefined))
(define (cache-entry-force! cache key entry owner)
  (guard
   (exception
    (else
     (let ((new (list (if (condition? exception) exception
			  (make-condition
			   &message 'message
			   (format "~a ~a ~a" key (cache-entry-thunk entry) exception))))))
       (with-cache-entry-owned
	entry owner
	(cache-value-fulfiled! cache entry #f)
	(set-entry-value! entry new raise)))))
   (call-with-values (cache-entry-thunk entry)
     (lambda result
       (with-cache-entry-owned
	entry owner
	(cache-value-fulfiled! cache entry result)
	(set-entry-value! entry result values))))))

(define-inline (cache-entry-delete! cache entry)
  ;; FIXME: should we t(h)erminate/grill the thread somehow?
  (cache-entry-thread-set! entry #f)
  (cache-node-delete! (cache-index cache) (cache-entry-key/s entry))
  (condition-variable-broadcast! (cache-entry-avail entry)))

;; Arrange to compute the value if needed and return it.

;; returns no known value
(define-inline (!cache-entry-force cache key entry)
  (or
   (cache-entry-thread entry)
   (cache-entry-thread-set!
    entry
    (thread-start!
     (make-thread
      (lambda ()
	(cache-entry-force! cache key entry (current-thread)))
      (dbgname key "~a-ref"))))))

(cond-expand
 (debug-lock
  (define-macro (with-cache-index cache . body)
    `(retain-mutex (cache-mutex ,cache) . ,body)))
 (else
  (define-macro (with-cache-index cache . body)
    `(with-mutex (cache-mutex ,cache) . ,body))))

;; Wait for computed value and return it.

(define (cache-entry-wait cache key entry)
  (let ((avail (cache-entry-cont entry)))
    (if avail
	(apply avail (cache-entry-values entry))
	(let loop ()
	  (mutex-lock! (cache-entry-mutex entry))
	  (let ((avail (cache-entry-cont entry)))
	    (if (procedure? avail)
		(let ((results (cache-entry-values entry)))
		  (mutex-unlock! (cache-entry-mutex entry))
		  (apply avail results))
		(begin
		  (yield-mutex-set! (!cache-entry-force cache key entry))
		  (mutex-unlock! (cache-entry-mutex entry) (cache-entry-avail entry))
		  (let ((results (cache-entry-values entry))
			(avail  (cache-entry-cont entry)))
		    (if (procedure? avail)
			(apply avail results)
			;; (loop) only, originally; Better be sure not
			;; to run into an endless loop.  BEWARE:
			;; invalidation MUST match!
			(if (cache-entry-thread entry) (loop)
			    (let ((new (with-cache-index cache (cache-lookup cache key))))
			      (if (and new (not (eq? new entry)))
				  (cache-entry-wait cache key new)
				  (raise-cache-abandon-exception
				   (format "cache-entry-wait ~a aborted on ~a"
					   (cache-name cache) key))))))))))))))

(define (cache-find! cache key thunk)
  (let* ((key/s ((cache-key2string cache) key))
	 (entry (cache-node-lookup (cache-index cache) key/s)))
    (or entry
	(let ((entry (make-cache-entry
		      ((cache-miss cache) (cache-state cache) key)
		      key/s key
		      thunk)))
	  (cache-node-insert! (cache-index cache) key/s entry)
	  entry))))

(define-inline (cache-find cache key thunk)
  (hang-on-mutex! 'cache-find (list (cache-mutex cache)))
  (let ((entry (cache-node-lookup (cache-index cache) ((cache-key2string cache) key))))
    (or entry
	(with-cache-index cache (cache-find! cache key thunk)))))

(define-inline (cache-entry-ref cache key entry)
  (hang-on-mutex! 'cache-entry-ref (list (cache-entry-mutex entry)))
  (let ((avail (cache-entry-cont entry)))
    (if (procedure? avail)
	(if ((cache-valid? cache) (cache-entry-state entry))
	    (let ((results (cache-entry-values entry)))
	      ((cache-hit cache) cache (cache-entry-state entry))
	      (apply avail results))
	    (begin
	      (with-cache-entry
	       entry
	       (let ((results (cache-entry-values entry))
		     (avail (cache-entry-cont entry)))
		 (if (and (procedure? avail)
			  ((cache-valid? cache) (cache-entry-state entry)))
		     ((cache-hit cache) cache (cache-entry-state entry))
		     (if (not (cache-entry-thread entry))
			 (begin
			   (cache-entry-cont-set! entry #f)
			   (cache-entry-thread-set! entry #f)
			   ((cache-delete cache) (cache-state cache) (cache-entry-state entry))
			   (cache-entry-state-set! entry ((cache-miss cache) (cache-state cache) key))
			   (cache-entry-values-set! entry #f))))))
	      (yield-mutex-set! (cache-entry-wait cache key entry))))
	(yield-mutex-set! (cache-entry-wait cache key entry)))))

(define-inline (cache-entry-ref/default cache key entry default)
  (hang-on-mutex! 'cache-entry-ref/default (list (cache-entry-mutex entry)))
  (let ((avail (cache-entry-cont entry)))
    (if (procedure? avail)
	(if ((cache-valid? cache) (cache-entry-state entry))
	    (let ((results (cache-entry-values entry)))
	      ((cache-hit cache) cache (cache-entry-state entry))
	      (apply avail results))
	    (with-cache-entry
	     entry
	     (let ((results (cache-entry-values entry))
		   (avail (cache-entry-cont entry)))
	       (if (and (procedure? avail)
			((cache-valid? cache) (cache-entry-state entry)))
		   (begin
		     ((cache-hit cache) cache (cache-entry-state entry))
		     (apply avail results))
		   (if (cache-entry-thread entry) default
		       (begin
			 (cache-entry-cont-set! entry #f)
			 (cache-entry-thread-set! entry #f)
			 ((cache-delete cache) (cache-state cache) (cache-entry-state entry))
			 (cache-entry-state-set! entry ((cache-miss cache) (cache-state cache) key))
			 (cache-entry-values-set! entry #f)
			 (yield-mutex-set! (!cache-entry-force cache key entry))
			 default))))))
	(begin
	  (yield-mutex-set! (!cache-entry-force cache key entry))
	  default))))

(define (cache-state-update! cache state proc)
  (hang-on-mutex! 'cache-state-update! (list (cache-mutex cache)))
  (with-cache-index cache (proc (cache-state cache) state)))

(define (raise-cache-abandon-exception message)
  (raise (make-condition &message 'message message)))

(define cache-abandon-exception
  (make-condition &message 'message "cache entry abandoned"))

(define-inline (%%cache-set! cache entry owner cont results) ; preco: entry is locked
  (if (eq? owner (cache-entry-thread entry))
      (begin
#|
	(if (and (thread? owner) (thread-alive? owner))
	    (thread-signal! t 'cache-entry-killed))
|#
	((cache-delete cache) (cache-state cache) (cache-entry-state entry))
	(cache-value-fulfiled! cache entry (and (eq? cont values) results))
	(set-entry-value! entry results cont)
	(if owner
	    (handler-case
	     (thread-deliver-signal! owner cache-abandon-exception)
	     ((<condition>)))))))

(define (%cache-set! cache key job . res)
  (hang-on-mutex! '%cache-set!
		  (let ((entry (cache-lookup cache key)))
		    (if entry (list (cache-entry-mutex entry)) '())))
  (let ((entry (cache-lookup cache key)))
    (if entry
	(let ((owner (cache-entry-thread entry)))
	  ;; Kill the owner if we can, that is, if it does not yet hold the entry.
	  (if owner
	      (with-cache-entry entry (%%cache-set! cache entry owner job res))
	      (with-cache-entry entry (%%cache-set! cache entry owner job res))))
	(with-cache-index cache (cache-find! cache key (lambda () (apply job res)))))))

;; (cache-set! cache key . job+res)
;;
;; if (null? job+res): remove entry
;; if (null? (cdr job+res)):
;;   (eq? (car job+res) #t): reset to last valid, unfulfiled last thunk
;;   (procedure? (car job+res)): reset to valid, unfilfiled (car job+res)
;; else: set to (apply (car job+res) (cdr job+res))

;;(: cache-set! ((struct <cache>) * #!rest -> . *))
(define (cache-set! cache key . job+res)  
  (cond
   ((null? job+res)
    (and-let* ((entry (cache-lookup cache key)))
	      ((cache-delete cache) (cache-state cache) (cache-entry-state entry))
	      (cache-entry-delete! cache entry)))
   ((eq? (car job+res) #t) (cache-invalid! cache key))
   ((null? (cdr job+res)) (cache-invalid! cache key (car job+res)))
   (else (apply %cache-set! cache key job+res))))

#|

Keep the full code until the RScheme version (at least) is forked.

(define (cache-invalid/check! check cache key . thunk)
  (let ((entry (cache-lookup cache key)))
    (if (and entry (check entry))
	(with-cache-index
	 cache
	 (let ((entry (cache-lookup cache key))) ;; TODO remove the lock and double lookup
	   (if (and entry (check entry))
	       (with-cache-entry
		entry
		(cache-entry-cont-set! entry #f)
		;; FIXME: should we t(h)erminate/grill the thread somehow?
		(cache-entry-thread-set! entry #f)
		((cache-delete cache) (cache-state cache) (cache-entry-state entry))
		(cache-entry-state-set! entry ((cache-miss cache) (cache-state cache) key))
		(if (pair? thunk) (cache-entry-thunk-set! entry (car thunk)))
		entry))))
	(begin
	  (if (and (not entry) (pair? thunk))
	      (let ((entry (cache-find cache key (car thunk))))
		(cache-entry-ref/default cache key entry #f)
		entry)
	      entry)))))
|#

(define (cache-invalid/check! check cache key . thunk)
  ;; may hang on the 'cache' itself, even though the current code will not
  (hang-on-mutex! 'cache-invalid/check! (list (cache-mutex cache)))
  (hang-on-mutex! 'cache-invalid/check!
		  (let ((entry (cache-lookup cache key)))
		    (if entry (list (cache-entry-mutex entry)) '())))
  (let ((entry (cache-lookup cache key)))
    (if (and entry (check entry))
	(with-cache-entry
	 entry
	 (cache-entry-cont-set! entry #f)
	 ;; FIXME: should we t(h)erminate/grill the thread somehow?
	 (cache-entry-thread-set! entry #f)
	 ((cache-delete cache) (cache-state cache) (cache-entry-state entry))
	 (cache-entry-state-set! entry ((cache-miss cache) (cache-state cache) key))
	 (if (pair? thunk) (cache-entry-thunk-set! entry (car thunk)))
	 entry)
	(begin
	  (if (and (not entry) (pair? thunk))
	      (let ((entry (cache-find cache key (car thunk))))
		(cache-entry-ref/default cache key entry #f)
		entry)
	      entry)))))

;; (cache-invalid! cache key . thunk)
;;
;; Invalidate the cached values.  If there's a running computation,
;; leave it running.  If thunk is given, it's arranged to be called,
;; otherwise default is returned.

;;(: cache-invalid! ((struct <cache>) * #!rest procedure -> *))
(define (cache-invalid! cache key . thunk)
  (define (check entry)
    (and-let* ((avail (cache-entry-cont entry)))
	      ((cache-valid? cache) (cache-entry-state entry))))
  (apply cache-invalid/check! check cache key thunk))

;; (cache-invalid/abort! cache key . thunk)
;;
;; Invalidate the cached values and abort any running computation. If
;; thunk is given, it's arranged to be called, otherwise default is
;; returned.

;;(: cache-invalid/abort! ((struct <cache>) * #!rest procedure -> *))
(define (cache-invalid/abort! cache key . thunk)
  (define (check entry)
    (let ((avail (cache-entry-cont entry)))
      (or (not avail)
	  ((cache-valid? cache) (cache-entry-state entry)))))
  (apply cache-invalid/check! check cache key thunk))

;; (cache-ref/default cache key thunk default)
;;
;; Returns current cached value or default.  Does never wait.  If
;; thunk is given, it's arranged to be called, otherwise default is
;; returned.

(: cache-ref/default ((struct <cache>) * (or boolean (procedure () . *)) * -> . *))
(define (cache-ref/default cache key thunk default)
  (if thunk
      (cache-entry-ref/default cache key (cache-find cache key thunk) default)
      (let ((entry (cache-lookup cache key)))
	(if entry (cache-entry-ref/default cache key entry default) default))))

;; (cache-ref cache key thunk . default)
;;
;; If default is given falls back to cache-ref/default.  Otherwise
;; returns the last valid cached values.  Always waits for valid
;; values, possibly arranging thunk to produce them.

(: cache-ref ((struct <cache>) * (procedure () . *) -> . *))
(define (cache-ref cache key thunk)
  (cache-entry-ref cache key (cache-find cache key thunk)))

(: cache-reref ((struct <cache>) * (procedure () . *) -> . *))
(define (cache-reref cache key thunk)
  (cache-entry-ref cache key (cache-invalid! cache key thunk)))

;; (cache-fold cache f nil)
;;
;; fold f(key value nil) over cache content

(: cache-fold ((struct <cache>) (procedure (* * *) . *) * -> . *))
(define (cache-fold cache f nil)
  (cache-node-fold (lambda (v nil)
		     (if (eq? (cache-entry-cont v) values)
			 (f (cache-entry-key v) (car (cache-entry-values v)) nil)
			 nil))
		   nil
		   (cache-index cache)))

;; (cache-cleanup! cache [valid?] [used?])
;;
;; valid? : default: (cache-valid cache)
;; used?  : no default; of cache result values arity.
;;
;; Remove all entries, which are not "valid?" and "used?" (if given).
;; Used is applied to the cached values.

;;(: cache-cleanup! ((struct <cache>) #!rest (procedure (*) boolean) procedure -> undefined))
(define (cache-cleanup! cache . predicates)
  (hang-on-mutex! 'cache-cleanup! (list (cache-mutex cache)))
  (let ((valid? (if (and (pair? predicates) (procedure? (car predicates)))
		    (car predicates) (cache-valid? cache)))
	(used? (if (and (pair? predicates) (pair? (cdr predicates)))
		   (cadr predicates) #t))
	(index (cache-index cache))
	(del (cache-delete cache)))
    (let ((removeable (cache-node-fold
		       (lambda (entry init)
			 (if (or (cache-entry-thread entry)
				 (and (valid? (cache-entry-state entry))
				      (pair? (cache-entry-values entry))
				      (or (eq? used? #t)
					  (apply used? (cache-entry-values entry)))))
			     init
			     (with-cache-entry
			      entry
			      (guard
			       (ex (else ;; (log-condition (cache-name cache) ex)
				    #f))
			       (del (cache-state cache) (cache-entry-state entry))
			       ;; FIXME: should we t(h)erminate/grill the thread somehow?
			       (cache-entry-thread-set! entry #f))
			      (cons entry init))))
		       '()
		       index)))
      (with-cache-index
       cache
       (for-each
	(lambda (entry) (cache-entry-delete! cache entry))
	removeable)))))
