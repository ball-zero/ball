;; (C) 1999-2003, 2008, 2010, 2011, 2013 Joerg F. Wittenberger see http://www.askemos.org

(define-macro (: name . typedeclaration) `(begin))
(define-macro (the type expression) expression)
(define-macro (xthe type expression) expression)

(define-syntax always-assert
  (syntax-form (expr)
	       (if (not expr)
		   (error "runtime assertion failed in ~s: ~s"
			  (*FUNCTION*)
			  (mquote expr))))
  (syntax-form (expr condition) (if (not expr) (error condition)))
  (syntax-form (expr . msg+args) (if (not expr) (error . msg+args))))

(cond-expand
 (debug-lock

  ;; *retained-mutex-set* would be better within the cond-expand, but does not work.
  (define *retained-mutex-set* (make-parameter '()))

  (define-macro (dbgname base fmt . rest)
    `(format ,fmt ,base ,@rest))

  (define (hang-on-mutex-report/logerr where mutex)
    (format (current-error-port) "WARNING: ~a may deadlock on ~a\n" where (mutex-name mutex)))

  (define (hang-on-mutex-report/raise where mutex)
    (error "WARNING: ~a may deadlock on ~a\n" where (mutex-name mutex)))

  (define hang-on-mutex-report hang-on-mutex-report/logerr)

  (define-macro (retain-mutex mutex . body)
    `(parameterize
      ((*retained-mutex-set* (cons ,mutex (*retained-mutex-set*))))
      (with-mutex ,mutex . ,body)))

  (define-macro (hang-on-mutex! where muxlist)
    `(for-each
      (lambda (m)
	(if (memq m (*retained-mutex-set*))
	    (hang-on-mutex-report ,where m)))
      ,muxlist))

  (define-macro (yield-mutex-set! . body)
    `(parameterize
      ((*retained-mutex-set* '()))
      . ,body))
  )
 (else

  (define *retained-mutex-set* "*retained-mutex-set* (compiled without util_FID=debug-lock)")

  (define-macro (dbgname base fmt . rest) fmt)

  (define-macro (retain-mutex mutex . body)
    `(with-mutex ,mutex . ,body))
  (define-macro (hang-on-mutex! loc expr) `(begin))
  (define-macro (yield-mutex-set! . body) `(begin . ,body))
  (define-macro (hang-on-mutex-report . body)
    (error "hang-on-mutex-report somwhow referenced white debug-lock inactive"))))

(define-method force ((self <recursive-promise>) . condition-handler)
  (let ((content (box self)))
    (case (car content)
      ((#f)
       (handler-case
	(call-with-values (cdr content)
	  (lambda tail
	    (if (and (pair? tail) (null? (cdr tail))
		     (recursive-promise? (car tail)))
		(let ((content (box self)))
		  (if (not (car content))
		      (let ((x (box (car tail))))
			(set-car! content (car x))
			(set-cdr! content (cdr x))
			(set-box! (car tail) content)))
		  (force self))
		(apply (car content) tail))))
	((<object> condition: c)
	 (let ((content (box self)))
	   (if (not (car content))
	       (begin
		 (set-car! content #t)
		 (set-cdr! content c)))
	   ((if (pair? condition-handler) (car condition-handler) reraise) (cdr content))))))
      ((#t)
       ((if (pair? condition-handler) (car condition-handler) reraise) (cdr content)))
      (else (apply (car content) (cdr content))))))

(define get-environment-variable getenv)

(define (yield-system-load)
  ; (thread-yield!)
  #f)

;; TODO make-a-transient-copy-of-object should be copy-lisp, copy,
;; clone or something like that.  It's supposed to create a fresh copy
;; equal?  to it's argument.  Because it's here used for just one
;; occurence, I'll make it more simple.
(define (make-a-transient-copy-of-object x)
  (map (lambda (i) (map identity i)) x))

(define-macro (fx- a b) `(fixnum- ,a ,b))
(define-macro (fx+ a b) `(fixnum+ ,a ,b))
(define-macro (fx* a b) `(fixnum* ,a ,b))
(define-macro (fx>= a b) `(fixnum>=? ,a ,b))
(define-macro (fx< a b) `(fixnum<? ,a ,b))
(define-macro (string-split/include-empty str delim) `(string-split ,str ,delim))

(define-macro (substring/shared s f t) `(substring ,s ,f ,t))

;; FIXME: we need to rewrite the code to use "real" Scheme strings
;; with no terminating zero eventually.  Serious changes to rscheme
;; required.  However the state of affairs is wasteful.

(define-safe-glue (make-string/uninit (s <raw-int>))
{
  REG0 = bvec_alloc(s+1, string_class);
  RETURN1();
})

(define-macro (make-shared-parameter . args)
  `(make-parameter . ,args))

#|
(define-macro (dbgname base . rest)
  base)
|#

(define (abandoned-mutex-exception? ex) #f)

;;* RScheme Call Backtrace

(define (ll->partial (llc <function>))
  (gvec-ref (environment llc) 1))

(define (continuation->partial (cc <function>))
  (ll->partial (gvec-ref (environment cc) 1)))

(define mutex-lock!!
  (let ((ml mutex-lock!))
    (define (mutex-lock!! m)
      (let ((cth (current-thread))
	    (o (mutex-owner m)))
	(if (eq? o cth)
	    (let ((c (count m)))
	      (logerr "thread ~a will block on mutex ~s (~a) already\n-------ORIGINALLY----------\n~a\n-------RELOCK----------\n~a\n"
		      cth m c
		      (with-output-to-string
			(lambda () (print (continuation->partial (mutex-specific m)))))
		      (with-output-to-string
			(lambda () (print (call-with-current-continuation
					   (lambda (cc)
					     (continuation->partial cc))))))))
            (begin
              (ml m)
              ;; remember where we grabbed the lock...
              (call-with-current-continuation
               (lambda (cc)
                 (mutex-specific-set! m cc)))))))
    mutex-lock!!))

(define-safe-glue (move-memory! from to (n <raw-int>) (foffset <raw-int>) (toffset <raw-int>))
{
#define min(a, b) (a) < (b) ? (a) : (b)
 void *f = STRING_P(from) ? string_text(from) : OBJ_TO_RAW_PTR(from);
 void *t = STRING_P(to) ? string_text(to) : OBJ_TO_RAW_PTR(to);
 if (f && t) {
   if( STRING_P(from) ) {
     foffset = min(string_length(from)-1, foffset);
     n = min(string_length(from)-foffset, n);
   }
   if( STRING_P(to) ) {
     toffset = min(string_length(to)-1, toffset);
     n = min(string_length(to)-toffset, n);
   }
   f = (char*)f + foffset;
   t = (char*)t + toffset;
   memcpy(t, f, n);
   REG0 = TRUE_OBJ;
 } else {
   REG0 = FALSE_OBJ;
 }
 RETURN1();
})

(define-safe-glue (memory-set! obj c (start <raw-int>)  (len <raw-int>))
{
 char *p = STRING_P(obj) ? string_text(obj) : OBJ_TO_RAW_PTR(obj);
 memset( p + start, ASCII_CHAR_VALUE(c), len);
 RETURN0();
})

(define (srfi:string-fill! s char . start+end)
  (let* ((as (pair? start+end))
	 (start (if as (car start+end) 0))
	 (ep (and as (cdr start+end)))
	 (ae (pair? ep))
	 (end (if ae (car ep) (string-length s))))
    ;; should we assert fixnumity like that?
    (fixnum+ start 0)
    (memory-set! s char start (fixnum- end start))))

;; (define-class <weak-blob-ptr> (<object>) :weak1
;;   referrer)

(define *transient-blobs* (make-symbol-table))

(define (semaphore-value x) (count x))

(define (semaphore-delete! sema)
  (handler-case
   (dequeue-for-each
    (lambda (t) (thread-deliver-signal! t 'abandoned-semaphore))
    sema)
   ((<condition>))))

;; (define (register-blob-ref! sha256 obj)
;;   (let ((r (%make <weak-blob-ptr> obj)))
;;     (table-insert! *transient-blobs*
;; 		   sha256 (cons r (or (table-lookup *transient-blobs* sha256) '())))
;;     r))

;; (define (unregister-blob-ref! sha256 obj)
;;   (let* ((changed #f)
;; 	 (r (filter
;; 	     (lambda (r)
;; 	       (if (and (referrer r)
;; 			(not (eq? (referrer r) obj)))
;; 		   #t
;; 		   (begin (set! changed #t) #f)))
;; 	     (or (table-lookup *transient-blobs* sha256) '()))))
;;     (if changed	(table-insert! *transient-blobs* sha256 r))))

;; (define (non-transient-blob? sha256)
;;   (let ((entry (table-lookup *transient-blobs* sha256)))
;;     (or (not entry)
;; 	(let* ((changed #f)
;; 	       (live-references (filter (lambda (r)
;; 					  (if (referrer r) #t
;; 					      (begin
;; 						(set! changed #t)
;; 						#f)))
;; 					entry)))
;; 	  (if (null? live-references)
;; 	      (begin
;; 		(table-remove! *transient-blobs* sha256)
;; 		#t)
;; 	      (begin
;; 		(if changed (table-insert! *transient-blobs* sha256 live-references))
;; 		#f))))))

;; (define (register-blob-ref! sha256 obj)
;;   (let* ((old (table-lookup *transient-blobs* sha256))
;; 	 (replaced (table-insert! *transient-blobs* sha256
;; 				  (list (if old (add1 (car old)) 1)))))
;;     (if (not (eq? old replaced))
;; 	(register-blob-ref! sha256 obj)
;; 	obj)))

;; (define (unregister-blob-ref! sha256 obj)
;;   (let* ((old (table-lookup *transient-blobs* sha256))
;; 	 (replaced (and old
;; 			(if (eqv? (car old) 1)
;; 			    (table-remove! *transient-blobs* sha256)
;; 			    (table-insert! *transient-blobs* sha256
;; 					   (list (sub1 (car old))))))))
;;     (if (not (eq? old replaced))
;; 	(if (and old (eqv? (car old) 1))
;; 	    (let loop ()
;; 	      (let ((old (table-lookup *transient-blobs* sha256))
;; 		    (clobbered (table-insert! *transient-blobs* sha256
;; 					      (list (+ (or (and old (car old)) 0)
;; 						       (car replaced))))))
;; 		(if (not (eq? old clobbered)) (loop))))
;; 	    (unregister-blob-ref! sha256 obj)))))

;; (define (non-transient-blob? sha256)
;;   (not (table-lookup *transient-blobs* sha256)))

;; DEPRECIATED 'receive*' is similar to srfi 8, but binds values to
;; #f, if expr returns too few values.  (remark: there seems to be no
;; performance difference between the 'bind' of rscheme and the
;; srfi 8 implementation.
(define-macro (receive* vars expr . body)
  `(bind ((,@vars ,expr)) . ,body))
(define-macro (receive-srfi vars expr . body)
  `(call-with-values (lambda () ,expr)
                     (lambda ,vars . ,body)))

;;;** dynamic-wind went into rscheme

;; (define (dynamic-wind before thunk after)
;;  (unwind-protect* after before 'dynamic-wind thunk))

;;* bind-exit

(define bind-exit call-with-current-continuation)

;;** combine returns a function which combines the argument operators
;;   in sequence from left to right.

;; (define (combine* operation operators)  ; for internal use
;;   (if (pair? operators)
;;       (lambda object
;;         (call-with-values (lambda () (apply operation object))
;;           (combine* (car operators) (cdr operators))))
;;       operation))

;; (define (combine f . fns) (combine* f fns))

;; Many thanks to Donovan Kolbly for coding the equivalent at the
;; level where it belongs:

(define-glue (combiner) :template
{
  USE_FUNCTION_ENVT();
  {
    PUSH_PARTCONT( combiner_1, 1 );
    SET_PARTCONT_REG( 0, LEXREF0(0) );
  }
  RETURN(arg_count_reg);
}
("combiner_1" {
  obj lst = PARTCONT_REG(0);
  RESTORE_CONT_REG();

  if (NULL_P( lst )) {
    RETURN( arg_count_reg );
  } else {
    PUSH_PARTCONT( combiner_1, 1 );
    SET_PARTCONT_REG( 0, pair_cdr(lst) );
    APPLY( arg_count_reg, pair_car(lst) );
  }
}))

(define (combine lst)
  (make-gvec <closure>
             combiner
             (make-gvec <binding-envt> '() lst)))


(define-macro (turing-tape slots init next)
  (let ((n (gensym)))
    `(lambda ()
       (letrec ((,n (lambda ,slots (values . ,next)))
                (loop (lambda ,slots
                        (values
                         (lambda ()
                           (call-with-values (lambda () (,n . ,slots))
                             loop))
                         . ,slots))))
         (loop . ,init)))))

;; Example: (define natural (turing-tape (i) (1) ((+ i 1))))
;; (natural)     -> values 1: iterator thunk, value 2: 1
;; ((natural))   -> values 1: iterator thunk, value 2: 2
;; (((natural))) -> values 1: iterator thunk, value 2: 3

;;** call-with-list-extending1

(define (call-with-list-extending1 proc)
  (receive (list last) (call-with-list-extending proc) list))

;;; andmap + ormap:

(define (andmap f first . rest)
  (cond ((null? rest)
         (let loop ((l first))
           (or (null? l)
               (and (f (car l)) (loop (cdr l))))))
        ((null? (cdr rest))
         (let loop ((l1 first) (l2 (car rest)))
           (or (null? l1)
               (and (f (car l1) (car l2)) (loop (cdr l1) (cdr l2))))))
        (else
         (let loop ((first first) (rest rest))
           (or (null? first)
               (and (apply f (car first) (map car rest))
                    (loop (cdr first) (map cdr rest))))))))

(define (ormap f first . rest)
  (cond
   ((null? first) (or))
   ((null? rest) (let loop ((first first))
                   (and (pair? first)
                        (or (f (car first))
                            (loop (cdr first))))))
   (else (let loop ((lists (cons first rest)))
           (and (pair? (car lists))
                (or (apply f (map car lists))
                    (loop (map cdr lists))))))))

;; Environment handling

(define (make-environment . flag)
  (if (and (pair? flag) (car flag))
      (error "make-environment flag not implemented"))
  (make-top-level-contour))

(define (environment? obj) (instance? obj <top-level-contour>))

(define (environment-copy env . flag)
  (if (and (pair? flag) (car flag))
      (error "environment-copy flag not implemented"))
  (copy-top-level-contour env))

(define (environment-ref envt symbol)
  (value (or (table-lookup (table envt) symbol)
	     (error (format "variable \"~a\" not bound" symbol)))))

(define (environment-extend! envt symbol . rest)
  (bind! envt
         (make <top-level-var>
           name: symbol
           value: (if (pair? rest)
                      (car rest))
           write-prot: (and (pair? rest) (pair? (cdr rest)) (cadr rest)))))

(define (environment-remove! env symbol)
  (table-remove! (table env) symbol))

(define enable-warnings (make-shared-parameter #f))

(define (condition->string/with-stack-trace ex s)
  (with-output-to-string
    (lambda ()
      (format #t "~a\n" ex)
      (format #t "+---------------------------<STACK>---\n")
      (print (vm-continuation-reg (cdr s)))
      (format #t "------------------------------<ABT>---\n")
      (with-module repl (apply-backtrace* 
			 (vm-dynamic-state-reg (cdr s)))))))

(define (condition->string ex)
  (define s (and (enable-warnings)
		 (pair? (properties ex))
		 (assq 'stack (properties ex))))
  (cond
   ((and (condition? ex) (condition-has-type? ex &message))
    (if s (condition->string/with-stack-trace ex s) (condition-message ex)))
   (else
    (if s (condition->string/with-stack-trace ex s) (format #f "~a" ex)))))

(define (condition->fields ex)
  (values (if (and (condition? ex) (condition-has-type? ex &message))
	      (condition-message ex)
	      (format #f "~a" ex))
	  (class-name (object-class ex))
          (let ((s (and (enable-warnings)
			(pair? (properties ex))
			(assq 'stack (properties ex)))))
	    (if s `(pre
		    ,(with-output-to-string
                       (lambda ()
                         (format #t "+---------------------------<STACK>---\n")
                         (print (vm-continuation-reg (cdr s)))
                         (format #t "------------------------------<ABT>---\n")
                         (with-module repl (apply-backtrace* 
                                            (vm-dynamic-state-reg (cdr s)))))))
		'()))
	  '()))

(define (condition->fields+ ex)
  (let* ((ex (let loop ((ex ex))
	      (or (and (uncaught-exception? ex)
		       (loop (uncaught-exception-reason ex)))
		  ex)))
	 (s  (and-let*
	      (((enable-warnings))
	       ((pair? (properties ex)))
	       (s (assq 'stack (properties ex))))
	      `(pre
		,(with-output-to-string
		   (lambda ()
		     (format #t "+---------------------------<STACK>---\n")
		     (print (vm-continuation-reg (cdr s)))
		     (format #t "------------------------------<ABT>---\n")
		     (with-module repl (apply-backtrace* 
					(vm-dynamic-state-reg (cdr s))))))))))
    (cond
     ((and (condition? ex) (condition-has-type? ex &message))
      (values 
       (call-with-output-string (lambda (port) (write-object ex port)))
       (condition-message ex)
       (or s '()) '()))
     ((eof-condition? ex)
      (values "eof-condition" (eof-condition-reason ex) (or s '()) '()))
     (else
      (values (class-name (object-class ex)) (format #f "~a" ex) (or s '()) '())))))

;;** SRFI-18/SRFI-21 exceptions

(define (current-exception-handler) (car *handler-chain*))

;; KLUDGE These are partial implementations, which serve the purpose
;; of Askemos (Nov 2002).

(define sp-lmu
  (let ((mu (make-mutex "Pipes")))
    (lambda (d p)
      (with-mutex
       mu
       (monitor-value
        'OPipes ((if (eq? d 'up) cons delete)
                 p (or (table-lookup *monitored-values* 'OPipes) '())))))))

;; http://www.call-with-current-continuation.org/eggs/mailbox2.html see also timeout.scm

(define (mailbox? obj) (instance? obj <mailbox>))

;; The underlying mailbox implementation should do the cloning.

(define (mailbox-clone mailbox)
  (let ((m (clone mailbox)))
    (set-state! m (clone (state mailbox)))
    m))

(define (mailbox-has-message? mailbox message equal)
  (let ((s (state mailbox)))
    (let loop (((i <fixnum>) (front mailbox)))
      (and ;; (has-data? mailbox)
	   (not (eqv? i (back mailbox)))
	   (or (equal (vector-ref s i) message)
	       (loop (modulo (add1 i) (vector-length s))))))))

(define-macro (mailbox-number-of-items dq) `(dequeue-count ,dq))

(define (mailbox-drop-matching! dq pred)
  (let loop ()
    (cond
     ((dequeue-empty? dq) #t)
     ((pred (vector-ref (state dq) (front dq)))
      (receive-message! dq) (loop))
     (else #f))))

;;; this went into rscheme base

;; (define-class <named-future> (<future>)
;;   name)

;; (define (thunk->named-future thunk name)
;;   (let ((f (make <named-future> name: name)))
;;     (thread-resume
;;      (make-thread (lambda ()
;; 		    (set-future-values-from-thunk f thunk))
;; 		  (to-string name)))
;;     f))

;; (define-syntax (nfuture expr name)
;;   (thunk->named-future (lambda () expr) name))

;; (define-method write-object ((self <named-future>) port)
;;   (format port "#[<future> ~s]" (name self)))

;; SRFI 34

(define (with-exception-handler handler thunk)
  (thread-let ((*handler-chain* (cons handler *handler-chain*)))
              (thunk)))

(define-macro (guard clause . body)
  (let ((guard-k (gensym))
        (handler-k (gensym))
	(condition (gensym)))
    `((call-with-current-continuation
       (lambda (,guard-k)
         (with-exception-handler
          (lambda (,condition)
            ((call-with-current-continuation
              (lambda (,handler-k)
                (,guard-k
                 (lambda ()
                   (let ((,(car clause) ,condition)) ; clauses may SET! var
                     ,(let loop ((clauses (cdr clause)))
                        (if (null? clauses)
                            `(,handler-k (lambda ()
                                           (raise ,condition)))
                            (let ((c (car clauses)))
                              (cond
                               ((eq? 'else (car c))
                                (if (null? (cdr c))
                                    '#f
                                    (if (null? (cddr c))
                                        (cadr c)
                                        `(begin . ,(cdr c)))))
                               ((and (pair? c) (pair? (cdr c))
                                     (eq? '=> (cadr c)))
                                (let ((v (gensym)))
                                  `(let ((,v ,(car c)))
                                     (if ,v
                                         (,(caddr c) ,v)
                                         ,(loop (cdr clauses))))))
                               ((and (pair? c) (null? (cdr c)))
                                (let ((v (gensym)))
                                  `(let ((,v ,(car c)))
                                     (if ,v ,v ,(loop (cdr clauses))))))
                               ((pair? c)
                                `(if ,(car c)
                                     ,(if (null? (cddr c))
                                          (cadr c)
                                          `(begin . ,(cdr c)))
                                     ,(loop (cdr clauses))))
                               (else (error "guard syntax error in ~a" c)))))))))))))
          (lambda ()
            (call-with-values
                (lambda () . ,body)
              (lambda args
                (,guard-k (lambda ()
                            (apply values args))))))))))))

(define (srfi34:raise c)
  (let loop ((chain *handler-chain*))
    (if (pair? chain)
	(let ((h (car chain)))
	  (if (instance? h <handler-context>)
              (if (instance? c (condition-class h))
                  ((handler-proc h) 
                   ;; first arg to handler function is the condition
                   c
                   ;; second arg is the next-handler proc
                   (lambda ()
                     ;; work around bug in compiler that will think `loop'
                     ;; is optimizable because it's only ever called from
                     ;; tail position -- never mind it's in a different proc!
                     loop
                     ;; keep looking...
                     (loop (cdr chain))))
                  (loop (cdr chain)))
              (thread-let ((*handler-chain* (cdr chain)))
                          (h c)))
	  (logerr "Handler ~a returned on ~a\n" h c))
        ;; (process-abort "unhandled exception" c)
        (begin
          (logerr " Unhandled exception ~a ~a\n" (current-thread) c)
          (halt-thread (current-thread) c)))))

(set! *signal-handler* srfi34:raise)

;; (define (logging-thread-backstop-handler condition next)
;;   (logerr " ExnBackStop ~a ~a\n" (current-thread) condition)
;;   (halt-thread (current-thread) condition))

;; (with-module
;;  mlink
;;  (let ((target (top-level-envt (get-module 'rs.sys.threads.manager))))
;;    (set-value! (table-lookup (table target) 'thread-backstop-handler)
;;                logging-thread-backstop-handler)))

;(define (current-exception-handler)
;  (let ((current *handler-chain*))
;    (lambda (exc)
;      (thread-let ((*handler-chain* current))
;                  (signal exc)))))

;(define-macro (handle-exceptions exn handler . body)
;  `(handler-case
;    (begin . ,body)
;    ((<condition> condition: ,exn) ,handler)))

;; (define (raise obj) (error obj))

(define raise srfi34:raise)

;; I can't find out how to write a rewriter for rscheme.  This is
;; probably not correct.  Please help to fix it.  There is a reference
;; implementation and also one in the chicken directory.

(define-rewriter (dsssl-guard form)
  (let ((clause (or (and (pair? form) (cadr form))
		    (error "guard: syntax error ~a" form)))
	(body (cddr form))
	(guard-k (gensym)))
    `((call-with-current-continuation
       (lambda (,guard-k)
	 (with-exception-handler
	  (lambda (,(car clause))
	    (,guard-k
	     (lambda ()
	       ,(let loop ((clauses (cdr clause)))
		  (if (null? clauses)
		      `(raise ,(car clause))
		      (let ((c (car clauses)))
			(cond
			 ((eq? 'else (car c))
			  (if (null? (cdr c))
			      '#f
			      (if (null? (cddr c))
				  (cadr c)
				  `(begin . ,(cdr c)))))
			 ((and (pair? c) (pair? (cdr c)) (eq? '=> (cadr c)))
			  (let ((v (gensym)))
			    `(let ((,v ,(car c)))
			       (if ,v (,(caddr c)) ,(loop (cdr clauses))))))
			 ((and (pair? c) (null? (cdr c)))
			  (let ((v (gensym)))
			    `(let ((,v ,(car c)))
			       (if ,v ,v ,(loop (cdr clauses))))))
			 ((pair? c)
			  `(if ,(car c)
			       ,(if (null? (cddr c))
				    (cadr c)
				    `(begin . ,(cdr c)))
			       ,(loop (cdr clauses))))
			 (else (error "guard syntax error in ~a" c)))))))))
	  (lambda ()
            (call-with-values
                (lambda () . ,body)
              (lambda args
                (,guard-k (lambda ()
                            (apply values args))))))))))))

;;; chicken compatible pathname operations (unix only) :

(define (absolute-pathname? pn)
  (and (not (string-null? pn)) (char=? (string-ref pn 0) #\/)) )

(define (chop-pds str pds)
  (and str
       (let ((len (string-length str))
	     (pdslen (if pds (string-length pds) 1)))
	 (if (and (fx>= len 1)
		  (if pds
		      (has-suffix? pds str)
		      (let ((c (string-ref str (sub1 len))))
			(or (eqv? c #\/) (eqv? c #\\))) ) )
	     (substring str 0 (fx- len pdslen))
	     str) ) ) )

(define make-pathname #f)
(define make-absolute-pathname #f)

(let ((def-pds "/") )

  (define (conc-dirs dirs pds)
    (let loop ((strs dirs))
      (if (null? strs)
	  ""
	  (let ((s1 (car strs)))
	    (if (string-null? s1)
		(loop (cdr strs))
		(string-append 
		 (chop-pds (car strs) pds)
		 (or pds def-pds)
		 (loop (cdr strs))) ) ) ) ) )

  (define (canonicalize-dirs dirs pds)
    (cond ((or (not dirs) (null? dirs)) "")
	  ((string? dirs) (conc-dirs (list dirs) pds))
	  (else           (conc-dirs dirs pds)) ) )

  (define (_make-pathname dir file ext pds)
    (let ((ext (or ext ""))
	  (file (or file ""))
	  (pdslen (if pds (string-length pds) 1)) )
      (string-append
       dir
       (if (and (fx>= (string-length file) pdslen)
		(if pds
		    (has-prefix? pds file)
		    (let ((c (string-ref file 0)))
		      (or (eqv? c #\/) (eqv? c #\\)))))
	   (substring file pdslen (string-length file))
	   file)
       (if (and (fixnum>? (string-length ext) 0)
		(not (char=? (string-ref ext 0) #\.)) )
	   "."
	   "")
       ext) ) )

  (set! make-pathname
    (begin
      (define (make-pathname dirs file . ext+pds)
	(let ((ext (and (pair? ext+pds) (car ext+pds)))
	      (pds (and (pair? ext+pds) (pair? (cdr ext+pds)) (cadr ext+pds))))
	  (_make-pathname (canonicalize-dirs dirs pds) file ext pds)))
      make-pathname))

  (set! make-absolute-pathname
	(begin
	  (define (make-absolute-pathname dirs file . ext+pds)
	    (let ((ext (and (pair? ext+pds) (car ext+pds)))
		  (pds (and (pair? ext+pds) (pair? (cdr ext+pds)) (cadr ext+pds))))
	      (_make-pathname
	       (let ((dir (canonicalize-dirs dirs pds)))
		 (if (absolute-pathname? dir)
		     dir
		     (string-append (or pds def-pds) dir)) )
	       file ext pds)) )
	  make-absolute-pathname) ) )

(define *meta-continuation*
  (make-parameter
   (lambda (v)
     (error "You forgot the top-level reset..."))))

(define (*abort thunk)
  (call-with-current-continuation
   (lambda (k)
     (lambda ()
       (k ((*meta-continuation*) (thunk)))))))

(define *reset
  (lambda (thunk)
    (let ((mc (*meta-continuation*)))
      (call-with-current-continuation
        (lambda (k)
	  (begin
	    (*meta-continuation*
	     (lambda (v)
	       (*meta-continuation* mc)
	       (k v)))
	    (*abort thunk)))))))

(define (*shift f)
  (call-with-current-continuation
   (lambda (k)
     (*abort (lambda ()
	       (f (lambda (v)
		    (*reset (lambda () (k v))))))))))

(define-rewriter (reset form)
  `(*reset (lambda () ,(cadr form))))

(define-rewriter (shift form)
  `(*shift (lambda (,(cadr form)) ,(caddr form))))

(define-record-type <zipper>
  (make-zipper z-curr-node z-k)
  zipper?
  (z-curr-node zipper-cursor)
  (z-k zipper-next))

(define exit process-exit)

(define (process-fork . thunk)
  (if (pair? thunk)
      (let ((child (fork)))
        (or child ((car thunk))))
      (or (fork) 0)))

(define current-process-id getpid)

(define-macro (process-wait pid)
  `(values ,pid #t (wait-for (process-id ,pid))))

(define (process-kill process)
  (kill (process-id process) 9))

(define (run-env* cmd args fds dir env #optional new-pgroup?)
  (let* ((cmd-str (as-string cmd))
	 (cmd-file (if (string-search cmd-str #\/)
		       (if (file-execute-access? cmd-str)
			   cmd-str
			   (error "~a: not accessible in `execute' mode" cmd))
		       (find-in-path cmd-str)))
	 (p (make <process>
		  name: cmd)))
    (guard
     (ex (else (pipe-condition-handler ex)))
     (fork-and-exec
      p
      cmd-file
      (list->vector (cons cmd-file
			  (map (lambda (a)
				 (as-string a))
			       args)))
      dir
      env
      fds
      new-pgroup?))
    p))

;; pipe-condition-handler is only temporary.  We ran out of file
;; descriptors and might find that fatal.

(define pipe-condition-handler raise)

(define (make-process-stub args dir . env)
  (define to-cmd-read #f)
  (define to-cmd-write #f)
  (define from-cmd-read #f)
  (define from-cmd-write #f)
  (guard
   (ex (else (logerr "run-env* ~a\n" ex)
	     (if to-cmd-read (file-close to-cmd-read))
	     (if to-cmd-write (file-close to-cmd-write))
	     (if from-cmd-read (file-close from-cmd-read))
	     (if from-cmd-write (file-close from-cmd-write))
	     (raise ex)))
   (receive
    (a b) (guard (ex (else (pipe-condition-handler ex))) (pipe))
    (fd-set-blocking b #f)
    (set! to-cmd-read a)
    (set! to-cmd-write b))
   (receive
    (a b) (pipe)
    (set! from-cmd-read a)
    (set! from-cmd-write b))
   (let ((str (srfi:string-join args))
	 (process (run-env*
		    (car args) (cdr args)
		    (vector to-cmd-read from-cmd-write 2)
		    dir
		    (if (pair? env)
			(apply vector
			       (append
				(map (lambda (x)
				       (string-append (car x) "=" (cdr x)))
				     (car env))
				(vector->list
				 (process-environment-as-vector))))
			(process-environment-as-vector)))))
     (file-close to-cmd-read)
     (file-close from-cmd-write)
     (values process
	     (make-process-output-port to-cmd-write process str)
	     (make-process-input-port from-cmd-read process str)))))

;; (define (close-all-ports-except . ports)
;;  (do ((i 0 (add1 i)))
;;      ((eqv? i 1024) #t)
;;    (if (not (memv i ports)) (file-close i))))

;; (define (make-process-stub cmd)
;;   (receive
;;    (to-cmd-read to-cmd-write) (pipe)
;;    (receive
;;     (from-cmd-read from-cmd-write) (pipe)
;;     (let ((pid (process-fork
;;                 ;; This is the child process.
;;                 (lambda ()
;;                   (file-close to-cmd-write)
;;                   (file-close from-cmd-read)
;;                   (duplicate-fileno to-cmd-read 0)
;;                   (duplicate-fileno from-cmd-write 1)
;;                   (close-all-ports-except 0 1 2)
;;                   (exit (system cmd))))))
;;       (file-close to-cmd-read)
;;       (file-close from-cmd-write)
;;       (values pid
;;               (open-queued-output to-cmd-write)
;;               (open-mbox-input-port from-cmd-read))))))


;;** I/O Extentions

;; (define-macro (call-with-input-string str proc)
;;  `(with-input-from-string ,str ,proc))

(define (read-all . port)
  (let ((port (if (null? port) (current-input-port) (car port))))
    (let loop ((expr (read port)) (result '()))
      (if (eof-object? expr)
          (reverse! result)
          (loop (read port) (cons expr result))))))

(define pretty-print pp)

(define (call-with-input-string str proc)
  (proc (open-input-string str)))

(define (cwd . dir)
  (if (pair? dir)
      (begin (os-setwd! dir) dir)
      (os-getwd)))

(define (dirname fn)
  (let* ((fnl (string-length fn)))
    (let loop ((last (if (and (> fnl 1)
                              (eqv? (string-ref fn (sub1 fnl)) #\/))
                         (- fnl 2)
                         (sub1 fnl))))
      (cond
       ((< last 1) "")
       ((eqv? (string-ref fn last) #\/) (substring fn 0 last))
       (else (loop (sub1 last)))))))

(define (file-size name)
  (let ((stat-data (stat name)))
    (and stat-data (stat-file? stat-data) (stat-size stat-data))))

(define (file-modification-time name)
  (let ((stat-data (stat name)))
    (and stat-data (receive (m a c) (stat-times stat-data) (time->epoch-seconds m)))))

(define (file-empty? name) (equal? (file-size name) 0))

(define (file-directory? name)
  (let ((stat-data (stat name)))
    (and stat-data (stat-directory? stat-data))))

(define remove-file unlink)
(define rename-file rename)
(define remove-dir rmdir)
(define change-file-mode chmod)
(define (set-file-modification-time! file seconds)
  (let ((t (epoch-seconds->time seconds)))
    (set-stat-times! file t t)))

(define (remove-dir-recursive dir)
  (let ((path (string->dir dir)))
    (do ((f (scandir dir) (cdr f)))
        ((null? f) (remove-dir dir))
      (if (not (or (string=? (car f) ".") (string=? (car f) "..")))
          (let ((fullname (pathname->string
                           (append-path path (string->file (car f))))))
            (if (file-directory? fullname)
                (remove-dir-recursive fullname)
                (remove-file fullname)))))))

;; (define (call-with-temporary-directory proc)
;;   (let ((dir (make-temporary-directory)))
;;     (if (file-exists? dir)
;;         (call-with-temporary-directory proc)
;;         (with-mutex
;; 	 *cwdmutex*
;; 	 (let ((cwd (pathname->string (current-absolute-directory))))
;; 	   (dynamic-wind
;; 	       (lambda () (or (and (mkdirs dir) (chdir dir))
;; 			      (error "can't switch to ~a" dir)))
;; 	       (lambda () (proc dir))
;; 	       (lambda ()
;; 		 (remove-dir-recursive dir)
;; 		 (chdir cwd))))))))

(define (call-with-temporary-directory proc)
  (let ((dir (make-temporary-directory)))
    (if (file-exists? dir)
        (call-with-temporary-directory proc)
        (let ((cwd #f))
	  (mkdirs dir)
	  (guard
	   (ex (else (chdir cwd) (remove-dir-recursive dir) (raise ex)))
	   (set! cwd (pathname->string (current-absolute-directory)))
	   (or (chdir dir) (error "can't switch to ~a" dir))
	   (call-with-values (lambda () (proc dir))
	     (lambda v
	       (chdir cwd)
	       (remove-dir-recursive dir)
	       (list->values v))))))))

;; This would be funny with mmap(2).

(%strategy bytecode
(define (filedata name)
  (let* ((s (stat name))
         (l (if s (stat-size s) (error (string-append "No file: " name))))
         (result (bvec-alloc <string> (fx+ l 1)))
         (fd (fd-open name (make-fd-open-mode 'read) 0)))
    ;; Don't read it at once, break it into protions for the scheduler.
    ;; (fd-read fd result 0 l)
    (do ((i 0 (fx+ i 4096)))
        ((fx>= i l))
      (fd-read fd result i (min (fx- l i) 4096)))
    (fd-close fd)
    result)))

(define-safe-glue (fd-copy-data!
		   (fd <raw-int>)
		   to
		   (offset <raw-int>)
		   (length <raw-int>))
{
  int rc;
  char *p = STRING_P(to) ? string_text(to) : OBJ_TO_RAW_PTR(to);
  if( length < 0 ) scheme_error( "fd-copy-data! negative ~a", 1, int2fx(length) );
  rc = read( fd, p + offset, length );
  REG0 = rc < 0 ? FALSE_OBJ : int2fx(rc);
  if( rc < 0 ) scheme_error( "fd-copy-data! read ~a", 1, int2fx(rc) );
  RETURN1();
})

(%strategy bytecode
 (define (copy-file-data! file to n foff toff)
   (let* ((size (file-size file))
	  (take (min n (fx- size foff))))
     (let* ((fileno (fd-open file (make-fd-open-mode 'read 0) 0))
	    (skv (fd-lseek fileno foff 0))
	    (took (fd-copy-data! fileno to toff take)))
       (fd-close fileno)
       took))))

;; We need to watch for tight disk space at the file system where 'nm'
;; lives.

(define (filesystem-free nm)
  (define df-block-regex (reg-expr->proc '(seq (prefix (* (not space)))
                                               (+ space)
                                               (let blocks (+ digit))
                                               (+ space)
                                               (let used (+ digit))
                                               (+ space)
                                               (let avail (+ digit))
                                               (+ any))))
  (bind ((df (open-input-process
              (string-append "/bin/df -kP " nm)))
         (s e b u a (begin (read-line df)
                           (df-block-regex (read-line df)))))
        (close-input-port df)
        (string->number a)))

;;*** regular expression matchers in rscheme syntax

(define content-type-charset
  (let ((match
         (reg-expr->proc
          `(prefix (seq (+ (not #\;)) #\;
                        (* space) "charset" (* space) #\=
                        (or (save (+ (not space)))
                            (seq (* space) #\' (save (* (not #\'))) #\')
                            (seq (* space) #\" (save (* (not #\"))) #\")))))))
    (lambda (v)
      (bind ((s e a b c (match v)))
	    (and s (or a b c)))
;       (receive (s e a b c) (match v)
;                (and s (or a b c)))
      )))

(define xml-pi-regex
  (reg-expr->proc
   `(prefix (seq (* (or #\space #\tab #\newline #\cr)) "<?xml"))))

(define html-regex
  (reg-expr->proc
   `(prefix (seq (* (or #\space #\tab #\newline #\cr))
		 (? (seq "<!--" (* (not #\-)) "-->"))
		 #\<
		 (or "html" "HTML"
		     (seq #\! (or "doctype" "DOCTYPE")
			  (+ (or #\space #\tab #\newline #\cr))
			  (or "HTML" "html")))))))

(define xml-pi-encoding
  (let ((match
         (reg-expr->proc
          `(prefix (seq (? (seq (* (or #\space #\tab #\newline #\cr))
                                "version" (* (or #\space #\tab #\newline #\cr))
                                #\= (* (or #\space #\tab #\newline #\cr))
                                (or (seq #\' (* (not #\')) #\')
                                    (seq #\" (* (not #\")) #\"))))
                        (? (seq (* (or #\space #\tab #\newline #\cr))
                                "encoding" (* (or #\space #\tab #\newline #\cr))
                                #\= (* (or #\space #\tab #\newline #\cr))
                                (or (seq #\" (save (* (not #\"))) #\")
                                    (seq #\' (save (* (not #\'))) #\'))))
                        )))))
    (lambda (v)
      (bind ((s e a b (match v)))
            (or a b)))))

(define dotted-match-regex
  (reg-expr->proc ;"^([^.]*)[.](.*)"
   '(prefix (seq (save (* (not #\.))) #\. (save (* any))))))

(define strip-html-suffix-regex
  (reg-expr->proc
   '(seq (save (* any))
         (suffix (seq #\.
                      (? (or #\x #\X))
                      (or #\h #\H)
                      (or #\t #\T)
                      (? (seq (or #\m #\M) (seq (? (or #\l #\L))))))))))

(define http-location-regex
  (reg-expr->proc '(seq (? (seq (save (or "http://" "https://"))
                                (save (+ (not #\/)))))
                        (save (* any)) )))

(define http-prefix? (reg-expr->proc '(or (prefix "http://")
                                          (prefix "https://"))))

(define is-proxy-request?
  (reg-expr->proc '(or (prefix "http://")
                       (prefix "https://")
                       (prefix "ftp://")
                       (prefix "ldap://"))))

(define http-is-login? (reg-expr->proc '(prefix "/LOGIN")))
(define http-is-logout? (reg-expr->proc '(prefix "/LOGOUT")))
(define http-is-post-url?
  (let ((match (reg-expr->proc '(prefix (seq "/POST" (? (seq #\= (* (not #\/))))
					     (save (seq #\/ (* any))))))))
    (lambda (str) (receive args (match str)
			   (if (pair? args)
			       (caddr args)
			       #f)))))

(define http-is-check-url? (reg-expr->proc '(prefix "/CHECK/")))

;; for http server command parsing only
(define http-proxy-cmd-regex
  (reg-expr->proc '(entire
                    (seq (or "http://")
                         (? (seq (save (+ (not (or #\: #\/))))
                                 #\:
                                 (save (+ (not #\/)))
                                 #\@))
                         (save (seq (+ (not (or #\: #\/)))
                                    (? (seq #\: (save (+ (not #\/)))))))
                         #\/
                         (seq (save (or "GET" "POST" "PUT" "HEAD"))
                              (+ #\space)
                              (save (+ (not #\space)))
                              #\space
                              (save (+ (not (or #\return #\newline))))
                              (? #\return))))))

;; for http proxy command parsing
(define http-cmd-regex
  (reg-expr->proc '(entire (seq (save (+ uppercase))
                                (+ #\space)
                                (save (+ (not #\space)))
                                #\space
                                (save (+ (not (or #\return #\newline))))
                                (? #\return)))))

(define lmtp-from-regex
  (reg-expr->proc '(seq "From "
                        (save (seq (* any) #\@ (* any)))
                        #\space
                        (save (+ (not (or #\return #\newline)))))))

(define lmtp-destination-email-regex
  (reg-expr->proc
   '(seq (* #\space)
         ;; (save (seq #\A (+ (or (range #\0 #\9) (range #\a #\f)))))
         (save (+ (not #\space)))
         (* #\space) #\<
         (save (+ (not #\>)))
         #\>)))

(define broken-303-client
  (reg-expr->proc '(prefix "Mozilla/4.")))

(define user-agent-amaya (reg-expr->proc '(prefix "amaya/")))

(define user-agent-mozilla (reg-expr->proc '(prefix "Mozilla/")))

(define user-agent-ie (reg-expr->proc '(prefix "Microsoft")))

;; The additional, optional slash enables forward compatible
;; processing of XPath selections within the target node.
;; TODO add XPath support here.

(define-macro (rs-regexp-alpha) '(list 'alpha #\� #\� #\� #\� #\� #\� #\�))

(define nunu-rs-regexp
  (reg-expr->proc
   `(seq (save (or (seq uppercase (* (or digit . ,(rs-regexp-alpha)))
                        uppercase (* (or digit . ,(rs-regexp-alpha))))))
         (? #\/))))
(define nunu-regexp nunu-rs-regexp)

;; nunu-oid-regexp will no longer match oid's only, since it's most
;; useful for the Wiki, where it better matches a path starting with
;; an oid.

(define nunu-oid-regexp
  (reg-expr->proc
   '(save (seq #\A
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (or digit #\a #\b #\c #\d #\e #\f)
               (? (seq #\/ (+ (not (or space #\<)))))))))

(define nunu-include-rs-regexp
  (reg-expr->proc
   `(seq #\@ (save (seq uppercase (* (or digit . ,(rs-regexp-alpha)))
                        uppercase (* (or digit . ,(rs-regexp-alpha)))
                        (* (or #\/ digit . ,(rs-regexp-alpha))))))))

(define nunu-include-regexp nunu-include-rs-regexp)

(define bold-rs-regexp
  (reg-expr->proc `(seq #\* (save (+ (or digit . ,(rs-regexp-alpha)))) #\*)))

(define bold-regexp bold-rs-regexp)

(define url-rs-regexp
  (reg-expr->proc '(save (or (seq (or "http://" "ftp://" "https://")
                              (+ (or alpha digit #\- #\.))
                              (? (seq #\: (+ digit)))
                              (* (not space)))
                             (seq "mailto:"
                                  (+ (or alpha digit #\- #\. #\=))
                                  #\@
                                  (+ (or alpha digit #\- #\.)))))))
(define url-regexp url-rs-regexp)

(define sql-special-regexp
  (reg-expr->proc '(or #\' ;; #\\ #\"
                       #\nul)))

(define colon-split-regex
  (reg-expr->proc '(seq (save (+ (not #\:))) #\: (* #\space)
                        (save (* (not (or #\newline #\return)))))))

;; We could be less restrictive here.  But why should we?
(define illegal-lout?
  (reg-expr->proc '(seq #\@ (or "Include"
                                "SysInclude"
                                "IncludeGraphic"
                                "PrependGraphic"
                                "SysPrependGraphic"
                                "Database"
                                "SysDatabase"
                                "Filter"))))

(define lout-literal-char-sequence-regexp
  (reg-expr->proc
   '(save (+ (not (or #\" #\& #\{ #\} #\| #\\ #\/ #\# #\@ #\^ #\~
                      #\newline #\return #\tab #\space))))))

(define lout-collapsable-whitespace-sequence-regexp
  (reg-expr->proc
   '(save (+ (or #\newline #\return #\tab #\space)))))


;;* debugging aid

(define (thread-list . envt)
  (for-each
   (lambda ((k <fixnum>))
     (let* ((t (table-lookup *thread-table* k))
	    (t (and t (thread t))))
       (if (and t (not (eq? (thread-state t) $thread-state-complete)))
	   (let (((n <string>) (internal-thread-name t)))
	     (format #t " ~-7d [~a]~a ~6a ~10a"
		     (thread-number t)
		     n
		     (vector-ref '#("          "
				    "         "
				    "        "
				    "       "
				    "      "
				    "     "
				    "    "
				    "   "
				    "  "
				    " "
				    "")
				 (min 10 (string-length n)))
		   (gvec-ref $thread-state-names (thread-state t))
		   (thread-time t))
	     (if (eq? (thread-state t) $thread-state-blocked)
		 (format #t " ~s\n" 
			 ; (class-name (object-class (thread-blocked-on t)))
                         (thread-blocked-on t))
		 (newline))))))
   (sort (key-sequence *thread-table*) <)))

(define thread-sleep!/ms thread-sleep-ms)


(define thread-resume! thread-resume)

(define-glue (set-sigqueue-size-200)
{
  set_sigqueue_size(200);
  RETURN0();
})

(set-sigqueue-size-200)


;; Fix this in rscheme!
; (%early-once-only (table-insert! *timezones* "CET" -3600))

(define-glue (get-timezone-offset)
{
 time_t n0 = time(NULL);
 struct tm * now =localtime(&n0);
 REG0 = int2fx( (now != NULL) ? now->tm_gmtoff : 0);
 RETURN1();
})

(define-glue (timezone-tzset-intern)
{
 tzset();
 RETURN0();
})

(define *timezone-offset* (get-timezone-offset))

(define (timezone-offset) *timezone-offset*)

(define (timezone-tzset)
  (timezone-tzset-intern)
  (set! *timezone-offset* (get-timezone-offset)))

;; time as defined by dsssl 10.???

(define (dsssl:time)
  (time->epoch-seconds (time)))

(define-class <oid> (<object>)
  (string-data type: <string> setter: #f))

(define-method to-string ((self <oid>))
  (string-data self))

(define-method equal? ((a <oid>) b) ;;(b <oid>))
  (or (eq? a b) (and (oid? b) (string=? (string-data a) (string-data b)))))

;;** Utilities for protocol implementations.

(%strategy bytecode
 (define (file-close fd) (fd-close fd))
 (define (duplicate-fileno old new) (fd-dup2 old new))
)

(define (file-execute-access? file)
  (file-access? file (access-mask execute)))

(define (file-find-in-path path name)
  (ormap (lambda (entry)
           (let ((file (if (eqv? (string-ref
                                  entry (sub1 (string-length entry)))
                                 #\/)
                           (string-append entry name)
                           (string-append entry "/" name))))
           (and (file-execute-access? file) file)))
         path))

;(define (read-bytes n port)
;  (if n
;      (let ((result (make-string n)))
;        (let loop ((i 0))
;          (if (eqv? i n)
;              result
;              (let ((c (read-char port)))
;                (if (eof-object? c)
;                    c
;                    (begin (string-set! result i c)
;                           (loop (add1 i))))))))
;      (call-with-output-string
;        (lambda (out)
;          (do ((c (read-char port) (read-char port)))
;              ((eof-object? c))
;            (display c out))))))

(define (read-bytes n port)
  (if n 
      (let ((first (read-string port n)))
        (if (eqv? (string-length first) n)
            first
            (let ((next (read-string port (fx- n (string-length first)))))
              (if (eof-object? next)
                  first
		  (call-with-output-string
		   (lambda (out)
		     (write-string out first)
		     (write-string out next)
		     (let loop ((done (fx+ (string-length first)
					   (string-length next))))
		       (let ((another (read-string port (fx- n done))))
			 (if (eof-object? another) #t
			     (begin
			       (write-string out another)
			       (loop (fx+ done (string-length another)))))))))))))
      (port->string port)))

(define (string->inet-socket-addr addr)
  (let ((d (string-search addr #\:)))
    (if d
	(make-inet-socket-addr
	 (string->inet-addr (substring addr 0 d))
	 (string->number
	  (substring addr (add1 d) (string-length addr))))
	(make-inet-socket-addr (string->inet-addr addr) 0))))

(define (askemos:run-tcp-server
         host port connection-handler
         maximum-semaphore capture-handler name)
  (define request-queue (tcp-listen port 4 host))
  (define (restore-queue-handler ex)
    ;; If `get-next-client' raises an exception (at June 2003 this can
    ;; happen, because rscheme sometimes delivers exceptions the wrong
    ;; way), we need to recreated the broken queue.  We want to know
    ;; about it (log) and eventually remove this handler alltogether.
    (logerr "get-next-client ~a\n" ex)
    (set! request-queue (tcp-listen port 4 host)))
  (define (loop)
    (guard
     (ex (else (restore-queue-handler ex)))
     (let ((cs (accept-client request-queue)))
       (if maximum-semaphore (semaphore-wait maximum-semaphore))
       (thread-start!
	(make-thread
	 (lambda ()
	   (guard (ex ((eq? ex 'connection-captured)
		       (if capture-handler
			   (let ((cs cs))
			     (capture-handler #f (input-port cs) (output-port cs)
					      (lambda () #t) (lambda (c) (close cs))))
			   (close cs))
		       (set! cs #f))
		      (else #f))
		  (connection-handler
		   (input-port cs) (output-port cs)
		   (receive (addr port) (inet-socket-addr-parts (peer cs))
			    addr)))
	   (if maximum-semaphore
	       (semaphore-signal maximum-semaphore))
	   (if cs (close cs)))
	 name))))
    (loop))
  (loop))

(define (askemos:run-ssl-server
         host port certinfo connection-handler exception-handler
         maximum-semaphore capture-handler name)
  (define request-queue (tcp-listen port 4 host))
  (define (restore-queue-handler ex)
    (logerr "restore-queue-handler ~a\n" ex)
    (set! request-queue (tcp-listen port 4 host)))
  (define blocking-connections #f)
  (define (loop)
    (guard
     (ex (else (restore-queue-handler ex)))
     (receive
      (cnx peer) (get-next-client (service request-queue))
      (if (and maximum-semaphore (eqv? (semaphore-value maximum-semaphore) 0) (not blocking-connections))
	  (begin
	    (set! blocking-connections #t)
	    (logerr "W Blocking connections ~a:~a\n" host port)))
      (if maximum-semaphore (semaphore-wait maximum-semaphore))
      (if (and blocking-connections (fx>= (semaphore-value maximum-semaphore) 1))
	  (set! blocking-connections #f))
      (thread-start!
       (make-thread
	(lambda ()
	  (let ((ssl #f))
	    (guard (ex ((and capture-handler (eq? ex 'connection-captured))
			(guard
			 (ex (else (logerr "Capture handler ~a\n" ex)))
			 (let ((ssl ssl))
			   (capture-handler
			    (certificate ssl) (input-port ssl) (output-port ssl)
			    (lambda () (and ssl (not (the-exit-status (process ssl)))))
			    (lambda dummy
			      (if ssl (let ((s ssl)) (set! ssl #f) (close s)))))))
			(set! ssl #f))
		       (else
			(if ssl (close ssl))
			(exception-handler ex)))
		   (set! ssl (make-sslmgr cnx peer
					  (if host
					      `("-i" ,host . ,certinfo)
					      certinfo)))
		   (connection-handler
		    (input-port ssl) (output-port ssl)
		    peer (certificate ssl)))
	    (if maximum-semaphore
		(semaphore-signal maximum-semaphore))
	    ;; FIXME the next call can hang forever.
	    ;; Otherwise we could wrap the threads body in
	    ;; with-semaphore.
	    (if ssl (close ssl))))
	name))))
    (loop))
  (loop))

(define (close-ports return exception-handler in out)
  (with-exception-handler identity 
   (lambda () (close-input-port in)))
  (with-exception-handler identity 
   (lambda () (close-output-port out))))

(define (askemos:run-tcp-client host port connection-handler)
  (define (exception-handler ex)
    (receive
     (title msg args rest) (condition->fields ex)
     (logcond 'run-tcp-client title msg args)))
  (bind-exit
   (lambda (return)
     (with-exception-handler
      (lambda (ex) (exception-handler ex) (raise ex))
      (lambda ()
        (receive
         (in out) (tcp-connect host port)
         (with-exception-handler
          (lambda (ex)
            (return (close-ports return exception-handler in out)))
          (lambda ()
            (let ((result (connection-handler in out)))
	      (close-ports return exception-handler in out)
	      result)))))))))

;;** Tables

;; Don't use table-fold anymore!  Move towards SRFI 69 hash-table-fold !

(define (table-fold combine init table)
  ;; we iterate and accumulate the result in "init".
  (table-for-each table (lambda (h k v) (set! init (combine k init))))
    ;; return the accumulated value.
    init)

;; imported from tables:
;; (define (make-symbol-table) (make-table eq? symbol->hash))
;; (define (make-string-table) (make-table string=? string->hash))
;; (define (oid->hash (oid <oid>)) (string->hash (string-data oid)))

(define (make-character-table) (make-table eqv? immob->hash))
;(define (make-fixnum-table) (make-table eqv? immob->hash))

;; define-rbtree license: LGPL
;; http://lists.gnu.org/archive/html/chicken-users/2008-11/msg00001.html

(define-macro (define-rbtree
		rbtree-init!
		node->rbtree
		lookup			; may be #f
		tree-fold		; may be #f
		tree-for-each		; may be #f
		insert!
		remove!
		reposition!
		empty?
		singleton?
		match?			; may be #f iff lookup is #f
		key<?			; may be #f iff lookup is #f
		before?
		color
		color-set!
		parent
		parent-set!
		left
		left-set!
		right
		right-set!
		leftmost
		leftmost-set!
		rightmost
		rightmost-set!)

  (define (black rbtree)
    rbtree)

  (define (black? rbtree)
    `(lambda (node)
       (,color node)))

  (define (blacken! rbtree)
    `(lambda (node)
       (,color-set! node ,(black rbtree))))

  (define (red)
    #f)

  (define (red?)
    `(lambda (node)
       (not (,color node))))

  (define (reden!)
    `(lambda (node)
       (,color-set! node ,(red))))

  (define (copy-color!)
    `(lambda (node1 node2)
       (,color-set! node1 (,color node2))))

  (define (exchange-color!)
    `(lambda (node1 node2)
       (let ((color-node1 (,color node1)))
	 (,color-set! node1 (,color node2))
	 (,color-set! node2 color-node1))))

  (define (update-parent!)
    `(lambda (parent-node old-node new-node)
       (if (eq? old-node (,left parent-node))
	   (,left-set! parent-node new-node)
	   (,right-set! parent-node new-node))))

  (define (rotate! side1 side1-set! side2 side2-set!)
    `(lambda (node)
       (let ((side2-node (,side2 node)))
	 (let ((side1-side2-node (,side1 side2-node)))
	   (,side2-set! node side1-side2-node)
	   (,parent-set! side1-side2-node node))
	 (let ((parent-node (,parent node)))
	   (,side1-set! side2-node node)
	   (,parent-set! node side2-node)
	   (,parent-set! side2-node parent-node)
	   (,(update-parent!) parent-node node side2-node)))))

  (define (rotate-left!)
    (rotate! left left-set! right right-set!))

  (define (rotate-right!)
    (rotate! right right-set! left left-set!))

  (define (neighbor side other-side)
    `(lambda (node rbtree)
       (let ((side-node (,side node)))
	 (if (eq? side-node rbtree)
	     (let ((parent-node (,parent node)))
	       (if (or (eq? parent-node rbtree)
		       (eq? node (,side parent-node)))
		   rbtree
		   parent-node))
	     (let loop ((x side-node))
	       (let ((other-side-x (,other-side x)))
		 (if (eq? other-side-x rbtree)
		     x
		     (loop other-side-x))))))))

  (define (insert-below! x)
    `(let ((x ,x))
       (if (,before? node x)
	   (insert-left! (,left x) x)
	   (insert-right! (,right x) x))))

  (define (insert-body side1 rotate-side1! side2 rotate-side2!)
    `(let ((side2-parent-parent-x (,side2 parent-parent-x)))
       (if (,(red?) side2-parent-parent-x)
	   (begin
	     (,(blacken! 'rbtree) parent-x)
	     (,(blacken! 'rbtree) side2-parent-parent-x)
	     (,(reden!) parent-parent-x)
	     (loop parent-parent-x))
	   (let ((y
		  (if (eq? x (,side2 parent-x))
		      (begin
			(,rotate-side1! parent-x)
			(,parent parent-x))
		      (,parent x))))
	     (,(blacken! 'rbtree) y)
	     (let ((parent-y (,parent y)))
	       (,(reden!) parent-y)
	       (,rotate-side2! parent-y))))))

  (define (remove-body side1 rotate-side1! side2 rotate-side2!)
    `(let ((x
	    (let ((side2-parent-node (,side2 parent-node)))
	      (if (,(red?) side2-parent-node)
		  (begin
		    (,(blacken! 'rbtree) side2-parent-node)
		    (,(reden!) parent-node)
		    (,rotate-side1! parent-node)
		    (,side2 parent-node))
		  side2-parent-node))))

       (define (common-case y)
	 (,(copy-color!) y parent-node)
	 (,(blacken! 'rbtree) parent-node)
	 (,(blacken! 'rbtree) (,side2 y))
	 (,rotate-side1! parent-node)
	 (,(blacken! 'rbtree) (,left rbtree)))

       (if (,(red?) (,side2 x))
	   (common-case x)
	   (let ((side1-x (,side1 x)))
	     (if (,(black? 'rbtree) side1-x)
		 (begin
		   (,(reden!) x)
		   (fixup! (,parent parent-node) parent-node))
		 (begin
		   (,(blacken! 'rbtree) side1-x)
		   (,(reden!) x)
		   (,rotate-side2! x)
		   (common-case (,side2 parent-node))))))))

  `(begin

     (define (,rbtree-init! rbtree)
       ,@(if leftmost
             `((,leftmost-set! rbtree rbtree))
             `())
       (,(blacken! 'rbtree) rbtree)
       (,parent-set! rbtree rbtree)
       (,left-set! rbtree rbtree)
       rbtree)

     (define (,node->rbtree node)
       (or (,color node)
	   (,color (,parent node))))

     ,@(if lookup
	   `((define (,lookup rbtree key)
	       (let loop ((node (,left (,node->rbtree rbtree))))
		 (if (eq? rbtree node) #f
		     (cond
		      ((,match? key node) node)
		      ((,key<? key node) (loop (,left node)))
		      (else (loop (,right node))))))))
	   '())

     ,@(if tree-fold
	   `((define (,tree-fold procedure init rbtree)
	       (define (fold init node)
		 (if (eq? rbtree node)
		     init
		     (fold (procedure node (fold init (,right node))) (,left node))))
	       (fold init (,left (,node->rbtree rbtree)))))
	   '())
     ,@(if tree-for-each
	   `((define (,tree-for-each procedure rbtree)
	       (let loop ((node (,left (,node->rbtree rbtree))))
		 (or (eq? rbtree node)
		     (begin
		       (procedure node)
		       (loop (,left node))
		       (loop (,right node)))))))
	   '())

     (define (,insert! rbtree node)

       (define (fixup!)

	 (let loop ((x node))
	   (let ((parent-x (,parent x)))
	     (if (,(red?) parent-x)
		 (let ((parent-parent-x (,parent parent-x)))
		   (if (eq? parent-x (,left parent-parent-x))
		       ,(insert-body left
				     (rotate-left!)
				     right
				     (rotate-right!))
		       ,(insert-body right
				     (rotate-right!)
				     left
				     (rotate-left!)))))))

	 (,(blacken! 'rbtree) (,left rbtree))
	 #f)

       (define (insert-left! left-x x)
	 (if (eq? left-x rbtree)
	     (begin
	       (,left-set! x node)
	       (,parent-set! node x)

	       ;; check if leftmost must be updated

	       ,@(if leftmost
		     `((if (eq? x (,leftmost rbtree))
			   (,leftmost-set! rbtree node)))
		     `())

	       (fixup!))
	     ,(insert-below! 'left-x)))

       (define (insert-right! right-x x)
	 (if (eq? right-x rbtree)
	     (begin
	       (,right-set! x node)
	       (,parent-set! node x)

	       ;; check if rightmost must be updated

	       ,@(if rightmost
		     `((if (eq? x (,rightmost rbtree))
			   (,rightmost-set! rbtree node)))
		     `())

	       (fixup!))
	     ,(insert-below! 'right-x)))

       (,(reden!) node)
       (,left-set! node rbtree)
       (,right-set! node rbtree)

       (insert-left! (,left rbtree) rbtree)

       (,parent-set! rbtree rbtree)

       node)

     (define (,remove! node)
       (let ((rbtree (,node->rbtree node)))

	 (define (fixup! parent-node node)
	   (cond ((or (eq? parent-node rbtree) (,(red?) node))
		  (,(blacken! 'rbtree) node))
		 ((eq? node (,left parent-node))
		  ,(remove-body left
				(rotate-left!)
				right
				(rotate-right!)))
		 (else
		  ,(remove-body right
				(rotate-right!)
				left
				(rotate-left!)))))

	 (let ((parent-node (,parent node))
	       (left-node (,left node))
	       (right-node (,right node)))

	   (,parent-set! node #f) ;; to avoid leaks
	   (,left-set! node #f)
	   (,right-set! node #f)

	   (cond ((eq? left-node rbtree)

		  ;; check if leftmost must be updated

		  ,@(if leftmost
			`((if (eq? node (,leftmost rbtree))
			      (,leftmost-set!
			       rbtree
			       (if (eq? right-node rbtree)
				   parent-node
				   right-node))))
			`())

		  (,parent-set! right-node parent-node)
		  (,(update-parent!) parent-node node right-node)

		  (if (,(black? 'rbtree) node)
		      (begin
			(,(reden!) node) ;; to avoid leaks
			(fixup! parent-node right-node))))

		 ((eq? right-node rbtree)

		  ;; check if rightmost must be updated

		  ,@(if rightmost
			`((if (eq? node (,rightmost rbtree))
			      (,rightmost-set!
			       rbtree
			       left-node)))
			`())

		  (,parent-set! left-node parent-node)
		  (,(update-parent!) parent-node node left-node)

		  ;; At this point we know that the node is black.
		  ;; This is because the right child is nil and the
		  ;; left child is red (if the left child was black
		  ;; the tree would not be balanced)

		  (,(reden!) node) ;; to avoid leaks
		  (fixup! parent-node left-node))

		 (else
		  (let loop ((x right-node) (parent-x node))
		    (let ((left-x (,left x)))
		      (if (eq? left-x rbtree)
			  (begin
			    (,(exchange-color!) x node)
			    (,parent-set! left-node x)
			    (,left-set! x left-node)
			    (,parent-set! x parent-node)
			    (,(update-parent!) parent-node node x)
			    (if (eq? x right-node)
				(if (,(black? 'rbtree) node)
				    (begin
				      (,(reden!) node) ;; to avoid leaks
				      (fixup! x (,right x))))
				(let ((right-x (,right x)))
				  (,parent-set! right-x parent-x)
				  (,left-set! parent-x right-x)
				  (,parent-set! right-node x)
				  (,right-set! x right-node)
				  (if (,(black? 'rbtree) node)
				      (begin
					(,(reden!) node) ;; to avoid leaks
					(fixup! parent-x right-x))))))
			  (loop left-x x)))))))

	 (,parent-set! rbtree rbtree)))

     (define (,reposition! node)
       (let* ((rbtree
	       (,node->rbtree node))
	      (predecessor-node
	       (,(neighbor left right) node rbtree))
	      (successor-node
	       (,(neighbor right left) node rbtree)))
	 (if (or (and (not (eq? predecessor-node rbtree))
		      (,before? node predecessor-node))
		 (and (not (eq? successor-node rbtree))
		      (,before? successor-node node)))
	     (begin
	       (,remove! node)
	       (,insert! rbtree node)))))

     (define (,empty? rbtree)
       (eq? rbtree (,left rbtree)))

     (define (,singleton? rbtree)
       (let ((root (,left rbtree)))
	 (and (not (eq? root rbtree))
	      (eq? (,left root) rbtree)
	      (eq? (,right root) rbtree))))))

#|

(define (string-node-before? node1 node2) ;; ordering function
  (string<? (string-node-key node1) (string-node-key node2)))

(define (string-node-match? key node)
  (string=? key (string-node-key node)))

(define (string-node-key-before? key node2) ;; ordering function
   (string<? key (string-node-key node2)))

(define-rbtree
  string-rbtree-init!       ;; defined by define-rbtree
  string-node->rbtree       ;; defined by define-rbtree
  string-rbtree-node-lookup ;; defined by define-rbtree
  string-rbtree-node-fold   ;; defined by define-rbtree
  string-rbtree-node-for-each ;; defined by define-rbtree
  string-rbtree-node-insert! ;; defined by define-rbtree
  string-node-remove!	     ;; defined by define-rbtree
  string-node-reposition!    ;; defined by define-rbtree
  string-rbtree-empty?	     ;; defined by define-rbtree
  string-node-singleton?     ;; defined by define-rbtree
  string-node-match?
  string-node-key-before?
  string-node-before?
  string-node-color
  string-node-color-set!
  string-node-parent
  string-node-parent-set!
  string-node-left
  string-node-left-set!
  string-node-right
  string-node-right-set!
  string-node-leftmost
  string-node-leftmost-set!
  #f
  #f)

(define (make-string-rbtree)
  (string-rbtree-init! (make-string-node #f #f #f #f  #f #f)))

(define (string-rbtree-set! tree key value)
  (let ((node (string-rbtree-node-lookup tree key)))
    (if node
	(string-node-value-set! node value)
	(string-rbtree-node-insert! tree (make-string-node #f #f #f #f key value))))
  #t)

(define (string-rbtree-ref/default tree key default)
  (let ((node (string-rbtree-node-lookup tree key)))
    (if node (string-node-value node) default)))

(define (string-rbtree-ref tree key default)
  (let ((node (string-rbtree-node-lookup tree key)))
    (if node (string-node-value node) (default))))

(define (string-rbtree-delete! tree key)
  (let ((node (string-rbtree-node-lookup tree key)))
    (if node (string-node-remove! node))))

(define (string-rbtree-fold proc init tree)
  (string-rbtree-node-fold
   (lambda (node init) (proc (string-node-key node) (string-node-value node) init))
   init tree))

(define (string-rbtree/min tree default)
  (let ((node (string-node-leftmost tree)))
    (if node (string-node-key node) default)))

(define (string-rbtree/delete-min! tree default)
  (let ((node (string-node-leftmost tree)))
    (if node
	(let ((key (key node)))
	  (string-node-remove! node)
	  key)
	default)))
|#

;; Define constructors and accessors for "string-llrbtree" and "string-node".

(define-class <string-node> (<object>)
  (color getter: string-node-color setter: string-node-color-set!)
  (left getter: string-node-left setter: string-node-left-set!)
  (right getter: string-node-right setter: string-node-right-set!)
  (key getter: string-node-key setter: string-node-key-set!)
  (value getter: string-node-value setter: string-node-value-set!))

(define (make-string-node color left right key value)
  (make <string-node> color: color left: left right: right key: key value: value))

(define-llrbtree-code
  (ordered)
  ((node . args)
   `(let ((node ,node))
      . ,(let loop ((args args))
	   (if (null? args)
	       '(node)
	       (cons
		(case (car args)
		  ((color:) `(string-node-color-set! node ,(cadr args)))
		  ((left:) `(string-node-left-set! node ,(cadr args)))
		  ((right:) `(string-node-right-set! node ,(cadr args)))
		  (else (error  (format "unbrauchbar ~a" args))))
		(loop (cddr args)))))))
  string-ordered-llrbtree-init!		   ;; defined
  string-ordered-llrbtree-node-lookup	   ;; defined
  string-ordered-llrbtree-min		   ;; defined
  string-ordered-llrbtree-node-fold	   ;; defined
  string-ordered-llrbtree-node-for-each	   ;; defined
  string-ordered-llrbtree-node-insert!	   ;; defined
  string-ordered-llrbtree-delete!	   ;; defined
  string-ordered-llrbtree-node-delete-min! ;; defined
  string-ordered-llrbtree-empty?	   ;; defined
  ((key node) ;; key-node-eq? function
   `(string=? ,key (string-node-key ,node)))
  ((key node) ;; key-node-<? ordering function
   `(string<? ,key (string-node-key ,node)))
  ((node1 node2) ;; before? node ordering function
   `(string<? (string-node-key ,node1) (string-node-key ,node2)))
  string-node-left string-node-left-set!
  string-node-right string-node-right-set!
  string-node-color string-node-color-set!
  #f
  )

(define (make-string-ordered-llrbtree)
  (string-ordered-llrbtree-init! (make-string-node #f #f #f  #f #f)))

(define (string-ordered-llrbtree-set! tree key value)
  (let ((node (string-ordered-llrbtree-node-lookup tree key)))
    (if node
	(string-node-value-set! node value)
	(string-ordered-llrbtree-node-insert! tree key (make-string-node #f #f #f key value))))
  #t)

(define (string-ordered-llrbtree-ref/default tree key default)
  (let ((node (string-ordered-llrbtree-node-lookup tree key)))
    (if node (string-node-value node) default)))

(define (string-ordered-llrbtree-ref tree key default)
  (let ((node (string-ordered-llrbtree-node-lookup tree key)))
    (if node (string-node-value node) (default))))

(define (string-ordered-llrbtree-fold proc init tree)
  (string-ordered-llrbtree-node-fold
   (lambda (node init) (proc (string-node-key node) (string-node-value node) init))
   init tree))

(define (string-ordered-llrbtree-for-each proc tree)
  (string-ordered-llrbtree-node-for-each
   (lambda (node) (proc (string-node-key node) (string-node-value node)))
   tree))

(define (string-llrbtree/min tree default)
  (let ((node (string-ordered-llrbtree-min tree)))
    (if node (values (string-node-key node) (string-node-value node)) (default))))

(define (string-llrbtree/delete-min! tree default)
  (let ((node (string-ordered-llrbtree-node-delete-min! tree)))
    (if node (values (string-node-key node) (string-node-value node)) (default))))

(: partition/iterative ((procedure (*) boolean) list --> list list))
(define (partition/iterative pred lst)
  (let ((t (cons #f '()))
	(f (cons #f '())))
    (let ((tl t) (fl f))
      (do ((lst lst (cdr lst)))
	  ((null? lst) (values (cdr t) (cdr f)))
	(let ((elt (car lst)))
	  (if (pred elt)
	      (let ((p (cons elt (cdr tl))))
		(set-cdr! tl p)
		(set! tl p))
	      (let ((p (cons elt (cdr fl))))
		(set-cdr! fl p)
		(set! fl p))))))))

(: partition/cps-sharing
   ((procedure (list list) . *)
    (procedure (*) boolean)
    list
    --> . *))
(define (partition/cps-sharing return pred lis)
  (let recur ((lis lis) (return return))
    (if (null? lis) (return lis lis)
	(let ((elt (car lis))
	      (tail (cdr lis)))
	  (let ((cont (lambda (in out)
			(if (pred elt)
			    (return (if (pair? out) (cons elt in) lis) out)
			    (return in (if (pair? in) (cons elt out) lis))))))
	    (recur tail cont))))))

(: partition/cps-iterative
   ((procedure (list list) . *)
    (procedure (*) boolean)
    list
    --> . *))
(define (partition/cps-iterative values pred lst)
  (let ((t (cons #f '()))
	(f (cons #f '())))
    (let ((tl t) (fl f))
      (do ((lst lst (cdr lst)))
	  ((null? lst) (values (cdr t) (cdr f)))
	(let ((elt (car lst)))
	  (if (pred elt)
	      (let ((p (cons elt (cdr tl))))
		(set-cdr! tl p)
		(set! tl p))
	      (let ((p (cons elt (cdr fl))))
		(set-cdr! fl p)
		(set! fl p))))))))


(define partition/cps partition/cps-iterative)
