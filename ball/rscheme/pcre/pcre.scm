;; (C) 2001, 2013 J�rg F. Wittenberger -*-Scheme-*- GPL

(define-macro (define-pcre-glue args . body)
  `(define-safe-glue ,args
     type-handler: (<pcre>
                    (direct-instance? <pcre>)
                    ("pcre *~a"
                     "(pcre *)PTR_TO_DATAPTR(gvec_ref(~a,SLOT(0)))"))
     properties: ((other-h-files "<pcre.h>")
		  (other-libs "pcre"))
     ,@body))

(define-class <pcre> (<object>)
  pcre)

(define-class <pcre-error> (<error>)
  (message type: <string>)
  (offset type: <fixnum>))

(define-method display-object ((self <pcre-error>) port)
  (format port "<pcre-error> ~a at char ~a\n" (message self) (offset self)))

(define-pcre-glue (pcre-compile (regex <raw-string>))
 literals: ((& <pcre>)
	    (& <pcre-error>))
{
  extern int rs_pcre_init();
  static int malloc_is_initialized = 0;
  const char *error=NULL;
  int erroffset = 0;
  pcre *re;

  if(!malloc_is_initialized ) malloc_is_initialized = rs_pcre_init();
  re = pcre_compile(
         regex,            /* the pattern */
         PCRE_UTF8, /* |PCRE_NO_UTF8_CHECK, */ /* no check: risk crash */
         &error,           /* for error message */
         &erroffset,       /* for error offset */
         NULL);
  if( !re ) {
    obj descr = make_string(error);
    raise_error( make3(TLREF(1), NIL_OBJ, descr, int2fx(erroffset)) );
  } else {
#ifndef DATAPTR_TO_PTR
#define DATAPTR_TO_PTR(p)       OBJ(((UINT_32)(p))+POINTER_TAG)
#endif
    /* Note that the hook pcre_malloc has been set up to return
     * RScheme heap-allocated objects (of type <byte-vector>),
     * so `re' really is an RScheme heap object.  Furthermore,
     * the PCRE documentation indicates that the compiled expression
     * is returned as a single, unstructured blob.
     */
    REG0 = make1( TLREF(0), DATAPTR_TO_PTR(re) );
  }
  RETURN1();
})

(define-pcre-glue (pcre-exec (regex <pcre>)
                             (subject <raw-string>)
                             (sl <raw-int>)
                             (start <raw-int>)
                             (options <raw-int>))
 literals: ((& <pcre>)
	    (& <pcre-error>))
{

  int rc, i;
  int data[30];
  rc = pcre_exec(
         regex,          /* result of pcre_compile() */
         NULL,           /* we didn't study the pattern */
         subject,        /* the subject string */
         sl,             /* the length of the subject string */
         start,          /* start at offset 0 in the subject */
         options,        /* default options */
         data,           /* vector for substring information */
         30);            /* number of elements in the vector */

  if( rc < 0 ) {
    switch( rc ) {
      case PCRE_ERROR_NOMATCH:
       REG0 = FALSE_OBJ;
       REG1 = FALSE_OBJ;
       RETURN(2);
      break;
      default:
        raise_error( make3(TLREF(1), NIL_OBJ,
                     make_string("unhandled error code"), int2fx(rc)) );
      break;
    }
  }

/*
  REG0 = int2fx(data[0]);
  REG1 = int2fx(data[1]);
*/
  REG0 = cons( int2fx(data[0]), int2fx(data[1]) );

  for( arg_count_reg=1; arg_count_reg < rc;) {
    size_t len = data[2*arg_count_reg+1]-data[2*arg_count_reg]+1;
    obj x = bvec_alloc( len, string_class );
    char *dst = PTR_TO_DATAPTR(x);
    int cc = pcre_copy_substring(subject, data, rc, arg_count_reg, dst, len);
    reg_set( arg_count_reg++, x );
  }

  /* RETURN(i+1); for (old) multiple value return version */
  COLLECT0();
  RETURN(1);
})

(define cached-pcre-compile (memoize pcre-compile string=?))

(define (pcre->proc (regex <string>))
  (let ((matcher (cached-pcre-compile regex)))
    (lambda (subject . rest)
      (pcre-exec matcher subject (string-length subject)
                 (or (and (pair? rest) (car rest)) 0)
                 0))))

(define (pcre->aproc (regex <string>))
  (let ((matcher (cached-pcre-compile regex)))
    (lambda (subject . rest)
      (pcre-exec matcher subject (string-length subject)
                 (or (and (pair? rest) (car rest)) 0)
                 #x10))))

(define (pcre/a->proc-uncached (regex <string>))
  (let ((matcher (pcre-compile regex)))
    (lambda (subject . rest)
      (pcre-exec matcher subject (string-length subject)
                 (or (and (pair? rest) (car rest)) 0)
                 #x10))))

(define (make-pcre-tokeniser lst)
  (let ((initial-cases (map
			(lambda (x) (cons (pcre->aproc (car x)) (cdr x)))
			lst)))
    (lambda (str)
      (let ((offset 0))
	(values
         (lambda ()
           (let loop ((cases initial-cases))
             (cond
              ((eqv? offset (string-length str)) '*eoi*)
              ((null? cases)
               (error "unknown token at pos: ~a '~a'" offset
                      (substring
                       str offset
                       (min (+ offset 30) (string-length str)))))
              (else (let ((match ((caar cases) str offset)))
                      (if match
                          (begin
                            (set! offset (cdar match))
                            (if (null? (cdar cases))
                                (loop initial-cases)
                                (if (null? (cdr match))
                                    (cadar cases)
                                    (make-lexical-token
				     (cadar cases)
				     str ; make-source-location ? costly for nothing
				     (cdr match)))))
                          (loop (cdr cases))))))))
         (lambda () offset))))))
