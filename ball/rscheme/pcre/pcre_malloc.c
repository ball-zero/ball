/* (C) 2001 J�rg F. Wittenberger -*-Scheme-*- GPL */

#include <pcre.h>
#include <rscheme.h>

void * rs_pcre_malloc(size_t size) {
  return PTR_TO_DATAPTR(alloc( size, byte_vector_class ));
}

void rs_pcre_free(void *dummy) { }

int rs_pcre_init()
{
  pcre_malloc = rs_pcre_malloc;
  pcre_free = rs_pcre_free;
  return 1;
}
