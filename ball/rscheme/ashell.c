//#define TRACE 1
#define USE_PIPE /* It APPEARS to be more reliable that RS_BC - is it? */
//#define USE_RS_CB
//#define TRACE_TIME 1

#include <rscheme/obj.h>
#include <rscheme.h>
#include <unistd.h>
#include <stdio.h>

#if !defined(USE_PIPE) && !defined(USE_RS_CB)
#if defined(linux) || defined(__linux__)
#define USE_PIPE
#else
#define USE_RS_CB
#endif
#endif

#if !defined(USE_PIPE) && !defined(USE_RS_CB)
#error "no alternative of USE_PIPE nor USE_RS_CB defined"
#endif
#if defined(USE_RS_CB) && defined(USE_PIPE)
#error "both alternatives USE_PIPE and USE_RS_CB defined"
#endif

#ifndef NO_THREAD_LOOP

#include <stdlib.h>
#include <pthread.h>
#include <errno.h>

#include <sys/types.h>

#ifdef TRACE_TIME
#include <time.h>

struct timeval t_start = {0 , 0};

void report_time(const char*fmt) {
  struct timeval tv;
  if (gettimeofday( &tv, NULL ) != 0)
    fprintf(stderr, "gettimeofday failed\n");
  else {
    if(t_start.tv_sec == 0) t_start.tv_sec = tv.tv_sec;
    fprintf(stderr, "TIMEPOINT,");
    fprintf(stderr, fmt, (tv.tv_sec - t_start.tv_sec) * 1000000 + tv.tv_usec);
  }
}
#endif


#include <sqlite3.h>
#include "library/dev/rs/db/sqlite3/sqglue.h"

typedef int (*askemos_request_function_t)(void *);

typedef struct _askemos_pool_entry {
  askemos_request_function_t function;
  void *data;
  obj callback;
}              askemos_pool_entry_t;

struct askemos_pool {
  pthread_mutex_t mutex;
  pthread_cond_t has_job;
  pthread_cond_t has_space;
  unsigned short int total, next, free;
  askemos_pool_entry_t *r;
};

static void *
worker_thread_loop(void *arg);

static void
askemos_pool_entry_init(struct askemos_pool * pool, askemos_pool_entry_t * r);

static int
askemos_pool_init(struct askemos_pool * pool)
{
  int i;

  pool->next = 0;
  pool->total = pool->free = 50;

  pthread_mutex_init(&pool->mutex, NULL);
  pthread_cond_init(&pool->has_job, NULL);
  pthread_cond_init(&pool->has_space, NULL);

  pool->r = malloc(sizeof(askemos_pool_entry_t) * pool->total);
  for (i = 0; i < pool->total; ++i) {
    pool->r[i].function = NULL;
    pool->r[i].data = NULL;
    pool->r[i].callback = FALSE_OBJ;
  }

  for (i = 0; i < 5; ++i) {
    int e;
    pthread_t thread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr,	/* PTHREAD_CREATE_DETACHED */
				PTHREAD_CREATE_JOINABLE);
    e = pthread_create(&thread, &attr, worker_thread_loop, pool);
    pthread_attr_destroy(&attr);
  }

  return 0;
}

static void
askemos_pool_send(struct askemos_pool * pool,
		  askemos_request_function_t function,
		  void *data,
		  obj callback)
{
  askemos_pool_entry_t *result = NULL;

  pthread_mutex_lock(&pool->mutex);

  do {

    if (pool->free) {
      result = &pool->r[pool->next];
      pool->next = (pool->next + 1) % pool->total;
      --pool->free;
    } else {
      fprintf(stderr, "Waiting for pool space\n");
      pthread_cond_wait(&pool->has_space, &pool->mutex);
    }
  } while( result == NULL );

  result->function = function;
  result->data = data;
  result->callback = callback;

#ifdef TRACE_TIME
  report_time("SEND,%d,,,,,,\n");
#endif
  pthread_mutex_unlock(&pool->mutex);
  pthread_cond_signal(&pool->has_job);

}

static void
askemos_pool_receive(struct askemos_pool * pool,
		     askemos_request_function_t *function,
		     void **data,
		     obj *callback)
{
  askemos_pool_entry_t *result = NULL;

  pthread_mutex_lock(&pool->mutex);

  do {

    if (pool->free != pool->total) {
      unsigned short int target = (pool->next + pool->free) % pool->total;
      result = &pool->r[target];
      ++pool->free;
      *function = result->function;
      *data = result->data;
      *callback = result->callback;
    } else {
      pthread_cond_wait(&pool->has_job, &pool->mutex);
    }

  } while( result == NULL );

  pthread_mutex_unlock(&pool->mutex);
  pthread_cond_signal(&pool->has_space);
}

static struct askemos_pool *request_pool = NULL;

void
start_asynchronous_request(askemos_request_function_t function,
			   void *data, obj callback)
{
  if( request_pool == NULL ) {
    fprintf(stderr, "thread pool not initialised\n");
    exit(1);
  }
#ifdef TRACE
  fprintf(stderr, "Pool call %p\n", data);
#endif
  askemos_pool_send(request_pool, function, data, callback);
}

static pthread_mutex_t callback_mutex;
static pthread_cond_t callback_free_cond;
static int the_interrupt_pipe[2] = {0, 0};
static obj the_callback = FALSE_OBJ;
static obj the_result = int2fx(0);

static void notify_external_interrupt()
{
  static char buf[1] = { (char) 254 };
#ifdef TRACE_TIME
  report_time("notify,,,,,,%d,\n");
#endif
  /*
  if(write(the_interrupt_pipe[1], buf, 1)) ;
  do {
    if( the_callback == TRUE_OBJ ) the_callback = FALSE_OBJ;
    if( truish(the_callback) ) {
#ifdef TRACE
      fprintf(stderr, "notify ULK wait\n");
#endif
      pthread_cond_wait(&callback_free_cond, &callback_mutex);
    }
  } while ( truish(the_callback) );
  */
#ifdef TRACE_TIME
report_time("gotsig ULK,,,,,,,%d\n");
#endif
  pthread_mutex_unlock(&callback_mutex);
  pthread_cond_broadcast(&callback_free_cond);
#ifdef TRACE
  fprintf(stderr, "done\n");
#endif
}

void clear_external_interrupt()
{
#ifdef USE_PIPE
  static int buf[1];
#ifdef TRACE
  fprintf(stderr, "reset cb ");
#endif
  /*  int r = read(the_interrupt_pipe[0], buf, 1); */
  the_callback = FALSE_OBJ; /* TRUE_OBJ; */
  // doesnt work: pthread_mutex_unlock(&callback_mutex);
  pthread_mutex_unlock(&callback_mutex);
  pthread_cond_broadcast(&callback_free_cond);
#endif
#ifdef TRACE_TIME
  report_time("clear external interrupt,,,,,,,%d\n");
#endif
}

int external_interrupt_filedes()
{
  return the_interrupt_pipe[0];
}

obj external_interrupt_callback()
{
  pthread_mutex_lock(&callback_mutex);
  while( the_callback == FALSE_OBJ)
      pthread_cond_wait(&callback_free_cond, &callback_mutex);
  return the_callback;
}

obj external_interrupt_result()
{
  return the_result;
}

// functions for calling a rscheme interrupt call
void rscheme_pthread_intr_call(obj cb, obj arg1)
{
#ifdef TRACE
  fprintf(stderr, "intr_call %p\n", (void*) arg1);
#endif
  pthread_mutex_lock(&callback_mutex);
  rscheme_intr_call1(cb, arg1);
  pthread_mutex_unlock(&callback_mutex);
  pthread_cond_broadcast(&callback_free_cond);
}

static void *
worker_thread_loop(void *arg)
{
  struct askemos_pool *pool = arg;
  askemos_request_function_t function;
  void *data;
  obj callback;
  int result;
  static char buf[1] = { (char) 254 };

  //  pthread_cleanup_push(worker_thread_unlock, ressources);
  while (1) {

    askemos_pool_receive(request_pool, &function, &data, &callback);
    result = (*function)(data);
#if defined(USE_PIPE)
#ifdef TRACE
fprintf(stderr, "Pool return LCK %p\n", data);
#endif
    if(!write(the_interrupt_pipe[1], buf, 1))
      fprintf(stderr, "Failed to write interrupt pipe signal rscheme thread.\n");
    pthread_mutex_lock(&callback_mutex);
    while( truish(the_callback) ) {
#ifdef TRACE
fprintf(stderr, "Pool return ULK wait %p\n", data);
#endif
      pthread_cond_wait(&callback_free_cond, &callback_mutex);
    }
#ifdef TRACE
fprintf(stderr, "Pool leave LCKed callback %p\n", data);
#endif
    the_result = int2fx(result);
    the_callback = callback;
#ifdef TRACE_TIME
    report_time("REPLY,,%d,,,,,\n");
#endif
    notify_external_interrupt();
#ifdef TRACE
fprintf(stderr, "Pool leave callback %p\n", data);
#endif
#elif defined(USE_RS_CB)
    rscheme_pthread_intr_call(callback, int2fx(result));
#endif

  }
  // pthread_cleanup_pop(1);
  return NULL;
}


void
askemos_pre_init()
{
  pthread_mutex_init(&callback_mutex, NULL);
  pthread_cond_init(&callback_free_cond, NULL);
  request_pool = malloc(sizeof(struct askemos_pool));
  askemos_pool_init(request_pool);

#ifdef USE_PIPE
  if( pipe(the_interrupt_pipe) == -1 )
  // if (socketpair(AF_UNIX, SOCK_STREAM, 0, the_interrupt_pipe) < 0)
    fprintf(stderr, "Failed to open interrupt channel\n");
#endif

}

#else  /* NO_THREAD_LOOP */

typedef int (*askemos_request_function_t)(void *);

void
start_asynchronous_request(askemos_request_function_t function,
			   void *data, obj callback){}
void
askemos_pre_init()
{
  fprintf(stderr, "thread pool not initialised\n");
}

#endif

/*

int
test_askemos_thread_sleep(void *data)
{
  int time = (int) data;
  sleep(time);
  return time;
}

*/
/*
** 2009 Juli 24
**
** This file contains OS interface code that is used with Askemos(R).
**/

/*
** standard include files.
*/
#include <sqlite3.h>
#include <string.h>
#include <pthread.h>

/*
** Pointer-Makros
*/

/*
** extracted scheme functions
*/
// typedef int (*askemos_request_function_t)(void *);
extern void start_asynchronous_request(askemos_request_function_t function,
                                       void *data, obj callback);

/* pointer to the parent sqlite3_vfs structure (e.g. unix) */
sqlite3_vfs *pVfs = NULL; 

// "host" callback
static obj the_vfs_callback;

typedef struct callback_file callback_file;
struct callback_file {
  sqlite3_io_methods *ioMethods;
  const char* zName;

  /* shared memory
   *
   * form: #((int opcode) (string buf) (int amount) (int offset) (pointer (condition done))
   */
  obj sm;
  
  // pointer to the sqlite3_file structure of the parent vfs (e.g. unix vfs)
  sqlite3_file pReal[1];
};

/*
** Method declarations for callback_file.
*/
static int callbackClose(sqlite3_file*);
static int callbackRead(sqlite3_file*, void*, int iAmt, sqlite3_int64 iOfst);
static int callbackWrite(sqlite3_file*,const void*,int iAmt, sqlite3_int64 iOfst);
static int callbackTruncate(sqlite3_file*, sqlite3_int64 size);
static int callbackFileSize(sqlite3_file*, sqlite3_int64 *pSize);
static int callbackFileControl(sqlite3_file*, int op, void *pArg);
static int callbackSectorSize(sqlite3_file*);
static int callbackDeviceCharacteristics(sqlite3_file*);

static int callbackCloseNot(sqlite3_file*);
static int callbackReadNot(sqlite3_file*, void*, int iAmt, sqlite3_int64 iOfst);
static int callbackWriteNot(sqlite3_file*,const void*,int iAmt, sqlite3_int64 iOfst);
static int callbackTruncateNot(sqlite3_file*, sqlite3_int64 size);
static int callbackSyncNot(sqlite3_file*, int flags);
static int callbackFileSizeNot(sqlite3_file*, sqlite3_int64 *pSize);
static int callbackSyncNot(sqlite3_file*, int flags);
static int callbackLockNot(sqlite3_file*, int);
static int callbackUnlockNot(sqlite3_file*, int);
static int callbackCheckReservedLockNot(sqlite3_file*, int *pResOut);
static int callbackSectorSizeNot(sqlite3_file*);

/*
** Method declarations for callback_vfs.
*/
static int callbackOpen(sqlite3_vfs*, const char *, sqlite3_file*, int , int *);
static int callbackDelete(sqlite3_vfs*, const char *zName, int syncDir);
static int callbackAccess(sqlite3_vfs*, const char *zName, int flags, int *);
static int callbackFullPathname(sqlite3_vfs*, const char *zName, int, char *zOut);
static void *callbackDlOpen(sqlite3_vfs*, const char *zFilename);
static void callbackDlError(sqlite3_vfs*, int nByte, char *zErrMsg);
static void (*callbackDlSym(sqlite3_vfs *pVfs, void *p, const char*zSym))(void);
static void callbackDlClose(sqlite3_vfs*, void*);
static int callbackRandomness(sqlite3_vfs*, int nByte, char *zOut);
static int callbackSleep(sqlite3_vfs*, int microseconds);
static int callbackCurrentTime(sqlite3_vfs*, double*);

static sqlite3_vfs callback_vfs = {
  1,                      /* iVersion */
  sizeof(callback_file),   /* szOsFile */
  CALLBACK_MAX_PATHNAME,   /* mxPathname */
  0,                      /* pNext */
  "askemos",              /* zName */
  0,                      /* pAppData */
  callbackOpen,            /* xOpen */
  callbackDelete,          /* xDelete */
  callbackAccess,          /* xAccess */
  callbackFullPathname,    /* xFullPathname */
  callbackDlOpen,          /* xDlOpen */
  callbackDlError,         /* xDlError */
  callbackDlSym,           /* xDlSym */
  callbackDlClose,         /* xDlClose */
  callbackRandomness,      /* xRandomness */
  callbackSleep,           /* xSleep */
  callbackCurrentTime      /* xCurrentTime */
};

static sqlite3_io_methods callback_io_methods = {
  1,                                 /* iVersion */
  callbackClose,                      /* xClose */
  callbackRead,                       /* xRead */
  callbackWrite,                      /* xWrite */
  callbackTruncate,                   /* xTruncate */
  callbackSyncNot,                    /* xSync */
  callbackFileSize,                   /* xFileSize */
  callbackLockNot,                    /* xLock */
  callbackUnlockNot,                  /* xUnlock */
  callbackCheckReservedLockNot,       /* xCheckReservedLock */
  callbackFileControl,                /* xFileControl */
  callbackSectorSize,                 /* xSectorSize */
  callbackDeviceCharacteristics       /* xDeviceCharacteristics */
};

static sqlite3_io_methods callback_io_noop_methods = {
  1,                                  /* iVersion */
  callbackCloseNot,                   /* xClose */
  callbackReadNot,                    /* xRead */
  callbackWriteNot,                   /* xWrite */
  callbackTruncateNot,                /* xTruncate */
  callbackSyncNot,                    /* xSync */
  callbackFileSizeNot,                /* xFileSize */
  callbackLockNot,                    /* xLock */
  callbackUnlockNot,                  /* xUnlock */
  callbackCheckReservedLockNot,       /* xCheckReservedLock */
  callbackFileControl,                /* xFileControl */
  callbackSectorSizeNot,                 /* xSectorSize */
  callbackDeviceCharacteristics       /* xDeviceCharacteristics */
};

/*
** Open an callback file handle.
*/

static pthread_mutex_t the_shared_memory_mux;
static obj the_shared_memory_for_open = FALSE_OBJ;

void lock_callback_open_parameters(obj x)
{
  pthread_mutex_lock(&the_shared_memory_mux);
  the_shared_memory_for_open = x;
}

void unlock_callback_open_parameters()
{
  the_shared_memory_for_open = FALSE_OBJ;
  pthread_mutex_unlock(&the_shared_memory_mux);
}

extern void sq_sqlite3_setup(sqlite3 *cnx, int i);

int
pthread_sqlite3_open(void *data)
{
  struct open_args *a = PTR_TO_DATAPTR(data);
  int rc;

#ifdef TRACE
  fprintf(stderr, "pDB ");
  fprintf(stderr, "%s vfs %s SM %p\n", a->dbn, a->vfs == FALSE_OBJ ? NULL : byte_string_text(a->vfs), (void*) a->sm);
#endif

  if(a->sm != FALSE_OBJ) {
    lock_callback_open_parameters(a->sm);
  }
  rc = sqlite3_open_v2( a->dbn,
			&a->cnx,
			( a->setup == 3 ? SQLITE_OPEN_READONLY :
			  ( SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE ) )
                        | SQLITE_OPEN_NOMUTEX,
			a->vfs == FALSE_OBJ ? NULL : (char*) byte_string_text(a->vfs) );
  /* unlock_callback_open_parameters(); done within open ASAP */
  sq_sqlite3_setup(a->cnx, a->setup);

#ifdef TRACE
  fprintf(stderr, "pDB %s got %p\n", a->dbn, a->cnx);
#endif

  return SQLITE_OK;
}

int
pthread_sqlite3_close(void *data)
{
  sqlite3 *a = OBJ_TO_RAW_PTR((obj) data);
#ifdef TRACE
  fprintf(stderr, "close %p\n", a);
#endif
  return sqlite3_close(a);
}

int pthread_sqlite3_prepare(void *data)
{
  struct prepare_args *a = PTR_TO_DATAPTR(data);
  int rc;
  const char *tail;
#ifdef TRACE
  fprintf(stderr, "prepar %p >>%s<< %d %d\n", a->db, a->sql, a->offset, a->sql_len);
#endif
  rc = sqlite3_prepare_v2( a->db,
			   a->sql + a->offset,
			   a->sql_len - a->offset,
			   &a->stmt,
			   &tail );
  if (a->stmt != NULL) {
    a->tail = tail - a->sql;
  }
  return rc;
}

int
pthread_sqlite3_step(void *data)
{
  obj s0=(obj) data;
  int rc;
#ifdef TRACE
  fprintf(stderr, "step %p\n", OBJ_TO_RAW_PTR(data));
#endif
  rc = sqlite3_step(OBJ_TO_RAW_PTR(data));
#ifdef TRACE
  fprintf(stderr, "step %p done\n", OBJ_TO_RAW_PTR(data));
#endif

  return rc;
}

static int callbackOpen(
  sqlite3_vfs *pCallbackVfs,
  const char *zName,
  sqlite3_file *pFile,
  int flags,
  int *pOutFlags
){
  callback_file *p = (callback_file *) pFile;
  /* Only for main db, to save energy. */
  if(flags & SQLITE_OPEN_MAIN_DB) {
#ifdef TRACE
    fprintf(stderr, "A open Main %s\n", zName);
#endif
    p->ioMethods = &callback_io_methods;
    p->zName = zName;
    p->sm = the_shared_memory_for_open;
    unlock_callback_open_parameters();
    return SQLITE_OK;
  } else if (flags & SQLITE_OPEN_MAIN_JOURNAL) {
    p->ioMethods = &callback_io_noop_methods;
    return SQLITE_OK;
  }

#ifdef TRACE
  fprintf(stderr, "A open Else %s\n", zName);
#endif
  return pVfs->xOpen(pVfs, zName, pFile, flags, pOutFlags);
}

/* 
 * fill callback args
 */
static callback_file *
fill_callback_args(sqlite3_file *sf, short type, void *zBuf, int iAmt, 
                   sqlite_int64 iOfst)
{
  callback_file *p = (callback_file *) sf;
  struct callback_args *a = PTR_TO_DATAPTR(p->sm);
  pthread_mutex_lock(&a->mux);
  while( CB_IS_BUSY(a->op) ) pthread_cond_wait(&a->cond, &a->mux);
  a->op = CB_M_CALL_OP(type);
  a->amount = iAmt;
  a->offset = iOfst;
  a->buf = zBuf;

  return p;
}

static int call_callback(callback_file *cf)
{
  struct callback_args *a = PTR_TO_DATAPTR(cf->sm);
  int result;
  static char buf[1] = { (char) 254 };

  // calling rscheme to add the blocklist to the mailbox (read)
#if defined(USE_PIPE)
#ifdef TRACE
  fprintf(stderr, "callback lock LCK from %p\n", cf);
#endif
  if(!write(the_interrupt_pipe[1], buf, 1))
    fprintf(stderr, "Failed to write interrupt pipe signal rscheme thread.\n");
  pthread_mutex_lock(&callback_mutex);
  while( truish(the_callback) ) {
#ifdef TRACE
  fprintf(stderr, "callback lock ULK wait %p\n", cf);
#endif
    pthread_cond_wait(&callback_free_cond, &callback_mutex);
  }
#ifdef TRACE
  fprintf(stderr, "callback locked LCKed by %p\n", cf);
  fprintf(stderr, "prepared: %p %x %d\n", cf, a->op, !CB_IS_RETURN(a->op));
#endif
  the_result = cf->sm;
  the_callback = the_vfs_callback;
  notify_external_interrupt();
#ifdef TRACE_TIME
  report_time("CBCA,,,%d,,,,\n");
#endif
#ifdef TRACE
fprintf(stderr, "callback leave %p\n", cf);
#endif
#elif defined(USE_RS_CB)
  rscheme_pthread_intr_call(the_vfs_callback, cf->sm);
#endif
  // waiting for the call to complete
#ifdef TRACE
  fprintf(stderr, "wait for completion %p %x %d\n", cf, a->op, !CB_IS_RETURN(a->op));
#endif
  while ( !CB_IS_RETURN(a->op) ) {
#ifdef TRACE
fprintf(stderr, "wait for completion %p\n", cf);
#endif
    pthread_cond_wait(&a->cond, &a->mux);
  }
  result = CB_GET_OP(a->op);
  a->op = 0;
  pthread_mutex_unlock(&a->mux);
  // pthread_cond_signal(&a->cond);
#ifdef TRACE_TIME
  report_time("CBRP,,,,%d,,,\n");
#endif
  return result;
}

/*
** Close an callback-file.
*/
static int callbackCloseNot(sqlite3_file *pFile)
{
  return SQLITE_OK;
}

static int callbackClose(sqlite3_file *sf){
#ifdef TRACE
  fprintf(stderr, "A close %p\n", sf);
#endif
  return call_callback(fill_callback_args(sf, CB_IO_TYPE_CLOSE, NULL, 0, 0));
}

/*
** Read data from an callback-file.
*/

static int callbackReadNot(sqlite3_file *sf, void *zBuf,
                           int iAmt, sqlite_int64 iOfst)
{
#ifdef TRACE
  fprintf(stderr, "read not %p\n", sf);
#endif
  return SQLITE_IOERR;
}

static int callbackRead(sqlite3_file *sf, void *zBuf,
                        int iAmt, sqlite_int64 iOfst)
{
#ifdef TRACE
  fprintf(stderr, "cb read %p %p %d %ld\n", sf, zBuf, iAmt, (long int) iOfst);
#endif
  return call_callback(fill_callback_args(sf, CB_IO_TYPE_READ, zBuf, iAmt, iOfst));
}

/*
** Write data to an callback-file.
*/
static int callbackWriteNot(sqlite3_file *pFile, const void *zBuf,
                            int iAmt, sqlite_int64 iOfst) {
  return SQLITE_OK;
}

static int callbackWrite(
  sqlite3_file *sf,     // structure
  const void *zBuf,     // buffer with data
  int iAmt,             // amount of bytes to write
  sqlite_int64 iOfst    // offset of file to write
){
#ifdef TRACE
  fprintf(stderr, "cb write %p %p %d %ld\n", sf, zBuf, iAmt, (long int) iOfst);
#endif
  return call_callback(fill_callback_args(sf, CB_IO_TYPE_WRITE, (void*) zBuf, iAmt, iOfst));
}

/*
** Truncate an callback-file.
*/
static int callbackTruncateNot(sqlite3_file *pFile, sqlite_int64 size)
{
  return SQLITE_OK;
}

static int callbackTruncate(sqlite3_file *sf, sqlite_int64 size)
{
#ifdef TRACE
  fprintf(stderr, "Atruncate\n");
#endif
  return call_callback(fill_callback_args(sf, CB_IO_TYPE_TRUNC, NULL, 0, size));
}

/*
** Sync an callback-file.
*/
static int callbackSyncNot(sqlite3_file *pFile, int flags)
{
  return SQLITE_OK;
}

/*
** Return the current file-size of an callback-file.
*/
static int callbackFileSizeNot(sqlite3_file *pFile, sqlite_int64 *pSize)
{
 *pSize = 0;
  return SQLITE_OK;
}
static int callbackFileSize(sqlite3_file *sf, sqlite_int64 *pSize)
{
  callback_file *p = (callback_file *) sf;
  struct callback_args *a = PTR_TO_DATAPTR(p->sm);
  int rc;
#ifdef TRACE
  fprintf(stderr, "cb fileSize\n");
#endif
  rc = call_callback(fill_callback_args(sf, CB_IO_TYPE_FSIZE, pSize, 0, 0));
#ifdef TRACE
  fprintf(stderr, "cb fileSize %ld\n", (long) a->offset);
#endif
  *pSize = a->offset;
  return SQLITE_OK;
}

/*
** Lock an callback-file.
*/
static int callbackLockNot(sqlite3_file *pFile, int eLock)
{
  return SQLITE_OK;
}

static int callbackLock(sqlite3_file *pFile, int eLock)
{
  callback_file *p = (callback_file *) pFile;
#ifdef TRACE
  fprintf(stderr, "Alock\n");
#endif
  return SQLITE_OK;
}

/*
** Unlock an callback-file.
*/
static int callbackUnlockNot(sqlite3_file *pFile, int eLock)
{
  return SQLITE_OK;
}
static int callbackUnlock(sqlite3_file *pFile, int eLock)
{
  callback_file *p = (callback_file *) pFile;
#ifdef TRACE
  fprintf(stderr, "Aunlock %s\n", p->zName);
#endif
  return SQLITE_OK;
}

/*
** Check if another file-handle holds a RESERVED lock on an callback-file.
*/
static int callbackCheckReservedLockNot(sqlite3_file *pFile, int *pResOut)
{
  return SQLITE_OK;
}

static int callbackCheckReservedLock(sqlite3_file *pFile, int *pResOut)
{
  callback_file *p = (callback_file *) pFile;
#ifdef TRACE
  fprintf(stderr, "cb check reserved lock\n");
#endif
  return SQLITE_OK;
}

/*
** File control method. For custom operations on an callback-file.
*/
static int callbackFileControl(sqlite3_file *pFile, int op, void *pArg)
{
  return SQLITE_NOTFOUND;
}

/*
** Return the sector-size in bytes for an callback-file.
*/
static int callbackSectorSizeNot(sqlite3_file *pFile)
{
  return 0;
}

static int callbackSectorSize(sqlite3_file *sf)
{
  callback_file *p = (callback_file *) sf;
  struct callback_args *a = PTR_TO_DATAPTR(p->sm);
  call_callback(fill_callback_args(sf, CB_IO_TYPE_BLKSZ, NULL, 0, 0));
#ifdef TRACE
  fprintf(stderr, "SectorSize %ld\n", (long) a->offset);
#endif
  return a->offset;
}

/*
** Return the device characteristic flags supported by an callback-file.
*/
static int callbackDeviceCharacteristics(sqlite3_file *pFile)
{
  return SQLITE_IOCAP_ATOMIC | SQLITE_IOCAP_SAFE_APPEND;
}

/*
** Delete the file located at zPath. If the dirSync argument is true,
** ensure the file-system modifications are synced to disk before
** returning.
*/
static int callbackDelete(sqlite3_vfs *aVfs, const char *zPath, int dirSync)
{
  return pVfs->xDelete(pVfs, zPath, dirSync);
}

/*
** Test for access permissions. Return true if the requested permission
** is available, or false otherwise.
*/
static int callbackAccess(
  sqlite3_vfs *aVfs,
  const char *zPath, 
  int flags, 
  int *pResOut
)
{
#ifdef TRACE
  fprintf(stderr, "Aaccess\n");
#endif
  return pVfs->xAccess(pVfs, zPath, flags, pResOut);
}

/*
** Populate buffer zOut with the full canonical pathname corresponding
** to the pathname in zPath. zOut is guaranteed to point to a buffer
** of at least (CALLBACK_MAX_PATHNAME+1) bytes.
*/
static int callbackFullPathname(
  sqlite3_vfs *aVfs, 
  const char *zPath, 
  int nOut, 
  char *zOut
){
#ifdef TRACE
  fprintf(stderr, "cb fpn %s\n", zPath);
#endif
  return pVfs->xFullPathname(pVfs, zPath, nOut, zOut);
}

/*
** Open the dynamic library located at zPath and return a handle.
*/
static void *callbackDlOpen(sqlite3_vfs *aVfs, const char *zPath){
#ifdef TRACE
  fprintf(stderr, "Adlop\n");
#endif
  return pVfs->xDlOpen(pVfs, zPath);
}

/*
** Populate the buffer zErrMsg (size nByte bytes) with a human readable
** utf-8 string describing the most recent error encountered associated 
** with dynamic libraries.
*/
static void callbackDlError(sqlite3_vfs *aVfs, int nByte, char *zErrMsg){
#ifdef TRACE
  fprintf(stderr, "Adlerr\n");
#endif
  return pVfs->xDlError(pVfs, nByte, zErrMsg);
}

/*
** Return a pointer to the symbol zSymbol in the dynamic library pHandle.
*/
static void (*callbackDlSym(sqlite3_vfs *aVfs, void *p, const char *zSym))(void){
#ifdef TRACE
  fprintf(stderr, "Adlsy\n");
#endif
  return pVfs->xDlSym(pVfs, p, zSym);
}

/*
** Close the dynamic library handle pHandle.
*/
static void callbackDlClose(sqlite3_vfs *aVfs, void *pHandle){
#ifdef TRACE
  fprintf(stderr, "Adlclo\n");
#endif
  return pVfs->xDlClose(pVfs, pHandle);
}

/*
** Populate the buffer pointed to by zBufOut with nByte bytes of 
** random data.
*/
static int callbackRandomness(sqlite3_vfs *aVfs, int nByte, char *zBufOut){
#ifdef TRACE
  fprintf(stderr, "arand\n");
#endif
  return pVfs->xRandomness(pVfs, nByte, zBufOut);
}

/*
** Sleep for nMicro microseconds. Return the number of microseconds 
** actually slept.
*/
static int callbackSleep(sqlite3_vfs *aVfs, int nMicro){
#ifdef TRACE
  fprintf(stderr, "Aslee\n");
#endif
  return pVfs->xSleep(pVfs, nMicro);
}

/*
** Return the current time as a Julian Day number in *pTimeOut.
*/
static int callbackCurrentTime(sqlite3_vfs *pVfs, double *result){
#ifdef TRACE
  fprintf(stderr, "Act\n");
#endif
  *result=-1.0;
  return -1;
}

/*
** Initialising the wrapper vfs callback_vfs
*/

#define max(a,b) ((a) > (b) ? (a) : (b))

sqlite3_vfs *callback_sqlite3_vfs_init(obj cb) {
  pthread_mutex_init(&the_shared_memory_mux, NULL);
  if(pVfs == NULL ) {
    pVfs = sqlite3_vfs_find(NULL); // fetch default vfs
    if( !pVfs ){
      return NULL;
    }
    callback_vfs.szOsFile = callback_vfs.szOsFile - sizeof(sqlite3_file) + pVfs->szOsFile;
    sqlite3_vfs_register(&callback_vfs, 0);
  }
  the_vfs_callback = cb;
#ifdef TRACE
  fprintf(stderr, "registered %s \n", callback_vfs.zName);
#endif
  return &callback_vfs;
}

int rs_cond_signal(obj rc) {
  return pthread_cond_broadcast(OBJ_TO_C_PTR(pthread_cond_t *, rc));
}
