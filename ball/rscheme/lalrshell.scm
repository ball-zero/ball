(define-macro (lalr-parser . arguments)
  ;; (with-module lalrgen (gen-lalr-parser tokens rules))
  (with-module compiletime (apply gen-lalr-parser arguments)))

(define-macro (define-llrbtree-code
		features
		update
		init-root-node!
		t-lookup
		t-min
		t-fold
		t-for-each
		t-insert
		t-delete
		t-delete-min
		t-empty?
		t-k-eq?
		t-k-<?
		t-<?
		left set-left!
		right set-right!
		color set-color!
		set-leftmost!
		)
  (with-module
   compiletime
   (make-llrbtree-code
    features
    (eval (cons 'lambda update))
    init-root-node!
    t-lookup
    t-min
    t-fold
    t-for-each
    t-insert
    t-delete
    t-delete-min
    t-empty?
    (eval (cons 'lambda t-k-eq?))
    (eval (cons 'lambda t-k-<?))
    (eval (cons 'lambda t-<?))
    left set-left!
    right set-right!
    color set-color!
    set-leftmost!
    )))
