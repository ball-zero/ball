(define-class <checked-mailbox> (<mailbox>)
  mailbox-value-predicate)

(define (make-checked-mailbox name pred)
  (%make <checked-mailbox>
	 (%make <vector> #f #f #f #f #f)                ; DEQ state
	 0                                              ; DEQ front
	 0                                              ; DEQ back
	 #t                                             ; MBOX has-data?
	 (and (pair? name) (car name))
	 pred))

(define-method mailbox-send! ((self <checked-mailbox>) value)
  (assert ((mailbox-value-predicate self) value))
  (send-message! self value))

(define-method mailbox-send! ((self <mailbox>) value)
  (send-message! self value))
