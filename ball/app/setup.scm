;; (C) 2000 Joerg F. Wittenberger <Joerg.Wittenberger@pobox.com>; GPL

;; Changed: 2016: remove old wiki.

;; Application setup
;;
;; This part of the setup contains the second part of the setup
;; process, where users and applications are installed.

;; Here we install an example user "gonzo" with password "oznog".

(define (application-setup-procedure . level)
  (define (make-new name tree . att)
    (make-link-form name (apply make-new-element tree att)))

  (define system-user "system") ;; currently hard coded

  (define setup-user "gonzo")
  (define setup-password "oznog")

  (define (make-link-id name id)
    (make-link-form name name (make-xml-element
			       'id #f '() (make-xml-literal
					   (oid->string id)))))
  (define (plain tree) (xml-format tree))

  (define xml-content (property 'content-type "text/xml"))
  (define jpg-content (property 'content-type "image/jpeg"))

  (define parsed-xml xml-parse)

  (define host (format #f "127.0.0.1:~a" ($http-server-port)))
  (define host-prop (property 'host host))
  (define write-prop (property 'type 'post))
  (define get-prop (property 'type 'read))

  ;; Mapped over a list of lists, where the car is an integer.  When
  ;; the level argument passed into setup-procedure indicates a lower
  ;; init level than this number, the request is returned, otherwise
  ;; #f telling the driver to skip the request.  0 is the virgin start
  ;; up and -1 evvectivly disables the request alltogether.
  ;;
  ;; cadr is the log message to emit and cddr are argments to
  ;; http-send...!
  (define setup-ve
    (let ((level (if (pair? level) (car level) 0)))
      (lambda (e)
        (delay (if (>= (car e) level)
                   (begin
                     (format #t (cadr e))
                     (logerr (cadr e))
                     (cddr e))
                   #f)))))

  (bind-exit
   (lambda (return)

     (http-for-each
      return
      (guard
	(ex (else (logerr "~a\n" ex) (error ex)))
       (map
       setup-ve
       `((2
	  " setting up system control accout \"system\"\n"
	  ,(make-message
	    xml-content
	    (property 'dc-identifier "")
	    (property
	     'mind-body
	     (plain
	      (make-link-form
	       "system"
	       (sxml
		`(letseq
		  (bindings
		   (bind (@ (name "public")) (id ($$ ,literal ,(public-oid))))
		   (bind
		    (@ (name "reflexive"))
		    (new
		     (@ (action "public"))
		     (output
		      ($$ ,parsed-xml ,(filedata (make-pathname '("app") "reflexive-xdslt" "xml"))))))
		   )
		   (new
		    (@ (action "reflexive")
		       (secret ,(secret-encode setup-password "magic:")))
		    (link (@ (name "xslt-method")) (ref "reflexive"))
		    (link (@ (name "xslt-bail-reflexive")) (ref "reflexive"))
		    (link (@ (name "public")) (ref "public"))
		    (link
		     (@ (name "system"))
		     (new
		      (@ (action "reflexive"))
		      ;;(link (@ (name "reflax")) (ref "reflexive"))
		      (output
		       ($$ ,parsed-xml ,(filedata (make-pathname '("policy") "sysinf" "xml"))))))
		    (output
		     ($$ ,parsed-xml ,(filedata (make-pathname '("app") "xslt-user" "xml")))))
		   )))))
	    host-prop write-prop)
	  ,(administrator) ,administrator-password)
#|

What we woud like to do here is to restrict access to the system
control interface here.  However because the requests are eagerly
contructed, we need a real dialog to the server.  This would need a
rewrite of the setup logic.

	 (1 " change protection of system control interface.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "system")
	      (property 'mind-body
			(plain (make-xml-element
				 'form namespace-mind mind-xmlns-attribute-list
				 (node-list
				  (make-xml-element 'action namespace-mind '() "protect")
				  (make-xml-element 'challenge namespace-mind '() (literal (entry-name->oid "system")))))))
	      host-prop write-prop)
	    ,system-user ,setup-password)
|#
         (1 " publishing short memo receiver code in 'system'\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "sms.code"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "sms" "xml")))
                                 (cons 'action "public"))))
               host-prop write-prop)
             ,system-user ,setup-password)
         (3 " webdav tree\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "davtree.code"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "xslt-davtree" "xml")))
                                 (cons 'action "public"))))
               host-prop write-prop)
             ,system-user ,setup-password)
         (3 " wallet skin code (simple)\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
				"wallet-skin-simple"
				(parsed-xml
				 (filedata (make-pathname '("app") "wallet-skin-simple" "xml")))
				(cons 'action "public"))))
               host-prop write-prop)
             ,system-user ,setup-password)
         (3 " wallet code (basic version)\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
				"wallet.code"
				(parsed-xml
				 (filedata (make-pathname '("app") "wallet" "xml")))
				(cons 'action "public"))))
               host-prop write-prop)
             ,system-user ,setup-password)
         (3 " wallet icon\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property
		'mind-body
		(plain (make-new
			"wallet.icon"
			(sxml `(output (@ (method "text") (media-type "image/x-icon"))
				       ($$ ,filedata ,(make-pathname '("app") "wallet-icon" "ico"))))
			(cons 'action "public"))))
               host-prop write-prop)
             ,system-user ,setup-password)
	 (1 " setting up collection-method.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"collection-method"
				(parsed-xml
				 (filedata (make-pathname '("policy") "webdav" "dsl")))
				(cons 'action "public"))))
	      host-prop write-prop)
	    ,system-user ,setup-password)
	 (1 " setting up custom-collection-method.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"custom-collection-method"
				(parsed-xml
				 (filedata (make-pathname '("policy") "webdav-custom" "dsl")))
				(cons 'action "public"))))
	      host-prop write-prop)
	    ,system-user ,setup-password)
	 (1 " setting up private rights text.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"private"
				(parsed-xml
				 (filedata (make-pathname '("mechanism") "private-place" "xml")))
				(cons 'action "public"))))
	      host-prop write-prop)
	    ,system-user ,setup-password)
	 (2 " setting up user page gonzo\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 setup-user
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "xslt-user" "xml")))
                                 (cons 'secret (secret-encode setup-password "magic:"))
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,(administrator) ,administrator-password)
         (3 " link public\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain
			 (make-link-form
			  "public" (make-xml-element
				    'id #f '() (make-xml-literal
						(oid->string (public-oid)))))))
	      host-prop write-prop)
	    ,setup-user ,setup-password)
         (3 " webdav net\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "davnet"
                                 (empty-node-list)
				 (cons 'protection setup-user)
                                 (cons 'action "system/collection-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)


	 ;; "davtree" and "directory" will be merged under the name
	 ;; "directory" into one thing doing both functions.

         (3 " webdav tree\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "davtree"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "xslt-davtree" "xml")))
				 (cons 'protection setup-user)
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (5 " setting up gonzo's directory code (old, relfexive style)\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "directory"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "directory" "xml")))
				 (cons 'protection setup-user)
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)

         (5 " setting up gonzo's message editor (reflexive style)\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "msged"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "xslt-edit" "xml")))
				 (cons 'protection setup-user)
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (1 " setting up gonzo's short memo receiver\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "sms"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "sms" "xml")))
				 (cons 'protection setup-user)
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (1 " setting up gonzo's short memo cheater\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "cheater"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "cheater" "xml")))
				 (cons 'protection setup-user)
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (3 " setting up gonzo's web mail\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "mailer"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "webmail" "xml")))
				 (cons 'protection setup-user)
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (-3 " setting up gonzo's address data base\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "addresses"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "xslt-addrdb2" "xml")))
				 (cons 'protection (string-append setup-user "/public"))
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (-2 " setting up application: Buchhaltung\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "Buchhaltung"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "Buchhaltung" "xml")))
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (-2 " setting up test application: latch\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "latch"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "xslt-latch" "xml")))
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (2 " setting up user page \"me\"\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "me"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "xslt-user" "xml")))
                                 (cons 'secret (secret-encode "em" "pepper"))
                                 (cons 'action "system/xslt-method"))))
               host-prop write-prop)
             ,(administrator) ,administrator-password)
         (1 " publishing up my short memo receiver code\n"
             ,(make-message
               xml-content
               (property 'dc-identifier "")
               (property 'mind-body
                         (plain (make-new
                                 "sms.code"
                                 (parsed-xml
                                  (filedata (make-pathname '("app") "sms" "xml")))
                                 (cons 'action "public"))))
               host-prop write-prop)
             ,setup-user ,setup-password)
         (-1 " linking nobody into public\n"
             ,(delay
                (make-message
                 xml-content
                 (property 'dc-identifier "")
                 (property
                  'mind-body (plain (make-link-id "nobody"
                                                  (entry-name->oid "nobody"))))
                 host-prop write-prop)))
         ))))
     )))
