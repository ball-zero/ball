# Wallet-basic

## Purpose

This code is intented to document the basic API at a single page in
simple examples.  It's only practical purpose is to serve as a save
fallback to upload a new 'skin' (and allow save removal of any active
skin).

Beware: Simplicity rules here.  No link carries login information.
This is not usable in practice with anon access codes.

Normally hidden form input elements are given as read only text elements.

## Status

This is in the process to be stripped down from the first skinable version,
which grew beyond bounds when adding features to the user interface.

We expect the API for mutating applications to stabilize soon.

## Code

Over all structure of the code (to be changed):

Some variables to be (re)used and named templates to be called.

A xsl:template named "html" comprising the "normal" user interface.

A xsl:template matching type "read" where the processing starts for read operations.

A xsl:template matching type "write" invoked for mutation requests (submitted forms).

A core:body holding tree more one-expression scripts to invoke the
bail interpreter on the subject with the template from the "parent contract".
