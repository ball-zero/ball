# Command Line

Status: The command line interface is under development.  This
documents the current status.

The build process is supposed to be changed which will modify the
command line program name.  Herein a shell alias is assumed:

    alias ASK=askemos-chicken.bin

# SYNOPSIS

ASK `-bare` *bare command* [ *options* ] ...

ASK `-offline` $REP *offline command* [ *options* ] ...

ASK `-online` $REP *offline command* [ *options* ] ...

ASK provides a simple command line to control an Askemos/BALL representative.

The *offline command*s require a directory $REP containing the
representatives repository which is offline, that is no process is active within.
The *online command*s require a directory $REP containing a running representative.
Commands are sent to the running representative.
The *bare commands* allow to rund scripts without any access to a repository at all.

# OFFLINE COMMANDS

- `load` [ `-o` *output* ] *file* ...

    Load *file*s.  If `-o` is given write the value from the last
    expression into *output*.

- `via` *entryname* ( `query` | `send` ) [ ( *file* | `-` )

    Dispatch a request (either an idempotent `query` or an possibly
    mutation one for `send`) via the *entryname*.

- `ps` *OID*

    Print status of *OID*.

- `meta` `-public` *OID*

    Print public visible metadata about *OID*.

- `channel` *channel command*

- `tofu` *CN* *USER* *PASSWD*

    Initialize TLS CA and entity certificate with common name *CN* and
    user *USER* in TOFU mode (trust on first use).  *PASSWD* is used
    to protect the private key of the CA.

## CHANNEL COMMANDS

- `ls`
    List all channels.

- `-link` *name* *value*

    Link new channel under *name* to *value*.  Value may be an OID.

- `-new` *name* *contract* *data* [ *content-type* ]

    Create a new channel going by the name *name* under *contract*
    given initial *data* with optional *content-type*.

- `-drop` *name*

    Remove the channel *name*.

- `-load` *name* *file*

    deprecated

- `-secret` `disable` *name*
- `-secret` `set` *name* [ `-` | *password* ]

    If a minus sign is given instead of a password, the password is
    read from standard input.

- `auth` ( `fixed` | `tofu` )

    Switch authentication mode.

    tofu: Accept users the first time they are seen in a certificate
    (and pin them to the CA key).

    fixed: Only register users from certificates to be manually
    enabled.

# ONLINE COMMANDS

- `connect` *URL*

- `support` *from* *name* [ *remote* ]

    Create local binding under *name* for the *remove* user/OID at
    representative *from*.  *remove* defaults to *name*.

- `via` *entryname* ( `query` | `send` ) [ ( *file* | `-` )

- `stop`
    reserved
- `start`
    reserved

- `print` *property*
    Print *property* on standard output (in script-readable format).
    *propert* may be one of:

    - `public`
        the public identifier of the rep (location)
    - `onion-route`
        the address of the onion service
    - `pid`
        the numeric process identifier

# BARE COMMANDS

- `load` [ -o *output* ] *file* ...

    Load *file*s.  If `-o` is given write the value from the last
    expression into *output*.