##### User Management

This section explains how to deploy "smart contracts", here those
representing the demo users (also called "entry points"), by selection
relevant notaries.

There is another default password here, the "/Administrator Secret/".
It is required in circumstances where user entry points may be added
or removed as this may result in horrible damage if used improperly.

The default is "sesam".  It can be changed in the "limits" section of
the system control panel.

Next we establish the notion of an user account a.k.a. entry point
being replicated over subset of the network.  Step by step until it
runs on the minimum of four notaries which is the whole network by
now.

This is again a tedious task.  For motivation recall what we simulate
here: In reality each notary would be under control of the person
having physical access to their notary at their machine.  They all
enter into a multilateral contract here to support the mere existence
of an "entry point" going by the name "me".  They do so by keeping a
copy of the object and validating all updates.  We simulate a
multi-party trust relationship wrt. the "state" of me.

We pick a user to run on all reps.  Start with /r4/ and enable "me"
under the name of "me".

Find the "entries" in the menu of the system control panel.  At the
bottom of the page find the listing of the existing entries and
notice: "me" is a default channel name.  We need to remove that before
we can reuse the name.

Above the listing there is the section "remove channel".  Enter "`me`"
as the name to be freed the "/Administrator Secrect/" and push "remove".

In the first form "create channel" fill in /here/ as "`me`" as the
name valid at the origin rep, here /r1/, the public OID of /r1/ as /from host/
and in the field /user/ "`me`" for the name to be used locally.

Wait what?  The "public OID of /r1/"?  That's the hash generated to
identify /r1/ when the rep was created.  The easiest way to find out
for now is to click the link "debug" right to the "restart" button in
the control panel.  The OID appears here labeled slightly misleading
as the "/Author/" here.  The Hash is the lower case hex number
starting with an upper case "A" right of the "Author" label.  (BTW:
The content linked under the "public OID" is actually the social
contract this rep assumes.)

So in order to complete the form copy that OID **as found on /r1/**
into the field labeled /from host/ on /r4/.  Don't get mixed up!  In
order to keep things half way simple we use only the default users
from the /r1/ in this demo.  Write the OID of /r1/ down somewhere and
only use this as /from host/.

Complete the form on /r4/ and find a new channel "me" in the listing.
Click on it and the log output in the terminals should indicate that
your request is actually forwarded from /r4/ to /r1/.

First step completed.  /r4/ knows that this "me" exists.  But it lives
on /r1/ exclusively.  That's we gonna change now.

In preparation, first we go through /r2/ and /r3/ and repeat the
delete-and-create procedure as just completed on /r4/.  (Alternatively
you may choose a different local name in the field labeled /here/.  But
that could become too confusing for now.  (Keyword "Zooko's triangle"...)

Now from /r1/ root (i.e. [https://127.0.0.1/](https://127.0.0.1/) )
click "Log out" and log in anew using "`me`" as name and "`em`" as
password.  Follow the "Internals" link and change the default now.

Next, still in the "Internals" follow the menu entry "support" just
above the password change form.

At the end of the page there is a listing of just one, the pubic OID
you wrote down before.

Now we need more.  We need all the public OIDs of all the four nodes.
Fortunately they are nicely listed behind the "network" menu entry of
the system control panel you might already be familiar with.  The left
side of the "Host Map".

Now copy the one pertaining to `127.0.0.1:12443` (or "`r4`" if seen
from /r4/) into the entry field under "Toggle support" and push
"submit".

At this point not only /r1/ has something to say about "me" but also
/r4/.  However only /r4/ has a certificate to authorize requests in
the name of "me".  (The 'O' in the subject line of the reps
certificate rules here.  A story to be deferred at this point.)

Therefore you need to switch to /r4/ port
[https://127.0.0.1:12443](https://127.0.0.1:12443) and click "Log out"
to log in again as "me" using the password you set before.

Again find "Internals" then "support" and repeat with the public OIDs
of the other two reps.  If everything went successfully, the listing
should now have all four OIDs.  That is all further operations on this
place are run in byzantine agreement over all four reps.  The logs
should confirm: Go to
[https://127.0.0.1:12443](https://127.0.0.1:12443) and push the
"tutorial" button while watching the logs on the terminals.

Now you can log in at all four reps as "me" and explore.  However try
anything but /r4/ in an attempt to modify things (i.e., push buttons):
It should not do any harm.  Just from /r4/ it should matter.

Congratulations once more!  You mastered to replicate a channel for
communication in Askemos.

In order do play the typical "Alice and Bob" examples, you will need a
second entry point for Bob.  For the sake of the demo we assume that
you repeat at some point the whole procedure of the the account
replication for "gonzo" to /r3/ instead of /r4/.

It should now be possible to be logged in /r3/ using /gonzo/ push
"create" open some obscure form and "close" to close it again.  The
same for "me" on /r4/.  Now we have enough to simulate two interacting
agents "me" calling from /r4/ and "gonzo" calling from /r3/ using two
additional notaries /r1/ and /r2/ to audit their transactions.
