#### Example Applications

Once your entry point is replicated over the network, continue with
this tutorial.

Log in at /r3/ as "gonzo" and push the button "tutorial".  In the form
field labeled "Tutorial" push the button "sms".  Now the button should
disappear and a view of incoming messages be shown.  The use should be
intuitive: anybody may type a message and push enter to send it.  The
owner should be able to check the "delete" boxes and push "sms weg" to
remove them.  This little tool will pretend to be a communication
channel like any chat for the following demo.

NB: The following demo will silently fail partially if it can no find
the executable `qrencode` in the path.  This may be confusing; you
have been warned.

Push the button "wallet" and find it replaced with a link to a fresh,
uninitialized wallet.  Follow the link.  Find it to ask for a title.
Give it any, you will later learn how to change it.

Repeat these steps (e.g., from a different browser) on /r4/ using the
user "me".

NB: The wallet is meant to be an example for a possible "back-end"
implementation.  Caveats apply regarding the user interface.  Under
the assumption that a payment system leans toward strict security
requirements and in order to make the code more obvious, there no
JavaScript is used.  Admitted: the workflow could be smother if that
restriction was relaxed.  The user interface is therefore
customizable.  It can be replaced or removed in the section "manage",
subsection "skin".  The default is even simpler and intended to
document how to actually write a skin.

The [documentation for the
wallet](http://ball.askemos.org/A0cd6168e9408c9c095f700d7c6ec3224/?_v=search&_id=1856)
should cover most use cases.

The fast path to at least have a first transaction sent:

1. Make the wallets known to each other: Copy the OID right next to
   the title at top of one wallet to the form field in the "Incoming"
   section of the other wallet (in the other browser) and push "Go".
   The title and terms (empty at this point) should be on display.
   Push "change contract" to accept it as a counter party.  (Listed in
   "Contacts").

2. At the sender side only: Click "Claim". This allows to define
   payment instruments (or similar legal titles).  Just enter an
   amount of shares, a name to be used and terms and conditions any
   claims defining the value.  This is your ICO moment.  Be creative.
   Convince whomever shall accept it why your issue is valuable.
   Well, this is a demo, don't bother too much.  Example: if you own
   fungible valuables in another currency, name those.  Now you can
   transfer them to Bob without paying transaction fees to the bank.

3. Still at the sender: Click "Order".  Fill in the amount, a subject
   and possibly additional notes and push "Sign Order".

4. Copy the green link with the OID to the clipboard.  Switch to the
   receivers browser.  Paste the link (or OID) into the "incoming"
   field as done with the wallet before and push "Go".

   Find a display of the incoming payment.  Push "Sign" to sign a
   receipt.

5. Convey the receipt (again the green link) to the sender the same
   way as before.  Once you are done, push the "Done" button to view
   your wallet again.

The next two buttons in the tutorial are "clipboard" and
"publications".  Both create an upload area.  The former intended for
private stuff not visible to others.  The latter intended for
publications.  (FIXME: The following change of access rights ought to
be automatic.  TBD: Figure out why it did not work.)  To fix the
permissions of the latter, click "debug" (atop the right side), then
"rights".  *Append* `/public` to the OID already there and push
"Protection" to apply the change.

It should be possible to upload files via the web form.  Alternatively
these objects could be mounted via WebDAV to be used with ordinary
tools for file management.
