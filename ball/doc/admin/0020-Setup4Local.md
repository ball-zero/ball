## A Full Test Network

To create a full network you need at least four reps.

This section walks through the initial setup up to the point that four
reps all support the two famous smart contracts "Alice" and "Bob".
(Using byzantine agreement, because "51% are not enough"[citation
needed].)  The next section shows them installing their applications.

This file pertains to the "traditional setup", which continues to
exist as it serves to test more features.  For the new, simpler setup
see [0120-Setup4Local.md](0120-Setup4Local.md).

### Step One: Create The Reps

The Makefile (still in the `ball`subdirectory) supports four test
reps.  The names are hard coded and the same names used from
`scripts/demo4.sh` in an attempt to start each in a terminal of its
own.

To create all four run:

    $ make r1 r2 r3 r4

These use as PORTBASE 9000, 10000, 11000 and 12000 respectively.

start them like

    $ ./scripts/demo4.sh

The next two subsections will complete the setup.  Once this first
section is completed you should have a network of peers trusting each
other by means of cryptographic certificates.  The second section will
cover how to actually deploy shared processes a.k.a. smart contracts.

Following that the tutorial will assist in first steps up to the point
that a first not-so-crypt currency is issued - that's the ICO
fashionable these days - and transferred from "Alice" to "Bob".

For the impatient: There is a (will be) directory `demo4` in the
[ball demos repository](https://gitlab.com/ball-zero/ball-demos).
It contains the reps r1..r4 set up (mostly) as
explained below.  Only the essentials of the tutorial have been
completed and the default passwords where left alone.

Copy reps into place and run as before.

### Step Two: Connect The Network

This is a daunting task.  We don't have a hard coded bootstrap hub
like most p2p networks to automate this task.  Actually the whole
thing is designed for the protocol implementation to be plugable; we
might want to reuse a good distributed messaging platform or
transparent network layer.

The idea we establish here is that there is a network of reps
a.k.a. notaries each owned/operated by the "system" account of the
node and each trusting the others by way of a valid X509 certificate.

This version uses a single CA (on `r1`) issuing all
certificates. There is an [alternative setup using individual
CA's](0120-Setup4Local.md).

Here roll your own using a single CA.

1. Select any (we shall use /r1/ here) to be your CA.

2. Log into
    [http://system@localhost:9081](http://system@localhost:9081) using
    the default password.

3. Find the link "System". Push the button "Restrict access to this user".
4. Find the link "Certs", second section "Certificate Authority Certificate".

    Here you can choose to upload you own CA on the left or create a
    new one.  Do the latter.  Remember the password.

5. (Using a new window or tab) Log into /r2/ at
    [http://system@localhost:10081](http://system@localhost:10081)
    repeat steps 2 and 3 but in step 4 retrieve the CA from /r1/.  To
    do so enter `127.0.0.1:9080` into the field "Source URL".  Enter
    the name of the default user (as valid on /r1/) you will use later
    for further experiments.  In order to complete the demo use
    "system" for /r2/ "gonzo" for /r3/ and "me" for /r4/. The push the
    button: "Fetch CA".

6. Proceed to the link "cm" right of "certs" and find your certificate
    request prepared.  Copy the PEM-encoded form at the right,
    including the links `BEGIN CERTIFICATE REQUEST` and the ending
    line into the clipboard.  Go back to the tab /r1/ at 9081 (or
    later port 9443 when repeating for the other reps over HTTPS) find
    the link "cm" there too.  Paste the certificate request into the
    form and push "store".

    The request is now displayed for verification.  Enter you password
    from step 4 and push "sign".

7. Now the page is a bit long as the certificate is listed too.  Copy
    the encoded certificate from `---BEGIN CERTIFICATE---` including
    `---END CERTIFICATE---` into the clipboard and switch back to the
    tab for /r2/ at 10081.

8. At /r2/ find the "certs" link again.  In the section "Local X509
    Certificate" at the right side there is an input field beneath
    "Submit Host Cert".  Paste your certificate there.  (Alternatively
    you might upload it using the file upload control above the
    button.)  Push the button.

9. Now you should be able to use HTTPS with r2. Try
    [https://127.0.0.1:10443](https://127.0.0.1:10443)
    and never come back to use the HTTP port to log in.

    Not that since we use a self signed certificate from step 4 as CA,
    your browser will complain.  Make it accept the certificate.

10. Back again at /r1/ still at HTTP port 1081 create a certificate
     for this host too.  Find the the "cm" link towards the end of the
     page the section `request`.  Accept all the magic values filled
     (including the empty password) and just push the button labeled
     "new".

    A new request and the last certificate are now shown.  Forget the
    latter and since we have not discussed how to check the former
    just believe it does and find the /request/ form again.  Enter the
    password this time and push "sign".  Now the cert should match the
    request.

    For simplicity you do not need to repeat the steps to copy the
    cert over to the "cert" section.  The last section of the "cm"
    page has a button "set host cert" to be pushed now.

11. Switch to [https://127.0.0.1:9443](https://127.0.0.1:9443) now and
    log in again.  Depending on how you make your browser accept the
    certificate in step 9 you may or may not have to make it shut up
    again.

12. Come back to the "cm" link in the system control panel in /r1/ as
    you will need this one to sign the certificates of the other two
    reps.  Repeat steps 5 to 9 for /r3/ and /r4/ using ports 11081 and
    12081 for plain HTTP an then switch to 11443 respectively 12443.


At this point all the reps should be running and have their HTTPS
ports open.  But they still don't know how to find each other.

On reps /r2/, /r3/ and /r4/ find the system control panel again.  This
time follow the link "network".  In the section /connect/ enter
`https://127.0.0.1:9443` and submit (push enter).  Do the respective
from each rep, i.e., make them "say hello" to each other.

To verify, check the table "Host Map" under the /connect/ form: it
should now hold four entries at all reps.  The connection table should
indicate that there are tree active connections.  Retry the /connect/
trick to on those nodes missing a link. FIXME: The last step sometimes
takes an unreasonable amount of time and there should be no need to
"retry" (20180122).



Congratulations!

So what is does the certificate actually certify?  The `L` component
of the X509 subject is used to identify the `notary`; the machine,
which mechanically verifies valid transactions.  The optional `O`
component identifies the "entry point" for which the notary may
"speak" in the sense of signing requests in the name of this user.

If everything went as expected, this means that /r4/ may issue
requests for "me" while /r3/ may issue requests for "gonzo".

