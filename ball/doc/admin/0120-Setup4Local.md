## A Full Test Network

To create a full network you need at least four reps.

This section walks through the initial setup up to the point that four
reps all support the two famous smart contracts "Alice" and "Bob".
(Using byzantine agreement, because "51% are not enough"[citation
needed].)  The next section shows them installing their applications.

### Step One: Create The Reps

The Makefile (still in the `ball` subdirectory) features a target
`reps4demo4`.

To create all four reps run:

    $ make reps4demo4 PASSWD=changethis

You may give them names like this to enable onion routing:

    $ make reps4demo4 PASSWD=changethis R1=*service_1*.onion .. R4=*service_4.onion*

This will run all the per-rep setup for `r1` to `r4`.  A subdirectory
is created for each.  On rep `r3` a local user account named "Alice"
and on `r4` there is a user "Bob".  Both follow the "wallet" contract
code.

All passwords in this process are hard coded to `2017` in the
Makefile.

Now start them like

    $ ./scripts/demo4.sh

The above command expects `xfce4-terminal`to be available.  Failing
that, you may want to modify the script to use another teminal.
Alternatively just open four terminals and run `make srX` in each of
them, replacing the `X` with 1..4.

The next subsection you will complete the setup.  Once completed you
should have a network of peers trusting each other by means of
cryptographic certificates and have the shared processes a.k.a. smart
contracts "Alice" and "Bob" deployed on all reps.

Following that the tutorial will assist in first steps up to the point
that a first not-so-crypt currency is issued - that's the ICO
fashionable these days - and transferred from "Alice" to "Bob".

### Step Three: Connect The Network and Share Accounts

The idea we establish here is, that there is a network of reps
a.k.a. notaries trusting each other by way of X509 certificates.  The
peer in `r3` is owned and operated by "Alice" while `r4` belongs to
Bob.

This setup uses one self signed CA for each rep, though contrary to
the "normal" use of X509 in TLS, the CA is pinned to the first
certificate seen by a peer.  TOFU, "trust on first use".  There is a
(deprecated) [alternative setup using CA's as
"normal"](0020-Setup4Local.md).

Once you started all 4 reps run:

    ./scripts/setup-demo4.sh

If everything went as expected, this means that /r4/ may issue
requests for "Bob" while /r3/ may issue requests for "Alice".  I.e.,
you can log in as Alice here:
[https://localhost:11443](https://Alice:2017@localhost:11443) and as
Bob here:
[https://Bob:2017@localhost:12443](https://Bob:2017@localhost:12443).

The impatient may proceed now to [play with the
wallet](0140-ExampleWallet.md).

What did the script [setup-demo4](../../scripts/setup-demo4.sh) did:

1. Tell the reps to establish a network connection to the others.

2. Pull Alice from `r3` and Bob from `r4` and register a local name
for them.

3. On rep `r3` set the name of the local user "Alice" to "Alice
Example" and on `r4` do the same for "Bob" als "Bob Beispiel".

4. Step-by-step expand the replica set for "Alice" and "Bob" to run on
all four reps.

See [setup-demo4](../../scripts/setup-demo4.sh) for details.
