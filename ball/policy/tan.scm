(define (select-tan-value nl) (data nl))

(define (use-tan! auth nl . pad)
  (check-spool-db!)
  (set! pad (and (pair? pad) (car pad)))
  (let ((tv (select-tan-value nl)))
    (db-set! *spool-db*
	     (lambda (sql)
	       (eqv? (sql-ref
		      (sql;; sql-exec-protected c
		       `(select TAN from TAN where
				O = ,auth ,@(if pad `(and PAD = ,pad) '())
				and TAN = ,tv))
		      #f #f)
		     1))
	     (lambda (sql)
	       (sql ;; sql-exec-protected c
		`(delete from TAN where O = ,auth ,@(if pad `(and PAD = ,pad) '())
			 and TAN = ,tv)))
	     (lambda (tv) (raise (format "invalid tan ~a" tv)))
	     tv)))

(define (check-tan! nl . pad)
  (check-spool-db!)
  (set! pad (and (pair? pad) (car pad)))
  (eqv?
   (sql-ref
    (if (pair? pad)
	(spool-db-select "SELECT TAN FROM TAN WHERE O = '~a' AND PAD = '~a' AND TAN = '~a'"
			 ((current-message) 'dc-creator)
			 (car pad) (select-tan-value nl))
	(spool-db-select "SELECT TAN FROM TAN WHERE O = '~a' AND TAN = '~a'"
			 ((current-message) 'dc-creator)
			 (select-tan-value nl)))
    #f #f)
   1))

(askemos-core-add-extension!
 any:
 (lambda (x r n) (is-mind-element? x 'tan))
 (lambda-transformer
  "sys:TAN:#use-tan!"
  (use-tan! (oid->string ((current-message) 'dc-creator)) (sosofo))))

(dsssl-export! check-tan! 'check-tan!)

(define (store-tans! auth pad tan1 . tans)
  (check-spool-db!)
  (if pad (set! pad (sql-quote pad)))
  (set! auth (sql-quote auth))
  (spool-db-exec
   "CREATE TABLE IF NOT EXISTS TAN(O char(33) NOT NULL, PAD integer, TAN integer NOT NULL);
CREATE INDEX IF NOT EXISTS TAN_O ON TAN(O)")
  (spool-db-exec
   (apply-string-append
    (map
     (lambda (tan)
       (if pad
	   (format "INSERT INTO TAN(O,PAD,TAN) VALUES('~a','~a','~a');" auth pad (sql-quote tan))
	   (format "INSERT INTO TAN(O,PAD,TAN) VALUES('~a',NULL,'~a');" auth (sql-quote tan))))
     (if (null? tans)
	 (call-with-input-string tan1 read-all-expressions)
	 (cons tan1 tans))))))

(define (set-tan-block oid message)
  (and-let* ((peer (get-slot message 'ssl-peer-certificate))
	     (dc-creator (mesh-cert-o peer))
	     ((quorum-local? (replicates-of (find-frames-by-id dc-creator))))
	     (body (get-slot message 'mind-body)))
	    (apply store-tans! dc-creator #f
		   (call-with-input-string body read-all-expressions)))
  (make-message))

(register-agreement-handler! 'set-tan-block set-tan-block)

(define (host-send-tans! host tan . tans)
  (let ((oid (or (and (tc-private-subject) (mesh-cert-o (tc-private-subject)))
		 (error "this is not a signed representative"))))
    (let ((host (or ((host-lookup) host)
		    (error "host not found"))))
      (https-send-message!
       #f
       (lambda (result)
	 (if (frame? result)
	     (if (not (eq? 200 (get-slot result 'http-status)))
		 (logerr "Setting TAN block failed ~a ~a answers: ~a\n"
			 oid host (get-slot result 'http-status)))))
       (make-message
	(property 'host host)
	(property 'type 'post)
	(property 'content-type text/plain)
	(property
	 'mind-body
	 (if (null? tans) tan
	     (call-with-output-string
	      (lambda (port)
		(display tan port)
		(do ((tans tans (cdr tans)))
		    ((null? tans))
		  (display #\space port)
		  (display (car tans) port))))))
	(property 'dc-identifier (literal oid))
	(property 'SOAPAction "set-tan-block"))
       (voter-auth host) #t))))
