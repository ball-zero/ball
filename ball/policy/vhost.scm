
(define *config-db-file* #f)
(define *config-db-connection* #f)

(define *vhost-cache* #f)


(define (virtual-host-default-setup!)
  (let ((db (sqlite3-open-restricted *config-db-file*)))
    (logerr "Creating new virtual host configuration in ~a\n" *config-db-file*)
    (sql-exec-protected
     db	"create table VirtualHosts (OID char(33) NOT NULL, Name text NOT NULL,
unique(OID,Name))")
    (sql-exec-protected
     db	"create index VirtualHosts_name ON VirtualHosts(name)")
    (sql-exec-protected
     db	"create table VirtualHostsSupport (root char(33) NOT NULL, supporter char(33) NOT NULL,
unique(root,supporter))")
    (sql-exec-protected
     db	"create index VirtualHostsSupport_by_root ON VirtualHostsSupport(root)")
    (sql-close db)))

(define (virtual-host-add! oid name)
  (let ((db (sqlite3-open-restricted *config-db-file*)))
    (sql-exec-protected
     db	`(insert into VirtualHosts(OID : Name) values( ,(oid->string oid) : ,name)))
    (sql-close db)))

(define (virtual-host-delete! value)
  (let ((db (sqlite3-open-restricted *config-db-file*)))
    (sql-exec-protected
     db (if (symbol? value)
	    `(delete from VirtualHosts where OID = ,(oid->string value))
	    `(delete from VirtualHosts where name = ,value)))
    (sql-close db)))

(define (virtual-root-support-add! root-oid supporter)
  (let ((db (sqlite3-open-restricted *config-db-file*)))
    (sql-exec-protected
     db	`(insert into VirtualHostsSupport(root : supporter) values(,(oid->string root-oid) : ,supporter)))
    (sql-close db)))

(define (virtual-root-support-remove! root-oid supporter)
  (let ((db (sqlite3-open-restricted *config-db-file*)))
    (sql-exec-protected
     db	`(delete from VirtualHostsSupport where root = ,(oid->string root-oid) and supporter = ,supporter))
    (sql-close db)))

(define (virtual-host-for-each proc)
  (if *config-db-connection*
      (sql-with-tupels
       *config-db-connection*
       "select root, name, supporter from
VirtualHosts as vh join VirtualHostsSupport as vhs on vh.OID = vhs.root"
       '()
       (lambda (res rows cols)
	 (do ((i 0 (add1 i)))
	     ((eq? i rows))
	   (proc (sql-value res i 0) (sql-value res i 1) (sql-value res i 2)))))))

;; FIXME this KULDGE is a *really* bad HACK to make things working
;; right now.  Needs cleanup.

(define (vhost-lookup nameserver host)
  (let ((addr0 (host-lookup* host)))
    (if (equal? addr0 (local-id))
	(dns-get-address nameserver addr0)
	(and ((host-alive?) addr0)
	     (let ((colon (string-index addr0 #\:)))
	       (if colon (substring addr0 0 colon) addr0))))))

(define virtual-host-sync-to-dns!
  (let ((not-a-full-mutex #f))
    (lambda ()
      (if not-a-full-mutex
	  (raise "virtual-host-sync-to-dns! already in progress"))
      (let ((db *config-db-connection*))
	(if db
	    (guard
	     (ex (else
		  (set! not-a-full-mutex #f)
		  (log-condition 'virtual-host-sync! ex)))
	     (set! not-a-full-mutex #t)
	     (let ((nameserver (dns-find-nameserver)))
	       (sql-with-tupels
		db
		"select distinct name from VirtualHosts"
		'()
		(lambda (res rows cols)
		  (do ((i 0 (add1 i)))
		      ((eq? i rows))
		    (and-let*
		     ((name (sql-value res i 0)))
		     (logtopo "updating vhost ~a for ~a\n" i name)
		     (let ((old-addresses
			    (guard
			     (ex (else '()))
			     (dns-get-addresses nameserver name))))
		       (sql-with-tupels
			db
			"select distinct supporter from
VirtualHosts as vh join VirtualHostsSupport as vhs on vh.OID = vhs.root where name = ?1"
			(list name)
			(lambda (res rows cols)
			  (let ((current-addresses
				 (let loop ((i 0))
				   (if (eq? i rows) '()
				       (let ((x (vhost-lookup nameserver (sql-value res i 0))))
					 (if x (cons x (loop (add1 i))) (loop (add1 i))))))))
			    (if (or
				 (any
				  (lambda (c) (not (member c old-addresses)))
				  current-addresses)
				 (any
				  (lambda (c) (not (member c current-addresses)))
				  old-addresses))
				(do ((current-addresses current-addresses (cdr current-addresses))
				     (method! dns-set-address! dns-add-address!))
				    ((null? current-addresses))
				  (let ((address (car current-addresses)))
				    (guard
				     (ex (else (log-condition (format "in insert of ~a as ~a" name address) ex)))
				     (logtopo "vhost: ~a ~a as ~a\n"
					      (if (eq? method! dns-set-address!) 'set 'add)
					      name address)
				     (method! nameserver name address))))))))))))))
	     (set! not-a-full-mutex #f)))))))

(define (init-virtual-hosts)
  (set! *vhost-cache*
	(make-cache "*vhost-cache*"
		    string=?
		    #f ;; state
		    ;; miss:
		    (lambda (cs k) (cons k #f))
		    ;; hit
		    #f ;; (lambda (c es) #t)
		    ;; fulfil
		    #f
		    ;; valid?
		    (lambda (es) #t)
		    ;; delete
		    #f ;; (lambda (cs es) #f)
		    )))

(define (reload-virtual-hosts! eval . base)
  (init-virtual-hosts)
  (if *config-db-connection* (sqlite3-close *config-db-connection*))
  (set! *config-db-connection* #f)
  (if (pair? base)
      (let ((fn (make-pathname base "config" "db")))
	(set! *config-db-file* (and (file-exists?  fn) fn))))
  (if *config-db-file*
      (begin
	(set! *config-db-connection* (sqlite3-open-restricted-ro *config-db-file*))
	(guard
	 (ex (else (log-condition 'load-extensions ex)))
	 (let* ((v (guard
		    (ex (else #f))
		    (sql-exec-protected
		     *config-db-connection*
		     '(select * from Extensions))))
		(n (if v (sql-ref v #f #f) 0)))
	   (do ((i 0 (add1 i)))
	       ((eqv? i n) #t)
	     (call-with-input-string
	      (sql-ref v i 0)
	      (lambda (port)
		(let loop ((expr (sugar-read port))
			   (last #f))
		  (if (eof-object? expr)
		      last
		      (loop (sugar-read port)
			    (guard
			     (ex (else (logerr "Load error in ~a in ~a\n"
					       (condition->string ex) expr)))
			     (eval expr)))))))))))))

(define (virtual-host-lookup host)
  (and host
       *config-db-connection*
       (cache-ref
	*vhost-cache* host
	(lambda ()
	  (and-let* ((v (guard
			 (ex (else #f))
			 (sql-exec-protected
			  *config-db-connection*
			  `(select OID from VirtualHosts where Name = ,host))))
		     (n (sql-ref v #f #f))
		     ((> n 0)))
		    (string->oid (sql-ref v 0 0)))))))

($virtual-host virtual-host-lookup)

(define *access-log-connection* #f)

(define (log-access/sqlite
          host client user date method location protocol state size)
  (sqlite3-exec
   *access-log-connection*
   (sql-write
    `(insert
      into access values
      (,host : ,client : ,(if user (literal user) 'null)
	     : ,(date->julian-day date)
	     : ,method : ,(uri-path location)
	     : ,(if (uri-query location) (uri-query location) 'null)
	     : ,protocol : ,state : ,size)))))

(define (log-remote/sqlite
          host client user date method location protocol state size)
  (sqlite3-exec
   *access-log-connection*
   (sql-write
    `(insert
      into remote values
      (,host : ,client : ,(if user (literal user) 'null)
	     : ,(date->julian-day date)
	     : ,method : ,(uri-path location)
	     : ,(if (uri-query location) (uri-query location) 'null)
	     : ,protocol : ,state : ,size)))))

(define (init-sqlite3-access-log dir nm)
  (let ((fn (make-pathname
	     dir
	     (string-append nm "-" (srfi19:date->string (current-date) "~Y~m~d"))
	     "sqlite"))
	(sql (lambda (_) (sqlite3-exec *access-log-connection* _))))
    (set! *access-log-connection* (sqlite3-open-restricted fn))
    (sql "create table if not exists access(host text, client text, user text,
date real, method text, path text, query text, protocol text, state integer, size integer)")
    (sql "create table if not exists remote(host text, user text,
date real, method text, path text, query text, protocol text, state integer, size integer)")
    (log-access log-access/sqlite)))
