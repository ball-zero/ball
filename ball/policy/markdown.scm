;; (C) 2011 Jörg F. Wittenberger, GPL

(cond-expand
 (single-binary
  (define libupskirt-lace #f))
 (else
  (define libupskirt-lace (list (find-in-path "lace") "-x"))))

(define passive-html
  (let ((white-list
	 '(href target rel rev
	   src alt
	   class title id name
	   ;; style ; could be used to suppress display of important information
	   value type
	   method action enctype	; form
	   size rows cols
	   readonly
	   lang dir
	   align valign
	   height width longdesc usemap ismap
	   )))
    (lambda (att)
      (memq (xml-attribute-name att) white-list))))

(define-values
  (xml-parse-from-string
   xml-parse-from-port)
  ;; The rules fix a design issue with markdown: it wraps (by
  ;; definition) an additional 'code' element around the content of a
  ;; 'pre'.  However that's very impractical, since the 'pre'
  ;; preserves whitespace while xml-c18n formatting adds it.
  (let ((pre-handler
	 (cons (lambda (node root ns-binding)
		 (and (xml-element? node) (eq? (gi node) 'pre)))
	       (lambda-transformer
		"markdown:postprocess:#cleanup.strange.markup"
		(make-xml-element
		 (gi (sosofo)) (ns (sosofo)) (attributes (sosofo))
		 (transform sosofos: (children (children (sosofo)))))))))
    (let ((rules
	   (list
	    pre-handler
	    (cons sxp:element?
		  (lambda-transformer
		   "markdown:postprocess:#"
		   (let ((nc (transform sosofos: (children (sosofo)))))
		     ;; conserve space: return old sofofo when nothing changed
		     (if (eq? nc (children (sosofo))) (sosofo)
			 (make-xml-element (gi (sosofo)) (ns (sosofo)) (attributes (sosofo)) nc)))))))
	  (sanitize
	   (list
	    pre-handler
	    (cons sxp:element?
		  (lambda-transformer
		   "markdown:postprocess:#"
		   (if (memq (gi (sosofo)) '(script style))
		       (empty-node-list)
		       (let* ((nc (transform sosofos: (children (sosofo))))
			      (na (filter passive-html (attributes (sosofo)))))
			 ;; conserve space: return old sofofo when nothing changed
			 (if (and (eq? nc (children (sosofo)))
				  (equal? na (attributes (sosofo))))
			     (sosofo)
			     (make-xml-element (gi (sosofo)) (ns (sosofo)) na nc)))))))))
      (define (parse-from-string input-string passive)
        (if (and (string? input-string) (not (string-null? input-string)))
            (apply
             (xml-walk
              #f #f 'no-root '() '() '() '()
              (xml-parse (string-append "<div>\n" input-string "</div>\n")))
             (if passive sanitize rules))
            (empty-node-list)))
      (values
       parse-from-string
       (lambda (passive)
         (lambda (from)
           ;; FIXME: this hurts in the eyes.
           (parse-from-string (read-bytes #f from) passive)))))))

(define (mdp-parse-using-lace str passive)
  (if (string-null? str)
      (empty-node-list)
      (cond-expand
       (single-binary
        (xml-parse-from-string (libupskirt-parse-markdown str) passive))
       (else
        (run-child-process
         (lambda (to) (display str to))
         (xml-parse-from-port passive)
         libupskirt-lace
         #f)))))

(define (maybe-wrap-diff nl)
  (let ((nel (node-list-reduce
	      nl
	      (lambda (nel n) (if (xml-element? n) (add1 nel) nel))
	      0)))
    (cond
     ((eqv? nel 1) (document-element nl))
     ((> nel 1) (make-xml-element 'div #f '() nl))
     (else nl))))

(define (markdown-parse str)
  (maybe-wrap-diff (mdp-parse-using-lace str #f)))

(dsssl-export! (lambda (obj) (markdown-parse obj)) 'markdown-parse)

;; markdown-parse+sanitize SHALL assure no active ever elements in the output.
(define (markdown-parse+sanitize str)
  (maybe-wrap-diff (mdp-parse-using-lace str #t)))

(define *markdown-formats* #f)

(cond-expand
 (chicken
  (define-type :markdown-tty: (* * -> fixnum))
  (define-type :markdown-context: (struct <markdown-context>))
  (define-type :markdown-handler: (procedure :xml-nodelist: :markdown-context: -> :markdown-context:))
  )
 (else (begin)))

(: define-markdown-rule (symbol :markdown-handler: -> *))
(define (define-markdown-rule key handler)
  (hash-table-set! *markdown-formats* key handler))

(: markdown-make-inline-format (string string --> :markdown-handler:))
(define (markdown-make-inline-format pre post)
  (lambda (display nl ctx)
    (display #\space pre)
    (let ((ctx (markdown-format-all display (children nl) ctx)))
      (display #\return post)
      ctx)))

(define (markdown-make-inline-xml-format nm)
  (let ((post (format "</~a>" nm)))
    (lambda (disp nl ctx)
      (disp #\space (call-with-output-string
		     (lambda (port)
		       (display #\< port)
		       (display nm port)
		       (do ((atts (xml-element-attributes nl) (cdr atts)))
			   ((null? atts) (display #\> port))
			 (format port " ~a=\"~a\""
				 (xml-attribute-name (car atts))
				 (xml-attribute-value (car atts)))))))
      (let ((ctx (markdown-format-all disp (children nl) ctx)))
	(disp #\return post)
	ctx))))

(define (markdown-make-block-format pre post)
  (lambda (display nl ctx)
    (display #\tab pre)
    (let ((ctx (markdown-format-all display (children nl) ctx)))
      (display #\newline post)
      ctx)))

(define (define-markdown-inline-rule key pre post)
  (define-markdown-rule key (markdown-make-inline-format pre post)))

(define (define-markdown-inline-xml-rule key)
  (define-markdown-rule key (markdown-make-inline-xml-format key)))

(define (define-markdown-block-rule key pre post)
  (define-markdown-rule key (markdown-make-block-format pre post)))

(define *markdown-inline-list*
  '(span
    script scheme			    ; funny enough
    del ins				    ; could span more than inline
    sub sup q bdo			    ; %special
    dfn code samp kbd var cite abbr acronym ; %phrase
    u tt i b big small			    ; %fontstyle
    ))

(define *markdown-used*
  (delay*
   (let ()
     (define (foreach-li f init nl)
       (let loop ((nl (children nl)) (init init))
	 (if (node-list-empty? nl) init
	     (loop (node-list-rest nl)
		   (f (node-list-first nl) init)))))
     (define (wrt-code wrt nl ctx)
       ;; FIXME: How would markdown quote multiple backticks?
       (wrt #\space "`` ") (wrt (data nl) #t) (wrt #\return " ``")
       ctx)
     (define wrt-anchor
       (let ()
	 (define (wrt-link-ref! wrt ctx text label)
	   (wrt #\space "[") (markdown-wrt wrt text ctx) (wrt #\return "]")
	   (wrt #\space "[") (wrt #t (literal label)) (wrt #\return "]"))
	 (lambda (wrt nl ctx)
	   (let ((href (attribute-string 'href nl)))
	     (if href
		 (let ((seen (mdp-link-ref ctx href)))
		   (if (mdp-ref-null? seen)
		       ;; Bad interface to lnks...
		       (let ((no (next-mdpc-nlnk! ctx))
			     (title (attribute-string 'title nl)))
			 (wrt-link-ref! wrt ctx (children nl) no)
			 (mdp-link-bind! ctx href (make-mdp-ctx-lnk no href title)))
		       (let* ((ot (mdp-ctx-lnk-title seen))
			      (title (or ot
					 ;; try to bind title
					 (and-let*
					  ((title (attribute-string 'title nl))
					   ((not (string-null? title))))
					  (set-mdp-ctx-lnk-title! seen title)
					  title))))
			 (wrt-link-ref! wrt ctx (children nl) (mdp-ctx-lnk-label seen))))))
	     ctx))))
     (set! *markdown-formats* (make-symbol-table))
     (for-each define-markdown-inline-xml-rule *markdown-inline-list*)
     (define-markdown-inline-rule 'em "*" "*")
     (define-markdown-inline-rule 'strong "**" "**")
     (define-markdown-rule 'code wrt-code)
     (define-markdown-rule 'tt wrt-code)
     (define-markdown-rule 'a wrt-anchor)
     (define-markdown-rule 'br
       (lambda (wrt nl ctx) (wrt #\return #\newline) ctx))
     (define-markdown-rule 'pre
       (lambda (wrt nl ctx)
	 (wrt #\tab "   ")
	 (let ((pre (wrt #\C #t)))
	   (markdown-wrt wrt (children nl) ctx)
	   (wrt #\C pre))
	 (wrt #\newline #\return)
	 (wrt #\newline #\newline)
	 ctx))
     (define-markdown-rule 'ul
       (lambda (wrt nl ctx)
	 (foreach-li
	  (lambda (nl init)
	    (wrt #\newline #f)
	    (wrt #t "*")
	    (wrt #\tab "   ")
	    (markdown-wrt wrt (children nl) ctx)
	    (wrt #\newline #\return)
	    init)
	  #f nl)
	 (wrt #\newline #\newline)
	 ctx))
     (define-markdown-rule 'ol
       (lambda (wrt nl ctx)
	 (foreach-li
	  (lambda (nl init)
	    (wrt #\newline #f)
	    (wrt #t (format "~a." init))
	    (wrt #\tab "   ")
	    (markdown-wrt wrt (children nl) ctx)
	    (wrt #\newline #\return)
	    (+ init 1))
	  1 nl)
	 (wrt #\newline #\newline)
	 ctx))
     (define-markdown-block-rule 'p #f #\newline)
     (define-markdown-block-rule 'div #f #f)
     (define-markdown-block-rule 'html #f #f)
     (define-markdown-block-rule 'head #f #f)
     (define-markdown-block-rule 'body #f #f)
     (define-markdown-block-rule 'blockquote ">" #\tab)
     (define-markdown-rule 'hr
       (lambda (wrt nl ctx) (wrt #\return "* * *") ctx))
     (define-markdown-block-rule 'h1 "#" #\tab)
     (define-markdown-block-rule 'h2 "##" #\tab)
     (define-markdown-block-rule 'h3 "###" #\return)
     (define-markdown-block-rule 'h4 "####" #\return)
     (define-markdown-block-rule 'h5 "#####" #\return)
     (define-markdown-block-rule 'h6 "######" #\return)
     (define-markdown-block-rule 'dl #f #f)
     (define-markdown-block-rule 'dt "   " #\return)
     (define-markdown-block-rule 'dd "    " #\return)
     ;; End of markdown initialisation.
     ) '*markdown-used*))


(define (string-for-each/s+e proc s start end)
  (do ((i start (add1 i)))
      ((fx>= i end))
    (proc (string-ref s i))))

(: make-mdp-display (output-port -> :markdown-tty:))
(define (make-mdp-display port)
  (force *markdown-used*)
  (let ((prefix (xthe (list-of string) '()))
	(column 0)
	(inline (xthe boolean #t))
	(inline-defered (xthe (list-of string) '()))
	(block #t)
	(max-columns 65)
	(pre #f)
	)
    (define (set-column! n)
      (set! column n)
      (if (> n 0) (set! block #f))
      (set! inline (eqv? n 0)))
    (define (defered-length)
      (if (null? inline-defered) 0
	  (let loop ((l inline-defered) (n 1))
	    (if (null? l) n
		(loop (cdr l) (fx+ n (string-length (car l))))))))
    (define (open-inline! str)
      (if (and (eqv? column 0) (pair? prefix))
	  (let loop ((prefix prefix) (n 0))
	    (if (null? prefix)
		(set-column! n)
		(begin
		  (loop (cdr prefix) (fx+ n (string-length (car prefix))))
		  (display (car prefix) port)))))
      (let ((i0 (if inline 0 1)))
	(if (not inline) (display #\space port))
	(let loop ((prefix inline-defered) (n (fx+ column i0)))
	  (if (null? prefix)
	      (begin
		(set-column! n)
		(set! inline-defered '()))
	      (begin
		(loop (cdr prefix) (fx+ n (string-length (car prefix))))
		(display (car prefix) port)))))
      (if str
	  (set-column! (begin
			 (display str port)
			 (fx+ column (if (string? str) (string-length str) 1)))))
      (set! inline #t))
    (define (close-inline! str)
      (if str
	  (begin (display str port)
		 (set-column! (fx+ column (string-length str)))))
      (set! inline #t))
    (define (outtag! tag)
      (open-inline! tag) (set! inline #f) (set! block #t))
    (define (new-line!)
      (if (not (eqv? column 0)) (display #\newline port))
      (set-column! 0))
    (define (push-prefix! obj)
      (set! prefix (cons obj prefix)))
    (define (open-block! x)
      (if (not block)
	  (begin
	    (new-line!)
	    (if (not x) (display #\newline port))))
      (if x (push-prefix! x))
      (set! block #t))
    (define (close-block! x)
      (new-line!)
      (set! block #t)
      (cond
       ((eqv? x #\return)
	(set! prefix (cdr prefix)))
       ((eqv? x #\newline)
	(display #\newline port))
       ((eqv? x #\tab)
	(set! prefix (cdr prefix))
	(display #\newline port))))
    (define (isblank? c)
      (case c ((#\space #\newline #\return) #t) (else #f)))
    (define (outchar c) (display c port))
    (define (outln!/bk str)
      (define (atend? i) (fx>= i (string-length str)))
      (define (peek i) (string-ref str i))
      (define (skip-blank i j cont)
	(cond
	 ((atend? j) (cont (if (fx>= j i) 'blank 'end) i j))
	 ((isblank? (peek j)) (skip-blank i (add1 j) cont))
	 (else (cont 'blank i j))))
      (define (scan-data i j cont)
	(cond
	 ((atend? j) (cont (if (fx>= j i) 'data 'end) i j))
	 ((isblank? (peek j)) (cont 'data i j))
	 (else (scan-data i (add1 j) cont))))
      (define (scan-token i j cont)
	(cond
	 ((atend? j) (cont 'end i j))
	 ((isblank? (peek j)) (skip-blank i j cont))
	 (else (scan-data i j cont))))
      (define (loop token i j)
	(case token
	  ((blank)
	   (set-column! column)
	   (scan-token j j loop))
	  ((data)
	   (if (fx>= (+ column (fx- j i) (defered-length))
		     max-columns)
	       (new-line!))
	   (open-inline! #f)
	   (string-for-each/s+e outchar str i j)
	   (set! column (+ column (fx- j i)))
	   (scan-token j j loop))
	  ((end) #f)))
      (scan-token 0 0 loop))
    (define (no-break str i n)
      (open-inline! (substring/shared str i n))
      (new-line!)
      (add1 n))
    (define (outln!/nb str)
      ;; TODO: better parse from a string-port.
      (let loop ((i 0))
	(if (fx>= i (string-length str))
	    column
	    (let ((n (string-index str #\newline i)))
	      (cond
	       ((not n) (loop (no-break str i (string-length str))))
	       (else (loop (no-break str i n))))))))
    (define (outstr! str) (if pre (outln!/nb str) (outln!/bk str)))
    (define (outxml! nl)
      ;; In block mode just include verbatim.
      ;; Q: should we include namspace declarations etc.?
      (display #\newline port)
      (outln!/nb
       (call-with-output-string
	(lambda (port) (xml-format-fragment-at-output-port nl port))))
      (set-column! 0)
      (set! block #f)
      )
    (lambda (obj rest)
      (cond
       ((string? obj)
	(if (equal? obj "") (open-inline! #f) (outstr! obj))
	column)
       ((char? obj)
	(case obj
	  ((#\space) (set! inline-defered (cons rest inline-defered)))
	  ((#\return)
	   (cond
	    ((eqv? rest #\newline) (display "  " port) (close-block! #t))
	    ((null? inline-defered) (close-inline! rest))))
	  ((#\tab) (open-block! rest))
	  ((#\newline) (close-block! rest))
	  ((#\C) (let ((v (and pre))) (set! pre (and rest no-break)) v))
	  (else (raise (format "mdp-display unknown key ~a" obj)))))
       ((or (xml-element? obj) (xml-comment? obj) (xml-pi? obj))
	(open-block! #f) (outxml! obj))
       ((symbol? obj) (outln!/nb (symbol->string obj)) column)
       ((eq? obj #t) (outtag! rest) column)
       (else (raise (format "mdp-display unknown obj ~a" obj)))))))

(define (markdown-wrt wrt nl ctx)
  (cond
   ((string? nl) (wrt nl #f) ctx)
   ((xml-element? nl)
    (let ((handler (and (gi nl) (hash-table-ref/default *markdown-formats* (gi nl) #f))))
      (if handler (handler wrt nl ctx) (begin (wrt nl #f) ctx))))
   ((pair? nl) (markdown-format-all wrt nl ctx))
   ((null? nl) ctx)
   (else (wrt (literal nl) #f) ctx)))

(define (markdown-format-all wrt nl ctx)
  (let loop ((nl nl) (ctx ctx))
    (if (node-list-empty? nl) ctx
	(loop
	 (node-list-rest nl)
	 (markdown-wrt wrt (node-list-first nl) ctx)))))

;; This code has a few indirections/restrictions too much (to avoid
;; importing too lolevel stuff).  Consider it experimental.
(define-record-type <markdown-context>
  (%make-mdp-context nlnk lnks)
  markdown-context?
  (nlnk mdpc-nlnk set-mdpc-nlnk!)
  (lnks mdpc-lnks set-mdpc-lnks!))
(define (make-mdp-context)
  (%make-mdp-context 0 (make-json-object)))
(define (mdp-link-bind! mdp k v)
  (json-object-bind! (mdpc-lnks mdp) k v))
(define (mdp-link-ref mdp k)
  (json-object-ref (mdpc-lnks mdp) k))
(define mdp-ref-null? null?)

(: mdp-ctx-lnk-label ((struct <markdown-context-link>) --> fixnum))
(define-record-type <markdown-context-link>
  (make-mdp-ctx-lnk label href title)
  markdown-ctx-lnk?
  (label mdp-ctx-lnk-label)
  (href mdp-ctx-lnk-href)
  (title mdp-ctx-lnk-title set-mdp-ctx-lnk-title!))

(define (next-mdpc-nlnk! mdp)
  (let ((n (add1 (mdpc-nlnk mdp))))
    (set-mdpc-nlnk! mdp n) n))

(define (mdp-links-for-each f mdp)
  (srfi:vector-for-each
   f
   (json-object-fold
    (lambda (k v all) (vector-set! all (sub1 (mdp-ctx-lnk-label v)) v) all)
    (make-vector (mdpc-nlnk mdp))
    (mdpc-lnks mdp))))

(: markdown-format-links (:markdown-tty: :markdown-context: -> :markdown-context:))
(define (markdown-format-links wrt ctx)
  (mdp-links-for-each
   (lambda (i l)
     (wrt #\newline #f)
     (wrt #t (format "[~a]:" (mdp-ctx-lnk-label l)))
     (wrt #\tab "     ")
     (wrt #t (mdp-ctx-lnk-href l))
     (and-let* ((title (mdp-ctx-lnk-title l)))
	       (wrt #\space "(") (wrt title #f) (wrt #\return ")"))
     (wrt #\newline #\return))
   ctx)
  ctx)

(define (markdown-format nl port)
  (let ((wrt (make-mdp-display port))
	(ctx (make-mdp-context)))
    (let ((ctx (markdown-format-all wrt nl ctx)))
      (wrt #\newline #\newline)
      (markdown-format-links wrt ctx)
      (wrt #\newline #f))))

(define (markdown->string text)
  (call-with-output-string (lambda (port) (markdown-format text port))))

(define (text/plain->markdown str)
  (let ((src (data (body->string str))))
    (if (string-null? src) src
	(string-append "    " (normalise-data src #f #f "\n    ")))))

(dsssl-export! (lambda (str) (markdown->string str)) 'markdown->string)

(register-mime-converter! text/xml "text/x-markdown" markdown-parse)
(register-mime-converter! text/xml "text/x-markdown; option=passive" markdown-parse+sanitize)
(register-mime-converter! "text/x-markdown" text/xml markdown->string)
(register-mime-converter! "text/x-markdown" text/plain text/plain->markdown)
