;; (C) 2018 JFW

;; TBD: Embed a hash (or asymetric signature) of the initial public
;; key in the public place and check it's presence (and the integrity
;; of the resulting publicOID) to confirm the TOFU phase.  This will
;; be required to prevent DoS attacks where the attacker sends valid
;; certificates for hosts before those connect.
;;
;; Doing so will put us back where we started: dependency on a long
;; lasting secret key.

(import (only sslsocket ssl-verify-callback-set!))

(define $tc-tofu (make-shared-parameter #t))

(define (tc-tofu-ssl-init! file)
  ;; This is overly strict.  We may want to allow CAs to sign for
  ;; multiple users and hosts.
  (let ((db (sqlite3-open-restricted file)))
    (define (get k)
      (let* ((e (sqlite3-exec db "select host, user, hvalid, uvalid from known where pk=?1" k))
	     (n (sql-ref e #f #f)))
	(cond
	 ((= n 0) (if ($tc-tofu) #f 'fixed)) ;; allow in TOFU mode
	 ((> n 1) 'invalid)
	 ((= (sql-ref e 0 2) 0) 'revoked)
	 (else
	  (let ((host (sql-ref e 0 0))
		(user (and (> (sql-ref e 0 3) 0) (sql-ref e 0 1))))
	    (make-mesh-cert
	     (and host (string->oid host))
	     (and user (string->oid user))))))))
    (define (set k v)
      (and
       (mesh-certificate? v)
       (begin
	 (sqlite3-exec
	  db "insert or ignore into known(pk, host, user, uvalid) values(?1,?2,?3,?4)" k
	  (and-let* ((s (mesh-cert-l v))) (oid->string s))
	  (and-let* ((s (mesh-cert-o v))) (oid->string s))
	  ($tc-tofu))
	 #t)
       #;(get k) v))
    (sqlite3-exec db "
create table if not exists known(
 id integer primary key autoincrement,
 pk text unique not null,
 host text,
 hvalid integer default 1,
 user text,
 uvalid integer default 0,
 unique(host, hvalid),
 unique(user, uvalid)
)
")
    (ssl-verify-callback-set! get set)))
