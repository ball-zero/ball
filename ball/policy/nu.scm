;; (C) 2000, 2001, 2003 J�rg F. Wittenberger see http://www.askemos.org

;; Handle small text bits between many users.

;;** About

;; This text has a three fold nature.

;; First it's sort of higly experimental.  I'm trying experimental
;; features here.

;; Second it's somehow the inner kernel of what's *all* is about: it
;; describes handling of little bits of information (atomic ideas,
;; "words").  For this social reason (personal interest) it's the most
;; stable part in the long run - it *will* still work when the
;; underlying mechanism is gone.

;; Third it's an extention, not part of the kernel, because it
;; implements policy, not mechanism.

;; The third point implies somehow, that those pieces, which are found
;; to be mechanism and half stable are moved to appropriate places.

;;** General Purpose

;; a) Demonstrate how to extend the kernel. (Otherwise I had written
;; that in XSLT)

;; b) Handle notes for a group of colaborating persons.

;; c) (added Okt 2001) should handle the DITA architecture.  See:
;;  http://www-106.ibm.com/developerworks/xml/library/x-dita1/?dwzone=xml

;;** Definitions

;; We need a word for the basic unit of an idea as it is handled by
;; our brain.  This basic unit is certainly not the technical "bit"
;; nor the single word of a language.  It's also not the pulse send
;; from an axon of a nerve cell.  Also the word idea itself doesn't
;; fit, because we have simple and compound ideas and those which are
;; clear while others are blured.

;; Such an "atomic idea" will simply be called "nu".  Blured ideas are
;; denoted by "nunu".  These words have to my knowledge no defined
;; meaning, but if you watch people talking in my home town, they'll
;; use them all the time (while german who live in the next town don't
;; ever use them) .  They are used simillar to the english words
;; "well", "yeah" and "fucking" in spoken language.

;;** Detailed Specification, Requirements

;; 0 It's a pity: todays web tools are far too brain dead asuming that
;;   everything in the world fits into file hierarchies.  A notepad as
;;   defined here could be maped at such a restricted point of view.
;;   This is not done.  Instead I try my best to find a practical
;;   route between usability and clean design.
;;
;;   The kind reader is asked -- as always -- to help out with better
;;   ideas.
;;
;; 1 For each (1st) nu of the message destination respectively the
;;   name given in the request, there's a page.
;; 2 There's a button at the page, which allows to change the text.
;; 3 While one person works on a draft, others can't.  They still
;;   see the last released text version.
;; 4 The first one who writes it owns the page and might
;;   + add other authors, who will have *all* the same rights as the
;;     original author
;;   + change the draft
;;   + release the draft as new text.
;; 5 compound words represent other ideas, which should be explained
;;   somewhere else.  They become cross references.
;;   A compound idea is denoted (by the user) with a word, which
;;   contains at least two upper case letters.  Most abbreviations are
;;   used that way - that's why.  You find the same mechanism in the
;;   wiki web.
;; 6 When viewed, cross references pointing at the nu from other nu's
;;   SHOULD be displayed.
;; 7 Boards are "stackable", i.e., a derived notepad inherits
;;   definitions (including templates) from a stack of more general
;;   notes.
;; 8 in case of inheriance it's not (yet) defined where edits shall
;;   go.

;;** Data Definitions

;; See file "nu-syntax.scm".

(define (literal-member who childs)
  (cond
   ((node-list-empty? childs) #f)
   ((string=? who (data (node-list-first childs))) #t)
   (else (literal-member who (node-list-rest childs)))))

(define (is-quote? node root ns-binding)
  (and (xml-element? node)
       (eq? (ns node) namespace-nu) (memq (gi node) '(abbr q))))

(define (holds-link? node root ns-binding)
  (and (xml-element? node)
       (eq? (ns node) namespace-nu)
       (memq (gi node) '(a include))))

(define semi-apply-templates-void
  (cons sxp:element?
        (lambda-transformer
	 "#nunu:semi-apply-templates-void"
	 (transform sosofos: (children (sosofo))))))

;; A nu can be locked by the creator of the msg if:

(define (nunu-lockable? node dc-creator)
  (let ((locks (if node
		  ((sxpath '(lock authors id *text*)) node)
		  (empty-node-list))))
    (or (node-list-empty? locks)
	(ormap
	 (lambda (a)
	   (let ((a (string->oid a)))
	     (or (not a) (eq? a dc-creator))))
	 locks))))

;; XForm-field is a redifinition of form field, cause it does the
;; same, except for other type of request.  Would be great if rscheme
;; could import selectivly and renaming like Sather.  It definately
;; ougth to!

(define (Xform-field key node)
  (and (xml-element? node)
       (eq? (gi node) 'form) ;; (eq? (ns node) namespace-forms)
       (select-elements (children node) key)))

(define-syntax nu-name			; requirement 1
  (syntax-rules ()
    ((_ msg)
     (let ((dst (msg 'destination)))
       (or
	(and (pair? dst) (pair? (cdr dst))
	     (strip-html-suffix (cadr dst)))
	(and (pair? dst)
	     (strip-html-suffix (car dst)))
	(let ((dst (and (equal? (msg 'content-type) text/xml)
			(Xform-field 'name (document-element
					    (msg 'body/parsed-xml))))))
	  (if (and dst (not (node-list-empty? dst)))
	      (strip-html-suffix (data dst))
	      "")))))))

(cond-expand
(never
;; State data is (pretty experimental).  All values could be computed
;; from the rest of the document(s).  It's used to fulfill requirement
;; number 6.

(define (make-state-data)
  (vector
   '()                                  ; set of nunu-links in text
   #f ;;(make-string-table)             ; set of nunu-links in draft
   (make-string-table)                  ; set of back links
   '()                                  ; cache of value-sequence of back links
   ))

(define (create-and-initialize-states me request)
  (let ((all-states (me 'nunu-state)))
    (if all-states
        all-states
        (let ((all-states (make-string-table)))
	  (initialize-states! all-states me)
          ((me 'get 'writer) 'nunu-state all-states)
          ;;           (let ((subscriptions (me subscribers-str)))
          ;;             (if subscriptions
          ;;                 ((me 'get 'writer) 'subscribe
          ;;                  (make-subscriber-list (nunu-text subscriptions)))))
          all-states))))

;; TODO this initialization was designed to run just once in the
;; unusual event of a broken pstore module; hence it's outdated and
;; horribly slow.

(define (initialize-states! all-states me)
  (nu-fold-all
   me
   (lambda (li all)
     (define name (attribute-string 'resource li))
     (named-state all-states name)
     (let* ((draft-links (make-draft-links me name))
            (register (make-register draft-links))
	    (nu (nu-get-nu me me (attribute-string 'href li))))
       (if nu
	   ((xml-walk me 'dummy 'no-root '() '() '() '() (message-body nu))
	    (cons holds-link?
		  (lambda-transformer
		   "nunu:nunu:#initialize-states"
		   (and-let* ((t (@? 'href))) (register t))))
	      semi-apply-templates-void))
       (set-draft-links! all-states name draft-links))
     (commit-state! me all-states name)
     (values #t all))
   'dummy))

(define (named-state all-states name)
  (hash-table-ref
   all-states name
   (lambda ()
     (let ((v (make-state-data)))
       (hash-table-set! all-states name v)
       v))))

(define (make-draft-links me name) (make-string-table))

(define (make-register draft-links)
  (lambda (target) (hash-table-set! draft-links target #t)))

(define (set-draft-links! all-states name register)
  (vector-set! (named-state all-states name) 1 register))

(define (commit-state! me all-states name)
  (let* ((state (named-state all-states name))
         (draft-links (if (vector-ref state 1)
                          (key-sequence (vector-ref state 1))
                          '())))
    (for-each
     (lambda (target)
       (if (not (member target draft-links))
	   (let (;; BEWARE: we should actually not create the
		 ;; target here.  This is a KLUDGE required to
		 ;; allow reloading from file system.
		 (ts (named-state all-states target)))
	     (if ts (let ((back-links (vector-ref ts 2)))
		      (hash-table-delete! back-links name)
		      (vector-set! ts 3 #f))))))
     (vector-ref state 0))
    (if (pair? draft-links)
	(for-each
	 (lambda (target)
	   (and
	    (not (string=? name target))
	    (not (member target (vector-ref state 0)))
	    (let ((ts (named-state all-states target)))
	      (let ((back-links (vector-ref ts 2)))
		(hash-table-set! back-links name #t)
		(vector-set! ts 3 #f)
		target))))
	 draft-links))
    (vector-set! state 0 draft-links)))

;; TODO What happens if an empty draft is released?  The logic below
;; will (starting at 2000-01-14) drop the name alltogether.  But if
;; there are links pointing here, this could be the wrong thing.
;; Should we keep it in that case?  Another question concerns when to
;; drop the state data as well.

;; Really, this is exported.  You see my policy to use names which fit
;; to 80% instead of 100% fits until I'm sure.

(define (is-not name) (lambda (target) (not (string=? name target))))

(define (nunubacklinks me name)
  (let* ((state (hash-table-ref/default (all-states me) name #f))
         (r (if state (vector-ref state 3) '())))
    (if r r (let ((v (filter (is-not name)
                             (key-sequence (vector-ref state 2)))))
              (vector-set! state 3 v)
              v))))

(define (nunulinks me name)
  (let ((state (hash-table-ref/default (all-states me) name #f)))
    (if state (vector-ref state 0) '())))

) (else

(define (set-draft-links! x n l) #f)

(define (commit-state! m s n) #f)

(define (make-draft-links me name) (vector me name))

(define (make-register draft-links)
  ((vector-ref draft-links 0)
   (format "delete from crossref where s = '~a'" (sql-quote (vector-ref draft-links 1)))
   'sql)
  (lambda (t) ((vector-ref draft-links 0)
	       (format "insert into crossref(s,t) values('~a','~a')"
		       (sql-quote (vector-ref draft-links 1)) (sql-quote t))
	       'sql)))

(define (initialize-states! me)
  (nu-fold-all
   me
   (lambda (li all)
     (define name (attribute-string 'resource li))
     (let ((register (make-register (make-draft-links me name)))
	   (nu (nu-get-nu me me (attribute-string 'href li))))
       (if nu
	   ((xml-walk me 'dummy 'no-root '() '() '() '() (dsssl-message-body nu))
	      (cons holds-link?
		    (lambda-transformer
		     "nunu:nunu:#initialize-states"
		     (and-let* ((t (@? 'href))) (register t))))
	      semi-apply-templates-void)))
     (values #t all))
   'dummy))

(define (create-and-initialize-states me msg)
  (guard
   (ex (else (me "create table crossref (s text, t text, unique(s,t) on conflict ignore)" 'sql) 
	     (initialize-states! me)))
   (if (eqv? (vector-length (me '(select count(*) from crossref) 'sql-query)) 1) (raise #f) )))

(define (q->nl me query)
  (let ((r (me query 'sql-query)))
    (let loop ((i 0))
      (if (eqv? (sql-ref r #f #f) i) '()
	  (cons (sql-ref r i 0) (loop (add1 i)))))))

(define (nunubacklinks me name)
  (q->nl me `(select s from crossref where t = ,name)))

(define (nunulinks me name)
  (q->nl me `(select t from crossref where s = ,name)))

))

;; (define (all-states me)
;;   (let ((all-states (me 'nunu-state)))
;;     (or all-states
;;         (force
;;          (with-mind-modification
;;           (lambda (oid)
;;             (!order (guard
;;                      (ex (else (logerr " nu:load ~a\n" ex) #f))
;;                      (respond!
;;                       (find-frames-by-id oid)
;;                       create-and-initialize-states
;;                       respond-local #t (make-message)))
;;                     "nu:load"))
;;           (me 'get 'id))))))

;; The former implementation is preferable but seems to break with
;; rscheme.

(define (all-states me)
  (let ((all-states (me 'nunu-state)))
    (or all-states
        (let ((self (me 'get 'id)))
	  (force
	   (pass-a-ref
	    self
	    (guard (ex ((timeout-condition? ex) (raise ex))
		       (else (receive
			      (title msg msgtxt rest)
			      (condition->fields
			       (cond
				((uncaught-exception? ex)
				 (uncaught-exception-reason ex))
				(else ex)))
			      (logcond 'nu-load title msg rest))
			     #f))
		   (respond!
		    (find-frames-by-id self)
		    create-and-initialize-states
		    respond-local-no-timeout ;; better respond-local
		    #t (make-message)))
	    (oid->string self)))))))

;; That's all for state data.

(define (make-subscriber-list subscriptions)
  (node-list-reduce
   ((sxpath '(li)) subscriptions)
   (lambda (result node)
     (let* ((host (data ((sxpath '(host)) node)))
            (voter (and host (voter-auth host))))
       (if voter
           (cons (cons host (data ((sxpath '(oid)) node))) result)
           (begin
             (logerr "ignoring unknown subscriber ~a\n" host)
             result))))
   '()))

;; Deliver the user view of the nu.
;; There's a special nu called template, which contains the view code.
;; I lack a better way without that special name, sorry.  Please
;; suggest some solution.

(define (format-nu me msg name node req-templ)
  (let* ((template-name (or (and (not (or (null? req-templ)
                                          (equal? req-templ "")))
                                 req-templ)
                            template-str))
         (template0 (nunu-text (fetch me msg template-name))))
    (receive
     (owner template) (if template0
                          (values #f template0)
                          (nunu-inherited me msg template-name))
     (if template
         (guard
          (condition
           (else (receive
		  (title msg msgtxt rest)
		  (condition->fields
		   (cond
		    ((uncaught-exception? condition)
		     (uncaught-exception-reason condition))
		    (else condition)))
		  (let ((message (if (message-condition? condition)
				     (condition-message condition) msg)))
		    (sxml
		     `(html
		       (head (title ,(literal title)))
		       (body (@ (bgcolor "white"))
			     (h1 ,(literal title))
			     ,@(cond
				((string? message) `((p ,message)))
				((node-list? message)
				 (map (lambda (m) `(p ,m)) message))
				((pair? message)
				 (map (lambda (m) `(p ,(literal m))) message))
				(else `((p ,(literal message)))))
			     ;; FIXME this is just to work around things as they where
			     ;; don't rely on this embedding, it will be removed
			     ,@(cond
				((node-list? msgtxt) msgtxt)
				((xml-element? msgtxt) (list msgtxt))
				(else (sxml msgtxt))))))))))
          ((make-xslt-transformer me msg)
           (if node node (make-nu name))
           template))
         (nu-error (string-append "requested template \""
                               template-name "\" not found"))))))

(define (nu-fold-all me fold-function init)
  (or
   (and-let*
    ((objects (guard
	       (ex (else #f))
	       (me "objects" 'metainfo-adopt))))
    (node-list-fold-left
     ((sxpath '(Description links Bag li)) objects)
     (lambda (dir init)
       (values
	#t
	(and-let*
	 ((objects (guard
		    (ex (else #f))
		    (me (attribute-string 'href dir)
			'metainfo-adopt))))
	 (node-list-fold-left
	  ((sxpath '(Description links Bag li)) objects)
	  fold-function init))))
     init))
   init))

(define find-matching-attributes
  (list (make-xml-attribute 'id #f "")))

(define (descendants-some? predicate nl)
  (and (not (node-list-empty? nl))
       (or (and (xml-element? (node-list-first nl))
                (descendants-some? predicate (children (node-list-first nl))))
           (and (xml-literal? (node-list-first nl))
                (predicate (data (node-list-first nl))))
           (descendants-some? predicate (node-list-rest nl)))))

(define (find-matching me msg pcregex)
  (make-xml-element
   'nu #f find-matching-attributes
   (make-xml-element
    'text #f (nu-text-attributes text/xml)
    (node-list
     (make-xml-element 'h1 #f (empty-node-list) (literal pcregex))
     (make-xml-element
      'ul #f (empty-node-list)
      (or
       (nu-fold-all
	me
	(lambda (li init)
	  (values
	   #t
	   (or (and-let*
		((obj (nu-get-nu me msg (attribute-string 'href li))))
		(if (descendants-some? (pcre->proc pcregex)
				       (message-body obj))
		    (cons (make-xml-element
			   'li #f
			   (list (make-xml-attribute
				  'href #f (attribute-string 'href li)))
			   (attribute-string 'resource li))
			  init)
		    init))
	       init)))
	(empty-node-list))
       ((me 'fold-links)
	(lambda (name init)
	  (if (descendants-some? (pcre->proc pcregex) (fetch me msg name))
	      (cons (make-xml-element 'li #f (empty-node-list)
				      (make-xml-literal name))
		    init)
	      init))
	(empty-node-list))))))))

(define (nunu-action-get me msg)
  (all-states me)
  (cond
   ((is-metainfo-request? msg)
    (xml-document->message
     me msg
     (make-xml-element
      'RDF namespace-rdf signature-att-decl
      (make-xml-element
       'Description namespace-rdf (list (make-xml-attribute
					 'about #f
					 (oid->string (me 'get 'id))))
       (node-list
	(make-xml-element 'creator namespace-dc '()
			  (literal (oid->string (me 'dc-creator))))
	(make-xml-element 'date namespace-dc '()
			  (literal (rfc-822-timestring (me 'dc-date))))
	(make-xml-element 'action-document namespace-mind '()
			  (literal (oid->string (me 'mind-action-document))))
	(let ((version (me 'version)))
	  (make-xml-element
	   'version namespace-mind rdf-parsetype-literal-attribute-list
	   (node-list
	    (make-xml-element 'serial namespace-mind '() (literal (car version)))
	    (make-xml-element
	     'checksum namespace-mind '() (literal (cdr version))))))
	(me 'replicates)
	(make-xml-element 'content-type #f '()
			  (literal (me 'content-type)))
	(let ((body (me 'mind-body)))
	  (make-xml-element
	   'body namespace-mind '()
	   (if (a:blob? body)
	       (blob->xml body)
	       (let ((l (string-length body)))
		 (blob->xml (a:make-blob (string->symbol (sha256-digest body)) l l 1 #f))))))
	;; -------------------------------------------------------------------
	;; Below this line we carry optional stuff (sugar).
	;; -------------------------------------------------------------------
	(make-xml-element 'last-modified namespace-dav '()
			  (rfc-822-timestring (or (me 'last-modified)
						  (me 'dc-date))))
	)))))
   (((make-service-level (me 'protection) (or (msg 'capabilities) '()))
     (my-oid))
    ;;   (guard
    ;;    (condition
    ;;     (else (table-remove! mind-pool (me 'get 'id))
    ;;           (raise "error redo")))

    ;; Nautilus is nasty enough to tell us WebDAV would be content type
    ;; "text/html"!!!  Until it's fixed, we report and continue.
    (let* ((form (and (or (xml-parseable? (msg 'content-type))
			  (and (html-parseable? (msg 'content-type))
			       (begin
				 (logerr "assuming html=xml for default\n")
				 #t)))
		      (document-element (msg 'body/parsed-xml))))
           (action (data (Xform-field 'action form))))
      (cond
       (((is-propfind-request?) msg)
	(let* ((objects "objects")
	       (oid (me objects)))
	  (if oid
	      (let* ((template template-str)
		     (raw (car ((me 'get 'reader)
				to: (cons oid (msg 'destination))
				type: 'call
				'location (msg 'location)))))
		(if (let ((d (msg 'destination)))
		      (or (null? d) (string=? (car d) "changes")))
		    raw
		    (let ((stat (message-body raw)))
		      (set-slot!
		       raw 'body/parsed-xml
		       ;; Too bad, we need to fix the content length
		       ;; KLUDGE: this ought to be done using xslt to shorten the code.
		       (make-xml-element
			(gi stat) (ns stat) (xml-element-attributes stat)
			(node-list-map
			 (lambda (resp)
			   (make-xml-element
			    'response (ns resp) '()
			    (receive
			     (name terminate)
			     (string-split-last
			      (uri-parse
			       (data ((sxpath '(href)) resp)))
			      #\/)
			      (node-list-map
			       (lambda (rc)
				 (if (eq? (gi rc) 'propstat)
				     (make-xml-element
				      (gi rc) (ns rc) (xml-element-attributes rc)
				      (node-list-map
				       (lambda (psc)
					 (if (eq? (gi psc) 'prop)
					     (make-xml-element
					      (gi psc) (ns psc) (xml-element-attributes psc)
					      (node-list-map
					       (lambda (p)
						 (if (eq? (gi p) 'getcontentlength)
						     (make-xml-element
						      (gi p) (ns p) (xml-element-attributes p)
						      (literal
						       (string-length
							(let* ((result
								(format-nu me msg name (fetch me msg name) template))
							       (other (document-element result)))
							  (if (eq? (gi other) 'output)
							      (receive
							       (b t) (mime-format other) b)
							      (xml-format other))))))
						     p))
					       (children psc)))
					     psc))
				       (children rc)))
				     rc))
			       (children resp)))))
			 (children stat))))))
		raw)
	      ((dav-propfind-message) me msg collection: #t children: '()))))
       ((equal? action "grep")
        (xml-document->message
         me msg
         (format-nu me msg "Grep Results"
                    (find-matching me msg (data (Xform-field 'pcre form)))
                    (and form (data (Xform-field 'template form))))))
       ((or (and-let* ((d (msg 'destination))
		       ((pair? d))
		       ((string=? (car d) "changes")))
		      ASIS-str)
	    (equal? action "changes"))
	=>
        (lambda (t0)
	  (let ((changes (car ((me 'get 'reader)
			       to:
			       (list "objects" "changes")
			       type: 'call
			       'location (msg 'location))))
		(template (if (string? t0) t0
			      (and form (data (Xform-field 'template form))))))
	    (if (equal? template ASIS-str)
		(xml-document/content-type->message
		 me msg
		 (document-element (message-body changes))
		 (get-slot changes 'content-type)
		 (msg 'dc-date))
		(xml-document->message
		 me msg
		 (format-nu me msg "Recent Changes"
			    (make-xml-element
			     'nu #f find-matching-attributes
			     (make-xml-element
			      'text #f (nu-text-attributes text/xml)
			      (node-list
			       (make-xml-element 'h1 #f (empty-node-list)
						 "Recent Changes")
			       (document-element
				(message-body changes)))))
			    template))))))
       (else
        (let ((name (nu-name msg))
              (location (property 'location
                                  (let ((md (msg 'destination)))
				    (if (pair? md)
					(append! (reverse md) (msg 'location))
					(cons "" (msg 'location)))))))
          (if name
              (let* ((node (fetch me msg name))
                     (template (and form (data (Xform-field 'template form)))))
                (if (equal? template ASIS-str)
                    (xml-document->message me msg (or node undefined-reply) location)
                    (let* ((result (format-nu me msg name node template))
                           (other (document-element result)))
                      (if (eq? (gi other) 'output)
                          (output-element->message me msg other '())
                          (xml-document->message me msg result location)))))
              (xml-document->message me msg
                                     (me 'body/parsed-xml) location)))))))
   (else 
    (nu-error "I'm sorry to inform you don't have the right to read this."))))

;; The rest needs some clean up.  Implements the controler code.

;; It's a bit "brain dead" or actually too principle ridden.  It would
;; be to some extend easier (and at least faster executable) to code
;; this in "normal scheme" without the help of the tree walker.  At
;; the other hand this way it serves as an simple example how more
;; complex transformations would work.

;; Don't be afraid to skip code parts, which look weird.  You won't
;; miss important information.

(define %nunu-make-empty-lock-value
  (node-list-first (select-elements (children the-empty-nu) 'lock)))
(define-transformer nunu-make-empty-lock "#nunu:empty-lock" %nunu-make-empty-lock-value)

(define drop-pi (cons sxp:pi? xml-drop))
(define keep-rest (cons sxp:element? (lambda-transformer "#nunu:keep-rest" (sosofo))))

(define (is-nu-link? x root ns-binding)
  (and (xml-element? x)
       (eq? (gi x) 'a) (eq? (xml-element-ns x) namespace-nu)))
(define (is-anchor? x root ns-binding) (and (xml-element? x) (eq? (gi x) 'a)))
(define (is-lock? x root ns-binding) (and (xml-element? x) (eq? (gi x) 'lock)))
(define (is-draft? x root ns-binding) (and (xml-element? x) (eq? (gi x) 'draft)))
(define (is-text? x root ns-binding) (and (xml-element? x) (eq? (gi x) 'text)))
(define (is-authors? x root ns-binding) (and (xml-element? x) (eq? (gi x) 'authors)))
(define (is-date? x root ns-binding) (and (xml-element? x) (eq? (gi x) 'date)))
(define (is-sms? x root ns-binding) (and (xml-element? x) (eq? (gi x) 'comments)))
(define make-empty-lock (cons is-lock? nunu-make-empty-lock))
(define make-empty-draft
  (cons is-draft?
        (lambda-transformer
	 "nunu:nunu:#make empty draft"
         (node-list-first (select-elements (children the-empty-nu)
                                           'draft)))))
(define (make-date-element dc-date)
  (cons is-date?
        (lambda-transformer
	 "nunu:nunu:#make current date"
         (make-xml-element
          'date namespace-nu (empty-node-list)
          (node-list (make-xml-literal
                      (rfc-822-timestring dc-date)))))))

(define (make-authors data)             ; TODO improve data model here.
  (cons is-authors?
        (let ((nsa namespace-nu)
	      (nsid #f))	; namespace-mind
	  (lambda-transformer
	   "nunu:nunu:/make/#authors" 
	   (make-xml-element
	    'authors nsa (empty-node-list)
	    (let loop ((d data) (r '()))
	      (if (null? d)
		  r
		  (loop (cdr d)
			(if (equal? (car d) "")
			    r
			    (cons (make-xml-element
				   'id nsid (empty-node-list)
				   (car d))
				  r))))))))))
(define (make-lock dc-creator)
  (lambda-transformer
   "nunu:nunu:#/make/#dc:creator"
   (make-xml-element 'lock namespace-nu (empty-node-list)
                     (make-xml-literal (oid->string dc-creator)))))

(define (nunu-lock dc-creator target name form)
  (let ((text (node-list-first
	       (select-elements (children target) 'text))))
    (make-xml-element
     'nu namespace-nu
     (cons (make-xml-attribute 'id #f name)
	   nu-xmlns-attributes)
     ((xml-walk
       #f #f 'no-root '() '()
       #f form (children target))
      (cons is-lock? (make-lock dc-creator))
      (cons is-draft?
	    (lambda-transformer
	     "nunu:nunu:#/lock#make-draft"
	     (make-xml-element
	      'draft namespace-nu
	      (if (xml-element? text)
		  (attributes text)
		  nu-media-type-default-attribute-list)
	      (children text))))
      keep-rest))))

(define rrs-att-list
  (list (make-xml-attribute 'media-type #f "application/rss+xml")
	(make-xml-attribute 'method #f "xml")))

(define (nu-update-rss old name value)
  (let* ((old (children (children old)))
	 (link (select-elements old 'link)))
    (make-xml-element
     'output namespace-mind rrs-att-list
     (make-xml-element
      'rss #f '()
      (make-xml-element
       'channel #f '()
       (node-list
	(select-elements old 'title)
	link
	(select-elements old 'description)
	(sxml `(item
		(title ,name)
		(link ,(data link) "/" ,name)
		(pubDate . ,(children (select-elements (if value (children value) old) 'date)))))
	(let loop ((items old) (count 50))
	  (cond
	   ((or (node-list-empty? items) (eqv? count 0)) (empty-node-list))
	   ((or (not (eq? (gi (node-list-first items)) 'item))
		(equal? name (data (select-elements
				    (children (node-list-first items)) 'title))))
	    (loop (node-list-rest items) count))
	   (else (cons (node-list-first items)
		       (loop (node-list-rest items) (sub1 count))))))))))))

(define update-nu-namespaces
  (list
   (make-xml-attribute 'core 'xmlns namespace-mind-str)))

(define (nunu-protection me)
  (right->string (append (me 'protection) (list (me 'get 'id) (my-oid)))))

(define (update-become-element x)
  (if (node-list-empty? x) x (document-element x)))

(define (update-nu me msg name value changed)
  ((me 'get 'linker) name #f)
  (let* ((objects "objects")
	 (action (literal (action-document (me 'mind-action-document))))
	 (protection (nunu-protection me))
	 (basemod
	  (collection-update-tree
	   (or (me objects) (sxml '(new)))
	   (nu-destination name)
	   name
	   (and value
		(make-xml-element
		 'new namespace-mind
		 (list (make-xml-attribute 'action #f action)
		       (make-xml-attribute 'protection #f (nunu-protection me)))
		 value))
	   context: me action: action protection: protection)))
    (make-xml-element
     'reply namespace-mind update-nu-namespaces
     (node-list
      (make-xml-element 'become namespace-mind '() (update-become-element (dsssl-message-body me)))
      (make-xml-element 'output namespace-mind '() "")
      (make-xml-element
       'link namespace-mind
       (list (make-xml-attribute 'name #f objects))
       (if changed
	   (collection-update-tree
	    basemod
	    '()
	    "changes"
	    (lambda (old) (nu-update-rss
			   (or old (sxml `(rss
					   (channel
					    (title)
					    (link ,(literal "/" (me 'get 'id)))
					    (description
					     ,(or (document-element
						   (me 'body/parsed-xml)) (empty-node-list)))))))
			   name value))
	    action: action
	    context: me protection: protection) 
	   basemod))
      ))))

(define (put-lock-on me msg target name form anyway)
  (let ((dc-creator (msg 'dc-creator)))
    (if (or (nunu-lockable? target dc-creator)
	    (and anyway
		 (or (node-list-empty? ((sxpath '(authors id *text*)) target))
		     (member (symbol->string dc-creator)
			     ((sxpath '(authors id *text*)) target))
		     (eq? ((make-service-level (me 'protection) (or (msg 'capabilities) '())))
			  #t))))
	(begin
	  (create-and-initialize-states me msg)
	  (update-nu me msg name (nunu-lock dc-creator target name form) #f))
	(nu-error (string-append "unable to lock " name)))))

(define (remove-lock-from0 me msg dc-creator dc-date target name form)
  (let ((text (node-list-first
	       (select-elements (children target) 'draft))))
    (if (node-list-empty? (children text))
	#f
	(make-xml-element
	 (gi target) (xml-element-ns target)
	 (attributes target)
	 ((xml-walk #f msg 'no-root #f '() '() form (children target))
	  make-empty-lock
	  (cons is-text?
		(lambda-transformer
		 "nunu:nunu:#/lock#unlock"
		 (make-xml-element
		  'text namespace-nu
		  (if (node-list-empty? text)
		      (attributes (sosofo))
		      (attributes text))
		  (children text))))
	  (make-authors
	   (string-split (data (Xform-field 'authors form)) ","))
	  (make-date-element dc-date)
	  make-empty-draft
	  keep-rest)))))

(define (nunu-unlock dc-creator dc-date target name form)
  (remove-lock-from0 #f #f dc-creator dc-date target name form))

(define (remove-lock-from me msg target name form)
  (if (nunu-locked? target (msg 'dc-creator))
      (let ((new (remove-lock-from0
		  #f #f (msg 'dc-creator) (msg 'dc-date) target name form)))
	(if (string=? name subscribers-str)
	    ((me 'get 'writer) 'subscribe
	     (make-subscriber-list ((sxpath '(text // li)) target))))
	(commit-state! me (create-and-initialize-states me msg) name)
	(update-nu me msg name new #t))
      (nu-error (string-append "nu " name " not locked by you"))))

(define (steal-lock-from me msg target name form text)
  (let ((old (string->oid (data (node-list-first
				 (select-elements (children target) 'lock))))))
    (if old
	(let ((new (put-lock-on me msg target name form #t)))
	  ;; KLUDGE: we better improve update-nu to include the send
	  ;; operation instead bypassing the accept part here.
	  ((me 'get 'sender)
	   to: old
	   type: 'write
	   'content-type text/xml
	   'body/parsed-xml 
	   (let ((text `("Forced lock on "
			 (a (@ (href ,(read-locator
				       (msg 'location-format)
				       (list name (me 'get 'id)))))
			    name)
			 " " ,text)))
	     (sxml
	      `(form
		(subject . ,text)
		(text . ,text)))))
	  new)
	(nu-error (string-append "nu " name " not locked")))))

(define (revert-nu me msg target name form)
  (if (nunu-locked? target (msg 'dc-creator))
      (begin
        (commit-state! me (create-and-initialize-states me msg) name)
        (update-nu
	 me msg name
         (make-xml-element
          (gi target) (xml-element-ns target) (attributes target)
          ((xml-walk #f #f 'no-root #f '() '() form (children target))
           make-empty-lock
           (cons is-draft?
                 (lambda-transformer
		  "nunu:nunu:#/revert"
                  (let ((text (node-list-first
                     (select-elements (children target) 'text))))
                    (make-xml-element
                     'draft namespace-nu (attributes (sosofo))
                     (children text)))))
           keep-rest))
	 #f))
      (nu-error (string-append "nu " name " not locked by you"))))

(define (comment-nu me msg target name form)
  (begin
    (let ((content (data (Xform-field 'sms form))))
      (if (equal? content "") (nu-error "Empty sms not added."))
      (commit-state! me (create-and-initialize-states me msg) name)
      (update-nu
       me msg name
       (make-xml-element
	(gi target) (xml-element-ns target) (attributes target)
	(let ((msgs (select-elements (children target) 'comments)))
	  (node-list
	   (make-xml-element
	    'comments #f (empty-node-list)
	    (node-list (children msgs)
		       (make-xml-element
			'sms #f '()
			(ennunu (lambda (m t c) (make-nunu-link 'a t c))
				(string-trim-both
				 (if (< (string-length content) 161)
				     content (substring content 0 160)))))))
	   ((xml-walk #f #f 'no-root #f '() '() form (children target))
	    (cons is-sms? xml-drop)
	    keep-rest))))
       #t))))

(define (nu-drop-comments me msg target name form)
  (if (nunu-lockable? target (msg 'dc-creator))
      (begin
        (commit-state! me (create-and-initialize-states me msg) name)
	(update-nu
	 me msg name
	 (make-xml-element
	  (gi target) (xml-element-ns target) (attributes target)
	  ((xml-walk #f #f 'no-root #f '() '() form (children target))
	   (cons is-sms? xml-drop)
	   keep-rest))
	 #f))
      (nu-error "I don't have enough permissions.")))

(define (old-fast-filter-decor nl)
  (node-list-map
   (lambda (node)
     (if (xml-element? node)
         (cond
          ((equal? (attribute-string 'class node) "decoration")
           (empty-node-list))
          ((equal? (attribute-string 'class node) "decorationframe")
           (filter-decor (children node)))
          (else node))
         node))
   nl))

(define (filter-decor nl)
  (let ((c ((sxpath '(// (* (@ (equal? (id "contentframe")))))) nl)))
    (if (node-list-empty? c)
        (old-fast-filter-decor nl)
        (children (node-list-first c)))))

(define (find-title form)
  (or (ormap
       (lambda (path)
	 (let ((orig ((sxpath path) form)))
	   (and (not (equal? (data orig) ""))
		(make-xml-element 'title #f (empty-node-list) (children orig)))))
       '((head title) (body (h1 1)) (body * (h1 1)) (body (h2 1)) (body * (h2 1)) (body (h3 1)) (body (h4 1))))
      (make-xml-element 'title #f (empty-node-list) (list ""))))

(define (make-head form)
  (make-xml-element
   'head #f (empty-node-list)
   (append ((sxpath '(head base)) form)
	   (list (find-title form))
	   ((sxpath '(head meta)) form)
	   ((sxpath '(head link)) form)
	   ((sxpath '(head style)) form)
	   ((sxpath '(head script)) form))))

(define guess-content-part0-html-namespaces
  (list #f namespace-xhtml namespace-html))

(define (guess-content-part0 form)
  (cond
   ((eq? (gi (document-element form)) 'form)
    ;; Parse the text content.
    (let ((text (children (node-list-first
                           (select-elements (children form) 'text))))
          (media-type (node-list-first
                       (select-elements (children form) 'media-type))))
      (let* ((in ((or (mime-converter text/xml (if (node-list-empty? media-type)
						   text/xml
						   (data media-type)))
		      (nu-error "no mime-cast for '~a' registered" media-type))
		  (data text)))
	     (ine (document-element in)))
	(if (member (ns ine) guess-content-part0-html-namespaces)
	    (let* ((body0 (if (or (eq? (gi ine) 'html) (eq? (gi ine) 'HTML))
			      (let ((b (select-elements (children ine) 'body)))
				(if (node-list-empty? b)
				    (node-list-first
				     (children (select-elements (children ine) 'BODY)))
				    (node-list-first b)))
			      (make-xml-element 'body #f '() in)))
		   (body (if (or (node-list-empty? body0)
				 (and (eq? (first-child-gi body0) 'div)
				      (node-list-empty? (children (children body0)))))
			     (empty-node-list)
			     (make-xml-element
			      'body (ns body0) (xml-element-attributes body0)
			      (if (and (eq? (gi (children body0)) 'div)
				       (node-list-empty? (node-list-rest (children body0))))
				  (children (children body0))
				  (children body0))))))
	      (if (not (node-list-empty? (children body)))
		  (make-xml-element
		   'html #f '()  (node-list
				  (make-head (if (or (eq? (gi ine) 'html) (eq? (gi ine) 'HTML))
						 ine
						 (make-xml-element 'html #f '() body)))
				  body))
		  (empty-node-list)))
	    in))))
   ((eq? (gi (document-element form)) 'html)
    (make-xml-element
     'html #f (empty-node-list)
     (node-list
      (make-head form)
      (make-xml-element
       'body #f (empty-node-list)
       (filter-decor
	(children (node-list-first (select-elements (children form) 'body))))))))
   (else form)))

(define (guess-content-part form)
  (if (eq? (gi form) 'put)
      (let ((d ((sxpath '(output)) form)))
	(guess-content-part0 ((or (mime-converter text/xml (attribute-string 'media-type d))
				  (nu-error "can't parse media type ~a"
					 (attribute-string 'media-type d)))
			      (data d))))
      (guess-content-part0 form)))

;; Transform an incomming link.  For html, we imply strong links if
;; the href attribute looks valid.  TODO look at the data content of
;; the node, if it's the same as the link but starts with a @, make it
;; an include link.  How about &lt;div class="include"&gt;?

(define (make-nu-link-transformer register)
  (lambda-transformer
   "nunu:nunu:#/tranform#make-link"
   (let ((href (@? 'href)))
     (if (and href (call-with-values (lambda () (nunu-regexp href))
		     (lambda args (pair? args))))
         (begin
           (register href)
           (if (eq? (xml-element-ns (sosofo)) namespace-nu)
               (sosofo)
               (make-xml-element
                (gi (sosofo)) namespace-nu (append (attributes (sosofo))
						   nu-xmlns-attributes)
                (children (sosofo)))))
         (sosofo)))))

(define (make-nu-a-transformer msg register)
  (lambda-transformer
   "nunu:nunu:#/tranform#make-a"
   (if (string=? (data (sosofo)) "?")
       (empty-node-list)
       (let ((s (@? 'href)))
         (if s ; (and s (http-prefix? s))
             (let loop ((href (parsed-locator s))
                        (location '()))
               (if (and (pair? href) (null? (cdr href)))
                   (let ((name (strip-html-suffix (car href))))
                     (if (and (receive (f t v) (nunu-regexp name) f)
                              (or (equal? location (msg 'location))
                                  (and (pair? location) (null? (cdr location))
                                       (string->oid (car location)))))
                         (begin
                           (register name)
                           (make-xml-element
                            (gi (sosofo)) namespace-nu
                            (cons (make-xml-attribute 'href #f name)
                                  nu-a-xmlns-attributes)
                            (children (sosofo))))
                         (sosofo)))
                   (loop (cdr href) (cons (car href) location))))
             (sosofo))))))

(define (change-draft me msg dc-creator orig-target name form)
  (define target (or (document-element orig-target)
		     (nu-error "can't update ~a" (xml-format orig-target))))
  (if (or (nunu-locked? target dc-creator) (nunu-lockable? target dc-creator))
      (let*
	  ((draft-links (make-draft-links me name))
	   (register (make-register draft-links))
	   ;; TODO: this walk would be better merged with
	   ;; guess-content-part's work
	   (text
	    (if (equal?
		 (data (node-list-first
			(select-elements (children form) 'media-type)))
		 "text/wikipedia")
		;; TODO remove this inefficiency: first we parse,
		;; then we walk and register links.  This should be
		;; one pass.
		(let ((parsed (guess-content-part form))
		      (regtrans
		       (lambda-transformer
			"nunu:nunu:#/tranform#change-draft"
			(let ((href (@? 'href)))
			  (if (and href
				   (not (http-prefix? href))
				   (not (and (> (string-length href) 0)
					     (eqv? (string-ref href 0) #\/)))
				   (call-with-values
				       (lambda () (nunu-regexp href))
				     (lambda args (pair? args))))
			      (register href))
			  (sosofo)))))
		  ((xml-walk me msg 'no-root '() '() '() '() parsed)
		   drop-pi
		   (cons is-include? regtrans)
		   (cons is-anchor? regtrans)
		   (cons sxp:element?
			 (lambda-transformer
			  "#nunu:/tranform#change-draft"
			  (transform sosofos: (children (sosofo))))))
		  parsed)
		((xml-walk me msg 'no-root '() '() '() '()
			   (guess-content-part form))
		 drop-pi
		 (cons is-quote? asis-transformer)
		 (cons sxp:literal?
		       (lambda-transformer
			"nunu:nunu:#/tranform#change-draft"
			(ennunu (lambda (mode linktext content)
				  (register linktext)
				  (make-nunu-link mode linktext content))
				(data (sosofo)))))
		 (if (eq? (gi form) 'html)
		     (cons is-anchor? (make-nu-a-transformer msg register))
		     (cons is-nu-link? (make-nu-link-transformer register)))
		 semi-apply-templates)))
	   (do-lock (use-locking? target form))
	   (node
	    (make-xml-element
	     (gi target) (ns target) (attributes target)
	     (list
	      (make-xml-element
	       'draft namespace-nu
	       (nu-text-attributes
		(select-elements (children form) 'media-type))
	       (if do-lock text (empty-node-list)))
	      (make-xml-element
	       'lock namespace-nu (empty-node-list)
	       (if do-lock
		   (list (make-xml-literal (oid->string dc-creator)))
		   (empty-node-list)))
	      (let ((orig (node-list-first
			   (select-elements (children target)
					    'authors)))
		    (oid (oid->string dc-creator)))
		(if (literal-member oid (children orig))
		    orig
		    (make-xml-element
		     (gi orig) (ns orig)
		     (attributes orig)
		     (cons (make-xml-literal oid) (children orig)))))
	      (if do-lock
		  (node-list-first (select-elements (children target)
						    'text))
		  (make-xml-element
		   'text namespace-nu
		   (nu-text-attributes
		    (select-elements (children form) 'media-type))
		   text))
	      (node-list-first (select-elements (children target)
						'date))
	      (node-list-first (select-elements (children target)
						'comments))))))
	(values node draft-links do-lock))
      ;; TODO catch that and make it http "423 Locked" for WebDAV
      (nu-error (string-append
		 "locked by "
		 (data (select-elements (children target) 'lock))))))

(define (nunu-change-draft dc-creator target name form)
  (receive (new-entry draft-links do-lock)
	   (change-draft #f #f dc-creator (or (document-element target)
					      (make-nu name)) name form)
	   (values
	    new-entry
	    (hash-table-fold
	     draft-links
	     (lambda (name v all)
	       (cons
		(cons name
		      (hash-table-ref/default draft-links name #f))
		all))
	     '() )
	    do-lock)))

(define (do-change-draft me msg target name form)
  (let ((all-states (create-and-initialize-states me msg)))
    (receive
     (node draft-links do-lock)
     (change-draft me msg (msg 'dc-creator) target name form)
     (set-draft-links! all-states name draft-links)
     (if (not do-lock) (commit-state! me all-states name))
     ;; not here anyway: (pilot-makedoc name (data draft))
     (update-nu me msg name node #f))))

(define (toggle-replication-state me msg form)
  (let* ((replicates (me 'replicates))
	 (li ((sxpath '(Bag li)) replicates)))
    (receive
     (deleteable insertable)
     (partition (lambda (name)
		  (ormap (lambda (li)
			   (equal? (attribute-string 'resource li) name))
			 li))
		(map data (Xform-field 'name form)))
     (let ((nli (fold
                 (lambda (name init)
                   (if (equal? name "") init
                       (node-list
			(make-xml-element
			 'li namespace-rdf
			 (list
			  (make-xml-attribute 'resource namespace-rdf name))
			 (empty-node-list))
			init)))
                 (empty-node-list)
                 insertable))
	   (left (node-list-filter
                  (lambda (li)
		    (not (member (attribute-string 'resource li) deleteable)))
		  li)))
       (if (and (node-list-empty? nli) (node-list-empty? left))
           (error "cowardly not removing the last supporter")
           (make-xml-element
	    (gi replicates) (ns replicates) (xml-element-attributes replicates)
	    (make-xml-element 'Bag #f '() (append nli left))))))))

;;(make-service-level (me 'protection)
;;                    (list (list (me 'dc-creator))))

;; *under construction* notify-subscribers has to be changed no longer
;; to use the configurable variables but a dynamich subscription
;; mechanism instead.

(define (notify-subscribers me msg)
  (if (me 'subscribe)
      (for-each
       (lambda (subscriber)
         (guard
          (c (else (logerr "notify-subscribers: ~a\n" c)))
          (http-send-message!
           #f
           (lambda (result) #t)
           (make-message
            (property 'host (car subscriber))
            (property 'dc-identifier
                      (string-append "/" (cdr subscriber) "/" (nu-name msg)))
            (property 'type 'write)
            (property 'content-type "text/xml")
            (property 'mind-body (msg 'mind-body)))
           (voter-auth (car subscriber))
	   #t)))
       (me 'subscribe))))

(define (nunu-missing-nu name)
  (nu-error (string-append "There is no nu " name ".")))

(define (nunu-action-do-write me msg)
  (let* ((form (document-element (msg 'body/parsed-xml)))
         (action (data (Xform-field 'action form))))

    ;; 2002-02-23: BEWARE To step by step repair an old sin, we drop
    ;; capabilities here, just in case we have some.  This should be
    ;; removed at a reasonable point in time.

    ((me 'get 'writer) 'capabilities '())

    (let* ((name (nu-name msg))
	   (t0 (if name
		   (fetch me msg name)
		   (nu-error "missing nu's name"))))
      ;; currently being modified.
      ;;         (if (not (string=? name subscribers-str))
      ;;             (notify-subscribers me msg))
      ;; TODO do something sensible, if there is an object accociated.
      (cond
       ((equal? action "lock")
	(let ((t (data (Xform-field 'text form)))
	      (target (if t0 t0 (make-nu name))))
	  (if (equal? t "")
	      (put-lock-on me msg target name form #f)
	      (steal-lock-from me msg target name form msg))))
       ((or (equal? action "release")
	    (equal? action "unlock")) ; deprecated
	(remove-lock-from
	 me msg
	 (or t0 (nu-error (string-append "There is no nu " name " to unlock.")))
	 name form))
       ((equal? action "release-all")
	;; TODO convert to nu-fold-all
	((me 'fold-links)
	 (lambda (name dummy)
	   (guard
	    (ex (else #f))
	    (remove-lock-from me msg (fetch me msg name) name
			      (make-xml-element
			       'form #f (empty-node-list)
			       (Xform-field 'authors form)))))
	 #f))
       ((equal? action "revert")
	(revert-nu me msg (or t0 (nunu-missing-nu name)) name form))
       ((equal? action "sms")
	(comment-nu me msg (or t0 (nunu-missing-nu name)) name form))
       ((equal? action "sms-drop")
	(nu-drop-comments me msg (or t0 (nunu-missing-nu name)) name form))

       ((equal? action "protect")
	;; This checks for domination, not service right.
	(if (eq? ((make-service-level (me 'protection) (or (msg 'capabilities) '())))
		 #t)
	    (let* ((challenge-text (data (Xform-field 'challenge form)))
		   (challenge (mind-parse-protection
			       challenge-text
			       (user-context me msg)
			       (public-context))))
	      (if (memq #f challenge)
		  (nu-error (format #f "could not parse given protection ~a, understood: ~a"
				    (data (Xform-field 'challenge form)) challenge)))
	      (if (dominates? challenge (msg 'capabilities))
		  (make-xml-element
		   'reply namespace-mind update-nu-namespaces
		   (node-list
		    (make-xml-element 'become namespace-mind '() (me 'body/parsed-xml))
		    (make-xml-element 'output namespace-mind '() "")
		    (make-xml-element
		     'protection namespace-mind '()
		     challenge-text)))
		  (nu-error "I don't have enough permissions.")))
	    (nu-error (string-append
		       "Only the owner (" 
		       (oid->string (me 'get 'dc-creator))
		       ") can change protection here."))))
       ((equal? action "toggle-replication-state")
	(make-xml-element
	 'reply namespace-mind update-nu-namespaces
	 (node-list
	  (make-xml-element 'become namespace-mind '() (me 'body/parsed-xml))
	  (make-xml-element 'output namespace-mind '() "")
	  (make-xml-element 'replicates namespace-mind '()
			    (toggle-replication-state me msg form)))))
       (else ;; (or (equal? action "write") (eq? (gi form) 'html))
	(do-change-draft me msg
			 (or (document-element t0) (make-nu name))
			 name
			 ;; If we use 'form' instead of the full
			 ;; message body, we drop superflous comments
			 ;; and xml declarations and become fully xml
			 ;; compatible.  However users, who
			 ;; accidentally write more than one top level
			 ;; element (which is not wellformed xml) will
			 ;; loose their work.
			 (msg 'body/parsed-xml)))))))

;; Upon initialization we put something into the "metasystems" slot.
;; We should probably do something more.  This is the very minimum to
;; find a template to present our view.  As defined further down, the
;; initialization is supposed to be a text/plain message (so far it
;; matches to the message send by the kernel) which arrives when
;; there's anything in the metasystems slot.

(define (nunu-initialize me msg)
  (let ((state (create-and-initialize-states me msg))
        (nu0 (make-nu metasystems-str)))
    (make-xml-element
     (gi nu0) (ns nu0) (attributes nu0)
     ((xml-walk me msg 'no-root '() '() '() '() (children nu0))
      (cons is-text?
            (lambda-transformer
	     "nunu:nunu:#initialize doesn't work anyway anymore"
             (make-xml-element
              'text  namespace-nu (nu-text-attributes "text/wikipedia")
              (node-list
               (make-xml-element
                'ol #f (empty-node-list)
                (node-list
                 (make-xml-element
                  'li #f (empty-node-list)
                  (make-xml-literal (oid->string
                                     (mind-default-lookup
                                      me msg (msg 'mind-body)))))))))))
      (make-date-element (msg 'dc-date))))))

(define (nunu-action-write me msg)
  (cond
   ((and (not (me "objects"))
	 (equal? (msg 'content-type) text/plain)
	 (string->oid (msg 'mind-body)))
    (let ((proposal (update-nu me msg metasystems-str
			       (nunu-initialize me msg) #t)))
      (values (xslt-transformation-accept  me msg proposal)
	      (xml-digest-simple proposal))))
   (((make-service-level (me 'protection) (or (msg 'capabilities) '())))
    (let ((proposal (nunu-action-do-write me msg)))
      (values (xslt-transformation-accept  me msg proposal)
	      (xml-digest-simple proposal))))
   (else
    (let* ((name (nu-name msg))
	   (form (document-element (msg 'body/parsed-xml)))
	   (t0 (if name
		   (fetch me msg name)
		   (nu-error "missing nu's name"))))
      (if (equal? (data (Xform-field 'action form))
		  "sms")
	  (let ((proposal
		 (comment-nu me msg (or t0 (nunu-missing-nu name)) name form)))

	    (values (xslt-transformation-accept me msg proposal)
		    (xml-digest-simple proposal)))
	  (nu-error "You may leave comments only."))))))
