;; (C) 2000, 2001, 2003 J�rg F. Wittenberger see http://www.askemos.org

;;** Data Definition

;; All elements here are from their own namespace:

(%early-once-only

 (define namespace-nu-str "nu")
 (define namespace-nu (string->symbol namespace-nu-str))

 (define nu-xmlns-attributes
   (list (make-xml-attribute 'xmlns #f namespace-html-str)
         (make-xml-attribute 'nu 'xmlns namespace-nu-str)))

 (define nu-a-xmlns-attributes
   (list (make-xml-attribute 'xmlns #f namespace-nu-str)))
 )

;; We store a nu like this.

;; <!element nu (text & date & lock? & draft? & authors?)>
;; <!attlist nu id ID #REQUIRED>

(define nunu-text-attributes-rest
  (node-list (make-xml-attribute 'space namespace-xml "preserve")))

(define (nu-text-attributes media-type)
  (cons
   (make-xml-attribute 'media-type namespace-nu
                       (if (or (node-list-empty? media-type)
                               (equal? (data media-type) ""))
                           text/xml
                           (data media-type)))
   nunu-text-attributes-rest))

(define nu-media-type-default-attribute-list
  (nu-text-attributes "text/wikipedia"))

(define (make-nu name)
  (make-xml-element
   'nu namespace-nu
   (cons (make-xml-attribute 'id #f name) nu-xmlns-attributes)
   (node-list
    (make-xml-element 'draft namespace-nu
                      nu-media-type-default-attribute-list (empty-node-list))
    (make-xml-element 'authors namespace-nu (empty-node-list)
                      (empty-node-list))
    (make-xml-element 'lock namespace-nu (empty-node-list) (empty-node-list))
    (make-xml-element 'text namespace-nu nu-media-type-default-attribute-list
                      (empty-node-list))
    (make-xml-element 'date namespace-nu (empty-node-list)
                      (literal (rfc-822-timestring (timestamp)))))))

;;** Error reporting

(define (nu-error msg . args)
  (raise
   (make-condition
    &http-effective-condition 'status 400
    'message (apply format msg args))))

;; Please note that there is a flaw in the above definition:

;; Human live within a strong distinction betwenn identifiers and
;; names.  By definition an identifier is anything, what can be used
;; instead of another object "to mean" or "point to" that other
;; object.  It's an "instrinsic" or "natural" property of the object
;; and can never be used for anything else.  There is a 1:1 relation
;; between object and identifier.  A name is also anything associated
;; with an object with the intetion to be usable instead of the object
;; much like an identifier can be used.  But in contrast to
;; identifiers a) the acciciation is artificial, given by a certain
;; authority b) it's arbitrary c) a name is only valid within a
;; certain name space, other name spaces might have independent names
;; for the same object d) a name can reference groups of objects.
;; This means that there is a n:m (usually n:1) relation between
;; objects and names.

;; Unfortunatly most people overlook the difference too often and tend
;; to fight too much over the unresolvable issues which inavoidable
;; result.  From a consuting job I did with IBM, I remember many
;; meeting hours where chiefs and senior developers argued over the
;; naming convention for their files.  Also publications at the net
;; are full of such lost arguments.  It's a generally unresolvable
;; problem.  We have to live with it and learn to handle multiple name
;; spaces.

;; TODO move the above 2 paragraphs somewhere else, refer to them here.

;; You see the "id" attribute in the definition above SHOULD actually
;; be called "name".  At the other hand the XSLT specification knows a
;; short hand, the id() and idref() functions, which select by the
;; given name of an object.  The name is by XSLT definition the value
;; of the attribute called "id".  Hence we need a compromise.  The
;; attribute is called "id", but all own definitions will use it as a
;; name and it's use should be avoided except by the XSLT-defined
;; functions.

;; There are some constant strings:

(define template-str "template")        ; The default view template
(define ASIS-str "ASIS")                ; The "don't touch template
(define metasystems-str "metasystems")  ; The nu which list inherited
					; meta systems

(define subscribers-str "subscribers")  ; The nu list of subscribers.
                                        ; A list of pairs "connection" oid.

(define (nunu-text node)
                                        ; TODO document-element is here
                                        ; to cope with legacy data only!
  (and-let* (node (t (select-elements (children (document-element node))
                                      'text)))
            (children t)))

(define (nunu-draft node)
                                        ; TODO document-element is here
                                        ; to cope with legacy data only!
  (and-let* (node (t (select-elements (children (document-element node))
                                      'draft)))
            (children t)))

(define (use-locking? orig form)
  (not (member "release"
               (node-list-map data (select-elements (children form) 'option)))))

(define the-empty-nu (make-nu "NotExistent"))

;; A nunu link is a cross reference to another nu (in the same name
;; space).

(define (make-nunu-link mode link-text content)
  (make-xml-element
   mode namespace-nu (list (make-xml-attribute 'href #f link-text))
   (list (make-xml-literal content))))

;; The as-is-request askes for the nu only.  no stylesheet MUST be
;; applied when answering.  It MUST be replied with the
;; undefined-reply if the nu is not already defined.

(define ASIS-template
  (make-xml-element
   'template #f (empty-node-list) (make-xml-literal ASIS-str)))
(define (as-is-request target)
  (make-xml-element
   'form namespace-forms
   (empty-node-list)
   (node-list
    (make-xml-element
     'name #f (empty-node-list) (make-xml-literal target))
    ASIS-template)))

(define undefined-reply the-empty-nu)

;; Fetch the nu named "name" from the name space "me".
;;
;; If we find an object identifier, read the object.
;;
;; Note there is currently no way to create the latter case.

(define (nu-destination name)
  (if (> (string-length name) 0)
      (list (utf8-substring name 0 1))
      '()))

(define (fetch me msg name)
  (let ((linked
	 (or (and-let* ((objects (me "objects"))
			(answer (guard
				 (ex ((object-not-available-condition? ex) #f))
				 (car ((me 'get 'reader)
				       to:
				       (cons objects
					     (append (nu-destination name)
						     (list name)))
				       type: 'call
				       body: #f
				       'location (msg 'location)))))) 
		       (message-body answer))
	     (me name))))
    (document-element
     (if (oid? linked)
         (message-body
          ;; TODO document somewhere and refer here how messages are send.
          (car ((me 'get 'reader) to: linked type: 'call)))
         linked))))

(define nunu-fetch fetch)

(define nunu-acquire fetch)

(define (nu-get-nu me msg href)
  (guard
   (ex ((object-not-available-condition? ex) #f))
   (car ((me 'get 'reader)
	 adopt: #t
	 to: href
	 type: 'call 'location (msg 'location)))))

;; The "metasystems" slot is a nu, which text contains a list.  The
;; data of each item of the list is supposed to be the OID of a place
;; from which this place inherits (recursively).

(define (nunu-inherited place msg target)
  (let ((anc (nunu-text (fetch place msg metasystems-str)))
        (sender (place 'get 'reader)))
    (if anc
        (let loop ((x (let ((d (document-element anc)))
			(if (eq? (gi d) 'ol) (children d)
			    ((sxpath '(* ol li)) anc)))))
          (if (node-list-empty? x)
              (values #f #f)
              (let* ((o (string->oid (data (node-list-first x))))
                     (v (guard
                         (exception (else (values #f #f)))
                         (sender to: o type: 'call
                                 'content-type text/xml
				 'location '()
                                 'body/parsed-xml (as-is-request target)))))
                (if (pair? v)
                    (let ((nu (document-element
                               (message-body (car v)))))
                      (if (equal? nu undefined-reply)
                          (loop (node-list-rest x))
                          (values o ((if (nunu-locked? nu (msg 'dc-creator))
                                         nunu-draft nunu-text) nu))))
                    (loop (node-list-rest x))))))
        (values #f #f))))

;; A question remains: Would it better to use mind-default-lookup
;; here?  Than we could have clear text names here.  Not too good if
;; we change the context because semantics would break.  But maybe it
;; would be easier to use.  Let's see how many complains come in over
;; this absolute addressing.

;;** transformations (and predicates)

;; The nu in "node" is locked by the creator of the "msg" if:

(define (nunu-locked? node dc-creator)
  (let ((lock (if node (node-list-first
                        (select-elements (children node) 'lock)) #f)))
    (and lock (not (node-list-empty? lock))
	 (eq? (string->oid (data lock)) dc-creator))))


;;Kept to remind jfw what to do here...
;;(string-append (oid->string (place 'get 'id))
;;                                         "/" target)

;; We need to exchange all nunu links to other ideas with one of:
;; - a html ancor to the idea, if found
;; - the word and a question mark with html ancor to create the idea
;; or
;; - just the plain word.
;; The last case applies only, if the nm-space parameter is #f
;; (meaning render to plain text, not html) and content and link
;; traget are equal.  In other words we ease plain text editing.
;;
;; TODO I just made a mess out of the next three function, hence they
;; deserve some cleanup.

(define-syntax make-nu-anchor
  (syntax-rules ()
    ((_ message location content)
     (make-xml-element 'a namespace-html
			(list (make-xml-attribute
			       'href namespace-html
			       (read-locator (message 'location-format)
					     location)))
			content))))

(define nu-undefined-link-content (node-list (make-xml-literal "?")))

(define-syntax make-virtual-file-name
  (syntax-rules () ((_ target location) (cons target location))))

(define-transformer nunu-to-html-transformator "nunu:html:a"
  (let ((target (or (attribute-string 'href (sosofo)) "")))
    (if (eq? (ns (sosofo)) namespace-nu)
        (if (nunu-acquire (current-place) (current-message) target)	; target name bound?
            (make-nu-anchor
             (current-message)
             (make-virtual-file-name target ((current-message) 'location)) ; FIXME deprecated API
             (transform sosofos: (children (sosofo))))
            (receive
             (owner inherited-node) (nunu-inherited (current-place) (current-message) target)
             (if owner
                 (make-nu-anchor
                  (current-message)
                  (make-virtual-file-name
                   target (list (oid->string owner)))
                  (transform sosofos: (children (sosofo))))
                 (node-list
                  (transform sosofos: (children (sosofo)))
                  (make-nu-anchor
                   (current-message)
                   (make-virtual-file-name target ((current-message) 'location)) ; FIXME deprecated API
                   nu-undefined-link-content)))))
        (make-xml-element
         'a namespace-html
         (list (make-xml-attribute 'href namespace-html target))
         (transform sosofos: (children (sosofo)))))))

(define-transformer nunu-to-html-nowiki-transformator "nunu:html:nowoki"
  (if (or (eq? (ns (sosofo)) namespace-html)
          (not (equal? (data (children (sosofo))) (@! 'href)))
          (not (nunu-acquire (current-place) (current-message)  (@! 'href))))
      (apply-transformer use: nunu-to-html-transformator)
      (transform sosofos: (children (sosofo)))))

;; Include the content of the target node, if it's not already among
;; the ancestors of the node.  In that case create a link (is that
;; really a good idea, I don't think so).

(define-transformer include-to-html-transformator "nunu:html:include"
  (let* ((target-string (or (@? 'href) ""))
         (slash (string-index target-string #\/ (min 1 (string-length target-string))))
         (target (if slash (substring target-string 0 slash) target-string))
         (target-node (cond
                       ((and (not (member target (ancestors)))
			     (nunu-acquire (current-place) (current-message) target)) => ; FIXME API
			     (lambda (n) ((if (nunu-locked? n ((current-message) 'dc-creator)) ; FIXME API
					      nunu-draft nunu-text) n)))
                       (else
                        (receive
                         (owner inherited-node)
                         (nunu-inherited (current-place) (current-message) target)
                         (and owner inherited-node))))))
    (if target-node
        (if (eq? (gi (sosofo)) 'include)
            (transform
             ancestors: (cons target (ancestors))
             sosofos: (if (node-list-empty? target-node)
                          (empty-node-list)
                          (if slash
                              ((sxpath (xpath:parse
                                        (substring
                                         target-string slash
                                         (string-length target-string))))
                               target-node)
                              (if (eq? (gi target-node) 'html)
				  ((sxpath '(body *any*)) target-node) ; KLUDGE, FIXME: API still valid?
				  target-node))))
            (make-nu-anchor
             (current-message)
             (make-virtual-file-name target ((current-message) 'location)) ; FIXME API
             (transform sosofos: (children (sosofo)))))
        (node-list
         (transform sosofos: (children (sosofo)))
         (make-nu-anchor
          (current-message)
          (make-virtual-file-name target ((current-message) 'location)) ; FIXME API
          nu-undefined-link-content)))))

(define-transformer nunu-to-plain-transformator "nunu:text/plain:a"
  (let ((target (attribute-string 'href (sosofo))))
    (if (equal? (data (sosofo)) target)
        (if (eq? (gi (sosofo)) 'include)
            (make-xml-literal (string-append "@" target))
            (make-xml-literal target))
        (sosofo))))

(define transformable-namespaces (list #f namespace-nu namespace-html namespace-xhtml))

(define (is-transformable? node root ns-binding)
  (and (xml-element? node)
       (memq (ns node) transformable-namespaces)
       (eq? (gi node) 'a)))

(define (is-include? node root ns-binding)
  (and (xml-element? node)
       (eq? (ns node) namespace-nu) (eq? (gi node) 'include)))

(define semi-apply-templates            ; see, almost xslt, but does not map
  (cons sxp:element?
        (lambda-transformer
	 "nunu:nunu:#semi-apply-templates"
         (let ((childs (transform sosofos: (children (sosofo)))))
           (if (eq? childs (children (sosofo)))
               (sosofo)
               (make-xml-element
                (gi (sosofo)) (xml-element-ns (sosofo))
                (attributes (sosofo)) childs))))))

(define (make-nunu-transformator nm-space)
  (cons
   is-transformable?
   (case nm-space
     ((html #t) nunu-to-html-transformator)
     ((html-nowiki) nunu-to-html-nowiki-transformator)
     ((#f) nunu-to-plain-transformator))))

(define (make-include-transformator nm-space)
  (cons
   is-include?
   (case nm-space
     ((html html-nowiki #t) include-to-html-transformator)
     ((#f) nunu-to-plain-transformator))))

(define (is-strong? node root ns-binding)
  (and (xml-element? node) (eq? (gi node) 'strong)))
(define-transformer strong-to-plain-transformer "nunu:html:plain#strong"
  (literal (string-append "*" (data (children (sosofo))) "*")))
(define-transformer asis-transformer "nunu:AS IS:#" (sosofo))

;; Analyse data and replace implicit cross references (requirement 5)
;; and URL's with appropriate links.

(define (ennunu.orig nunu-link text)
  (map-matches-alternate
   nunu-oid-regexp
   (lambda (t r) (cons (make-xml-element
                        'a namespace-html
                        (list (make-xml-attribute 'href namespace-html
                                                  (string-append "/" t)))
                        (node-list (make-xml-literal t)))
                       r))
   (lambda (t r)
     (map-matches-alternate
      url-regexp
      (lambda (t r) (cons (make-xml-element
                           'a namespace-html
                           (list (make-xml-attribute 'href namespace-html t))
                           (node-list (make-xml-literal
                                       (srfi:string-join
					(string-split t "<")
					"&lt;"))))
                          r))
      (lambda (t r)
        (map-matches-alternate
         nunu-include-regexp
         (lambda (t r) (cons (nunu-link 'include t t) r))
         (lambda (t r)
           (map-matches-alternate
            nunu-regexp
            ;; The 'a type of a nunu-link was 'refer-to, which says more what it
            ;; is, but the html 'a does the same and makes things easier.
            (lambda (t r) (cons (nunu-link 'a t t) r))
            (lambda (t r)
              (map-matches-alternate
               bold-regexp
               (lambda (t r) (cons (make-xml-element
                                    'strong namespace-html
                                    (empty-node-list)
                                    (node-list (make-xml-literal t))) r))
               (lambda (t r) (cons (make-xml-literal t) r))
               t
               r))
            t
            r))
         t
         r))
      t
      r))
   (node-list-map node-list-first text) ; should result in identity, we hope
   '()))

(define (ennunu.wikipedia nunu-link text)
  (call-wikipedia-parse (data text)))

;; Convert text replacing cross references with either their names or
;; a html anchor to the other nu (depending on the ns parameter).

(define (denunu.orig place message ns text . blocked-ancestors)
  ((xml-walk
    place message 'no-root '()
    '()					; namespaces
    blocked-ancestors text text)
   drop-pi
   (cons is-strong? (if ns asis-transformer strong-to-plain-transformer))
   (make-nunu-transformator ns)
   (make-include-transformator ns)
   semi-apply-templates))

(define (denunu.wikipedia place message ns text . blocked-ancestors)
  (if ns
      (apply denunu.orig place message ns text blocked-ancestors)
      (wikipedia->string text)))

(define denunu #f)

(define nunu-switch-syntax #f)

(define ennunu
  (let ((syntax-list `((#f ,ennunu.orig ,denunu.orig)
		       (orig ,ennunu.orig ,denunu.orig)
		       (#t ,ennunu.wikipedia ,denunu.wikipedia)
		       (wikipedia ,ennunu.wikipedia ,denunu.wikipedia)))
	(key #f))
    (define (ennunu . args) (let ((v (assq key syntax-list)))
			      (apply (cadr v) args)))
    (set! denunu (lambda args (let ((v (assq key syntax-list)))
				(apply (caddr v) args))))
    (set! nunu-switch-syntax (lambda (k) (set! key k)))
    ennunu))
