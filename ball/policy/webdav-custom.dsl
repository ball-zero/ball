<?xml version="1.0" ?>
<!DOCTYPE action SYSTEM "action.dtd" >

<action>

 <method type="read">
  <programlisting>
   (custom-collection-read me msg)
  </programlisting>
 </method>

 <method type="propose">
  <programlisting>
   (custom-collection-propose me msg)
  </programlisting>
 </method>

 <method type="accept">
  <programlisting>
   (collection-accept me msg result)
  </programlisting>
 </method>

</action>
