<?xml version="1.0" ?>
<!DOCTYPE action SYSTEM "action.dtd" >

<action>

 <method type="read">
  <programlisting>
   (nunu-action-get me msg)
  </programlisting>
 </method>

 <method type="write">
  <programlisting>
   (nunu-action-write me msg)
  </programlisting>
 </method>

</action>
