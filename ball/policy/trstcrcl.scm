;;* High Level PKI Interface.

;; We should have at least two implementations for this interface.
;; One based on openssl and one using pgp.

;; We should make that DKIM compatible over time, I guess.  See:
;; http://www.ietf.org/rfc/rfc4871.txt

(define $tc-cert-processing-days (make-shared-parameter 20))

;;** OpenSSL

;; The openssl command line utility is really hard to use.  We better
;; switch to something else.

(define ssl-directory (make-shared-parameter (list "etc" "ssl" "certs"))) ; export

(define openssl (make-shared-parameter
		   (cond-expand
		    (single-binary #f)
		    (else
		     (find-in-path "openssl")))))

(define openssl-config-default-file (make-absolute-pathname '("etc" "ssl") "openssl" "cnf"))

(define (openssl-config-default)
  (if (and #f (file-exists? openssl-config-default-file))
      (display (filedata openssl-config-default-file))
      (display "
[ req ]
default_bits            = 2048
default_md              = sha256
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
#attributes              = req_attributes
x509_extensions = v3_ca # The extentions to add to the self signed cert
req_extensions  = v3_req
x509_extensions = usr_cert

[ usr_cert ]
basicConstraints=CA:FALSE
nsCertType                      = client, server, email
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth, clientAuth, codeSigning, emailProtection
nsComment                       = \"OpenSSL Generated Certificate\"
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer

[ v3_req ]
extendedKeyUsage = serverAuth, clientAuth, codeSigning, emailProtection
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment

[ req_distinguished_name ]
countryName			= Country Name (2 letter code)
countryName_min			= 2
countryName_max			= 2

localityName			= Locality Name (eg, city)

organizationalUnitName		= Organizational Unit Name (eg, section)
#organizationalUnitName_default	=

commonName			= Common Name (e.g. server FQDN or YOUR name)
commonName_max			= 64

emailAddress			= Email Address
emailAddress_max		= 64

")))

;; " a quote char to make emacs happy

(define (openssl-config-extend fn . thunks)
  (with-output-to-file fn
    (lambda ()
      (for-each
       (lambda (thunk) (thunk) (newline))
       (cons openssl-config-default thunks)))))

;;*** (RSA) Key Handling

(define (tc-genkey password . kwds)	; TODO define keyword list
  #;(let ((result (run-child-process
		 (lambda (to) (if password (display password to)))
		 consume-output
		 `(,(openssl) "genrsa"
		   ,@(if password '("-des3" "-passout" "fd:0") '())
		   "2048")
		 #f)))
    (if (string=? result "")
	(error (format "failed: ~a" `(,(openssl) "genrsa" ;; "-batch"
				      ,@(if password '("-des3" "-passout" "fd:0") '())
				      "2048")))
	result))
  (genrsa 4096 password "des3"))

(define (rsa-text key)
  (let ((text (run-child-process
	       (lambda (to) (display key to))
	       consume-output
	       `(,(openssl) "rsa" "-text" "-noout")
	       #f)))
    (if (equal? text "")
	(error "invalid rsa key:\n~a" key)
	text)))

(define (rsa-change-password key old new)
  (call-with-temporary-directory
   (lambda (dir)
     (let ((keyfile (make-pathname dir "key" "pem")))
       (call-with-output-file keyfile (lambda (port) (display key port)))
       (let ((text (run-child-process
		    (lambda (to)
		      (if new (display new to)))
		    consume-output
		    `(,(openssl) "rsa"
		      "-in" ,keyfile
		      ,@(if new '("-passout" "fd:0") '())
		      ,@(if old '("-passin" "env:old") '()))
		    dir
		    (if old (list (cons "old" old)) '()))))
	 (if (equal? text "")
	     (error "RSA key password change failed")
	     text))))))

;;*** Key Store Management

(: tc-private-cert (or boolean :mesh-cert:))
(define *tc-private-cert* #f)

(define (tc-private-cert-reset!)
  (set! *tc-private-cert*
	(and-let* ((fn (tc-private-cert-file))
		   ((file-exists? fn))
		   (encoded-data (filedata fn)))
		  (subject-string->mesh-certificate (x509-subject encoded-data))))
  *tc-private-cert*)

(: tc-private-cert (-> (or boolean :mesh-cert:)))
(define (tc-private-cert)
  (if (not *tc-private-cert*) (tc-private-cert-reset!))
  *tc-private-cert*)

(define *tc-private-subject* (delay (tc-compute-private-subject)))

(define (tc-clear-private-subject!)
  (set! *tc-private-subject* (delay (tc-compute-private-subject))))

(define *tc-private-end-date* (delay (tc-compute-private-end-date)))

(define (tc-clear-private-end-date!)
  (set! *tc-private-end-date* (delay (tc-compute-private-end-date))))

(define (tc-store-key! str)
  (define file
    (make-pathname (ssl-directory) (x509-subject-hash (filedata str)) "pem"))
  (mkdirs (dirname file))
  (call-with-output-file file      
    (lambda (port) (display str port))))

(define (tc-private-cert-file)
  (make-pathname (ssl-directory) "private-cert" "pem"))

(define (tc-private-cert-req-file)
  (make-pathname (ssl-directory) "private-req" "pem"))

(define (tc-private-cert-req-key-file)
  (make-pathname (ssl-directory) "private-req-key" "pem"))

(define (tc-private-key-file)
  (make-pathname (ssl-directory) "private-key" "pem"))

(define (tc-ca-cert-file)
  (make-pathname (ssl-directory) "ca-cert" "pem"))

(define (ca-cert-reply oid message)
  (make-message
   (property 'http-status 200)
   (property 'content-type text/plain)
   (property 'mind-body (filedata (tc-ca-cert-file)))))

(register-agreement-handler! 'X509CACertificate ca-cert-reply)

(define (tc-cert-file-for name)
  (make-pathname (ssl-directory) name "pem"))

(define (tc-certification-for name)
  (let ((ca (tc-ca-cert-file))
	(target-cert (tc-cert-file-for name)))
    (values
     (and (file-exists? ca) ca)
     (if (file-exists? target-cert)
	 target-cert
	 (let ((c (tc-private-cert-file))) (and (file-exists? c) c)))
     (tc-private-key-file))))

(define (tc-store-cert-for! name str)
  (let ((file (tc-cert-file-for name)))
    (mkdirs (dirname file))
    (if (file-exists? file) (remove-file file))
    (call-with-output-file file
      (lambda (port) (display str port)))))

(define (tc-ca-key-file)
  (make-pathname (ssl-directory) "ca-key" "pem"))

(define (tc-store-cert! str)
  (define hash (x509-subject-hash str))
  (define file
    (make-pathname (ssl-directory) hash "0"))
  (if (and hash (not (file-exists? file)))
      (begin
	(mkdirs (dirname file))
	(call-with-output-file file
	  (lambda (port) (display str port)))))
  #t)

(define (tc-store-host-cert! str)
  (if (tc-store-cert! str)
      (begin
	(let ((file (tc-private-cert-file)))
	  (if (file-exists? file) (remove-file file))
	  (call-with-output-file file  (lambda (port) (display str port))))
	(tc-clear-private-subject!)
	(tc-clear-private-end-date!))))

(define (tc-store-host-cert-req! str)
  (define file (tc-private-cert-req-file))
  (if (file-exists? file) (remove-file file) (mkdirs (dirname file)))
  (call-with-output-file file
    (lambda (port) (display str port))))

(define (tc-store-host-cert-req-key! str)
  (define file (tc-private-cert-req-key-file))
  (if (file-exists? file) (remove-file file) (mkdirs (dirname file)))
  (call-with-output-file file
    (lambda (port) (display str port))))

(define (tc-store-host-key! str)
  (define file (tc-private-key-file))
  ;; FIXME there's a bug in call-with-output-file, which requires to
  ;; remove the file before writing to it.  This is especially bad
  ;; since we should set the permissions *before* we write the file!
  (if (file-exists? file)
      (remove-file file)
      (mkdirs (dirname file)))
  (call-with-output-file file
    (lambda (port)
      (change-file-mode file #o0600)
      (display str port))))

(define (tc-store-ca-cert! str key)
  (if str
      (let ((cacert (make-pathname (ssl-directory) "ca-cert" "pem")))
	(mkdirs (dirname cacert))
	(call-with-output-file cacert
	  (lambda (port) (display str port)))
	(call-with-output-file
	    (make-pathname (ssl-directory) (x509-subject-hash str) "pem")
	  (lambda (port) (display str port)))))
  (if key
      (call-with-output-file (make-pathname (ssl-directory) "ca-key" "pem")
	(lambda (port) (display key port)))))

(cond-expand
 ((or single-binary rscheme chicken)
  (define (tc-compute-private-subject)
    (and-let* ((cert (tc-private-cert)))
	      (mesh-cert-subject->string cert))))
 (else
  (define (tc-compute-private-subject)
    (and (file-exists? (tc-private-cert-file))
	 (let loop ((retry 3))
	   (let ((text (run-child-process
			(lambda (to) (display (filedata (tc-private-cert-file)) to))
			consume-output
			(cons (openssl) '("x509" "-subject" "-noout"))
			#f)))
	     (if (equal? text "")
		 (if (eqv? retry 0)
		     (raise (format "invalid private cert ~s" (tc-private-cert-file)))
		     (loop (sub1 retry)))
		 (substring text 9 (sub1 (string-length text))))))))))

(cond-expand
 ((or single-binary rscheme chicken)
  (define (tc-compute-private-end-date)
    (time-utc->date (make-time 'time-utc 0 (x509-expiration-time (filedata (tc-private-cert-file))))
		    (timezone-offset))))
 (else
  (define (tc-compute-private-end-date)
    (and (file-exists? (tc-private-cert-file))
	 (or (srfi19:string->date
	      (run-child-process
	       (lambda (to) (display (filedata (tc-private-cert-file)) to))
	       consume-output
	       (cons (openssl) '("x509" "-enddate" "-noout"))
	       #f)
	      "notAfter=~b ~d ~H:~M:~S ~Y ~z")
	     (error "invalid private cert"))))))

(define (tc-private-subject) (force *tc-private-subject*))

(define (tc-private-end-date) (force *tc-private-end-date*))

(define (tc-make-ca nickname password days)
  (call-with-values
      (lambda ()
	(tc-certificate-root cn: nickname password: password days: days))
    tc-store-ca-cert!))

(define (tc-request-client-key nick pass days)
  (let ((key (tc-genkey #f)))
    (tc-store-host-cert-req-key! key)
    (let ((req (tc-make-certificate-request
		key pass
		subject:
		(list "CN" (tc-certificate-request-assert cn: nick)
		      "L" (if (eq? (public-oid) one-oid)
			      (local-id)
			      (oid->string (public-oid))))
		subjectAltName: nick
		days: days)))
      (tc-store-host-cert-req! req)
      (guard
       (ex (else (logerr " tc-request-client-key email failed ~a\n." ex)))
       (send-email $administrator req '((Subject "certificate request"))))
      req)))

(define (tc-setup)
  ;; (tc-make-ca $administrator $administrator-passwort 365)
  (tc-request-client-key (if (eq? (public-oid) one-oid)
			     (local-id)
			     (oid->string (public-oid)))
			 (oid->string (my-oid))
			 30))

;;*** X509 Certificate Handling

(cond-expand
 ((or single-binary rscheme))
 (else
  (define (x509-text pem)
    (let ((text (run-child-process
		 (lambda (to) (display pem to))
		 consume-output
		 `(,(openssl) "x509" "-text" "-noout")
		 #f)))
      (if (equal? text "")
	  (error "invalid x509 certificate:\n~a" pem)
	  text)))))

(cond-expand
 (single-binary
  (define (tc-request-text pem) (x509req-text pem))
  )
 (else

  (define (x509-subject-hash certificate)
    (let ((ln (run-child-process
               (lambda (to) (display certificate to))
               consume-output
               `(,(openssl) "x509" "-noout" "-hash")
               #f)))
      (if (string=? ln "")
          (error "x509 failed on\n~a" certificate)
          (substring ln 0 (sub1 (string-length ln))))))

  (define (x509req-subject req)
    (run-child-process
     (lambda (to) (display req to))
     consume-output
     `(,(openssl) "req" "-noout" "-subject")
     #f))
  
  (define (tc-request-text pem)
    (let ((text (run-child-process
                 (lambda (to) (display pem to))
                 consume-output
                 `(,(openssl) "req" ;; "-batch"
                   "-verify" "-text" "-noout")
                 #f)))
      (if (equal? text "")
          (error "invalid x509 request:\n~a" pem)
          text)))
  ))

(define (subject-line->cn subject)
  (let ((mcn (irregex ".*CN[[:space:]]*=[[:space:]]*([^/,]+).*")))
    (fold
     (lambda (s i)
       (let ((m (irregex-match mcn s)))
	 (if m (cons (irregex-match-substring m 1) i) i)))
     '()
     (string-split subject "/,"))))

(cond-expand
 (single-binary

(define (x509-sign input num key password #!key (ca #f) (days #f) (usage #f))
  (or (integer? days)
      (error "x509-sign days: period not supplied (or not an integer)"))
  (or ca (error "x509-sign: missing CA"))
  (let ((enable-ca (and usage (memq 'CA usage)))
	#;(cn (subject-line->cn subject)))
    (x509-pem (x509-create-signature (filedata ca) key password num input days))))

(define RSA-KEY-BITS 4096)

(define (tc-certificate-root #!key (days #f) (password #f) (cn #f) (subject #f))
  (or cn (error "certificate-root cn: not supplied"))
  (or days (error "certificate-root days: period not supplied"))
  (let* ((subject (or subject
		      `(("CN" ,cn)
			("L" ,(literal (public-oid))))))
	 (pkey (genrsa RSA-KEY-BITS password))
	 (serial-number 0)
	 (req (x509-create-certificate-request pkey password subject #f #t)))
    (values
     (x509-pem (x509-create-signature #f pkey password serial-number req days))
     pkey)))

(define (tc-make-certificate-request
	 key #!key (password #f) (days #f) (subject #f) (subjectAltName #f))
  (let ((days (or days (error "certificate-request days: period not supplied")))
	(subject (or subject `(("L" ,(literal (public-oid))))))
	(subjAlt (cond
		  ((not subjectAltName) #f)
		  ((string? subjectAltName) (list subjectAltName))
		  ((pair? subjectAltName) subjectAltName)
		  (error "tc-make-certificate-request: unknown subjectAltName" subjectAltName))))
    (x509req-pem
     (x509-create-certificate-request
      key password subject
      (and subjAlt (map (lambda (n) (string-append "DNS:" n)) subjAlt))))))

(define (tc-certificate-request-assert key value)
  (or value
      (error "tc-certificate-request: missing key ~a" key)))

(define (tc-certificate-request key password . args)
  (let ((cn #f)
	(subjectAltName #f)
	(o #f)
	(l (oid->string (public-oid)))
	(days #f))
    (do ((args args (cddr args)))
	((null? args)
	 (tc-make-certificate-request
	  key password: password
	  subject: `(("CN" ,(tc-certificate-request-assert cn: cn))
		     ("O" ,(tc-certificate-request-assert o: o))
		     ("L" ,(tc-certificate-request-assert l: l)))
	  subjectAltName: (or cn subjectAltName)
	  days: days))
      (case (car args)
	((cn: commonName:) (set! cn (cadr args)))
	((o: organizationName:) (set! o (cadr args)))
	((l: Locality:) (set! l (cadr args)))
	((days:) (set! days (cadr args)))
	((subjectAltName:) (set! subjectAltName (cadr args)))
	(else (error "tc-certificate-request: unknown keyword ~a" (car args)))))))

) (else

(define (x509-sign input num key password . kwds)
  (let ((days (or (and-let* ((d (memq days: kwds))) (cadr d))
		  (error "x509-sign days: period not supplied")))
	(ca (and-let* ((d (memq ca: kwds))) (cadr d)))
	(usage (and-let* ((d (memq usage: kwds))) (cadr d)))
	(subject (x509req-subject input)))
    (let ((enable-ca (and usage (memq 'CA usage)))
	  (cn (subject-line->cn subject)))
      (call-with-temporary-directory
       (lambda (dir)
	 (call-with-output-file "ca.pem" (lambda (p) (display (filedata ca) p)))
	 (call-with-output-file "key.pem" (lambda (p) (display key p)))
	 (call-with-output-file "input.pem" (lambda (p) (display input p)))
	 ;; (call-with-output-file "v3ext.cnf" (lambda (p) (display "[ SAN ]\ncopy_extensions = copy\n" p)))
	 (call-with-output-file "v3ext.cnf"
	   (lambda (p)
	     (display "basicConstraints=CA:FALSE\n" p)
	     (for-each
	      (lambda (cn)
		(format p "subjectAltName=DNS:~a\n" cn))
	      cn)))
	 (if enable-ca
	     (call-with-output-file "extension.cnf"
	       (lambda (p) (display "[ v3_ca ]
basicConstraints = CA:true
" p))))
	 (run-child-process
	  (lambda (to)
	    (if password (display password to)))
	  (lambda (from)
	    (let ((result (read-bytes #f from)))
	      (if (string=? result "")
		  (error "no output")
		  (values result (add1 num)))))
	  `(,(openssl) "x509"
	    ,@(if ca '("-req" "-CA" "ca.pem") '())
	    ,@(if enable-ca
		  '("-extensions" "v3_ca" "-extfile" "extension.cnf")
		  '())
	    "-extfile" "v3ext.cnf"
	    ;; "-batch" "-utf8" "-multivalue-rdn"
	    ,@(if password '("-passin" "fd:0") '())
	    "-in" "input.pem"
	    "-set_serial" ,(literal num)
	    "-CAkey" "key.pem" "-days" ,(literal days))
	  dir))))))

(define (tc-certificate-root . kwds)
  (define CN (or (and-let* ((d (memq cn: kwds))) (cadr d))
		 (error "certificate-root cn: not supplied")))
  (let ((days (or (and-let* ((d (memq days: kwds))) (cadr d))
		  (error "certificate-root days: period not supplied")))
	(password (or (and-let* ((d (memq password: kwds))) (cadr d))
		      (error "certificate-root password: password not supplied")))
	(subj (or (and-let* ((d (memq subject: kwds))) (cadr d))
		  `("CN" ,CN
		    "L" ,(literal (public-oid))))))
    (call-with-temporary-directory
     (lambda (dir)
       (let* ((keyout (make-pathname dir "key" "pem"))
	      ;; (config (let ((extfile (make-pathname dir "openssl" "cnf")))
	      ;; 		 (call-with-output-file extfile
	      ;; 		   (lambda (port) (format port "[SAN]\nsubjectAltName=DNS:~a\n" CN)))
	      ;; 		 extfile))
	      (cert (run-child-process
		     (lambda (to) (display password to))
		     (make-none-empty-output (lambda () (error "certificate-root failed")))
		     `(,(openssl) "req" "-utf8"
		       "-new" "-x509" "-passout" "fd:0"
		       "-subj" ,(tc-escape-subjects subj)
		       ;; "-config" ,config "-reqexts" "SAN"
		       "-keyout" ,keyout "-days" ,(literal days))
		     dir)))
	 (values cert (filedata keyout)))))))

(define (tc-make-certificate-request key . kwds)
  (let ((days (or (and-let* ((d (memq days: kwds))) (cadr d))
		  (error "certificate-request days: period not supplied")))
	(password (and-let* ((d (memq password: kwds))) (cadr d)))
	(subjAlt (or (and-let* ((d (memq subjectAltName: kwds))) (cadr d))
		     (error "certificate-request missing subjectAltName")))
	(subj (or (and-let* ((d (memq subject: kwds))) (cadr d))
		  `("L" ,(literal (public-oid))))))
    (call-with-temporary-directory
     (lambda (dir)
       (let* ((keyfile (make-pathname dir "key" "pem"))
	      (config (let ((fn (make-pathname dir "openssl" "cnf")))
			(openssl-config-extend
			 fn
			 (lambda () (format #t "[SAN]\nsubjectAltName=DNS:~a\n" subjAlt)))
			fn))
	      (cmd `(,(openssl) "req" "-new"
		     "-utf8" ;; "-batch" "-multivalue-rdn"
		     ,@(if password '("-passin" "fd:0") '())
		     "-subj" ,(tc-escape-subjects subj)
		     "-config" ,config "-reqexts" "SAN"
		     "-key" ,keyfile "-days" ,(literal days))))
	 (call-with-output-file keyfile (lambda (p) (display key p)))
	 (run-child-process
	  (lambda (to)
	    (if password (display password to)))
	  (make-none-empty-output (lambda () (raise cmd)))
	  cmd dir))))))

(define (tc-certificate-request-assert key value)
  (or value
      (error "tc-certificate-request: missing key ~a" key)))

(define (tc-certificate-request key password . args)
  (let ((cn #f)
	(o #f)
	(l (oid->string (public-oid)))
	(days #f))
    (do ((args args (cddr args)))
	((null? args)
	 (tc-make-certificate-request
	  key password: password
	  subject: `(("CN" (tc-certificate-request-assert cn: cn))
		     ("O" (tc-certificate-request-assert o: o))
		     ("L" (tc-certificate-request-assert l: l)))
	  subjectAltName: cn
	  days: days))
      (case (car args)
	((cn: commonName:) (set! cn (cadr args)))
	((o: organizationName:) (set! o (cadr args)))
	((l: Locality:) (set! l (cadr args)))
	((days:) (set! days (cadr args)))
	(else (error "tc-certificate-request: unknown keyword ~a" (car args)))))))

))

;;** PKCS12

(define (make-pkcs12 cert key password . rest)
  (call-with-temporary-directory
   (lambda (dir)
     (let* ((input (make-pathname dir "input" "pem")))
       (call-with-output-file input
	 (lambda (port)
	   (display cert port)
	   (display key port)
	   (for-each (lambda (x) (display x port)) rest)))
       (run-child-process
	(lambda (to) (if password (display password to)))
	(lambda (from)
	  (let ((text (read-bytes #f from)))
	    (if (equal? text "")
		(error "make-pkcs12 failed"))
	    text))
	`(,(openssl) "pkcs12" "-utf8"
	  "-export" "-passout" "fd:0" "-in" ,input)
	dir)))))

;;*** Utilities

(define (consume-output from) (read-bytes #f from))

(define (make-none-empty-output fail)
  (lambda (from)
    (let ((result (read-bytes #f from)))
      (if (or (not (string? result)) (string=? result ""))
	  (fail)
	  result))))

(define none-empty-output (make-none-empty-output (lambda () (error "no output"))))

(define (tc-string-filter proc str port)
  (do ((i 0 (add1 i)))
      ((fx>= i (string-length str)) str)
    (proc (string-ref str i) port)))

(define (tc-escape-subject-char c out)
  (case c
    ((#\\ #\/ #\=) (display #\\ out)))
  (display c out))

(define (tc-escape-subjects args)
  (call-with-output-string
   (lambda (port)
     (do ((args args (cdr args)))
	 ((null? args))
       (display #\/ port)
       (tc-string-filter tc-escape-subject-char (caar args) port)
       (display #\= port)
       (tc-string-filter tc-escape-subject-char (cadar args) port)))))

;;** PGP/GPG

;; TODO keep gpg process always running.

(define (xgpg-sign str)
  (run-child-process
   (lambda (to) (display str to))
   (lambda (from) (read-bytes #f from))
   "gpg --batch --sign"  ;; use --use-agent ?
   #f))

#|
(define (xgpg-verify required str)
  (run-child-process
   (lambda (to) (display str to))
   (lambda (from)
     ;; FIXME need to verify the signature is from 'required'
     (and (gpg-verify-regex (read-bytes #f from)) #t))
   "gpg --batch --verify" #f))
|#
