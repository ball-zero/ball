;; (C) 2004 Peter Hochgemuth see http://www.askemos.org
;; this is experimentel code!
;; see mechanism/protocol/http/webdav.scm for the protocol adapter part

; (define-macro (custom-template? name body)
;   `(and (eq? 'stylesheet (gi ,body))
; 	(eq? namespace-xsl (ns ,body))
; 	(pair? ((sxpath '((template (@ (equal? (name ,name)))))) ,body))))

(define (custom-template? name body)
  (and (eq? 'stylesheet (gi body))
       (eq? namespace-xsl (ns body))
       (let loop ((names (map xml-attribute-value
			      ((sxpath '(template (@ (name)))) body))))
	 (and (pair? names)
	      (or (string=? name (car names))
		  (loop (cdr names)))))))
  
(define (predicate name body default)
  (or (and-let* ((p0 ((sxpath `((predicate (@ (equal? (name ,name))) 1)))
		     body))
		 ((pair? p0))
		 (p (car p0))
		 ((eq? (ns p) namespace-mind))
		 (t (attribute-string 'test p))
		 ((not (string=? "" t))))
		 t)
       default))

(define mind:service (make-xml-element 'service namespace-mind '() '()))
(define mind:forward (make-xml-element 'forward namespace-mind '() '()))
(define mind:no-access (make-xml-element 'no-access namespace-mind '() '()))

(define (xsl:call-template name)
  (make-xml-element
   'call-template namespace-xsl
   `(,(make-xml-attribute 'name #f name)) '()))

(define (xsl:choose default . choices)
  (if (pair? choices)
      (make-xml-element
       'choose namespace-xsl '()
       (fold
	(lambda (alt init)
	  (cons (make-xml-element
		 'when namespace-dsssl
		 `(,(make-xml-attribute 'test #f (car alt)))
		 (cdr alt))
		init))
	`(,(make-xml-element 'otherwise namespace-xsl '() default))
	choices))
      default))

(define (mind:version v)
  (make-xml-element 'version namespace-mind '() (literal (or v 0))))

(define (mind:date d)
  (make-xml-element 'date namespace-mind '() (date->time-literal d)))

(define (custom-stylesheet method service forward body . adds)
  (let* ((call
	  (if (custom-template? method body)
	      (xsl:call-template method)
	      mind:service))
	 (forward
	  (apply xsl:choose call 
		 (if (string? forward) `((,forward . ,mind:forward)) '())))
	 (template
	  (make-xml-element
	   'template namespace-xsl
	   `(,(make-xml-attribute 'match #f "request")
	     ,(make-xml-attribute 'priority #f "1"))
	   (if (string? service)
	       (apply xsl:choose
		      (if (custom-template? "no-access" body)
			  (xsl:call-template "no-access")
			  mind:no-access)
		      `((,service . ,forward)))
	       forward))))
  (make-xml-element
   'stylesheet namespace-xsl
   (fold
    (lambda (att init)
      (if (and (eq?  (xml-attribute-ns att) 'xmlns)
	       (memq (xml-attribute-name att)
		     '(mind d dsssl xsl)))
	  init
	  (cons att init)))
   `(,(make-xml-attribute 'mind  'xmlns namespace-mind-str)
     ,(make-xml-attribute 'd     'xmlns namespace-dsssl-str)
     ,(make-xml-attribute 'dsssl 'xmlns namespace-dsssl-str)
     ,(make-xml-attribute 'xsl   'xmlns namespace-xsl-str))
   (xml-element-attributes body))
   (node-list-reduce
    (children body)
    (lambda (init n)
      (if (xml-element? n) (cons n init) init))
    `(,template
      ,(make-xml-element
	'variable namespace-xsl
	`(,(make-xml-attribute 'name #f "method")) method)
      . ,adds)))))

(define (not-available-message me msg)
  (let* ((abs (append (reverse (msg 'destination)) (msg 'location)))
	 (here (read-locator (msg 'location-format) 
			     (msg 'location)))
	 (there (read-locator (msg 'location-format) abs))
	 (res  (make-html-error
		 404 abs "Object not available" "" 
		 `(p ,(make-xml-literal "Requestet Object ")
		     (b ,there)
		     ,(make-xml-literal" is not available at ")
		     (a (@ (href ,here)) ,here)))))
    (set-slot! res 'dc-creator  (msg 'dc-creator))
    (set-slot! res 'caller  (me 'get 'id))
    res))

(define (no-access-message me msg)
  (let* ((abs (append (reverse (msg 'destination)) (msg 'location)))
	 (here (read-locator (msg 'location-format) 
			     (msg 'location)))
	 (there (read-locator (msg 'location-format) abs))
	 (res (make-html-error
	       403 abs "Request not allowed" ""
	       `(div
		 (p ,(make-xml-literal
		      "You dont't have the neccessary rights to access:"))
		 (p  (b ,here))))))
    (set-slot! res 'dc-creator  (msg 'dc-creator))
    (set-slot! res 'caller  (me 'get 'id))
    res))


;; read access
(define (read-method msg)
  (let ((body (document-element (msg 'body/parsed-xml))))
  (if (is-meta-form? msg)
      (cond 
       ((equal? (msg 'accept) application/rdf+xml)
	"metainfo")
       ((memq (gi body) '(propfind options))
	(symbol->string (gi body)))
       (else "metaview"))
      "index")))

(define (default-read-method method)
  (case (string->symbol method)
    ((metainfo)
     (lambda (me msg)
       (xml-document->message me msg (access-signature me))))
    ((propfind) 
     (lambda (me msg)
       ((dav-propfind-message) me msg collection: #t)))
    ((options)
     (lambda (me msg)
       (xml-document->message 
	me msg 
	(dav-options (dav-supported-methods me)))))
    ((metaview) (lambda (me msg) (metaview me msg)))
    ((index)
     fancy-index)))

(define (read-service-predicate body)
  (or (predicate "read-service" body #f)
      (predicate "service" body #f)))

(define (read-forward-predicate body)
  (or (predicate "read-forward" body #f)
      (predicate "forward" body #f)))

(define (read-forward me msg)
  (car ((me 'get 'reader)
	to: (msg 'destination)
	type: pishing-mode-compatible-forward-call ; 'call
	'location (cons (car (msg 'destination))
			(msg 'location)))))

(define (flat-list-once lst)
  (let loop ((lst lst))
    (if (null? lst) lst
	(append (car lst) (loop (cdr lst))))))

(define (fancy-index me msg)
  (let ((base (http-format-read-location (cons "" (msg 'location)))))
    (xml-document->message 
     me msg
     (let ((title (make-xml-literal (format "~a" (uri-parse base)))))
       (sxml
	`(html 
	  (head (title ,title)
		(style (@ (language "text/css"))
		  ".propln { font-size:60%; background-color:silver; }")
		(meta (@ (http-equiv "Content-Type") (content "text/html; charset=UTF-8"))))
	  (body
	   (h1 ,title)
	   (table
	    . ,(flat-list-once
		(map-parallel
		 thread-signal-timeout!
		 raise
		 #f			; all
		 #f   			; timeout
		 (lambda (l)
		   (define desc (guard (ex (else (empty-node-list)))
				       (with-timeout
					(and-let* ((to (respond-timeout-interval))
						   ((number? to)))
						  (min (quotient (* to 2) 3) 1))
					(lambda () (me l 'metainfo)))))
		   (sxml
		    `(*TOP*
		      (tr
		       (td (a (@ (href ,(make-xml-literal
					 (string-append 
					  base (uri-quote l)))))
			      ,(make-xml-literal l)))
		       (td  . ,(let ((s (data ((sxpath '(Description creator)) desc))))
				 (if (string-null? s) '()
				     `((small ,(entry-name (string->oid s)))))))
		       (td (small (code ,(oid->string (me l))))))
		      (tr (@ (class "propln"))
			  (td  . ,((sxpath '(Description date *text*)) desc))
			  (td . ,((sxpath '(Description version serial *text*)) desc))
			  (td
			   (small (code ,(let ((ad (data ((sxpath '(Description action-document)) desc))))
					   `(a (@ (href ,(read-locator (msg 'location-format) ad)))
					       ,ad)))))))))
		 ((me 'fold-links-ascending) cons '())))))))))))

(define (custom-collection-read me msg)
  (let* ((body    (document-element (me 'body/parsed-xml)))
	 (dst     (msg 'destination))
	 (srv     (me `(,(my-oid)) 'serve))
	 (nofwd   (or (null? dst) (not dst) (equal? dst '(""))))
	 (method  (read-method msg))
	 (service (read-service-predicate body))
	 (forward (read-forward-predicate body))
	 (result  (and (or service
			   (and srv forward)
			   (and srv nofwd (custom-template? method body)))
		       (document-element
			((me 'get 'xslt-transform)
			 (make-xml-element 'request namespace-mind
					   read-attribute-list
					   (xslt-request-childen msg))
			 (custom-stylesheet method service forward
					    body)))))) ;; + adds
    (cond
     ((not result)
      (if srv
	  (if nofwd 
	      ((default-read-method method) me msg)
	      (read-forward me msg))
	  (no-access-message me msg)))
     ((eq? (gi result) 'service) ;; check namespace too
      ;; if there is a service predicate but no forward predicate, the result may be
      ;; 'service whithout a forward check
      (if (and (not forward) nofwd)
	  ((default-read-method method) me msg)
	  (read-forward me msg)))
     ((eq? (gi result) 'forward)
      (read-forward me msg))
     ((eq? (gi result) 'no-access) 
      (no-access-message me msg))
     ((eq? (gi result) 'not-available) 
      (not-available-message me msg))
     ((eq? (gi result) 'output)
      (output-element->message 
       me msg (document-element result) '()))
     ((xml-element? result)
      (xml-document->message me msg result))
     (else 
      (mime-document->message*
       me msg
       (if (string? result) result "") "text/plain"
       (askemos:dc-date (msg 'dc-date)))))))

(define (collection-read me msg)
  (if ((make-service-level (me 'protection) (msg 'capabilities))
       (my-oid))
      (let ((destination (msg 'destination))
	    (request (document-element (msg 'body/parsed-xml))))
	(cond
	 ((or (null? destination) (equal? destination '("")))
	  (if (is-meta-form? msg)
	      (cond 
	       ((equal? (msg 'accept) application/rdf+xml)
		(xml-document->message me msg (access-signature me)))
	       ((eq? (gi request) 'propfind)
		((dav-propfind-message) me msg collection: #t))
	       ((eq? (gi request) 'options)
		(xml-document->message me msg (dav-options (dav-supported-methods me))))
	       (else (metaview me msg)))
	      (fancy-index me msg))) ;; or something like metaview 
	 (else (read-forward me msg))))
      (raise-access-denied-condition me msg)))


;; write access
(define (output->message me msg tree)
  (let ((args (let ((loc (attribute-string 'location tree)))
		(if loc (list (property 'location loc)) '()))))
    (cond ((node-list-empty? tree)
	   (xml-document->message me msg (empty-node-list)))
	  ((and (node-list-empty? (node-list-rest tree))
		(xml-parseable? (attribute-string 'media-type tree)))
	   (apply xml-document->message
		  me msg (children tree) args))
	  (else (output-element->message me msg tree args)))))

(define (propose-error->message me msg tree)
  (if (node-list-empty? tree)
      (set! tree
	    (sxml
	     `(output
	       (@ (media-type ,application/xml))
	       (error
		(status 400)
		(text
		 (html
		  (head (title "user code error")) 
		  (body (h1 "Request not proccessable")
			(p "Your request does'nt produce a valid result at: "
			   (b ,(read-locator (msg 'location-format) 
					     (msg 'location))))
			"This is typicaly a coding error in user code"))))))))
  (let ((status ((sxpath '(error status)) tree))
	(res  (output->message me msg tree)))
    (set-slot! res 'http-status (if (pair? status)
				    (string->number (data status)) 400))
    res))

(define (write-method msg)
  (let* ((body (document-element (msg 'body/parsed-xml)))
	 (action (data (form-field 'action body))))
    (cond ((is-meta-form? msg)
	   (cond 
	    ((memq (gi body)
		   '(put mkcol copy move propertyupdate lock unlock))
	     (symbol->string (gi body)))
	    ((member action '("copy" "move" "link")) action)
	    (else "metactrl")))
	  ((and (eq? (gi body) 'Fault)
		(eq? (ns body) namespace-soap-envelope))
	   "soap-fault")
	  (else
	   "update"))))

(define (default-write-method method me msg)
  (cond
    ((string=? method "lock")
     ;; lock-reply-of me msg
     (error "not implemened"))
    ((string=? method "unlock")
     ;; (lock-reply-of me msg))
     (error "not implemened"))
    ((string=? method "put") (make-put-element me msg))
    ((or (string=? method "copy") (string=? method "link"))
     (make-copy-element me msg))
    ((string=? method "move") (make-move-element me msg))
    ((string=? method "mkcol") (make-mkcol-element me msg))
    ((string=? method "propertyupdate") (make-proppatch-element me msg))
    ((string=? method "metactrl")
     (add-seed (message-body (metactrl me msg))
	       (me 'version) (msg 'dc-date)))
    ((string=? method "soap-fault")
     (logerr " ~a ignore soap-error notification\n" (me 'get 'id))
     (make-xml-element 
      'reply namespace-mind '()
      (make-xml-element
       'continue #f '()
       (document-element (me 'body/parsed-xml)))))
     ((string=? method "update")
      (error "dont know how to write to a standard collection"))
     (else (error " collection-propose coding error ~a" method))))

;; Kannst Du die hier bitte kommentieren.  Ich verstehe die Motivation
;; nicht.  Warum suchst Du in den namespace-Prefixen?  Ich h�tte
;; verstanden, wenn Du die URL-Werte aussiebst.  Die initiale Liste
;; w�rde ich �brigens als Konstante %early-only-once machen.

(define (add-seed tree version date)
  (let ((e (document-element tree)))
    (make-xml-element
     (gi e) (ns e) 
     (fold
      (lambda (att init)
	(if (and (eq?  (xml-attribute-ns att) 'xmlns)
		 (memq (xml-attribute-name att)
		       '(mind d dsssl xsl)))
	    init
	    (cons att init)))
      `(,(make-xml-attribute 'mind  'xmlns namespace-mind-str)
	,(make-xml-attribute 'd     'xmlns namespace-dsssl-str)
	,(make-xml-attribute 'dsssl 'xmlns namespace-dsssl-str)
	,(make-xml-attribute 'xsl   'xmlns namespace-xsl-str))
      (xml-element-attributes e))
     (node-list
      (mind:version version)
      (mind:date date)
      (children e)))))

(define (write-service-predicate body)
  (or (predicate "write-service" body #f)
      (predicate "service" body #f)))

(define (write-forward-predicate body)
  (or (predicate "write-forward" body #f)
      (predicate "forward" body #f)))

(define (propose-forward me msg)
  (cond ((me (car (msg 'destination)))
	 (add-seed mind:forward (me 'version) (msg 'dc-date)))
	(else (raise (make-object-not-available-condition
		      (read-locator (msg 'location-format) (reverse (msg 'destination)))
		      (me 'get 'id))))))

(define (custom-collection-propose me msg)
  (let* ((body   (document-element (me 'body/parsed-xml)))
	 (dst    (msg 'destination))
	 (srv    (me '() 'serve))
	 (nofwd  (or (null? dst) (not dst) (equal? dst '(""))))
	 (method (write-method msg))
	 (result (or (let* ((service (write-service-predicate body))
			    (forward (write-forward-predicate body)))
		       (and (or service
				(and srv forward)
				(and srv nofwd (custom-template? method body)))
			    (document-element
			     ((me 'get 'xslt-transform)
			      (make-xml-element 'request namespace-mind
						write-attribute-list
						(xslt-request-childen msg))
			      (custom-stylesheet method service forward
						 body) ; + adds
			      ))))
		     (if (not srv)
			 mind:no-access
			 (if nofwd 
			     (default-write-method method me msg)
			     mind:forward)))))
    (let loop ((result result))
      (cond
       ((eq? (gi result) 'service) ;; check namespace too
	(let ((result (default-write-method method me msg)))
	  (if (eq? (gi result) 'service)
	      (error "coding error: default-collection-method for ~a loops!" method)  
	      (loop result))))
     ((eq? (gi result) 'forward)
      (propose-forward me msg))
     ((eq? (gi result) 'no-access) 
      (raise (no-access-message me msg))) ;make a unauthorised-condition here!
     ((eq? (gi result) 'not-available)
      (raise (make-object-not-available-condition
	      (read-locator (msg 'location-format) (reverse (msg 'destination)))
	      (me 'get 'id))))
     ((eq? (gi result) 'reply)
      (add-seed result (me 'version) (msg 'dc-date)))
     (else
      (raise (propose-error->message me msg result)))))))

(define (collection-propose me msg)
  (if ((make-service-level (me 'protection) (msg 'capabilities)))
      (let ((destination (msg 'destination))
	    (request (document-element (msg 'body/parsed-xml))))
	(cond
	 ((and (pair? destination)
	       (not (equal? '("") destination)))
	  ;; forward this request
	  (if (me (car destination))
	      (make-xml-element
	       'forward namespace-mind '()
	       (node-list
		(make-xml-element 'version namespace-mind '()
				  (literal (or (me 'version) 0)))
		(make-xml-element
		 'date namespace-mind '()
		 (date->time-literal (msg 'dc-date)))))
	      (raise (make-object-not-available-condition 
 		      (read-locator (msg 'location-format) (reverse (msg 'destination)))
 		      (me 'get 'id)))))
	 (else
	  (cond
	   ((is-meta-form? msg)
	    (let ((action (data (form-field 'action (msg 'body/parsed-xml)))))
	      (cond
	       ;;((memq (gi request) '(lock unlock)) ;;"LOCK" "UNLOCK"
	       ;; (lock-reply-of me msg))
	       ((eq? (gi request) 'put) 
		(make-put-element me msg))
	       ((eq? (gi request) 'grant)
		(accept-right-reply me msg))
	       ((string=? "copy" action) 
		(make-copy-element me msg))
	       ((string=? "move" action) 
		(make-move-element me msg))
	       ((string=? "link" action) 
		(make-copy-element me msg))
	       ((eq? (gi request) 'mkcol)
		(make-mkcol-element me msg))
	       ((eq? (gi request) 'propertyupdate)
		(make-proppatch-element me msg))
	       (else 
		(let ((reply (document-element
			      (message-body (metactrl me msg)))))
		  (make-xml-element
		   (gi reply) (ns reply) (xml-element-attributes reply)
		   (node-list
		    (make-xml-element 'version namespace-mind '()
				      (literal (or (me 'version) 0)))
		    (make-xml-element
		     'date namespace-mind '()
		     (date->time-literal (msg 'dc-date)))
		    (make-xml-element
		     ;; Maybe we ought to provide a valid location?
		     'output namespace-mind '()
		     (make-xml-element 'success namespace-mind '()
				       ;; (make-xml-element 'status namespace-mind '() status)
				       (empty-node-list)
				       ))
		    (children reply))))))))
	   ;ignore soap-X-fault (this happends only at entry points!)
	   ;it would be better not to send this messages at all in respond!
	   ((and (not destination)
		 (eq? (gi request) 'Fault)
		 (eq? (ns request) namespace-soap-envelope))
	    (logerr " ~a ignore soap-error notification\n" (me 'get 'id))
	    (sxml
	     `(api:reply
	       (@ (@ (*NAMESPACES* (,namespace-mind ,namespace-mind-str api))))
	       (api:version ,(literal (or (me 'version) 0)))
	       (api:date ,(date->time-literal (msg 'dc-date)))
	       (continue , (document-element (me 'body/parsed-xml))))))
	   (else
	    (error "there is nothing you can write to a collection"))))))
      ;; 403 Forbidden
      (raise-access-denied-condition me msg)))

(define (accept-forward me msg)
  (guard (e ((object-not-available-condition? e)
	     (let ((result (not-available-message me msg)))
	       (if (msg 'reply-channel)
		   (send-message! (msg 'reply-channel) result))
	       result))
	    (else (raise e)))
	 ((me 'get 'sender)
	  to: (msg 'destination) type: 'write
	  'location (cons (car (msg 'destination))
			  (msg 'location)))
	 (xml-document->message me msg has-been-forwarded-nl)))

(define (collection-accept me msg result)
  (cond ((is-forward-extension? (document-element result) #f #f)
	 (accept-forward me msg))
	(else
	 (let* ((setter (me 'get 'writer))
		(answer1 (document-element 
			  (execute-extensions me msg (make-environment) result)))
		(continue ((sxpath '(continue)) result))
		(body (if (node-list-empty? continue)
			  answer1 (children continue)))
		(enc (and-let* ((n1 (node-list-first body))
				((eq? (xml-pi-tag n1) 'xml)))
			       (xml-pi-encoding (xml-pi-data n1)))))
	   ;(setter 'dc-date (msg 'dc-date))
	   (setter 'body/parsed-xml body)
	   (setter 'content-type
		   (if (member enc '(#f "UTF8" "UTF-8")) 
		       text/xml (string-append "text/xml; charset=" enc))))
	 ;; collection-propose returns only xml, we know.
	 ;; (output->message me msg (select-elements (children result) 'output))
	 (xml-document->message me msg (children (select-elements (children result) 'output))))))

(define (collection-write me msg)
  (let ((result (collection-propose me msg)))
    (values
     (collection-accept me msg result)
     (xml-digest-simple result))))

(define (collection-write-local me msg)
  (collection-accept me msg (collection-propose me msg)))

(define (custom-collection-write me msg)
  (let ((result (custom-collection-propose me msg)))
    (values
     (collection-accept me msg result)
     (xml-digest-simple result))))

(define (custom-collection-write-local me msg)
  (collection-accept me msg (custom-collection-propose me msg)))

(define (dav-copy-link li)
  (if (eq? (gi li) 'link) li
      (sxml `(link (@ (name ,(attribute-string 'resource li)))
		   (id ,(attribute-string 'href li))))))

;use (dav:resourcetype-collection) from mechanism/webdav.scm
(define davdir (document-element
		(sxml `(dav:prop
			(@ (@ (*NAMESPACES* (,namespace-dav ,namespace-dav-str dav))))
			(resourcetype (collection))
			))))

(define (dav-other-locks old-body here lockinfo)
  (let ((ishere (pcre->proc (string-append ".*/?" here "/?$")) )
	(locktoken ((sxpath '(locktoken href)) lockinfo)))
    (node-list-filter
     (lambda (x)
       (not (and (ishere (attribute-string 'resource x))
		 (or (and (pair? locktoken)
			  (equal? (data locktoken) (data ((sxpath '(locktoken href)) x))))
		     (raise (make-condition
			     &http-effective-condition
			     'message (format "~a is locked by ~a"
					      (attribute-string 'resource x)
					      (data ((sxpath '(owner id)) x)))
			     'status 423))))))
     ((sxpath '(locks *)) old-body))))

(define (dav-lock-make-lockinfo resource lockrequest)
  (let ((ishere (pcre->proc (string-append ".*/?" resource "/?$")) ))
    (make-xml-element
     (gi lockrequest) (ns lockrequest)
     (map
      (lambda (att)
	(if (eq? (xml-attribute-name att) 'resource)
	    (make-xml-attribute 'resource #f resource)
	    att))
      (xml-element-attributes lockrequest))
     (node-list-map
      (lambda (n)
	(if (eq? (gi n) 'locks)
	    (node-list-map
	      (lambda (n)
		(if (eq? (gi n) 'lock)
		    (if (ishere (attribute-string 'resource n))
			(children n)
			(empty-node-list))
		    n))
	      (children n))
	    n))
      (children lockrequest)))))

(define (dav-make-property-entry li propertyupdate)
  (make-xml-element
   'li #f (list (make-xml-attribute
		 'resource #f
		 (if (string? li) li  (attribute-string 'resource li))))
   (if (xml-element? li)
       (dav:accept-propertyupdate (children li) propertyupdate)
       (children (children propertyupdate)))))

(define (dav-make-dir-body old-body path lock request)
  (case lock
    ((#t lock recursive)
     (let ((ishere (pcre->proc (string-append ".*/?" (car path) "/?$")) ))
       (sxml
	`(WebDAVDirectory
	  (locks ,(if request
		      (if (null? (cdr path))
			  (dav-lock-make-lockinfo (car path) request)
			  (if (eq? lock 'recursive)
			      (dav-lock-make-lockinfo (car path) request)
			      '()))
		      '())
		 . ,(dav-other-locks old-body (car path) request))
	  . ,(node-list-filter (lambda (x) (not (eq? (gi x) 'locks)))
			       (children old-body))))))
    ((#f unlock)
     (let ((ishere (pcre->proc (string-append ".*/?" (car path) "/?$")) ))
       (sxml
	`(WebDAVDirectory
	  (locks . ,(dav-other-locks old-body (car path) request))
	  . ,(node-list-filter (lambda (x) (not (eq? (gi x) 'locks)))
			       (children old-body))))))
    ((propertyupdate)
     (if (null? (cdr path))
	 (let ((current-properties ((sxpath '(properties *)) old-body)))
	   (sxml
	    `(WebDAVDirectory
	      (properties
	       . ,(let ((updated (node-list-map
				  (lambda (li)
				    (if (equal? (attribute-string 'resource li)
						(car path))
					(dav-make-property-entry li request)
					(empty-node-list)))
				  current-properties)))
		    (if (node-list-empty? updated)
			(if (null? current-properties)
			    (node-list
			     (dav-make-property-entry (car path) request))
			    (%node-list-cons
			     (dav-make-property-entry (car path) request)
			     current-properties))
			updated)))
	      . ,(node-list-filter (lambda (x) (not (eq? (gi x) 'properties)))
				   (children old-body)))))
	 old-body))
    (else old-body)))

(define empty-dav-dir-body
  (sxml '(WebDAVDirectory (locks))))

(define (dav-is-link-child node)
  (case (gi node) ((id new ref) #t) (else #f)))

;; helper to be maped over tree nodes.

(define (dav-update-node lookup new-attributes node how)
  (cond
   ((eq? how identity)
    (let ((href (attribute-string 'href node)))
      (if href
	  (make-xml-element 'id namespace-mind '() href)
	  node)))
   ((procedure? how)
    (let ((value (how (and node
			   (if (eq? (gi node) 'new)
			       node
			       (node-list-rest
				(children
				 (lookup
				  (if (eq? (gi node) 'link)
				      (data (select-elements
					     (children node) 'id))
				      (attribute-string 'href node))))))))))
      (cond
       ((oid? value)
	(make-xml-element 'id namespace-mind '() (oid->string value)))
       ((not value) (empty-node-list))
       ((and (xml-element? value)
	     (eq? (gi value) 'new) (eq? (ns value) namespace-mind))
	value)
       (else (make-xml-element 'new namespace-mind new-attributes value)))))
   (else how)))

(define (collection-update-tree* lookup tree path target value new-attributes lock lockinfo)
  (define old-state
    (if (eq? (gi (document-element tree)) 'new)
	#f
	(lookup tree)))
  (define dir-body (dav-make-dir-body
		    ((sxpath '(WebDAVDirectory)) old-state)
		    (list target) lock lockinfo))
  (make-xml-element
   'new namespace-mind new-attributes
   (node-list
    davdir
    (let loop ((from (if old-state
			 ((sxpath '(RDF Description links Bag li))
			  old-state)
			 ((sxpath '(link)) tree))))
      (if (or (node-list-empty? from) (not from))
	  (let loop ((path path))
	    (if (null? path)
		(make-xml-element
		 'link #f (list (make-xml-attribute 'name #f target))
		 (dav-update-node lookup new-attributes #f value))
		(make-xml-element
		 'link #f (list (make-xml-attribute 'name #f (car path)))
		 (make-xml-element
		  'new #f new-attributes
		  (node-list
		   davdir (loop (cdr path))
		   (dav-make-dir-body
		    empty-dav-dir-body path lock lockinfo))))))
	  (let* ((node (node-list-first from))
		 (name (attribute-string (if (eq? (gi node) 'link) 'name 'resource) node))
		 (t (if (null? path) target (car path))))
	    (cond
	     ((string=? t name)
	      (if (or value (pair? path))
		  (cons
		   (make-xml-element
		    'link #f (list (make-xml-attribute 'name #f t))
		    (if (null? path)
			(dav-update-node lookup new-attributes node value)
			(collection-update-tree*
			 lookup
			 (if (eq? (gi node) 'link)
			     (node-list-filter dav-is-link-child (children node))
			     (attribute-string 'href node))
			 (cdr path) target value new-attributes
			 lock lockinfo)))
		   (node-list-map dav-copy-link (node-list-rest from)))
		  (node-list-map dav-copy-link (node-list-rest from))))
	     ((string<? t name)
	      (cons
	       (let loop ((path path))
		 (if (null? path)
		     (make-xml-element
		      'link #f (list (make-xml-attribute 'name #f target))
		      (dav-update-node lookup new-attributes #f value))
		     (make-xml-element
		      'link #f (list (make-xml-attribute 'name #f (car path)))
		      (make-xml-element
		       'new #f new-attributes
		       (node-list
			davdir (loop (cdr path))
			(dav-make-dir-body
			 empty-dav-dir-body path lock lockinfo))))))
	       (node-list-map dav-copy-link from)))
	     (else (cons (dav-copy-link node)
			 (loop (node-list-rest from))))))))
    dir-body)))

(define (collection-update-tree** . args)
  (let ((r (apply collection-update-tree* args)))
    (logerr "RES\n~a\n" (xml-format r)) r))

(define (dav-cook-protection me optionals)
  (or (and-let* ((p (memq protection: optionals))
		 (a (cadr p)))
		(cond
		 ((string? a) a)
		 ((node-list? a)
		  (right->string 
		   (or (node-list->right a) '())))
		 ((pair? a) (right->string a))
		 (else (error "illegal protection ~a" a))))
      (and-let* ((p (me 'protection)))
		(right->string p))))

(define (dav-make-default-collection-lookup me)
  (lambda (tree)
    (and tree (make-xml-element
	       'Collection namespace-dav (empty-node-list)
	       (node-list
		(me tree 'metainfo)
		(document-element (fetch-other me tree body: '())))))))

(define (dav-make-locktoken me)
  (literal "locktoken:" (me 'get 'id) "-" (car (me 'version))))

(define (dav-make-lock owner name depth locktoken)
  (document-element
   (sxml `(core:lock
	   (@ (@ (*NAMESPACES*
		  (,namespace-dav ,namespace-dav-str D)
		  (,namespace-mind ,namespace-mind-str core)))
	      (resource ,name)
	      (depth ,(literal depth)))
	   (D:owner (core:id ,(literal owner)))
	   (D:locktoken (D:href ,locktoken))
	   (D:timeout "Infinite")
	   (D:lockscope (D:exclusive))
	   (D:locktype (D:write))))))

(define (collection-update-tree tree path target value . optionals)
  (let* ((me (or (and-let* ((a (memq context: optionals))) (cadr a))
		 (lambda args (error "collection-update-tree no context given"))))
	 (new-attributes
	  (let ((protection (dav-cook-protection me optionals)))
	    (cons
	     (make-xml-attribute
	      'action #f
	      (or (and-let* ((a (memq action: optionals))) (literal (cadr a)))
		  (error "collection-update-tree: mandatory parameter 'action:' missing")))
	     (if protection
		 (list (make-xml-attribute 'protection #f protection)) '()))))
	 (lookup (or (and-let* ((a (memq lookup: optionals))) (cadr a))
		     (dav-make-default-collection-lookup me)))
	 (owner (or (and-let* ((a (memq owner: optionals))) (cadr a))
		    #f))
	 (lockinfo (or (and-let*
			((a (memq lock: optionals))
			 (v (cadr a))
			 ((or (eq? v #t)
			      (eq? (gi v) 'lock) (eq? (gi v) 'unlock))))
			(if (eq? v #t)
			    (dav-make-lock owner target "0" (dav-make-locktoken me))
			    (begin (debug 'hups (xml-format v)) v)))
		       (empty-node-list))))
    (collection-update-tree*
     lookup
     (cond ((document-element tree) => identity)
	   ((oid? tree) tree)
	   ((string? tree) (me tree))
	   (else (sxml '(new))))
     path target value new-attributes
     (and-let* (((eq? (gi lockinfo) 'lock))
		(depth (attribute-string 'depth lockinfo)))
	       (if (string-ci=? depth "infinite") 'recursive 0))
     (children lockinfo))))

;; unused!
(define (dav-location msg resource isdir)
  (read-locator (msg 'location-format) 
		(let ((d (reverse (msg 'destination))))
		  (if isdir
		      `("" ,resource ,@d . ,(msg 'location))
		      `(,resource ,@d . ,(msg 'location))))))

(define (collection-update-propose me msg objects . optionals) ; badly named
  (let ((new-attributes
	 (let ((protection (dav-cook-protection me optionals)))
	   (cons
	    (make-xml-attribute
	     'action #f
	     (or (and-let* ((a (memq action: optionals))) (literal (cadr a)))
		 (oid->string (mind-default-lookup me msg "public"))))
	    (if protection
		(list (make-xml-attribute 'protection #f protection)) '()))))
	(request (document-element
		  (or (and-let* ((a (memq request: optionals))) (cadr a))
		      (msg 'body/parsed-xml))))
	(old-objects (or (me objects) (sxml '(new))))
	(destination (or (and-let* ((a (memq destination: optionals))) (cadr a))
			 (msg 'destination)))
	(lookup (or (and-let* ((a (memq lookup: optionals))) (cadr a))
		    (dav-make-default-collection-lookup me)))
	(become (document-element (me 'body/parsed-xml))))
    (cond
     ((eq? (gi request) 'put)
      (let ((name (attribute-string 'filename request)))
	(sxml
	 `(reply
	   (become . ,become)
	   (output (success (status 201)))
	   (link (@ (name ,objects))
		 ,(collection-update-tree*
		   lookup old-objects destination name
		   (make-xml-element 'new namespace-mind
				     new-attributes
				     (select-elements (children request) 'output))
		   new-attributes
		   0 (dav-make-lock (msg 'dc-creator) name "0"
				    (dav-make-locktoken me))))))))
     ((eq? (gi request) 'mkcol)
      (sxml
       `(reply
	 (become . ,become)
	 (output (success (status 201)))
	 (link (@ (name ,objects))
	       ,(collection-update-tree* lookup old-objects destination
			(attribute-string 'name request)
			(make-xml-element 'new namespace-mind new-attributes davdir)
			new-attributes #f (empty-node-list))))))
     ((eq? (gi request) 'lock)
      (let* ((resource (attribute-string 'resource request))
	     (locktoken (dav-make-locktoken me))
	     (lockelement (dav-make-lock (msg 'dc-creator) resource "0" locktoken)))
	(sxml
	 `(core:reply
	   (@ (@ (*NAMESPACES*
		  (,namespace-dav ,namespace-dav-str D)
		  (,namespace-mind ,namespace-mind-str core))))
	   (become . ,become)
	   (core:output
	    (core:success
	     (D:Envelope
	      (@ (@ (*NAMESPACES*
		  (,namespace-dav ,namespace-dav-str D)
		  (,namespace-mind ,namespace-mind-str core))))
	      (D:Header
	       (D:Lock-Token ,locktoken))
	      (D:Body
	       (D:prop
		(@ (@ (*NAMESPACES*
		       (,namespace-dav ,namespace-dav-str D))))
		(D:lockdiscovery (D:activelock . ,(children lockelement))))))))
	   (link (@ (name ,objects))
		 ,(collection-update-tree*
		   lookup old-objects destination
		   resource identity new-attributes 'lock
		   lockelement))))))
     ((eq? (gi request) 'unlock)
      (let ((resource (attribute-string 'resource request)))
	(sxml
	 `(core:reply
	   (@ (@ (*NAMESPACES*
		  (,namespace-dav ,namespace-dav-str D)
		  (,namespace-mind ,namespace-mind-str core))))
	   (become . ,become)
	   (core:output
	    (core:success (status 204)))
	   (link (@ (name ,objects))
		 ,(collection-update-tree*
		   lookup old-objects destination
		   resource identity new-attributes (gi request)
		   request))))))
     ((eq? (gi request) 'propertyupdate)
      (let* ((name (car (take-right destination 1)))
	     (path (drop-right destination 1))
	     (old ((sxpath `(properties (* (@ (equal? (about ,name))))))
		   (fetch-other me (cons objects path)))))
	(sxml
	 `(reply
	   (become . ,become)
	   (output (success (status 201)))
 	   (link (@ (name ,objects))
 		 ,(collection-update-tree*
 		   lookup old-objects path name
 		   identity new-attributes (gi request) request))))))
     ((and-let* ((x (member (data (form-field 'action request))
			    '("copy" "move" "link" "form-link"))))
		(string->symbol (car x)))
      =>
      (lambda (action)
	(receive
	 (path src name)
	 (if (eq? action 'form-link)
	     (values destination
		     (parsed-locator (string-trim-both (data (form-field 'value request))))
		     (string-trim-both (data (form-field 'name request))))
	     (let* ((dst (parsed-locator
			  (data ((sxpath '(destination)) request))))
		    (s0 (parsed-locator (data ((sxpath '(source)) request))))
		    (src (append destination s0)))
	       (let loop ((d dst) (r '()))
		 (cond
		  ((and (pair? d) (pair? (cdr d)))
		   (loop (cdr d) (cons (car d) r)))
		  ((null? d) (raise "empty destination"))
		  (else (values (reverse! r) src (car d)))))))
	 (sxml
	  `(reply
	    (become . ,become)
	    (output (success (status 201)))
	    (link (@ (name ,objects))
		  ,(let ((n1 (collection-update-tree*
			      lookup old-objects
			      (append destination path)
			      name
			      (make-xml-element
			       'id namespace-mind '()
			       (if (and (null? (cdr src)) (string->oid (car src)))
				   src
				   (attribute-string
				    'about
				    ((sxpath '(Description))
				     (me (cons objects src) 'metainfo)))))
			      new-attributes
			      #f #f
			      )))
		     (if (eq? action 'move)
			 (receive
			  (spath sname)
			  (let loop ((d src) (r '()))
			    (cond
			     ((and (pair? d) (pair? (cdr d)))
			      (loop (cdr d) (cons (car d) r)))
			     ((null? d) (raise "collection-update-tree no destination given"))
			     (else (values (reverse! r) (car d)))))
			  (collection-update-tree* lookup n1 spath sname #f new-attributes
						   #f (empty-node-list)))
			 n1))
		  ))))))
     (else (let* ((links ((sxpath '(link)) (msg 'body/parsed-xml)))
		  (name (attribute-string 'name  links)))
	     (if (node-list-empty? links)
		 (error "collection-update-tree unknown request\n ~a" (xml-format request))
		 (sxml
		  `(reply
		    (become . ,become)
		    (output (success (status ,(guard (ex ((object-not-available-condition? ex)
							  201)
							 (else (raise ex)))
						     (and (me objects)
							  (me
							   `(,old-objects ,@destination ,name)
							   'metainfo)
							  204)))))
		    (link (@ (name ,objects))
			  ,(collection-update-tree*
			    lookup old-objects destination name
			    (cond
			     ((not (node-list-empty?
				    (children links)))
			      (children (node-list-first links)))
			     ((equal? (data (form-field 'action request)) "link")
			      (sxml
			       `(link (@ (name ,(data (form-field 'name request))))
				      (id ,(data (form-field 'value request))))))
			     (else #f))
			    new-attributes
			    #f (empty-node-list)))))))))))


;; (for-each
;;  (lambda (symbol) 
;;    (dsssl-export! (eval symbol) symbol))
;;  '(collection-read 
;;    collection-propose 
;;    collection-accept
;;    collection-write 
;;    collection-write-local
;;    custom-collection-read
;;    custom-collection-propose 
;;    custom-collection-write
;;    custom-collection-write-local
;;    ))

;;; imports: 
;; http-format-read-location - mechanism/protocol/http/server.scm
;; xml-document->message     - mechanism/nunu.scm
;; is-meta-form?
;; make-message
;; metaview
;; message-body
;; is-forward-extension?
;; execute-extensions
;; sxml                      - mechanism/tree.scm
;; document-element
;; namespace-mind
;; make-xml-literal          - mechanism/tree-rscheme.scm
;; make-xml-element
;; make-service-level        - mechanism/protection.scm
;; dav-propfind-message      - mechanism/webdav.scm
;; dav:propertyupdate
;; *supported-dav-levels*
;; askemos:dc-date           - mechanism/place.scm
;; string-append
;; string-join
;; string=?
;; respond-timeout-interval  
;; object-not-available-condition? - mechanism/util-rscheme.scm
;; read-locator              - mechanism/util.scm
;; make-html-error           - mechanism/protocol/util.scm
;; xml-digest-simple         - mechanism/notation/xml/render.scm