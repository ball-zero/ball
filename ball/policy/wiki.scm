;; So far I haven't been able to create a lalr(1) grammar for the
;; wikipedia list syntax.  Also I'm not convinced, that it is worth
;; the efford.  At least I need to finish now.  Therefore I'll leave
;; the following change to wikipedia syntax behind here:
;;
;; List nesting: * and # start list imtems; they don't mix and at all
;; (use div instead).
;;
;; pre-Tags are both hadly parsable and ugly in use (since a leading
;; space per line interferes badly with cut&paste operations of source
;; code, a main use of pre-marked sections.  Therefor we enclose them
;; in "<---"-marks.

(define-macro (sxml-expand x) (identity x))

(define (paraexpand x)
  (if (null? x) '() (sxml-expand `(p . ,x))))

(define (paraexpandlast x)
  (if (null? x) '() (list (sxml-expand `(p . ,x)))))

(define wikipedia-parse
  (pcre-lalr-parser
   (("\r+")                             ; drop unwanted chars
    ("\n" newline)
    ("'''''" strong)
    ("'''" em)
    ("''" cite)
    ("<u>" uo)
    ("</u>" uc)
    ("<small>" so)
    ("</small>" sc)
    ("<del>" delo)
    ("</del>" delc)
    ("<br[[:space:]]*/>" br)
    ("====[[:space:]]*([^=]*)[[:space:]]*==*[[:space:]]*\r?\n?" h4)
    ("===[[:space:]]*([^=]*)[[:space:]]*==*[[:space:]]*\r?\n?" h3)
    ("==[[:space:]]*([^=]*)[[:space:]]*==[[:space:]]*\r?\n?" h2)
    ("<@(\\$?[[:alpha:]][[:alnum:]]*)@>" inc)
    ("\\*{[[:space:]]*" ulio)
    ("\\*}[[:space:]]*" ulic)
    ("\\*[[:space:]]*" uli)
    ("#{[[:space:]]*" olio)
    ("#}[[:space:]]*" olic)
    ("#[[:space:]]*" oli)
    ("{\\|[[:space:]]*" tableo)
    ("[[:space:]]*\\|}" tablec)
    ("[[:space:]]*\\|-[[:space:]]*" tr)
    ("[[:space:]]*\\|[[:space:]]*" td)
    ("[[:space:]]*![[:space:]]*" th)
    ("----[[:space:]]*\r?\n" hr)
    (";[[:space:]]+([^:\n]+)" dt)
    (":{[[:space:]]*" ddo)
    (":}" ddc)
    (":[[:space:]]*" dd)
    ("<nowiki>(.*)</nowiki>" nowiki)
    ("<div([[:space:]]+align[[:space:]]*=[[:space:]]*\"(center|left|right)\")?[[:space:]]*>[[:space:]]*\n?"
     divo)
    ("</div>\n?" divc)
    ("\\[\\[([^]|]+)(\\|([^]]+))?\\]\\]" a)
    ("(((https?://)|(www\\.)|(mailto:))[[:alnum:]-.]+(?:[[:digit:]]+)?[^\r\n[:space:]]*)" url)
    ("<---[[:space:]]*\r?\n?" preline)
    ("(.[^[<'\r\n]*)\r?" rawdata)
    )
   ;; --- token definitions --------------------------------------------- ;;
   ;;
   ;; If there's an error like:
   ;; <simple-runtime-error> runtime error: obj basic_or: argument
   ;; 5.36871e+08 not basic in: unknown () <simple-runtime-error>
   ;; the lalr parser does a few 2^n (expt) operations too much.
   ;; There's an inexact->exact converions in lalr.scm now,
   ;; which fixes the problem for now.  But it's ugly.
(expect: 1000)
   (em strong uo uc so sc delo delc br newline preline
       tableo tablec td th tr  hr dt
       h2 h3 h4
       divo divc
       oli
       uli
       dd
       ddo ddc
       nowiki
       olio olic
       ulio ulic
       inc url a rawdata cite
       )
   ;; --- rules --------------------------------------------------------- ;;

    (page (blockelements) : (sxml-expand `((div (@ (align "left")) . ,$1)))
          (blockelements sections)
          : (sxml-expand `((div (@ (align "left")) ,@$1 . ,$2))))

    (sections (h2page) : $1)

    (h2page (h2 blockelements moreh3 moreh2)
            : (cons (sxml-expand `(h2 . ,$1)) (append $2 $3 $4)))
    (moreh2 () : '()
            (h2page) : $1)

    (h3page (h3 blockelements moreh4 moreh3)
            : (cons (sxml-expand `(h3 . ,$1)) (append $2 $3 $4)))
    (moreh3 () : '()
            (h3page) : $1)

    (h4page (h4 blockelements moreh4)
            : (cons (sxml-expand `(h4 . ,$1)) (append $2 $3)))
    (moreh4 () : '()
            (h4page) : $1)

    ;; Block Elements

    (blockelement (pre) : $1
                  (ul) : $1
                  (ol) : $1
                  (dl) : $1
                  (tabled) : $1
                  ;; (includeelement) : $1
                  (div) : $1
                  (hr) : (sxml-expand '(hr))
                  )
    (blockelements (text) : (paraexpandlast $1) 
                   (text newline blockelements) : (cons (paraexpand $1) $3)
                   (text blockelement blockelements)
                   : `(,(paraexpand $1) ,$2 . ,$3)
                   (newline blockelements) : $2
                   (blockelement blockelements) : (cons $1 $2)
                   )

    (textorblocks
     (text) : $1
     (text newline blockelements) : (cons (paraexpand $1) $3)
     (text blockelement blockelements) : `(,(paraexpand $1) ,$2 . ,$3)
     (blockelement blockelements) : (cons $1 $2)
     )
    (pretext (preline) : '()
	     (text pretext) : `(,@$1 "\n" . ,$2)
	     (newline pretext) : `("\n" . ,$2))
    (pre (preline pretext) : (sxml-expand `(pre . ,$2)))

    (div (divo blockelements divc)
         : (sxml-expand `(div (@ (align ,(if (pair? $1) (cadr $1) "center")))
                              . ,$2)))

    (ul (ulitems) : (sxml-expand `(ul . ,$1)))
    (ulitems (uli ulitem moreulitems)
             : (cons (sxml-expand `(li . ,$2)) $3)
             (ulio ulitem ulic moreulitems)
             : (cons (sxml-expand `(li . ,$2)) $4)
             )
    (moreulitems () : '()
		 (ulic) : '()
                 (newline moreulitems) : $2
                 (ulitems) : $1)

    (ulitem (text) : $1
            (text newline itemelements) : (cons (paraexpand $1) $3)
            (text itemelement itemelements) : `(,(paraexpand $1) ,$2 . ,$3)
            )
    (itemelement (pre) : $1
                 (tabled) : $1
                 (div) : $1
                 )
    (itemelements (text) : (paraexpandlast $1)
                  (text newline itemelements)
                  : (cons (paraexpand $1) $3)
                  (text itemelement itemelements)
                  : `(,(paraexpand $1) ,$2 . ,$3)
                  (itemelement itemelements) : (cons $1 $2)
                  (newline itemelements) : $2
                  )
    (ol (olitems) : (sxml-expand `(ol . ,$1)))
    (olitems (oli ulitem moreolitems)
             : (sxml-expand `((li . ,$2) . ,$3))
             (olio ulitem olic moreolitems)
             : (cons (sxml-expand `(li . ,$2)) $4)
             )
    (moreolitems () : '()
		 (olic) : '()
                 (newline moreolitems) : $2
                 (olitems) : $1)

    (dl (dlitems) : (sxml-expand `(dl . ,$1)))

    (dlitems (dt moredlitems) : (cons (sxml-expand `(dt . ,$1)) $2)
             (dd dltextorblocks moredlitems) : (sxml-expand `((dd . ,$2) . ,$3))
             (ddo blockelements ddc moredlitems)
             : (sxml-expand `((dd . ,$2) . ,$4))
             )
    (moredlitems () : '()
                 (dlitems) : $1)
    (dlelement (itemelement) : $1
               (ul dlparagraphs) : (cons $1 $2)
               (ol dlparagraphs) : (cons $1 $2)
               )
    (dlparagraphs (text) : (paraexpandlast $1)
                  (text newline dlparagraphs)
                  : (cons (sxml-expand `(p . ,$1)) $3)
                  (text dlelement dlparagraphs)
                  : (cons (paraexpand $1) (cons $2 $3))
                  )
    (dltextorblocks (text) : $1
;; This doesn't work good.
;;
;;                   (text newline dlparagraphs) : (cons (paraexpand $1) $3)
;;                   (text dlelement dlparagraphs)
;;                   : (cons (paraexpand $1) (cons $2 $3))
                    )
    (tabled (table) : $1)
    (table (tableo tablerows) : (sxml-expand `(table . ,$2)))
    (tablerows (tablerowitems tr tablerows)
               : (cons (sxml-expand `(tr . ,$1)) $3)
               (tablerowitems tablec)
               : (list (sxml-expand `(tr . ,$1)))
               )
    (tablerowitems (td textorblocks moretablerowitems)
                   : (cons (sxml-expand `(td . ,$2)) $3)
                   (th textorblocks moretablerowitems)
                   : (cons (sxml-expand `(th . ,$2)) $3))
    (moretablerowitems () : '()
                       (tablerowitems) : $1)

    ;; There should be a better way than post processing, however I
    ;; can't get the grammer right now.

    (text () : '()
          (textelement newline text)
          : (if (null? $3) (list $1) `(,$1 "\n" . ,$3))
          (textelement inlineelements) : (cons $1 $2)
          (textelement inlineelements newline text)
          : (if (null? $4) (cons $1 $2) `(,$1 ,@$2 "\n" . ,$4))
          )
    (inlineelements () : '()
                   (textelement inlineelements) : (cons $1 $2)
                   (moretext inlineelements) : (cons $1 $2)
                   )
   (moretext (ulio) : "*{"
             (ulic) : "*}"
             (uli) : "*"
             (olio) : "#{"
             (olic) : "#}"
             (oli) : "#"
             (tableo) : "{|"
             (tablec) : "|}"
             (tr) : "|-"
             (td) : "|"
             (th) : "!"
             (ddo) : ":{"
             (ddc) : ":}"
             (dd) : ":"
             (dt) : "; "
             )
   (textelement (anchor) : $1
                (citetext) : $1
                (emtext) : $1
                (strongtext) : $1
                (underline) : $1
                (small) : $1
                (del) : $1
                (linebreak) : $1
                (nowiki) : $1
                (includeelement) : $1
                (rawdata) : (car $1)
                )

   (includeelement (inc) :
                   (if (eqv? (string-ref (car $1) 0) #\$)
                       (make-xml-element
                        'copy-of namespace-xsl
                        (list (make-xml-attribute 'select #f (car $1)))
                        (empty-node-list))
                       (make-xml-element
                        'include 'nu
                        (list (make-xml-attribute 'href #f (car $1))
                              (make-xml-attribute 'xmlns #f "nu"))
                        (empty-node-list))))

   (citetext (cite text-nocite cite) : (sxml-expand `(cite . ,$2))
             (cite text-nocite) : (sxml-expand `(cite . ,$2)))
   (text-nocite () : '()
                (textelement-nocite text-nocite) : (cons $1 $2))

   (textelement-nocite (rawdata) : (car $1)
                       (underline) : $1
                       (small) : $1
                       (del) : $1
                       (linebreak) : $1
                       (nowiki) : $1
                       (anchor) : $1
                       (includeelement) : $1
                       (moretext) : $1
                       )

   (emtext (em text-noem em) : (sxml-expand `(em . ,$2))
           (em text-noem) : (sxml-expand `(em . ,$2)))
   (text-noem () : '()
              (textelement-noem text-noem) : (cons $1 $2))

   (textelement-noem (rawdata) : (car $1)
                     (strongtext) : $1
                     (underline) : $1
                     (small) : $1
                     (del) : $1
                     (linebreak) : $1
                     (nowiki) : $1
                     (anchor) : $1
                     (includeelement) : $1
                     (moretext) : $1
                     )

   (strongtext (strong text-nostrong strong) : (sxml-expand `(strong . ,$2))
               (strong text-nostrong) : (sxml-expand `(strong . ,$2)))
   (text-nostrong () : '()
                  (textelement-nostrong text-nostrong) : (cons $1 $2))

   (textelement-nostrong (rawdata) : (car $1)
                         (emtext) : $1
                         (underline) : $1
                         (small) : $1
                         (del) : $1
                         (linebreak) : $1
                         (nowiki) : $1
                         (anchor) : $1
                         (includeelement) : $1
                         (moretext) : $1
                         )

   (underline (uo text uc) : (sxml-expand `(u . ,$2))
              (uo text) : (sxml-expand `(u . ,$2)))
   (small (so text sc) : (sxml-expand `(small . ,$2))
          (so text) : (sxml-expand `(small . ,$2)))
   (del (delo text delc) : (sxml-expand `(del . ,$2))
        (delo text) : (sxml-expand `(del . ,$2)))
   (linebreak (br) : (sxml-expand `(br)))

   (anchor (a) : (if (and (pair? (cdr $1)) (caddr $1))
                     (sxml-expand `(a (@ (href ,(car $1))) ,(caddr $1)))
                     (sxml-expand `(a (@ (href ,(car $1))) ,(car $1))))
           (url) : (sxml-expand `(a (@ (href  ,(car $1))) ,(car $1)))
           )
   ))

;; One level of indirection to allow reloading files without
;; reexporting.
(define (call-wikipedia-parse str)
  (let ((t0 (wikipedia-parse str)))     ; ?? (normalise-data str #f #f "\n")
    (if (xml-element? t0) t0 (sxml t0))))

(dsssl-export! call-wikipedia-parse 'wikipedia-parse)

(%early-once-only

(define *wikipedia-formats* (make-symbol-table))

(define (wikipedia-format nl rest port)
  (cond
   ((string? nl) (display nl port))
   ((xml-element? nl)
    (let ((handler (and (gi nl) (hash-table-ref/default *wikipedia-formats* (gi nl) #f))))
      (if handler (handler nl rest port)
          (wikipedia-format-all (children nl) port))))))

(define (wikipedia-format-all nl port)
  (if (not (node-list-empty? nl))
      (let loop ((x (node-list-first nl)) (rest (node-list-rest nl)))
        (if (not (node-list-empty? x))
            (begin (wikipedia-format x rest port)
                   (loop (node-list-first rest) (node-list-rest rest)))))))

(define (wikipedia->string text)
  (call-with-output-string (lambda (port) (wikipedia-format-all text port))))

(dsssl-export! (lambda (str) (wikipedia->string str)) 'wikipedia->string)

(register-mime-converter! text/xml "text/wikipedia" call-wikipedia-parse)
(register-mime-converter! "text/wikipedia" text/xml wikipedia->string)

(define (wikipedia-make-format pre post)
  (lambda (nl next port)
    (display pre port)
    (wikipedia-format-all (children nl) port)
    (display post port)))

(define wikipedia-cite (wikipedia-make-format "''" "''"))
 (hash-table-set! *wikipedia-formats* 'cite wikipedia-cite)
 (hash-table-set! *wikipedia-formats* 'it wikipedia-cite)
(define wikipedia-em (wikipedia-make-format "'''" "'''"))
 (hash-table-set! *wikipedia-formats* 'em wikipedia-em)
 (hash-table-set! *wikipedia-formats* 'b wikipedia-em)
(define wikipedia-strong (wikipedia-make-format "'''''" "'''''"))
 (hash-table-set! *wikipedia-formats* 'strong wikipedia-strong)
(define wikipedia-u (wikipedia-make-format "<u>" "</u>"))
 (hash-table-set! *wikipedia-formats* 'u wikipedia-u)
(define wikipedia-small (wikipedia-make-format "<small>" "</small>"))
 (hash-table-set! *wikipedia-formats* 'small wikipedia-small)
(define wikipedia-del (wikipedia-make-format "<del>" "</del>"))
 (hash-table-set! *wikipedia-formats* 'del wikipedia-del)
(define wikipedia-br (wikipedia-make-format "<br/>" ""))
 (hash-table-set! *wikipedia-formats* 'br wikipedia-br)
(define wikipedia-h2 (wikipedia-make-format "== " "==\n"))
 (hash-table-set! *wikipedia-formats* 'h2 wikipedia-h2)
(define wikipedia-h3 (wikipedia-make-format "=== " "===\n"))
 (hash-table-set! *wikipedia-formats* 'h3 wikipedia-h3)
(define wikipedia-h4 (wikipedia-make-format "==== " "====\n"))
 (hash-table-set! *wikipedia-formats* 'h4 wikipedia-h4)
(define wikipedia-uli (wikipedia-make-format "*" "\n"))
(define wikipedia-oli (wikipedia-make-format "#" "\n"))
(define wikipedia-nowiki (wikipedia-make-format "<nowiki>" "</nowiki>"))
 (hash-table-set! *wikipedia-formats* 'nowiki wikipedia-nowiki)
(define wikipedia-hr (wikipedia-make-format "----\n" ""))
 (hash-table-set! *wikipedia-formats* 'hr wikipedia-hr)

(define wikipedia-pre (wikipedia-make-format "<---\n" "<---\n"))
 (hash-table-set! *wikipedia-formats* 'pre wikipedia-pre)

(define (wikipedia-table nl next port)
  (display "{|\n" port)
  (let loop ((tr (let ((tl ((sxpath '(tr)) nl)))
                   (if (node-list-empty? tl)
                       ((sxpath '(* tr)) nl)
                       tl))))
    (wikipedia-format-all (children (node-list-first tr)) port)
    (let ((rest (node-list-rest tr)))
      (if (not (node-list-empty? rest))
          (begin
            (display " |-\n" port)
            (loop rest)))))
  (display " |}\n" port))

(hash-table-set! *wikipedia-formats* 'table wikipedia-table)

(define *wikipedia-noparabreak-elements* (make-symbol-table))

(for-each
 (lambda (el) (hash-table-set! *wikipedia-noparabreak-elements* el #t))
 '(div DIV dl DL ul UL ol OL table TABLE))

(define (wikipedia-p nl rest port)
  (if (node-list-empty? (children nl))
      #f
      (let* ((next (node-list-first rest))
             (noparabreak
              (and (gi next)
                   (hash-table-ref/default *wikipedia-noparabreak-elements* (gi next) #f))))
        (wikipedia-format-all (children nl) port)
        (display (if noparabreak "\n" "\n\n") port))))

(hash-table-set! *wikipedia-formats* 'p wikipedia-p)

(define *nestedlists* '(p pre ul ol dl P PRE UL OL DL))

(define (wikipedia-ul nl next port)
  (let loop ((li (children nl)))
    (if (not (node-list-empty? li))
        (let ((nested (let loop ((k (children (node-list-first li))))
                        (if (node-list-empty? k)
                            #f
                            (or (memq (gi (node-list-first k)) *nestedlists*)
                                (loop (node-list-rest k)))))))
	  (cond
	   (nested (display "*{ " port)
		   (wikipedia-format-all (node-list-first li) port)
		   (display "*}\n" port)
		   (loop (node-list-rest li)))
	   ((and (node-list-empty? (node-list-rest li))
		 (gi next)
		 (not (hash-table-ref/default *wikipedia-noparabreak-elements* (gi next) #f)))
	    (display "* " port)
	    (wikipedia-format-all (node-list-first li) port)
	    (display "\n*}\n" port)
	    (loop (node-list-rest li)))
	   (else (display "* " port)
		 (wikipedia-format-all (node-list-first li) port)
		 (newline port)
		 (loop (node-list-rest li))))))))

(hash-table-set! *wikipedia-formats* 'ul wikipedia-ul)

(define (wikipedia-ol nl next port)
  (let loop ((li (children nl)))
    (if (not (node-list-empty? li))
        (let ((nested (let loop ((k (children (node-list-first li))))
                        (if (node-list-empty? k)
                            #f
                            (or (memq (gi (node-list-first k)) *nestedlists*)
                                (loop (node-list-rest k)))))))
          (cond
	   (nested (display "#{ " port)
		   (wikipedia-format-all (node-list-first li) port)
		   (display "#}\n" port)
		   (loop (node-list-rest li)))
	   ((and (node-list-empty? (node-list-rest li))
		 (gi next)
		 (not (hash-table-ref/default *wikipedia-noparabreak-elements* (gi next) #f)))
	    (display "# " port)
	    (wikipedia-format-all (node-list-first li) port)
	    (display "\n#}\n" port)
	    (loop (node-list-rest li)))
	   (else (display "# " port)
		 (wikipedia-format-all (node-list-first li) port)
		 (newline port)
		 (loop (node-list-rest li))))))))

(hash-table-set! *wikipedia-formats* 'ol wikipedia-ol)

(define *nestedlists-dl* '(ul ol dl DL UL OL))

(define *nesting-elements-dl* '(p pre table div))

(define (wikipedia-dl nl next port)
  (let loop ((li (children nl)))
    (if (not (node-list-empty? li))
        (let ((item (node-list-first li)))
          (if (or (eq? (gi item) 'dt) (eq? (gi item) 'DT))
              (let* ((next (node-list-first (node-list-rest li)))
                     (nested (let loop ((k (children next)))
                               (if (node-list-empty? k)
                                   #f
                                   (or (memq (gi (node-list-first k))
                                             *nestedlists-dl*)
                                       (loop (node-list-rest k)))))))
                (display "; " port)
                (wikipedia-format-all (children item) port)
                (if (or (eq? (gi next) 'dd) (eq? (gi next) 'DD))
                    (if nested
                        (begin
                          (display ":{ " port)
                          (wikipedia-format-all (children next) port)
                          (display ":}\n" port)
                          (loop (node-list-rest (node-list-rest li))))
                        (begin
                          (display ": " port)
                          (wikipedia-format-all (children next) port)
                          (newline port)
                          (loop (node-list-rest (node-list-rest li)))))
                    (begin
                          (display ":\n" port)
                          (loop (node-list-rest li)))))
              (let ((nested (let loop ((k (children item)))
                              (if (node-list-empty? k)
                                  #f
                                  (or (memq (gi (node-list-first k))
                                            *nestedlists-dl*)
                                      (loop (node-list-rest k)))))))
                (if (or nested
                        (and (node-list-empty? (node-list-rest li))
                             (memq (gi (node-list-first next))
                                   *nesting-elements-dl*)))
                    (begin
                      (display ":{ " port)
                      (wikipedia-format-all (children item) port)
                      (display "\n:}\n" port))
                    (begin
                      (display ": " port)
                      (wikipedia-format-all (children item) port)
                      (newline port)))
                (loop (node-list-rest li))))))))

(hash-table-set! *wikipedia-formats* 'dl wikipedia-dl)

(define wikipedia-tr (wikipedia-make-format " |" "\n"))
 (hash-table-set! *wikipedia-formats* 'tr wikipedia-tr)
(define wikipedia-td (wikipedia-make-format " |" "\n"))
 (hash-table-set! *wikipedia-formats* 'td wikipedia-td)
(define wikipedia-th (wikipedia-make-format " !" "\n"))
 (hash-table-set! *wikipedia-formats* 'th wikipedia-th)

(define (wikipedia-div nl next port)
  (format port "<div align=\"~a\">\n" (or (attribute-string 'align nl)
                                          "center"))
  (wikipedia-format-all (children nl) port)
  (display "</div>\n" port))

(hash-table-set! *wikipedia-formats* 'div wikipedia-div)

(define (wikipedia-a nl next port)
  (let ((href (or (attribute-string 'href nl) ""))
        (content (data nl)))
    (if (or (string=? content "") (string=? content href))
        (format port "[[~a]]" href)
        (format port "[[~a|~a]]" href content))))

(hash-table-set! *wikipedia-formats* 'a wikipedia-a)

(define (wikipedia-include nl next port)
  (format port "<@~a@>" (attribute-string (case (gi nl)
                                            ((copy-of) 'select)
                                            (else 'href))
                                          nl)))

(hash-table-set! *wikipedia-formats* 'include wikipedia-include)
(hash-table-set! *wikipedia-formats* 'copy-of wikipedia-include)

) ; %early-once-only
