;;; (C) 2011 JFW

;;; WARNING
;;;
;;; XMLRPC is a seriously flawed standard.  (Both HTTP wise, because
;;; it mandates the POST method even for idempotent calls and XML wise
;;; wrt. to XML and even SGML encoding practise.)
;;;
;;; The use of XMLRPC is strongly discurraged.
;;;
;;; This code shall never make it into the "mechanism" section!  It
;;; was only written because I need to control rtorrent.

(cond-expand
 (chicken (define http-property-post (property 'type 'post)))
 (else (begin)))
(define http-property-content-xml (property 'content-type text/xml))
;;
(define-condition-type &xmlrpc-condition &message
  xmlrpc-condition?
  (code xmlrpc-condition-code))

(define (raise-xmlrpc-error code msg . more)
  (raise
   (make-condition
    &xmlrpc-condition
    'message (format "~a ~a" msg more)
    'code code)))

;;

(define (xmlrpc-parse-number e)
  (string->number (data e)))

(define (xmlrpc-parse-number-boolean e)
  (not (equal? (string->number (data e)) 0)))

(define (xmlrpc-parse-iso8601 e)
  (srfi19:string->date (data e) iso-8601-time-string-format))

(define (xmlrpc-parse-array e)
  (list->vector
   (map xmlrpc-element-parse ((sxpath '(value *)) e))))

(define (xmlrpc-parse-struct e)
  (let ((result (make-mind-links)))
    (node-list-map
     (lambda (e)
       (let ((name (data (select-elements (children e) 'name))))
	 (if (string-null? name)
	     (raise-xmlrpc-error 0 "Malformed struct member\n" (xml-format e))
	     (link-bind!
	      result name
	      (xmlrpc-element-parse (children (select-elements (children e) 'value)))))))
     (select-elements e 'member))
    result))

(define (xmlrpc-element-parse xml-element)
  ((case (gi xml-element)
     ((i4 int double) xmlrpc-parse-number)
     ((boolean) xmlrpc-parse-number-boolean)
     ((string) data)
     ((base64) pem-decode)
     ((dateTime.iso8601) xmlrpc-parse-iso8601)
     ((array) xmlrpc-parse-array)
     ((struct) xmlrpc-parse-struct)
     (else (raise-xmlrpc-error -1 "unsupported encoding" (gi xml-element))))
   (children xml-element)))

;;-

(define (xmlrpc-dict? obj)		; originally not exported for a reason
  (guard
   (ex (else #f))
   (link-ref obj "")
   #t))

(define (xmlrpc-encode obj)
  (make-xml-element
   'param #f '()
   (sxml
    `(value
      ,(cond
	((vector? obj)
	 `(array . ,(vector-fold-right
		     (lambda (idx init e) (cons (xmlrpc-encode e) init))
		     '() obj)))
	((pair? obj) `(array . ,(map xmlrpc-encode obj)))
	((number? obj)
	 (if (exact? obj) `(i4 ,(number->string obj)) `(double ,(number->string obj))))
	((boolean? obj) `(boolean ,(if obj "1" "0")))
	((string? obj) `(string ,obj))
	((symbol? obj) `(string ,(literal obj)))
	((a:blob? obj) `(base64 ,(pem-encode obj)))
	((xmlrpc-dict? obj)
	 `(struct . ,(fold-links
		      (lambda (k e init) (cons (xmlrpc-encode e) init))
		      '() obj)))
	(else (raise-xmlrpc-error -1 "unsupported data type" obj)))))))

;;

(define xmlrpc-http-headers
  '(content-length content-type))

(define-record-type <xmlrpc-connection>
  (%make-xmlrpc-connection protocol host path)
  xmlrpc-connection?
  (protocol xmlrpc-connection-protocol)
  (host xmlrpc-connection-host)
  (path xmlrpc-connection-path))

(define (split-target name)
  (let ((colon (string-index name #\:)))
    (values (if colon (substring name 0 colon) name)
            (if colon
                (string->number
                 (substring name (add1 colon) (string-length name)))
                80))))

(define (make-xmlrpc-connection protocol url)
  (let ((u (uri url)))
    (%make-xmlrpc-connection
     (case protocol
       ((#f http)
	(lambda (xml)
	  (parameterize
	   ((*http-transfered-headers* xmlrpc-http-headers))
	   (http-response
	    (make-message
	     http-property-post
	     http-property-content-xml
	     (property 'host (uri-authority u))
	     (property 'dc-identifier (uri-path u))
	     (property 'body/parsed-xml xml))))))
       ((scgi)
	(receive
	 (host port) (split-target (or (uri-authority u) url))
	 (lambda (xml)
	   (scgi-response
	    host port
	    (make-message
	     http-property-post
	     http-property-content-xml
	     (property 'body/parsed-xml xml)))))))
     (uri-authority u) (uri-path u))))

(define (xmlrpc-parse-response msg)
  (let* ((ans (select-elements (message-body msg) 'methodResponse))
	 (fault (select-elements (children ans) 'fault)))
    (if (node-list-empty? ans)
	(raise-xmlrpc-error 0 "Malformed response\n" (message-body/plain msg))
	(if (node-list-empty? fault)
	    (apply
	     values
	     (map xmlrpc-element-parse ((sxpath '(params param value *)) ans)))
	    (let ((val (xmlrpc-element-parse ((sxpath '(value *)) fault))))
	      (raise-xmlrpc-error (link-ref val "faultCode")
				  (link-ref val "faultString")))))))

(define (xmlrpc-respond xmlrpc-connection xml)
  (xmlrpc-parse-response
   ((xmlrpc-connection-protocol xmlrpc-connection) xml)))

(define (xmlrpc-method xmlrpc-connection method-name)
  (let ((method-name (literal method-name)))
    (lambda args
      (xmlrpc-respond
       xmlrpc-connection
       (sxml
	(if (null? args) `(methodCall (methodName ,method-name))
	    `(methodCall
	      (methodName ,method-name)
	      (params . ,(map xmlrpc-encode args)))))))))
