;; This is all subject ot change.  I just need something half way working
;; *right now*.

(define $trustedcode-oid #f)
(define $clock-oid #f)
(define $cert-registry #f)

(define $clock-tick 60)

(define control-password-hash #f)

(define (make-local-secret-salt)
  (md5-digest (literal (current-date))))

(define (check-control-password word)
  (secret-verify control-password-hash word))
(define (set-control-password! word digest)
  (set! control-password-hash
	(and (not (string=? word ""))
	     (if digest (secret-encode word (make-local-secret-salt)) word))))

(define $server-listen-address (make-config-parameter #f))

(define (set-server-listen-address! addr)
  ($server-listen-address addr))

(define (numeric-of x dflt)
  (cond
   ((number? x) x)
   ((string? x) (or (string->number x) dflt))
   (else dflt)))

(define $control-port (make-config-parameter 7070))

(define (set-control-port! p)
  (let* ((o ($control-port))
	 (v (numeric-of p o)))
    ($control-port v)
    (not (eqv? v o))))

(define $http-server-port (make-config-parameter 7080))

(define (set-http-server-port! p)
  (let* ((o ($http-server-port))
	 (v (numeric-of p o)))
    ($http-server-port v)
    (not (eqv? v o))))

(define $https-server-port (make-config-parameter 7443))

(define (set-https-server-port! p)
  (let* ((o ($https-server-port))
	 (v (numeric-of p o)))
    ($https-server-port v)
    (not (eqv? v o))))

(define $lmtp-server-port (make-config-parameter 7024))

(define (set-lmtp-server-port! p)
  (let* ((o $lmtp-server-port)
	 (v (numeric-of p o)))
    ($lmtp-server-port v)
    (not (eqv? v o))))

(define signal-clock
  (let ((next 0))
    (lambda (now)
      (and (>= (time-second now) next)
	   (begin
	     ;; After system suspension we rather "restart".
	     (if (> (/ (- (time-second now) next) $clock-tick) 3)
		 (pool-reset!))

	     (set! next (* $clock-tick
			   (+ 1 (quotient (time-second now) $clock-tick))))

	     (if $clock-oid
		 (guard
		  (exception (else #f))
		  (let ((now-date (time-utc->date now (timezone-offset)))
			(clock (or (find-frames-by-id $clock-oid)
				   (error "signal-clock: clock not found"))))
		    ;; FIXME: once the respond! handling is mature
		    ;; enough to compe with the situation, do no longer
		    ;; drop the last transaction, but try to complete
		    ;; it.  Do not do so for now, since it could hang
		    ;; forever, since there only a small chance to
		    ;; complete it.
		    (call-subject!
		     clock
		     (replicates-of clock)
		     'write
		     (make-message
		      (property 'body/parsed-xml
				(make-xml-element
				 'tick namespace-mind (empty-node-list)
				 (rfc-822-timestring now-date)))
		      (property 'content-type text/xml)
		      (property 'dc-creator (public-oid))
		      (property 'dc-date now-date)
		      (property 'caller (public-oid))
		      (property 'capabilities
				(make-a-transient-copy-of-object
				 (public-capabilities))))))))
	     (if (and
		  (not (file-exists? (tc-private-cert-req-key-file)))
		  (file-exists? (tc-private-cert-file))
		  (let ((d (tc-private-end-date)))
		    (or (not d)
			(< (time-second (time-difference
					 (date->time-utc d)
					 *system-time*))
			   (* 86400 ($tc-cert-processing-days))))))
		 (ball-send-new-certificate-request!))
	     (and ($dns-zone) (virtual-host-sync-to-dns!))
	     ;; End of per-turn actions
	     ))
      (guard (ex (else (log-condition 'alive-check ex) (raise ex)))
	     (and-let* ((public (public-oid)) (here ((host-lookup) public))
			(dead
			 (cache-fold
			  *http-routeable-hosts*
			  (lambda (k entry i)
			    (let ((h ((host-lookup) (host-id entry))))
			      (if (and
				   (not (equal? h here))
				   (let ((a ((host-alive?) h)))
				     (or (not a)
					 (> (time-second (time-difference a now)) 900))))
				  (cons* (add1 (car i)) k (cdr i))
				  i)))
			  (list 0))))
	       (do ((n (- (cache-size *http-routeable-hosts*)
			  (car dead))
		       (sub1 n))
		    (dead (cdr dead) (cdr dead)))
		   ((or (null? dead) (< n ($host-map-size))))
		 (http-host-remove! (car dead)))
	       (gc-mind *the-registered-stores*)))
      (guard
       (ex (else (log-condition 'reconnn ex) (raise ex)))
       (askemos-reconnect-all))
      (if (config-save-reqested?)
	  (begin (ball-save-config) (ball-config-saved!)))
      (flush-log-queue!)
      )))

(define (make-integer-input name value min max step)
  (if step
      (error "make-integer-input steps NYI")
      (make-xml-element
       'input #f
       (list (make-xml-attribute 'name #f (literal name))
	     (make-xml-attribute 'size #f "5")
	     (make-xml-attribute 'type #f "text")
	     (make-xml-attribute 'value #f (literal value)))
       (empty-node-list))))

(define (make-boolean-input name value)
  (make-xml-element
   'input #f
   (cons* (make-xml-attribute 'name #f (literal name))
	  (make-xml-attribute 'type #f "checkbox")
	  (make-xml-attribute 'value #f "on")
	  (if value (list (make-xml-attribute 'checked #f "checked")) '()))
   (empty-node-list)))

(define (make-string-input name value size)
  (make-xml-element
   'input #f
   (list (make-xml-attribute 'name #f (literal name))
	 (make-xml-attribute 'size #f (literal size))
	 (make-xml-attribute 'type #f "text")
	 (make-xml-attribute 'value #f (or value "")))
   (empty-node-list)))

(define (make-select-input name value options . args)
  (make-xml-element
   'select #f (list (make-xml-attribute 'name #f (literal name)))
   (map
    (lambda (l)
      (make-xml-element
       'option #f
       (cons (make-xml-attribute 'value #f (cadr l))
	     (if (eq? (car l) value)
		 (list (make-xml-attribute 'selected #f "selected"))
		 '()))
       (node-list (literal (caddr l)))))
    options)))

;; (define (make-password-input name value size)
;;   (make-xml-element
;;    'input #f
;;    (list (make-xml-attribute 'name #f (literal name))
;; 	 (make-xml-attribute 'size #f (literal size))
;; 	 (make-xml-attribute 'type #f "password")
;; 	 (make-xml-attribute 'value #f value))
;;    (empty-node-list)))


(define *ball-user-cert-space* (make-oid-table))

(define (ball-user-cert user)
  (hash-table-ref
   *ball-user-cert-space* user
   (lambda ()
     (let ((e (guard
	       (ex (else '#("" "" "")))
	       (vector
		""
;; 		(let ((f (tc-private-cert-req-key-file)))
;; 		  (if (file-exists? f) (filedata f) ""))
		(let ((f (tc-private-cert-req-file)))
		  (if (file-exists? f) (filedata f) ""))
		""))))
       (hash-table-set! *ball-user-cert-space* user e)
       e))))

(define (ball-user-cert-xml user)
  (let ((entry (or (hash-table-ref/default *ball-user-cert-space* user #f)
		   (guard
		    (ex (else '#("" "" "")))
		    (vector
		     (let ((f (tc-private-cert-req-key-file)))
		       (if (file-exists? f) (filedata f) ""))
		     (let ((f (tc-private-cert-req-file)))
		       (if (file-exists? f) (filedata f) ""))
		     "")))))
    (if entry
	(sxml `(user-cert-data
		(@ (name ,(oid->string user)))
		,@(if (vector-ref entry 0) `((key ,(vector-ref entry 0))) '())
		,@(if (vector-ref entry 1) `((req ,(vector-ref entry 1))) '())
		,@(if (vector-ref entry 2) `((cert ,(vector-ref entry 2))) '())))
	(sxml '(user-cert-data)))))

;; TODO this one needs to be changed.

(define bocntrl-sync-on-load sync-on-load)

(define ball-kernel-info-handlers
  `(("systemtime"
     ,(lambda args
	(sxml
	 `(systemtime ,(rfc-822-timestring (time-utc->date *system-time* (timezone-offset)))))))
    ("local-id" ,(lambda args (sxml `(local-id ,(local-id)))))
    ("channels" ,(lambda args
		   (make-xml-element
		    'channels #f '()
		    (let ((links (fget (find-local-frame-by-id (my-oid) 'ball-kernel-info-handlers:channels) 'mind-links)))
		      (table->rdf-bag-sorted links)))))
    ("cacert" ,(lambda args
		 (receive (cacert cert key) (tc-certification-for (local-id))
			  (sxml (cons 'cacert (if cacert (list (filedata cacert)) '()))))))
    ("cert" ,(lambda args
		 (receive (cacert cert key) (tc-certification-for (local-id))
			  (sxml (cons 'cert (if cert (list (filedata cert)) '()))))))
    ("user-x509" ,ball-user-cert-xml)
    ("hosts"
     ,(lambda args
	(sxml `(hosts . ,(map (lambda (x) (list 'host (literal x))) ($cfg-voters))))))
    ("known-hosts" ,(lambda args (sxml (list 'known-hosts (http-display-hosts)))))
    ("pstore-cache-size"
     ,(lambda args (sxml `(p "# of places in cache: "
			     ,(pool-fold! 1 (lambda (k v i) (add1 i)) 0)))))
    ("kernelthreads"
     ,(lambda args
	(sxml `(kernelthreads ,(with-output-to-string (lambda () (thread-list 'dummy)))))))
    ("timeeventqueue"
     ,(lambda args (sxml (display-stop-list))))
    ("httpchannels" ,(lambda args (sxml `(http-channels ,(display-http-channels)))))
    ("dnszone" ,(lambda args
		   (sxml (list 'dnszone
			       (call-with-output-string
				(lambda (port) (write-host-map-as-zone-file
						*http-routeable-hosts* port)))))))
    ("limits"
     ,(lambda args
	(sxml
	 `(limits
	   (table
	    (tr (td "Local ID")
		(td ,(make-string-input 'new-local-id (local-id) 35)))
;; 	    (tr (td "SSL directory")
;; 		(td ,(make-string-input 'ssl-directory (ssl-directory) 40)))
	    (tr (td "Administrator")
		(td ,(make-string-input 'administrator $administrator 35)))
	    (tr (td "Domain")
		(td ,(make-string-input 'http-domain (the-http-domain) 35)))
	    (tr (td "External Route")
		(td ,(make-string-input 'routing-info ($external-address) 35)))
	    (tr (td "SOCKS4a server")
		(td ,(make-select-input
		      'socks4a-mode (let ((x ($https-use-socks4a)))
				      (cond
				       ((x "askemos.org") #t)
				       ((x ".onion") 'maybe)
				       ((x ".i2p") 'maybe)
				       (else #f)))
		      '((#t "yes" "always")
			(#f "no" "never")
			(maybe "maybe" "when required")))
		    ,(make-string-input
		      'socks4a-server (or ($https-socks4a-server) "") 25)))
	    (tr (td "SMTP smart host")
		(td ,(make-string-input
		      'smtp-smart-host (or ($smtp-host) "") 35)))
;; 	    (tr (td "Administrator Secret")
;; 		(td ,(make-password-input 'administrator-password "" 20)))
;; 	    (tr (td "Operator Secret")
;; 		(td ,(make-password-input 'operator-password "" 20)))
	    (tr (td "Sync on Load")
		(td ,(make-boolean-input 'sync-on-load (sync-on-load))
		    ;; FIXME: the whole crap ought to work without this switch
		    "leave this ON if you are not a scientist at this time"))
	    (tr (td "Defer Sync")
		(td ,(make-integer-input 'defer-sync-messages ($defer-sync-replies-to-stable-state) 0 3 #f)
		    "[0..3] LSB: defer state, 2nd Bit: defer resync"))
	    (tr (td "Client Mode")
		(td ,(make-boolean-input 'client-mode (ball-client-mode))))
	    (tr (td "Mandatory Capabilities")
		(td ,(make-boolean-input 'mandatory-capabilities
					 $pishing-prevention)))
	    (tr (td "Clock OID")
		(td ,(make-string-input 'clock-oid
					(if $clock-oid (literal $clock-oid) "")
					35)))
	    (tr (td "CA mode")
		(td ,(let ((mode (cond
				  (($ca-slave) 'slave)
				  (($tc-tofu) 'tofu)
				  (else 'fixed))))
		       (make-select-input
			'trust-mode mode
			'((slave "slave" "slave")
			  (tofu "tofu" "TOFU")
			  (fixed "fixed" "fixed"))))))
	    (tr (td "Cert Registry")
		(td ,(make-string-input 'registry-oid
					(if $cert-registry (literal $cert-registry) "")
					35)))
	    (tr (td "Trusted Code OID")
		(td ,(make-string-input 'trusted-code
					(if $trustedcode-oid
					    (literal $trustedcode-oid) "")
					35)))
	    (tr (td "Control Port")
		(td ,(make-integer-input 'control-port ($control-port) 25 10000 #f)))
	    (tr (td "HTTP Port")
		(td ,(make-integer-input 'http-port ($http-server-port) 1 10000 #f)))
	    (tr (td "HTTPS Port")
		(td ,(make-integer-input 'https-port ($https-server-port) 1 10000 #f)))
	    (tr (td "External HTTPS Port")
		(td ,(make-integer-input 'external-port ($external-port) 1 10000 #f)))
	    (tr (td "LMTP Port")
		(td ,(make-integer-input 'lmtp-port ($lmtp-server-port) 25 10000 #f)))
	    (tr (td "response timeout")
		(td ,(make-integer-input
		      "response"
		      (timeout-second
		       (respond-timeout-interval)) #f #f #f)))
	    (tr (td "remote timeout")
		(td ,(make-integer-input
		      "remote"
		      (timeout-second
		       (remote-fetch-timeout-interval)) #f #f #f)))
	    (tr (td "broadcast timeout")
		(td ,(make-integer-input "broadcast" ($broadcast-timeout) #f #f #f)))
	    (tr (td "broadcast retries")
		(td ,(make-integer-input "retries" ($broadcast-retries) #f #f #f)))
	    (tr (td "max. conn. to host")
		(td ,(make-integer-input "max-client-connections"
					 ($http-client-connections-maximum) #f #f #f)))
	    (tr (td "max. incomming conn.")
		(td ,(make-integer-input "max-server-connections"
					 (parallel-requests) #f #f #f)))
	    (tr (td "requests per HTTP connection")
		(td ,(make-integer-input
		      "requests-per-connection"
		      ($http-requests-per-connection) #f #f #f)))
	    (tr (td "requests per HTTPS connection")
		(td ,(make-integer-input
		      "requests-per-ssl-connection"
		      ($https-requests-per-connection) #f #f #f)))
	    ;; 		   (tr (td "require ssl cert")
	    ;; 		       (td ,(make-boolean-input "require-cert" $https-server-require-cert)))
	    (tr (td "SSL keep alive time (low level)")
		(td ,(make-integer-input
		      "ssl-keep-alive"
		      (ssl-keep-alive-timeout) #f #f #f)))
	    (tr (td "min fsm delay")
		(td ,(make-integer-input
		      "minimum-delay-between-fsgc"
		      (fsm-delay) #f #f #f)))
	    (tr (td (@ (title "checksum blobs upon load from local store (normally off)"))
		    "paranoid checksums")
		(td ,(make-boolean-input "local-blob-paranoia" ($fsm-blob-paranoia))))
	    (tr (td (@ (title "check OID signature for immutables"))
		    "paranoid immutables")
		(td ,(make-boolean-input "immutable-paranoia" ($enforce-frame-oid-match))))
	    (tr (td "log about")
		(td "agreement"
		    ,(make-boolean-input 'log-agreement (agree-verbose))
		    "net"
		    ,(make-boolean-input 'log-network ($topology-verbose))
		    "client"
		    ,(make-boolean-input 'log-client ($https-client-verbose))
		    "server"
		    ,(make-boolean-input 'log-server ($http-server-verbose))))
	    )))))
    ))

(define (ball-kernel-info creator)
  (lambda (key)
    (and-let* ((entry (assoc key ball-kernel-info-handlers)))
	      ((cadr entry) creator))))

(define (ball-info args)
  (map (ball-kernel-info (car args)) (cdr args)))

(define (find-first-data-field key node)
  (and-let* ((fl (filter
		  (lambda (x) (not (string=? x "")))
		  (node-list-map
		   (lambda (x)
		     (normalise-data (data x) "UTF-8" "UTF-8" "\n"))
		   (form-field key node))))
	     ((pair? fl)))
	    (car fl)))

(define x509-certificate-validity-period (make-shared-parameter 30))

(define (ball-send-certificate-request! key req)
  (guard
   (ex (else (logerr "FAILED TO STORE CERTIFICATION REQUEST ~a\n" ex) (raise ex)))
   (tc-store-host-cert-req-key! key)
   (tc-store-host-cert-req! req))
  (guard
   (ex (else (logerr "Failed to send certification request ~a\n" ex)))
   (http-send-certificate-request 'A9f7ce54ad83cb8ca6bd8e6e1381a3acc req))
  (send-email $administrator req
	      '((From (string-append $administrator " - " (local-id)))
		(Subject "certificate request")))
  (logerr "timed certification request mailed request to ~a\n"
	  $administrator))

(define (ball-prepare-new-certificate-request send)
  (guard
   (ex (else (log-condition "Error preparing new cert request" ex)))
   (and-let*
    ((cert (tc-private-cert))
     (subject (tc-private-subject))
     (key (tc-genkey #f))
     (cn0 (or
	  ((pcre/a->proc-uncached ".*CN=([^/,]*)")
	   subject)
	  (raise (format "No CN in current subject ~a\n" subject))))
     (cn (cadr cn0))
     (client (mesh-cert-l cert)))
    (let* ((user (mesh-cert-o cert))
	   (req (tc-make-certificate-request
		 key
		 subject: `(("CN" ,cn)
			    ("L" ,(literal client))
			    . ,(if user
				   `(("O" ,(literal user)))
				   '()))
		 subjectAltName: cn
		 days: (literal (x509-certificate-validity-period)))))
      (send key req)))))

(define (ball-send-new-certificate-request!)
  (ball-prepare-new-certificate-request ball-send-certificate-request!))

(define (ball-prepare-certificate-request send)
  (guard
   (ex (else (logerr "Error preparing cert request ~a\n" ex)))
   (and-let*
    ((cert (tc-private-cert))
     (subject (tc-private-subject))
     (key (filedata (tc-private-key-file)))
     (cn (or
	  ((pcre/a->proc-uncached ".*CN=([^/,]*)")
	   subject)
	  (raise (format "No CN in current subject ~a\n" subject))))
     (client (mesh-cert-l cert)))
    (let* ((user (mesh-cert-o cert))
	   (req (let ((cn (cadr cn)))
		  (tc-make-certificate-request
		   key
		   subject: `(("CN" ,cn)
			      ("L" ,(literal client))
			      . ,(if user
				     `(("O" ,(oid->string user)))
				     '()))
		   subjectAltName: cn
		   days: (literal (x509-certificate-validity-period))))))
      (send key (sxml
		 `(certificate-request
		   (l ,client)
		   ,@(if user
			 (list 'o (oid->string user))
			 '())
		   (x509 ,req))))))))

(define (ball-store-certificate-request! key req)
  (guard
   (ex (else (logerr "Failed to register certification request ~a\n" ex)))
   (let* ((registry (or (find-frames-by-id $cert-registry)
			(error "cert registry found")))
	  (result (call-subject!
		   registry
		   (replicates-of registry)
		   'write
		   (make-message
		    (property 'body/parsed-xml req)
		    (property 'content-type text/xml)
		    (property 'dc-creator (public-oid))
		    (property 'dc-date (time-utc->date *system-time* (timezone-offset)))
		    (property 'caller (public-oid))
		    (property 'capabilities
			      (make-a-transient-copy-of-object
			       (public-capabilities)))))))
     (if (not (eqv? (get-slot result 'http-status) 200))
	 (raise-service-unavailable-condition
	  (format "Error registering cert request to ~a status ~a"
		  $cert-registry (get-slot result 'http-status)))))))

(define (ball-publish-certificate-request!)
  (ball-prepare-certificate-request ball-store-certificate-request!))

(define (ball-manage-user-cert node)
  (let* ((opcode (data (form-field 'op node)))
	 (user (string->oid (data (form-field 'user node))))
	 (entry (ball-user-cert user)))
    (cond
     ((equal? opcode "new")
      (let* ((nick (data (form-field 'name node)))
	     (client (data (form-field 'client node)))
	     (pass (data (form-field 'password node)))
	     (kpass (and (not (equal? pass "")) pass))
	     (cn (data (form-field 'cn node)))
	     (user (and-let* ((oid (string->oid nick))) (find-frames-by-id oid)))
	     ;; (metainfo (call-subject! user #f 'read (force public-read-metainfo-request)))
	     (num (if user (car (fget user 'version)) (time-second *system-time*)))
	     (days (or (string->number (data (form-field 'days node))) 30))
	     (key (tc-genkey kpass))
	     (req (tc-certificate-request
		   key kpass cn: cn o: nick
		   l: (if (string=? client "") (literal (public-oid)) client)
		   days: days)))
	(vector-set! entry 0 key)
	(vector-set! entry 1 req)))
     ((equal? opcode "store")
      (vector-set! entry 1 (find-first-data-field 'cert node)))
     ((equal? opcode "sign")
      (and (vector-ref entry 1)
	   (let* ((num (or (string->number (data (form-field 'number node)))
			   (error "serial number missing")))
		  (days (or (string->number (data (form-field 'days node))) 30))
		  (ca (not (node-list-empty? (form-field 'enable-ca node))))
		  (cert (x509-sign (vector-ref entry 1)
				   num
				   (filedata (tc-ca-key-file))
				   (data (form-field 'password node))      
				   ca: (tc-ca-cert-file) days: days
				   usage: (if ca '(CA) '()))))
	     (vector-set! entry 2 cert))))
     ((equal? opcode "store-as-host")
      (if (and ($bopctrl-require-host-master)
	       (not (check-control-password (data (form-field 'password node)))))
	  (raise (make-condition &unauthorised 'status 401
				 'message (format "Host Administrator Password ~a ?"
						  (data (form-field 'password node)))
				 'resource "host certificate"
				 'source "certificate management"))
	  #t)
      (guard
       (ex (else #f))
       (let ((cert (vector-ref entry 2))
	     (req (vector-ref entry 1))
	     (key (let ((key (vector-ref entry 0)))
		    (if (string=? key "")
			(let ((file (tc-private-cert-req-key-file)))
			  (if (file-exists? file) (filedata file) ""))
			key))))
	 (case (cond ((and (string=? cert "")
			   (not (string=? req ""))
			   (not (string=? key "")))
		      'req)
		     ((and (not (string=? cert ""))
			   (not (string=? key "")))
		      'cert)
		     ((string=? key "") (raise "no key"))
		     (else (raise "no cert")))
	   ((req)
	    (tc-store-host-cert-req-key! key)
	    (tc-store-host-cert-req! req))
	   ((cert)
	    (tc-store-host-cert! cert)
	    (tc-store-host-key! key)
	    (let ((file (tc-private-cert-req-key-file)))
	      (if (file-exists? file) (remove-file file))))))))
     ((equal? opcode "delete")
      (hash-table-delete! *ball-user-cert-space* user))
     (else (error "manage-user-cert unknown op-code ~a" opcode)))))

;; Config file

;; until there are config entries for all timeout values we use this
;; to update dependant parameter
(define (remote-fetch-timeout-kept-here-for-the-time-beeing val)
  (let ((val (timeout-second val)))
    (remote-fetch-timeout-interval val)  ; mechanism/timeout.scm
    (fsm-remote-timeout val)             ; mechanism/storage/fsm.scm
    (write-plain-timeout val)            ; mechanism/protocol/util.scm
    (http-read-status-line-timeout val)  ; mechanism/protocol/util.scm
    ))

(define (respond-timeout-kept-here-for-the-time-beeing val)
  (let ((val (timeout-second val)))
    (respond-timeout-interval val)	; mechanism/timeout.scm
    (call-subject-remote-answer-timeout	; mechanism/methods.scm
     (and val (* 2 val)))
    (http-read-to-end-timeout val)	; mechanism/protocol/util.scm
    (http-eval-respond-timeout val)	; mechanism/protocol/util.scm
    (http-with-channel-timeout val)	; mechanism/protocol/util.scm
    ))

(define (update-dependent-parameters)
  (let ((n ($broadcast-retries))
	(val (or ($broadcast-timeout) 1))
	(st (+ 6 ;; (ssl-setup-timeout)
	     (or (respond-timeout-interval) 1))))
    ($complete-timeout (+ st (* n val)))))

(define ball-client-mode
  (lambda v
    (if (pair? v)
	(let ((how (car v)))
	  ($http-is-synchronous how)
	  (global-proxy-mode how)))
    (global-proxy-mode)))

(define (boolean-on/off v) (if v "on" "off"))

(define (boolean-sxpath node path)
  (equal? (data ((sxpath path) node)) "on"))

(define (ball-configuration->xml)
  (sxml
   `(configuration
;;      (paths
;;       (ssl-directory ,(ssl-directory)))
     ,@(if $trustedcode-oid `((trusted-code ,(literal $trustedcode-oid))) '())
     (agreement
      (sync-on-load ,(boolean-on/off (sync-on-load)))
      (defer-sync-messages ,(literal ($defer-sync-replies-to-stable-state)))
      (mandatory-capabilities ,(boolean-on/off $pishing-prevention))
      ,@(if $clock-oid `((clock ,(literal $clock-oid))) '())
      ,@(if $cert-registry `((registry ,(literal $cert-registry))) '())
      (response ,(timeout-second (respond-timeout-interval)))
      (remote ,(timeout-second (remote-fetch-timeout-interval)))
      (broadcast ,($broadcast-timeout))
      (broadcast-retries ,($broadcast-retries))
      (log (agreement ,(boolean-on/off $agree-verbose))
	   (topology ,(boolean-on/off ($topology-verbose)))
	   (client ,(boolean-on/off ($https-client-verbose)))
	   (server ,(boolean-on/off ($http-server-verbose)))))
     (restrictions
      (paranoia
       (local-blob ,(boolean-on/off ($fsm-blob-paranoia)))
       (immutables ,(boolean-on/off ($enforce-frame-oid-match)))))
     (storage
      (gc-delay ,(fsm-delay)))
     (host
      ,@(if (equal? (local-id) "localhost")
	    '()
	    `((local-id ,(local-id))))
      ,@(if administrator-password-hash
	    `((administrator
	       (email ,$administrator)
	       (password-md5s6 ,administrator-password-hash))) '())
      ,@(if control-password-hash
	    `((operator
	       (password-md5s6 ,control-password-hash))) '())
      (require-certificates ,(boolean-on/off ($https-server-require-cert)))
      (trust
       (mode ,(if ($ca-slave) "slave" (if ($tc-tofu) "tofu" "fixed"))))
      (client-mode ,(boolean-on/off (ball-client-mode)))
      (http-domain ,(the-http-domain))
      ,@(if (not (equal? ($smtp-host) "localhost"))
	    `((smtp-smart-host ,(or ($smtp-host) ""))) '())
      ,@(cond
	 ((($https-use-socks4a) "askemos.org") '((socks4a-mode "yes")))
	 ((($https-use-socks4a) ".onion") '((socks4a-mode "maybe")))
	 ((($https-use-socks4a) ".i2p") '((socks4a-mode "maybe")))
	 (else '()))
      ,@(if ($https-socks4a-server)
	    `((socks4a-server ,($https-socks4a-server))) '())
      ,@(let ((external-address ($external-address)))
	  (if external-address `((route ,external-address)) '()))
      )
     (ports
      (max-connections ,(parallel-requests))
      (control ,($control-port))
      (http ,($http-server-port))
      (https ,($https-server-port))
      (https-external ,($external-port))
      (lmtp ,($lmtp-server-port)))
     (links
      (max-connections ,($http-client-connections-maximum))
      (max-requests ,($http-requests-per-connection))
      (max-ssl-requests ,($https-requests-per-connection))
      (ssl-keep-alive ,(ssl-keep-alive-timeout)))
     ,(ball-host-map->xml *http-routeable-hosts*
			  (if (equal? ((host-lookup) (public-oid)) (local-id))
			      (oid->string (public-oid)) (local-id))
			  ($host-map-size)))))

;; KLUDGE the parameter works in rscheme but in chicken it's already thread-local.
;; The former seems right to me.
;;
;;(define ball-config-file (make-shared-parameter "./localhost/config.xml"))

(define ball-config-file
  (let ((f #f))
    (lambda args
      (if (pair? args)
	  (begin
	    (set! f (car args))))
      (or f (error "ball-config-file not initialised")))))

(define ball-save-config
  (let ((mutex (make-mutex 'save-config)))
    (define (ball-save-config)
      (let ((str (xml-format (ball-configuration->xml))))
	(with-mutex mutex
		    (let* ((cf (ball-config-file))
			   (nf (string-append cf ",n")))
		      (if (file-exists? nf) (remove-file nf))
		      (call-with-output-file nf
			(lambda (port) (display str port)))
		      (rename-file nf cf)))))
    ball-save-config))

(define (ball-load-config)
  (let ((f (ball-config-file)))
    (and
     (file-exists? f)
     (guard
      (c (else (receive
       (title msg args rest) (condition->fields c)
       (logcond 'ball-load-config title msg args))))
      (let ((node (document-element (xml-parse (filedata f)))))
	(let ((a ((sxpath '(host administrator)) node)))
	  (set-administrator-email! (data ((sxpath '(email)) a)))
	  (set-administrator-password!
	   (or (and-let* ((v1 ((sxpath '(password-md5s6)) a))
			  ((not (node-list-empty? v1))))
			 (data v1))
	       (data ((sxpath '(password-sha256)) a)))
	   #f))
	(set-control-password!
	 (or (and-let* ((v1 ((sxpath '(host operator password-md5s6)) node))
			((not (node-list-empty? v1))))
		       (data v1))
	     (data ((sxpath '(host operator password-sha256)) node)))
	 #f)
	;; Paths
	;; (ssl-directory (data ((sxpath '(paths ssl-directory)) node)))
	;; Host
	(let ((c (string->oid (data ((sxpath '(trusted-code)) node)))))
	  (if c (set! $trustedcode-oid c)))
	(let ((c (string->oid (data ((sxpath '(agreement clock)) node)))))
	  (if c (set! $clock-oid c)))
	(let ((c (string->oid (data ((sxpath '(agreement registry)) node)))))
	  (if c (set! $cert-registry c)))
	(let ((trust-mode (data ((sxpath '(host trust)) node))))
	  ($ca-slave (equal? trust-mode "slave"))
	  ($tc-tofu (equal? trust-mode "tofu")))
	(ball-client-mode (boolean-sxpath node '(host client-mode)))
	;; Agreement
	(bocntrl-sync-on-load (boolean-sxpath node '(agreement sync-on-load)))
	(and-let* ((p (string->number
		       (data ((sxpath '(agreement defer-sync-messages)) node)))))
		  ($defer-sync-replies-to-stable-state p))
	(use-mandatory-capabilities
	 (boolean-sxpath node '(agreement mandatory-capabilities)))
	(the-http-domain (data ((sxpath '(host http-domain)) node)))
	(let ((node ((sxpath '(host)) node)))
	  (let ((smtp-host ((sxpath '(smtp-smart-host)) node)))
	    (if (not (node-list-empty? smtp-host))
		(let ((v (data smtp-host)))
		  ($smtp-host (and (not (string=? v "")) v)))))
	  (let ((socks4a-mode ((sxpath '(socks4a-mode)) node)))
	    (if (not (node-list-empty? socks4a-mode))
		($https-use-socks4a (data socks4a-mode))))
	  (let ((socks4a-host ((sxpath '(socks4a-server)) node)))
	    (if (not (node-list-empty? socks4a-host))
		(let ((v (data socks4a-host)))
		  ($https-socks4a-server (and (not (string=? v "")) v)))))
	  (let ((route ((sxpath '(route)) node)))
	    (if (not (node-list-empty? route))
		(let ((v (data route)))
		  ($external-address (and (not (string=? v "")) v)))))
	  )
	(let ((ports ((sxpath '(ports)) node)))
	  (set-http-server-port! (data ((sxpath '(http)) ports)))
	  (set-https-server-port! (data ((sxpath '(https)) ports)))
	  (and-let* ((p (string->number
			 (data ((sxpath '(https-external)) ports)))))
		    ($external-port p))
	  (set-lmtp-server-port! (data ((sxpath '(lmtp)) ports)))
	  (set-control-port! (data ((sxpath '(control)) ports)))
	  (parallel-requests
	   (string->number (data ((sxpath '(max-connections)) ports)))))
	(for-each
	 (lambda (k)
	   (case (gi k)
	     ((agreement) (agree-verbose (boolean-sxpath k '(*any*))))
	     ((topology) ($topology-verbose (boolean-sxpath k '(*any*))))
	     ((client) ($https-client-verbose (boolean-sxpath k '(*any*))))
	     ((server) ($http-server-verbose (boolean-sxpath k '(*any*))))
	     (else (logerr "Unknown log key ~a in config\n"(gi k)))))
	 ((sxpath '(agreement log *)) node))
	(for-each
	 (lambda (k)
	   (case (gi k)
	     ((local-blob) ($fsm-blob-paranoia (boolean-sxpath k '(*any*))))
	     ((immutables) ($enforce-frame-oid-match (boolean-sxpath k '(*any*))))
	     (else (logerr "Unknown paranoia key ~a in config\n"(gi k)))))
	 ((sxpath '(restrictions paranoia *)) node))
        (for-each
         (lambda (k)
           (case (gi k)
             ((gc-delay) (and-let* ((x (string->number (data k)))) (fsm-delay x)))
             (else (logerr "Unknown storage key ~a in config\n"(gi k)))))
	 ((sxpath '(storage *)) node))
	(let ((links ((sxpath '(links)) node)))
	  (let ((x (string->number (data ((sxpath '(max-connections)) links)))))
	    (if x ($http-client-connections-maximum x)))
	  (let ((x (string->number (data ((sxpath '(max-requests)) links)))))
	    (if x ($http-requests-per-connection x)))
	  (let ((x (string->number (data ((sxpath '(max-ssl-requests)) links)))))
	    (if x ($https-requests-per-connection x)))
	  (let ((x (string->number (data ((sxpath '(ssl-keep-alive)) links)))))
	    (if x (ssl-keep-alive-timeout x))))
	(let ((node ((sxpath '(agreement response)) node)))
	  (if (not (node-list-empty? node))
	      (respond-timeout-kept-here-for-the-time-beeing (string->number (data node)))))
	(let ((node ((sxpath '(agreement remote)) node)))
	  (if (not (node-list-empty? node))
	      (remote-fetch-timeout-kept-here-for-the-time-beeing (string->number (data node)))))
	(let ((node ((sxpath '(agreement broadcast)) node)))
	  (if (not (node-list-empty? node))
	      ($broadcast-timeout (string->number (data node)))))
	(let ((node ((sxpath '(agreement broadcast-retries)) node)))
	  (if (not (node-list-empty? node))
	      ($broadcast-retries (string->number (data node)))))
	(update-dependent-parameters)
	(ball-read-host-map! ((sxpath '(HostsDB)) node) #t))))))

(define cn-pcre
  (pcre/a->proc-uncached "(?:[/,])?CN=(?:https?://)?([^/,]+)(?:[/,].+)?"))

(define (ball-load-local-id-from-config)
  (or (let ((f (ball-config-file)))
	(and
	 (file-exists? f)
	 (guard
	  (c (else (receive
		    (title msg args rest) (condition->fields c)
		    (logcond 'ball-load-local-id-from-config title msg args))))
	  (let ((node (document-element (xml-parse (filedata f)))))
	    (let ((id (data ((sxpath '(host local-id)) node))))
	      (and (not (string=? id ""))
		   (begin (local-id id) id)))))))
      (and-let* ((subject (tc-private-subject))
		 (match (cn-pcre subject)))
		(local-id (cadr match))
		#t)))

(define *startup-time* *system-time*)

(define (mind-restart)
  (if (> (time-second *system-time*) (+ (time-second *startup-time*) 30))
      (begin
	(logerr "\n~a\nrestart\n"
		(with-output-to-string (lambda () (thread-list))))
	(exit 1))
      (raise
       (make-condition
	&http-effective-condition 'status 400
	'message "restart disabled"))))

(define $bopctrl-require-host-master (make-shared-parameter #f))

(define (ball-set-parameters node)
  (define need-restart #f)
  (if (and ($bopctrl-require-host-master)
	   (not (check-control-password
		 (or (find-first-data-field 'password node) ""))))
      (raise (make-condition
	      &unauthorised 'status 401
	      'message "Control password incorrect."
	      'resource "parameters"
	      'source "set parameters")))
  (and-let* ((a (find-first-data-field 'new-local-id node))
	     ((not (equal? a (local-id)))))
	    (local-id a)
	    (set! need-restart #t))
  (and-let* ((a (find-first-data-field 'administrator node)))
	    (set-administrator-email! a))
  (and-let* ((a (find-first-data-field 'http-domain node)))
	    (the-http-domain a))
  (and-let* ((pw (find-first-data-field 'administrator-password node)))
	    (set-administrator-password! pw #t))
  (and-let* ((pw (find-first-data-field 'operator-password node)))
	    (set-control-password! pw #t))
  (and-let* ((ff (find-first-data-field 'smtp-smart-host node)))
	    ($smtp-host ff))
  (and-let* ((ff (find-first-data-field 'socks4a-mode node)))
	    ($https-use-socks4a ff))
  (and-let* ((ff (find-first-data-field 'socks4a-server node)))
	    ($https-socks4a-server ff))
  (and-let* ((ff (find-first-data-field 'routing-info node)))
	    ($external-address ff))
  ;; paths
  ;; -
  ;; agreement
  (and-let* ((ff (find-first-data-field 'response node)))
	    (let* ((time (string->number ff))
		   (tv (and (number? time) (> time 0) time)))
	      (respond-timeout-kept-here-for-the-time-beeing tv)))
  (and-let* ((ff (find-first-data-field 'remote node)))
	    (let* ((time (string->number ff))
		   (tv (and (number? time) (> time 0) time)))
	      (remote-fetch-timeout-kept-here-for-the-time-beeing tv)))
  (and-let* ((ff (find-first-data-field 'broadcast node)))
	    (let* ((time (string->number ff))
		   (val (and (fixnum? time) (> time 0) time)))
	      ($broadcast-timeout val)))
  (and-let* ((ff (find-first-data-field 'retries node)))
	    (let ((time (string->number ff)))
	      ($broadcast-retries (and (fixnum? time) (> time 0) time))))
  (and-let* ((ff (find-first-data-field 'requests-per-connection node)))
	    (let ((n (string->number ff)))
	      ($http-requests-per-connection (and (fixnum? n) (> n 0) n))))
  (and-let* ((ff (find-first-data-field 'requests-per-ssl-connection node)))
	    (let ((n (string->number ff)))
	      ($https-requests-per-connection (and (fixnum? n) (> n 0) n))))
  (and-let* ((ff (find-first-data-field 'max-client-connections node)))
	    (let ((n (string->number ff)))
	      ($http-client-connections-maximum (and (fixnum? n) (> n 0) n))))
  (and-let* ((ff (find-first-data-field 'ssl-keep-alive node)))
	    (let ((n (string->number ff)))
	      (ssl-keep-alive-timeout (and (fixnum? n) (>= n 0) n))))

  (and-let* ((ff (find-first-data-field 'minimum-delay-between-fsgc node)))
	    (let ((n (string->number ff)))
	      (fsm-delay (and (fixnum? n) (> n 100) n))))
  ($fsm-blob-paranoia (equal? (find-first-data-field 'local-blob-paranoia node) "on"))
  ($enforce-frame-oid-match (equal? (find-first-data-field 'immutable-paranoia node) "on"))

  ;; host
  (and-let* ((ff (find-first-data-field 'require-cert node))
	     ((not (eq? (equal? ff "on") ($https-server-require-cert)))))
	    ($https-server-require-cert (not ($https-server-require-cert)))
	    (set! need-restart #t))

  (and-let* ((ff (find-first-data-field 'trust-mode node)))
	    ($ca-slave (equal? ff "slave"))
	    ($tc-tofu (equal? ff "tofu")))

  (ball-client-mode (equal? (find-first-data-field 'client-mode node) "on"))
  (use-mandatory-capabilities
   (equal? (find-first-data-field 'mandatory-capabilities node) "on"))
  (bocntrl-sync-on-load (find-first-data-field 'sync-on-load node))
  (and-let* ((p (string->number (find-first-data-field 'defer-sync-messages node))))
	    ($defer-sync-replies-to-stable-state p))

  (agree-verbose (equal? (find-first-data-field 'log-agreement node) "on"))
  ($topology-verbose (equal? (find-first-data-field 'log-network node) "on"))
  ($https-client-verbose (equal? (find-first-data-field 'log-client node) "on"))
  ($http-server-verbose (equal? (find-first-data-field 'log-server node) "on"))

  (and-let* ((ff (find-first-data-field 'clock-oid node)))
	    (set! $clock-oid (string->oid ff)))
  (and-let* ((ff (find-first-data-field 'registry-oid node)))
	    (set! $cert-registry (string->oid ff)))
  (and-let* ((ff (find-first-data-field 'trusted-code node))
	     (oid (string->oid ff))
	     ((not (eq? $trustedcode-oid oid))))
	    (set! $trustedcode-oid oid)
	    (set! need-restart #t))
  ;; ports
  (let ((ff (find-first-data-field 'max-server-connections node)))
    (parallel-requests (and ff (string->number ff))))
  (and-let* ((ff (find-first-data-field 'control-port node))
	     ((set-control-port! ff)))
	    (set! need-restart #t))
  (and-let* ((ff (find-first-data-field 'http-port node))
	     ((set-http-server-port! ff)))
	    (set! need-restart #t))
  (and-let* ((ff (find-first-data-field 'https-port node))
	     ((set-https-server-port! ff)))
	    (set! need-restart #t))
  (and-let* ((ff (find-first-data-field 'external-port node))
	     (p (string->number ff)))
	    ($external-port p))
  (and-let* ((ff (find-first-data-field 'lmtp-port node))
	     ((set-lmtp-server-port! ff)))
	    (set! need-restart #t))
;;   (and-let* ((ssldir (find-first-data-field 'ssl-directory node)))
;; 	    (ssl-directory ssldir))
  (update-dependent-parameters)
  (ball-save-config)
  (if need-restart (mind-execute-synchron mind-restart)))

(define (ball-control-connect node)
  (let ((to (data (form-field 'to node))))
    (askemos-connect to)))

(define (ball-control-default key is-local node)
  (define (check-hostmaster)
    (if (and ($bopctrl-require-host-master)
	     (not (check-control-password (data (form-field 'password node)))))
	(raise (make-condition &unauthorised 'status 403
			       'message (format "Host Administrator Password ~a ?"
						(data (form-field 'password node)))
			       'resource "host certificate"
			       'source "certificate management"))
	#t))
  (case key
    ((connect)
     (ball-control-connect node))
    ((create-channel)
     (if is-local
	 (let ((from (find-first-data-field 'from node)))
	   (or (check-administrator-password (data (form-field 'password node)))
	       (raise (make-condition &unauthorised 'status 403
				      'message (format "User Administrator Password ~a ?"
						       (data (form-field 'password node)))
				      'resource "host certificate"
				      'source "certificate management")))
	   (if from
	       (or (support-entry-point! from
					 (find-first-data-field 'name node)
					 (find-first-data-field 'rname node))
		   (error "Failed to support ~a from ~a\n" (find-first-data-field 'rname node) from))
	       (let ((s (find-first-data-field 'secret1 node)))
		 (if (not (equal? s (find-first-data-field 'secret2 node)))
		     (error "secret and 2nd secret are not the same, try again."))
		 (make-entry-point!
		  (find-first-data-field 'host node)
		  (find-first-data-field 'password node)
		  (find-first-data-field 'name node)
		  (make-xml-element
		   'new namespace-mind
		   (list (make-xml-attribute 'secret #f (secret-encode s (make-local-secret-salt)))
			 (make-xml-attribute
			  'action #f
			  (let ((s (find-first-data-field 'contract node)))
			    (or (and (string->oid s) s)
				(oid->string ((make-context (public-oid)) s))
				(error "contract \"~a\" not found" s)))))
		   (let ((from (find-frames-by-id (public-oid))))
		     (node-list
		      (make-xml-element
		       'link namespace-mind
		       (list (make-xml-attribute 'name #f "public"))
		       (make-xml-element 'id namespace-mind '()
					 (oid->string (aggregate-entity from))))
		      (message-body
		       (call-subject!
			from (replicates-of from) 'read
			(make-message
			 (property 'capabilities (get-slot (aggregate-meta from)
							   'capabilities))
			 (property 'accept application/rdf+xml)
			 (property 'content-type text/xml)
			 (property 'location '())
			 (property 'destination
				   (let ((d (find-first-data-field 'data node)))
				     (if (and d (not (string=? d "")))
					 (list d) '())))
			 (property 'dc-creator (public-oid))))))))))))
	 (error "Not allowed")))
    ((remove-channel)
     (if (and is-local (check-hostmaster))
	 (let ((del (find-first-data-field 'name node)))
           (if (or (member del '("system" "public"))
                   (let ((o (mesh-cert-o (tc-private-cert))))
                     (and o (equal? del (entry-name o)))))
               (raise
                (make-condition
                 &http-effective-condition 'status 400
                 'message (format "refusing to drop essential channel \"~a\"" del))))
           (drop-entry-point! del))))
    ((manage-user-cert)
     (if is-local (ball-manage-user-cert node)))
    ((set-limits)
     (if (and is-local (check-hostmaster))
	 (ball-set-parameters node)))
    ((store-cert)
     (if (and is-local (check-hostmaster))
	 (let ((key (filedata (tc-private-cert-req-key-file)))
	       (cert (find-first-data-field 'cert node)))
	   (if (and key (x509-subject-hash cert))
	       (begin
		 (tc-store-host-cert! cert)
		 (tc-store-host-key! key)
		 (remove-file (tc-private-cert-req-key-file)))))))
    ((store-cacert)
     (if (and is-local (check-hostmaster))
	 (let ((cert (find-first-data-field 'cacert node))
	       (key (find-first-data-field 'cakey node)))
	   (tc-store-ca-cert! (and (guard (ex (else #f))
					  (x509-subject-hash cert))
				   cert)
			      key))))
#;    ((ca-peer)
     (if (and is-local (check-hostmaster))
	 (let* ((source (data (find-first-data-field 'source node)))
		(result (and source (http-get-ca source))))
	   (if (not (eqv? (get-slot result 'http-status) 200))
	       (raise-service-unavailable-condition
		(format "Error retrieving CA from ~a status ~a"
			source (get-slot result 'http-status))))
	   (if result
	       (let* ((peer (mesh-cert-l (make-mesh-certificate (x509-subject (get-slot result 'mind-body)))))
		      (ca-file (tc-cert-file-for peer)))
		 (begin
		   (logerr "storing peer CA certificate for ~a\n" peer)
		   (tc-store-ca-cert-for! peer (get-slot result 'mind-body))))))))
    ((publish-cert-request)
     (if (and is-local (check-hostmaster))
	 (ball-publish-certificate-request!)))
    ((ca-connect)
     (if (and is-local (check-hostmaster))
	 (let* ((source (data (find-first-data-field 'source node)))
		(result (and source (http-get-ca source))))
	   (if (not (eqv? (get-slot result 'http-status) 200))
	       (raise-service-unavailable-condition
		(format "Error retrieving CA from ~a status ~a"
			source (get-slot result 'http-status))))
	   (if (and result (or (not (file-exists? (tc-ca-cert-file)))
			       (not (string=? (data (tc-ca-cert-file))
					      (get-slot result 'mind-body)))))
	       (begin
		 (logerr "storing new CA certificate\n")
		 (tc-store-ca-cert! (get-slot result 'mind-body) #f)))
	   (let* ((rname (find-first-data-field 'rname node))
		  (user-data (and source rname ((metainfo-remote) source (data rname))))
		  (nick (if (and rname
				 (or (eqv? (get-slot user-data 'http-status) 200)
				     (error
				      (format "Error getting user info for \"~a\" ~a"
					      rname (get-slot user-data 'http-status)))))
			    (if user-data
				(attribute-string
				 'about (node-list-first
					 ((sxpath '(Description))
					  (message-body user-data))))
				(error "no user ~a at ~a" (data rname) source))
			    (find-first-data-field 'localuser node)))
		  (client (if (eq? (public-oid) null-oid)
			      (data (form-field 'host node))
			      (oid->string (public-oid))))
		  (pass (data (form-field 'password node)))
		  (kpass (and #f (not (equal? pass "")) pass))
		  (cn (data (form-field 'cn node)))
		  (user (and-let* ((nick)
				   (oid (string->oid nick)))
				  (find-local-frame-by-id oid 'ca-connect)))
		  ;; (metainfo (call-subject! user #f 'read (force public-read-metainfo-request)))
		  (num (if user (car (fget user 'version)) 1))
		  (days (or (string->number (data (form-field 'days node))) 30)))
	     (force
	      (!order
	       (let* ((key (tc-genkey kpass))
		      (req (tc-certificate-request
			    key kpass cn: cn o: (or nick "") l: client days: days)))
		 (tc-store-host-cert-req-key! key)
		 (tc-store-host-cert-req! req)
		 (let ((entry (ball-user-cert (string->oid (data (form-field 'user node))))))
		   (vector-set! entry 0 key)
		   (vector-set! entry 1 req))
		 (guard
		  (ex (else (logerr "ca-connect email failed ~a\n." ex)))
		  (send-email $administrator req '((Subject "certificate request")))
		  (logerr "ca-connect mailed request to ~a\n" $administrator)))
	       "generate key"))))))
    ((create-new-key)
     (if (and is-local (check-hostmaster))
	 (begin
	   (tc-request-client-key
	    (data (form-field 'cn node))
	    #f (x509-certificate-validity-period))
	   (hash-table-set! *ball-user-cert-space*
			    (string->oid (data (form-field 'user node)))
			    (vector
			     (filedata (tc-private-cert-req-key-file))
			     (filedata (tc-private-cert-req-file))
			     "")))))
    ((create-new-ca)
     (if (and is-local (check-hostmaster))
	 (and-let*
	  ((pass (data (form-field 'password node)))
	   (kpass (and (not (equal? pass "")) pass))
	   (days (or (string->number (data (form-field 'days node)))
		     3000)))
	  (tc-make-ca (if (eq? (public-oid) one-oid)
			  (local-id)
			  (oid->string (public-oid)))
		      pass days))
	 (error "new ca: request ignored")))
    ((restart) (and is-local (check-hostmaster) (mind-restart)))
    (else (raise
	   (make-condition
	    &http-effective-condition 'status 400
	    'message (format "ball-control command ~a not understood in ~a"
			     key (xml-format node))))))
  (empty-node-list))

(define ball-control-hook ball-control-default)

(define (ball-control node)
  (ball-control-hook
   (string->symbol (data (form-field 'action node)))
   (equal? (data (form-field 'host node)) (local-id))
   node))
