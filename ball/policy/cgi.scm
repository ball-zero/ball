;; (C) 2003 J�rg F. Wittenberger see http://www.askemos.org

;; CGI

;; The CGI stuff is dead by default.  Do NOT casually export CGI
;; execution to the application level.  That would introduce a
;; unimaginable wide security hole!

;; Nevertheles CGI support is useful, many third party software comes
;; as CGI.  To enable CGI execution, create a function like this:

(define (test-cgi request) (run-cgi request *cgi-dir* "test-cgi"))

;; Than maintain an entry in the TrustedCode table.

;; Don't forget to set $trustedcode-oid in your configuration
;; accordingly.  Restart to re-read the TrustedCode table or call

;; (init-trustedcode $trustedcode-oid)

;; from the online debugger.

(define *cgi-dir* "/usr/lib/cgi-bin/")

(define (make-cgi-env request cgi is-public scgi)
  `(("CONTENT_LENGTH"
     ;; The "CONTENT_LENGTH" header MUST the FIRST ONE (for SCGI that is).
     . ,(number->string (message-body/plain-size request)))
    ,@(if scgi
	  '(("SGCI" . "1"))
	  '(("QUERY_STRING" . ,(call-with-output-string
				(lambda (port)
				  (write-query-form (message-body request) port))))))
    ("CONTENT_TYPE" . ,(request 'content-type))
    ("SERVER_SOFTWARE" . "Askemos/BALL")
    ("SERVER_NAME" . , (or (request 'http-base-url) ""))
    ("GATEWAY_INTERFACE" . "CGI/1.1")
    ("SERVER_PROTOCOL" . "HTTP/1.1")
    ("SERVER_PORT" . "80") ; fake
    ("REQUEST_METHOD" . ,(or (request 'request-method)
			     (let ((type (request 'type)))
			       (cond
				((member type '(post "POST"))    "POST")
				((member type '(write "PUT" #t)) "PUT")
				((eq? type 'CONNECT) "CONNECT")
				(else "GET")))))
    ("HTTP_ACCEPT" . ,(or (request 'accept) ""))
    ("HTTP_USER_AGENT" . , (or (request 'http_user_agent) "mozilla"))
    ("PATH_INFO" . ,(http-format-read-location
		     (reverse (or (request 'destination) '()))))
    ("PATH_TRANSLATED" . ,cgi)		; ought to be "file" from run-cgi
    ("SCRIPT_NAME" . ,cgi)
    ("REMOTE_HOST" . "localhost")
    ("REMOTE_ADDR" . "127.0.0.1")
    ("REMOTE_USER" . ,(or (and (not is-public)
			       (entry-name (request 'dc-creator)))
			  ""))
    ("AUTH_TYPE" . ,(if is-public "" "Basic"))))

(define (run-cgi request dir cgi)
  (let ((file (string-append dir cgi)))
    (if (file-execute-access? file)
        (run-child-process
         (lambda (out) (write-query-form (message-body request) out))
         (lambda (in)
           (guard
            (ex (else #f))
	    (let ((reply (http-read-content in #t #f (lambda args (raise 'cgi-broken)))))
	      (set-slot! reply 'http-status (or (get-slot reply 'status) 200))
	      reply)))
         (list file)
         dir
         (make-cgi-env request cgi (eq? (request 'dc-creator) (public-oid)) #f))
        (make-message
         (property 'mind-body (file->string file))
         (property 'content-type (file-name->content-type file))))))

(define (display-netstring str port)
  (display (string-length str) port)
  (display #\: port)
  (display str port)
  (display #\, port))

(define (display-scgi-header name value port)
  (display name port)
  (display #\x0 port)
  (display value port)
  (display #\x0 port))

(define (scgi-response host port msg . cgi)
  (set! cgi (if (null? cgi) host (car cgi)))
  (askemos:run-tcp-client
   host port
   (lambda (in out)
     (let ((request (if (procedure? msg) msg (make-reader msg))))
       (display-netstring
	(call-with-output-string
	 (lambda (port)
	   (for-each
	    (lambda (var) (display-scgi-header (car var) (cdr var) port))
	    (make-cgi-env request cgi (eq? (request 'dc-creator) (public-oid)) #t))))
	out))
     (let ((body (message-body/plain msg)))
       (cond
	((a:blob? body)
	 (display-blob-notation
	  *the-registered-stores* body #f (lambda (o . p) (display o out))))
	((string? body) (display body out))))
     (flush-output-port out)
     (let ((reply (http-read-content in #f #f (lambda _ (raise 'scgi-response-broken)))))
       (set-slot! reply 'http-status (get-slot reply 'status))
       reply))))
