#;(import-for-syntax matchable)

(char-name 'NUL (integer->char 0))
(char-name 'nul (integer->char 0))
(char-name 'cr (integer->char 13))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/srfi/llrbtree.scm")
