;; (C) 2002 Jörg F. Wittenberger see http://www.askemos.org

;;** Evaluation

;; We need a sandbox, i.e., a restricted environment to evaluate user
;; supplied expressions.

;; It's an unfortune fact of life that we have no standard way to
;; create environments.  We'll see this code heavily depend on the
;; underlying language implementation.

(define r4rs-globals
  '(not
    boolean? eq? eqv? equal? pair? cons car cdr
    caar cadr cdar cddr caaar caadr cadar caddr cdaar
    cdadr cddar cdddr caaaar caaadr caadar caaddr cadaar cadadr cadddr
    cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr
    set-car! set-cdr! null? list? list length list-tail list-ref
    append reverse memq memv member assq assv assoc symbol?
    symbol->string string->symbol
    number? integer? exact? real? complex? inexact? rational?
    zero? odd? even? positive? negative?
    max min + - * / = > < >= <= quotient remainder modulo
    gcd lcm abs floor ceiling truncate round
    exact->inexact inexact->exact
    exp log expt sqrt sin cos tan asin acos atan
    number->string string->number
    char? char=? char>? char<? char>=? char<=? char-ci=? char-ci<? char-ci>?
    char-ci>=? char-ci<=?
    char-alphabetic? char-whitespace? char-numeric? char-upper-case?
    char-lower-case? char-upcase char-downcase
    char->integer integer->char string? string=?
    string>? string<? string>=? string<=? string-ci=? string-ci<?
    string-ci>? string-ci>=? string-ci<=?
    make-string string-length string-ref string-set!
    string-append string-copy string->list 
    list->string substring string-fill! vector? make-vector vector-ref
    vector-set! string vector
    vector-length vector->list list->vector vector-fill!
    procedure? map for-each apply force 
    call-with-current-continuation input-port? output-port?
    current-input-port current-output-port
    call-with-input-file call-with-output-file open-input-file
    open-output-file close-input-port
    close-output-port load transcript-on transcript-off
    read eof-object? read-char peek-char
    write display write-char newline with-input-from-file
    with-output-to-file
    call-with-values
    values dynamic-wind
    ;; make-promise
    ))

(define dsssl-environment (the (or false (struct hash-table)) #f))
(define eval-sane (the (or false procedure) #f))

(define (dsssl-export! value name)
  (hash-table-set! dsssl-environment name (vector value)))

(define (dsssl-export-named! sym)
  (if (pair? sym)
      (dsssl-export! (eval (car sym)) (cadr sym))
      (dsssl-export! (eval sym) sym)))

 (define (with-exception-guard handler thunk)
   ((call-with-current-continuation
     (lambda (return)
       (let ((oldh ##sys#current-exception-handler))
	 (with-exception-handler
	  (lambda (condition)
	    (with-exception-handler
	     oldh
	     (call-with-current-continuation
	      (lambda (handler-k)
		(return (lambda () (handler condition)))))))
	  (lambda ()
	    (##sys#call-with-values
	     thunk
	     (lambda args
	       (return (lambda () (##sys#apply ##sys#values args)))) ) ) )) ) )) )

(define (init-dsssl!)
  (set! dsssl-environment (make-hash-table))
  (set! eval-sane
        (let ((g (make-compiler dsssl-environment r4rs-special-form-table)))
          (lambda (exp) (let ((proc ((g exp) '())))
                          (lambda args (apply proc args)))) ) )
  (dsssl-export! eval-sane 'eval)
  (for-each
   (lambda (sym)
     (if (not (memq sym r5rs-side-effecty-things))
         (dsssl-export-named! sym)))
   r4rs-globals)
  (for-each
   (lambda (lst) (for-each dsssl-export-named! lst))
   (list dsssl-forms srfi19-forms srfi43-forms))
  (dsssl-export! fun:empty-node-list 'empty-node-list)
  (dsssl-export! error 'error)
  (dsssl-export! cons* 'cons*)
  (dsssl-export! srfi:member 'member)
  (dsssl-export! with-exception-guard 'with-exception-guard)
  #t)

(init-dsssl!)

(define (eval-sane-string str)
  (handle-exceptions
   ex (error (format #f "while evaluating \"~a\" ~a" str ex))
   ;; We put a lambda around the expression to support "define".
   ;; Otherwise top level defines could extend over the invocation.
   (let ((expr `(lambda () . ,(with-input-from-string str read))))
     (eval-sane expr))))

(define eval-thread-safe eval)
