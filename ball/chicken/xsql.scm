;; (C) 2002, 2003, 2008 J�rg F. Wittenberger

;; chicken module clause for the Askemos function module.

(declare
 (unit xsql)
 ;; promises
 (disable-interrupts)			;; loops check interrupts
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (fixnum)
 (strict-types)
 (usual-integrations))

(module
 xsql
 (
  init-xsql
  xsql-user-database-file
  with-connection
  sql-exec-protected sql-write
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 vector-copy! with-exception-handler condition? promise?)
        shrdprmtr
	atomic (except srfi-18 raise) srfi-34 srfi-35 srfi-45
	foreign)

(import srfi-1 srfi-4 srfi-13 srfi-19 srfi-43 srfi-69
	matchable ports memoize pcre environments timeout parallel cache sqlite3
	protection util utf8utils tree wttree notation xpath extras data-structures)

(import srfi-110)

(import	(prefix srfi-1 srfi:)
	(prefix srfi-13 srfi:)
	(prefix srfi-43 srfi:))

(import function-common dsssl)

(import aggregate-chicken)		; string-llrb-tree

(import xslt xslt-stylesheet)

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/function/xsql.scm")

)

(import (prefix xsql m:))

(define xsql-user-database-file m:xsql-user-database-file)

(define sql-write m:sql-write)
