(declare
 (unit openssl)
 (fixnum)
; (strict-types)
 (usual-integrations)

;(require-extension srfi-34)
; was macht srfi-18?
)

(module
 openssl
 (
  md2-digest
  md4-digest
  md5-digest
  sha1-digest
  sha224-digest
  sha256-digest
  sha384-digest
  sha512-digest
  ripemd160-digest
  md2-hmac
  md4-hmac
  md5-hmac
  sha1-hmac
  sha224-hmac
  sha256-hmac
  sha384-hmac
  sha512-hmac
  ripemd160-hmac
  ;;
  x509-pem
  x509-read-file
  x509-read-string
  x509-subject
  x509-subject-hash
  x509-expiration-time
  x509-text
  ;;
  x509req-pem
  x509req-read-string
  x509req-text
  x509req-subject
  x509-create-certificate-request
  ;;
  x509-create-signature
  ;;
  genrsa
  ;; rsa-text
  read-private-key
  )

(import scheme
	(except chicken add1 sub1 with-exception-handler condition? promise? vector-copy!)
	foreign
	srfi-34 srfi-35
	(only ports call-with-output-string)
        extras)

(define (make-string/uninit size)
  (##sys#allocate-vector size #t #f #f))

#>
#include <openssl/evp.h>
#include <openssl/hmac.h>

/* Compatibility section; see https://wiki.openssl.org/index.php/Talk:OpenSSL_1.1.0_Changes */

#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined (LIBRESSL_VERSION_NUMBER)
static HMAC_CTX *HMAC_CTX_new(void)
{
   HMAC_CTX *ctx = OPENSSL_malloc(sizeof(*ctx));
   if (ctx != NULL)
       HMAC_CTX_init(ctx);
   return ctx;
}

static void HMAC_CTX_free(HMAC_CTX *ctx)
{
   if (ctx != NULL) {
       HMAC_CTX_cleanup(ctx);
       OPENSSL_free(ctx);
   }
}
#endif

#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined (LIBRESSL_VERSION_NUMBER)
#define EVP_MD_CTX_new EVP_MD_CTX_create
#define EVP_MD_CTX_free EVP_MD_CTX_destroy
#endif
/* End of compatibility section */

#define C_OPENSSL_BUFFER_SIZE 8192
static C_TLS C_char buffer[C_OPENSSL_BUFFER_SIZE];
#include <openssl/conf.h>
<#

;; initialization
(define (openssl-init)
  ;;((foreign-lambda void "OpenSSL_add_all_algorithms"))
  ;;((foreign-lambda void "OpenSSL_add_all_ciphers"))
  ((foreign-lambda void "OPENSSL_no_config"))
  ((foreign-lambda void "OpenSSL_add_all_digests")))

(openssl-init)

(define-foreign-type EVP_MD_CTX* (nonnull-c-pointer "EVP_MD_CTX"))
(define-foreign-type EVP_MD*     (nonnull-c-pointer "EVP_MD"))
(define-foreign-type ENGINE*     (c-pointer "ENGINE"))

(define unpack!
  (foreign-lambda*
   scheme-object ((scheme-object digest))
    "C_i_foreign_string_argumentp(digest); "
    "static char hex[] = \"0123456789abcdef\";

     C_uchar *in = (C_uchar *) C_c_string( digest );
     int i = C_header_size(digest) >> 1;

     for( --i; i >= 0; i-- ) {
       in[ 2 * i + 1 ] = hex[   in[i]        & 0x0f ] ;
       in[ 2 * i     ] = hex[ ( in[i] >> 4 ) & 0x0f ] ;
     }
     C_return(digest);"))

;;; handling context
#|
(define (EVP_MD_CTX_create)
  (let ((v ((foreign-lambda EVP_MD_CTX* "EVP_MD_CTX_create"))))
					;(printf "EVP_MD_CTX_create ~a\n" v)
    (and v (set-finalizer! v EVP_MD_CTX_destroy))
    v))
|#

(define EVP_MD_CTX_create
  (foreign-lambda EVP_MD_CTX* "EVP_MD_CTX_new"))

(define (EVP_MD_CTX_destroy ctx)
  ;(printf "EVP_MD_CTX_destroy ~a\n" ctx)
  ((foreign-lambda void "EVP_MD_CTX_free" EVP_MD_CTX*) ctx) #f)

;;; handling digests
;; explicite named digest types
(define EVP_md-null (foreign-lambda EVP_MD* "EVP_md_null"))
;(define EVP_md2 (foreign-lambda EVP_MD* "EVP_md2"))
(define EVP_md4 (foreign-lambda EVP_MD* "EVP_md4"))
(define EVP_md5 (foreign-lambda EVP_MD* "EVP_md5"))
(define EVP_sha1 (foreign-lambda EVP_MD* "EVP_sha1"))
(define EVP_sha224 (foreign-lambda EVP_MD* "EVP_sha224"))
(define EVP_sha256 (foreign-lambda EVP_MD* "EVP_sha256"))
(define EVP_sha384 (foreign-lambda EVP_MD* "EVP_sha384"))
(define EVP_sha512 (foreign-lambda EVP_MD* "EVP_sha512"))
;(define EVP_mdc2 (foreign-lambda EVP_MD* "EVP_mdc2"))
(define EVP_ripemd160 (foreign-lambda EVP_MD* "EVP_ripemd160"))

;; get digest
(define (EVP_get_digestbyname name)
 (or ((foreign-lambda EVP_MD* "EVP_get_digestbyname" c-string) name)
     (raise (sprintf
	     "EVP_get_digestbyname: unknown message digest name ~A"
	     name))))

(define (EVP_get_digestbynid id)
  (EVP_get_digestbyname
   (or (OBJ_nid2sn id)
       (raise (sprintf
	       "EVP_get_digestbynid: no valid message digest id ~A" id)))))

;; digest informations
(define OBJ_nid2sn (foreign-lambda c-string "OBJ_nid2sn" integer))
(define OBJ_nid2ln (foreign-lambda c-string "OBJ_nid2ln" integer))

(define (EVP_MD_type md)
  ((foreign-lambda* integer ((EVP_MD* md))
		    "C_return( EVP_MD_type( md ));") md))

(define (EVP_MD_name md)
  ((foreign-lambda* c-string ((EVP_MD* md))
		    "C_return( EVP_MD_name( md ));") md))

(define (EVP_MD_lname md) ; private
  ((foreign-lambda* c-string ((EVP_MD* md))
		    "C_return( OBJ_nid2ln( EVP_MD_type( md )));") md))

(define (EVP_MD_pkey_type md)
  ((foreign-lambda* integer ((EVP_MD* md))
		    "C_return( EVP_MD_pkey_type( md ));") md))

(define (EVP_MD_pkey_name md) ; private
  ((foreign-lambda* c-string ((EVP_MD* md))
		    "C_return( OBJ_nid2sn( EVP_MD_pkey_type( md )));") md))

(define (EVP_MD_pkey_lname md) ; private
  ((foreign-lambda* c-string ((EVP_MD* md))
		    "C_return( OBJ_nid2ln( EVP_MD_pkey_type( md )));") md))

(define (EVP_MD_size md)
  ((foreign-lambda* integer ((EVP_MD* md))
		    "C_return( EVP_MD_size( md ));") md))

(define (EVP_MD_block_size md)
  ((foreign-lambda* integer ((EVP_MD* md))
		    "C_return( EVP_MD_block_size( md ));") md))

(define (EVP_MD_CTX_md ctx)
  ((foreign-lambda* EVP_MD* ((EVP_MD_CTX* ctx))
		    "C_return( EVP_MD_CTX_md( ctx ));") ctx))

(define (EVP_MD_CTX_size ctx)
  ((foreign-lambda* integer ((EVP_MD_CTX* ctx))
		    "C_return( EVP_MD_CTX_size( ctx ));") ctx))

(define (EVP_MD_CTX_block_size ctx)
  ((foreign-lambda* integer ((EVP_MD_CTX* ctx))
		    "C_return( EVP_MD_CTX_block_size( ctx ));") ctx))

(define (EVP_MD_CTX_type ctx)
  ((foreign-lambda* integer ((EVP_MD_CTX* ctx))
		    "C_return( EVP_MD_CTX_type( ctx ));") ctx))

;; digest operations
(define (EVP_DigestInit ctx type impl) ;; need EVP_MD_CTX_create or ~_init
  (if ((foreign-lambda*
	bool
	((EVP_MD_CTX* ctx) (EVP_MD* type) (ENGINE* impl))
	;; "C_i_foreign_pointer_argumentp(ctx); "
	"C_return( EVP_DigestInit_ex( ctx, type, impl));")
       ctx type impl)
      ctx
      (raise (sprintf "EVP_DigestInit failed for type ~A"
		      (EVP_MD_lname type)))))

(define (EVP_DigestUpdate ctx d cnt off)
  (if (or (fx> (fx+ off cnt) (string-length d))
	  (negative? off) (negative? cnt))
      (raise (sprintf
	      "EVP_DigestUpdate: parameter out of range (~A..~A) [0..~A]"
	      off (fx+ off cnt) (string-length d))))
  (if ((foreign-lambda*
	scheme-object
	((EVP_MD_CTX* ctx) (scheme-object data) (integer cnt) (integer off))
	;; "C_i_foreign_pointer_argumentp(ctx); "
	"C_i_foreign_string_argumentp(data); "
        "C_return( EVP_DigestUpdate( ctx, C_c_string(data)+off, (size_t) cnt));")
       ctx d cnt off)
      ctx
      (raise (sprintf "EVP_DigestUpdate failed"))))

(define (EVP_DigestFinal ctx packed)
  (let ((md (make-string/uninit (fx* (if packed 1 2) (EVP_MD_CTX_size ctx)))))
    (if (not ((foreign-lambda*
	       bool
	       ((EVP_MD_CTX* ctx) (scheme-object md))
	       "C_i_foreign_string_argumentp(md); "
	       "C_return( EVP_DigestFinal_ex( ctx, (C_uchar *)C_c_string(md), NULL));")
	      ctx md))
	(raise "EVP_DigestFinal failed"))
    (EVP_MD_CTX_destroy ctx)
    (if packed md (unpack! md))))

(define (EVP_Digest data type . packed+impl)
  (let* ((packed (and (pair? packed+impl) (car packed+impl)))
	 (impl (and (pair? packed+impl) (pair? (cdr packed+impl))
		    (cadr packed+impl)))
    	 (md (or ((foreign-lambda*
		   scheme-object
		   ((scheme-object data) (EVP_MD* type)
		    (scheme-object md) (ENGINE* impl))
		   "C_i_foreign_string_argumentp(data); "
		   "C_i_foreign_string_argumentp(md); "
		   "C_return( C_and( EVP_Digest(C_c_string(data),
                                                C_header_size(data),
                                                (C_uchar *)C_c_string(md),
                                                NULL, type, impl),
                                     md));")
		  data type
		  (make-string/uninit (* (if packed 1 2) (EVP_MD_size type)))
		  impl)
		 (raise "EVP_Digest failed"))))
    (if packed md (unpack! md))))

(define (EVP_MD_CTX_copy out in)
  (if ((foreign-lambda*
	bool
	((scheme-object out) (scheme-object in))
	"C_i_foreign_pointer_argumentp(out); "
	"C_i_foreign_pointer_argumentp(in); "
	"C_return( EVP_MD_CTX_copy((EVP_MD_CTX *)C_c_pointer_nn(out),
                                 (EVP_MD_CTX *)C_c_pointer_nn(in)));")
       out in)
      out
      (raise "EVP_MD_CTX_copy failed")))

;;; HMAC
;;; handling HMAC context

(define-foreign-type HMAC_CTX* (c-pointer "HMAC_CTX"))

(define (HMAC_size ctx)
  ((foreign-lambda* integer ((HMAC_CTX* ctx))
		    "C_return( HMAC_size( ctx ));") ctx))
#|
(define (HMAC_CTX_create)
  (let ((v ((foreign-lambda*
	     HMAC_CTX* ()
	     "HMAC_CTX *ctx=OPENSSL_malloc(sizeof *ctx); "
	     "HMAC_CTX_init(ctx); " "/* segfaults here if malloc fails */"
	     "C_return(ctx);"))))
    (set-finalizer! v HMAC_CTX_destroy)
    v))
|#

(define HMAC_CTX_create
  (foreign-lambda HMAC_CTX* "HMAC_CTX_new"))

(define (HMAC_CTX_destroy ctx)
  ((foreign-lambda void "HMAC_CTX_free" HMAC_CTX*) ctx) #f)

(define (HMAC_Init ctx key type . impl) ;; need HMAC_CTX_create or ~_init
  ((foreign-lambda
    void "HMAC_Init_ex" HMAC_CTX* c-string integer EVP_MD* ENGINE*)
   ctx key (string-length key) type (and (pair? impl) (car impl)))
  ctx)

(define (HMAC_Update ctx d cnt off)
  (if (or (fx> (fx+ off cnt) (string-length d))
	  (negative? off) (negative? cnt))
      (raise (sprintf
	      "HMAC_Update: parameter out of range (~A..~A) [0..~A]"
	      off (fx+ off cnt) (string-length d))))
  ((foreign-lambda*
    void
    ((HMAC_CTX* ctx) (scheme-object data) (integer cnt) (integer off))
    "C_i_foreign_string_argumentp(data); "
    "HMAC_Update( ctx, (C_uchar *)C_c_string(data)+off, (size_t) cnt);")
   ctx d cnt off)
  ctx)

(define (HMAC_Final ctx . packed)
  (let* ((packed (and (pair? packed) (car packed)))
	 (md (make-string/uninit (* (if packed 1 2) (HMAC_size ctx)))))
    ((foreign-lambda*
      void ((HMAC_CTX* ctx) (scheme-object md))
      "C_i_foreign_string_argumentp(md); "
      "HMAC_Final( ctx, (C_uchar *)C_c_string(md), NULL);")
     ctx md)
    (HMAC_CTX_destroy ctx)
    (if packed md (unpack! md))))

(define (HMAC data key type . packed)
  (let* ((packed (and (pair? packed) (car packed)))
	 (md (make-string/uninit (* (if packed 1 2) (EVP_MD_size type)))))
    ((foreign-lambda*
      void
      ((EVP_MD* type) (scheme-object key) (scheme-object d) (scheme-object md))
      "C_i_foreign_string_argumentp(key); "
      "C_i_foreign_string_argumentp(d); "
      "C_i_foreign_string_argumentp(md); "

      "HMAC( type, (C_uchar *) C_c_string(key), C_header_size(key),
                   (C_uchar *) C_c_string(d), C_header_size(d),
                   (C_uchar *) C_c_string(md), NULL); ")
     type key data md)
    (if packed md (unpack! md))))

;;;
(define *string-digest-chunk-size* 512)
(define *port-digest-chunk-size* 4096)

(define (string-digest data type . packed+impl)
  (let ((cs *string-digest-chunk-size*)
	(packed (and (pair? packed+impl) (car packed+impl)))
	(impl (and (pair? packed+impl) (pair? (cdr packed+impl))
		   (cadr packed+impl))))
    (let loop ((ctx (EVP_DigestInit (EVP_MD_CTX_create) type impl))
	       (off 0) (rest (string-length data)))
      (if (< rest cs)
	  (EVP_DigestFinal (EVP_DigestUpdate ctx data rest off) packed)
	  (loop (EVP_DigestUpdate ctx data cs off) (+ off cs) (- rest cs))))))

(define (port-digest port type . packed+impl)
  (let ((cs *port-digest-chunk-size*)
	(packed (and (pair? packed+impl) (car packed+impl)))
	(impl (and (pair? packed+impl) (pair? (cdr packed+impl))
		   (cadr packed+impl))))
    (let loop ((ctx (EVP_DigestInit (EVP_MD_CTX_create) type impl))
	       (data (read-string cs port)))
      (let ((rest (string-length data)))
	(if (< rest cs)
	    (EVP_DigestFinal (EVP_DigestUpdate ctx data rest 0) packed)
	    (loop (EVP_DigestUpdate ctx data rest 0)
		  (read-string cs port)))))))

(define (string-hmac data key type . packed+impl)
 (let ((cs *string-digest-chunk-size*)
       (packed (and (pair? packed+impl) (car packed+impl)))
       (impl (and (pair? packed+impl) (pair? (cdr packed+impl))
		  (cadr packed+impl)))
       (rest (string-length data)))
   (let loop ((ctx (HMAC_Init (HMAC_CTX_create) key type impl))
	      (off 0) (rest (string-length data)))
     (if (< rest cs)
	 (HMAC_Final (HMAC_Update ctx data rest off) packed)
	 (loop (HMAC_Update ctx data cs off) (+ off cs) (- rest cs))))))

(define (port-hmac port key type . packed+impl)
  (let ((cs *port-digest-chunk-size*)
	(packed (and (pair? packed+impl) (car packed+impl)))
	(impl (and (pair? packed+impl) (pair? (cdr packed+impl))
		   (cadr packed+impl))))
    (let loop ((ctx (HMAC_Init (HMAC_CTX_create) key type impl))
	       (data (read-string cs port)))
      (let ((rest (string-length data)))
	(if (< rest cs)
	    (HMAC_Final (HMAC_Update ctx data rest 0) packed)
	    (loop (HMAC_Update ctx data rest 0)
		  (read-string cs port)))))))

#;(define-macro (define-digests . types)
  `(begin
     ,@(map
	(lambda (type)
	  (let* ((type (if (string? type) type (symbol->string type)))
		 (name (string->symbol (string-append  type "-digest")))
		 (EVP_ (string->symbol (string-append "EVP_" type))))
	    `(define ,name
	       (let ((type (,EVP_)))
		 (lambda (obj)
		   ((if (string? obj) string-digest port-digest) obj type))))))
	types)))

#;(define-digests
  md-null md2 md4 md5 sha sha1 dss dss1 ecdsa sha224 sha256 sha384 sha512 ripemd160)

(define (md2-digest obj) (error "MD2 no longer available from openssl"))

(define
  md-null-digest
  (let
   ((type (EVP_md-null)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))
 #;(define
  md2-digest
  (let
   ((type (EVP_md2)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))
 (define
  md4-digest
  (let
   ((type (EVP_md4)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))
 (define
  md5-digest
  (let
   ((type (EVP_md5)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))
 (define
  sha1-digest
  (let
   ((type (EVP_sha1)))
   (lambda
    (obj . packed)
    ((if (string? obj) string-digest port-digest) obj type
     (and (pair? packed) (car packed))))))
 (define
  sha224-digest
  (let
   ((type (EVP_sha224)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))
 (define
  sha256-digest
  (let
   ((type (EVP_sha256)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))
 (define
  sha384-digest
  (let
   ((type (EVP_sha384)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))
 (define
  sha512-digest
  (let
   ((type (EVP_sha512)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))
 (define
  ripemd160-digest
  (let
   ((type (EVP_ripemd160)))
   (lambda
    (obj)
    ((if (string? obj) string-digest port-digest) obj type))))

#;(define-macro (define-hmacs . types)
  `(begin
     ,@(map
	(lambda (type)
	  (let* ((type (if (string? type) type (symbol->string type)))
		 (name (string->symbol (string-append  type "-hmac")))
		 (EVP_ (string->symbol (string-append "EVP_" type))))
	    `(define ,name
	       (let ((type (,EVP_)))
		 (lambda (obj key)
		   ((if (string? obj) string-hmac port-hmac) obj key type))))))
	types)))

#;(define-hmacs
  md-null md2 md4 md5 sha sha1 dss dss1 ecdsa sha224 sha256 sha384 sha512 ripemd160)

(define (md2-hmac obj key) (md2-digest obj))

 (define
  md-null-hmac
  (let
   ((type (EVP_md-null)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 #;(define
  md2-hmac
  (let
   ((type (EVP_md2)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 (define
  md4-hmac
  (let
   ((type (EVP_md4)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 (define
  md5-hmac
  (let
   ((type (EVP_md5)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 (define
  sha1-hmac
  (let
   ((type (EVP_sha1)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 (define
  sha224-hmac
  (let
   ((type (EVP_sha224)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 (define
  sha256-hmac
  (let
   ((type (EVP_sha256)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 (define
  sha384-hmac
  (let
   ((type (EVP_sha384)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 (define
  sha512-hmac
  (let
   ((type (EVP_sha512)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))
 (define
  ripemd160-hmac
  (let
   ((type (EVP_ripemd160)))
   (lambda
    (obj key)
    ((if (string? obj) string-hmac port-hmac) obj key type))))


#>
#include <openssl/x509v3.h>
#include <openssl/pem.h>
#include <openssl/asn1.h>

/*
List:       openssl-users
Subject:    Re: time_t from  ASN1_TIME
From:       Jay Case <Jay.Case () worldnet ! att ! net>
Date:       2003-11-02 23:58:18

Messy hack, but here's what I arrived at. Please yell out if you smell
anything too foul;

//==============================================================================
// Based on X509_cmp_time() for intitial buffer hacking.
//==============================================================================
*/
time_t
getTimeFromASN1(const ASN1_TIME * aTime)
{
	time_t lResult = 0;
	struct tm lTime;

	char lBuffer[24];

	char * pBuffer = lBuffer;

	size_t lTimeLength = aTime->length;
	char * pString = (char *)aTime->data;

	if (aTime->type == V_ASN1_UTCTIME)
	{
		if ((lTimeLength < 11) || (lTimeLength > 17))
                 {
                 	return 0;
                 }

		C_memcpy(pBuffer, pString, 10);
		pBuffer += 10;
		pString += 10;
	}
	else
	{
		if (lTimeLength < 13)
                 {
                 	return 0;
                 }

		C_memcpy(pBuffer, pString, 12);
		pBuffer += 12;
		pString += 12;
	}

	if ((*pString == 'Z') || (*pString == '-') || (*pString == '+'))
	{
		*(pBuffer++) = '0';
		*(pBuffer++) = '0';
	}
	else
	{
		*(pBuffer++) = *(pString++);
		*(pBuffer++) = *(pString++);
		// Skip any fractional seconds...
		if (*pString == '.')
		{
			pString++;
			while ((*pString >= '0') && (*pString <= '9'))
                         {
                         	pString++;
                         }
		}
	}

	*(pBuffer++) = 'Z';
	*(pBuffer++) = '\0';

	time_t lSecondsFromUCT;
	if (*pString == 'Z')
	{
		lSecondsFromUCT = 0;
	}
	else
	{
		if ((*pString != '+') && (pString[5] != '-'))
                 {
			return 0;
                 }

		lSecondsFromUCT = ((pString[1]-'0') * 10 + (pString[2]-'0')) * 60;
		lSecondsFromUCT += (pString[3]-'0') * 10 + (pString[4]-'0');
		if (*pString == '-')
		{
			lSecondsFromUCT = -lSecondsFromUCT;
		}
	}

	lTime.tm_sec  = ((lBuffer[10] - '0') * 10) + (lBuffer[11] - '0');
	lTime.tm_min  = ((lBuffer[8] - '0') * 10) + (lBuffer[9] - '0');
	lTime.tm_hour = ((lBuffer[6] - '0') * 10) + (lBuffer[7] - '0');
	lTime.tm_mday = ((lBuffer[4] - '0') * 10) + (lBuffer[5] - '0');
	lTime.tm_mon  = (((lBuffer[2] - '0') * 10) + (lBuffer[3] - '0')) - 1;
	lTime.tm_year = ((lBuffer[0] - '0') * 10) + (lBuffer[1] - '0');
         if (lTime.tm_year < 50)
         {
         	lTime.tm_year += 100; // RFC 2459
         }
	lTime.tm_wday = 0;
	lTime.tm_yday = 0;
	lTime.tm_isdst = 0;  // No DST adjustment requested

         lResult = mktime(&lTime);
         if ((time_t)-1 != lResult)
         {
         	if (0 != lTime.tm_isdst)
                 {
                 	lResult -= 3600;  // mktime may adjust for DST  (OS dependent)
                 }
         	lResult += lSecondsFromUCT;
         }
         else
         {
         	lResult = 0;
         }

	return lResult;
}



<#

#>
static void C_BIO_print_name(BIO *out, const char *title, X509_NAME *nm,
                unsigned long lflags)
{
    char *buf;
    char mline = 0;
    int indent = 0;

    if (title)
        BIO_puts(out, title);
    if ((lflags & XN_FLAG_SEP_MASK) == XN_FLAG_SEP_MULTILINE) {
        mline = 1;
        indent = 4;
    }
    if (lflags == XN_FLAG_COMPAT) {
        buf = X509_NAME_oneline(nm, 0, 0);
        BIO_puts(out, buf);
//        BIO_puts(out, "\n");
        OPENSSL_free(buf);
    } else {
        if (mline)
            BIO_puts(out, "\n");
        X509_NAME_print_ex(out, nm, indent, lflags);
//        BIO_puts(out, "\n");
    }
}

static int memout_bio_to_buffer(BIO *bio)
{
  BUF_MEM *p;
  size_t n=0;
  BIO_get_mem_ptr(bio, &p);
  n = C_OPENSSL_BUFFER_SIZE-1 < p->length ? C_OPENSSL_BUFFER_SIZE-1 : p->length;
  C_memcpy(buffer, p->data, n);
  buffer[n]='\0';
  BIO_free(bio);
  return n;
}

typedef void (*bio_3_arg_proc)(BIO *out, void* a1, void* a2, C_word a3);
static int call_with_memout_bio3(bio_3_arg_proc proc, void* a1, void* a2, C_word a3)
{
  BIO *bio=BIO_new(BIO_s_mem());
  proc(bio, a1, a2, a3);
  return memout_bio_to_buffer(bio);
}

typedef void (*bio_1_arg_proc)(BIO *out, void* a1);
static int call_with_memout_bio1(bio_1_arg_proc proc, void* a1)
{
  BIO *bio=BIO_new(BIO_s_mem());
  proc(bio, a1);
  return memout_bio_to_buffer(bio);
}
<#

(define-foreign-type X509*     (c-pointer "X509"))

(define-type :X509: *)

(define X509_new (foreign-lambda X509* "X509_new"))
(define (X509_free c)
  ((foreign-lambda void "X509_free" X509*) c) #f)

(define (X509_set_serialNumber! x509 n)
  (or ((foreign-lambda*
        bool ((X509* req) (integer n))
        "ASN1_INTEGER *s = ASN1_INTEGER_new(); ASN1_INTEGER_set(s, n);"
        "int result = X509_set_serialNumber(req, s);"
        "ASN1_INTEGER_free(s);"
        "C_return(result);")
       x509 n)
      (error "X509_set_serialNumber failed")))

(define X509_set_version (foreign-lambda bool "X509_set_version" X509* integer))

(define X509-pem
  (foreign-lambda*
   c-string ((X509* x509))
   "C_return(call_with_memout_bio1((bio_1_arg_proc)PEM_write_bio_X509, x509) > 0 ? buffer : NULL );"))

(define x509-pem X509-pem)

(define (x509-read-file fn)
  (let ((v ((foreign-lambda*
	     X509* ((c-string fn))
	     "X509 *c=NULL; FILE *f=fopen(fn, \"r\");\n"
	     "if( f != NULL) {"
             "  c=PEM_read_X509(f, NULL, NULL, NULL);"
	     "  fclose(f); }"
	     "C_return(c);")
	    fn)))
    (and v (set-finalizer! v X509_free))
    v))

(define (x509-read-string pem)
  (let ((v ((foreign-lambda*
	     X509* ((scheme-object pem))
	     "X509 *c=NULL;"
	     "BIO *bio=BIO_new_mem_buf(C_c_string(pem), C_header_size(pem));"
	     "c=PEM_read_bio_X509(bio, NULL, NULL, NULL);"
	     "BIO_free(bio);"
	     "C_return(c);")
	    pem)))
    (and v (set-finalizer! v X509_free))
    v))

(define-foreign-type ASN1_INTEGER* (c-pointer "ASN1_INTEGER"))

(define ASN1_INTEGER_free (foreign-lambda void "ASN1_INTEGER_free" ASN1_INTEGER*))

(define (wrap-reader reader proc)
  (lambda (x) (and-let* ((x (if (string? x) (reader x) x))) (proc x))))

(: X509-subject (:X509: --> string))
(define (X509-subject x509)
  ((foreign-lambda*
	     c-string ((X509* x509))
	     "X509_NAME * name = X509_get_subject_name(x509);"
	     "X509_NAME_oneline(name, buffer, C_OPENSSL_BUFFER_SIZE);"
	     ;; "X509_NAME_free(name);"
	     "C_return(buffer);")
   x509))

(: x509-subject ((or string :X509:) --> (or boolean string)))
(define x509-subject (wrap-reader x509-read-string X509-subject))

(define X509-subject-hash (foreign-lambda unsigned-integer "X509_subject_name_hash" X509*))

(define (x509-subject-hash x #!optional (converter (lambda (r) (number->string r 16))))
  (and-let* ((x (if (string? x) (x509-read-string x) x)))
	    (converter (X509-subject-hash x))))


(: X509-expiration-time ((or string :X509:) --> number))
(define (X509-expiration-time x509)
  ((foreign-lambda*
    long ((X509* x509))
    "ASN1_TIME *t = X509_get_notAfter(x509);"
    "C_return(getTimeFromASN1(t));")
   x509))

(: x509-expiration-time ((or string :X509:) --> (or boolean number)))
(define x509-expiration-time (wrap-reader x509-read-string X509-expiration-time))

(: X509-text (:X509: --> string))
(define X509-text
  (foreign-lambda*
   c-string ((X509* x509))
   "BIO *bio=BIO_new(BIO_s_mem());"
   "BUF_MEM *p; size_t n;"
   "X509_print(bio, x509);"
   "BIO_get_mem_ptr(bio, &p);"
   "n = C_OPENSSL_BUFFER_SIZE-1 < p->length ? C_OPENSSL_BUFFER_SIZE-1 : p->length;"
   "C_memcpy(buffer, p->data, n);"
   "buffer[n]='\\0';"
   "BIO_free(bio);"
   "C_return(buffer);"))

(: x509-text ((or string :X509:) --> (or boolean string)))
(define x509-text (wrap-reader x509-read-string X509-text))

(define-foreign-type EVP_PKEY* (c-pointer "EVP_PKEY"))
(define EVP_PKEY_free (foreign-lambda void "EVP_PKEY_free" EVP_PKEY*))

(define X509_get0_pubkey (foreign-lambda EVP_PKEY* "X509_get0_pubkey" X509*))

(define (X509_set_pubkey! x509 pkey)
  (or ((foreign-lambda bool "X509_set_pubkey" X509* EVP_PKEY*) x509 pkey)
      (error "X509_set_pubkey failed")))


(define X509_verify (foreign-lambda bool "X509_verify" X509* EVP_PKEY*))

(define-foreign-type X509REQ*     (c-pointer "X509_REQ"))

(define X509_REQ_new (foreign-lambda X509REQ* "X509_REQ_new"))
(define X509_REQ_free (foreign-lambda void "X509_REQ_free" X509REQ*))

(define X509_REQ_get0_pubkey (foreign-lambda EVP_PKEY* "X509_REQ_get0_pubkey" X509REQ*))

(define X509_REQ_verify (foreign-lambda bool "X509_REQ_verify" X509REQ* EVP_PKEY*))

(define X509req-pem
  (foreign-lambda*
   c-string ((X509REQ* req))
   "C_return(call_with_memout_bio1((bio_1_arg_proc)PEM_write_bio_X509_REQ, req) > 0 ? buffer : NULL );"))

(define x509req-pem X509req-pem)

(define (x509req-read-string pem)
  (let ((v ((foreign-lambda*
	     X509REQ* ((scheme-object pem))
	     "X509_REQ *c=NULL;"
	     "BIO *bio=BIO_new_mem_buf(C_c_string(pem), C_header_size(pem));"
	     "c=PEM_read_bio_X509_REQ(bio, NULL, NULL, NULL);"
	     "BIO_free(bio);"
	     "C_return(c);")
	    pem)))
    (and v (set-finalizer! v X509_REQ_free))
    v))

#;(define X509req-text
  (foreign-lambda*
   c-string ((X509REQ* req))
   "BIO *bio=BIO_new(BIO_s_mem());"
   "BUF_MEM *p; size_t n;"
   "unsigned long reqflag = 0;"
   ;; "X509_REQ_print_ex(bio, req, get_nameopt(), reqflag);"
   "X509_REQ_print(bio, req);"
   "BIO_get_mem_ptr(bio, &p);"
   "n = C_OPENSSL_BUFFER_SIZE-1 < p->length ? C_OPENSSL_BUFFER_SIZE-1 : p->length;"
   "C_memcpy(buffer, p->data, n);"
   "buffer[n]='\\0';"
   "BIO_free(bio);"
   "C_return(buffer);"))

(define X509req-text
  (foreign-lambda*
   c-string ((X509REQ* req))
   "unsigned long reqflag = 0;"
   "unsigned long nameopt = 0;"
   "C_return(call_with_memout_bio3((bio_3_arg_proc)X509_REQ_print_ex, req, (void*)nameopt, (C_word)reqflag) > 0 ? buffer : NULL );"))

(define x509req-text (wrap-reader x509req-read-string X509req-text))

(define X509req-subject
  (foreign-lambda*
   c-string ((X509REQ* req))
   "C_return(call_with_memout_bio3((bio_3_arg_proc)C_BIO_print_name, NULL, X509_REQ_get_subject_name(req), 0) > 0 ? buffer : NULL );"))

(define x509req-subject (wrap-reader x509req-read-string X509req-subject))

(define (X509_REQ_set_pubkey! req pkey)
  (or ((foreign-lambda bool "X509_REQ_set_pubkey" X509REQ* EVP_PKEY*) req pkey)
      (error "X509_REQ_set_pubkey failed")))

(define X509_REQ_set_version (foreign-lambda bool "X509_REQ_set_version" X509REQ* integer))

(define X509_REQ_add1_attr_by_utf8txt*
  (foreign-lambda*
   bool ((X509REQ* req) (c-string attname) (c-string attval))
   "C_return(X509_REQ_add1_attr_by_txt(req, attname, MBSTRING_UTF8, (unsigned char*)attval, -1));"))

(define (X509_REQ_add1_attr_by_utf8txt req attname attval)
  (or (X509_REQ_add1_attr_by_utf8txt* req attname attval)
      (error "X509_REQ_add1_attr_by_txt failed for utf8 value")))

;; X509_NAME

(define-foreign-type X509_NAME* (c-pointer "X509_NAME"))

(define X509_NAME_new (foreign-lambda X509_NAME* "X509_NAME_new"))
(define X509_NAME_free (foreign-lambda void "X509_NAME_free" X509_NAME*))

(define (with-X509name proc)
  (let ((name #f))
    (dynamic-wind
        (lambda () (set! name (X509_NAME_new)))
        (lambda () (proc name))
        (lambda () (X509_NAME_free name)))))

(define X509_get_subject_name (foreign-lambda X509_NAME* "X509_get_subject_name" X509*))
(define X509_REQ_get_subject_name (foreign-lambda X509_NAME* "X509_REQ_get_subject_name" X509REQ*))

(define X509_REQ_set_subject_name
  (foreign-lambda bool "X509_REQ_set_subject_name" X509REQ* X509_NAME*))

(define (X509_set_issuer_name! x509 name)
  (or ((foreign-lambda bool "X509_set_issuer_name" X509* X509_NAME*) x509 name)
      (error "X509_set_issuer_name failed")))

(define (X509_set_subject_name! x509 name)
  (or ((foreign-lambda bool "X509_set_subject_name" X509* X509_NAME*) x509 name)
      (error "X509_set_subject_name failed")))

(define X509_NAME_add_entry_by_utf8txt
  (foreign-lambda*
   bool ((X509_NAME* name) (c-string field) ;; (int type) -> MBSTRING_UTF8
         (scheme-object bytes)
         (int loc) (bool set))
   "C_return(X509_NAME_add_entry_by_txt(name, field, MBSTRING_UTF8,
             (unsigned char *)C_c_string(bytes),
             C_header_size(bytes), loc, set ? 0 : -1));"))

(define EVP_MD_CTX_new (foreign-lambda EVP_MD_CTX* "EVP_MD_CTX_new"))
(define EVP_MD_CTX_free (foreign-lambda void "EVP_MD_CTX_free" EVP_MD_CTX*))

(define (EVP_DigestSignInit pkey md)
  ((foreign-lambda*
    EVP_MD_CTX* ((EVP_MD_CTX* ctx) (EVP_PKEY* pkey) (EVP_MD* md))
    #<<EOF
    EVP_PKEY_CTX *pkctx = NULL;
    int success = EVP_DigestSignInit(ctx, &pkctx, md, NULL, pkey);
    if( !success ) EVP_MD_CTX_free(ctx);
    C_return( success ? ctx : NULL);
EOF
)
   (EVP_MD_CTX_new) pkey md))

(define X509_sign_ctx (foreign-lambda bool "X509_sign_ctx" X509* EVP_MD_CTX*))

(define (sign-with-sha256 x509 pkey)
  (and-let*
   ((ctx (EVP_DigestSignInit pkey (EVP_sha256)))
    (success (X509_sign_ctx x509 ctx)))
   (EVP_MD_CTX_free ctx)
   (or success (error "sign-with-sha256 failed"))))

#>
static int x509v3_sign_req(X509_REQ *req, EVP_MD *md, EVP_PKEY *pkey, C_word ext_cfg)
{
  X509V3_CTX ext_ctx;
  EVP_PKEY_CTX *pkctx = NULL;
  EVP_MD_CTX *mctx = EVP_MD_CTX_new();
  CONF *conf = NULL;
  BIO *ext_bio = NULL;
  int success = 0;
  /* Set up V3 context struct */
  X509V3_set_ctx(&ext_ctx, NULL, NULL, req, NULL, 0);
  if( ext_cfg ) {
    long int errline;
    ext_bio = BIO_new_mem_buf(C_c_string(ext_cfg), C_header_size(ext_cfg));
    conf = NCONF_new(NULL);
    success = ext_bio && conf && NCONF_load_bio(conf, ext_bio, &errline);
    if( !success ) goto done;
    X509V3_set_ctx_test(&ext_ctx);
    X509V3_set_nconf(&ext_ctx, conf);
    success = X509V3_EXT_REQ_add_nconf(conf, &ext_ctx, "default", req);
    // success = X509V3_EXT_REQ_add_nconf(conf, &ext_ctx, "default", req);
    if( !success ) goto done;
  }
  success = EVP_DigestSignInit(mctx, &pkctx, md, NULL, pkey);
  if( !success ) goto done;
  success = X509_REQ_sign_ctx(req, mctx);
done:
  BIO_free(ext_bio);
  NCONF_free(conf);
  EVP_MD_CTX_free(mctx);
  return success;
}

<#

(define X509v3-sign (foreign-lambda bool "x509v3_sign_req" X509REQ* EVP_MD* EVP_PKEY* scheme-object))

(define (x509v3-sign! req md pkey exts)
  (or (X509v3-sign req md pkey exts)
      (error "X509v3 request signing failed")))

#; `(,(openssl) "req" "-new"
		     "-utf8" ;; "-batch" "-multivalue-rdn"
		     ,@(if password '("-passin" "fd:0") '())
		     "-subj" ,(tc-escape-subjects subj)
		     "-config" ,config "-reqexts" "SAN"
		     "-key" ,keyfile "-days" ,(literal days))
;; key: PEM string
;; password: string
;; subj: '((key part ...) ...)
;; subjAlt: (list-of string)
(define (x509-create-certificate-request pkey password subj subjAlt #!optional enable-ca)
  (let ((pkey (read-private-key pkey password))
        (result (X509_REQ_new)))
    (set-finalizer! result X509_REQ_free)
    (X509_REQ_set_version result 2)
    (X509_REQ_set_pubkey! result pkey)
    (with-X509name
     (lambda (name)
       (for-each
        (lambda (s)
          (let ((key (car s)))
            (do ((v (cdr s) (cdr v))
                 (i 0 (+ i 1)))
                ((null? v))
              (or (X509_NAME_add_entry_by_utf8txt name key (car v) i (null? (cdr v)))
                  (error "failed to add subject component" key (car v))))))
        (reverse subj))
       (X509_REQ_set_subject_name result name)))
    ;; (X509_REQ_add1_attr_by_utf8txt result "basicConstraints" (if enable-ca "CA:TRUE" "CA:FALSE"))
    (x509v3-sign!
     result (EVP_sha256) pkey
     ;; Assemble V3 extensions
     (call-with-output-string
      (lambda (p)
	(display "basicConstraints=" p)
	(display (if enable-ca "CA:TRUE\n" "CA:FALSE\n") p)
	(if subjAlt
	    (for-each
	     (lambda (nm)
	       (display "subjectAltName=" p)
	       (display nm p)
	       (newline p))
	     subjAlt)))))
    result))

;; CA signing

(define (check-date-string! date)
  (or ((foreign-lambda* bool ((c-string date))
                        "C_return(ASN1_TIME_set_string_X509(NULL, date));")
       date)
      (error "date is invalid, it should be YYMMDDHHMMSSZ or YYYYMMDDHHMMSSZ")))

#>
extern int ASN1_TIME_set_string_X509(ASN1_TIME *, const char *);
static int set_cert_times(X509 *x, const char *startdate, const char *enddate,
                   int days)
{
    if (startdate == NULL || strcmp(startdate, "today") == 0) {
        if (X509_gmtime_adj(X509_getm_notBefore(x), 0) == NULL)
            return 0;
    } else {
        if (!ASN1_TIME_set_string_X509(X509_getm_notBefore(x), startdate))
            return 0;
    }
    if (enddate == NULL) {
        if (X509_time_adj_ex(X509_getm_notAfter(x), days, 0, NULL)
            == NULL)
            return 0;
    } else if (!ASN1_TIME_set_string_X509(X509_getm_notAfter(x), enddate)) {
        return 0;
    }
    return 1;
}
<#

(define set_cert_times (foreign-lambda bool "set_cert_times" X509* c-string c-string int))

(define X509_to_X509_REQ (foreign-lambda X509REQ* "X509_to_X509_REQ" X509* EVP_PKEY* EVP_MD*))


#>

static int copy_all_extensions(X509 *x, X509_REQ *req)
{
  int i, n;
  X509_EXTENSION *ext;
  STACK_OF(X509_EXTENSION) *exts = X509_REQ_get_extensions(req);
  n = X509v3_get_ext_count(exts);
  // fprintf(stderr, "Extensions %d\n", n);
  for(i=0; i<n; ++i) {
    ext = X509v3_get_ext(exts, i);
    X509_add_ext(x, ext, i);
    X509_EXTENSION_free(ext);
  }
  return(1);
}

<#

(define copy_all_extensions (foreign-lambda bool "copy_all_extensions" X509* X509REQ*))


;; cert-auth: CA(file)
;; key: (string key)
;; password: (string password)
;; serial-num: integer
;; input: request (PEM-string)
;; days: integer
(define (x509-create-signature cert-auth key password serial-num input days)
  (or (and (integer? days) (> days 0))
      (error "x509-create-signature illegal value for days" days))
  (let ((cacert (and cert-auth
                     (or (x509-read-string cert-auth)
                         (error "failed to load cacert"))))
        (req (if (string? input)
		 (or (x509req-read-string input)
		     (error "failed to load input to be signed"))
                 input)))
    (or (X509_REQ_verify req (X509_REQ_get0_pubkey req))
        (error "x509-create-signature failed to verify request"))
    (let ((md (EVP_sha256))
          (ret (X509_new)))
      (set-finalizer! ret X509_free)
      (X509_set_version ret 2) ;; v3
      (X509_set_serialNumber! ret serial-num)
      (X509_set_issuer_name!
       ret (if cacert (X509_get_subject_name cacert) (X509_REQ_get_subject_name req)))
      (or (set_cert_times ret #f #f days)
          (error "setting cert times failed"))
      (X509_set_subject_name! ret (X509_REQ_get_subject_name req))
      (X509_set_pubkey! ret (X509_REQ_get0_pubkey req))
      (or (copy_all_extensions ret req)
          (error "copy_all_extensions failed"))
      (let ((pkey (read-private-key key password)))
        (if pkey
            (sign-with-sha256 ret pkey)
            (error "failed to decode private key")))
      ret)))

;; **************** RSA *****************
#>
#include <openssl/rsa.h>
typedef struct pw_cb_data {
    const void *password;
    const char *prompt_info;
    } PW_CB_DATA;
static int password_callback(char *buf, int bufsiz, int verify, PW_CB_DATA *cb_data)
{
  strncpy(buf, cb_data->password, bufsiz);
  return strlen(buf);
}

#define RSA_DEFAULT_PRIME_NUM 2

int RSA_generate_multi_prime_key(RSA *rsa, int bits, int primes, BIGNUM *e_value, BN_GENCB *cb);

static int genrsa_cb(int p, int n, BN_GENCB *cb)
{
    char c = '*';

    if (!0)
        return 1;

    if (p == 0)
        c = '.';
    if (p == 1)
        c = '+';
    if (p == 2)
        c = '*';
    if (p == 3)
        c = '\n';
/*     BIO_write(BN_GENCB_get_arg(cb), &c, 1);
     (void)BIO_flush(BN_GENCB_get_arg(cb));
*/
    write(2,&c,1);
    return 1;
}

static C_char * C_generate_rsa_key(int num, char *passwd, char *enc_name)
{
  BIO *out=BIO_new(BIO_s_mem());
  BUF_MEM *p;
  size_t n = 0;
  BN_GENCB *cb = BN_GENCB_new();
  PW_CB_DATA cb_data;
  BIGNUM *bn = cb ? BN_new() : NULL;
  const BIGNUM *e;
  unsigned long f4 = RSA_F4;
  const EVP_CIPHER *enc = NULL;
  int primes = RSA_DEFAULT_PRIME_NUM;
  RSA *rsa = bn ? RSA_new() : NULL;

  // BN_GENCB_set(cb, genrsa_cb, bio_err);
  BN_GENCB_set(cb, genrsa_cb, (void*)2);

  if(!BN_set_word(bn, f4)) goto end;

  if( !RSA_generate_key_ex(rsa, num, bn, cb)) goto end;
  // if( !RSA_generate_multi_prime_key(rsa, num, primes, bn, cb)) goto end;

  RSA_get0_key(rsa, NULL, &e, NULL);
  if( passwd ) {
    enc = EVP_get_cipherbyname(enc_name);
   if( !enc ) goto end;
    cb_data.password = passwd;
    cb_data.prompt_info = NULL;
  }
  if (!PEM_write_bio_RSAPrivateKey(out, rsa, enc, NULL, 0,
                                     (pem_password_cb *)password_callback,
                                     &cb_data))
    goto end;
  BIO_get_mem_ptr(out, &p);
  n = C_OPENSSL_BUFFER_SIZE-1 < p->length ? C_OPENSSL_BUFFER_SIZE-1 : p->length;
  C_memcpy(buffer, p->data, n);
  end:
  buffer[n]='\0';
  BIO_free_all(out);
  BN_free(bn);
  BN_GENCB_free(cb);
  return( n>0 ? buffer : NULL );
}

<#

(define-foreign-type RSA*     (c-pointer "RSA"))

(define RSA_free (foreign-lambda void "RSA_free" RSA*))

(define %genrsa (foreign-lambda c-string "C_generate_rsa_key" int c-string c-string))

(define (genrsa bits password #!key (enc "des3"))
  (%genrsa bits password enc))

#;(define (rsa-text key)
  (cond
   ((string? key) ((foreign-lambda c-string "C_RSA_text_s" RSA*) key))
   (else (error "unknown RSA key type" key))))

(define (%PEM-read-private-key-from-string in password)
  ((foreign-lambda*
   EVP_PKEY* ((scheme-object pem) (c-string password))
   #<<EOF
   EVP_PKEY* pkey = NULL;
   PW_CB_DATA cb_data;
   BIO *key=BIO_new_mem_buf(C_c_string(pem), C_header_size(pem));
   cb_data.password = password;
   cb_data.prompt_info = NULL;
   pkey = PEM_read_bio_PrivateKey(key, NULL, (pem_password_cb *)password_callback, &cb_data);
   BIO_free(key);
   C_return(pkey);
EOF
) in password))

(define (read-private-key in password)
  (or (not password) (string? password)
      (error "PEM-read-private-key: invalid password" password))
  (cond
   ((string? in)
    (and-let* ((v (%PEM-read-private-key-from-string in password)))
              (set-finalizer! v EVP_PKEY_free)
              v))))

) ;; module openssl

(import (prefix openssl m:))
(define md5-digest m:md5-digest)
(define sha256-digest m:sha256-digest)
(define sha1-digest m:sha1-digest)

(define x509-read-file m:x509-read-file)
(define x509-read-string m:x509-read-string)
(define x509-subject m:x509-subject)
(define X509-text m:x509-text)
(define x509-expiration-time m:x509-expiration-time)

(define x509req-read-string m:x509req-read-string)
(define x509req-text m:x509req-text)

;; (define subtest
;;   (let ((count 0))
;;     (lambda (msg exp)
;;       (printf "Test ~A: ~A ~A\n" count msg (or (and exp "passed") "failed"))
;;       (set! count (+ count 1))
;;       exp)))

;; (define (test)
;;   (or (and
;;        (subtest
;; 	"unpack!"
;; 	(equal? "313233343536" (unpack! "123456......")))

;;        (subtest
;; 	"CTX handling"
;; 	(let ((ctx (EVP_MD_CTX_create)))
;; 	  (and (eq? (EVP_MD_CTX_init ctx) ctx)
;; 	       (eq? #f (EVP_MD_CTX_cleanup ctx))
;; 	       ;DANGER! (eq? #f (EVP_MD_CTX_destroy ctx))
;; 	       )))
;;       ;(subtest "(EVP_MD_CTX_cleanup #f)" (guard (ex (else #t)) (EVP_MD_CTX_cleanup #f)))
;;       ;(subtest "(EVP_MD_CTX_destroy #f)" (guard (ex (else #t)) (EVP_MD_CTX_destroy #f)))
;;        (subtest
;; 	"digest handling"
;; 	(and
;; 	 ;;(subtest "md_null" (equal? (EVP_md_null) (EVP_get_digestbyname "md_null")))
;; 	 (subtest "md2" (equal? (EVP_md2) (EVP_get_digestbyname "md2")))
;; 	 (subtest "md4" (equal? (EVP_md4) (EVP_get_digestbyname "md4")))
;; 	 (subtest "md5" (equal? (EVP_md5) (EVP_get_digestbyname "md5")))
;; 	 (subtest "sha" (equal? (EVP_sha) (EVP_get_digestbyname "sha")))
;; 	 (subtest "sha1" (equal? (EVP_sha1) (EVP_get_digestbyname "sha1")))
;; 	 (subtest "dss" (equal? (EVP_dss) (EVP_get_digestbyname "DSA-SHA")))
;; 	 (subtest "dss1" (equal? (EVP_dss1) (EVP_get_digestbyname "dss1")))
;; 	 (subtest "ecdsa" (equal? (EVP_ecdsa) (EVP_get_digestbyname "ecdsa-with-SHA1")))
;; 	 (subtest "sha224" (equal? (EVP_sha224) (EVP_get_digestbyname "sha224")))
;; 	 (subtest "sha256" (equal? (EVP_sha256) (EVP_get_digestbyname "sha256")))
;; 	 (subtest "sha384" (equal? (EVP_sha384) (EVP_get_digestbyname "sha384")))
;; 	 (subtest "sha512" (equal? (EVP_sha512) (EVP_get_digestbyname "sha512")))
;; 	 (subtest "ripemd160" (equal? (EVP_ripemd160) (EVP_get_digestbyname "ripemd160")))

;; 	 (subtest "MD_name.." (equal? (EVP_MD_name (EVP_get_digestbyname "md2"))
;; 				      (OBJ_nid2sn (EVP_MD_type (EVP_md2)))))))
;;        (subtest
;; 	"digest operations"
;; 	(and
;; 	 (subtest
;; 	  "md5 line"
;; 	  (let ((msg "Hallo"))
;; 	    (equal?
;; 	     (EVP_DigestFinal
;; 	      (EVP_DigestUpdate
;; 	       (EVP_DigestInit (EVP_MD_CTX_create) (EVP_md5))
;; 	       msg (string-length msg) 0))
;; 	     (EVP_Digest msg (EVP_get_digestbyname "md5")))))

;; 	 (subtest
;; 	  "string-digest 2*cs+99 sha512"
;; 	  (let ((msg (make-string (+ (* 2 *string-digest-chunk-size*) 99) #\a)))
;; 	    (equal? (string-digest msg (EVP_sha512))
;; 		    (EVP_Digest msg (EVP_get_digestbyname "sha512")))))

;; 	 (subtest
;; 	  "string-digest 1*cs sha256"
;; 	  (let ((msg (make-string *string-digest-chunk-size* #\b)))
;; 	    (equal? (string-digest msg (EVP_sha256))
;; 		    (EVP_Digest msg (EVP_get_digestbyname "sha256")))))

;; 	 (subtest
;; 	  "string-digest cs-1 md5"
;; 	  (let ((msg (make-string (- *string-digest-chunk-size* 1) #\c)))
;; 	    (equal? (string-digest msg (EVP_md5))
;; 		    (EVP_Digest msg (EVP_get_digestbyname "md5")))))

;; 	 (subtest
;; 	  "md5-digest -> fixed string"
;; 	  (equal? (md5-digest "a") "0cc175b9c0f1b6a831c399e269772661"))))

;;        (subtest
;; 	"HMAC CTX handling"
;; 	(let ((ctx (HMAC_CTX_create)))
;; 	  (and (eq? (HMAC_CTX_init ctx) ctx)
;; 	       (eq? #f (HMAC_CTX_cleanup ctx))
;; 	       ;;DANGER! (eq? #f (HMAC_CTX_destroy ctx))
;; 	       )))

;;        (subtest
;; 	"HMAC handling"
;; 	(let ((msg "Hallo"))
;; 	  (equal?
;; 	   (HMAC_Final
;; 	    (HMAC_Update
;; 	     (HMAC_Init (HMAC_CTX_create) "key" (EVP_md5))
;; 	     msg (string-length msg) 0))
;; 	   (HMAC msg "key" (EVP_md5)))))
;;        (subtest
;; 	"md5-hmac"
;; 	(let ((msg "Hallo"))
;; 	  (equal?
;; 	   (md5-hmac msg "key")
;; 	   (HMAC msg "key" (EVP_md5)))))
;;        (subtest
;; 	"string|port-digest"
;; 	(let ((msg (make-string (+ (* 2 *port-digest-chunk-size*) 99) #\a)))
;; 	  (and
;; 	   (equal? (md5-digest (open-input-string msg))
;; 		   (EVP_Digest msg (EVP_get_digestbyname "md5")))
;; 	   (equal? (md5-digest msg)
;; 		   (EVP_Digest msg (EVP_get_digestbyname "md5"))))))
;;        (subtest
;; 	"string|port-hmac"
;; 	(let ((msg (make-string (+ (* 2 *port-digest-chunk-size*) 99) #\a)))
;; 	  (and
;; 	   (equal? (md5-hmac (open-input-string msg) "key")
;; 		   (HMAC msg "key" (EVP_get_digestbyname "md5")))
;; 	   (equal? (md5-hmac msg "key")
;; 		   (HMAC msg "key" (EVP_get_digestbyname "md5"))))))
;;        )
;;       (error "test failed!")))

;; (for-each
;;  (lambda (cmd) (if (equal? cmd "test") (test)))
;;  (command-line-arguments))
