(declare
 (unit lalrgen)
 (usual-integrations)
;;NO (disable-interrupts)			; why bother?
 )

(module
 lalrgen
 (
  lalr-parser gen-lalr-parser
  )

 (import scheme)
 (import (except chicken add1 sub1))
 (import (only extras pretty-print))

 (import (prefix atomic c:))

 (include "../mechanism/notation/Lalr/lalr-gen.scm")

 (define-syntax lalr-parser
   (lambda (x r c)
     (apply gen-lalr-parser (cdr x))))

 )

(import (prefix lalrgen lalr:))
(define gen-lalr-parser lalr:gen-lalr-parser)
