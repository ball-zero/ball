;; (C) 2002, 2003 Jörg F. Wittenberger

;; chicken module clause for the Askemos 'step' (byzantine protocol
;; implementation) module .

(declare
 (unit step)
 ;;
 (usual-integrations))

(module
 step
 (
  collection-read
  collection-propose
  collection-accept
  collection-write
  collection-write-local
  custom-collection-read
  custom-collection-propose
  custom-collection-write
  custom-collection-write-local
  collection-update-tree collection-update-propose
  ;;
  $access-denied-reveals-creadentials
  raise-access-denied-condition
  read-only-plain-transformation
  public-place
  raise-access-denied-condition
  transaction-private-plain-transformation!
  transaction-public-plain-transformation!
  ;;
  kernel-extent-proxy-forward
  ;;
  reflexive-read-only-xslt-transformation
  reflexive-xslt-transformation-propose
  xslt-transformation-accept
  transaction-xslt-transformation!
  ;; Experimental
  bail/read bail/confirm
  bail/propose bail/submit
  bail/accept
  )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	(only atomic yield-system-load)
	srfi-1 srfi-13 (except srfi-18 raise) srfi-19 srfi-34 srfi-35 srfi-45
	data-structures extras
	openssl mailbox environments pcre timeout atomic parallel shrdprmtr util sqlite3
	protection tree notation function aggregate channel place-common pool place-ro)

(import (only place
	      $broadcast-debug-proposal prevent-pishing pishing-mode-compatible-forward-call))

(import msgcoll corexml webdavlo protocol)

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(include "../policy/webdav.scm")
(include "../mechanism/step.scm")

)

(import (prefix step m:))

(define public-place m:public-place)

(define $access-denied-reveals-creadentials m:$access-denied-reveals-creadentials)
(define raise-access-denied-condition m:raise-access-denied-condition)

(define collection-read m:collection-read)
(define collection-propose m:collection-propose)
(define collection-accept m:collection-accept)
(define collection-write m:collection-write)
(define collection-write-local m:collection-write-local)
(define custom-collection-read m:custom-collection-read)
(define custom-collection-propose m:custom-collection-propose)
(define custom-collection-write m:custom-collection-write)
(define custom-collection-write-local m:custom-collection-write-local)
(define collection-update-tree m:collection-update-tree)
(define collection-update-propose m:collection-update-propose)

(define read-only-plain-transformation m:read-only-plain-transformation)
(define transaction-private-plain-transformation! m:transaction-private-plain-transformation!)

(define reflexive-read-only-xslt-transformation m:reflexive-read-only-xslt-transformation)
(define reflexive-xslt-transformation-propose m:reflexive-xslt-transformation-propose)
(define xslt-transformation-accept m:xslt-transformation-accept)
(define transaction-xslt-transformation! m:transaction-xslt-transformation!)
(define bail/read m:bail/read)
(define bail/confirm m:bail/confirm)
(define bail/propose m:bail/propose)
(define bail/submit m:bail/submit)
(define bail/accept m:bail/accept)
