;;;; mailbox.scm - Thread-safe blocking queues
;;;; Modified by Jörg Wittenberger

;
; Copyright (c) 2000-2003, Felix L. Winkelmann
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following
; conditions are met:
;
;   Redistributions of source code must retain the above copyright notice, this list of conditions and the following
;     disclaimer.
;   Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
;     disclaimer in the documentation and/or other materials provided with the distribution.
;   Neither the name of the author nor the names of its contributors may be used to endorse or promote
;     products derived from this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
; AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
; OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;
; Send bugs, suggestions and ideas to:
;
; felix@call-with-current-continuation.org
;
; Felix L. Winkelmann
; Steinweg 1A
; 37130 Gleichen, OT Weissenborn
; Germany

;; Issues
;;
;; - Depends on "library.scm" declaring '(disable-interrupts)'.
;;
;; - Depends on thread internals! Layout of thread record, return value
;; for a resumed thread.
;;
;; - Uses ##sys#thread-unblock! of unit "scheduler".
;;
;; - Note use of 'undefined' returned value to signal an empty mailbox.

(declare
  (unit mailbox)
  (uses srfi-1 srfi-18 library scheduler)
  (disable-interrupts)
  (usual-integrations)
  (no-procedure-checks-for-usual-bindings)
  (inline)
  (fixnum)
  (bound-to-procedure
    ##sys#check-structure
    ##sys#structure?
    ##sys#delq
    ##sys#thread-unblock!
  	mailbox-timeout-exception?)
  (always-bound
    ##sys#current-thread)
 )

(module
 mailbox
 (
  ;;
  mailbox-timeout-exception?
  ;;
  make-mailbox
  mailbox?
  mailbox-name
  mailbox-empty?
  mailbox-send!
  mailbox-wait!
  mailbox-receive!
  mailbox-push-back!
  mailbox-push-back-list!
  ;;
  make-mailbox-cursor
  mailbox-cursor?
  mailbox-cursor-mailbox
  mailbox-cursor-next
  mailbox-cursor-rewind mailbox-cursor-extract-and-rewind!
  ;;
  ;;
  mailbox-clone
  mailbox-has-message?
  send-message! receive-message! receive-message-if-present!
  mailbox-drop-matching!
  mailbox-number-of-items		; avoid!
  )

 (import scheme chicken srfi-1 srfi-18)

;;;

(define-inline (%undefined? x)
  (##core#inline "C_undefinedp" x))

;;; From list code in "library.scm"

(define-inline (%null? l)
  (##core#inline "C_i_nullp" l) )

(define-inline (%car p)
  (##sys#slot p 0) )

(define-inline (%cdr p)
  (##sys#slot p 1) )

(define-inline (%set-cdr! p v)
  (##sys#setslot p 1 v) )

(define-inline (%last-pair lst0)
  (do ((lst lst0 (%cdr lst)))
       ((%null? (%cdr lst)) lst)) )

(define-inline (%delete-first x lst)
  (##sys#delq x lst) )

;;; Thread Operations

(define-inline (thread-blocked? thread)
  (eq? 'blocked (thread-state thread)) )

(define-inline (thread-unblock! thread)
  (##sys#thread-unblock! thread) )

(define-inline (blocked-thread-for-timeout? thread)
  (and (##sys#slot thread 4)
       (not (##sys#slot thread 11))) )

;;; From queue code in "data-structures.scm"
;;; All safety checks removed

;; Queue Accessors

(define-inline (%make-queue)
  (##sys#make-structure 'queue '() '()))

(define-inline (%queue-empty? q)
  (%null? (%queue-first-pair q)) )

(define-inline (%queue-first-pair q)
  (##sys#slot q 1) )

(define-inline (%queue-last-pair q)
  (##sys#slot q 2) )

(define-inline (%queue-first-pair-set! q v)
  (##sys#setslot q 1 v) )

(define-inline (%queue-last-pair-set! q v)
  (##sys#setslot q 2 v) )

;; Queue Operations

(define (%queue-add! q datum)
  (let ((new-pair (cons datum '())))
    (if (%null? (%queue-first-pair q))
        (%queue-first-pair-set! q new-pair)
        (%set-cdr! (%queue-last-pair q) new-pair) )
    (%queue-last-pair-set! q new-pair) ) )

(define (%queue-remove! q)
  (let* ((first-pair (%queue-first-pair q))
         (first-cdr (%cdr first-pair)))
    (%queue-first-pair-set! q first-cdr)
    (when (%null? first-cdr)
      (%queue-last-pair-set! q '()) )
    (%car first-pair) ) )

(define (%queue-push-back! q item)
  (let ((newlist (cons item (%queue-first-pair q))))
    (%queue-first-pair-set! q newlist)
    (when (%null? (%queue-last-pair q))
      (%queue-last-pair-set! q newlist))))

(define (%queue-push-back-list! q itemlist)
  (let* ((newlist (append itemlist (%queue-first-pair q)))
         (newtail (if (%null? newlist) '() (%last-pair newlist))))
    (%queue-first-pair-set! q newlist)
    (%queue-last-pair-set! q newtail)))

(define (%queue-extract-pair! q pair)
  ; Scan queue list until we find the item to remove
  (let loop ((this-pair (%queue-first-pair q)) (prev-pair '()))
    ; Found it?
    (if (eq? this-pair pair)
        ; then cut out the pair
        (let ((next-pair (%cdr this-pair)))
          ; At the head of the list, or in the body?
          (if (%null? prev-pair)
              (%queue-first-pair-set! q next-pair)
              (%set-cdr! prev-pair next-pair) )
          ; When the cut pair is the last item update the last pair ref.
          (when (eq? (%queue-last-pair q) this-pair)
            (%queue-last-pair-set! q prev-pair)) )
        ; else keep looking for the pair
        (loop (%cdr this-pair) this-pair) ) ) )

;;; Mailbox Support

;; Mailbox Accessors

(define-inline (%mailbox-queue mb)
  (##sys#slot mb 1) )

(define-inline (%mailbox-thread-queue mb)
  (##sys#slot mb 2) )

(define-inline (%mailbox-name mb)
  (##sys#slot mb 3) )

(define-inline (%mailbox-thread-queue-set! mb v)
  (##sys#setslot mb 2 v) )

;; Mailbox Exceptions

(define (make-mailbox-timeout-condition loc timeout-args)
  (make-composite-condition
   (make-property-condition 'exn 'location loc 'arguments timeout-args)
   (make-property-condition 'mailbox)
   (make-property-condition 'timeout)) )

;; Mailbox Threading

(define (ready-mailbox! mb)
  ; Ready oldest waiting thread
  (let ((ts (%mailbox-thread-queue mb)))
    (unless (%null? ts)
      (let ((thread (%car ts)))
        ; Pop thread queue
        (%mailbox-thread-queue-set! mb (%cdr ts))
        ; Ready the thread based on wait mode
        (if (thread-blocked? thread)
            (when (blocked-thread-for-timeout? thread)
              ; Sleeping, so wake early
              (thread-unblock! thread)
              (thread-signal! thread 'unblocked) )
            ; Suspended
            (thread-resume! thread) ) ) ) ) )

(define (wait-mailbox! loc mb . timeout-args)
  ; Push current thread on mailbox waiting queue
  (%mailbox-thread-queue-set!
   mb
   (append (%mailbox-thread-queue mb) (list ##sys#current-thread)))
  ; Timeout wanted?
  (if (pair? timeout-args)
      ; Yes, then must sleep until something happens
      (let ((early? #f))
        ; Sleep current thread until desired seconds elapsed
        (condition-case (thread-sleep! (%car timeout-args))
          (var () (if (eq? 'unblocked var) (set! early? #t) (signal var))))
        ; Do nothing when unblocked early, otherwise report timeout
        (unless early?
          ; Remove from wait queue
          (%mailbox-thread-queue-set!
           mb
           (%delete-first ##sys#current-thread (%mailbox-thread-queue mb)))
          ; Default result provided?
          (if (%null? (%cdr timeout-args))
              ; then signal an timeout exception
              (thread-signal! ##sys#current-thread
               (make-mailbox-timeout-condition loc timeout-args))
              ; else return default
              (let ((def (cadr timeout-args)))
                (if (%undefined? def)
                    (error loc "undefined default value for timeout")
                    def ) ) ) ) )
      ; No, then suspend until something delivered
      (thread-suspend! ##sys#current-thread) ) )

(define (wait-mailbox-if-empty! loc mb . timeout-args)
  (when (%queue-empty? (%mailbox-queue mb))
    (apply wait-mailbox! loc mb timeout-args) ) )

;;; Mailbox Globals

;; Mailbox Exceptions

(define mailbox-timeout-exception?
  (let ((exf (condition-predicate 'exn))
        (mbf (condition-predicate 'mailbox))
        (tmf (condition-predicate 'timeout)))
    (lambda (obj)
      (and (exf obj) (mbf obj) (tmf obj)) ) ) )

;; Mailbox Operations

(define (make-mailbox . name)
  (##sys#make-structure
   'mailbox
   (%make-queue) '() (optional name (gensym 'mb))) )

(define (mailbox? x)
  (##sys#structure? x 'mailbox) )

(define (mailbox-name mb)
  (##sys#check-structure mb 'mailbox 'mailbox-name)
  (%mailbox-name mb) )

(define (mailbox-send! mb x)
  (##sys#check-structure mb 'mailbox 'mailbox-send!)
  (%queue-add! (%mailbox-queue mb) x)
  (ready-mailbox! mb) )

(define (mailbox-wait! mb . timeout-args)
  (##sys#check-structure mb 'mailbox 'mailbox-wait!)
  (apply wait-mailbox-if-empty! 'mailbox-wait! mb timeout-args) )

(define (mailbox-receive! mb . timeout-args)
  (##sys#check-structure mb 'mailbox 'mailbox-receive!)
  (let ((res (apply wait-mailbox-if-empty! 'mailbox-receive! mb timeout-args)))
    (if (%undefined? res)
        (%queue-remove! (%mailbox-queue mb))
        res ) ) )

(define (mailbox-empty? mb)
  (##sys#check-structure mb 'mailbox 'mailbox-empty?)
  (%queue-empty? (%mailbox-queue mb)) )

;; (mailbox-push-back! mb item)
;; Pushes an item into the first position of a mb.

(define (mailbox-push-back! mb x)
  (##sys#check-structure mb 'mailbox 'mailbox-send!)
  (%queue-push-back! (%mailbox-queue mb) x)
  (ready-mailbox! mb) )

;; (mailbox-push-back-list! mb item-list)
;; Pushes the items in item-list back into the mailbox,
;; so that (car item-list) becomes the next receivable item.

(define (mailbox-push-back-list! mb x)
  (##sys#check-structure mb 'mailbox 'mailbox-send!)
  (%queue-push-back-list! (%mailbox-queue mb) x)
  (ready-mailbox! mb) )

;;; Mailbox Cursor (enumerate mailbox items)

;; Mailbox Cursor Accessors

(define-inline (%mailbox-cursor-mailbox mbc)
  (##sys#slot mbc 3) )

(define-inline (%mailbox-cursor-previous-pair mbc)
  (##sys#slot mbc 2) )

(define-inline (%mailbox-cursor-previous-pair-set! mbc v)
  (##sys#setslot mbc 2 v) )

(define-inline (%mailbox-cursor-next-pair mbc)
  (##sys#slot mbc 1) )

(define-inline (%mailbox-cursor-next-pair-set! mbc v)
  (##sys#setslot mbc 1 v) )

;; Mailbox Cursor Operations

(define (%mailbox-cursor-rewind mbc)
  (%mailbox-cursor-next-pair-set! mbc '())
  (%mailbox-cursor-previous-pair-set! mbc #f) )

(define (%mailbox-cursor-extract! mbc)
  (let ((previous-pair (%mailbox-cursor-previous-pair mbc)))
    ; Unless 'mailbox-cursor-next' has been called don't remove
    (when previous-pair
      (%queue-extract-pair!
       (%mailbox-queue (%mailbox-cursor-mailbox mbc))
       previous-pair) ) ) )

;; Mailbox Cursor Globals

(define (make-mailbox-cursor mb)
  (##sys#check-structure mb 'mailbox 'mailbox-cursor)
  (##sys#make-structure 'mailbox-cursor '() #f mb) )

(define (mailbox-cursor? x)
  (##sys#structure? x 'mailbox-cursor) )

(define (mailbox-cursor-mailbox mbc)
  (##sys#check-structure mbc 'mailbox-cursor 'mailbox-cursor-mailbox)
  (%mailbox-cursor-mailbox mbc) )

(define (mailbox-cursor-rewind mbc)
  (##sys#check-structure mbc 'mailbox-cursor 'mailbox-cursor-rewind)
  (%mailbox-cursor-rewind mbc) )

(define (mailbox-cursor-next mbc . timeout-args)
  (##sys#check-structure mbc 'mailbox-cursor 'mailbox-cursor-next)
  ; Waiting mailbox peek.
  ; Note that the 'previous-pair' is used as a flag here.
  (let ((mb (%mailbox-cursor-mailbox mbc))
        (active? (%mailbox-cursor-previous-pair mbc)))
    (let loop ((next-pair (%mailbox-cursor-next-pair mbc)))
      ; Anything next?
      (if (%null? next-pair)
          ; then get something
          (let ((q (%mailbox-queue mb)))
            ; An active cursor?
            (if active?
                ; then wait for something to be appended
                (let ((res (apply wait-mailbox! 'mailbox-cursor-next mb timeout-args)))
                  (if (%undefined? res)
                      ; then still scanning
                      (begin
                        (%mailbox-cursor-next-pair-set! mbc (%queue-last-pair q))
                        (loop (%mailbox-cursor-next-pair mbc)) )
                      ; else return timeout default value
                      res ) )
                ; else grab the start of a, probably, non-empty queue
                (let ((res (apply wait-mailbox-if-empty! 'mailbox-cursor-next mb timeout-args)))
                  (if (%undefined? res)
                      ; then still scanning
                      (begin
                        (%mailbox-cursor-next-pair-set! mbc (%queue-first-pair q))
                        (loop (%mailbox-cursor-next-pair mbc)) )
                      ; else return timeout default value
                      res ) ) ) )
          ; else peek into the queue
          (let ((item (%car next-pair)))
            (%mailbox-cursor-previous-pair-set! mbc next-pair)
            (%mailbox-cursor-next-pair-set! mbc (%cdr next-pair))
            item ) ) ) ) )

(define (mailbox-cursor-extract-and-rewind! mbc)
  (##sys#check-structure mbc 'mailbox-cursor 'mailbox-cursor-extract-and-rewind!)
  (%mailbox-cursor-extract! mbc)
  (%mailbox-cursor-rewind mbc) )

;;

(define (mailbox-number-of-items mb)
  (length (%queue-first-pair (%mailbox-queue mb))))

(define (mailbox-clone mailbox)
  (let ((result (##sys#make-structure 'mailbox (%make-queue) '() (%mailbox-name mailbox))))
    (%queue-push-back-list!
     (%mailbox-queue result) (%queue-first-pair (%mailbox-queue result)))
    result))

(define (mailbox-has-message? mailbox message equal)
  (find (lambda (x) (equal x message)) (%queue-first-pair (%mailbox-queue mailbox))))

(define send-message! mailbox-send!)

(define (receive-message! mailbox) (mailbox-receive! mailbox))

(define (receive-message-if-present! ch)
  (##sys#check-structure ch 'mailbox 'mailbox-receive!)
  (if (%queue-empty? (%mailbox-queue ch))
      (values #f #f)
      (values (%queue-remove! (%mailbox-queue ch)) #t)) )

(define (mailbox-drop-matching! mailbox pred)
  (define c (make-mailbox-cursor mailbox))
    (let loop ()
      (cond
       ((mailbox-empty? mailbox) #t)
       ((pred (mailbox-cursor-next c 0))
	(mailbox-cursor-extract-and-rewind! c) (loop))
       (else #f))))

) ;; module mailbox
