(define-macro (guard . form)
  (let* ((clause (or (and (pair? form) (car form))
                     (error "guard: syntax error in" form)))
         (body (cdr form))
	 (condition (gensym))
	 (handler-k (gensym))
	 (return (gensym))
	 (oldh (gensym)))
    `((call-with-current-continuation
       (lambda (,return)
	 (let ((,oldh (current-exception-handler)))
	   (with-exception-handler
	    (lambda (,condition)
	      (with-exception-handler
	       ,oldh
	       (call-with-current-continuation
		(lambda (,handler-k)
		  (,return (lambda ()
			    ((lambda (,(car clause))
			       ,(let loop ((clauses (cdr clause)))
				  (if (null? clauses)
				      `(raise ,(car clause))
				      (let ((c (car clauses)))
					(cond
					 ((eq? 'else (car c))
					  (if (null? (cdr c))
					      '#f
					      (if (null? (cddr c))
						  (cadr c)
						  `(begin . ,(cdr c)))))
					 ((and (pair? c) (pair? (cdr c)) (eq? '=> (cadr c)))
					  (let ((v (gensym)))
					    `(let ((,v ,(car c)))
					       (if ,v (,(caddr c)) ,(loop (cdr clauses))))))
					 ((and (pair? c) (null? (cdr c)))
					  (let ((v (gensym)))
					    `(let ((,v ,(car c)))
					       (if ,v ,v ,(loop (cdr clauses))))))
					 ((pair? c)
					  `(if ,(car c)
					       ,(if (null? (cddr c))
						    (cadr c)
						    `(begin . ,(cdr c)))
					       ,(loop (cdr clauses))))
					 (else (error "guard syntax error in ~a" c)))))))
			     ,condition)))))))
	    (lambda ()
	      (##sys#call-with-values
	       (lambda ()
		 ,(if (and (pair? body) (null? (cdr body)))
		      (car body) `(begin . ,body) ))
	       (lambda args
		 (,return (lambda () (##sys#apply ##sys#values args)))) ) ) )) ) ))))
