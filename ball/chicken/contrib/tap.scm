(declare
 (unit tap)
 (uses srfi-1 srfi-13 srfi-18 ports)
 (import "util-chicken.scm")
 (fixnum-arithmetic)
 (usual-integrations)
 (export make-input-tap make-output-tap tap-main-port tap-port)
 )

(define string-ref++
  (foreign-lambda*
   char
   ((scheme-pointer buf) ((c-pointer integer) i))
   "char *p=(char *)buf; return(p[(*i)++]);"))

(define make-internal-pipe
  (let ([make-input-port make-input-port]
	[make-output-port make-output-port]
	[make-mutex make-mutex]
	[make-queue make-queue]
	[make-condition-variable make-condition-variable]
	[string-length string-length]
	[string-ref string-ref]
	(nothing (lambda () #t)))
    (lambda args
      (define name (or (and (pair? args) (car args))
		       'internal-pipe))
      (let-location
       ((off integer 0))
       (let ((mutex (make-mutex name))
	     (condition (make-condition-variable name))
	     (queue (make-queue))
	     (buf #f))
	 (define (eof?) (eq? #!eof buf))
	 (define (buf-empty?) (or (not buf) (fx>= off (string-length buf))))
	 (define (read!)
	   (let loop ()
	     (if (eof?) buf
		 (if (buf-empty?)
		     (begin
		       (mutex-lock! mutex)
		       (if (buf-empty?)
			   (if (queue-empty? queue)
			       (begin
				 (mutex-unlock! mutex condition)
				 (loop))
			       (begin
				 (set! buf #f)
				 (set! buf (queue-remove! queue))
				 (set! off 1)
				 (if (eof-object? buf) buf
				     (let ((c (string-ref buf 0)))
				       (mutex-unlock! mutex)
				       c))))
			   (let ((c (string-ref buf off)))
			     (set! off (add1 off))
			     (mutex-unlock! mutex)
			     c)))
		     (string-ref++ buf (location off))))))
	 (define (ready?)
	   (and (not (eof?))
		(or (not (buf-empty?))
		    (not (queue-empty? queue)))))
	 (define (write! s)
	   (if (or (and (string? s) (fx> (string-length s) 0))
		   (eof-object? s))
	       (begin
		 (mutex-lock! mutex)
		 (queue-add! queue s)
		 (condition-variable-signal! condition)
		 (mutex-unlock! mutex) )))
	 (values
	  (make-input-port read! ready? nothing)
	  (make-output-port write! (lambda () (write! #!eof)))))))))

(define-record-type <tap> (allocate-tap source dir ports) tap?
  (source tap-source)
  (dir tap-dir)
  (ports tap-ports set-tap-ports!))

(define (make-input-tap port)
  (allocate-tap port 'input '()))

(define (make-output-tap port)
  (allocate-tap port 'output '()))

(define tap-main-port
  (let ([make-input-port make-input-port]
	[eof-object? eof-object?]
	[read-char read-char])
    (define (input-tap-port tap)
      (define cur #f)
      (define (eof?) (eq? #!eof cur))
      (define (send c)
	(for-each (lambda (p) (display c p)) (tap-ports tap))
	c)
      (define (read!)
	(cond
	 ((eof-object? cur) cur)
	 (cur (let ((c cur)) (set! cur #f) (send c)))
	 (else (let ((c (read-char (tap-source tap))))
		 (if (eof-object? c) (set! cur c))
		 (send c)))))
      (define (ready?)
	(if cur (not (eof-object? cur))
	    (begin (set! cur (read-char (tap-source tap))) (ready?))))
      (define (close)
	(close-input-port))
      (make-input-port read! ready? close))
    (define (output-tap-port tap)
      (define closed #f)
      (define (write! s)
	(if (not closed)
	    (begin
	      (for-each (lambda (p) (display s p)) (tap-ports tap))
	      (if (eof-object? s) (set! closed #t)))))
      (define (close) (write! #!eof))
      (make-output-port write! close))
    (lambda (tap)
      ((case (tap-dir tap)
	 ((input) input-tap-port)
	 ((output) output-tap-port))
       tap))))

(define tap-port
  (let ([make-input-port make-input-port]
	[eof-object? eof-object?]
	[read-char read-char])
    (lambda (tap)
      (receive (in out) (make-internal-pipe)
	       (set-tap-ports! tap (cons out (tap-ports tap)))
	       in))))
