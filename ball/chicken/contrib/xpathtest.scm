(declare
 (uses extras library posix srfi-1 srfi-13 regex)
 (usual-integrations)
 ;; (disable-interrupts)
 )

(cond-expand
 [paranoia]
 [else
  (declare
    (no-bound-checks)
    (bound-to-procedure
     ##sys#check-string ##sys#check-exact ##sys#make-pointer ##sys#cons ##sys#size ##sys#slot) ) ] )

(cond-expand
 [unsafe
  (eval-when (compile)
    (define-macro (##sys#check-structure x y) '(##core#undefined))
    (define-macro (##sys#check-range x y z) '(##core#undefined))
    (define-macro (##sys#check-pair x) '(##core#undefined))
    (define-macro (##sys#check-list x) '(##core#undefined))
    (define-macro (##sys#check-symbol x) '(##core#undefined))
    (define-macro (##sys#check-string x) '(##core#undefined))
    (define-macro (##sys#check-char x) '(##core#undefined))
    (define-macro (##sys#check-exact x) '(##core#undefined))
    (define-macro (##sys#check-port x) '(##core#undefined))
    (define-macro (##sys#check-number x) '(##core#undefined))
    (define-macro (##sys#check-byte-vector x) '(##core#undefined)) ) ]
 [else] )

(define bind-exit call-with-current-continuation)

(define (debug lbl v)
  (format (current-error-port)"~a ~a\n~!" lbl v)
  v)

;; (C) 2002 J�rg F. Wittenberger see http://www.askemos.org

(define (xpath:parse str)
  (receive (end value) (xpath:location-path str 0) value))

;; SSAX.scm stolen functions

(define (SSAX:ncname-starting-char? a-char)
  (and (char? a-char)
    (or
      (char-alphabetic? a-char)
      (char=? #\_ a-char))))

;; -------------------------------------------------------------------------

(define xml:ncname-starting-char? SSAX:ncname-starting-char?)

;; FIXME URGENT wire these correctly
(define (xml:extender? c) #f)
(define (xml:combining-char? c) #f)
(define xml:digit? char-numeric?)
(define xml:letter? char-alphabetic?)
(define (xml:ncname-char? c)
  (or (xml:letter? c) (xml:digit? c)
      (eqv? c #\.) (eqv? c #\-) (eqv? c #\_)
      (xml:combining-char? c) (xml:extender? c)))

;; -------------------------------------------------------------------------

(define (xpath:ncname str off)
  (let ((l (string-length str)))
    (if (and (fx< off l) (xml:ncname-starting-char? (string-ref str off)))
        (do ((p (add1 off) (add1 p)))
            ((or (eqv? p l) (not (xml:ncname-char? (string-ref str p))))
             (values p (substring str off p))))
        (values #f #f))))

(define (xpath:qname str off)
  (receive (e n1) (xpath:ncname str off)
           (if (and e
                    (fx< e (string-length str))
                    (eqv? (string-ref str e) #\:))
               (receive (e n2) (xpath:ncname str (add1 e))
                        (values e (list ': n2 n1)))
               (values e n1))))

(define (xpath:error msg str off)
  (error "xpath error:~a:~a ~a" str off msg))

(define-macro (xpath:skip-ws string offset)
  `(do ((i ,offset (add1 i)))
       ((or (eqv? i (string-length ,string))
            (not (char-whitespace? (string-ref ,string i))))
        i)))

;; There should be a test not to match on node-type-test's
(define (xpath:function-name str off)
  (receive (end name) (xpath:ncname str off)
           (if end
               (receive
                (e0 v) (looking-at-open-paren str (xpath:skip-ws str end))
                (if e0
                    (values e0 name)
                    (values #f off)))
               (values #f off))))


;;** xpath match constructors

;; Package a node test.  These are going to be simillar to those from
;; sxpath, but tailored for use with a xpath parser and later the xslt
;; implementation.

(define-macro (make-node-match expr)
  (if (eq? (car expr) 'construct)
      `(lambda ,(reverse (cadr expr)) . ,(cddr expr))
      (error "syntax error: not a construction rule ~a\n" expr)))

;; This macro saves the following code (mostly yet) from knowledge
;; about the parsing method.  It's sort of optimized to parse from
;; short strings.  All whitespace after matched expressions is
;; consumed before return.
;;
;; Usage:
;;
;; (xpath:y <source-string> <start-offset> <grammar-rules>)

;; where <grammar-rules> is (<rule> ...) and <rule>:
;; <match-rule> | <else-rule> .
;; The <else-rule> creates a default value.
;; <match-rule> is: (<start-rule> <other-rules>* <value-expression>)
;;
;; All rules are (effectivly, but that will become transparent) two
;; ari functions taking a string to parse from and a start offset and
;; return two values, 1st: #f or the offset to continue parsing from
;; and the semantic value of the parsed rule.  In case of missmatch
;; the second return value is abused to carry the last offset, because
;; the following macro needed it.  (Someone with more macro experience
;; please fix it.)
;;
;; <start-rule> is just a expression parser as described above.
;; <other-rules> come as: (<start-rule> <missmatch-handler>) with
;; <missmatch-handler> being a string to emmit as error message or the
;; symbol :optional in which case the value #f is recorded for the
;; position in the <value-expression>.  <value-expression> is a lambda
;; expression which takes as many parameters as there are start- and
;; other- rules.  Note: the parameters are in reverse order.

;; KLUDGE: I can't get the make-node-match macro to be visible now.
;; Please fix it.

(define-macro (xpath:y string offset lst)
  (let loop ((lst lst))
    (cond
     ((null? lst) `(values #f ,offset))
     ((eq? (caar lst) 'else) `(values ,offset (begin ,@(cdar lst))))
     (else
      `(receive
        (end value) (,(caar lst) ,string ,offset)
        (if end
            ;; Duplicated from 11 lines down for common special case.
            ,(if (null? (cddar lst))
                 `(values end ,(cond
                                ((eq? (cadar lst) 'identity) 'value)
                                ((symbol? (cadar lst))
                                 (list (cadar lst) 'value))
                                (else (list ;(make-node-match (cadar lst))
                                       (if (eq? (car (cadar lst)) 'construct)
                                           `(lambda ,(reverse (cadr (cadar lst))) . ,(cddr (cadar lst)))
                                           (error "syntax error: not a construction rule ~a\n" (cadar lst)))
                                            'value))))
                 (let loop ((end `(xpath:skip-ws ,string end))
                            (vals '(list value))
                            (steps (cdar lst)))
                   `(let ((vv ,vals))
                      ,(if (null? (cdr steps))
                           `(values ,end
                                    (apply
                                     ,(if (symbol? (car steps))
                                          (car steps)
                                          ;;(make-node-match (car steps))
                                          (if (eq? (car (car steps)) 'construct)
                                              `(lambda ,(reverse (cadr (car steps))) . ,(cddr (car steps)))
                                              (error "syntax error: not a construction rule ~a\n" (car steps)))
                                          )
                                     vv))
                           `(receive
                             (e0 value) (,(caar steps) ,string ,end)
                             (if e0
                                 ,(loop `(xpath:skip-ws ,string e0)
                                        '(cons value vv)
                                        (cdr steps))
                                 ,(if (eq? (cadar steps) ':optional)
                                      (loop 'value
                                            '(cons #f vv)
                                            (cdr steps))
                                      `(xpath:error ,(cadar steps)
                                                    ,string end))))))))
            ,(loop (cdr lst))))))))

(define-macro (define-xpath-rule symbol . body)
  `(define (,(string->symbol (string-append "xpath:" (symbol->string symbol)))
            string offset)
(debug ',symbol offset)
            (xpath:y string offset ,body)))

;; All these looking-at-* functions should really be done using
;; regular expressions.  Just there's no standard in scheme, it's a
;; pity.

(define (looking-at-- string offset)
  (values
   (and (fx< offset (string-length string))
        (eqv? (string-ref string offset) #\-)
        (add1 offset)) #f))

(define (looking-at-comma string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\,)
               (add1 offset)) #f))

(define (looking-at-bar string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\|)
               (add1 offset)) '*union*))

(define (looking-at-dollar string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\$)
               (add1 offset)) '*varef*))

(define (looking-at-// string offset)
  (values (and (fx< offset (sub1 (string-length string)))
               (eqv? (string-ref string offset) #\/)
               (eqv? (string-ref string (add1 offset)) #\/)
               (+ offset 2)) #f))

(define (looking-at-/ string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\/)
               (add1 offset)) #f))

(define (looking-at-.. string offset)
  (values (and (fx< offset (sub1 (string-length string)))
               (eqv? (string-ref string offset) #\.)
               (eqv? (string-ref string (add1 offset)) #\.)
               (+ offset 2)) #f))

(define (looking-at-. string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\.)
               (add1 offset)) #f))

(define (looking-at-at string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\@)
               (add1 offset)) #f))

(define (looking-at-open-paren string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\()
               (add1 offset)) #f))

(define (looking-at-close-paren string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\))
               (add1 offset)) #f))

(define (looking-at-open-brace string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\[)
               (add1 offset)) #f))

(define (looking-at-close-brace string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\])
               (add1 offset)) #f))

(define (looking-at-star string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\*)
               (add1 offset)) #f))

(define (looking-at-colon string offset)
  (values (and (fx< offset (string-length string))
               (eqv? (string-ref string offset) #\:)
               (add1 offset)) #f))

;(define looking-at-or
;  (let ((re (reg-expr->proc '(prefix (seq "or" (not alpha))))))
;    (lambda (a b)
;      (receive (s e) (re a b) (and e (values (sub1 e) '*or*))))))

(define (looking-at-or a b)
  (if (and
       (> (string-length a) (add1 b))
       (eqv? (string-ref a b) #\o)
       (eqv? (string-ref a (add1 b)) #\r)
       (not (char-alphabetic? (string-ref a (+ b 2)))))
      (values (+ b 2) '*or*)
      (values #f #f)))

;(define looking-at-and
;  (let ((re (reg-expr->proc '(prefix (seq "and" (not alpha))))))
;    (lambda (a b)
;      (receive (s e) (re a b) (and e (values (sub1 e) '*and*))))))

(define (looking-at-and a b)
  (if (and
       (> (string-length a) (+ b 3))
       (eqv? (string-ref a b) #\a)
       (eqv? (string-ref a (add1 b)) #\n)
       (eqv? (string-ref a (+ b 2)) #\d)
       (not (char-alphabetic? (string-ref a (+ b 3)))))
      (values (+ b 3) '*and*)
      (values #f #f)))

(define (xpath:location-path str offset)
  (let ((off (xpath:skip-ws str offset)))
    (xpath:y
     str off
     ((looking-at-//                    ; [10]
       (xpath:relative-location-path "expected relative location path")
       (construct (dummy step)
                  `(node-reduce
                    (node-descendant-or-self (node-typeof? '*any*))
                    ,step)))
      (looking-at-/
       (xpath:relative-location-path :optional)
       (construct (dummy v) (or v '())))
      (xpath:relative-location-path identity)
      (else (xpath:error "expected location path" str off))))))

(define-xpath-rule relative-location-path-rest
  (looking-at-//
   (xpath:relative-location-path "expected relative location path")
   (construct (dummy step)
              `(node-reduce
                (node-descendant-or-self (node-typeof? '*any*))
                ,step)))
  (looking-at-/
   (xpath:relative-location-path "expected relative location path")
   (construct (dummy v) v)))

(define-xpath-rule relative-location-path
  (xpath:step
   (xpath:relative-location-path-rest :optional)
   (construct (step rest)
     (if (and rest (not (null? rest)))
         `(node-reduce ,step . ,rest)
         step))))

(define-xpath-rule step
  (looking-at-.. (construct (v) 'dotdot))
  (looking-at-. (construct (v) `(sxp:filter (node-typeof? '*any*))))
  (xpath:axis-specifier
   (xpath:node-test "no node test found")
   (xpath:predicate :optional)
   (construct (axis node-test predicate)
              (if predicate
                  `(node-reduce ,axis ,node-test ,predicate)
                  (list 'node-reduce axis node-test)))))

(define xpath:axis-specifier-list
  '( "ancestor"
     "ancestor-or-self"
     "attribute"
     "child"
     "descendant"
     "descendant-or-self"
     "following"
     "following-sibling"
     "namespace"
     "parent"
     "preceding"
     "preceding-sibling"
     "self"))

(define xpath:axis-specifier-indicators
  `(("ancestor" . ::ancestor)
    ("ancestor-of-self" . ::ancestor-of-self)
    ("attribute" . node-attributes)
    ("child" . sxp:child)
    ("descendant" . node-closure)
    ("descendant-of-self" . node-descendant-or-self)
    ("following" . ::following)
    ("following-sibling" . ::following-sibling)
    ("namespace" . node-namespaces)
    ("parent" . ::parent)
    ("preceding" . ::preceding)
    ("preceding-sibling" . ::preceding-sibling)
    ("self" . node-self)))

;; xpath:axis-specifier-read uses regular expressions, hence it's
;; defined elsewhere.

(define-xpath-rule axis-specifier
  (looking-at-at (construct (v) 'node-attributes))
  (xpath:axis-specifier-read (construct (x) x))
  (else 'sxp:child))                      ; default axis

(define-xpath-rule node-test
  (xpath:node-type-test
   (xpath:literal :optional)            ; BUG this is actually only valid
                                        ; for pi's
   (looking-at-close-paren "missing ')'")
   (construct (type l cp) (or (and l (list type l)) type)))
  (xpath:name-test identity))

;; xpath:literal-regex-dq and xpath:literal-regex-sq use regex ->
;; other file.

(define (xpath:literal str off)
  (receive (s e v) (xpath:literal-regex-sq (debug 'TRY str) off)
           (if (debug 'IS-SQ e) (values e v)
               (receive (s e v) (xpath:literal-regex-dq str off)
                        (values e (if e v off))))))

(define-xpath-rule name-test
  (looking-at-star (construct (v) `(sxp:filter (node-typeof? '*))))
  (xpath:ncname
   (xpath:local-name-test :optional)
   (construct (name local)
     (if local `(node-reduce (ntype-namespace-id? ',(string->symbol name)) ,local)
         `(sxp:filter (node-typeof? ',(string->symbol name)))))))

(define-xpath-rule local-name-test-after-colon
  (looking-at-star (construct (x) `(sxp:filter (node-typeof? '*any*))))
  (xpath:ncname
   (construct
    (name) `(sxp:filter (node-typeof? ',(string->symbol name))))))

(define-xpath-rule local-name-test
  (looking-at-colon
   (xpath:local-name-test-after-colon  "expected either '*' or NCname")
   (construct (dummy local) local)))

(define xpath:node-type-list
  '( "comment"
     "text"
     "processing-instruction"
     "node"))

(define xpath:node-type-indicators
  `(("comment" node-typeof? *comment*)
    ("text"    node-typeof? *text*)
    ("processing-instruction" node-typeof? *PI*)
    ("node"    node-typeof? *any*)))

;; xpath:node-type-test see again regex file.

(define-xpath-rule predicate
  (looking-at-open-brace
   (xpath:expr "expected expression")
   (looking-at-close-brace "missing ']'")
   (construct (ob expr cb) expr)))

(define (xpath:left-assoc rest left)    ; The arguments ar reverse ordered.
  (let loop ((left left) (rest (or rest '())))
    (if (null? rest)
        left
        (loop `(,(car rest) ,left ,(cadr rest)) (cddr rest)))))

(define (xpath:collect-rest rest ex op) ; The arguments ar reverse ordered.
  (if rest `(,op ,ex . ,rest) (list op ex)))

(define-xpath-rule or-expr-rest
  (looking-at-or
   (xpath:or-expr "expected an and expression")
   (xpath:or-expr-rest :optional)
   (construct (op ex rest) (if rest `(sxp:or ,ex ,rest) ex))))

(define-xpath-rule or-expr
  (xpath:and-expr
   (xpath:or-expr-rest :optional)
   (construct (l r) (if r `(sxp:or ,l ,r) l))))

(define xpath:expr xpath:or-expr)

(define-xpath-rule and-expr-rest
  (looking-at-and
   (xpath:equal-expr "expected equality expression")
   (xpath:and-expr-rest :optional)
   (construct (op ex rest) (if rest `(sxp:and ,ex ,rest) ex))))

(define-xpath-rule and-expr
  (xpath:equal-expr
   (xpath:and-expr-rest :optional) 
   ;;xpath:left-assoc
   (construct (ex rest) (if rest `(sxp:and ,ex ,rest) ex))))

;(define-xpath-rule and-expr
;  (xpath:equal-expr (xpath:and-expr-rest :optional)
;                    ;;xpath:left-assoc
;                    (construct (e r) (if r `(node-reduce ,e ,r) e))
;                    ))

(define (looking-at-equality a b)
  (cond
   ((and (> (string-length a) b)
         (eqv? (string-ref a b) #\=))
    (values (add1 b) 1))
   ((and (> (string-length a) (add1 b))
         (eqv? (string-ref a b) #\!)
         (eqv? (string-ref a (add1 b)) #\=))
    (values (+ b 2) 2))
   (else (values #f #f))))

;(define looking-at-equality
;  (let ((re (reg-expr->proc '(prefix (seq (? #\!) #\=)))))
;    (lambda (a b) (receive (s e) (re a b) (values e (and e (- e s)))))))

(define-xpath-rule equal-expr-rest
  (looking-at-equality
   (xpath:relational-expr "expected relational expression")
   (xpath:equal-expr-rest :optional)
   (construct (op ex rest)
     (let ((op (if (eqv? op 1) '= '!=)))
       (if rest
           (case (car rest)
             ((=) (list op 'sxp:equal? ex (cdr rest)))
             ((!=) (list op 'sxp:not-equal? ex (cdr rest)))
             (else (cons op (cons ex rest))))
           (cons op ex))))))

(define-xpath-rule equal-expr
  (xpath:relational-expr
   (xpath:equal-expr-rest :optional)
   (construct (expr rest)
    (if rest
        (case (car rest)
          ((=) `(sxp:equal? ,expr ,(cdr rest)))
          ((!=) `(sxp:not-equal? ,expr ,(cdr rest)))
          (else (cons expr rest)))
        expr))))

(define (looking-at-relation-operator a b)
  (if (> (string-length a) b)
      (case (string-ref a b)
        ((#\<) (if (and (> (string-length a) (add1 b))
                        (eqv? (string-ref a (add1 b)) #\=))
                   (values (+ b 2) '<=) (values (add1 b) '<)))
        ((#\>) (if (and (> (string-length a) (add1 b))
                        (eqv? (string-ref a (add1 b)) #\=))
                   (values (+ b 2) '>=) (values (add1 b) '>)))
        (else (values #f #f)))
      (values #f #f)))

(define-xpath-rule relational-expr-rest
  (looking-at-relation-operator
   (xpath:additive-expr "expected right additive expression")
   (xpath:relational-expr-rest :optional)
   (construct (op ex rest)
     (let ((op (if (eqv? op 1) '= '!=)))
       (if rest
           (case (car rest)
             ((<) (list op 'sxp:< ex (cdr rest)))
             ((>) (list op 'sxp:> ex (cdr rest)))
             ((<=) (list op 'sxp:<= ex (cdr rest)))
             ((>=) (list op 'sxp:>= ex (cdr rest)))
             (else (cons op (cons ex rest))))
           (cons op ex))))))

(define-xpath-rule relational-expr
  (xpath:additive-expr
   (xpath:relational-expr-rest :optional)
   (construct (expr rest)
    (if rest
        (case (car rest)
          ((<) `(sxp:< ,expr ,(cdr rest)))
          ((>) `(sxp:> ,expr ,(cdr rest)))
          ((<=) `(sxp:<= ,expr ,(cdr rest)))
          ((>=) `(sxp:>= ,expr ,(cdr rest)))
          (else (cons expr rest)))
        expr))))

(define (looking-at-additive-operator string offset)
  (if (fx< offset (string-length string))
      (let ((c (string-ref string offset)))
        (if (or (eqv? c #\+) (eqv? c #\-))
            (values (add1 offset) (if (eqv? c #\+) 'sxp:+ 'sxp:-))
            (values #f #f)))
      (values #f #f)))

(define-xpath-rule additive-expr-rest
  (looking-at-additive-operator
   (xpath:multiplicative-expr  "expected right multiplicative expression")
   (xpath:additive-expr-rest :optional)
   xpath:collect-rest))

(define-xpath-rule additive-expr
  (xpath:multiplicative-expr
   (xpath:additive-expr-rest :optional)
   xpath:left-assoc))

(define (looking-at-multiply-operator a b)
  (if (> (string-length a) b)
      (if (eqv? (string-ref a b) #\*)
          (values (add1 b) 'sxp:mul)
          (if (and
               (> (string-length a) (+ b 3))
               (not (char-alphabetic? (string-ref a (+ b 3)))))
              (if (and (eqv? (string-ref a b) #\d)
                       (eqv? (string-ref a (add1 b)) #\i)
                       (eqv? (string-ref a (+ b 2)) #\v))
                  (values (+ b 3) 'sxp:div)
                  (if (and (eqv? (string-ref a b) #\m)
                           (eqv? (string-ref a (add1 b)) #\o)
                           (eqv? (string-ref a (+ b 2)) #\d))
                      (values (+ b 3) 'sxp:mod)
                      (values #f #f)))
              (values #f #f)))
      (values #f #f)))

(define-xpath-rule multiplicative-expr-rest
  (looking-at-multiply-operator
   (xpath:unary-expr "expected right unary expression")
   (xpath:multiplicative-expr-rest :optional)
   xpath:collect-rest))

(define-xpath-rule multiplicative-expr
  (xpath:unary-expr
   (xpath:multiplicative-expr-rest :optional)
   xpath:left-assoc))

(define-xpath-rule unary-expr
  (looking-at--
   (xpath:unary-expr "expected value after '-'")
   (construct (op expr) (list '- expr)))
  (xpath:union-expr identity))

(define-xpath-rule union-expr-rest
  (looking-at-bar
   (xpath:path-expr "expected path expression")
   (xpath:union-expr-rest :optional)
   (construct (op left rest) (if rest `(sxp:union ,left ,rest) left))))

(define-xpath-rule union-expr
  (xpath:path-expr (xpath:union-expr-rest :optional) xpath:left-assoc))

(define-xpath-rule path-expr
  (xpath:filter-expr
   (xpath:relative-location-path-rest :optional)
   (construct (f rest) (if rest (cons f rest) f)))
  (xpath:location-path identity))

(define-xpath-rule filter-expr
  (xpath:primary-expr
   (xpath:predicate :optional)
   (construct (prim pred) (if pred (cons pred prim) prim))))

(define-xpath-rule primary-expr
  (xpath:variable-reference identity)
  (looking-at-open-paren
   (xpath:expr "expression expected") (looking-at-close-paren "missing ')'")
   (construct (le expr re) expr))
  (xpath:literal (construct (n) `(node-constant ,n)))
  (xpath:number (construct (n) `(node-constant ,(number-value n))))
  (xpath:function-call identity))

;; xpath:number using regex

(define-xpath-rule variable-reference
  (looking-at-dollar
   (xpath:qname "variable name expected")
   (construct (dollar name) name)))

(define-xpath-rule function-call
  (xpath:function-name
   ;; in function-name: (looking-at-open-paren "expected '(' in function call")
   (xpath:argument-list "incomplete argument list")
   (construct (name args) `(*function-call* ,name . ,args))))

(define-xpath-rule argument-list-rest
  (looking-at-comma
   (xpath:expr "expected expression")
   (xpath:argument-list-rest "argument list not closed")
   (construct (comma expr rest) (if rest (cons expr rest) (list expr))))
  (looking-at-close-paren (construct (x) '())))

(define-xpath-rule argument-list
  (looking-at-close-paren (construct (x) '()))
  (xpath:expr
   (xpath:argument-list-rest "argument list not closed")
   (construct (expr rest) (if rest (cons expr rest) expr))))


(define xpath:axis-specifier-read
  (let ((match (regexp*
                (apply string-append
                       (cons* "(" (car xpath:axis-specifier-list)
                              (fold (lambda (a d) (cons* "|" a d))
                                    '(")[[:blank:]]*::")
                                    (cdr xpath:axis-specifier-list))))
		'(anchored))))
    (lambda (str off)
      (let ((result (string-match-positions match str off)))
        (if result
            (values (cadar result)
                    (cdr (assoc (substring str (caadr result) (cadadr result))
                                xpath:axis-specifier-indicators)))
            (values #f #f))))))

(define xpath:literal-regex-dq
  (let ((match (regexp* "\"([^\"]*)\"" '(anchored))))
    (lambda (str off)
      (let ((result (string-match-positions match str off)))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caadr result) (cadadr result)))
            (values #f #f #f))))))

(define xpath:literal-regex-sq
  (let ((match (regexp* "'([^']*)'" '(anchored))))
    (lambda (str off)
      (let ((result (debug 'SQ (string-match-positions match str off))))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caadr result) (cadadr result)))
            (values #f #f #f))))))

(define xpath:node-type-test
  (let ((match (regexp* (apply string-append
			       (cons* "(" (car xpath:node-type-list)
				      (fold (lambda (a d) (cons* "|" a d))
					    '(")[[:blank:]]*\\(\\)")
					    (cdr xpath:node-type-list))))
			'(anchored))))
    (define (xpath:node-type-test str off)
      (let ((result (string-match-positions match str off)))
        (if result
            (values (caar result) (cadar result)
                    (cdr (assoc (substring str (caadr result) (cadadr result))
                                xpath:node-type-indicators)))
            (values #f #f))))
    xpath:node-type-test))



(define (number-value input)
  (if (number? input) input
      (string->number (string-value input))))

(define xpath:number
  (let ((match (regexp*
                "((?:[[:digit:]]+(?:\\.[[:digit:]]+)?)|(?:\\.[[:digit:]]+))"
		'(anchored))))
    (lambda (str off)
      (let  ((result (string-match-positions match str off)))
	(if (debug 'number result)
	    (values
	     (cadar result)
	     (string->number (substring str (caadr result) (cadadr result))))
	    (values #f #f))))))

(print (xpath:number "a4" 0))

(for-each
 (lambda (name)
   (format (current-output-port) "Start ~a\n~!" name)
   (format (current-output-port) "Result: ~a\n~!" (pretty-print (xpath:parse name))))
 (cdr (argv)))
