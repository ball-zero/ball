#!/usr/bin/csi -i

(require-extension srfi-18)

(##sys#extend-macro-environment
 'parameterize '()
 (##sys#er-transformer
  (let ((ipp 'not-in-parameterize?))
    (lambda (form r c)
      (##sys#check-syntax 'parameterize form '#(_ 2))
      (let* ((bindings (cadr form))
	     (body (cddr form))
	     (swap (r 'swap))
	     (%let (r 'let))
	     (%lambda (r 'lambda))
	     [params (##sys#map car bindings)]
	     [vals (##sys#map cadr bindings)]
	     [aliases (##sys#map (lambda (z) (r (gensym))) params)]
	     [aliases2 (##sys#map (lambda (z) (r (gensym))) params)] )
	`(,%let ,(##sys#append (map ##sys#list aliases params) (map ##sys#list aliases2 vals))
		;; REMARK: wouldn't it be better to first compute all
		;; values and then swap the bindings?  The "guard"
		;; parameter of the parameters could have captured
		;; continuations, which would cause a lot of
		;; dynamic-winds
		(,%let ((,swap (,%lambda ()
					 ,@(map (lambda (a a2)
						  `(,%let ((t (,a))) (,a ,a2 #:local)
							  (##core#set! ,a2 t)))
						aliases aliases2) ) ) )
		       (##sys#dynamic-wind 
			,swap
			(,%lambda () ,@body)
			,swap) ) ) ) ))))

(define make-shared-parameter
  (let ([count 0]
	[defaults '#()]
	[vals (make-parameter '(#f . #()))]
	[undefined '(undefined)])
    (lambda (init . sane)
      (let* ([sane (if (pair? sane) (car sane) (lambda (x) x))]
	     [val (sane init)] 
	     [i count] )
	(set! count (fx+ count 1))
	(when (fx>= i (vector-length defaults))
	  (set! defaults (vector-resize defaults (fx+ i 1) undefined) ) )
	(vector-set! defaults i val)
	(lambda arg
	  (let* ((e (vals))
		 (current (cdr e))
		 [n (vector-length current)])
	    (cond [(and (pair? arg) (null? (cdr arg)))
		   (if (or (fx>= i n)
			   (eq? (vector-ref current i) undefined))
		       (vector-set! defaults i (sane (car arg)))
		       (vector-set! current i (sane (car arg))))
		   (##core#undefined) ]
		  [(pair? arg)
		   (cond
		    ((fx>= i n)
		     (set! current (vector-resize current (fx+ i 1) undefined) )
		     (vals (cons (current-thread) current)))
		    ((not (eq? (car e) (current-thread)))
		     (set! current (vector-copy! current 0 (vector-length current)) )
		     (vals (cons (current-thread) current))))
		   (vector-set! current i (sane (car arg)))
		   (##core#undefined) ]
		  [(fx>= i n)
		   (vector-ref defaults i) ]
		  [else
		   (let ([val (vector-ref current i)])
		     (if (eq? val undefined)
			 (vector-ref defaults i) 
			 val) ) ] ) ) ) ) ) ) )

(define p (make-shared-parameter #f))

(define ts (thread-start! (lambda () (thread-sleep! 3)
				  (parameterize ((p 'gaga))
						(print "ip " (p)))
				  (print "now " (p)))))

(thread-sleep! 1)

(p 42)

(thread-join! ts)

(exit 0)
