(require-extension ports)
(require-extension srfi-18)
#|
(require-extension syntax-case)

(define-syntax guard
  (syntax-rules ()
    ((guard (var clause ...) e1 e2 ...)
     ((call-with-current-continuation
       (lambda (guard-k)
         (let ((oldh (current-exception-handler)))
	   (with-exception-handler
	    (lambda (condition)
	      (with-exception-handler
	       oldh
	       (call-with-current-continuation
		(lambda (handler-k)
		  (guard-k
		   (lambda ()
		     (let ((var condition))      ; clauses may SET! var
		       (guard-aux (handler-k (lambda ()
						  (raise condition)))
				     clause ...))))))))
	    (lambda ()
	      (call-with-values
		  (lambda () e1 e2 ...)
		(lambda args
		  (guard-k (lambda ()
			     (apply values args))))))))))))))

(define-syntax guard-aux
  (syntax-rules (else =>)
    ((guard-aux reraise (else result1 result2 ...))
     (begin result1 result2 ...))
    ((guard-aux reraise (test => result))
     (let ((temp test))
       (if temp 
           (result temp)
           reraise)))
    ((guard-aux reraise (test => result) clause1 clause2 ...)
     (let ((temp test))
       (if temp
           (result temp)
           (guard-aux reraise clause1 clause2 ...))))
    ((guard-aux reraise (test))
     test)
    ((guard-aux reraise (test) clause1 clause2 ...)
     (let ((temp test))
       (if temp
           temp
           (guard-aux reraise clause1 clause2 ...))))
    ((guard-aux reraise (test result1 result2 ...))
     (if test
         (begin result1 result2 ...)
         reraise))
    ((guard-aux reraise (test result1 result2 ...) clause1 clause2 ...)
     (if test
         (begin result1 result2 ...)
         (guard-aux reraise clause1 clause2 ...)))))
|#

(define-macro (guard . form)
  (let* ((clause (or (and (pair? form) (car form))
                     (error "guard: syntax error in" form)))
         (body (cdr form)))
    `(with-exception-guard
      (lambda (,(car clause))
        ,(let loop ((clauses (cdr clause)))
           (if (null? clauses)
               `(raise ,(car clause))
               (let ((c (car clauses)))
                 (cond
                  ((eq? 'else (car c))
                   (if (null? (cdr c))
                       '#f
                       (if (null? (cddr c))
                           (cadr c)
                           `(begin . ,(cdr c)))))
                  ((and (pair? c) (pair? (cdr c)) (eq? '=> (cadr c)))
                   (let ((v (gensym)))
                     `(let ((,v ,(car c)))
                        (if ,v (,(caddr c)) ,(loop (cdr clauses))))))
                  ((and (pair? c) (null? (cdr c)))
                   (let ((v (gensym)))
                     `(let ((,v ,(car c)))
                        (if ,v ,v ,(loop (cdr clauses))))))
                  ((pair? c)
                   `(if ,(car c)
                        ,(if (null? (cddr c))
                             (cadr c)
                             `(begin . ,(cdr c)))
			,(loop (cdr clauses))))
                  (else (error "guard syntax error in ~a" c)))))))
      (lambda ()
        ,(if (and (pair? body) (null? (cdr body)))
             (car body) `(begin . ,body) )))))

(define (with-exception-guard handler thunk)
  ((call-with-current-continuation
    (lambda (return)
      (let ((oldh (current-exception-handler)))
	(with-exception-handler
	 (lambda (condition)
	   (with-exception-handler
	    oldh
	    (call-with-current-continuation
	     (lambda (handler-k)
	       (return (lambda () (handler condition)))))))
	 (lambda ()
	   (##sys#call-with-values
	    thunk
	    (lambda args
	      (return (lambda () (##sys#apply ##sys#values args)))) ) ) )) ) )) )

(print (guard (ex (else 'success)) (call-with-input-string ")" read)))
;(print (condition-case
;	(call-with-input-string ")" read)
;	(var () 'condition-case-does-a-better-job-than-guard)))

(print (guard (ex ((number? ex) ex)) (raise 0)))
;(print (guard (ex ((number? ex) ex)) (raise 'nix)))

(with-exception-handler
 (lambda (ex) (print "Toplevelhandler:" ex))
 (lambda ()
   (call-with-current-continuation
    (lambda (k)
      (with-exception-handler
       (lambda (x)
	 (display "reraised ") (write x) (newline)
	 (k 'zero))
       (lambda ()
	 (guard (condition
		 ((positive? condition) 'positive)
		 ((negative? condition) 'negative))
		(raise 0))))))))
