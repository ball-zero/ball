(define chicken-exception-handler (current-exception-handler))

(define srfi34:*current-exception-handlers*
  (make-parameter (list chicken-exception-handler)))

(define (srfi34:with-exception-handler handler thunk)
  (srfi34:with-exception-handlers
   (cons handler (srfi34:*current-exception-handlers*))
   thunk))

(define (srfi34:with-exception-handlers new-handlers thunk)
  (let ((previous-handlers (srfi34:*current-exception-handlers*)))
    (dynamic-wind
      (lambda ()
        (srfi34:*current-exception-handlers* new-handlers))
      thunk
      (lambda ()
        (srfi34:*current-exception-handlers* previous-handlers)))))

(define (srfi34:raise obj)
  (let ((handlers (srfi34:*current-exception-handlers*)))
    (srfi34:with-exception-handlers
     (cdr handlers)
     (lambda ()
       ((car handlers) obj)
       ;; (error "handler returned" (car handlers) obj)
       (chicken-exception-handler
	(make-property-condition
	 'exn 'message "exception handler returned"))))))

(set! with-exception-handler srfi34:with-exception-handler)

(set! current-exception-handler
      (lambda () (car (srfi34:*current-exception-handlers*))))

(set! ##sys#default-exception-handler current-exception-handler)

; (define (##sys#escape x)
;   (let ((handler-chain ##sys#current-exception-handler))
;     (set! ##sys#current-exception-handler
;           (cdr ##sys#current-exception-handler))
;     ((car handler-chain) x)
;     (set! ##sys#current-exception-handler handler-chain)))

;(define srfi-34:raise ##sys#escape)

(set! ##sys#abort
      (lambda (x)
        ((car (srfi34:*current-exception-handlers*)) x)
        (##sys#abort (make-property-condition
                      'exn 'message "exception handler returned")) ))

(set! ##sys#signal
      (lambda (x)
        ((car (srfi34:*current-exception-handlers*)) x)) )

(set! ##sys#current-exception-handler current-exception-handler)

(define raise srfi34:raise)
(set! abort ##sys#abort)
(set! signal ##sys#signal)
(set! error
      (lambda (msg . args)
	(let ((s (if (pair? args) (format #f "~a ~s" msg args) msg)))
	  (if (enable-warnings) (##sys#signal-hook #:warning s))
	  (srfi34:raise s))))

(set! ##sys#error error)
