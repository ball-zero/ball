(declare
 (uses extras library posix srfi-1 srfi-13 regex)
 (usual-integrations)
 ;; (disable-interrupts)
 )

(cond-expand
 [paranoia]
 [else
  (declare
    (no-bound-checks)
    (bound-to-procedure
     ##sys#check-string ##sys#check-exact ##sys#make-pointer ##sys#cons ##sys#size ##sys#slot) ) ] )

(cond-expand
 [unsafe
  (eval-when (compile)
    (define-macro (##sys#check-structure x y) '(##core#undefined))
    (define-macro (##sys#check-range x y z) '(##core#undefined))
    (define-macro (##sys#check-pair x) '(##core#undefined))
    (define-macro (##sys#check-list x) '(##core#undefined))
    (define-macro (##sys#check-symbol x) '(##core#undefined))
    (define-macro (##sys#check-string x) '(##core#undefined))
    (define-macro (##sys#check-char x) '(##core#undefined))
    (define-macro (##sys#check-exact x) '(##core#undefined))
    (define-macro (##sys#check-port x) '(##core#undefined))
    (define-macro (##sys#check-number x) '(##core#undefined))
    (define-macro (##sys#check-byte-vector x) '(##core#undefined)) ) ]
 [else] )

(define bind-exit call-with-current-continuation)

(define (debug lbl v)
  (format (current-error-port)"~a ~a\n~!" lbl v)
  v)

;;* Data Definition

(define-macro (make-xml-literal data) data)
(define-macro (xml-literal-value lit) lit)

;(define-record-type <xml-attribute>
;  (make-xml-attribute name ns value)
;  xml-attribute?
;  (name xml-attribute-name)
;  (ns xml-attribute-ns)
;  (value xml-attribute-value))

(define (make-xml-attribute name ns value)
  (vector 'xml-attribute name ns value))

(define (xml-attribute? obj)
  (and (vector? obj) (eq? (vector-ref obj 0) 'xml-attribute)))

(define (xml-attribute-name x) (vector-ref x 1))
(define (xml-attribute-ns x) (vector-ref x 2))
(define (xml-attribute-value x) (vector-ref x 3))

;;* Namespaces

(define (find-namespace pred lst)
  (and (pair? lst) (or (and (pred (car lst)) (car lst))
                       (find-namespace pred (cdr lst)))))

(define-record-type <xml-namespace>
  (make-xml-namespace local uri)
  xml-namespace?
  (local xml-namespace-local)
  (uri xml-namespace-uri))

;;* Elements

;(define-record-type <xml-element>
;  (make-xml-element name ns attributes content)
;  xml-element?
;  (name xml-element-name)
;  (ns xml-element-ns)
;  (attributes attributes)
;  (content %children))

(define (make-xml-element name ns attributes content)
  (vector 'xml-element name ns attributes content))

(define (xml-element? obj)
  (and (vector? obj) (eq? (vector-ref obj 0) 'xml-element)))

(define (xml-element-name e) (vector-ref e 1))
(define (xml-element-ns e) (vector-ref e 2))
(define (attributes e) (vector-ref e 3))
(define (%children e) (vector-ref e 4))

(define (gi obj) (and (xml-element? obj) (xml-element-name obj)))
(define (ns obj) (and (xml-element? obj) (xml-element-ns obj)))

(define (make-xml-pi tag line) (vector 'xml-pi tag line))
(define (xml-pi? node)
  (and (vector? node) (eq? (vector-ref node 0) 'xml-pi)))

(define (make-xml-comment line) (vector 'xml-comment line))
(define (xml-comment? node)
  (and (vector? node) (eq? (vector-ref node 0) 'xml-comment)))

(define (make-xml-doctype data)
  (vector 'DOCTYPE data))

;;* Node Lists

(define-macro (empty-node-list) ''())
(define (fun:empty-node-list) (empty-node-list))

(define (apply-string-append lst)
  (let loop ((i 0) (l lst))
    (if (null? l)
        (let ((result (make-string i)))
          (let loop ((j 0) (from lst))
            (if (eqv? i j)
                result
                (let ((s (car from)))
                  (do ((k 0 (add1 k)) (j j (add1 j)))
                      ((eqv? k (string-length s)) (loop j (cdr from)))
                    (string-set! result j (string-ref s k)))))))
        (loop (+ i (string-length (car l))) (cdr l)))))

(include "ssax-chicken.scm")

(define (file->string filename)
  (read-string (file-size filename) (open-input-file filename)))

(for-each
 (lambda (name)
   (format (current-output-port) "Start ~a\n~!" name)
   (format (current-output-port) "Result: ~a\n~!" (pretty-print (xml-parse (file->string name)))))
 (cdr (argv)))
