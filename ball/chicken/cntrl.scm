;; (C) 2010, Jörg F. Wittenberger

;; chicken module for the Askemos ball operation control module.

(declare
 (unit cntrl)
 ;;
 (usual-integrations))

(module
 cntrl
 (
  check-control-password ; to main.scm
  $server-listen-address
  $control-port
  $http-server-port
  $https-server-port
  $lmtp-server-port
  set-control-password!  ; to setup.scm
  set-server-listen-address!
  set-control-port!
  set-http-server-port!
  set-https-server-port!
  set-lmtp-server-port!
  ball-client-mode
  signal-clock
  ball-info ball-control ball-config-file ball-control-hook
  ball-load-config ball-kernel-info-handlers
  ball-load-local-id-from-config ball-save-config
  $trustedcode-oid
  mind-restart
  ball-send-new-certificate-request!  
  )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	(except srfi-18 raise)
	srfi-1 srfi-13 (prefix srfi-13 srfi:)
	srfi-19 srfi-34 srfi-35 srfi-45 srfi-69
	(except posix file-close create-pipe duplicate-fileno process-wait process-signal)
	files ports extras
	pcre openssl shrdprmtr util timeout atomic parallel cache)
(import tree notation function aggregate pool storage-api place-common place methods corexml)
(import (only sslsocket ssl-keep-alive-timeout $ca-slave))
(import smtp protocol-connpool protocol dispatch storage mesh-cert hostmesh trstcntl)
(import (only storage-fsm $fsm-blob-paranoia))

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-inline (make-a-transient-copy-of-object x) x)
(include "../policy/bopcntrl.scm")

)

(import (prefix cntrl m:))

(define check-control-password m:check-control-password)
(define $server-listen-address m:$server-listen-address)
(define $control-port m:$control-port)
(define $http-server-port m:$http-server-port)
(define $https-server-port m:$https-server-port)
(define $lmtp-server-port m:$lmtp-server-port)
(define set-control-password! m:set-control-password!)
(define set-server-listen-address! m:set-server-listen-address!)
(define set-control-port! m:set-control-port!)
(define set-http-server-port! m:set-http-server-port!)
(define set-https-server-port! m:set-https-server-port!)
(define set-lmtp-server-port! m:set-lmtp-server-port!)
(define ball-client-mode m:ball-client-mode)
(define signal-clock m:signal-clock)
(define ball-info m:ball-info)
(define ball-control m:ball-control)
(define ball-config-file m:ball-config-file)
(define ball-control-hook m:ball-control-hook)
(define ball-load-config m:ball-load-config)
(define ball-kernel-info-handlers m:ball-kernel-info-handlers)
(define ball-save-config m:ball-save-config)
(define $trustedcode-oid m:$trustedcode-oid)
(define mind-restart m:mind-restart)
(define ball-send-new-certificate-request! m:ball-send-new-certificate-request!)
