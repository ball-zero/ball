;; (C) 2002, 2008 J�rg F. Wittenberger

;; chicken module clause for Askemos' central place module.

(declare
 (unit place)
 ;;
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)

 (disable-interrupts) ;; currently contains define-atomic-type

 )

(module
 place
 (
  host-lookup host-lookup* hosts-lookup-at-least
  message-digest
  early-timeout-object? late-timeout-object? agreement-timeout?
  sync-message? ; sync-message-data?
  agreement-last-message
  ;; protocol adapter interface
  quorum-lookup forward-call replicate-call replicate-reply echo-call send-ready send-one-shot rerequest-state request-reply
  ;;
  resync-now!
  init-place-upcalls!
  resync-locked-now!
  quorum-broadcast-method $broadcast-retries $broadcast-timeout
  $broadcast-update-statistics
  $broadcast-debug-proposal
  $complete-timeout
  simple-majority
  ;; place.scm
  $use-well-known-symbols
  respond!-mutex
  oid-channel-unregister-mailbox!
  transaction-send-message-version! transaction-send-message!
  transaction-request transaction-reply
  register-transaction! remove-transaction!

  $default-sql-block-size $default-sql-bpb
  $max-direct-string-length
  commit-frame!
  *local-quorum* replicates-of
  advance-transactions message-ahead-of
  respond-local respond-local-no-timeout
  respond-cache respond-replicated respond-making-laune
  respond-digest respond-treshold
  ;; place.scm
  bind-place! frame-commit!
  condition->message
  respond!
  current-place current-message
  public-capabilities
  make-condition-message
  accept-header-matches-application/soap+xml
  $pishing-prevention
  prevent-pishing
  pishing-mode-compatible-forward-call
  use-mandatory-capabilities
  $use-inherited-protection
  fetch-other
  ;;;
  transaction-completed?
  with-mind-access-now with-mind-access
  enter-front-court-now enter-front-court
  go-on-access set-fast-access!
  with-mind-modification
  $max-commits $commits-per-expire-ratio
  make-mind-gatekeeper
  mind-commit mind-execute-synchron mind-exit
  ;;;
  initial-mind ; should be private
  locked-frames				; debugging aid
  known-slots transient-meta-slots
  optional-transient-reply-meta-slots optional-transient-meta-slots
  serializable-transient-meta-slots
  make-place! make-place-for-id!
  sync-on-load? sync-on-load set-missing-oid! delete-is-sync-flag! is-synced?
  find-frames-by-id find-frame-by-id/quorum
  find-local-frame-by-id
  find-frame-by-id/asynchroneous find-frame-by-id/asynchroneous+quorum
  find-already-loaded-frame-by-id
  document
  aggregate-fetch-blob
  make-reader make-access make-reader-access make-look-ahead-reader
  ;; internal (exception because of poor implementationin sequence handler)
  make-sender
  ;; protocoll interface
  metainfo-remote resynchronize invite-new-supporters!
  message-body-fetch-blob-function
  ;; 
  quorum-get!
  null-oid one-oid my-oid public-oid
  strict-public-equivalent? rough-public-equivalent? public-equivalent?
  $administrator administrator set-administrator-email!
  check-administrator-password set-administrator-password! administrator-password-hash
  $insecure-mode

  is-meta-form? action-document secure-action-document
  message-protection message-capabilities message-content-type
  make-context public-context user-context
 
  read-attribute-list write-attribute-list rdf-parsetype-literal-attribute-list
  signature-att-decl
  make-rdf-li-for-link table->rdf-bag-sorted
  want-links-local? want-body-local?
  ;;;
  ;; protocoll interface
  host-alive? host-maybe-alive?
  ;;;
  make-html-error make-soap-error
  ;;;
  make-object-not-available-condition
  timeout-condition?
  ;;
  askemos:dc-date
  *dump-message-signal*
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-34 srfi-35 srfi-45)

(import srfi-1 srfi-13 (except srfi-18 raise) srfi-19 (prefix srfi-43 srfi:)
	openssl
	shrdprmtr
	mailbox util timeout atomic parallel cache protection sqlite3
	ports extras data-structures tree notation function aggregate channel)

(import storage-api place-common pool place-ro)

(import (prefix srfi-13 srfi:))

(import (only lolevel move-memory!))
(import bhob)

(include "typedefs.scm")

(define-type :db-conn-state: (or false (struct mutex) (struct <sqlite3-database>)))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define-macro (define-atomic-type type-name 
                constructor-name+field-tags
		define-update-name
                predicate-name
                . field-specs)
  (begin
    (define constructor-name (car constructor-name+field-tags))
    (define field-tags (cdr constructor-name+field-tags))
    (define (fs-name fs) (car fs))
    (define (fs-accessor fs) (cadr fs))
    (define (fs-mutable fs) (and (pair? (cddr fs))
				 (eq? (caddr fs) ':mutable)))
    (define (field-spec fs)
      (list (fs-name fs) (fs-accessor fs)))
    (define object (gensym))
    (define outer-object (gensym))
    (define get-proc (gensym))
    (define exch-proc (gensym))
    (define proc (gensym))
    (define resultsym (gensym 'result))
					;
    `(begin
       (define-record-type ,type-name
	 (,constructor-name . ,field-tags)
	 ,predicate-name
	 . ,(map field-spec field-specs))

       (define-macro (,define-update-name update-method
		       input-fields update-fields arguments results
		       . body)
	 (let* ((updater (gensym))
		(update-method2 (string->symbol (string-append (symbol->string update-method) "!")))
		(typename ',type-name)
		(constructor-name ',constructor-name)
		(object ',object)
		(outer-object ',outer-object)
		(get-proc ',get-proc)
		(exch-proc ',exch-proc)
		(resultsym ',resultsym)
		(getterlist (let loop ((field-specs ',field-specs))
			      (if (null? field-specs) '()
				  (if (memq (car (car field-specs))
					    input-fields)
				      (cons `(,(cadr (car field-specs)) ,object)
					    (loop (cdr field-specs)))
				      (loop (cdr field-specs))))))
		(makelist (let loop ((field-specs ',field-specs))
			    (if (null? field-specs) '()
				(let ((name (car (car field-specs))))
				  `(,(if (memq name update-fields)
					 name
					 `(,(cadr (car field-specs)) ,object))
				    . ,(loop (cdr field-specs))))))))
	   `(begin
	      (define ,update-method
		(let ((,updater (lambda ,(cons 'values (append input-fields arguments)) . ,body)) )
		  (define ,(cons* update-method object arguments)
		    ,(cons*
		      updater
		      `(lambda ,(append results update-fields)
			 ,(cons
			   'values
			   (append results (list (cons constructor-name makelist)))))
		      (append getterlist arguments)))
		  ,update-method))
	      (define (,update-method2 ,outer-object ,get-proc ,exch-proc . ,arguments)
		(let ,updater ((,object (,get-proc ,outer-object)))
		  ((lambda (values ,@input-fields ,@arguments) . ,body)
		   (lambda (,@results ,@update-fields)
		     (let ((,resultsym (,constructor-name . ,makelist)))
		       (if (eq? (,exch-proc ,outer-object ,object ,resultsym) ,resultsym)
			   (values . ,(append results (list resultsym)))
			   (,updater (,get-proc ,outer-object)))))
		   ,@getterlist ,@arguments))))))
       )))

(: transaction-send-message-version!
   (:oid: fixnum (or (struct <frame>) (struct :condition-type)) -> (struct <frame>)))
(include "../mechanism/quorum.scm")
(include "../mechanism/place.scm")

(define ##draft#make-reader-access make-reader-access)

)

(define draft-make-reader-access ##draft#make-reader-access)

(import (prefix place m:))

(define $broadcast-debug-proposal m:$broadcast-debug-proposal)
(define $broadcast-retries m:$broadcast-retries)
(define $broadcast-timeout m:$broadcast-timeout)
(define $complete-timeout m:$complete-timeout)
(define $broadcast-update-statistics m:$broadcast-update-statistics)

(define administrator m:administrator)

(define $default-sql-block-size m:$default-sql-block-size)
(define $default-sql-bpb m:$default-sql-bpb)
(define $max-direct-string-length m:$max-direct-string-length)
(define prevent-pishing m:prevent-pishing)
(define use-mandatory-capabilities m:use-mandatory-capabilities)
(define $use-inherited-protection m:$use-inherited-protection)

;;

(define public-context m:public-context)
(define user-context m:user-context)

(define document m:document)

;;
;;
(define message-content-type m:message-content-type)
(define message-protection m:message-protection)
(define message-capabilities m:message-capabilities)
(define message-digest m:message-digest)

(define is-meta-form? m:is-meta-form?)
(define (enter-front-court proc . data)
  (apply m:enter-front-court proc data))
(define current-place m:current-place)
(define current-message m:current-message)

;; setup
(define set-administrator-email! m:set-administrator-email!)
(define set-administrator-password! m:set-administrator-password!)
(define $use-well-known-symbols m:$use-well-known-symbols)

(define with-mind-access m:with-mind-access)
(define with-mind-access-now m:with-mind-access)
(define make-place! m:make-place!)
(define frame-commit! m:frame-commit!)
