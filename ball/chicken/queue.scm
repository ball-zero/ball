;;;; queue.scm

;;;; Duplicate of queue source in 'extras.scm' library.

;;

(define-inline (%null? l) (##core#inline "C_i_nullp" l) )
#;
(define-inline (%null? l) (eq? '() l) )

(define-inline (%car p)
  (##sys#slot p 0) )

(define-inline (%cdr p)
  (##sys#slot p 1) )

(define-inline (%set-cdr! p v)
  (##sys#setslot p 1 v) )

(define-inline (%last-pair lst0)
  (do ([lst lst0 (%cdr lst)])
       ([%null? (%cdr lst)] lst)))

;;

(define-inline (%make-queue)
  (##sys#make-structure 'queue '() '()))

#;
(define-inline (%queue? obj)
	(eq? 'queue (##sys#slot obj 0)) )

(define-inline (%queue-tailpair q)
  (##sys#slot q 2) )

(define-inline (%queue-tailpair-set! q v)
  (##sys#setslot q 2 v) )

(define-inline (%queue-headpair q)
  (##sys#slot q 1) )

(define-inline (%queue-headpair-set! q v)
  (##sys#setslot q 1 v) )

(define-inline (%queue-empty? q)
  (%null? (%queue-headpair q)) )

(define (%queue-add! q datum)
  (let ([new-pair (cons datum '())])
    (cond [(%null? (%queue-headpair q))
            (%queue-headpair-set! q new-pair)]
          [else
            (%set-cdr! (%queue-tailpair q) new-pair)] )
    (%queue-tailpair-set! q new-pair) ) )

(define (%queue-remove! q)
  (let* ([first-pair (%queue-headpair q)])
    (let ([first-cdr (%cdr first-pair)])
      (%queue-headpair-set! q first-cdr)
      (when (%null? first-cdr)
        (%queue-tailpair-set! q '()) ) )
    (%car first-pair) ) )

(define (%queue-push-back! q item)
  (let ([newlist (cons item (%queue-headpair q))])
    (%queue-headpair-set! q newlist)
    (when (%null? (%queue-tailpair q))
      (%queue-tailpair-set! q newlist))))

(define (%queue-push-back-list! q itemlist)
  (let* ([newlist (append itemlist (%queue-headpair q))]
         [newtail (if (%null? newlist) '() (%last-pair newlist))])
    (%queue-headpair-set! q newlist)
    (%queue-tailpair-set! q newtail)))

(define (%queue-extract-pair! q pair)
  #;(##sys#check-structure q 'queue 'queue-extract-next!)
  #;(##sys#check-list pair 'queue-extract-next!)
  ; Scan queue list until we find the item to remove
  (let loop ([this-pair (%queue-headpair q)] [prev-pair '()])
    ; Found it?
    (if (eq? this-pair pair)
        ; then cut out the pair
        (let ([next-pair (%cdr this-pair)])
          ; At the head of the list, or in the body?
          (if (%null? prev-pair)
              (%queue-headpair-set! q next-pair)
              (%set-cdr! prev-pair next-pair) )
          ; When the cut pair is the last item update the last pair ref.
          (when (eq? (%queue-tailpair q) this-pair)
            (%queue-tailpair-set! q prev-pair)) )
        ; else keep looking for the pair
        (loop (%cdr this-pair) this-pair) ) ) )
