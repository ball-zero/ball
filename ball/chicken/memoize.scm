;; (C) 2002, 2008, 2015 Jörg F. Wittenberger

;; chicken module clause for "memoize" chached computations support.

(declare
 (unit memoize)
 (usual-integrations)
 (disable-interrupts))

(module
 memoize
 (memoize)
 (import scheme atomic srfi-18)
 (import (except chicken add1 sub1))
 (import llrb-string-table srfi-69)

 (include "../mechanism/function/memoize.scm"))

