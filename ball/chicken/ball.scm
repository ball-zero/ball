;   (declare (uses srfi-18 util memoize
;                  protection
;                  tree notation function place timeout
;                  protocol storage step regex nu)
;            )


(define-syntax guard
  (syntax-rules ()
    ((_ handler body ...) (s34:guard handler body ...))))

;(require 'chicken-more-macros)
;(include "chicken-more-macros.scm")
(require-library chicken-syntax)
;(include "chicken-syntax.scm")

;; Switch chicken evaluation globally to suffix style (DSSSL) keywords.

(keyword-style suffix:)
(case-sensitive #t)

(declare
 (uses
  library srfi-18 posix
  srfi-43 shrdprmtr
  matchable
  lolevel
  regex
  srfi-1 srfi-34
  srfi-4
  srfi-35
  srfi-106
  threadpool ;; used by forcible
  timeout
  srfi-45 ;; forcible
  srfi-69
  srfi-13 clformat v-clformat
  tl-srfi
  srfi-19 v-srfi-19 tl-srfi-19
  comparators
   llrb-tree
  ;;
  tweetnacl
  ;;
  openssl
  libmagic
  ;;
  atomic
  pthreads
   sqlite3
  environments ;; TODO: kick out
  structures
  mailbox
  lalr
   memoize pcre
    mesh-cert
  ;;
  protection
  ;;
   util
    aggregate-chicken sslsocket
    v-timeout parallel cache
     channel
  tl-cntrlflw
  ;;
  tree
  xpath
  ;;
  ;; notation
  srfi-110
  srfi-49
  notation-common
   notation-charenc
    notation-xmlparse
    notation-xmlrender
    notation-lout
     notation
  utf8utils
  ;; function
  ;; aggregate-chicken
  wttree
   dsssl
   function-common
    xslt-stylesheet xslt
     xsql
      function
  ;;
  aggregate
  ;;
  storage-api
   bhob
   place-common pool place-ro place
    corexml
    methods 
  ;; webdav utilities
  webdavlo
  msgcoll
  ;;
  dns
   protocol-common
    protocol-connpool
     rfc-2616 rfc-2518
      http-client protocol-clntclls
       protocol-lowlevel
       hostmesh
      protocol-webdav http-server
    smtp
       protocol
   ;;
   storage-common storage-sql storage-fsm storage-gc storage-remote
    storage
   ;;
   step
   wiki-serial nu
   xmlrpc markdown
   ;;
     dispatch
   ;;
     qrcodegen
     stb-image-write
     userenvt
     trstcntl cntrl
      main-chicken
   ;;
   ask-repl-server
   wot
  ))

(import main-chicken)
(import matchable)
;; BEGIN handle force-primordial
(import lolevel util)

#|
Chicken is still broken, but we work around it different way for now.
(handle-exceptions
 ex
 (begin
   (logerr "Notice: Good: This chicken version does not have ##sys#force-primordial.\n"))
 (mutate-procedure!
  ##sys#force-primordial
  (lambda (old) (lambda () (logerr "primordial thread forced due to interrupt - ingored!\n"))))
 #;(logerr "Notice: This chicken still has ##sys#force-primordial. (Overwritten.)\n"))
|#

#;(mutate-procedure!
 thread-sleep!
 (lambda (old) (lambda (tm) (logerr "Thread ~a sleeps for ~a\n" (current-thread) tm) (old tm))))

;; TODO: Some things don't work on Android.  However the
;; TARGET_FEATURES of my Chicken are not yet working either.
(import (only posix change-file-mode))
(cond-expand
 (android
  (logerr "Compiled for Android, change-file-mode and set-file-modification-time! disabled.\n")
  (on-exit (lambda () (logerr "Exit from BALL kernel.\n")))
  (mutate-procedure!
   change-file-mode
   (lambda (old)
     (lambda (fn m)
       (logerr "Ignoring mode change on '~a' to mode ~a\n" fn m)
       #t)))
  ;; FIXME: This should actually do what the HACK in util-chicken accomplishes.
  #;(mutate-procedure!
   set-file-modification-time!
   (lambda (old) (lambda (fn t) #t))))
 (windows
  (logerr "Compiled for Windows Platform, forking subprocesses disabled.\n"))
 (else
  #t #;(logerr "Compiled for generic Platform.\n")))

(import srfi-18)
#;(let ((silent '("*spool-db-file*")))
  (mutate-procedure!
   mutex-lock!
   (lambda (old)
     (let ((mutex-lock! (mutex-lock!! old)))
       (lambda (mux . more)
	 (unless #f (member (mutex-name mux) '())
		 (logerr "~a locking mutex ~a state ~a args ~a\n" (thread-name (current-thread)) (mutex-name mux) (mutex-state mux) more))
	 (if (pair? more)
	     (apply old mux more)
	     (mutex-lock! mux))))))
  (mutate-procedure!
   mutex-unlock!
   (lambda (old)
     (lambda (mux . more)
       (logerr "~a unlocking mutex ~a args ~a\n" (thread-name (current-thread)) (mutex-name mux) more)
       (apply old mux more))))
  )

;; END handle force-primordial

;; BEGIN temporaries

;; END temporaries

(main (let loop ((args (let ((args (command-line-arguments)))
			 (if (or (null? args) (not (string=? (car args) "-qimage")))
			     args (cddr args)))))
	(if (or (null? args)
		(< (string-length (car args)) 2)
		(not (and (eqv? (string-ref (car args) 0) #\-)
			  (eqv? (string-ref (car args) 1) #\:))))
	    args
	    (loop (cdr args)))))
