;; (C) 2013 J�rg F. Wittenberger

;; chicken module clause for lout module.

(declare
 (unit notation-lout)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (disable-interrupts)			; checked loops
 (strict-types)
 (usual-integrations)
 (fixnum)
)

(module
 notation-lout
 (
  lout-format-output-element
  lout-format-at-current-output-port
  pdf-format
  lout-pdf-format
  )

(import (except scheme vector-fill! vector->list list->vector)
	(except chicken add1 sub1 condition? vector-copy! with-exception-handler)
	srfi-1 srfi-13 srfi-34 srfi-35
	srfi-43 (prefix srfi-43 srfi:)
	ports (only extras write-string)
	util tree
	notation-charenc notation-common)

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/notation/lout.scm")

)
