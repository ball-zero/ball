;; (C) 2002, 2003, 2008 Jörg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit protocol)
 ;;
 (usual-integrations)
 (disable-interrupts))

(module
 protocol
 (
  sendmail send-email $smtp-host *not-authorised*
  $authenticate-www-user
  http-property-server
  nameserver parallel-requests
  expect-object
  is-text/scheme? http-property-text/scheme
  http-property-post
  http-require-auth-message
  http-property-soap+xml

  file-name->content-type load-mime.types! mime-types-file
  http-read-content ;; for cgi
  default-log-access log-access
  http-register-handler
  https-client-open
  https-send-message! https-response https-response-auth
  http-send-message! http-response http-for-each
  http-affirm-user-authorization
  $user-agent-needs-html
  $http-is-synchronous
  $http-magic-post-get
  ;;
  http-form-encode
  ;; not sure that we should export the helpers
  http-parse-query http-format-location
  rfc2046->node-list ;; should go into notation (dependency issue)
  http-format-write-location http-format-read-location
  form-element-attributes write-query-form
  http-redirect-off             ; only for debugging
  http-log-header

  ;; sure export the server itself
  http-format-answer
  http-server https-server
  http-request-server			; merly for internal use
  $http-server-verbose
  http-keep-alive-timeout
  http-read-status-line-timeout
  http-with-channel-timeout
  write-plain-timeout
  write-chunk-timeout
  http-read-to-end-timeout
  http-eval-respond-timeout
  short-local-timeout
  
  lmtp-server lmtp-read-data

  display-http-channels http-display-hosts
  *http-routeable-hosts*
  http-close-channel! host-id host-protocol host-address host-cert
  init-http-voters! voter-auth
  host-cert-lookup http-collect-call-function
  http-host-lookup http-all-hosts
  http-host-alive? http-host-maybe-alive? host-seems-dead?
  http-host-add! http-host-remove!
  http-update-alive-time!
  http-connect-back $connect-back
  http-replicate-call  http-replicate-reply http-forward-call
  http-echo-call http-send-ready http-send-one-shot
  http-get-digest http-get-place http-get-meta http-get-blob
  http-get-state http-invite-new-supporters!
  http-get-reply
  http-get-ca
  https-certification-for
  http-send-certificate-request
  ;;
  http-eval
  ;;
  eval-server eval-request-server
  make-tunnel ssl-tunnel
  $http-bandwidth
  $http-max-write-length
  $https-server-require-cert
  $http-requests-per-connection
  $https-requests-per-connection
  $http-client-connections-maximum
  $https-client-verbose $log-content-type-source
  $https-socks4a-server $https-use-socks4a
  the-http-domain
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:)
	 srfi-19 srfi-34 srfi-35 srfi-45 srfi-69
	 atomic mailbox pcre regex util timeout parallel ports
	 (prefix dns dns-) sslsocket
	 tree notation aggregate storage-api place-common place
	 (only msgcoll has-been-forwarded-nl)
	 webdavlo protocol-common rfc-2616 rfc-2518
	 smtp protocol-webdav protocol-connpool http-client protocol-clntclls
	 http-server protocol-lowlevel
	 extras data-structures)

)

(import (prefix protocol m:))


(define sendmail m:sendmail)
(define $smtp-host m:$smtp-host)
(define $authenticate-www-user m:$authenticate-www-user)
(define http-property-server m:http-property-server)
(define nameserver m:nameserver)
(define parallel-requests m:parallel-requests)
(define http-property-post m:http-property-post)
(define http-property-soap+xml m:http-property-soap+xml)
(define load-mime.types! m:load-mime.types!)
(define mime-types-file m:mime-types-file)
(define file-name->content-type m:file-name->content-type)
(define default-log-access m:default-log-access)
(define log-access m:log-access)
(define http-register-handler m:http-register-handler)
(define http-send-message! m:http-send-message!)
(define https-send-message! m:https-send-message!)
(define https-response m:https-response)
(define https-response-auth m:https-response-auth)
(define http-affirm-user-authorization m:http-affirm-user-authorization)
(define $user-agent-needs-html m:$user-agent-needs-html)
(define $http-is-synchronous m:$http-is-synchronous)
(define $http-magic-post-get m:$http-magic-post-get)

(define http-format-write-location m:http-format-write-location)
(define http-format-read-location m:http-format-read-location)
(define http-redirect-off m:http-redirect-off)
(define http-log-header m:http-log-header)

(define http-server m:http-server)
(define https-server m:https-server)
(define $http-server-verbose m:$http-server-verbose)

(define lmtp-server m:lmtp-server)
(define lmtp-read-data m:lmtp-read-data)

(define short-local-timeout m:short-local-timeout)
(define http-for-each m:http-for-each)
(define display-http-channels m:display-http-channels)
(define http-display-hosts m:http-display-hosts)
(define http-close-channel! m:http-close-channel!)
(define host-id m:host-id)
(define host-protocol m:host-protocol)
(define host-address m:host-address)
(define host-cert m:host-cert)
(define init-http-voters! m:init-http-voters!)
(define host-cert-lookup m:host-cert-lookup)
(define http-collect-call-function m:http-collect-call-function)
(define http-host-lookup m:http-host-lookup)
(define http-all-hosts m:http-all-hosts)
(define http-host-alive? m:http-host-alive?)
(define http-host-maybe-alive? m:http-host-maybe-alive?)
(define http-host-add! m:http-host-add!)
(define http-host-remove! m:http-host-remove!)
(define http-update-alive-time! m:http-update-alive-time!)
(define http-connect-back m:http-connect-back)
(define $connect-back m:$connect-back)
(define http-replicate-call m:http-replicate-call)
(define http-replicate-reply m:http-replicate-reply)
(define http-forward-call m:http-forward-call)
(define http-echo-call m:http-echo-call)
(define http-send-ready m:http-send-ready)
(define http-send-one-shot m:http-send-one-shot)
(define http-get-digest m:http-get-digest)
(define http-get-place m:http-get-place)
(define http-get-meta m:http-get-meta)
(define http-get-state m:http-get-state)
(define http-invite-new-supporters! m:http-invite-new-supporters!)
(define http-get-ca m:http-get-ca)
(define https-certification-for m:https-certification-for)
(define http-send-certificate-request m:http-send-certificate-request)
(define http-eval m:http-eval)
(define eval-server m:eval-server)
(define eval-request-server m:eval-request-server)
(define make-tunnel m:make-tunnel)
(define ssl-tunnel m:ssl-tunnel)
(define $http-bandwidth m:$http-bandwidth)
(define $http-max-write-length m:$http-max-write-length)
(define $large-request-handler m:$large-request-handler)
(define $https-server-require-cert m:$https-server-require-cert)
(define $http-requests-per-connection m:$http-requests-per-connection)
(define $https-requests-per-connection m:$https-requests-per-connection)
(define $http-client-connections-maximum m:$http-client-connections-maximum)
(define $https-client-verbose m:$https-client-verbose)
(define $log-content-type-source m:$log-content-type-source)
(define $https-socks4a-server m:$https-socks4a-server)
(define $https-use-socks4a m:$https-use-socks4a)
(define the-http-domain m:the-http-domain)

(import (prefix protocol-common pc:))
(define $large-request-handler pc:$large-request-handler)
