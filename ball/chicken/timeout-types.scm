(define-type :timeout: (or number :time:))
(define-type :defer-target: (or (struct thread) procedure (struct <mailbox>)))
(define-type :timeout-reference: (struct <timeout-queue>))

(: timeout-object? (* --> boolean))

(: display-stop-list (-> . *))
(: update-system-time! (:time: (procedure () . *) -> . *))
(: make-watchdog ((procedure () . *) --> :thunk:))

(: send-alive-signal (-> . *))

(: register-time-procedure! ((procedure (:time:) . *) -> undefined))

;;

(: thread-signal-timeout! ((struct thread) -> . *))

(: register-timeout-message!
   ((or false :timeout:) :defer-target: -> (or boolean :timeout-reference:)))
(: cancel-timeout-message! ((or false :timeout-reference:) -> . *))
(: *system-time* :time:)
(: *system-date* :date:)

(: timestamp (--> :date:))

(: respond-timeout-interval (&rest -> (or false :timeout:)))
(: remote-fetch-timeout-interval (#!rest -> (or boolean :timeout:)))

(: timeout deprecated)

(: with-timeout ((or false :timeout:) :thunk: -> . *))

(: with-timeout! ((or false :timeout:) :thunk: -> . *))

(: timeout-handling-policy (symbol -> undefined))
