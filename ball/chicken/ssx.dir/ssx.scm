;; (C) 2013, Jörg F. Wittenberger - public domain
(use extras)
(use srfi-1)

;(use srfi-34 srfi-35 matchable)
(declare (uses srfi-34 srfi-35))
;;(declare (uses extras posix irregex files)) ; for divert
(declare (uses ssx-divert))
(declare (uses matchable))
(declare (uses srfi-13 srfi-69 srfi-110))
(declare (uses llrb-tree structures alexpander))
(declare (uses synclo))
(declare (uses cps cloev clocom))
(declare (uses make))

(import-for-syntax srfi-34 matchable)
(import ssx-divert) (import (prefix ssx-divert save:))
(import srfi-34 srfi-35)
(import extras)

(import matchable srfi-110)
(import (prefix alexpander alexpander.orig:))
(import (prefix synclo synclo.orig:))

(import make)
(import (prefix structures structures:))

(define empty-binding-set structures:empty-binding-set)
(define binding-set-empty? structures:binding-set-empty?)
(define make-binding-set structures:make-binding-set)
(define binding-set-ref/default structures:binding-set-ref/default)
(define binding-set-ref structures:binding-set-ref)
(define binding-set-insert structures:binding-set-insert)
(define binding-set-cons structures:binding-set-cons)
(define binding-set-fold structures:binding-set-fold)
(define binding-set-union structures:binding-set-union)

(define make-symbolic-environment structures:make-symbolic-environment)
(define symbol-environment-ref/default structures:symbol-environment-ref/default)
(define symbol-environment-ref structures:symbol-environment-ref)
(define symbol-environment-insert structures:symbol-environment-insert)




(keyword-style suffix:)


;; Invocation Parameters

(define check-with-expanders
  '(
    ;; syntactic-closure
    alexpander
    ))


;; Config Parameters

(define %%early-once-only (list '%%early-once-only))

#;(define-macro (regex-case str . clauses)
  (let ((tmp (gensym)))
    `(let ((,tmp ,str))
       ,(let fold ((clauses clauses))
	  (if (null? clauses)
	      '(no-values)
	      (let ((match (gensym))
		    (c (car clauses)))
		(if (eq? 'else (car c))
		    `(let () ,@(cdr c))
		    `(let ((,match (,(car c) ,tmp)))
                      (if ,match
                          (apply (lambda ,(cadr c) ,@(cddr c)) ,match)
                          ,(fold (cdr clauses)))))))))))

(define predefined-syntax
  '(
    ;; Remove declarations
    (define-syntax : (syntax-rules (no-output-at-all) ((_ f ...) no-output-at-all)))
    (define-syntax xthe (syntax-rules () ((_ type-declaration expr) expr)))
    ;;
;    (define-syntax %%%reg-expr->proc (syntax-rules () ((_ rx) (reg-expr->proc rx))))
    ;;
    (define-syntax let-alias 
      (syntax-rules () 
	((_ ((id alias) ...) body ...) 
	 (let-syntax ((helper (syntax-rules () 
				((_ id ...) (begin body ...))))) 
	   (helper alias ...)))))
    ;;
    (define-syntax receive
      (syntax-rules ()
	((_ b expr body ...)
	 (call-with-values (lambda () expr) (lambda b body ...)))))
    (define-syntax receive/values-syntax
      (syntax-rules ()
	((_ bindings expr body ...)
	 (let ((receiver (lambda bindings body ...)))
	   (let-syntax
	       ((rewrite (syntax-rules ()
			   ((_ (*values *values/syntax *list->values *begin) *expr)
			    (letrec-syntax
				((*values (syntax-rules <...> ()
							((_ rv <...>) (receiver rv <...>))))
				 (*values/syntax (syntax-rules <...> ()
							       ((_ rv <...>) (receiver rv <...>))))
				 (*begin (syntax-rules <...> ()
						       ((_ (f a <...>)) (f receiver a <...>))))
				 (*list->values
				  (syntax-rules ()
				    ((_ list)
				     (*list->values bindings list ()))
				    ((_ "reverse" res ())
				     (receiver . res))
				    ((_ "reverse" res (x . tail))
				     (*list->values "reverse" (x . res) tail))
				    ((_ () list vals)
				     (*list->values "reverse" () vals))
				    ((_ (x . m) lst vals)
				     (let ((x (car lst)) (r (cdr lst)))
				       (*list->values m r (x . vals)))))))
			      *expr)))))
	     (extract (values values/syntax list->values begin) expr (rewrite () expr)))))))
    ))

(define alexpander-post-syntax
  '(
    (define-syntax %%%reg-expr->proc (syntax-rules () ((_ rx) (reg-expr->proc rx))))
    ))


;;## Source

;;### Generate Output

;;### Debug And Utils

(define (pperr tag expr)
  (call-with-error-log
   (lambda (p)
     (format p "~a:\n" tag)
     (pretty-print expr p)
     (newline p))))

(define (pperr/s tag exprs)
  (call-with-error-log
   (lambda (p)
     (format p "~a:\n" tag)
     (for-each (lambda (expr) (pretty-print expr p) (newline p)) exprs)
     (newline p))))

(define (pretty-print-all port lst)
  (for-each (lambda (e) (pretty-print e port)) lst))

;; Second winner in worst definition ever.  Be sure to never redefine
;; the standard bindings or just fix this.
(define (rscheme:alexpander-null-output)
  ;; NOT working (map (lambda (def) `(define-inline . ,(cdr def))) null-output)
  '(
    (define-inline (_eqv?_15 a b) (eqv? a b))
    (define-inline (_cons_30 a b) (cons a b))
    (define-macro (_append_31 . r) `(append . ,r))
    (define-macro (_list_32 . r) `(list . ,r))
    (define-macro (_vector_33 . r) `(vector . ,r))
    (define-inline (_list->vector_34 l) (list 'list->vector l))
    (define-macro (_map_35 f . r) `(map ,f . ,r))
    )
  )

(define (alexpander:write! port output)
  (begin
    (pretty-print-all port (rscheme:alexpander-null-output))
    (pretty-print-all port output)))

(define (ssx:write-result! dst result)
  (save:call-with-output-file
   dst
   (lambda (port)
     (alexpander:write! port result))))

;;### Process Input

#;(define (ssx:fold-input-files kons nil lst)
  (fold
   (lambda (file init)
     (append init
	     (call-with-input-file file
	       (lambda (port)
		 (log-progress "processing file \"~a\"" file)
		 (let loop ((expr (read port)))
		   (if (eof-object? expr) '()
		       (kons expr (loop (read port)))))))))
   nil
   lst))

(define ssx:read sweet-read)

(define (ssx:fold-input-files tr kons nil files)
  (fold
   (lambda (file init)
     (let ((new (call-with-input-file file
		  (lambda (port)
		    (log-progress "processing \"~a\"" file)
		    (let loop ((expr (ssx:read port)))
		      (if (eof-object? expr) '()
			  (let ((e (tr expr))) (kons e (loop (ssx:read port))))))))))
       (append init new)))
   nil
   files))


;;### (Various) Expansion(s)

;; RScheme - currently the only backend

;; #### Rationale
;;
;; RScheme may eventually compile alexpander's output.  But I lost
;; patience waiting for it to do so.  We need to fold immediate
;; applications of annonymous lambdas into the corresponding `let`
;; forms to achieve a reasonable compile time of the RScheme backend.

(define collapse-anon-lambda
  (match-lambda
   ( (('lambda (n ...) . body) v ...)
     `(let ,(map
	     (lambda (n v) (list n (collapse-anon-lambda v)))
	     n v)
	. ,(map collapse-anon-lambda body)))
   ( ('lambda (and args (? symbol?)) . body)
     `(lambda ,args . ,(map collapse-anon-lambda body)))
   ( (('letrec ((ln ('lambda (a ...) . body))) lnr) v ...)
     (=> continue)
     (if (eq? ln lnr)
	 `(let ,ln ,(map list a (collapse-anon-lambda v))
	    . ,(map collapse-anon-lambda body))
	 (continue)))
   ( ('letrec ((n v) ...) . body)
     `(letrec ,(map (lambda (n v) (list n (collapse-anon-lambda v))) n v)
	. ,(map collapse-anon-lambda body)))
   ( ('begin . body)
     `(begin . ,(map collapse-anon-lambda body)))
   ( ('if test then . otherwise)
     `(if ,(collapse-anon-lambda test)
	  ,(collapse-anon-lambda then)
	  . ,(map collapse-anon-lambda otherwise)))
   ( ('delay body)
     `(delay ,(collapse-anon-lambda body)))
   ( (x ...)
     (map collapse-anon-lambda x))
   (X X)))

;(define (collapse-anon-lambda b) b)

(define (alexpander-post-process output)
  (fold-right
   (lambda (e i)
     (match e
	    ('no-output-at-all i)
	    (('define defined-item v)
	     `((define ,defined-item ,(collapse-anon-lambda v)) . ,i))
	    #;(('define defined-item . body)
	    `((define ,defined-item
	    . ,(expand-top-level-forms! body mstore))
	    . ,i))
	    ;;(X (append (expand-top-level-forms! (list X) mstore) i))
	    (_ (cons e i))
	    )
     )
   '()
   output))

(define (alexpander:expand-top-level-forms!/verbose forms mstore)
  (pperr/s "Alexin" forms)
  (let ((output (alexpander.orig:expand-top-level-forms! forms mstore)))
    (pperr/s "Alexou" output)
    output))

(define (alexpander:expand-top-level-forms!/quiet forms mstore)
  (guard
   (ex (else (pperr/s "Alexin" forms)
	     (raise ex)))
   (alexpander.orig:expand-top-level-forms! forms mstore)))

(define (synclo:expand-top-level-forms!/quiet forms mstore)
  (guard
   (ex (else (pperr/s "Synclo Input" forms)
	     (raise ex)))
   (list (synclo.orig:expand-top-level-forms! forms mstore))))

;(define alexpander:expand-top-level-forms! alexpander:expand-top-level-forms!/quiet)
(define alexpander:expand-top-level-forms! alexpander:expand-top-level-forms!/verbose)
;(define alexpander:expand-top-level-forms! alexpander.orig:expand-top-level-forms!)

(define synclo:expand-top-level-forms! synclo:expand-top-level-forms!/quiet)

(define known-expanders
  `(
    (syntactic-closure ,synclo:expand-top-level-forms!)
    (alexpander ,alexpander:expand-top-level-forms!)
    ))

(define (ssx:expand-top-level-forms! forms envt)
  (let* ((r0 (fold (lambda (exdef i)
		     (if (memq (car exdef) check-with-expanders)
			 `((,(car exdef)
			    ,((cadr exdef) forms envt))
			   . ,i)
			 i))
		   '()
		   known-expanders))
	 (r1 (assq 'alexpander r0)))
    (cadr r1)))

(define (alexpander:call-fold fold-proc kons knil lst)
  (let ((mstore (alexpander.orig:null-mstore))
	(mstore2 (synclo.orig:null-mstore)))
    (define (tr form)
      (match
       form
       ;; #### Build Specific
       ;; Just load macros from this module.
       (('module nm expt ('import . imp) . body)
	(for-each
	 (lambda (e)
	   (match e
		  (('cond-expand . clauses)
		   (let ((hit (or (assq 'alexpander clauses)
				  (assq 'else clauses))))
		     (if hit (map tr hit))))
		  (_ (tr e))))
	 body)
	'())		; return nothing in this case
       (('cond-expand . body)
	`((,(car form)
	   . ,(map
	       (lambda (cls)
		 (cons
		  (car cls)
		  (apply append
			 (map
			  (lambda (clause)
			    (alexpander-post-process ; not alexpander, target...
			     (ssx:expand-top-level-forms! (list clause) mstore)))
			  (cdr cls)))))
	       body))))
       (('%early-once-only . body)
	(list (cons (car form) (apply append (map tr body))))
	)
;       (('define-macro . more) (list form))
       ;; #### Target Specific
       ((define-keyword (definition-name . args) . body)
	(map
	 (match-lambda
	   (('define expanded-definition-name ('lambda expanded-args . expanded-body))
	    `(define (,expanded-definition-name . ,expanded-args)
	       . ,(map collapse-anon-lambda expanded-body)))
	   (X X))
	 (alexpander-post-process
	  (ssx:expand-top-level-forms! (list form) mstore))))
       ;; #### General
       (_ (alexpander-post-process
	   (ssx:expand-top-level-forms! (list form) mstore)))
       ;; End of long match 
       ))

    (log-progress "prepare predefined syntax") (for-each tr predefined-syntax)	; FIXME, should be elsewhere.
    (fold-proc tr kons knil lst)))


;; Single Run

;;; Invocation

(define (help-invocation)
  (log-error "usage: command ...
  commands:
   expand
   expand+
   make
")
  (raise 'aborted))

(define (ssx:run-expand args)
  (let ((nargs (length args)))
    (cond
     ((< nargs 2)
      (log-error "usage: expand output input ...
   output file \"-\" is special: expand to standard output")
      (raise 'aborted))
     (else
      (ssx:write-result!
       (car args)
       (let ((result (alexpander:call-fold ssx:fold-input-files append '()
					   (source-files (cdr args)))))
	 result))))))

(define (ssx:make-expander eval)
  (let ((mstore (alexpander.orig:null-mstore)))
    (for-each eval alexpander.orig:null-output)
    (lambda (form)
      (alexpander-post-process
       (ssx:expand-top-level-forms! (list form) mstore)))))

(define (ssx:run-make args)
  (if (null? args)
      (begin (log-error "usage: make <file> ... [TO BE CHANGED]") (raise 'aborted))
      (make/load-files
       list ;(ssx:make-expander eval)
       args)))

(define (ssx:compile-make/chicken args)
  (if (null? args)
      (begin (log-error "usage: compile-make to <file> ... [TO BE CHANGED]") (raise 'aborted))
      (save:call-with-output-file
       (car args)
       (lambda (port)
	 (pretty-print-all
	  port
	  `((declare
	     (uses library srfi-1 ports files posix))
	    (module
	     make-outpout
	     ()
	     (import (prefix scheme scheme:))
	     (import (except scheme
			     display
			     call-with-output-file with-output-to-file
			     member assoc eval))
	     (import (except chicken expand file-exists?))
	     (import (prefix chicken chicken:))
	     (import srfi-1 files)
	     (import (except posix file-modification-time))
	     (import (prefix posix posix:))
	     (import (only ports call-with-output-string))
	     (import extras)
	     (define (display x . port)
	       (apply scheme:display (if (filename? x) (literal-filename x) x) port))
	     ,@alexpander.orig:null-output
	     ,@(make/compile-files (ssx:make-expander (lambda (x)x)) (cdr args))
	     (make/proc (*rules*) (command-line-arguments)))
	    ))))))

(let ((args (command-line-arguments)))
  (if (< (length args) 1) (help-invocation)
      (case (string->symbol (car args))
	((expand) (ssx:run-expand (cdr args)))
	((expand+)
	 (set! check-with-expanders (cons 'syntactic-closure check-with-expanders))
	 (ssx:run-expand (cdr args)))
	((make) (ssx:run-make (cdr args)))
	((compile-make) (ssx:compile-make/chicken (cdr args)))
	(else (error (format "unknown command ~a" (car args)))))))

;(newline (current-output-port))
(flush-output (current-output-port))
(log-progress "done.")
(exit 0)
