;;;; clocom.scm - A simple compiler core.

;;; The closure-compiler:
;
; - Returns a procedure of one argument that compiles an s-expression
; into a closure, with the global-environment and special-forms given.
; - Environment-representation:
;   a) Compile-time: a list of lists, where each (inner) list contains
;   the names of the variables in that lexical environment. Newer
;   environments are positioned in front of older ones
;   b) Run-time: a list of vectors, where each vector contains the
;   values of the variables in that lexical environment

(: cc-unbound-symbol (symbol * -> . *))
(define (cc-unbound-symbol x exp)
  ((cc-error-handler)
   (make-condition
    &message 'message (format "unbound variable ~a in ~a" x exp))))

(: make-cc-unbound-symbol (symbol * -> :cc-val:))
(define (make-cc-unbound-symbol x exp)
  (lambda (_)
    (cc-unbound-symbol x exp)))

;; Note the use of a 1-element vector to hold the value: this lets us
;; do the lookup at compile-time.  (We might define-sytax this away.)

(define-syntax cc-global-varref (syntax-rules () ((_ var) (cc-variable-value var))))

(: cc-define-new-global (:cc-global-envt: symbol :cc-val: -> :cc-var:))
(define (cc-define-new-global envt var exp)
  (let ((ref (make-variable 'unbound '* var (make-cc-unbound-symbol var exp))))
    (hash-table-set! envt var ref)
    ref))

(: cc-define-new-global (:cc-global-envt: symbol :cc-val: -> :cc-global-envt:))
(define (cc-define-global! envt var val)
  (hash-table-set! envt var val)
  envt)

(: cc-define-new-global/primop (:cc-global-envt: symbol symbol :cc-val: -> :cc-global-envt:))
(define (cc-define-global/primop! envt var type val)
  (let ((ref (make-variable 'primop type var val)))
    (hash-table-set! envt var ref)
    envt))

(: cc-global-ref (:cc-global-envt: symbol --> (or false :cc-val:)))
(define (cc-global-ref envt var)
  (hash-table-ref/default envt var #f))

;;; Definition of special forms:
;
; - A special-form table maps symbols to compilation-procedures - A
; special form is defined as a procedure taking these arguments:
;
;   1) The complete expression to be compiled
;   2) a compile-procedure that takes an expression and a compile-time
;   environment and returns a procedure
;   X) a lookup-procedure that takes a symbol and a compile-time
;   environment and returns either #f (if the variable is not defined)
;   or a pair of the form (<run-time environment-index>
;   . <variable-index>).
;   3) the current compile-time environment
;   4) a flag "tail position"
;   5) a flag "mode" (for cps)
;

(: cc-special-form-ref (:cc-syntax-envt: symbol --> (or false :cc-expander:)))
(define (cc-special-form-ref envt form)
  (binding-set-ref/default envt form #f))

(: cc-define-special-form! (:cc-global-syntax-envt: symbol :cc-expander: -> undefined))
(define (cc-define-special-form! special-form-table operator compile)
  (vector-set! special-form-table 0
	       (binding-set-cons operator compile (vector-ref special-form-table 0))))

(: cc-bind-special-form (:cc-syntax-envt: symbol :cc-expander: --> :cc-syntax-envt:))
(define (cc-bind-special-form special-form-table operator compile)
  (binding-set-cons operator compile special-form-table))

(define-record-type <cc-core>
  (make-cc-core sym)
  cc-core?
  (sym cc-core-sym))

(define-record-printer (<cc-core> x out)
  (format out "#<CC core ~a>" (cc-core-sym x)))

(define (dbg x a) (format (current-error-port) "CompDbg ~a: ~a\n" x a) x)

(: make-compiler
   (:cc-global-envt:
    :cc-syntax-envt:
    :cc-mode:
    -> (procedure (*) :cc-val:)))
(define (make-compiler global-environment special-form-table mode)
  (lambda (exp . t)
    (define ctx #f)
    ;; Recursive compilation procedure:
    (: compile :cc-compile:)
    (define (compile x e t m)
      (match x
	     ((or (? boolean?) (? keyword?) (? char?) (? number?) (? string?)) ; constant
	      (generate-literal m t x ctx) )
	     ((? symbol?)		; variable
	      (let ((binding (generate-lookup m x e)))
		(match binding
		       (((i . j) . var)	; local or lexical...
			(generate-reference m t binding ctx) )
		       ;; global
		       (_ (let ((s (or (cc-global-ref global-environment x)
				       ;; so far we don't support global definitions
				       (raise (cc-unbound-symbol x exp))
				       ;; else: not yet defined... add a new entry
				       (cc-define-new-global global-environment x exp)))) 
			    (generate-reference m t s ctx) ) ) )) )
	     (((? cc-core? fn) args ...)	; special-form or function-call
	      (let ((h (cc-special-form-ref special-form-table (cc-core-sym fn))))
		(if h
		    (h x compile e t m ctx)
		    ((cc-syntax-error-handler) (cc-core-sym fn))) ) )
	     (((? symbol? fn) args ...)	; special-form or function-call
	      (let ((binding (generate-lookup m fn e)))
		(match
		 binding
		 ((info . var) ; local or lexical...
		  (let ((fnc (generate-reference m t binding ctx))
			(argsc (map (let ((t (generate-receiver m 'combination t ctx)))
				      (lambda (x) (compile x e t m)))
				    args)))
		    (if (primop? var)
			(generate-leaf-combination m t var fnc argsc ctx)
			(generate-combination m t fnc argsc ctx))) )
		 ;; global
		 (_ (let ((var (cc-global-ref global-environment fn)))
		      (if var
			  (let ((fnc (generate-reference m t var ctx))
				(argsc (let ((t (generate-receiver m 'combination t ctx)))
					 (map (lambda (x) (compile x e t m)) args))))
			    (if (primop? var)
				(generate-leaf-combination m t var fnc argsc ctx)
				(generate-combination m t fnc argsc ctx)))
			  (let ((h (cc-special-form-ref special-form-table fn)))
			    (if h
				(h x compile e t m ctx)	; invoke special-form handler
				(raise (cc-unbound-symbol fn exp))))) ) ) )) )
	     ((fn args ...)
	      (generate-combination
	       m t (compile fn e t m)
	       (map (lambda (x) (compile x e (generate-receiver m 'combination t ctx) m)) args) ctx) )
	     (_ ((cc-syntax-error-handler) x)) ) )

    ;;(trace cc-lookup compile)
    (compile exp '() (if (pair? t) (car t) #t) mode) ) )


; - The expander of a macro-special form is a simple one-argument
; procedure that gets an expression and returns another expression
; which is compiled recursively.

(define (macro-expander expander)
  (lambda (x c e t m ctx) (c (expander x) e t m)) )


;;; A special-form table with some default entries:
;
; - Supports: quote, lambda, let, letrec, begin, if

(define (make-symbol-table) (make-hash-table eq?))

(define (cc-make-toplevel-environment) (make-symbol-table))

(define r4rs-special-form-table (vector (make-binding-set)))

(define (cc-r4rs-environment) (vector-ref r4rs-special-form-table 0))

(cc-define-special-form!
 r4rs-special-form-table 'quote
 (lambda (x c e t m context)
   (match x
     ((_ const) (generate-literal m t const context))
     (_ ((cc-syntax-error-handler) x)) ) ) )

(cc-define-special-form!
 r4rs-special-form-table 'lambda
 (lambda (x c e t m context)
   ;; Decompose the lambda-list:
   (define (decomp llist0)
     (let loop ((llist llist0) (vars '()) (argc 0))
       ;; rerevese! since it's just consed up.
       (cond ((null? llist) (values (reverse! vars) argc #f))
	     ((symbol? llist) (let ((rvar (generate-variable m 'restarg '(list-of *) llist e context)))
				(values (reverse! (cons rvar vars)) argc rvar)))
	     ((not (pair? llist)) ((cc-syntax-error-handler) x))
	     (else (let ((avar (generate-variable m 'arg '* (car llist) e context)))
		     (loop (cdr llist) (cons avar vars) (add1 argc)))) ) ) )
   (match x
     ((_ llist body ...)
      (receive
       (vars argc rest) (decomp llist)
       (let* ((e2 (if (null? vars) e (generate-extend-envt m vars e)))
              (bodyc (c (cons cc-sequence body) e2 t m)) )
	 (generate-abstraction m t vars (if rest (- -1 argc) argc) bodyc context)) ) )
     (_ ((cc-syntax-error-handler) x)) ) ) )

(define cc-sequence (make-cc-core 'sequence))

(cc-define-special-form!
 r4rs-special-form-table (cc-core-sym cc-sequence)
 (lambda (x c e t m context)
   (match x
     ((_) (lambda _ (void)))
     ;; ((_ x) (let ((xc (c x e t m))) (lambda (e) (xc e))))
     ((_ x) (c x e t m))
     ;; Drop undefined values.
     ((_ (if #f #f) . xs) (c (cons cc-sequence xs) e t m))
     ;; ((_ x1 . xs)
     ;;  (let ((xc (c x1 e #f m))
     ;; 	    (xsc (c `(,cc-sequence ,@xs) e t m)) )
     ;; 	(lambda (e) (xc e) (xsc e)) ) )
     ((_ x1 . xs)
      (let ((t0 (generate-receiver m 'effect t context)))
	(let ((xc (c x1 e t0 m))
	      (xscl (let loop ((xs xs))
		      (if (null? (cdr xs))
			  (list (c (car xs) e t m))
			  (cons
			   (c (car xs) e t0 m)
			   (loop (cdr xs)))))) )
	  (generate-sequence m t (cons xc xscl) context) )) )
     (_ ((cc-syntax-error-handler) x)) ) ) )

(cc-define-special-form!
 r4rs-special-form-table 'begin
 (macro-expander
  (match-lambda
   ((_ body ...) (cons cc-sequence body))
   (x ((cc-syntax-error-handler) x)) ) ) )

(cc-define-special-form!
  r4rs-special-form-table 'let
  (lambda (x c e t m context)
    (match x
      ((_ ((vars vals) ...) body ...)
       (let ((vars (map (lambda (v) (generate-variable m 'val '* v e context)) vars)))
	 (let ((valsc (map
			 (lambda (l v)
			   (let ((t (generate-receiver m l t context)))
			     (c v e t m)))
		       vars vals))
	       (bodyc (c (cons cc-sequence body) (generate-extend-envt m vars e) t m)) )
	   (generate-let-values m t vars valsc bodyc context) )) )
      ((_ (? symbol? n) ((vars vals) ...) body ...)
       (c `((letrec ((,n (lambda ,vars ,@body)))
	     ,n)
	   ,@vals)
	 e t m) )
      (_ ((cc-syntax-error-handler) x)) ) ))

(cc-define-special-form!
 r4rs-special-form-table 'letrec
 (lambda (x c e t m context)
   (match x
     ((_ ((vars vals) ...) body ...)
      (let ((vars (map (lambda (v) (generate-variable m 'val '* v e context)) vars)))
	(let ((e2 (generate-extend-envt m vars e)))
	  (let ((valsc (map
			  (lambda (l r)
			    (let ((t (generate-receiver m l t context)))
			      (c r e2 t m)))
			vars vals))
		(bodyc (c (cons cc-sequence body) e2 t m)))
	    (generate-letrec-values m t vars valsc bodyc context)) )) )
     (_ ((cc-syntax-error-handler) x)) ) ) )

(cc-define-special-form!
 r4rs-special-form-table 'if
 (lambda (x c e t m context)
   (match x
     ((_ x y) 
      (let ((xc (let ((t (generate-receiver m 'test t context))) (c x e t m)))
            (yc (c y e t m))
	    (zc (c #t e t m)))
	(generate-conditional m t xc yc zc context) ) )
     ((_ x y z)
      (let ((xc (let ((t (generate-receiver m 'test t context))) (c x e t m)))
            (yc (c y e t m)) 
            (zc (c z e t m)) )
	(generate-conditional m t xc yc zc context) ) )
     (_ ((cc-syntax-error-handler) x)) ) ) )

;;; Global environments:
;
; - A global environment is a hash-table (eq? predicate) that maps
; symbols to "boxes", i.e.  one-element vectors containing the actual
; value.
; - The following code is for testing purposes, only.

(define (cc-make-global-environment) (make-symbol-table))

(define cc-syntax-error-handler
  (make-parameter
   (lambda (f . args)
     (raise
      (call-with-output-string
       (lambda (ep)
	 (display "syntax error " ep)
	 (if (pair? args)
	     (apply format ep f args)
	     (begin (display "in " ep) (pretty-print f ep)))))))) )

