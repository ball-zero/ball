;;;; cloev.scm - compiler target interface and a simple evaluator.

(define-record-type <cc-generator>
  (make-cc-generator
   generate-literal
   generate-variable
   generate-lookup
   generate-extend-envt
   generate-reference
   generate-receiver
   generate-abstraction
   generate-test
   generate-conditional
   generate-letrec-values
   generate-let-values
   generate-sequence
   generate-combination
   generate-leaf-combination
   generate-subproblems
   generate-values-binding
   )
  generator?
  (generate-literal gen-generate-literal)
  (generate-variable gen-generate-variable)
  (generate-lookup gen-generate-lookup)
  (generate-extend-envt gen-generate-extend-envt)
  (generate-reference gen-generate-reference)
  (generate-receiver gen-generate-receiver)
  (generate-abstraction gen-generate-abstraction)
  (generate-test gen-generate-test)
  (generate-conditional gen-generate-conditional)
  (generate-letrec-values gen-generate-letrec-values)
  (generate-let-values gen-generate-let-values)
  (generate-sequence gen-generate-sequence)
  (generate-combination gen-generate-combination)
  (generate-leaf-combination gen-generate-leaf-combination)
  (generate-subproblems gen-generate-subproblems)
  (generate-values-binding gen-generate-values-binding)
  )

;; This really should be a syntax expansion extending to define-record-type

(define (derive-cc-generator parent . args)
  (let ((generate-literal (gen-generate-literal parent))
	(generate-variable (gen-generate-variable parent))
	(generate-lookup (gen-generate-lookup parent))
	(generate-extend-envt (gen-generate-extend-envt parent))
	(generate-reference (gen-generate-reference parent))
	(generate-receiver (gen-generate-receiver parent))
	(generate-abstraction (gen-generate-abstraction parent))
	(generate-test (gen-generate-test parent))
	(generate-conditional (gen-generate-conditional parent))
	(generate-letrec-values (gen-generate-letrec-values parent))
	(generate-let-values (gen-generate-let-values parent))
	(generate-sequence (gen-generate-sequence parent))
	(generate-combination (gen-generate-combination parent))
	(generate-leaf-combination (gen-generate-leaf-combination parent))
	(generate-subproblems (gen-generate-subproblems parent))
	(generate-values-binding (gen-generate-values-binding parent))
	)
    (define (set-val! label proc)
      (match
       label
       ('literal: (set! generate-literal proc))
       ('variable: (set! generate-variable proc))
       ('lookup: (set! generate-lookup proc))
       ('extend-envt: (set! generate-extend-envt proc))
       ('reference: (set! generate-reference proc))
       ('receive: (set! generate-receiver proc))
       ('abstraction: (set! generate-abstraction proc))
       ('test: (set! generate-test proc))
       ('conditional: (set! generate-conditional proc))
       ('letrec: (set! generate-letrec-values proc))
       ('let: (set! generate-let-values proc))
       ('sequence: (set! generate-sequence proc))
       ('combination: (set! generate-combination proc))
       ('leaf-combination: (set! generate-leaf-combination proc))
       ('subproblems: (set! generate-subproblems proc))
       ('values-binding: (set! generate-values-binding proc))))
    (define (extend-val! label proc)
      (match
       label
       ('literal: (set! generate-literal (proc generate-literal)))
       ('variable: (set! generate-variable (proc generate-variable)))
       ('lookup: (set! generate-lookup (proc generate-lookup)))
       ('extend-envt: (set! generate-extend-envt (proc generate-extend-envt)))
       ('reference: (set! generate-reference (proc generate-reference)))
       ('receive: (set! generate-receiver (proc generate-receiver)))
       ('abstraction: (set! generate-abstraction (proc generate-abstraction)))
       ('test: (set! generate-test (proc generate-test)))
       ('conditional: (set! generate-conditional (proc generate-conditional)))
       ('letrec: (set! generate-letrec-values (proc generate-letrec-values)))
       ('let: (set! generate-let-values (proc generate-let-values)))
       ('sequence: (set! generate-sequence (proc generate-sequence)))
       ('combination: (set! generate-combination (proc generate-combination)))
       ('leaf-combination: (set! generate-leaf-combination (proc generate-leaf-combination)))
       ('subproblems: (set! generate-subproblems (proc generate-subproblems)))
       ('values-binding: (set! generate-values-binding (proc generate-values-binding)))))
    (let loop ((args args))
      (match
       args
       (() (make-cc-generator
	    generate-literal
	    generate-variable
	    generate-lookup
	    generate-extend-envt
	    generate-reference
	    generate-receiver
	    generate-abstraction
	    generate-test
	    generate-conditional
	    generate-letrec-values
	    generate-let-values
	    generate-sequence
	    generate-combination
	    generate-leaf-combination
	    generate-subproblems
	    generate-values-binding
	    ))
       (((or 'ignore: 'IGNORE:) (or 'extend: 'overwrite:) (? symbol?) (? procedure?) . rest) (loop rest))
       (((or 'ignore: 'IGNORE:) (? symbol?) (or 'extend: 'overwrite:) (? procedure?) . rest) (loop rest))
       (('extend: (? symbol? label) (? procedure? proc) . rest)
	(begin (extend-val! label proc) (loop rest)))
       (('overwrite: (? symbol? label) (? procedure? proc) . rest)
	(begin (set-val! label proc) (loop rest)))
       (((? symbol? label) 'extend: (? procedure? proc) . rest)
	(begin (extend-val! label proc) (loop rest)))
       (((? symbol? label) 'overwrite: (? procedure? proc) . rest)
	(begin (set-val! label proc) (loop rest)))))))

(define (generate-literal mode t datum context)
  ((gen-generate-literal mode) t datum context))
(define (generate-variable mode class type variable envt context)
  ((gen-generate-variable mode) class type variable envt context))
(define (generate-lookup mode variable envt)
  ((gen-generate-lookup mode) variable envt))
(define (generate-extend-envt mode variables envt)
  ((gen-generate-extend-envt mode) variables envt))
(define (generate-reference mode t variable context)
  ((gen-generate-reference mode) t variable context))
(define (generate-receiver mode type receiver context)
  ((gen-generate-receiver mode) type receiver context))
(define (generate-abstraction mode t bvl argc body-generator context)
  ((gen-generate-abstraction mode) t bvl argc body-generator context))
(define (generate-test mode t predicate operand-generators context)
  ((gen-generate-test mode) t predicate operand-generators context))
(define (generate-conditional mode t condition-generator consequent-generator alternative-generator context)
  ((gen-generate-conditional mode) t condition-generator consequent-generator alternative-generator context))
(define (generate-letrec-values mode t bvls generators body-generator context)
  ((gen-generate-letrec-values mode) t bvls generators body-generator context))
(define (generate-let-values mode t bvls generators body-generator context)
  ((gen-generate-let-values mode) t bvls generators body-generator context))
(define (generate-sequence mode t generators context)
  ((gen-generate-sequence mode) t generators context))
(define (generate-combination mode t operator-generator operand-generators context)
  ((gen-generate-combination mode) t operator-generator operand-generators context))
(define (generate-leaf-combination mode t operator operator-generator operand-generators context)
  ((gen-generate-leaf-combination mode) t operator operator-generator operand-generators context))
(define (generate-subproblems mode generators context combiner)
  ((gen-generate-subproblems mode) generators context combiner))
(define (generate-values-binding mode bvls generators context finish)
  ((gen-generate-values-binding mode) bvls generators context finish))

(define-record-type variable
  (make-variable class type reference value)
  variable?
  (class variable-class)
  (type variable-type)
  (reference variable-reference)
  (value variable-value variable-value-set!))

(define-record-printer (variable x out)
  (format out "#<variable ~a ~a ~a>" (variable-reference x) (variable-type x) (variable-class x)))

(define (variable-name x)
  (match
   x
   ((? variable?) (variable-reference x))
   ((? symbol?) x)))

(: primop? (* --> boolean : (struct variable)))
(define (primop? var) (eq? (variable-class var) 'primop))

(: make-cc-varref (fixnum fixnum --> :cc-val:))

(cond-expand
 (no-chicken

  (define-inline (allocate-envt n) (make-vector n))

  (define-syntax cc-varref
    (syntax-rules ()
      ((_ bindings no) (##sys#slot bindings no))))

  (define-inline (make-cc-varref i j)
    (case i
      ((0) (lambda (v) 
	     (##sys#slot (##sys#slot v 0) j)))
      ((1) (lambda (v) 
	     (##sys#slot (##sys#slot (##sys#slot v 1) 0) j)))
      ((2) (lambda (v) 
	     (##sys#slot 
	      (##sys#slot (##sys#slot (##sys#slot v 1) 1) 0)
	      j)))
      ((3) (lambda (v) 
	     (##sys#slot 
	      (##sys#slot
	       (##sys#slot (##sys#slot (##sys#slot v 1) 1) 1)
	       0)
	      j)))
      (else
       (lambda (v)
	 (##sys#slot (##core#inline "C_u_i_list_ref" v i) j)))))

  (define-syntax cc-setvar!
    (syntax-rules ()
      ((_ bindings no v) (##sys#setslot bindings no v))))

  )
 (chicken

  (define-syntax cc-varref
    (syntax-rules () ((_ bindings no) (vector-ref bindings no))))
  (define-syntax cc-setvar! (syntax-rules () ((_ bindings no v) (vector-set! bindings no v))))

  (define-syntax allocate-envt
    (syntax-rules () ((_ n) (make-vector n))))

  (define (make-cc-varref i j)
    (lambda (e)
      (do ((k 0 (add1 k))
	   (e e (cc-varref e 0)) )
	  ((= k i) (cc-varref e j)) ) ))
  )
 (else

  (define-macro (cc-varref bindings no)
    `(vector-ref ,bindings ,no))
  (define-macro (cc-setvar! bindings no v)
    `(vector-set! ,bindings ,no ,v))

  (define-syntax allocate-envt
    (syntax-rules () ((_ n) (make-vector n))))

  (define (make-cc-varref i j)
    (lambda (e)
      (do ((k 0 (add1 k))
	   (e e (cc-varref e 0)) )
	  ((= k i) (cc-varref e j)) ) ))

  ))

(define cc-error-handler
  (make-parameter raise) )

(: cloev-mode (--> (struct <cc-generator>)))
(define cloev-mode
  (let ()

    (cond-expand
     (never
      (define (callof fnc e)

	(let ((p (fnc e)))
	  (if (procedure? p) p
	      ((cc-error-handler)
	       (make-condition
		&message 'message (format "not a procedure: ~a" p))))))
      )
     (else
      (define-inline (callof fnc e)
	(fnc e))
      ))

    (: combine-envt (:cc-envt: :cc-envt: -> :cc-envt:))
    (define-inline (combine-envt inner outer) (begin (cc-setvar! inner 0 outer) inner))
    
    (: extend-envt (:cc-envt: fixnum list -> :cc-envt:))
    (define-inline (extend-envt e n vals)
      (let ((v (allocate-envt (add1 n))))
	(do ((i 1 (add1 i))
	     (vals vals (cdr vals)))
	    ((null? vals)
	     (combine-envt v e))
	  (cc-setvar! v i ((car vals) e)))))

    (: extend-rec-envt (:cc-envt: fixnum list -> :cc-envt:))
    (define-inline (extend-rec-envt e n valsc)
      (let ((n (add1 n)))
	(let* ((v (allocate-envt n))
	       (e (combine-envt v e)) )
	  (do ((i 1 (add1 i))
	       (xs valsc (cdr xs)) )
	      ((fx>= i n) e)
	    (cc-setvar! v i ((car xs) e)) ) )))

    (: callof (:cc-val: :cc-envt: -> (procedure (&rest :cc-val:) . *)))

    ;; Lookup variable in compile-time environment, if found return
    ;; environment- and variable index for the run-time environment. If
    ;; not found, return #f:

    (: cc-lookup :cc-lookup:)
    (define (cc-lookup var e)
      (let loop ((e e) (i 0))
	(and (pair? e)
	     (let loop2 ((vs (car e)) (j 0))
	       (cond ((null? vs) (loop (cdr e) (add1 i)))
		     ((eq? (variable-name (car vs)) var) `((,i . ,j) . ,(car vs)))
		     (else (loop2 (cdr vs) (add1 j))) ) ) ) ) )

    (let ((genvar (lambda (class type variable envt context) (make-variable class type variable #f)))
	  (combiner (lambda (t fnc args context)
		      ;; We use some special cases to speed things up:
		      (match args
			     (() (lambda (e) ((callof fnc e))))
			     ((a1) (lambda (e) ((callof fnc e) (a1 e))))
			     ((a1 a2) (lambda (e) ((callof fnc e) (a1 e) (a2 e))))
			     ((a1 a2 a3) (lambda (e) ((callof fnc e) (a1 e) (a2 e) (a3 e))))
			     ((a1 a2 a3 a4) (lambda (e) ((callof fnc e) (a1 e) (a2 e) (a3 e) (a4 e))))
			     (as (lambda (e) (apply (callof fnc e)
						    (map (lambda (f) (f e)) as)))) ))))
      (let ((mode
		(make-cc-generator
		 ;; generate-literal
		 (lambda (t datum context) (lambda (_) datum))
		 ;; generate-variable
		 genvar
		 ;; generate-lookup
		 cc-lookup
		 ;; generate-extend-envt
		 cons
		 ;; generate-reference
		 (lambda (t variable context)
		   (if (pair? variable)
		       (make-cc-varref (caar variable) (add1 (cdar variable)))
		       (lambda (_) (variable-value variable))))
		 ;; generate-receiver
		 (lambda (type receiver context) type)
		 ;; generate-abstraction
		 (lambda (t vars argc body-generator context)
		   (if (negative? argc)
		       ;; Lambda with rest-parameter:
		       (let ((argc (- -1 argc))
			     (vn (length vars)))
			 (lambda (e)
			   (lambda as
			     (let ((e2 (allocate-envt (add1 vn))))
			       (let loop ((as as) (i 0) (j 1))
				 (cond ((fx>= i argc) 
					(cc-setvar! e2 j as)
					(body-generator (combine-envt e2 e)) )
				       ((null? as)
					((cc-error-handler)
					 (make-condition
					  &message 'message
					  (format "too few arguments (~a) in function call ~a"
						  (length as) vn))) )
				       (else
					(cc-setvar! e2 j (car as))
					(loop (cdr as) j (add1 j)) ) ) ) ) ) ))
		       ;; ... without rest-parameter:
		       (case argc
			 ;; We handle 0 args specially (more efficient):
			 ((0) (lambda (e) (lambda () (body-generator e))))
			 (else
			  (let ((vn (length vars)))
			    (lambda (e)
			      (lambda as
				(let ((e2 (allocate-envt (add1 vn))))
				  (let loop ((as as) (i 0) (j 1))
				    (cond ((fx>= i vn)
					   (if (null? as)
					       (body-generator (combine-envt e2 e))
					       ((cc-error-handler)
						(make-condition
						 &message 'message
						 (format "too many arguments (~a) in function call ~a"
							 (length as) vn))) ) )
					  ((null? as)
					   ((cc-error-handler)
					    (make-condition
					     &message 'message
					     (format "too few arguments (~a) in function call ~a"
						     (length as) argc))) )
					  (else
					   (cc-setvar! e2 j (car as))
					   (loop (cdr as) j (add1 j)) ) ) ) ) ) ) ) ) )))
		 ;; generate-test
		 (lambda (t predicate operand-generators context)
		   (lambda (e) (apply predicate (map (lambda (g) (g e)) operand-generators))))
		 ;; generate-conditional
		 (lambda (t condition-generator consequent-generator alternative-generator context)
		   (lambda (e) (if (condition-generator e) (consequent-generator e) (alternative-generator e))))
		 ;; generate-letrec-values
		 (lambda (t bvls valsc bodyc context)
		   (let ((n (length valsc)))
		     (lambda (e) (bodyc (extend-rec-envt e n valsc)) )))
		 ;; generate-let-values
		 (lambda (t vars valsc bodyc context)
		   (let ((n (length valsc)))
		     (lambda (e)
		       (bodyc (extend-envt e n valsc)) )))
		 ;; generate-sequence
		 (lambda (t generators context)
		   (let loop ((generators generators))
		     (let ((xc (car generators))
			   (xsc (if (null? (cddr generators)) (cadr generators)
				    (loop (cdr generators)))))
		       (lambda (e) (xc e) (xsc e)))))
		 ;; generate-combination
		 combiner
		 ;; generate-leaf-combination
		 (lambda (t operator operator-generator operand-generators context)
		   (combiner t operator-generator operand-generators context))
		 ;; generate-subproblems
		 (lambda (generators context combiner) (error "subproblems not implemented in cloev"))
		 ;; generate-values-binding
		 (lambda (bvls generators context finish)
		   (error "values-binding Not Yet Implemented in cloev"))
		 )))
	(lambda () mode)))))
