;; (C) 2015 Joerg F. Wittenberger see http://www.askemos.org

(declare
 (unit clocom)
 (not standard-bindings with-exception-handler)
 ;; optional
 (disable-interrupts)
 ;; promises
 (strict-types)
 (usual-integrations)
 )

(module
 clocom
 (
  cc-make-global-environment
  cc-define-new-global			; export?
  cc-define-global!
  cc-define-global/primop!
  cc-global-ref
  cc-define-special-form!
  make-compiler
  macro-expander
  cc-make-toplevel-environment
  cc-r4rs-environment
  cc-define-special-form!
  cc-bind-special-form
  )
 (import scheme)
 (import (except chicken
		 ;; add1 sub1
		 condition?
		 ;; promise?
		 with-exception-handler))
 (import srfi-34 srfi-35)
 (import srfi-1)
 (import structures srfi-69)
 (import matchable)
 (import (only extras format pretty-print))
 (import (only ports call-with-output-string))
 (import cloev)

 (include "transformers/clocom/clotypes.scm") 
 (include "transformers/clocom/clocom.scm")
 
 )

(import (prefix clocom m:))

(define cc-make-global-environment m:cc-make-global-environment)
(define cc-define-new-global m:cc-define-new-global)
(define cc-define-global! m:cc-define-global!)
(define cc-define-global/primop! m:cc-define-global/primop!)
(define cc-global-ref m:cc-global-ref)
(define cc-define-special-form! m:cc-define-special-form!)
(define make-compiler m:make-compiler)
(define macro-expander m:macro-expander)
(define cc-make-toplevel-environment m:cc-make-toplevel-environment)
(define cc-r4rs-environment m:cc-r4rs-environment)
(define cc-define-special-form! m:cc-define-special-form!)
(define cc-bind-special-form m:cc-bind-special-form)

