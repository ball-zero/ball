;; (C) 2015 Joerg F. Wittenberger see http://www.askemos.org

(declare
 (unit cloev)
 (not standard-bindings with-exception-handler)
 ;; optional
 (disable-interrupts)
 ;; promises
 (strict-types)
 (usual-integrations)
 )

(module
 cloev
 *
 (import scheme)
 (import (except chicken
		 ;; add1 sub1
		 condition?
		 ;; promise?
		 with-exception-handler))
 (import srfi-34 srfi-35)
 (import matchable)
 (import (only extras format))

 (include "transformers/clocom/clotypes.scm") 
 (include "transformers/clocom/cloev.scm") 
 )

(import (prefix cloev m:))

(define make-cc-generator m:make-cc-generator)
(define cc-generator? m:generator?)
(define derive-cc-generator m:derive-cc-generator)
(define generate-literal m:generate-literal)
(define generate-variable m:generate-variable)
(define generate-lookup m:generate-lookup)
(define generate-extend-envt m:generate-extend-envt)
(define generate-reference m:generate-reference)
(define generate-abstraction m:generate-abstraction)
(define generate-test m:generate-test)
(define generate-conditional m:generate-conditional)
(define generate-letrec-values m:generate-letrec-values)
(define generate-let-values m:generate-let-values)
(define generate-sequence m:generate-sequence)
(define generate-combination m:generate-combination)
(define generate-leaf-combination m:generate-leaf-combination)
(define generate-subproblems m:generate-subproblems)
(define generate-values-binding m:generate-values-binding)

(define cc-make-variable m:make-variable)
(define cc-variable? m:variable?)
(define cc-variable-class m:variable-class)
(define cc-variable-type m:variable-type)
(define cc-variable-reference m:variable-reference)
(define cc-variable-value m:variable-value)
(define cc-variable-value-set! m:variable-value-set!)

(define cc-variable-name m:variable-name)

(define cc-primop? m:primop?)

(define cc-error-handler m:cc-error-handler)
(define cc-cloev-mode m:cloev-mode)
