;; (C) 2014 Joerg F. Wittenberger see http://www.askemos.org

(declare
 (unit cps)
 (not standard-bindings with-exception-handler)
 ;; optional
 (disable-interrupts)
 ;; promises
 (strict-types)
 (usual-integrations)
 )

(module
 cps
 *
 #;(
  with-cps-generation
  cps/generate-reference
  cps/generate-literal
  cps/generate-abstraction
  cps/generate-operand
  cps/generate-conditional
  cps/generate-sequence
  ;; Combinations and Tests
  cps/generate-combination
  cps/generate-leaf-combination
  cps/generate-test
  ;; Bindings
  cps/generate-let-values
  cps/generate-letrec-values
  ;; Continuators
  operand-continuator
  effect-continuator
  conditional-continuator
  variable-continuator
  inline-continuator
  reified-continuator
  fork-continuator ;; maybe not to be exported?
  ;;
  make-cps-target
  build-cps-target
  using-cps-target
  )
 (import scheme chicken)

 (define-type :thunk: (procedure () . *))
 (define-type :cps-target: (struct <cps-target>))
 (define-type :with-cps-target: (procedure (:thunk:) :thunk:))
 (: make-cps-target
    (:with-cps-target:
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     procedure
     --> :cps-target:))

 (define-record-type <cps-target>
   (make-cps-target
    with-cps-target
    make-reference make-literal make-user-abstraction make-conditional
    make-conditional-join make-combination make-leaf-combination
    make-test make-subproblem make-fork make-Y
    make-unary-subproblem-bvl subproblem-bvl-variables
    make-truth-test
    make-return
    make-subproblem-continuation-abstraction
    make-value-continuation-abstraction
    make-ignoring-continuation-abstraction)
   cps-target?
   (with-cps-target %cps-target/with-cps-target)
   (make-reference %cps-target/make-reference)
   (make-literal %cps-target/make-literal)
   (make-user-abstraction %cps-target/make-user-abstraction)
   (make-conditional %cps-target/make-conditional)
   (make-conditional-join %cps-target/make-conditional-join)
   (make-combination %cps-target/make-combination)
   (make-leaf-combination %cps-target/make-leaf-combination)
   (make-test %cps-target/make-test)
   (make-subproblem %cps-target/make-subproblem)
   (make-fork %cps-target/make-fork)
   (make-Y %cps-target/make-Y)
   (make-unary-subproblem-bvl %cps-target/make-unary-subproblem-bvl)
   (subproblem-bvl-variables %cps-target/subproblem-bvl-variables)
   (make-truth-test %cps-target/make-truth-test)
   (make-return %cps-target/make-return)
   (make-subproblem-continuation-abstraction %cps-target/make-subproblem-continuation-abstraction)
   (make-value-continuation-abstraction %cps-target/make-value-continuation-abstraction)
   (make-ignoring-continuation-abstraction %cps-target/make-ignoring-continuation-abstraction)
   )

 ;; To handle keyword arguments without a syntax expander.  TBD, just keep the export.
 (define (build-cps-target . args)
   (apply make-cps-target args))

  (define *cps-target* (make-parameter #f))

  (define (using-cps-target target procedure)
    (parameterize
     ((*cps-target* target))
     (with-cps-target procedure)))

  (define-syntax define-cps-target
    (syntax-rules ()
      ((_ (name . args) selector)
       (define (name . args)
	 ((selector (*cps-target*)) . args)))))

  (: with-cps-target (:cps-target: --> :with-cps-target:))
  (define-cps-target (with-cps-target procedure) %cps-target/with-cps-target)
  (define-cps-target (cps-target/make-reference variable context) %cps-target/make-reference)
  (define-cps-target (cps-target/make-literal datum context) %cps-target/make-literal)
  (define-cps-target (cps-target/make-user-abstraction bvl body-constructor context)
    %cps-target/make-user-abstraction)
  (define-cps-target (cps-target/make-conditional condition consequent alternative context)
    %cps-target/make-conditional)
  (define-cps-target (cps-target/make-conditional-join abstraction constructor context)
    %cps-target/make-conditional-join)
  (define-cps-target (cps-target/make-combination operator continuation operands context)
    %cps-target/make-combination)
  (define-cps-target (cps-target/make-leaf-combination operator operands context)
    %cps-target/make-leaf-combination)
  (define-cps-target (cps-target/make-test predicate operands context)
    %cps-target/make-test)
  (define-cps-target (cps-target/make-subproblem variables bvl constructor context)
    %cps-target/make-subproblem)
  (define-cps-target (cps-target/make-fork subproblems join context)
    %cps-target/make-fork)
  (define-cps-target (cps-target/make-Y subproblems body context)
    %cps-target/make-Y)
  (define-cps-target (cps-target/make-unary-subproblem-bvl context)
    %cps-target/make-unary-subproblem-bvl)
  (define-cps-target (cps-target/subproblem-bvl-variables bvl context)
    %cps-target/subproblem-bvl-variables)
  (define-cps-target (cps-target/make-truth-test operand context)
    %cps-target/make-truth-test)
  (define-cps-target (cps-target/make-return continuation operands context)
    %cps-target/make-return)
  (define-cps-target (cps-target/make-subproblem-continuation-abstraction
		      variables bvl body context)
    %cps-target/make-subproblem-continuation-abstraction)
  (define-cps-target (cps-target/make-value-continuation-abstraction
		      body-constructor context)
    %cps-target/make-value-continuation-abstraction)
  (define-cps-target (cps-target/make-ignoring-continuation-abstraction body context)
    %cps-target/make-ignoring-continuation-abstraction)

  (include "transformers/cps-campbell/cps.scm")
  
  )

(import (prefix cps m:))

(define with-cps-generation m:with-cps-generation)
(define cps/generate-reference m:cps/generate-reference)
(define cps/generate-literal m:cps/generate-literal)
(define cps/generate-abstraction m:cps/generate-abstraction)
(define cps/generate-operand m:cps/generate-operand)
(define cps/generate-conditional m:cps/generate-conditional)
(define cps/generate-sequence m:cps/generate-sequence)
(define cps/generate-combination m:cps/generate-combination)
(define cps/generate-leaf-combination m:cps/generate-leaf-combination)
(define cps/generate-test m:cps/generate-test)
(define cps/generate-let-values m:cps/generate-let-values)
(define cps/generate-letrec-values m:cps/generate-letrec-values)
(define operand-continuator m:operand-continuator)
(define effect-continuator m:effect-continuator)
(define conditional-continuator m:conditional-continuator)
(define variable-continuator m:variable-continuator)
(define inline-continuator m:inline-continuator)
(define reified-continuator m:reified-continuator)
(define fork-continuator m:fork-continuator) ;; maybe not to be exported?

(define make-cps-target m:make-cps-target)
(define build-cps-target m:build-cps-target)
(define using-cps-target m:using-cps-target)
