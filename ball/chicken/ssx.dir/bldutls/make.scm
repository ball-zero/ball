;;;; make.scm - PLT's `make' macro for CHICKEN - adapted from felix'
;;;; version by jfw

#|
expands to

  (make/proc
    (list (list target (list depend ...) (lambda () command ...)) ...)
    argv)

> (make/proc spec argv) performs a make according to `spec' and using
`argv' as command-line arguments selecting one or more targets.
`argv' can either be a string or a vector of strings.

`spec' is a MAKE-SPEC:

  MAKE-SPEC = (list-of MAKE-LINE)
  MAKE-LINE = (list TARGET (list-of DEPEND-STRING) COMMAND-THUNK)
  TARGET = (union string (list-of string)) ; either a string or a list of strings
  DEPEND-STRING = string
  COMMAND-THUNK = (-> void)

To make a target, make/proc is first called on each of the target's
dependencies. If a target is not in the spec and it exists, then the
target is considered made. If a target is older than any of its
dependencies, the corresponding COMMAND-THUNK is invoked. The
COMMAND-THUNK is optional; a MAKE-LINE without a COMMAND-THUNK is
useful as a target for making a number of other targets (the
dependencies).

Parameters:

> ($make-print-checking [on?]) - If #f, make only prints when it is
making a target. Otherwise, it prints when it is checking the
dependancies of a target. Defaultly #t.

> ($make-print-dep-no-line [on?]) - If #f, make only prints "checking..."
lines for dependancies that have a corresponding make line.  Defaultly
#f.

> ($make-print-reasons [on?]) If #t, make prints the reason for each
dependancy that fires. Defaultly #t.
|#

(declare
 (unit make)
 (uses srfi-34 posix)
 ;; promises
 (strict-types)
 (usual-integrations)
)

;; To be replaced by specialized eval:
(declare (uses eval))

(module
 make
 (
  $make-print-checking
  $make-print-dep-no-line
  $make-print-reasons
  make/load-files
  make/compile-files
  make/add-rule!
  ;; helpers
  run
  all-source-files
  all-result-files			; may miss some!
  )
 
 (import scheme)
 (import srfi-34 srfi-35)
 (import (except chicken file-exists?))
 (import (except posix file-modification-time))
 (import ssx-divert)
 (import extras srfi-1 matchable srfi-110)
 (import (only ports call-with-output-string))

 ;;(import eval) ;; to be replaced by specialised eval

 ;; This is a bad idea.  TODO: take it out.
 (: call-with-list-extending1
    ((procedure (* list) . *)		 ; receiver
     (procedure ((procedure (*) *)) . *) ; body
     -> list))
 (define (call-with-list-extending1 receiver proc)
   (let* ((h (list #f))
	  (t h)
	  (r (proc (lambda (x) (let ((p (list x)))
				 (set-cdr! t p)
				 (set! t p))))))
     (receiver r (cdr h))))


 (*source-code* '(define (raise obj) (error obj)))

 (*syntax-code*

  '(define-syntax set-default!
     (syntax-rules ()
       ((_ param val)
	(if (not param) (set! param val)))))

  '(define-syntax guard
     (syntax-rules ()
       ((guard (var clause ...) e1 e2 ...)
	((call-with-current-continuation
	  (lambda (guard-k)
	    (with-exception-handler
	     (lambda (condition)
	       ((call-with-current-continuation
		 (lambda (handler-k)
		   (guard-k
		    (lambda ()
		      (let ((var condition))      ; clauses may SET! var
			(guard-aux (handler-k (lambda ()
						(raise condition)))
				   clause ...))))))))
	     (lambda ()
	       (call-with-values
		   (lambda () e1 e2 ...)
		 (lambda args
		   (guard-k (lambda ()
			      (apply values args)))))))))))))

  '(define-syntax guard-aux
     (syntax-rules (else =>)
       ((guard-aux reraise (else result1 result2 ...))
	(begin result1 result2 ...))
       ((guard-aux reraise (test => result))
	(let ((temp test))
	  (if temp 
	      (result temp)
	      reraise)))
       ((guard-aux reraise (test => result) clause1 clause2 ...)
	(let ((temp test))
	  (if temp
	      (result temp)
	      (guard-aux reraise clause1 clause2 ...))))
       ((guard-aux reraise (test))
	test)
       ((guard-aux reraise (test) clause1 clause2 ...)
	(let ((temp test))
	  (if temp
	      temp
	      (guard-aux reraise clause1 clause2 ...))))
       ((guard-aux reraise (test result1 result2 ...))
	(if test
	    (begin result1 result2 ...)
	    reraise))
       ((guard-aux reraise (test result1 result2 ...) clause1 clause2 ...)
	(if test
	    (begin result1 result2 ...)
	    (guard-aux reraise clause1 clause2 ...)))))

  )

 (define+source
   (define $make-print-checking (make-parameter #f))
   (define $make-print-dep-no-line (make-parameter #f))
   (define $make-print-reasons (make-parameter #f))
   (define $make-print-eval (make-parameter #f))

   (let ((setp (get-environment-variable "SSX_VERBOSE")))
     (and setp
	  (let ((parms `((#\c . ,$make-print-checking)
			 (#\n . ,$make-print-dep-no-line)
			 (#\r . ,$make-print-reasons)
			 (#\e . ,$make-print-eval))))
	    (do ((i 0 (+ i 1))) ((= i (string-length setp)))
	      (let* ((c (string-ref setp i))
		     (o (assv c parms)))
		(if o ((cdr o) (not ((cdr o))))))))))

   )


 
 (define-type :filename: (struct <filename>))
 (define-type :make-command: (-> . *))
 (define-type :makerule-command-entry: (pair :make-command: *))
 
 ;; Rules

 (define+source
   (define-record-type <makerule>
     (make-makerule doc fresh exist dependent commands)
     makerule?
     (doc makerule-doc)			; a short doc string, not yet used
     (fresh makerule-fresh)		; results which MUST be fresh
     (exist makerule-exist)		; aux. results must just exists
     (dependent makerule-dependent)	; dependencies
     (commands makerule-commands)		; commands
     ))

 (define-type :makerule: (struct <makerule>))
 (: makerule? (* --> boolean : :makerule:))
 (: makerule-doc (:makerule: --> (or boolean string)))
 (: makerule-fresh (:makerule: --> (list-of :filename:)))
 (: makerule-exist (:makerule: --> (list-of :filename:)))
 (: makerule-dependent (:makerule: --> (list-of :filename:)))
 (: makerule-commands (:makerule: --> (list-of :makerule-command-entry:)))
 (: make-makerule
    ((or boolean string)
     (list-of :filename:)
     (list-of :filename:)
     (list-of :filename:)
     (list-of :makerule-command-entry:)
     --> :makerule:))

;;; Test And Accumulate

 (: *rules* (#!rest :makerule: -> (list-of :makerule:)))
 (define+source (define *rules* (make-list-accumulator)))

 (define-type :make-object: (or :filename: string symbol))
 (: make/add-rule!
    ((list-of :make-object:)
     (list-of :make-object:)
     (list-of :make-object:)
     (list-of :make-command:)
     -> *))
 (define+source
   (define (make/add-rule! fresh-targets exist-targets depends . commands)
     (let ((fresh-targets (flatmap make-simple-filename fresh-targets))
	   (exist-targets (flatmap make-simple-filename exist-targets))
	   (depends (flatmap make-simple-filename depends)))
       ;; (redundant - produced by syntax) check commands
       (assert
	(every
	 (lambda (c)
	   (or (procedure? (car c))
	       (raise (format "~a command part of line is not a thunk: ~a"
			      (append fresh-targets exist-targets) c))))
	 commands))
       (*rules* (make-makerule #f fresh-targets exist-targets depends commands)))))

 ;; Assertions

 (: assert-source-preservation! ((list-of :makerule:) --> *))
 (define (assert-source-preservation! rules)
   (let ((targets (concatenate (map makerule-fresh rules))))
     (for-each
      (lambda (depend)
	(if (not (member depend targets filename=?)) (source-file depend)))
      (concatenate (map makerule-dependent rules)))
     (assert-full-source-preservation!)))

 (define (all-source-files)
   (assert-source-preservation! (*rules*))
   (current-source-files))

 (define (all-result-files)
   (assert-source-preservation! (*rules*))
   (current-result-files))

 ;; Eventually execute them.

 (define *make/env* (get-environment-variables))

 (define *make-target* '())

 (define *parameters* '())

 ;; Interpret "special" control syntax.  (ATM embedded in vectors.)

 (define (control/endif? expr)
   (if (eof-object? expr) (raise 'missing-end))
   (and (vector? expr) (> (vector-length expr) 0)
	(memq (vector-ref expr 0) '(endif: endif end: end))))

 (define (control/if? expr)
   (if (eof-object? expr) (raise 'missing-end))
   (and (vector? expr) (> (vector-length expr) 0)
	(memq (vector-ref expr 0) '(if: if))))

 (define (make/handle-control! read expand eval control! active?. expr)
   (or (> (vector-length expr) 0) (raise expr))
   (case (vector-ref expr 0)
     ((param: param parameter: parameter)
      (let ((name (vector-ref expr 1))
	    (dflt (and (> (vector-length expr) 2) (vector-ref expr 2))))
	(or (string? name) (symbol? name) (raise expr))
	(let* ((nmstr (if (string? name) name (symbol->string name)))
	       (nmsym (if (symbol? name) name (string->symbol name)))
	       (val (or (and-let* ((p (assoc nmstr *make/env*))) (cdr p))
			dflt)))
	  ;; define only if not yet defined
	  (guard (ex (else (for-each eval (expand `(define ,nmsym ,val)))
			   (set! *parameters* (cons nmsym *parameters*))))
		 (eval nmsym)))))
     ((if if:)
      (or (> (vector-length expr) 1) (raise expr))
      (let* ((pred (vector-ref expr 1))
	     (val (and active?. (eval (car (expand pred))))))
	(if val
	    (do ((expr (read) (read)))
		((control/endif? expr) #t)
	      (cond
	       ((vector? expr) (control! read expand eval control! #t expr))
	       (else (for-each eval (expand expr)))))
	    (let loop ((expr (read)))
	      (cond
	       ((control/if? expr) (loop (read)) (loop (read)))
	       ((not (control/endif? expr)) (loop (read))))))))
     (else #f)))

 ;; Compile "special" control syntax.  See make/handle-control!
 (define (make/compile-control! read expand eval control! would-be-active expr)
   (or (> (vector-length expr) 0) (raise expr))
   (case (vector-ref expr 0)
     ((param: param parameter: parameter)
      (let ((name (vector-ref expr 1))
	    (dflt (and (> (vector-length expr) 2) (vector-ref expr 2))))
	(or (string? name) (symbol? name) (raise expr))
	(let* ((nmstr (if (string? name) name (symbol->string name)))
	       (nmsym (if (symbol? name) name (string->symbol name)))
	       (val (or (and-let* ((p (assoc nmstr *make/env*))) (list (cdr p)))
			(expand dflt))))
	  ;; define only if not yet defined
	  (if (not (memq nmsym *parameters*))
	      (eval `(define-make-parameter ,nmsym ,nmstr ,@val))))))
     ((if if:)
      (or (> (vector-length expr) 1) (raise expr))
      (let* ((pred (expand (vector-ref expr 1)))
	     (body (call-with-list-extending1
		    (lambda (dummy body) body)
		    (lambda (eval2)
		      (define compile
			(match-lambda
			 (('define-make-parameter nmsym nmstr val)
			  (begin
			    (eval `(define-make-parameter ,nmsym ,nmstr #f))
			    (eval2 `(if (not ,nmsym) (set! ,nmsym ,val)))))
			 (X (eval2 X))))
		      (do ((expr (read) (read)))
			  ((control/endif? expr) #t)
			(cond
			 ((vector? expr) (control! read expand compile control! would-be-active expr))
			 (else (for-each compile (expand expr)))))))))
	(if (pair? body)
	    (eval `(if ,@pred ,(if (pair? (cdr body)) (cons 'begin body) (car body)))))))
     (else #f)))

 (define (load-file! read expand eval control! f)
   (with-input-from-file f
     (lambda ()
       (do ((expr (read) (read)))
	   ((eof-object? expr) #t)
	 (cond
	  ((vector? expr) (control! read expand eval control! #t expr))
	  (else
	   (guard
	    (ex (else
		 ;; FIXME print the actual error, not only it's type
		 (log-error "Error: ~s in ~a" ex expr)
		 (raise ex)))
	    (for-each eval (expand expr)))))))))

 (define (make/load-files expand files)
   (define eval.local eval)		; be sure to never look at tl again!
   (define (eval-logged expr) (eval.local (debug 'Eval expr)))
   (define used-eval (if ($make-print-eval) eval-logged eval.local))
   (define (loadem all)
     (log-progress "make: load ~a" (car all))
     (load-file! sweet-read expand used-eval make/handle-control! (car all))
     (if (pair? (cdr all)) (loadem (cdr all))))
   (for-each (lambda (s) (for-each used-eval (expand s))) (*syntax-code*))
   (for-each
    used-eval
    '((set! with-output-to-file ssx-save:with-output-to-file)
      (set! call-with-output-file ssx-save:call-with-output-file)
      ))
   (loadem files)
   (assert-source-preservation! (*rules*))
   (log-progress "Parameters: ~a" *parameters*)
   (log-progress "Source files: ~a" (map literal-filename (current-source-files)))
   (log-progress "Result files: ~a" (map literal-filename (current-result-files)))
   (make/proc (*rules*) *make-target*) ; FIXME argv handling missing
   )

 (*source-code*
  '(define (%%*make-parameter-value*%% name default)
     (or (get-environment-variable name) default)))

 (define (make/compile-files expand files)
   (for-each expand (*syntax-code*))
   (call-with-list-extending1
    (lambda (dummy body) body)
    (lambda (add-expr)
      (define compile
	(let ((params '()))
	  (match-lambda
	   (('define-make-parameter nmsym nmstr val)
	    (if (not (memq nmsym params))
		(begin
		  (set! params (cons nmsym params))
		  (add-expr
		   `(define ,nmsym (%%*make-parameter-value*%% ,nmstr ,val))))))
	   #;(('make/add-rule! fresh-targets exist-targets depends . commands)
	   (apply make/add-rule! fresh-targets exist-targets depends commands))
	   (X (add-expr X)))))
      (define (loadem all)
	(log-progress "compile make: load ~a" (car all))
	(load-file! read expand compile make/compile-control! (car all))
	(if (pair? (cdr all)) (loadem (cdr all))))
      (for-each (lambda (e) (for-each add-expr (expand e))) (*source-code*))
      (loadem files))))

 

 (define+source

   (define (find-matching-rule target exists rules)
     (let ((match? (lambda (s) (filename=? s target))))
       (let loop ((rules rules))
	 (and (pair? rules)
	      (let ((r (car rules)))
		(if (or (any match? (makerule-fresh r))
			(and (not exists) (any match? (makerule-exist r))))
		    r (loop (cdr rules))))))))

 ;;  (define *made* (make-list-accumulator))

   (define (make-file-for-reason target tgtlit date line indent depends)
     (and-let*
      ((reason
	(or (not date)
	    (find (lambda (dep)
		    (or (file-exists? dep)
			(error (format "dependancy ~a was not made"
				       (literal-filename dep))))
		    (and (> (file-modification-time dep) date)
			 dep))
		  depends))))
;;      (*made* target)
      (log-progress
       "make: ~amaking ~a~a"
       (if ($make-print-checking) indent "")
       tgtlit
       (if ($make-print-reasons)
	   (cond
	    ((not date)
	     (string-append " because " tgtlit " does not exist"))
	    ((filename? reason)
	     (string-append " because " (literal-filename reason) " changed"))
	    (else
	     (format " just because ~a is was modified ~a second later" 
		     (literal-filename reason)
		     (- (file-modification-time reason) date))))
	   ""))
      (for-each
       (lambda (x)
	 (guard
	  (ex (else (log-error
		     "make: Failed to make ~a in ~a: ~a"
		     (map literal-filename
			  (append (makerule-fresh line)
				  (makerule-exist line)))
		     (cdr x)
		     (if ((condition-predicate 'exn) ex)
			 (list
			  ((condition-property-accessor 'exn 'message) ex)
			  ((condition-property-accessor 'exn 'arguments) ex)
			  ((condition-property-accessor 'exn 'location) ex))
			 ex))
		    ;;(raise ex)
		    (log-error "make aborted")
		    (raise ex)))
	  ((car x))))
       (makerule-commands line))
      (flush-output (current-output-port))
      (log-progress " make: made ~a" tgtlit)
      ))

   (define (make-file spec target context indent)
     (let ((tgtlit (literal-filename target))
	   (date (and (file-exists? target)
		      (file-modification-time target))))
       (let ((line (find-matching-rule target date spec)))

	 (if (and ($make-print-checking)
		  (or line
		      ($make-print-dep-no-line)))
	     (log-progress "make: ~achecking ~a" indent tgtlit))

	 (if (member target context filename=?)
	     (raise (string-append "recursive dependency on " tgtlit)))

	 (if line
	     (let ((depends (makerule-dependent line)))
	       (let ((inner-indent (string-append indent " "))
		     (inner-context (cons target context)))
		 (for-each (lambda (d) (make-file spec d inner-context inner-indent))
			   depends))
	       (make-file-for-reason target tgtlit date line indent depends))
	     (if date
		 (source-file target)
		 (error (format "don't know how to make ~a\nRules:\n~a"
				tgtlit
				(map (lambda (r)
				       (format "fresh: ~a exist:~a\n"
					       (map tostr (makerule-fresh r))
					       (map tostr (makerule-exist r))))
				     spec))))))))

   )
 
 (define (make:form-error s p)
   (error (format "~a: ~s" s p)))
 (define (make:line-error s p n)
   (error (format "~a: ~s for line: ~a" s p n)))

 (define (make:check-spec spec)
   (and
    #;(or (list? spec) (make:form-error "specification is not a list" spec))
    (or (pair? spec) (make:form-error "specification is an empty list" spec))
    (every
     (lambda (line)
       (and
	#;(or (and (list? line) (< 2 (length line) 3))
	(make:form-error "list is not a list with 2 or 3 parts" line))
	(or (every filename? (makerule-fresh line))
	    (make:form-error "fresh targets invalid" line))
	(or (every filename? (makerule-exist line))
	    (make:form-error "exist targets invalid" line))
	(every (lambda (dep)
		 (or (filename? dep)
		     (make:form-error "dependency item is not a <filename>" dep)))
	       (makerule-dependent line))))
     spec)))

 (define (make:check-argv argv)
   (or (string? argv)
       (every 
	string?
	(if (vector? argv) (vector->list argv) argv))
       (error "argument is not a string or string vector" argv)))

 (define+source
   (define (make/proc spec argv)
;;     (make:check-spec spec)
;;     (make:check-argv argv)
     (cond
      ((string? argv) (make-file spec (make-simple-filename argv) '() ""))
      ((or (null? argv) (equal? argv '#()))
       (make-file
	spec
	(let loop ((spec spec))
	  (if (null? spec)
	      (raise "found nothing to make")
	      (let ((dflt (makerule-fresh (car spec))))
		(if (pair? dflt) (car dflt) (loop (cdr spec))))))
	'() ""))
      (else (for-each (lambda (f) (make-file spec (make-simple-filename f) '() ""))
		      (if (vector? argv) (vector->list argv) argv))))))


 (define-syntax+source
   (define-syntax make-rule/aux
     (syntax-rules (fresh: exist:)
       ;; This is clearly a case of code, which SHOULD NOT ever be
       ;; written but always generated from some human readable source.
       ;;
       ;; Can't help myself: as much as I like Scheme for it's clarity,
       ;; this as as clear as any average Perl code.  All advantages gone.
       ((_ sv () (target-fresh ...) (target-exist ...) (dependencies ...) command ...)
	(make/add-rule!
	 (list target-fresh ...) (list target-exist ...) (list dependencies ...)
	 ;;'target-fresh 'target-exist 'dependencies
	 (cons (lambda () command) 'command) ...))
       ((_ sv () target-fresh target-exist dependencies command ...)
	(make-rule/aux sv () target-fresh target-exist (dependencies) command ...))
       ((_ sv (fresh: f . m) target-fresh target-exist . rest)
	(make-rule/aux fresh: m (f . target-fresh) target-exist . rest))
       ((_ sv (exist: f . m) target-fresh target-exist . rest)
	(make-rule/aux exist: m target-fresh (f . target-exist) . rest))
       ((_ fresh: (f . m) target-fresh target-exist . rest)
	(make-rule/aux fresh: m (f . target-fresh) target-exist . rest))
       ((_ fresh: predeflst target-fresh target-exist . rest)
	(make-rule/aux fresh: () (predeflst . target-fresh) target-exist . rest))
       ((_ exist: (f . m) target-fresh target-exist . rest)
	(make-rule/aux exist: m target-fresh (f . target-exist) . rest))
       ))

   (define-syntax make-rule
     (syntax-rules ()
       ((_ target deps . more)
	(make-rule/aux fresh: target () () deps . more))))

   (define-syntax target:
     (syntax-rules ()
       ((_ target deps . more)
	(make-rule/aux fresh: target () () deps . more))))
   )

 

 
 (define-syntax+source
   (define-syntax define-file
     (syntax-rules ()
       ((_ name p ...) (define name (filename p ...)))))
   (define-syntax define-source
     (syntax-rules ()
       ((_ name p) (define name (source-file (if (filename? p) p (filename p)))))
       ((_ name p ...) (define name (source-file (filename p ...))))))

   ;; FIXME: should only register file as output, but give the
   ;; "writing..." message only upon use.
   (define-syntax define-result
     (syntax-rules ()
       ((_ name p) (define name (result-file (if (filename? p) p (filename p)))))
       ((_ name p ...) (define name (result-file (filename p ...))))))

   (define-syntax define-dir
     (syntax-rules ()
       ((_ name p ...) (define name (filename (list (filename p #f #f) ...) #f #f)))))
   )
 

 
 ;; Helpers

 (define (identity x) x)
 (define+source
   (define (tostr x)
     (cond
      ((filename? x) (literal-filename x))
      ((string? x) x)
      ((symbol? x) (symbol->string x))
      ((number? x) (number->string x))
      ((not x) x)
      (else (call-with-output-string (lambda (p) (display x p)))))))

 (define (flatmap0 f lst . more)
   (let loop ((fl lst) (ml more))
     (cond
      ((null? fl)
       (if (null? ml) '()
	   (loop (car ml) (cdr ml))))
      ((pair? fl)
       (let ((fl1 (car fl)))
	 (cond
	  ((null? fl1) (loop (cdr fl) ml))
	  ((pair? fl1) (loop fl1 (cons (cdr fl) ml)))
	  (else (cons (f fl1) (loop (cdr fl) ml))))))
      (else (cons (f fl) (loop '() ml))))))

 (define+source

   ;; flatmap/wild is more aggressive, but easily never terminates.
   ;; The result of "f" is passed into the process again if it doesn't
   ;; pass the test "p?".
   (: flatmap/wild ((procedure (*) *) (procedure (*) boolean) * #!rest * --> (list-of *)))
   (define (flatmap/wild f p? lst . more)
     (let loop ((fl lst) (ml more))
       (cond
	((null? fl)
	 (if (null? ml) '()
	     (loop (car ml) (cdr ml))))
	((pair? fl) (loop (car fl) (cons (cdr fl) ml)))
	(else
	 (let ((e (f fl)))
	   (cond
	    ((p? e) (cons e (loop '() ml)))
	    ((pair? e) (loop e ml))
	    (else (loop '() ml))))))))

   (define (flatmap f lst . more)
     (define (p x)
       (not (or (boolean? x) )))
     (flatmap/wild f p lst more))

   )

 (define+source
   (define (run cmd . lst)
     (let ((plain (flatmap tostr cmd lst)))
       (call-with-values
	   (lambda ()
	     (process-wait
	      (let ((pid (process-fork)))
		(cond
		 ((eq? pid 0)
		  (log-progress "run ~s" plain)
		  (process-execute (car plain) (cdr plain))
		  (log-error "Failed to execute ~a" (car plain))
		  (exit 1))
		 (else pid)))))
	 (lambda (pid success res)
	   (if (and success (eqv? res 0))
	       (log-progress "done: ~a" plain)
	       (raise (format "return ~a code: ~a" success res))))))))

 

 )

(import (prefix make make:))
;(define make/proc make:make/proc)
(define make/add-rule! make:make/add-rule!)
(define all-source-files make:all-source-files)
(define all-result-files make:all-result-files)
;;
(define run make:run)

;; FIXME: This is horribly missplaced code.  How do we reliably export
;; to cicken's interpreter?  Do we actually want to use that eval?

(import (prefix srfi-34 s34:))

(define raise s34:raise)
(define with-exception-handler s34:with-exception-handler)
