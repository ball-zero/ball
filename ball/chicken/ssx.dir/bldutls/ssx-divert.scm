(declare
 (unit ssx-divert)
; (uses extras posix files)
 ;; promises
 (strict-types)
 (usual-integrations)
)

(module ssx-divert
  (
   debug
   log-progress
   log-error
   call-with-progress-log
   call-with-error-log
   ;;
   call-with-output-file
   with-output-to-file
   source-file source-files
   result-file
   current-source-files current-result-files
   assert-full-source-preservation!
   ;;
   filename ;; syntax
   make-filename make-simple-filename
   filename?
   filename-dir filename-base filename-ext
   filename=?
   literal-filename
   ;;
   file-exists? file-modification-time
   ;;
   make-list-accumulator
   *source-code* *syntax-code*
   (define+source *source-code*)
   (define-syntax+source *syntax-code*)
   )

  (import (except scheme call-with-output-file with-output-to-file))
  (import (prefix scheme scheme:))
  (import (except chicken file-exists?))
  (import (prefix chicken chicken:))
  (import srfi-34 srfi-35)
  (import extras srfi-1)
  (import (prefix posix posix:))
  (import files)

  ;; Utility Functions

  (define (make-list-accumulator)
    (let* ((head (list #f))
	   (tail head))
      (lambda args
	(if (pair? args)
	    (begin
	      (set-cdr! tail args)
	      (let loop ((args args))
		(if (null? (cdr args))
		    (set! tail args)
		    (loop (cdr args))))))
	(cdr head))))

  ;; Code to be copied into the output.
  (define *source-code* (make-list-accumulator))
  (define *syntax-code* (make-list-accumulator))

  (*source-code*
   '(define (make-list-accumulator)
      (let* ((head (list #f))
	     (tail head))
	(lambda args
	  (if (pair? args)
	      (begin
		(set-cdr! tail args)
		(let loop ((args args))
		  (if (null? (cdr args))
		      (set! tail args)
		      (loop (cdr args))))))
	  (cdr head)))))

  (define-syntax define+source
    (syntax-rules ()
      ((_ def ...) (begin (*source-code* 'def) ... def ...))))

  (define-syntax define-syntax+source
    (syntax-rules ()
      ((_ def ...) (begin (*syntax-code* 'def) ... def ...))))

  ;; Files And Path As Records

  (define+source
    (define-record-type <filename>
      (%make-filename abs dir base ext)
      filename?
      (abs filename-abs)
      (dir filename-dir)
      (base filename-base)
      (ext filename-ext)
      ))

  (define-record-printer <filename>
    (lambda (x p)
      (format p "(dir: ~a fn: ~a ext: ~a)" (filename-dir x) (filename-base x) (filename-ext x))))

  ;; FIXME: Arbitrary wild logic???
  (: make-filename (* * #!rest * --> (struct <filename>)))
  (define+source
    (define (make-filename dir base . ext)
      (define (dirspec->dir dir)
	(cond
	 ((filename? dir) (filename-dir dir))
	 ((symbol? dir) (list (symbol->string dir)))
	 ((pair? dir) (concatenate (map (lambda (x) (or (dirspec->dir x) '())) dir)))
	 ((not dir) #f)
	 (else (list dir))))
      (let* ((p0 (dirspec->dir dir))
	     #;(p (if p0
	     (cond
	     ((filename? base)
	     (if (filename-dir base)
	     (append p0 (filename-dir base))
	     p0))
	     (else p0))
	     (and (filename? base)
	     (filename-dir base))))
	     (abs? (or (and (filename? p0) (filename-abs p0))
		       (and (pair? p0) (let ((p01 (car p0)))
					 (and (filename? p01) (filename-abs p01))))))
	     (b (if (filename? base)
		    (filename-base base)
		    (if (symbol? base) (symbol->string base) base)))
	     (e (if (pair? ext)
		    (let ((ext (car ext)))
		      (cond
		       ((filename? ext) (filename-ext ext))
		       ((symbol? ext) (symbol->string ext))
		       ((or (not ext) (string? ext)) ext)
		       (else (raise (format "make-filename 'ext' invalid: ~a" ext)))))
		    (and (filename? base) (filename-ext base)))))
	(%make-filename abs? p0 b e))))

  (define-syntax+source
    (define-syntax filename
      (syntax-rules ()
	((_ fn) (make-filename #f fn #f))
	((_ () fn . ext) (make-filename #f fn . ext))
	((_ (pth ...) fn . ext) (make-filename (pth ...) fn . ext))
	((_ fn ext) (make-filename #f fn ext))
	((_ pth fn ext) (make-filename pth fn ext))
	)))

  (define+source

    (define (literal-filename x)
      ((if (filename-abs x) make-absolute-pathname make-pathname)
       (filename-dir x) (filename-base x) (filename-ext x)))

    (define (make-simple-filename x)
      (cond
       ((string? x)
	;; NOT using `receive` syntax here.
	(call-with-values
	    (lambda () (decompose-pathname x))
	  (lambda (d n e)
	    (if d
		(call-with-values (lambda () (decompose-directory d))
		  (lambda (drv abs dirs)
		    (%make-filename (or drv abs) dirs n e)))
		(%make-filename #f #f n e)))))
       ((symbol? x) (make-filename #f (symbol->string x) #f))
       (else x)))

  ;;; string->path (or alike) would decompose according to platform NYI

    (define (filename=? a b)
      (assert (filename? a))
      (assert (filename? b))
      (and (equal? (filename-dir a) (filename-dir b))
	   (equal? (filename-base a) (filename-base b))
	   (equal? (filename-ext a) (filename-ext b))))

  ;;;

    (define (file-exists? fn)
      (chicken:file-exists? (literal-filename fn)))

    (define (file-modification-time fn)
      (posix:file-modification-time (literal-filename fn)))
    )

;;; XXX KLUDGE
#;    (define (make-simple-filename x)
      (cond
       ((string? x) (receive
		     (d n e) (decompose-pathname (debug 'Zerlegen x))
		     (%make-filename #f #f n e)))
       ((symbol? x) (make-filename #f (symbol->string x) #f))
       (else x)))
;;; XXX KLUDGE

  ;; ### Debug And Log

  (define+source
    (define log-port current-error-port)

    (define (debug l v) (format (log-port) "D: ~a ~s\n" l v) v)

    (define (log-progress msg . args)
      (apply format (log-port) msg args)
      (newline (log-port)))

    (define (call-with-progress-log proc) (proc (log-port)))

    (define (log-error msg . args)
      (apply format (log-port) msg args)
      (newline (log-port)))

    (define (call-with-error-log proc) (proc (log-port)))
    )


  ;; ### Avoid accidental source overwrites.

  (define+source

    (define (is-fd-spec? fn)  (equal? fn "-"))

    (define current-source-files (make-list-accumulator))

    (define current-result-files (make-list-accumulator))

    (define (assert-source-preservation! dst)
      (assert (filename? dst) dst)
      (if (member dst (current-source-files) filename=?)
	  (error (format "will not overwrite source file \"~a\"" (literal-filename dst)))
	  ;;(log-progress "writing \"~a\"" (literal-filename dst))
	  ))

    (define (assert-full-source-preservation!)
      (for-each assert-source-preservation! (current-result-files)))

    (define (file-spec->filename x)
      (if (is-fd-spec? x) x (make-simple-filename x)))

    (define (source-file x)
      (if (is-fd-spec? x) x
	  (let* ((x (file-spec->filename x))
		 (found (member x (current-source-files) filename=?)))
	    (if found
		(car found)
		(begin
		  (current-source-files x)
		  x)))))

    (define (source-files more)
      (for-each source-file more)
      more)

    (define (result-file f)
      (let* ((f (file-spec->filename f))
	     (found (begin
		      (assert-source-preservation! f)
		      (member f (current-result-files) filename=?))))
	(if found
	    (car found)
	    (begin
	      (current-result-files f)
	      f))))

    (define call-with-output-file
      (let ((next-handler scheme:call-with-output-file))
	(lambda (fn proc)
	  (if (is-fd-spec? fn)
	      (proc (current-output-port))
	      (let ((fn (make-simple-filename fn)))
		(assert-source-preservation! fn)
		(next-handler (literal-filename fn) proc))))))

    (define with-output-to-file
      (let ((with-output-to-file scheme:with-output-to-file))
	(lambda (fn thunk)
	  (if (is-fd-spec? fn)
	      (error "with-output-to-file target special target NYI")
	      (let ((fn (make-simple-filename fn)))
		(assert-source-preservation! fn)
		(with-output-to-file (literal-filename fn) thunk))))))
    );;define+source

)

(import (prefix ssx-divert m:))
(define make-filename m:make-filename)
(define make-simple-filename m:make-simple-filename)
(define filename? m:filename?)
(define literal-filename m:literal-filename)
;
(define source-file m:source-file)
(define result-file m:result-file)
;
(define ssx-save:with-output-to-file m:with-output-to-file)
(define ssx-save:call-with-output-file m:call-with-output-file)
#;(set! display
      (let ((display display))
	(lambda (x . port)
	  (apply display (if (filename? x) (literal-filename x) x) port))))
