(define (append-map f lst)
  (apply append (map f lst)))

;;; "synclo.scm" Syntactic Closures		-*-Hen -*-
;;; Copyright (c) 1989-91 Massachusetts Institute of Technology
;;;
;;; This material was developed by the Scheme project at the
;;; Massachusetts Institute of Technology, Department of Electrical
;;; Engineering and Computer Science.  Permission to copy and modify
;;; this software, to redistribute either the original software or a
;;; modified version, and to use this software for any purpose is
;;; granted, subject to the following restrictions and understandings.
;;;
;;; 1. Any copy made of this software must include this copyright
;;; notice in full.
;;;
;;; 2. Users of this software agree to make their best efforts (a) to
;;; return to the MIT Scheme project any improvements or extensions
;;; that they make, so that these may be included in future releases;
;;; and (b) to inform MIT of noteworthy uses of this software.
;;;
;;; 3. All materials developed as a consequence of the use of this
;;; software shall duly acknowledge such use, in accordance with the
;;; usual standards of acknowledging credit in academic research.
;;;
;;; 4. MIT has made no warranty or representation that the operation
;;; of this software will be error-free, and MIT is under no
;;; obligation to provide any services, by way of maintenance, update,
;;; or otherwise.
;;;
;;; 5. In conjunction with products arising from the use of this
;;; material, there shall be no use of the name of the Massachusetts
;;; Institute of Technology nor of any adaptation thereof in any
;;; advertising, promotional, or sales literature without prior
;;; written consent from MIT in each case.

#|
(declare
  (uses srfi-1)
  (export ill-formed-syntax synclo:expand
	  make-syntactic-closure capture-syntactic-environment
	  identifier? identifier=?
	  scheme-syntactic-environment macroexpand
	  ##sys#macroexpand-1-local
	  ##sys#compiler-toplevel-macroexpand-hook
	  ##sys#interpreter-toplevel-macroexpand-hook) )
|#

;;;; Syntactic Closures
;;; written by Alan Bawden
;;; extensively modified by Chris Hanson

;;; See "Syntactic Closures", by Alan Bawden and Jonathan Rees, in
;;; Proceedings of the 1988 ACM Conference on Lisp and Functional
;;; Programming, page 86.

;;;; Syntax Checking
;;; written by Alan Bawden
;;; modified by Chris Hanson

(define (syntax-check pattern form)
  (if (not (syntax-match? (cdr pattern) (cdr form)))
      (syntax-error "ill-formed special form" form)))

(define (ill-formed-syntax form)
  (syntax-error "ill-formed special form" form))

(define (syntax-match? pattern object)
  (define (check-llist object)
    (let loop ((seen '()) (object object))
      (or (null? object)
	  (if (identifier? object)
	      (not (memq object seen))
	      (and (pair? object)
		   (identifier? (car object))
		   (not (memq (car object) seen))
		   (loop (cons (car object) seen) (cdr object)))))))
  (let ((match-error
	 (lambda ()
	   (impl-error "ill-formed pattern" pattern))))
    (cond ((symbol? pattern)
	   (case pattern
	     ((identifier) (identifier? object))
	     ((datum expression form) #t)
	     ((r4rs-bvl dsssl-bvl) (check-llist object))
;;	     ((dsssl-bvl) (or (##sys#extended-lambda-list? object) (check-llist object)))
	     ;;((mit-bvl) (lambda-list? object))
	     (else (match-error))))
	  ((pair? pattern)
	   (case (car pattern)
	     ((*)
	      (if (pair? (cdr pattern))
		  (let ((head (cadr pattern))
			(tail (cddr pattern)))
		    (let loop ((object object))
		      (or (and (pair? object)
			       (syntax-match? head (car object))
			       (loop (cdr object)))
			  (syntax-match? tail object))))
		  (match-error)))
	     ((+)
	      (if (pair? (cdr pattern))
		  (let ((head (cadr pattern))
			(tail (cddr pattern)))
		    (and (pair? object)
			 (syntax-match? head (car object))
			 (let loop ((object (cdr object)))
			   (or (and (pair? object)
				    (syntax-match? head (car object))
				    (loop (cdr object)))
			       (syntax-match? tail object)))))
		  (match-error)))
	     ((?)
	      (if (pair? (cdr pattern))
		  (or (and (pair? object)
			   (syntax-match? (cadr pattern) (car object))
			   (syntax-match? (cddr pattern) (cdr object)))
		      (syntax-match? (cddr pattern) object))
		  (match-error)))
	     ((quote)
	      (if (and (pair? (cdr pattern))
		       (null? (cddr pattern)))
		  (eqv? (cadr pattern) object)
		  (match-error)))
	     (else
	      (and (pair? object)
		   (syntax-match? (car pattern) (car object))
		   (syntax-match? (cdr pattern) (cdr object))))))
	  (else
	   (eqv? pattern object)))))

;;;; Syntaxer Output Interface

(define impl-error error)

(define *counter* 0)

(define (make-name-generator)
  (let ((make-suffix
	 (lambda ()
	    (string-append "."
			   (number->string (begin
					     (set! *counter* (+ *counter* 1))
					     *counter*))))))
    (lambda (identifier)
      (string->symbol
       (string-append "."
		      (symbol->string (identifier->symbol identifier))
		      (make-suffix))))))

(define (rename-top-level-identifier identifier)
  (if (symbol? identifier)
      identifier
      ((make-name-generator) identifier)))

(define (output/variable name)
  name)

(define (output/literal-unquoted datum)
  datum)

(define (output/literal-quoted datum);was output/constant (inefficient)
  `(quote ,datum))

(define (output/assignment name value)
  `(set! ,name ,value))

(define (output/top-level-definition name value)
  `(define ,name ,value))

(define (output/conditional predicate consequent alternative)
  `(if ,predicate ,consequent ,alternative))

(define (output/sequence expressions)
  (if (null? (cdr expressions))
      (car expressions)
      `(begin ,@expressions)))

(define (output/combination operator operands)
  `(,operator ,@operands))

(define (output/lambda pattern body)
  `(lambda ,pattern ,body))
#|
(define (output/delay expression)
  `(##sys#make-promise (lambda () ,expression)))

(define (output/unassigned)
  `(##core#undefined))

(define (output/unspecific)
  `(##sys#void))

|#
(define (output/delay expression)
  `(delay ,expression))

(define (output/unassigned)
  `undefined)

(define (output/unspecific)
  `(values))


;;;; Classifier
;;;  The classifier maps forms into items.  In addition to locating
;;;  definitions so that they can be properly processed, it also
;;;  identifies keywords and variables, which allows a powerful form
;;;  of syntactic binding to be implemented.

(define (classify/form form environment definition-environment)
  (cond ((identifier? form)
	 (syntactic-environment/lookup environment form))
	((syntactic-closure? form)
	 (let ((form (syntactic-closure/form form))
	       (environment
		(filter-syntactic-environment
		 (syntactic-closure/free-names form)
		 environment
		 (syntactic-closure/environment form))))
	   (classify/form form
			  environment
			  definition-environment)))
	((pair? form)
	 (let ((item
		(classify/subexpression (car form) environment)))
	   (cond ((keyword-item? item)
		  ((keyword-item/classifier item) form
						  environment
						  definition-environment))
		 ((list? (cdr form))
		  (let ((items
			 (classify/subexpressions (cdr form)
						  environment)))
		    (make-expression-item
		     (lambda ()
		       (output/combination
			(compile-item/expression item)
			(map compile-item/expression items)))
		     form)))
		 (else
		  (syntax-error "combination must be a proper list"
				form)))))
	(else
	 (make-expression-item ;don't quote literals evaluating to themselves
	   (if (or (boolean? form) (char? form) (number? form) (string? form))
	       (lambda () (output/literal-unquoted form))
	       (lambda () (output/literal-quoted form))) form))))

(define (classify/subform form environment definition-environment)
  (classify/form form
		 environment
		 definition-environment))

(define (classify/subforms forms environment definition-environment)
  (map (lambda (form)
	 (classify/subform form environment definition-environment))
       forms))

(define (classify/subexpression expression environment)
  (classify/subform expression environment environment))

(define (classify/subexpressions expressions environment)
  (classify/subforms expressions environment environment))

;;;; Compiler
;;;  The compiler maps items into the output language.

(define (compile-item/expression item)
  (let ((illegal
	 (lambda (item name)
	   (let ((decompiled (decompile-item item))) (newline)
	   (error (string-append name " may not be used as an expression")
		  decompiled)))))
    (cond ((variable-item? item)
	   (output/variable (variable-item/name item)))
	  ((expression-item? item)
	   ((expression-item/compiler item)))
	  ((body-item? item)
	   (let ((items (flatten-body-items (body-item/components item))))
	     (if (null? items)
		 (illegal item "empty sequence")
		 (output/sequence (map compile-item/expression items)))))
	  ((definition-item? item)
	   (output/top-level-definition
	    (definition-item/name item)
	    (compile-item/expression (definition-item/value item))))
	  ((keyword-item? item)
	   (illegal item "keyword"))
	  (else
	   (impl-error "unknown item" item)))))

(define (compile/subexpression expression environment)
  (compile-item/expression
   (classify/subexpression expression environment)))

(define (compile/top-level forms environment)
  ;; Top-level syntactic definitions affect all forms that appear
  ;; after them.
  (output/top-level-sequence
   (let forms-loop ((forms forms))
     (if (null? forms)
	 '()
	 (let items-loop
	     ((items
	       (item->list
		(classify/subform (car forms)
				  environment
				  environment))))
	   (cond ((null? items)
		  (forms-loop (cdr forms)))
		 ((definition-item? (car items))
		  (if (not (keyword-item? (definition-item/value (car items))))
		      (cons (output/top-level-definition
			     (definition-item/name (car items))
			     (compile-item/expression
			      (definition-item/value (car items))))
			    (items-loop (cdr items)))
		      ;;
		      (items-loop (cdr items))
		      #;(cons
		       (let* ((thing (car items))
			      (nm (debug 'NM (definition-item/name thing))))
			 `(define-rewriter
			    (,nm x)
			    (compile/top-level (list (cons ',(definition-item/name thing) (cdr x)))
					       scheme-syntactic-environment)))
		       (items-loop (cdr items)))
		      ;;
		      ))
		 (else
		  (cons (compile-item/expression (car items))
			(items-loop (cdr items))))))))))

;;;; De-Compiler
;;;  The de-compiler maps partly-compiled things back to the input language,
;;;  as far as possible.  Used to display more meaningful macro error messages.

(define (decompile-item item)
    (display " ")
    (cond ((variable-item? item) (variable-item/name item))
	  ((expression-item? item)
	   (decompile-item (expression-item/annotation item)))
	  ((body-item? item)
	   (let ((items (flatten-body-items (body-item/components item))))
	     (display "sequence")
	     (if (null? items)
		 "empty sequence"
		 "non-empty sequence")))
	  ((definition-item? item) "definition")
	  ((keyword-item? item)
	   (decompile-item (keyword-item/name item)));in case expression
	  ((syntactic-closure? item); (display "syntactic-closure;")
	   (decompile-item (syntactic-closure/form item)))
	  ((list? item) (display "(")
		(map decompile-item item) (display ")") "see list above")
	  ((string? item) item);explicit name-string for keyword-item
	  ((symbol? item) (display item) item) ;symbol for syntactic-closures
	  ((boolean? item) (display item) item) ;symbol for syntactic-closures
	  (else (write item) (impl-error "unknown item" item))))

;;;; Syntactic Closures

(define-record syntactic-closure environment free-names form)

;(define-record-printer (syntactic-closure x p)
;  (fprintf p "#<syntactic closure ~s>" (syntactic-closure-form x) ) )

(define syntactic-closure/environment syntactic-closure-environment)
(define syntactic-closure/free-names syntactic-closure-free-names)
(define syntactic-closure/form syntactic-closure-form)

(define (make-syntactic-closure-list environment free-names forms)
  (map (lambda (form) (make-syntactic-closure environment free-names form))
       forms))

(define (strip-syntactic-closures object)
  (cond ((syntactic-closure? object)
	 (strip-syntactic-closures (syntactic-closure/form object)))
	((pair? object)
	 (cons (strip-syntactic-closures (car object))
	       (strip-syntactic-closures (cdr object))))
	((vector? object)
	 (let ((length (vector-length object)))
	   (let ((result (make-vector length)))
	     (do ((i 0 (+ i 1)))
		 ((= i length))
	       (vector-set! result i
			    (strip-syntactic-closures (vector-ref object i))))
	     result)))
	(else
	 object)))
;@
(define (identifier? object)
  (or (symbol? object)
      (synthetic-identifier? object)))

(define (synthetic-identifier? object)
  (and (syntactic-closure? object)
       (identifier? (syntactic-closure/form object))))

(define (identifier->symbol identifier)
  (cond ((symbol? identifier)
	 identifier)
	((synthetic-identifier? identifier)
	 (identifier->symbol (syntactic-closure/form identifier)))
	(else
	 (impl-error "not an identifier" identifier))))
;@
(define (identifier=? environment-1 identifier-1 environment-2 identifier-2)
  (let ((item-1 (syntactic-environment/lookup environment-1 identifier-1))
	(item-2 (syntactic-environment/lookup environment-2 identifier-2)))
    (or (eq? item-1 item-2)
	;; This is necessary because an identifier that is not
	;; explicitly bound by an environment is mapped to a variable
	;; item, and the variable items are not cached.  Therefore
	;; two references to the same variable result in two
	;; different variable items.
	(and (variable-item? item-1)
	     (variable-item? item-2)
	     (eq? (variable-item/name item-1)
		  (variable-item/name item-2))))))

;;;; Syntactic Environments

(define-record syntactic-environment
  parent lookup-operation rename-operation define-operation bindings-operation)

(define syntactic-environment/parent syntactic-environment-parent)
(define syntactic-environment/lookup-operation syntactic-environment-lookup-operation)
(define (syntactic-environment/assign! environment name item)
  (let ((binding
	 ((syntactic-environment/lookup-operation environment) name)))
    (if binding
	(set-cdr! binding item)
	(impl-error "can't assign unbound identifier" name))))

(define syntactic-environment/rename-operation syntactic-environment-rename-operation)

(define (syntactic-environment/rename environment name)
  ((syntactic-environment/rename-operation environment) name))

(define syntactic-environment/define!
  (let ((accessor syntactic-environment-define-operation))
    (lambda (environment name item)
      ((accessor environment) name item))))

(define syntactic-environment/bindings
  (let ((accessor syntactic-environment-bindings-operation))
    (lambda (environment)
      ((accessor environment)))))

(define (syntactic-environment/lookup environment name)
  (let ((binding
	 ((syntactic-environment/lookup-operation environment) name)))
    (cond (binding
	   (let ((item (cdr binding)))
	     (if (reserved-name-item? item)
		 (syntax-error "premature reference to reserved name"
			       name)
		 item)))
	  ((symbol? name)
	   (make-variable-item name))
	  ((synthetic-identifier? name)
	   (syntactic-environment/lookup (syntactic-closure/environment name)
					 (syntactic-closure/form name)))
	  (else
	   (impl-error "not an identifier" name)))))

(define root-syntactic-environment
  (make-syntactic-environment
   #f
   (lambda (name)
     name
     #f)
   (lambda (name)
     (rename-top-level-identifier name))
   (lambda (name item)
     (impl-error "can't bind name in root syntactic environment" name item))
   (lambda ()
     '())))

(define null-syntactic-environment
  (make-syntactic-environment
   #f
   (lambda (name)
     (impl-error "can't lookup name in null syntactic environment" name))
   (lambda (name)
     (impl-error "can't rename name in null syntactic environment" name))
   (lambda (name item)
     (impl-error "can't bind name in null syntactic environment" name item))
   (lambda ()
     '())))

(define (top-level-syntactic-environment parent)
  (let ((bound '()))
    (make-syntactic-environment
     parent
     (let ((parent-lookup (syntactic-environment/lookup-operation parent)))
       (lambda (name)
	 (or (assq name bound)
	     (parent-lookup name))))
     (lambda (name)
       (rename-top-level-identifier name))
     (lambda (name item)
       (let ((binding (assq name bound)))
	 (if binding
	     (set-cdr! binding item)
	     (set! bound (cons (cons name item) bound)))))
     (lambda ()
       (map (lambda (pair) (cons (car pair) (cdr pair))) bound)))))

(define (internal-syntactic-environment parent)
  (let ((bound '())
	(free '()))
    (make-syntactic-environment
     parent
     (let ((parent-lookup (syntactic-environment/lookup-operation parent)))
       (lambda (name)
	 (or (assq name bound)
	     (assq name free)
	     (let ((binding (parent-lookup name)))
	       (if binding (set! free (cons binding free)))
	       binding))))
     (make-name-generator)
     (lambda (name item)
       (cond ((assq name bound)
	      =>
	      (lambda (association)
		(if (and (reserved-name-item? (cdr association))
			 (not (reserved-name-item? item)))
		    (set-cdr! association item)
		    (impl-error "can't redefine name; already bound" name))))
	     ((assq name free)
	      (if (reserved-name-item? item)
		  (syntax-error "premature reference to reserved name"
				name)
		  (impl-error "can't define name; already free" name)))
	     (else
	      (set! bound (cons (cons name item) bound)))))
     (lambda ()
       (map (lambda (pair) (cons (car pair) (cdr pair))) bound)))))

(define (filter-syntactic-environment names names-env else-env)
  (if (or (null? names)
	  (eq? names-env else-env))
      else-env
      (let ((make-operation
	     (lambda (get-operation)
	       (let ((names-operation (get-operation names-env))
		     (else-operation (get-operation else-env)))
		 (lambda (name)
		   ((if (memq name names) names-operation else-operation)
		    name))))))
	(make-syntactic-environment
	 else-env
	 (make-operation syntactic-environment/lookup-operation)
	 (make-operation syntactic-environment/rename-operation)
	 (lambda (name item)
	   (impl-error "can't bind name in filtered syntactic environment"
		       name item))
	 (lambda ()
	   (map (lambda (name)
		  (cons name
			(syntactic-environment/lookup names-env name)))
		names))))))

;;;; Items

;;; Reserved name items do not represent any form, but instead are
;;; used to reserve a particular name in a syntactic environment.  If
;;; the classifier refers to a reserved name, a syntax error is
;;; signalled.  This is used in the implementation of LETREC-SYNTAX
;;; to signal a meaningful error when one of the <init>s refers to
;;; one of the names being bound.

(define-record reserved-name-item)

;;; Keyword items represent macro keywords.

(define-record keyword-item classifier name)

(define keyword-item/classifier keyword-item-classifier)
(define keyword-item/name keyword-item-name)

;;; Variable items represent run-time variables.

(define-record variable-item name)

(define variable-item/name variable-item-name)

;;; Expression items represent any kind of expression other than a
;;; run-time variable or a sequence.  The ANNOTATION field is used to
;;; make expression items that can appear in non-expression contexts
;;; (for example, this could be used in the implementation of SETF).

(define-record expression-item compiler annotation)

(define expression-item/compiler expression-item-compiler)
(define expression-item/annotation expression-item-annotation)

;;; Body items represent sequences (e.g. BEGIN).

(define-record body-item components)

(define body-item/components body-item-components)

;;; Definition items represent definitions, whether top-level or
;;; internal, keyword or variable.

(define-record definition-item name value)

(define definition-item/name definition-item-name)
(define definition-item/value definition-item-value)

(define (syntactic-binding-theory environment name item)
  (if (keyword-item? item)
      (begin
	(syntactic-environment/define! environment name item)
	(make-definition-item (rename-top-level-identifier name) item))
      (syntax-error "syntactic binding value must be a keyword or a variable"
		    item)))

(define (variable-binding-theory environment name item)
  ;; If ITEM isn't a valid expression, an error will be signalled by
  ;; COMPILE-ITEM/EXPRESSION later.
  (make-definition-item (bind-variable! environment name) item))

(define (overloaded-binding-theory environment name item)
  (if (keyword-item? item)
      (syntactic-binding-theory environment name item)
      (variable-binding-theory environment name item)))

;;;; Classifiers, Compilers, Expanders

(define (sc-expander->classifier expander keyword-environment)
  (lambda (form environment definition-environment)
    (classify/form (expander form environment)
		   keyword-environment
		   definition-environment)))

(define (er-expander->classifier expander keyword-environment)
  (sc-expander->classifier (er->sc-expander expander) keyword-environment))

(define (er->sc-expander expander)
  (lambda (form environment)
    (capture-syntactic-environment
     (lambda (keyword-environment)
       (make-syntactic-closure
	environment '()
	(expander form
		  (let ((renames '()))
		    (lambda (identifier)
		      (let ((association (assq identifier renames)))
			(if association
			    (cdr association)
			    (let ((rename
				   (make-syntactic-closure
				    keyword-environment
				    '()
				    identifier)))
			      (set! renames
				    (cons (cons identifier rename)
					  renames))
			      rename)))))
		  (lambda (x y)
		    (identifier=? environment x
				  environment y))))))))

(define (classifier->keyword classifier)
  (make-syntactic-closure
   (let ((environment
	  (internal-syntactic-environment null-syntactic-environment)))
     (syntactic-environment/define! environment
				    'keyword
				    (make-keyword-item classifier "c->k"))
     environment)
   '()
   'keyword))

(define (compiler->keyword compiler)
  (classifier->keyword (compiler->classifier compiler)))

(define (classifier->form classifier)
  `(,(classifier->keyword classifier)))

(define (compiler->form compiler)
  (classifier->form (compiler->classifier compiler)))

(define (compiler->classifier compiler)
  (lambda (form environment definition-environment)
    definition-environment		;ignore
    (make-expression-item
     (lambda () (compiler form environment)) form)))

;;;; Macrologies
;;;  A macrology is a procedure that accepts a syntactic environment
;;;  as an argument, producing a new syntactic environment that is an
;;;  extension of the argument.

(define (make-primitive-macrology generate-definitions)
  (lambda (base-environment)
    (let ((environment (top-level-syntactic-environment base-environment)))
      (let ((define-classifier
	      (lambda (keyword classifier)
		(syntactic-environment/define!
		 environment
		 keyword
		 (make-keyword-item classifier keyword)))))
	(generate-definitions
	 define-classifier
	 (lambda (keyword compiler)
	   (define-classifier keyword (compiler->classifier compiler)))))
      environment)))

(define (make-expander-macrology object->classifier generate-definitions)
  (lambda (base-environment)
    (let ((environment (top-level-syntactic-environment base-environment)))
      (generate-definitions
       (lambda (keyword object)
	 (syntactic-environment/define!
	  environment
	  keyword
	  (make-keyword-item (object->classifier object environment) keyword)))
       base-environment)
      environment)))

(define (make-sc-expander-macrology generate-definitions)
  (make-expander-macrology sc-expander->classifier generate-definitions))

(define (make-er-expander-macrology generate-definitions)
  (make-expander-macrology er-expander->classifier generate-definitions))

(define (compose-macrologies . macrologies)
  (lambda (environment)
    (do ((macrologies macrologies (cdr macrologies))
	 (environment environment ((car macrologies) environment)))
	((null? macrologies) environment))))

;;;; Utilities

(define (bind-variable! environment name)
  (let ((rename (syntactic-environment/rename environment name)))
    (syntactic-environment/define! environment
				   name
				   (make-variable-item rename))
    rename))

(define (reserve-names! names environment)
  (let ((item (make-reserved-name-item)))
    (for-each (lambda (name)
		(syntactic-environment/define! environment name item))
	      names)))
;@
(define (capture-syntactic-environment expander)
  (classifier->form
   (lambda (form environment definition-environment)
     form				;ignore
     (classify/form (expander environment)
		    environment
		    definition-environment))))

(define (unspecific-expression)
  (compiler->form
   (lambda (form environment)
     form environment			;ignore
     (output/unspecific))))

(define (unassigned-expression)
  (compiler->form
   (lambda (form environment)
     form environment			;ignore
     (output/unassigned))))

(define (syntax-quote expression)
  `(,(compiler->keyword
      (lambda (form environment)
	environment			;ignore
	(syntax-check '(keyword datum) form)
	(output/literal-quoted (cadr form))))
    ,expression))

(define (flatten-body-items items)
  (append-map item->list items))

(define (item->list item)
  (if (body-item? item)
      (flatten-body-items (body-item/components item))
      (list item)))

(define (output/let names values body)
  (if (null? names)
      body
      (output/combination (output/lambda names body) values)))

(define (output/letrec names values body)
  (if (null? names)
      body
      (output/let
       names
       (map (lambda (name) name (output/unassigned)) names)
       (output/sequence
	(list (if (null? (cdr names))
		  (output/assignment (car names) (car values))
		  (let ((temps (map (make-name-generator) names)))
		    (output/let
		     temps
		     values
		     (output/sequence
		      (map output/assignment names temps)))))
	      body)))))

(define (output/top-level-sequence expressions)
  (if (null? expressions)
      (output/unspecific)
      (output/sequence expressions)))

;;;; R4RS Syntax

(define scheme-syntactic-environment #f)

(define (initialize-scheme-syntactic-environment!)
  (set! scheme-syntactic-environment
	((compose-macrologies
	  (make-core-primitive-macrology)
	  (make-binding-macrology syntactic-binding-theory #f
				  'let-syntax 'letrec-syntax 'define-syntax)
	  (make-binding-macrology variable-binding-theory #t
				  'let 'letrec 'define)
	  (make-r4rs-primitive-macrology)
	  (make-core-expander-macrology)
	  (make-syntax-rules-macrology))
	 root-syntactic-environment)))

;;;; core primitives

(define (make-core-primitive-macrology)
  (make-primitive-macrology
   (lambda (define-classifier define-compiler)

     (define-classifier 'begin
       (lambda (form environment definition-environment)
	 (syntax-check '(keyword * form) form)
	 (make-body-item (classify/subforms (cdr form)
					    environment
					    definition-environment))))

     (define-compiler 'delay
       (lambda (form environment)
	 (syntax-check '(keyword expression) form)
	 (output/delay
	  (compile/subexpression (cadr form)
				 environment))))

     (define-compiler 'if
       (lambda (form environment)
	 (syntax-check '(keyword expression expression ? expression) form)
	 (output/conditional
	  (compile/subexpression (cadr form) environment)
	  (compile/subexpression (caddr form) environment)
	  (if (null? (cdddr form))
	      (output/unspecific)
	      (compile/subexpression (cadddr form)
				     environment)))))

     (define-compiler 'quote
       (lambda (form environment)
	 environment			;ignore
	 (syntax-check '(keyword datum) form)
	 (output/literal-quoted (strip-syntactic-closures (cadr form))))))))

;;;; bindings

(define (make-binding-macrology binding-theory retain-bindings?
				let-keyword letrec-keyword define-keyword)
  (make-primitive-macrology
   (lambda (define-classifier define-compiler)

     (let ((pattern/let-like
	    '(keyword (* (identifier expression)) + form))
	   (compile/let-like
	    (lambda (form environment body-environment output/let)
              ;; This must be a LET* so that the applications of
              ;; BINDING-THEORY, which have side effects, will happen
              ;; before the classification and compilation of the body.
	      (let* ((definitions
                      (map (lambda (binding)
                             (binding-theory body-environment
                                             (car binding)
                                             (classify/subexpression
                                              (cadr binding)
                                              environment)))
                           (cadr form)))
                     (body
                      (compile-item/expression
                       (classify/body (cddr form)
                                      body-environment))))
                (if retain-bindings?
                    (output/let (map definition-item/name definitions)
                                (map (lambda (definition)
                                       (compile-item/expression
                                        (definition-item/value definition)))
                                     definitions)
                                body)
                    (output/let '() '() body))))))

       (define-compiler let-keyword
	 (lambda (form environment)
	   (syntax-check pattern/let-like form)
	   (compile/let-like form
			     environment
			     (internal-syntactic-environment environment)
			     output/let)))

       (define-compiler letrec-keyword
	 (lambda (form environment)
	   (syntax-check pattern/let-like form)
	   (let ((environment (internal-syntactic-environment environment)))
	     (reserve-names! (map car (cadr form)) environment)
	     (compile/let-like form
			       environment
			       environment
			       output/letrec)))))

     (define-classifier define-keyword
       (lambda (form environment definition-environment)
	 (syntax-check '(keyword identifier expression) form)
	 (reserve-names! (list (cadr form)) definition-environment)
	 (binding-theory definition-environment
			 (cadr form)
			 (classify/subexpression (caddr form)
						 environment)))))))

;;;; bodies

(define (classify/body forms environment)
  (let ((environment (internal-syntactic-environment environment)))
    (let forms-loop
	((forms forms)
	 (definitions '()))
      (if (null? forms)
	  (syntax-error "no expressions in body")
	  (let items-loop
	      ((items
		(item->list
		 (classify/subform (car forms)
				   environment
				   environment)))
	       (definitions definitions))
	    (cond ((null? items)
		   (forms-loop (cdr forms)
			       definitions))
		  ((definition-item? (car items))
		   (items-loop (cdr items)
			       (cons (car items) definitions)))
		  (else
		   (let ((body
			  (make-body-item
			   (append items
				   (flatten-body-items
				    (classify/subforms
				     (cdr forms)
				     environment
				     environment))))))
		     (make-expression-item
		      (lambda ()
			(output/letrec
			 (map definition-item/name definitions)
			 (map (lambda (definition)
				(compile-item/expression
				 (definition-item/value definition)))
			      definitions)
			 (compile-item/expression body))) forms)))))))))

;;;; r4rs primitives

(define (make-r4rs-primitive-macrology)
  (make-primitive-macrology
   (lambda (define-classifier define-compiler)

     (define (transformer-keyword expander->classifier)
       (lambda (form environment definition-environment)
	 definition-environment		;ignore
	 (syntax-check '(keyword expression) form)
	 (let ((item
		(classify/subexpression (cadr form)
					scheme-syntactic-environment)))
	   (let ((transformer (base:eval (compile-item/expression item))))
	     (if (procedure? transformer)
		 (make-keyword-item
		  (expander->classifier transformer environment) item)
		 (syntax-error "transformer not a procedure"
			       transformer))))))

     (define-classifier 'transformer
       ;; "syntactic closures" transformer
       (transformer-keyword sc-expander->classifier))

     (define-classifier 'er-transformer
       ;; "explicit renaming" transformer
       (transformer-keyword er-expander->classifier))

     (define-compiler 'lambda
       (lambda (form environment)
	 (syntax-check '(keyword dsssl-bvl + form) form)
	 (let ((environment (internal-syntactic-environment environment)))
	   ;; force order -- bind names before classifying body.
	   (call-with-values
	       (lambda ()
		 ;; (if (##sys#extended-lambda-list? (cadr form))
		 ;;     (##sys#expand-extended-lambda-list (cadr form) (cddr form) ##sys#syntax-error-hook) 
		 ;;     (values (cadr form) (cddr form)) )
		 (values (cadr form) (cddr form))
		 )
	     (lambda (llist body)
	       (let ((bvl-description
		      (let ((rename
			     (lambda (identifier)
			       (bind-variable! environment identifier))))
			(let loop ((bvl llist))
			  (cond ((null? bvl)
				 '())
				((pair? bvl)
				 (cons (rename (car bvl)) (loop (cdr bvl))))
				(else
				 (rename bvl)))))))
		 (output/lambda bvl-description
				(compile-item/expression
				 (classify/body body environment)))))))))

     (define-compiler 'set!
       (lambda (form environment)
	 (syntax-check '(keyword form expression) form)
	 (output/assignment
	  (let loop
	      ((form (cadr form))
	       (environment environment))
	    (cond ((identifier? form)
		   (let ((item
			  (syntactic-environment/lookup environment form)))
		     (if (variable-item? item)
			 (variable-item/name item)
			 (error "target of assignment not a variable"
				       form))))
		  ((syntactic-closure? form)
		   (let ((form (syntactic-closure/form form))
			 (environment
			  (filter-syntactic-environment
			   (syntactic-closure/free-names form)
			   environment
			   (syntactic-closure/environment form))))
		     (loop form
			   environment)))
		  (else
		   (error "target of assignment not an identifier"
				 form))))
	  (compile/subexpression (caddr form)
				 environment))))

     ;; end make-r4rs-primitive-macrology
     )))

;;;; core expanders

(define (make-core-expander-macrology)
  (make-er-expander-macrology
   (lambda (define-expander base-environment)

     (let ((keyword (make-syntactic-closure base-environment '() 'define)))
       (define-expander 'define
	 (lambda (form rename compare)
	   compare			;ignore
	   (cond ((syntax-match? '((identifier . dsssl-bvl) + form) (cdr form))
		  `(,keyword ,(caadr form)
			     (,(rename 'lambda) ,(cdadr form) ,@(cddr form))) )
		 ((syntax-match? '(identifier) (cdr form))
		  `(,keyword ,(cadr form) ,(output/unspecific)) )
		 (else `(,keyword ,@(cdr form)))))) )

     (let ((keyword (make-syntactic-closure base-environment '() 'let)))
       (define-expander 'let
	 (lambda (form rename compare)
	   compare			;ignore
	   (if (syntax-match? '(identifier (* (identifier expression)) + form)
			      (cdr form))
	       (let ((name (cadr form))
		     (bindings (caddr form)))
		 `((,(rename 'letrec)
		    ((,name (,(rename 'lambda) ,(map car bindings) ,@(cdddr form))))
		    ,name)
		   ,@(map cadr bindings)))
	       `(,keyword ,@(cdr form))))))

     (define-expander 'let*
       (lambda (form rename compare)
	 compare			;ignore
	 (if (syntax-match? '((* (identifier expression)) + form) (cdr form))
	     (let ((bindings (cadr form))
		   (body (cddr form))
		   (keyword (rename 'let)))
	       (if (null? bindings)
		   `(,keyword ,bindings ,@body)
		   (let loop ((bindings bindings))
		     (if (null? (cdr bindings))
			 `(,keyword ,bindings ,@body)
			 `(,keyword (,(car bindings))
				    ,(loop (cdr bindings)))))))
	     (ill-formed-syntax form))))

     (define-expander 'and
       (lambda (form rename compare)
	 compare			;ignore
	 (if (syntax-match? '(* expression) (cdr form))
	     (let ((operands (cdr form)))
	       (if (null? operands)
		   `#t
		   (let ((if-keyword (rename 'if)))
		     (let loop ((operands operands))
		       (if (null? (cdr operands))
			   (car operands)
			   `(,if-keyword ,(car operands)
					 ,(loop (cdr operands))
					 #f))))))
	     (ill-formed-syntax form))))

     (define-expander 'or
       (lambda (form rename compare)
	 compare			;ignore
	 (if (syntax-match? '(* expression) (cdr form))
	     (let ((operands (cdr form)))
	       (if (null? operands)
		   `#f
		   (let ((let-keyword (rename 'let))
			 (if-keyword (rename 'if))
			 (temp (rename 'temp)))
		     (let loop ((operands operands))
		       (if (null? (cdr operands))
			   (car operands)
			   `(,let-keyword ((,temp ,(car operands)))
					  (,if-keyword ,temp
						       ,temp
						       ,(loop (cdr operands)))))))))
	     (ill-formed-syntax form))))

     (define-expander 'case
       (lambda (form rename compare)
	 (if (syntax-match? '(expression + (datum + expression)) (cdr form))
	     (letrec
		 ((process-clause
		   (lambda (clause rest)
		     (cond ((null? (car clause))
			    (process-rest rest))
			   ((and (identifier? (car clause))
				 (compare (rename 'else) (car clause))
				 (null? rest))
			    `(,(rename 'begin) ,@(cdr clause)))
			   ((list? (car clause))
			    `(,(rename 'if) (,(rename 'memv) ,(rename 'temp)
							     ',(car clause))
					    (,(rename 'begin) ,@(cdr clause))
					    ,(process-rest rest)))
			   (else
			    (syntax-error "ill-formed clause" clause)))))
		  (process-rest
		   (lambda (rest)
		     (if (null? rest)
			 (unspecific-expression)
			 (process-clause (car rest) (cdr rest))))))
	       `(,(rename 'let) ((,(rename 'temp) ,(cadr form)))
				,(process-clause (caddr form) (cdddr form))))
	     (ill-formed-syntax form))))

     (define-expander 'cond
       (lambda (form rename compare)
	 (letrec
	     ((process-clause
	       (lambda (clause rest)
		 (cond
		  ((or (not (list? clause))
		       (null? clause))
		   (syntax-error "ill-formed clause" clause))
		  ((and (identifier? (car clause))
			(compare (rename 'else) (car clause)))
		   (cond
		    ((or (null? (cdr clause))
			 (and (identifier? (cadr clause))
			      (compare (rename '=>) (cadr clause))))
		     (syntax-error "ill-formed else clause" clause))
		    ((not (null? rest))
		     (syntax-error "misplaced else clause" clause))
		    (else
		     `(,(rename 'begin) ,@(cdr clause)))))
		  ((null? (cdr clause))
		   `(,(rename 'or) ,(car clause) ,(process-rest rest)))
		  ((and (identifier? (cadr clause))
			(compare (rename '=>) (cadr clause)))
		   (if (and (pair? (cddr clause))
			    (null? (cdddr clause)))
		       `(,(rename 'let)
			 ((,(rename 'temp) ,(car clause)))
			 (,(rename 'if) ,(rename 'temp)
					(,(caddr clause) ,(rename 'temp))
					,(process-rest rest)))
		       (syntax-error "ill-formed => clause" clause)))
		  (else
		   `(,(rename 'if) ,(car clause)
				   (,(rename 'begin) ,@(cdr clause))
				   ,(process-rest rest))))))
	      (process-rest
	       (lambda (rest)
		 (if (null? rest)
		     (unspecific-expression)
		     (process-clause (car rest) (cdr rest))))))
	   (let ((clauses (cdr form)))
	     (if (null? clauses)
		 (syntax-error "no clauses" form)
		 (process-clause (car clauses) (cdr clauses)))))))

     (define-expander 'do
       (lambda (form rename compare)
	 compare			;ignore
	 (if (syntax-match? '((* (identifier expression ? expression))
			      (+ expression)
			      * form)
			    (cdr form))
	     (let ((bindings (cadr form)))
	       `(,(rename 'letrec)
		 ((,(rename 'do-loop)
		   (,(rename 'lambda)
		    ,(map car bindings)
		    (,(rename 'if) ,(caaddr form)
				   ,(if (null? (cdaddr form))
					(unspecific-expression)
					`(,(rename 'begin) ,@(cdaddr form)))
				   (,(rename 'begin)
				    ,@(cdddr form)
				    (,(rename 'do-loop)
				     ,@(map (lambda (binding)
					      (if (null? (cddr binding))
						  (car binding)
						  (caddr binding)))
					    bindings)))))))
		 (,(rename 'do-loop) ,@(map cadr bindings))))
	     (ill-formed-syntax form))))

     (define-expander 'quasiquote
       (lambda (form rename compare)
	 (define (descend-quasiquote x level return)
	   (cond ((pair? x) (descend-quasiquote-pair x level return))
		 ((vector? x) (descend-quasiquote-vector x level return))
		 (else (return 'quote x))))
	 (define (descend-quasiquote-pair x level return)
	   (cond ((not (and (pair? x)
			    (identifier? (car x))
			    (pair? (cdr x))
			    (null? (cddr x))))
		  (descend-quasiquote-pair* x level return))
		 ((compare (rename 'quasiquote) (car x))
		  (descend-quasiquote-pair* x (+ level 1) return))
		 ((compare (rename 'unquote) (car x))
		  (if (zero? level)
		      (return 'unquote (cadr x))
		      (descend-quasiquote-pair* x (- level 1) return)))
		 ((compare (rename 'unquote-splicing) (car x))
		  (if (zero? level)
		      (return 'unquote-splicing (cadr x))
		      (descend-quasiquote-pair* x (- level 1) return)))
		 (else
		  (descend-quasiquote-pair* x level return))))
	 (define (descend-quasiquote-pair* x level return)
	   (descend-quasiquote
	    (car x) level
	    (lambda (car-mode car-arg)
	      (descend-quasiquote
	       (cdr x) level
	       (lambda (cdr-mode cdr-arg)
		 (cond ((and (eq? car-mode 'quote) (eq? cdr-mode 'quote))
			(return 'quote x))
		       ((eq? car-mode 'unquote-splicing)
			(if (and (eq? cdr-mode 'quote) (null? cdr-arg))
			    (return 'unquote car-arg)
			    (return 'append
				    (list car-arg
					  (finalize-quasiquote cdr-mode
							       cdr-arg)))))
		       ((and (eq? cdr-mode 'quote) (list? cdr-arg))
			(return 'list
				(cons (finalize-quasiquote car-mode car-arg)
				      (map (lambda (element)
					     (finalize-quasiquote 'quote
								  element))
					   cdr-arg))))
		       ((eq? cdr-mode 'list)
			(return 'list
				(cons (finalize-quasiquote car-mode car-arg)
				      cdr-arg)))
		       (else
			(return
			 'cons
			 (list (finalize-quasiquote car-mode car-arg)
			       (finalize-quasiquote cdr-mode cdr-arg))))))))))
	 (define (descend-quasiquote-vector x level return)
	   (descend-quasiquote
	    (vector->list x) level
	    (lambda (mode arg)
	      (case mode
		((quote) (return 'quote x))
		((list) (return 'vector arg))
		(else
		 (return 'list->vector
			 (list (finalize-quasiquote mode arg))))))))
	 (define (finalize-quasiquote mode arg)
	   (case mode
	     ((quote) `(,(rename 'quote) ,arg))
	     ((unquote) arg)
	     ((unquote-splicing) (syntax-error ",@ in illegal context" arg))
	     (else `(,(rename mode) ,@arg))))
	 (if (syntax-match? '(expression) (cdr form))
	     (descend-quasiquote (cadr form) 0 finalize-quasiquote)
	     (ill-formed-syntax form))))

;;; end make-core-expander-macrology
     )))

;;;; Rule-based Syntactic Expanders

;;; See "Syntactic Extensions in the Programming Language Lisp", by
;;; Eugene Kohlbecker, Ph.D. dissertation, Indiana University, 1986.
;;; See also "Macros That Work", by William Clinger and Jonathan Rees
;;; (reference? POPL?).  This implementation is derived from an
;;; implementation by Kent Dybvig, and includes some ideas from
;;; another implementation by Jonathan Rees.

;;; The expansion of SYNTAX-RULES references the following keywords:
;;;   ER-TRANSFORMER LAMBDA IF BEGIN SET! QUOTE
;;; and the following procedures:
;;;   CAR CDR NULL? PAIR? EQUAL? MAP LIST CONS APPEND
;;;   ILL-FORMED-SYNTAX
;;; it also uses the anonymous keyword SYNTAX-QUOTE.

;;; For testing.
;;;(define (run-sr form)
;;;  (expand/syntax-rules form (lambda (x) x) eq?))

(define (make-syntax-rules-macrology)
  (make-er-expander-macrology
   (lambda (define-classifier base-environment)
     base-environment			;ignore
     (define-classifier 'syntax-rules expand/syntax-rules))))

(define (expand/syntax-rules form rename compare)
  (if (syntax-match? '((* identifier) + ((identifier . datum) expression))
		     (cdr form))
      (let ((keywords (cadr form))
	    (clauses (cddr form)))
	(if (let loop ((keywords keywords))
	      (and (pair? keywords)
		   (or (memq (car keywords) (cdr keywords))
		       (loop (cdr keywords)))))
	    (syntax-error "keywords list contains duplicates" keywords)
	    (let ((r-form (rename 'form))
		  (r-rename (rename 'rename))
		  (r-compare (rename 'compare)))
	      `(,(rename 'er-transformer)
		(,(rename 'lambda)
		 (,r-form ,r-rename ,r-compare)
		 ,(let loop ((clauses clauses))
		    (if (null? clauses)
			`(,(rename 'ill-formed-syntax) ,r-form)
			(let ((pattern (caar clauses)))
			  (let ((sids
				 (parse-pattern rename compare keywords
						pattern r-form)))
			    `(,(rename 'if)
			      ,(generate-match rename compare keywords
					       r-rename r-compare
					       pattern r-form)
			      ,(generate-output rename compare r-rename
						sids (cadar clauses)
						syntax-error)
			      ,(loop (cdr clauses))))))))))))
      (ill-formed-syntax form)))

(define (parse-pattern rename compare keywords pattern expression)
  (let loop
      ((pattern pattern)
       (expression expression)
       (sids '())
       (control #f))
    (cond ((identifier? pattern)
	   (if (memq pattern keywords)
	       sids
	       (cons (make-sid pattern expression control) sids)))
	  ((and (or (zero-or-more? pattern rename compare)
		    (at-least-one? pattern rename compare))
		(null? (cddr pattern)))
	   (let ((variable ((make-name-generator) 'control)))
	     (loop (car pattern)
		   variable
		   sids
		   (make-sid variable expression control))))
	  ((pair? pattern)
	   (loop (car pattern)
		 `(,(rename 'car) ,expression)
		 (loop (cdr pattern)
		       `(,(rename 'cdr) ,expression)
		       sids
		       control)
		 control))
          ((vector? pattern)
           (loop (vector->list pattern)
		 `(,(rename 'vector->list) ,expression)
		 sids
		 control))
	  (else sids))))

(define (generate-match rename compare keywords r-rename r-compare
			pattern expression)
  (letrec
      ((loop
	(lambda (pattern expression)
	  (cond ((identifier? pattern)
		 (if (memq pattern keywords)
		     (let ((temp (rename 'temp)))
		       `((,(rename 'lambda)
			  (,temp)
			  (,(rename 'if)
			   (,(rename 'identifier?) ,temp)
			   (,r-compare ,temp
				       (,r-rename ,(syntax-quote pattern)))
			   #f))
			 ,expression))
		     `#t))
		((and (zero-or-more? pattern rename compare)
		      (null? (cddr pattern)))
		 (do-list (car pattern) expression))
		((and (at-least-one? pattern rename compare)
		      (null? (cddr pattern)))
		 `(,(rename 'if) (,(rename 'null?) ,expression)
				 #f
				 ,(do-list (car pattern) expression)))
		((pair? pattern)
		 (let ((generate-pair
			(lambda (expression)
			  (conjunction
			   `(,(rename 'pair?) ,expression)
			   (conjunction
			    (loop (car pattern)
				  `(,(rename 'car) ,expression))
			    (loop (cdr pattern)
				  `(,(rename 'cdr) ,expression)))))))
		   (if (identifier? expression)
		       (generate-pair expression)
		       (let ((temp (rename 'temp)))
			 `((,(rename 'lambda) (,temp) ,(generate-pair temp))
			   ,expression)))))
		((null? pattern)
		 `(,(rename 'null?) ,expression))
                ((vector? pattern)
                 (letrec
                     ((len (vector-length pattern))
                      (generate-vector
                       (lambda (len res)
                         (if (negative? len)
                           res
                           (generate-vector
                            (- len 1)
                            (conjunction (loop (vector-ref pattern len)
                                               `(,(rename 'vector-ref)
                                                 ,expression
                                                 ,len))
                                         res))))))
                   (if (zero? len)
                     `(,(rename 'equal?) ,expression '#())
                     (conjunction
                      `(,(rename 'vector?) ,expression)
                      (if (eq? '... (vector-ref pattern (- len 1)))
                        (conjunction
                         `(,(rename '>=)
                           (,(rename 'vector-length) ,expression)
                           ,(- len 2))
                         (conjunction
                          (generate-vector (- len 2) #t)
                          (do-vec (vector-ref pattern (- len 1))
                                  expression
                                  (- len 1))))
                        (conjunction
                         `(,(rename '>=)
                           (,(rename 'vector-length) ,expression)
                           ,(- len 2))
                         (generate-vector (- len 1) #t)))))))
		(else
		 `(,(rename 'equal?) ,expression
				     (,(rename 'quote) ,pattern))))))
       (do-list
	(lambda (pattern expression)
	  (let ((r-loop (rename 'loop))
		(r-l (rename 'l))
		(r-lambda (rename 'lambda)))
	    `(((,r-lambda
		(,r-loop)
		(,(rename 'begin)
		 (,(rename 'set!)
		  ,r-loop
		  (,r-lambda
		   (,r-l)
		   (,(rename 'if)
		    (,(rename 'null?) ,r-l)
		    #t
		    ,(conjunction
		      `(,(rename 'pair?) ,r-l)
		      (conjunction (loop pattern `(,(rename 'car) ,r-l))
				   `(,r-loop (,(rename 'cdr) ,r-l)))))))
		 ,r-loop))
	       #f)
	      ,expression))))
       (do-vec
	(lambda (pattern expression start)
	  (let ((r-loop (rename 'loop))
		(r-vec (rename 'vec))
                (r-len (rename 'len))
                (r-i (rename 'i))
		(r-lambda (rename 'lambda)))
	    `((,r-lambda
               (,r-vec)
               ((,r-lambda
                 (,r-len)
                 ((,r-lambda
                   (,r-loop)
                   (,(rename 'begin)
                    (,(rename 'set!)
                     ,r-loop
                     (,r-lambda
                      (,r-i)
                      (,(rename 'if)
                       (,(rename '>=) ,r-i ,r-len)
                       #t
                       ,(conjunction
                         (loop pattern `(,(rename 'vector-ref) ,r-vec))
                         `(,r-loop (,(rename '+) ,r-i 1))))))
                    (,r-loop ,start)))
                  #f))
                (,(rename 'vector-length) ,r-vec)))
	      ,expression))))
       (conjunction
	(lambda (predicate consequent)
	  (cond ((eq? predicate #t) consequent)
		((eq? consequent #t) predicate)
		(else `(,(rename 'if) ,predicate ,consequent #f))))))
    (loop pattern expression)))

(define (generate-output rename compare r-rename sids template syntax-error)
  (let loop ((template template) (ellipses '()))
    (cond ((identifier? template)
	   (let ((sid
		  (let loop ((sids sids))
		    (and (not (null? sids))
			 (if (eq? (sid-name (car sids)) template)
			     (car sids)
			     (loop (cdr sids)))))))
	     (if sid
		 (begin
		   (add-control! sid ellipses syntax-error)
		   (sid-expression sid))
		 `(,r-rename ,(syntax-quote template)))))
	  ((or (zero-or-more? template rename compare)
	       (at-least-one? template rename compare))
	   (optimized-append rename compare
			     (let ((ellipsis (make-ellipsis '())))
			       (generate-ellipsis rename
						  ellipsis
						  (loop (car template)
							(cons ellipsis
							      ellipses))))
			     (loop (cddr template) ellipses)))
	  ((pair? template)
	   (optimized-cons rename compare
			   (loop (car template) ellipses)
			   (loop (cdr template) ellipses)))
	  (else
	   `(,(rename 'quote) ,template)))))

(define (add-control! sid ellipses syntax-error)
  (let loop ((sid sid) (ellipses ellipses))
    (let ((control (sid-control sid)))
      (cond (control
	     (if (null? ellipses)
		 (syntax-error "missing ellipsis in expansion" #f)
		 (let ((sids (ellipsis-sids (car ellipses))))
		   (cond ((not (memq control sids))
			  (set-ellipsis-sids! (car ellipses)
					      (cons control sids)))
			 ((not (eq? control (car sids)))
			  (syntax-error "illegal control/ellipsis combination"
					control sids)))))
	     (loop control (cdr ellipses)))
	    ((not (null? ellipses))
	     (syntax-error "extra ellipsis in expansion" #f))))))

(define (generate-ellipsis rename ellipsis body)
  (let ((sids (ellipsis-sids ellipsis)))
    (let ((name (sid-name (car sids)))
	  (expression (sid-expression (car sids))))
      (cond ((and (null? (cdr sids))
		  (eq? body name))
	     expression)
	    ((and (null? (cdr sids))
		  (pair? body)
		  (pair? (cdr body))
		  (eq? (cadr body) name)
		  (null? (cddr body)))
	     `(,(rename 'map) ,(car body) ,expression))
	    (else
	     `(,(rename 'map) (,(rename 'lambda) ,(map sid-name sids) ,body)
			      ,@(map sid-expression sids)))))))

(define (zero-or-more? pattern rename compare)
  (and (pair? pattern)
       (pair? (cdr pattern))
       (identifier? (cadr pattern))
       (compare (cadr pattern) (rename '...))))

(define (at-least-one? pattern rename compare)
;;;  (and (pair? pattern)
;;;       (pair? (cdr pattern))
;;;       (identifier? (cadr pattern))
;;;       (compare (cadr pattern) (rename '+)))
  pattern rename compare		;ignore
  #f)

(define (optimized-cons rename compare a d)
  (cond ((and (pair? d)
	      (compare (car d) (rename 'quote))
	      (pair? (cdr d))
	      (null? (cadr d))
	      (null? (cddr d)))
	 `(,(rename 'list) ,a))
	((and (pair? d)
	      (compare (car d) (rename 'list))
	      (list? (cdr d)))
	 `(,(car d) ,a ,@(cdr d)))
	(else
	 `(,(rename 'cons) ,a ,d))))

(define (optimized-append rename compare x y)
  (if (and (pair? y)
	   (compare (car y) (rename 'quote))
	   (pair? (cdr y))
	   (null? (cadr y))
	   (null? (cddr y)))
      x
      `(,(rename 'append) ,x ,y)))

(define-record sid name expression control output-expression)

(define make-sid
  (let ((make-sid make-sid))
    (lambda (name expression control)
      (make-sid name expression control (values)) ) ) )

(define set-sid-output-expression! sid-output-expression-set!)

(define-record ellipsis sids)

(define set-ellipsis-sids! ellipsis-sids-set!)

;;; OK, time to build the databases.
(initialize-scheme-syntactic-environment!)

;;@ MACRO:EXPAND is for you to use.  It takes an R4RS expression, macro-expands
;;; it, and returns the result of the macro expansion.
(define (synclo:expand expression)
  (set! *counter* 0)
  (compile/top-level (list expression) scheme-syntactic-environment))

;; A hastily written wrapper to make the API look like alexpander.
;;
;; It just doen't work (yet).
(define (null-mstore) (vector scheme-syntactic-environment))

#;(define (expand-top-level-forms! expressions mstore)
  (let ((current-state scheme-syntactic-environment))
    (let ((result
	   (begin
	     (set! *counter* 0)
	     (set! scheme-syntactic-environment (vector-ref mstore 0))
	     (compile/top-level expressions scheme-syntactic-environment))))
      (vector-set! mstore 0 scheme-syntactic-environment)
      (set! scheme-syntactic-environment current-state)
      result)))

(define (expand-top-level-forms! expressions mstore)
  (set! *counter* 0)
  (compile/top-level expressions scheme-syntactic-environment))

;(set! ##sys#compiler-toplevel-macroexpand-hook synclo:expand)
;(set! ##sys#interpreter-toplevel-macroexpand-hook synclo:expand)
;(set! macroexpand (lambda (exp . me) (synclo:expand exp)))
;(set! ##sys#macroexpand-1-local (lambda (x me) x))

;(register-feature! 'syntax-rules 'hygienic-macros 'syntactic-closures)

;(define (base:eval x) ((##sys#eval-handler) x))
(define (base:eval x) (eval x))

(define (debug l v)
  (format (current-error-port) "HOHO: ~a ~s\n" l v) v)

#|
(with-module repl
  (with-module compiler
    (set-value!
	   (& expr->thunk)
	   (let ((e->t expr->thunk))
	     (lambda (expr envt)
	       (e->t (let ((v (synclo:expand expr)))
		       (pp v)
		       v)
		     envt))))))
|#

