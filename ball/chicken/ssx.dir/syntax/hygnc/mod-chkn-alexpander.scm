;; (C) 2008, 2010 Joerg F. Wittenberger see http://www.askemos.org

;; Try to be nice and mix+match with chickens native error handling.
;; Could probably be much mor efficient if we did not do so.

(declare
 (unit alexpander)
 (not standard-bindings with-exception-handler)
 ;; optional
 (disable-interrupts)
 ;; promises
 (strict-types)
 (usual-integrations)
 )

(module
 alexpander
*
#; (
  alexpander-repl
  expand-program
  expand-top-level-forms!
  expand-top-level-forms
  ;;
  null-mstore
  null-store null-loc-n
  )

 (import scheme (only chicken include make-parameter))
 (import (only extras pretty-print))

 (import (only chicken : define-type))
 (import (only chicken define-record-type))

 (import (only chicken define-record-printer))

 (import srfi-34)

 (define (error err . more)
   (if (null? more) (raise err) (raise (cons err more))))


(import (only extras format) (only chicken current-error-port))
(define (dbg l f v)
  (format (current-error-port) "D ~a: ~a ~a\n" l (if f (f v) "") v)
  v)


(cond-expand
 (v1
  (define-record-type <sids-box>
    (list->svector elements)
    svector?
    (elements svector->list))

  (define-record-type <renamed-sid>
    (make-sid name id location)
    renamed-sid?
    (name renamed-sid-name)
    (id renamed-sid-id)
    (location renamed-sid-location))

(define-record-printer (<sids-box> x out)
  (format out "#(svec ~a  )" (svector->list x)) )

(define-record-printer (<renamed-sid> x out)
  (format out "#(<renamed-sid> ~a ~a ~a)"
	  (renamed-sid-name x)
	  (renamed-sid-id x)
	  (renamed-sid-location x)
	  ) )

  (define (sid-name sid) (if (symbol? sid) sid (renamed-sid-name sid)))
  (define (sid-id sid)   (if (symbol? sid) sid (renamed-sid-id sid)))
  (define (sid-location sid) (if (symbol? sid) sid (renamed-sid-location sid)))

  (define-type :sids: (struct <sids-box>))
  (define-type :renamed-sid: (struct <renamed-sid>))

;; Map-vecs does a deep map of x, replacing any vector, svector or
;; renamed-sic v with (f v).  We assume that f never returns #f.  If a
;; subpart contains no vectors, we don't waste space copying it.
;; (Yes, this is grossly premature optimization.)
#;(define (map-vecs f x)
  ;; mv2 returns #f if there are no vectors in x.
  (define (mv2 x)
    (if (or
	 (svector? x)
	 (renamed-sid? x)
	 (vector? x)			;JFW: unused case now?
	 )
	(f x)
	(and (pair? x)
	     (let ((a (car x)) (b (cdr x)))
	       (let ((a-mapped (mv2 a)))
		 (if a-mapped
		     (cons a-mapped (mv b))
		     (let ((b-mapped (mv2 b)))
		       (and b-mapped (cons a b-mapped)))))))))
  (define (mv x) (or (mv2 x) x))
  (mv x))

;; The "unoptimized" version.
;;
;;JFW: Since the mapping itself is done anyway (thus no savings in GC)
;;will the conserved space actually outweight the computational
;;overhead?

(define (map-vecs f x)
     (cond ((vector? x) (f x))		; does this ever happend?
	   ((svector? x) (f x))
	   ((renamed-sid? x) (f x))
           ((pair? x) (cons (map-vecs f (car x)) (map-vecs f (cdr x))))
           (else x)))

;;JFW: wrap-vecs/unwrap-vecs folded into a single definition each to
;; avoid unintentional use of their parts.
(define wrap-vecs
  (letrec
      ((wrap-vec (lambda (v) (list->svector (wrap-vecs (vector->list v)))))
       (wrap-vecs (lambda (input) (map-vecs wrap-vec input))))
    wrap-vecs))

(define unwrap-vecs
  (letrec
      ((unwrap-vec
	(lambda (v-sexp)
	  (cond
	   ((svector? v-sexp)
	    (list->vector (unwrap-vecs (svector->list v-sexp))))
	   ((renamed-sid? v-sexp)
	    (sid-name v-sexp))
	   (else (vector-ref v-sexp 0))	; JFW: seems to never kick in
	   )))
       (unwrap-vecs (lambda (sexp) (map-vecs unwrap-vec sexp))))
    unwrap-vecs))

  )
 (else
  (define-type :sids: (vector list))
  (define-type :renamed-sid: (or (vector symbol fixnum) (vector symbol fixnum symbol)))
  ))

(define-type :sid: (or symbol fixnum :renamed-sid:))
(: svector? (* --> boolean : :sids:))
(: list->svector ((list-of *) --> :sids:))
(: svector->list (:sids: --> (list-of *)))

(: make-sid (symbol fixnum symbol --> :renamed-sid:))
(: renamed-sid? (* --> boolean : :renamed-sid:))
(: sid-name (:sid: --> symbol))
(: sid-id (:sid: --> fixnum))
(: sid-location (:sid: --> symbol))

;; internal types
(define-type :loc-n: fixnum)
(define-type :location: (or :loc-n: symbol))

(define-type :buildin: (list symbol symbol))

(define-type :syntax-rule: (pair * pair)) ; questionable

;; symbols are rare, overwhelming fixnums.
(define-type :env-binding-set: (list-of (pair :sid: fixnum)))
(define-type :store-binding-set: (list-of (pair (or symbol fixnum) *)))
(: make-env-bindings-set (--> :env-binding-set:))
(: make-store-bindings-set (--> :store-binding-set:))

(define-type :mstore: (pair :store-binding-set: fixnum))


;(: lookup-sid (:sid: :env-binding-set: --> :sid:))
;(: lookup-location (:sid: :store-binding-set: --> :sid:))

; (include "../../mechanism/syntax/alexpander.scm")
 (include "syntax/hygnc/alexpander.scm")

;; EXPERIMENTAL: strip them down.
;(define (wrap-vecs x) x)
;(define (unwrap-vecs x) x)


 )
