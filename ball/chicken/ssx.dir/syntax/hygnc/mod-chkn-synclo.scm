;; (C) 2008, 2010 Joerg F. Wittenberger see http://www.askemos.org

;; Try to be nice and mix+match with chickens native error handling.
;; Could probably be much mor efficient if we did not do so.

(declare
 (unit synclo)
 (not standard-bindings with-exception-handler)
 ;; optional
 (disable-interrupts)
 ;; promises
 (strict-types)
 (usual-integrations)
 )

(module
 synclo
 (
  expand-top-level-forms!
  ;;
  null-mstore
  ;;
  ;; crazy stuff going into the toplevel interpreter (for now)
  identifier? identifier=?
  )

 (import scheme (only chicken include error make-parameter :))
 (import (only chicken define-record syntax-error current-error-port))
 (import (only extras pretty-print format))

 (include "syntax/hygnc/synclo/syntactic-closures.scm")

 )

(import (prefix synclo synclo:))
(define identifier? synclo:identifier?)
(define identifier=? synclo:identifier=?)
