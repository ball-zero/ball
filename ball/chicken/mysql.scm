(declare
 (unit mysql)
 (usual-integrations)

 (export
  mysql-connect mysql-close mysql-with-tupels mysql-value mysql-field
  
 )

 (foreign-declare #<<EOF
#include <mysql/mysql.h>

EOF
))

(define-foreign-type mysql-connection (pointer "MYSQL"))

(define mysql-error
  (foreign-lambda c-string "mysql_error" (pointer "MYSQL")))

(define (mysql-connect db host user pass)
  (let ((conn ((foreign-lambda (c-pointer "MYSQL")
                               "mysql_init" (c-pointer "MYSQL")) #f)))
    ((foreign-lambda bool "mysql_real_connect"
                     (c-pointer "MYSQL")
                     c-string
                     c-string
                     c-string
                     c-string
                     integer
                     c-string
                     integer)
     conn host user pass db
     0 ; port
     "" ; unix-socket
     0 ; clientflag
     )
    conn))

(define (mysql-close db)
  ((foreign-lambda void "mysql_close" (c-pointer "MYSQL")) db))

(define (mysql-with-tupels db query proc)
  (if (zero? ((foreign-lambda integer "mysql_real_query"
                              (c-pointer "MYSQL")
                              c-string
                              unsigned-integer)
              db query (string-length query)))
      (let ((result ((foreign-lambda (c-pointer "MYSQL_RES")
                                     "mysql_store_result" (c-pointer "MYSQL"))
                     db)))
        (if result
            (let ((retval
                   (proc result
                         ((foreign-lambda integer "mysql_num_rows"
                                          (c-pointer "MYSQL_RES")))
                         ((foreign-lambda integer "mysql_num_fields"
                                          (c-pointer "MYSQL_RES"))))))
              ((foreign-lambda void "mysql_free_result" (c-pointer "MYSQL_RES"))
               result)
              retval)
             (error (mysql-error db))))))


(define (mysql-value result row field)
  (if (or (< row 0)
          (>= row ((foreign-lambda integer "mysql_num_rows"
                                   (c-pointer "MYSQL_RES"))
                   result)))
      (error "illegal raw number")
      (begin
        ((foreign-lambda void "mysql_data_seek" (c-pointer "MYSQL_RES") integer)
         result row)
        (let ((data ((foreign-lambda (c-pointer "MYSQL_ROW") "mysql_fetch_row"
                                     (c-pointer "MYSQL_RES"))
                     result)))
          (if data
              ((foreign-lambda* c-string
                                (((c-pointer "MYSQL_ROW") d) (integer i))
                                "d[i];")
               data)
              (error "mysql-value-error"))))))

(define (mysql-field result field)
  (if (or (< field 0)
          (>= field ((foreign-lambda integer "mysql_num_fields"
                                     (c-pointer "MYSQL_RES"))
                     result)))
      (error "illegal field number")
      ((foreign-lambda*
        c-string (((c-pointer "MYSQL_RES") result) (integer field))
        "MYSQL_FIELD *f = mysql_fetch_field_direct(result, field);"
        "return(f->name);") result field)))
