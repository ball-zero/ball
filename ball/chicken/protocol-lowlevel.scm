;; (C) 2002, 2003, 2008 J�rg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit protocol-lowlevel)
 ;;
 (usual-integrations)
;;NO (disable-interrupts)  ; no reasons at all
 )

(module
 protocol-lowlevel
 (
  http-eval-respond-timeout
  eval-server
  eval-request-server
  make-tunnel ssl-tunnel
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 condition? with-exception-handler)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:) (except srfi-18 raise)
	 srfi-19 srfi-34 srfi-35 srfi-110 srfi-69
	 shrdprmtr
	 atomic parallel libmagic pcre regex util sslsocket timeout
	 (prefix dns dns-)
	 tree notation aggregate place-common
	 protocol-common protocol-connpool http-client protocol-clntclls
	 extras data-structures ports)

(define-inline (loghttps fmt . rest)
  (if https-client-verbose (apply logerr fmt rest)))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "typedefs.scm")
(include "../mechanism/protocol/lowlevel.scm")

)
