(declare
 (unit sslsocket)
 ;; optional
 (disable-interrupts)
 ;; promises
 ;; BEWARE: (strict-types) are violated here.
 (fixnum-arithmetic)
 (usual-integrations)
#; (no-bound-checks)
 (no-procedure-checks-for-usual-bindings)
 (bound-to-procedure
  string-append string-ref
  write-string flush-output current-error-port
  create-pipe make-internal-pipe process-fork current-process-id duplicate-fileno
  )
 (foreign-declare #<<EOF
#include <errno.h>
#ifdef _WIN32
/* FIXME: This is likely wrong sometimes. */
# define HAVE_WINSOCK2_H
# define HAVE_WS2TCPIP_H
# if (defined(HAVE_WINSOCK2_H) && defined(HAVE_WS2TCPIP_H))
#  include <winsock2.h>
#  include <ws2tcpip.h>
# else
#  include <winsock.h>
# endif
# ifndef POLLIN
#  define USE_SELECT_SCHED
# endif
/* Beware: winsock2.h must come BEFORE windows.h */
# define socklen_t       int
static WSADATA wsa;
# define fcntl(a, b, c)  0
# ifndef EWOULDBLOCK
#  define EWOULDBLOCK     0
# endif
# ifndef EINPROGRESS
#  define EINPROGRESS     0
# endif
# ifndef EAGAIN
#  define EAGAIN          0
# endif
# define typecorrect_getsockopt(socket, level, optname, optval, optlen)	\
    getsockopt(socket, level, optname, (char *)optval, optlen)
#else
# include <fcntl.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <sys/time.h>
# include <netinet/in.h>
# include <unistd.h>
# include <netdb.h>
# include <signal.h>
# define INVALID_SOCKET  -1
# define closesocket     close
# define typecorrect_getsockopt getsockopt
#endif

#ifndef SD_RECEIVE
# define SD_RECEIVE      0
# define SD_SEND         1
#endif

#ifdef ECOS
#include <sys/sockio.h>
#endif


static C_char addr_buffer[ 46 ];
static int io_needs_restart()
{
  switch(errno) {
   case EAGAIN:
   case EINTR:
    return 1;
   default: return 0;
 }
}

EOF
))

(define ##net#startup
  (foreign-lambda* bool () #<<EOF
#ifdef _WIN32
     C_return(WSAStartup(MAKEWORD(1, 1), &wsa) == 0);
#else
     signal(SIGPIPE, SIG_IGN);
     C_return(1);
#endif
EOF
) )

(unless (##net#startup)
  (##sys#signal-hook #:network-error "cannot initialize Winsock") )

(module
 ;; This module has only a purpose for debugging.  It should be
 ;; removed from the production system.  Traffic shaping should be
 ;; done in the firewall.  This horrible code introduces an artificial
 ;; delay for peers assumed to behave badly and logs the incedent.
 sslsocket-connlimit
 (make-peer-pool
  ssl-peers-set!
  add-peer! release-peer! ssl-connlimit-set! ssl-connlimit ssl-connections-fold)
 (import scheme chicken atomic util llrb-tree)

 (define *ssl-connlimit* 40)
 (define (ssl-connlimit) *ssl-connlimit*)
 (define (ssl-connlimit-set! val)
   (if (and (fixnum? val) (positive? val)) (set! *ssl-connlimit* val)))

 (define (make-peer-pool) (string-make-table))

 (define (add-peer! *peers* peer)
   (let ((s (string-table-ref
	     *peers* peer
	     (lambda ()
	       (let ((limit (make-semaphore peer *ssl-connlimit*)))
		 (string-table-set! *peers* peer limit)
		 limit)))))
     ;; BEWARE: this requires disable-interrupts!
     (and (fx> (semaphore-value s) 0)
	  (begin (semaphore-wait-by! s 1) #t))))

 (define (release-peer! *peers* peer)
   (and-let*
    ((e (string-table-ref/default *peers* peer #f)))
    (semaphore-signal-by! e 1)
    (if (eqv? (semaphore-value e) *ssl-connlimit*)
	(string-table-delete! *peers* peer))))

 (define *ssl-peers* (make-peer-pool))

 (define (ssl-peers-set! x) (set! *ssl-peers* x))

 (define (ssl-connections-fold proc init)
   (string-table-fold proc init *ssl-peers*))

 ) (import (prefix sslsocket-connlimit cnl:))

(define ssl-connlimit cnl:ssl-connlimit)
(define ssl-connlimit-set! cnl:ssl-connlimit-set!)
(define ssl-connections-fold cnl:ssl-connections-fold)

(define ##dbg#connlimitsignal (lambda (peer) #t))
(define (dbg-connlimitsignal-set! proc) (set! ##dbg#connlimitsignal proc))

(module
 sslsocket
 (
  ip-address/port? ipv4-address? ipv4-address/port? ipv6-address? ipv6-address/port?
  make-sslmgr
  ssl-close ssl-certificate debug-ssl
  ssl-keep-alive-timeout
  ssl-input-port ssl-output-port
  askemos:run-ssl-server askemos:open-ssl-client
  ssl-verify-callback-set!
  $ca-slave
  ;;
  tcp-listen tcp-connect tcp-close
  ;;
  askemos:run-tcp-server askemos:run-tcp-client
  )

(import scheme foreign
	(except chicken add1 sub1 with-exception-handler condition? errno)
	srfi-13 (except srfi-18 raise)
	srfi-34 atomic mesh-cert util ports extras)
(import (only posix process-execute current-process-id _exit))
(import (only srfi-106 unix-socket-pair*))

(import (only lolevel allocate))

(import sslsocket-connlimit)

(include "typedefs.scm")

;; TBD: This should be completely rewritten:
;;
;; 1. The whole "framing_mode" is inherited from RScheme, not required
;; and just adds complexity.  Should use a dedicated file descriptor
;; instead.
;;
;; 2. Better have a separate implementation for GNU TLS instead of
;; mixing.  GNU TLS is no longer fully supported by this code anyway.

(cond-expand
 (never-fork
  (foreign-declare "
#define getpid() 0
#define AS_PTHREAD
"))
 (else))

(cond-expand
 ((or never-fork single-binary)
  (foreign-declare #<<EOF
#include <pthread.h>
#define AS_EMBEDDED 1
#define USE_OPENSSL
#include "../unix/ssl/server.c"
#define MAXARGC 32    /* Maximum arg count supported for sslmgr */

struct ssl_mgr_args {
  int argc;               /* Argument count */
  char *argv[MAXARGC+1];  /* Argument vector */
  char *argf;             /* field with all arguments */
  pthread_t thread;       /* TBD remove from declaration if no pthreads are used. */
  struct ssl_mgr_state state;
};

static struct ssl_mgr_args *make_mgr_args() {
   struct ssl_mgr_args *r = C_malloc(sizeof(struct ssl_mgr_args));
   if(r) {
     C_memset(r, 0, sizeof(struct ssl_mgr_args));
     setup_mgr_state(&r->state);
   }
   return r;
}

static void ssl_mgr_cleanup_args(void *arg)
{
  struct ssl_mgr_args *ap=arg;
  C_free(ap->argf);
  ap->argf=NULL;
  C_free(ap);
}

EOF
)

  (define-foreign-type mgr-args-t (c-pointer (struct "ssl_mgr_args")))

  (define (make-mgr-args)
   (or ((foreign-lambda mgr-args-t "make_mgr_args"))
       (error "memory allocation for sslmgr failed")))
  (define mgr-args-free (foreign-lambda void "ssl_mgr_cleanup_args" mgr-args-t))

  (define (set-mgr-args! state lst)
     (let ((field (do ((n 0 (add1 n)) (lst lst (cdr lst))
                       (l 0 (+ l 1 (string-length (car lst)))))
                      ((null? lst) (allocate l))
                    (if (> n 32) (error "too many arguments for sslmgr")))))
       ((foreign-lambda*
         void ((mgr-args-t s) (nonnull-c-pointer f))
         "s->argf = f;") state field)
       (do ((n 0 (add1 n)) (lst lst (cdr lst))
            (offset 0 (+ offset 1 (string-length (car lst)))))
           ((null? lst))
         ((foreign-lambda*
           void ((mgr-args-t s) (int n) (int offset) (nonnull-c-string a) (int l))
           "char *to = s->argf + offset;"
           "C_strncpy(to, a, l+1);"
           "s->argc = n+1;"
           "s->argv[n] = to;")
          state n offset (car lst) (string-length (car lst))))))
 ) (else ;; neither single-binary or never-fork
(foreign-declare "
#ifdef _WIN32
 typedef uint32_t u32;
/**
 * int inet_pton(int af, const char *src, void *dst);
 *
 * https://github.com/dubcanada/inet_pton/blob/master/src/inet_pton.c
 *
 * Compatible with inet_pton just replace inet_pton with xp_inet_pton
 * and you are good to go. Uses WSAStringToAddressA instead of
 * inet_pton, compatible with Windows 2000 +
 */
static int inet_pton(int family, const char *src, void *dst)
{
    int rc;
    struct sockaddr_storage addr;
    int addr_len = sizeof(addr);

    addr.ss_family = family;

    rc = WSAStringToAddressA((char *) src, family, NULL,
        (struct sockaddr*) &addr, &addr_len);
    if (rc != 0) {
        return -1;
    }

    if (family == AF_INET) {
        memcpy(dst, &((struct sockaddr_in *) &addr)->sin_addr,
            sizeof(struct in_addr));
    } else if (family == AF_INET6) {
        memcpy(dst, &((struct sockaddr_in6 *)&addr)->sin6_addr,
            sizeof(struct in6_addr));
    }

    return 1;
}
#else
 typedef u_int32_t u32;
 #include <arpa/inet.h>
#endif
")))

(foreign-declare #<<EOF
static int is_ip6_w_port(char *x) {
 struct sockaddr_in6 addr; char *rb=NULL;
 if(*x=='[') {
   ++x;
   rb=strrchr(x, ']');
   if(rb==NULL) return 0;
   else {if(rb[1]!=':') return 0; else *rb='\0';}
 }
 return inet_pton(AF_INET6, x, &addr);
}
EOF
) ;;

(define (ipv4-address? x)
  (cond
   ((string? x)
    ;; deprecated: used of c-string
    ((foreign-lambda* bool ((nonnull-c-string x)) "struct sockaddr_in addr; C_return(inet_pton(AF_INET, x, &addr));") x))
   (else #f)))

(define (ipv4-address/port? x)
  (cond
   ((string? x)
    ((foreign-lambda*
      bool ((nonnull-c-string x)) ;; deprecated: used of c-string
      "struct sockaddr_in addr;"
      "char *colon = strrchr(x, ':'); if(colon) *colon='\\0';"
      "C_return(inet_pton(AF_INET, x, &addr));")
     x))
   (else #f)))

(define (ipv6-address? x)
  (cond
   ((string? x)
    ;; deprecated: used of c-string
    ((foreign-lambda*
      bool ((nonnull-c-string x)) ;; deprecated: used of c-string
      "struct sockaddr_in6 addr; C_return(inet_pton(AF_INET6, x, &addr));")
     x)
    ((foreign-lambda bool "is_ip6_w_port" nonnull-c-string) x))
   (else #f)))

(define (ipv6-address/port? x)
  (cond
   ((string? x)
    ((foreign-lambda bool "is_ip6_w_port" nonnull-c-string) x))
   (else #f)))

(define (ip-address/port? x) (or (ipv6-address/port? x) (ipv4-address/port? x)))

(cond-expand
 (never-fork

  (define-foreign-type pthread c-pointer)

  (foreign-declare #<<EOF

static void ssl_mgr_args_set_initial(struct ssl_mgr_args *arg, int in, int out, int status)
{
  arg->state.input_fd = in;
  arg->state.output_fd = out;
  arg->state.status_fd = status;
}

static void ssl_mgr_cleanup_state(void *arg)
{
  struct ssl_mgr_args *ap=arg;
  cleanup_mgr_state(&ap->state);
}

static void* ssl_mgr_thread(void *arg)
{
  struct ssl_mgr_args *ap=arg;
  pthread_cleanup_push(ssl_mgr_cleanup_args, ap);
  pthread_cleanup_push(ssl_mgr_cleanup_state, ap);
  ssl_mgr_main(&ap->state, ap->argc, ap->argv);
  pthread_cleanup_pop(1);
  pthread_cleanup_pop(1);
  return NULL;
}

static int pthread_ssl(struct ssl_mgr_args *arg)
{
  int res = 0;
  sigset_t set, orig;
  sigfillset(&set);
  res = pthread_sigmask(SIG_BLOCK, &set, &orig);
  if(res) return res;
  res = pthread_create(&arg->thread, NULL, ssl_mgr_thread, arg);
  pthread_sigmask(SIG_SETMASK, &orig, NULL);
  if( res != 0 ) {
    perror("pthread_create");
    ssl_mgr_cleanup_args(arg);
    return res;
  }
  return pthread_detach(arg->thread);
}


EOF
  )


  (define mgr-args-set-fds! (foreign-lambda void "ssl_mgr_args_set_initial" mgr-args-t int int int))
  (define (ssl-mgr-main in out status args)
    (let ((state (make-mgr-args)))
      (mgr-args-set-fds! state in out status)
      (set-mgr-args! state args)
      (let ((err ((foreign-lambda int "pthread_ssl" mgr-args-t) state)))
        (if (= err 0)
            (make-pthread-from-thread-id
             ((foreign-lambda* pthread ((mgr-args-t p)) "C_return(p->thread);" ) state))
            err))))

  ;; end cond-expand "never-fork"
  )
 (single-binary

  (define (ssl-mgr-main in out status args)
    (let ((state (make-mgr-args)))
      ((foreign-lambda* void ((mgr-args-t p)) "setup_mgr_state012(&p->state);") state)
      ;; (mgr-args-set-fds! state in out status)
      (set-mgr-args! state args)
      ;; This is run in a subprocess, we still rely on the OS to free arguments etc.
      (_exit ((foreign-lambda*
               int ((mgr-args-t p))
               "C_return(ssl_mgr_main(&p->state, p->argc, p->argv));")
              state))))
 ;; end of cond-expand "single-binary"
 )
 (else))

(: network-error (symbol string #!rest -> . *))
(define (network-error tag message . rest)
  (apply os-error #:network-error tag message rest))

(define-foreign-variable errno int "errno")
(define strerror (foreign-lambda c-string "strerror" int))

(define-foreign-variable _af_inet int "AF_INET")
(define-foreign-variable _af_inet6 int "AF_INET6")
(define-foreign-variable _sock_stream int "SOCK_STREAM")
(define-foreign-variable _sock_dgram int "SOCK_DGRAM")
(define-foreign-variable _sockaddr_in_size int "sizeof(struct sockaddr_in)")
(define-foreign-variable _sockaddr_in6_size int "sizeof(struct sockaddr_in6)")
(define-foreign-variable _addrinfo_size int "sizeof(struct addrinfo)")
(define-foreign-variable _ewouldblock int "EWOULDBLOCK")
(define-foreign-variable _eagain int "EAGAIN")
(define-foreign-variable _eintr int "EINTR")
(define-foreign-variable _einprogress int "EINPROGRESS")
(define-foreign-variable _ealready int "EALREADY")
(define-foreign-variable _ebadf int "EBADF")
(define-foreign-variable _econnrefused int "ECONNREFUSED")

(define-foreign-variable _invalid_socket int "INVALID_SOCKET")

(define-foreign-variable _sd_receive int "SD_RECEIVE")
(define-foreign-variable _sd_send int "SD_SEND")
(define-foreign-variable _invalid_socket int "INVALID_SOCKET")

(define-constant default-backlog 100)

(define make-nonblocking
  (foreign-lambda* bool ((int fd))
    "int val = fcntl(fd, F_GETFL, 0);"
    "if(val == -1) C_return(0);"
    "C_return(fcntl(fd, F_SETFL, val | O_NONBLOCK) != -1);") )
(define accept (foreign-lambda int "accept" int c-pointer c-pointer))
(define listen (foreign-lambda int "listen" int int))
(define bind (foreign-lambda int "bind" int scheme-pointer int))
(define connect (foreign-lambda int "connect" int scheme-pointer int))
(define socket (foreign-lambda int "socket" int int int))
(define shutdown (foreign-lambda int "shutdown" int int))

;; FIXME: On WINDOWS this may not work!
;;(define closesocket (foreign-lambda int "closesocket" int))
(define closesocket file-close!)


(define gai_strerror (foreign-lambda c-string "gai_strerror" int))


(define getaddrinfo_hostaddr
  (foreign-lambda*
   int ((scheme-pointer saddr) (c-string host))
   #<<EOF
   struct addrinfo hints;
   struct addrinfo *result, *rp;
   struct sockaddr_in *our_addr = (struct sockaddr_in *)saddr;
   int s;

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
   hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
   hints.ai_flags = 0;
   hints.ai_protocol = 0;          /* Any protocol */

   s = getaddrinfo(host, NULL, &hints, &result);

   if( s != 0 ) C_return(s);
   if( result ) {
     *our_addr = *(struct sockaddr_in *)result->ai_addr;
   }
   freeaddrinfo(result);
   C_return(0);

EOF
   ))

(define (##test#addrinfo host)
  (let* ((addr (make-string/uninit _sockaddr_in_size))
	 (cc (getaddrinfo_hostaddr addr host)))
    (case cc
      ((0) addr)
      (else (raise cc)))))

(define (##test#try)
  (set! gethostaddr (lambda (saddr host port) (getaddrinfo_hostaddr saddr host))))

(define gethostaddr
  (foreign-lambda* bool ((scheme-pointer saddr) (c-string host) (unsigned-short port))
    "struct hostent *he = gethostbyname(host);"
    "struct sockaddr_in *addr = (struct sockaddr_in *)saddr;"
    "if(he == NULL) C_return(0);"
    "memset(addr, 0, sizeof(struct sockaddr_in));"
    "addr->sin_family = AF_INET;"
    "addr->sin_port = htons((short)port);"
    "addr->sin_addr = *((struct in_addr *)he->h_addr_list[0]);"
    "C_return(1);") )
(define getpeername
  (foreign-lambda* c-string ((int s))
    "struct sockaddr_in sa;"
    "unsigned char *ptr;"
    "unsigned int len = sizeof(struct sockaddr_in);"
    "if(getpeername(s, (struct sockaddr *)&sa, ((unsigned int *)&len)) != 0) return(NULL);"
    "ptr = (unsigned char *)&sa.sin_addr;"
    "sprintf(addr_buffer, \"%d.%d.%d.%d\", ptr[ 0 ], ptr[ 1 ], ptr[ 2 ], ptr[ 3 ]);"
    "return(addr_buffer);") )
(define getpeername6
  (foreign-lambda* c-string ((int s))
    "struct sockaddr_in6 sa;"
    "unsigned char *ptr;"
    "unsigned int len = sizeof(struct sockaddr_in6);"
    "if(getpeername(s, (struct sockaddr *)&sa, ((unsigned int *)&len)) != 0) return(NULL);"
    "ptr = (unsigned char *)&sa.sin6_addr;"
    "if(getnameinfo((struct sockaddr *)&sa, len, addr_buffer, sizeof(addr_buffer), 0, 0, NI_NUMERICHOST)) return(NULL);"
    "return(addr_buffer);") )
(define fresh-addr
  (foreign-lambda* void ((scheme-pointer saddr) (unsigned-short port))
    "struct sockaddr_in *addr = (struct sockaddr_in *)saddr;"
    "memset(addr, 0, sizeof(struct sockaddr_in));"
    "addr->sin_family = AF_INET;"
    "addr->sin_port = htons(port);"
    "addr->sin_addr.s_addr = htonl(INADDR_ANY);") )
(define fresh-addr6
  (foreign-lambda* void ((scheme-pointer saddr) (unsigned-short port))
    "struct sockaddr_in6 *addr = (struct sockaddr_in6 *)saddr;"
    "memset(addr, 0, sizeof(struct sockaddr_in6));"
    "addr->sin6_family = AF_INET6;"
    "addr->sin6_port = htons(port);"
    "addr->sin6_addr = in6addr_any;") )
(define get-socket-error
  (foreign-lambda* int ((int socket))
    "int err, optlen;"
    "optlen = sizeof(err);"
    "if (typecorrect_getsockopt(socket, SOL_SOCKET, SO_ERROR, &err, (socklen_t *)&optlen) == -1)"
    "  C_return(-1);"
    "C_return(err);"))

(define ##debug#ipv4-only #f)

(define-inline (host-ipv6? host)
  ;; FIXME, should also accept IPv6 addresses for host.
  (and (boolean? host) #;host (not ##debug#ipv4-only)))

(: bind-socket (fixnum fixnum (or boolean string) -> fixnum string))
(define (bind-socket port style host)
  (if (or (fx< port 0) (fx> port 65535))
      (domain-error 'bind-socket "invalid port number" port) )
  (define ipv6 (host-ipv6? host))
  (let ((s (socket (if ipv6 _af_inet6 _af_inet) style 0)))
    (when (eq? _invalid_socket s)
	  (error (string-append "cannot create socket" (strerror errno))) )
    (when (and ipv6
               (eq? -1 ((foreign-lambda* int ((int socket))
                                         "int no = 0;
                      C_return(setsockopt(socket, IPPROTO_IPV6, IPV6_V6ONLY, (const char *)&no, sizeof(int)));")
                        s) ))
      (closesocket s)
      (network-error 'tcp-listen "error while setting up socket to use both ipv4 and ipv6" s (strerror errno)) )
    ;; PLT makes this an optional arg to tcp-listen. Should we as well?
    (when (eq? -1 ((foreign-lambda* int ((int socket))
		     "int yes = 1;
                      C_return(setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&yes, sizeof(int)));")
		   s) )
      (let ((err errno))
        (closesocket s)
        (network-error 'tcp-listen "error while setting up socket" s)) )
    (let* ((s_size (if ipv6 _sockaddr_in6_size _sockaddr_in_size))
           (addr (make-string/uninit s_size)))
      (if (string? host)
	  (or (gethostaddr addr host port)
	      (begin
                (closesocket s)
                (network-error 'tcp-listen "getting listener host IP failed" host port)) )
	  (if ipv6 (fresh-addr6 addr port) (fresh-addr addr port)) )
      (let ((b (bind s addr s_size)))
	(if (eq? -1 b)
	    (begin
              (closesocket s)
              (network-error 'tcp-listen "cannot bind to socket" s port)) )
	(values s addr) ) ) ) )

(: tcp-listen (fixnum #!optional fixnum string -> (struct tcp-listener)))
(define tcp-listen-tag '(tcp listen))
(define (tcp-listen port . more)
  (let-optionals more ((w default-backlog) (host #f))
    (receive
     (s addr) (bind-socket port _sock_stream host)
     (##sys#check-exact w)
     (let ((l (listen s w)))
       (if (eq? -1 l)
	   (begin
             (closesocket s)
             (network-error 'tcp-listen "cannot listen on socket" s port)) )
       (set-open-fd! s tcp-listen-tag)
       (##sys#make-structure 'tcp-listener s) ) ) ) )

(define (tcp-close tcpl)
  (##sys#check-structure tcpl 'tcp-listener)
  (let ((s (##sys#slot tcpl 1)))
    (if (fx= -1 (closesocket s))
	(network-error 'tcp-close "cannot close TCP socket" tcpl) ) ) )

(define (shutdown-read fd) (shutdown fd _sd_receive))
(define (shutdown-write fd) (shutdown fd _sd_send))

(define tcp-connect-tag '(tcp client))
(: tcp-connect (string fixnum -> input-port output-port))
(define (tcp-connect host port)
  (##sys#check-string host)
  (##sys#check-exact port)
  (let ((addr (let ((addr (make-string/uninit _sockaddr_in_size)))
		(or (gethostaddr addr host port)
		    (network-error 'tcp-connect "cannot find host address" host) )
		addr))
	(s (socket _af_inet _sock_stream 0)) )
    (define (fail msg err)
      (closesocket s)
      (network-error 'tcp-connect msg host port) )
    (if (eq? _invalid_socket s)
	(network-error 'tcp-connect "cannot create socket" host port) )
    (or (make-nonblocking s)
	(fail "fcntl() failed" errno) )
    (set-open-fd! s tcp-connect-tag)
    (let loop ()
      (if (eq? -1 (connect s addr _sockaddr_in_size))
	  (cond ((eq? errno _einprogress)
		 (thread-wait-for-i/o! s #:output))
		((eq? errno _eintr)
		 (##sys#dispatch-interrupt loop))
		(else (fail "cannot connect to socket" errno) ) )))
    (let ((err (get-socket-error s)))
      (cond
       ((fx= err 0)
	(net-io-ports
	 s (getpeername s)
	 shutdown-read shutdown-write closesocket))
       ((fx= err -1) (fail "getsockopt() failed" err))
       ((fx= err _econnrefused) (fail "connection refused" err))
       (else (fail "getsockopt error" err))))) )

(define (tcp-accept tcpl)
  (##sys#check-structure tcpl 'tcp-listener)
  (let ((fd (##sys#slot tcpl 1)))
    (let loop ()
      (let ((fd (accept fd #f #f)))
	(cond ((not (eq? -1 fd))
	       (set-open-fd! fd '(tcp server))
	       (net-io-ports
		fd (getpeername fd)
		shutdown-read shutdown-write closesocket))
	      ((eq? errno _eintr)
	       (##sys#dispatch-interrupt
		(lambda () (thread-wait-for-i/o! fd #:input) (loop))))
	      (else
	       (network-error 'tcp-accept "could not accept from listener" tcpl)))) ) ) )

;; own socket interface

(: askemos:run-tcp-server
   ((or false :ip-addr:) fixnum
    (procedure (input-port output-port *) *)
    (or false (struct <semaphore>))
    (or false (procedure
	       ((or false string) input-port output-port
		       (procedure () boolean) (procedure (#!rest) undefined))
	       . *))
    string
    -> undefined))
(define (askemos:run-tcp-server
         host port connection-handler
         maximum-semaphore capture-handler name)
  (define request-queue (tcp-listen port 4 host))
  (define (exception-handler tag ex)
    (receive
     (title msg args rest) (condition->fields ex)
     (logerr "~a (host ~a port ~a) ~a ~a ~a\n"
             tag host port title msg args)))
  (define (close-ports in-port out-port)
    (guard
     (ex (else (exception-handler 'tcp-close-input ex)))
     (close-input-port in-port))
    (guard
     (ex (else (exception-handler 'tcp-close-output ex)))
     (close-output-port out-port)))
  (set-open-fd! (##sys#slot request-queue 1) '(tcp listen))
  (let loop ()
    (guard
     (ex (else (exception-handler 'run-tcp-server ex)))
     (receive
      (fd peer) (tcp-get-next-client request-queue 'tcpsocket)
      (receive
       (in-port out-port) (net-io-ports fd peer shutdown-read shutdown-write closesocket)
       (guard
	(ex
	 (else (close-ports in-port out-port) (raise ex)))
	(if maximum-semaphore (semaphore-wait-by! maximum-semaphore 1))
	#;(if maximum-semaphore (add-peer! peer))
	(thread-start!
	 (make-thread
	  (lambda ()
	    (guard
	     (ex
	      ((eq? ex 'connection-captured)
	       (if (procedure? capture-handler)
		   (let ((in in-port) (out out-port))
		     (capture-handler
		      #f in-port out-port
		      (lambda () #t) (lambda (x) (close-ports in out))))
		   (close-ports in-port out-port))
	       (set! in-port #f))
	      (else (exception-handler 'run-tcp-connection ex)))
	     (connection-handler in-port out-port peer))
	    (if maximum-semaphore (semaphore-signal-by! maximum-semaphore 1))
	    #;(if maximum-semaphore (release-peer! peer))
	    (if in-port (close-ports in-port out-port) ))
	  name))))))
    (loop)))

(define (close-ports return exception-handler in out)
  (close-input-port in)
  (with-exception-handler
   (lambda (ex) (exception-handler ex) (return ex))
   (lambda () (close-output-port out))))

(: askemos:run-tcp-client (:ip-addr: fixnum (input-port output-port -> *) -> . *))
(define (askemos:run-tcp-client host port connection-handler)
  (define (exception-handler ex)
    (receive
     (title msg args rest) (condition->fields ex)
     (logerr "run-tcp-client (host ~a port ~a) ~a ~a ~a\n"
             host port title msg args)))
  (bind-exit
   (lambda (return)
     (with-exception-handler
      (lambda (ex) (exception-handler ex) (return ex))
      (lambda ()
        (receive
         (in out) (tcp-connect host port)
         (with-exception-handler
          (lambda (ex)
            (return (close-ports return exception-handler in out)))
          (lambda ()
            (let ((result (connection-handler in out)))
	      (close-ports return exception-handler in out)
	      result)))))))))

(define *debug-ssl* #f)

(define-syntax if-debug-ssl
  (syntax-rules ()
    ((if-debug-ssl body ...)
     (if *debug-ssl* (begin body ...)))))

(define (debug-ssl . flag)
  (if (pair? flag)
      (set! *debug-ssl* (car flag)))
  *debug-ssl*)

(define ssl-keep-alive-timeout
  (let ((t 660))
    (lambda args
      (if (pair? args)
	  (let ((t0 t)) (set! t (car args)) t0)
	  t))))

(define mux-header-type
  (foreign-lambda*
   integer ((scheme-object h))
   "return(((*(u32 *)C_c_string(h))>>24) & 0xFF);"))

(define mux-header-size
  (foreign-lambda*
   integer ((scheme-object h))
   "return((*(u32 *)C_c_string(h)) & 0x3FFFF);"))

(define make-mux-header*
  (foreign-lambda*
   void ((scheme-object o) (integer type) (integer len))
   "C_char *buf=C_c_string(o);
    *((u32 *) buf) = ((type << 24) & 0xFF000000) + (len & 0x3FFFF);"))

(define (make-mux-header type len)
  (if (or (fx< type 0) (fx> type #xff))
      (error (format "make-mux-header type out of range ~a" type)))
  (if (or (fx< len 0) (fx> len #x3ffff))
      (error (format "make-mux-header length out of range ~a" len)))
  (let ((s (make-string/uninit 4))) (make-mux-header* s type len) s))

(define-record-type <ssl-socket>
  (make-ssl-socket properties peer
		   certificate cakey cert-mux cert-avail
		   plaintext-in plaintext-out
		   mux pid)
  ssl-socket?
  (properties properties set-properties!)
  (peer ssl-peer)
  (certificate certificate-value set-certificate-value!)
  (cakey ssl-cakey ssl-cakey-set!)
  (cert-mux cert-mux)
  (cert-avail cert-avail)
  (plaintext-in ssl-input-port)
  (plaintext-out ssl-output-port)
  (mux mux-out)
  (pid ssl-process set-ssl-process!))

(define (ssl-certificate self)
  (if (eq? (certificate-value self) 'unknown)
      (begin
	(mutex-lock! (cert-mux self) #f #f)
	(if (eq? (certificate-value self) 'unknown)
	    (begin
	      (mutex-unlock! (cert-mux self) (cert-avail self))
	      (ssl-certificate self))
	    (let ((c (certificate-value self)))
	      (mutex-unlock! (cert-mux self)) c)))
      (certificate-value self)))

(define (set-certificate! self cert)
  (set-certificate-value! self cert)
  (condition-variable-broadcast! (cert-avail self)))

(define $ctl-shutdown (string-append (make-mux-header #xCC 1) "c"))

(define (control-write self packet)
  (if-debug-ssl (format (current-error-port) "*** >>> CONTROL ~s\n" (string-ref packet 4)))
  (write-string packet #f (mux-out self)))

#|
(define (get-rid-of-pid pid)
  (if pid
      (handle-exceptions
       ex #f
       (let loop ()
	 (receive (p f s) (process-wait pid #t)
		  (if (eqv? p 0)
		      (begin
			(process-signal pid signal/kill)
			(loop))
		      s))))))
|#

(define (ssl-close self)
  (guard
   (err (else
	 (if-debug-ssl
	  (format (current-error-port) "*** close<ssl-socket> ~a\n" err))))
   (control-write self $ctl-shutdown)
   (flush-output (mux-out self)))
  ;; this should normally trigger the subprocess to exit
  (guard
   (err (else
	 (if-debug-ssl
	  (format (current-error-port) "*** close<ssl-socket> (plaintext-out self) ~a\n" err))))
   (close-output-port (ssl-output-port self)))
  (guard (ex (else #f)) (close-input-port (ssl-input-port self)))
  ;; get-rid-of-pid should not be here at all; it better wouldn't exist in the 1st place!
  (if (ssl-process self)
      (begin
	(get-rid-of-pid (ssl-process self))
	(set-ssl-process! self #f)))
  #t)

(define (make-mux-output-port mux-output)
  (define (write-chunk s)
    (write-string (make-mux-header #xDD (string-length s)) #f mux-output)
    (write-string s #f mux-output)
    (flush-output mux-output))
  (make-output-port
   (lambda (str)
     (if (< (string-length str) 250000)
        (write-chunk str)
        (let loop ((i 0))
          (let ((j (+ i 250000)))
            (if (>= j (string-length str))
                (write-chunk (substring/shared str i))
                (begin
                  (write-chunk (substring/shared str i j))
                  (loop j)))))))
   (lambda () (close-output-port mux-output))
   (lambda () (flush-output mux-output))))

(define (make-sslmgr cnx1 cnx peer certinfo)
  (make-sslmgr* cnx1 cnx peer certinfo #t #f))

(define *sslmgr-executable* "sslmgr")

(define (make-ssl-manager-options cnx peer certinfo server?)
  `("-F"
    ,@(let ((tmo (ssl-keep-alive-timeout)))
        (if (fixnum? tmo) `("-t" ,(number->string tmo)) '()))
    "-p"
    ,(if server? (format "fdsrv:~a" cnx)
         (if (string? cnx) (string-append cnx peer)
             (format "fdclient:~a" cnx)))
    ,@(if ($ca-slave) '() '("-w"))
    ,@(if *debug-ssl* '("-v") '())
    . ,certinfo))

(cond-expand
 (never-fork
  (define (setup-ssl-manager cnx1 cnx peer certinfo server?)
    (let ((sr (sslmgr-connection-r0 cnx1))
          (sw (sslmgr-connection-w1 cnx1))
          (cmd (make-ssl-manager-options cnx peer certinfo server?)))
      (let ((tid (ssl-mgr-main sr sw sw `("sslmgr" . ,cmd))))
        (if (not (pthread? tid))
            (begin
              (if (number? cnx) (file-close! cnx))
              (if *debug-ssl* (format (current-error-port) "pthread_sslmgr failed ~a\n" cmd))
              (error "failed to start sslmgr in pthread" tid))
            (let ((cr (sslmgr-connection-r1 cnx1)))
              (if *debug-ssl* (format (current-error-port) "pthread_sslmgr ~a ~a ~a\n" sr sw cmd))
              (receive
               (in out)
               (net-io-ports
                cr #f
                shutdown-read shutdown-write
                (lambda (fd) (close-sslmgr-connection cnx1 'c)))  ;; closesocket
               (values tid out in))))))))
 (else
  (define (setup-ssl-manager cnx1 cnx peer certinfo server?)
    (let ((pid (save-fork)))
      (cond
       ((fx= pid 0)
        ((foreign-lambda*
          void () #<<EOF
#if defined(ANDROID) || defined(LINUX)
  if(prctl(PR_SET_NAME, (unsigned long) "sslmgr", 0, 0, 0))
    fprintf(stderr, "ERROR: PR_SET_NAME %s\n", strerror(errno));
#else
#endif
EOF
))
        (guard
         (ex (else (_exit 1)))
         (let ((pid (current-process-id)))
           ;; (sudo! runuser)
           ;; (file-close cr) (file-close cw)
           (duplicate-fileno (sslmgr-connection-r0 cnx1) 0)
           (duplicate-fileno (sslmgr-connection-w1 cnx1) 1)
           (if (number? cnx) (duplicate-fileno cnx 3) (file-close! 3))
					;(format (current-error-port) "~a close all ports\n" pid)

           ;; DON'T try (again) to live without closing those ports!
           ;; If the chicken is killed the sslmgrs will keep
           ;; listening on the sockets, preventing the restart.
           (close-all-ports-except 0 1 2 3)
					;(format (current-error-port) "~a force major gc\n" pid)
					;(gc #t)
					;(format (current-error-port) "~a run server ~a\n" pid server?)
           (let ((cmd (make-ssl-manager-options (if (number? cnx) 3 cnx) peer certinfo server?)))
             (cond-expand
              (single-binary
               (if *debug-ssl* (format (current-error-port) "forked_sslmgr ~a\n" cmd))
               (ssl-mgr-main 0 1 2 `("sslmgr" . ,cmd)))
              (else
               (if *debug-ssl* (format (current-error-port) "~a ~a\n" *sslmgr-executable* cmd))
               (process-execute *sslmgr-executable* cmd))))
           (_exit 0))))
       ((fx= pid -1)
        (close-sslmgr-connection cnx1 #t)
        (if (number? cnx) (file-close! cnx))
        ;; FIXME: some chicken update must have broken things.
        ;; Force a restart here while chicken is broken.
        ;;
        ;;(error "make-sslmgr: fork failed")
        (logerr "make-sslmgr: fork failed\n")
        (exit 1)
        )
       (else
        (close-sslmgr-connection cnx1 's)
        (if (number? cnx) (file-close! cnx))
        ;;(make-nonblocking w0)
        (receive (in out) (process-io-ports pid (sslmgr-connection-r1 cnx1) (sslmgr-connection-w0 cnx1))
                 (values pid out in))))))))

(define-record-type sslmgr-connection
  (%make-sslmgr-connection r0 w0 r1 w1)
  sslmgr-connection?
  (r0 sslmgr-connection-r0 sslmgr-connection-r0-set!)
  (w0 sslmgr-connection-w0 sslmgr-connection-w0-set!)
  (r1 sslmgr-connection-r1 sslmgr-connection-r1-set!)
  (w1 sslmgr-connection-w1 sslmgr-connection-w1-set!))

(define (make-sslmgr-connection)
  (cond-expand
   (never-fork
    (receive
     (c s) (unix-socket-pair*)
     (begin
       (set-open-fd! c 'sslmgr-c) #;(set-open-fd! s 'sslmgr-s))
     (%make-sslmgr-connection s c c s)))
   (else
    (receive
     (r0 w0) (create-pipe)
     (receive
      (r1 w1) (create-pipe)
      (%make-sslmgr-connection r0 w0 r1 w1))))))

(define (close-sslmgr-connection cnx key)
  (case key
    ((s)
     (and-let*
      ((r (sslmgr-connection-r0 cnx)))
      (let ((w (sslmgr-connection-w1 cnx)))
        (if (eqv? w r)
            (begin
              (closesocket w)
              (sslmgr-connection-r0-set! cnx #f)
              (sslmgr-connection-w1-set! cnx #f))
            (begin
              (file-close! r)
              (file-close! w)
              (sslmgr-connection-r0-set! cnx #f)
              (sslmgr-connection-w1-set! cnx #f))))))
    ((c)
     (and-let*
      ((r (sslmgr-connection-r1 cnx)))
      (let ((w (sslmgr-connection-w0 cnx)))
        (if (eqv? w r)
            (begin
              (closesocket w)
              (sslmgr-connection-r1-set! cnx #f)
              (sslmgr-connection-w0-set! cnx #f))
            (begin
              (file-close! r)
              (file-close! w)
              (sslmgr-connection-r1-set! cnx #f)
              (sslmgr-connection-w0-set! cnx #f))))))
    (else (close-sslmgr-connection cnx 'c) (close-sslmgr-connection cnx 's))))

(define (make-sslmgr* cnx1 cnx peer certinfo server? pp)
  (receive
   (rpin wpin) (make-internal-pipe)	; pin="Plaintext INput"
   (receive
    (pid mux-out mux-in) (setup-ssl-manager cnx1 cnx peer certinfo server?)

    (let ((ssl (make-ssl-socket
                '() peer 'unknown #f
                (make-mutex peer) (make-condition-variable peer)
                rpin (make-mux-output-port mux-out)
                mux-out
                pid)))
      ;;
      (if pp
          (begin
            (format (current-error-port) "Setting PASSPHRASE = ~s\n" pp)
            (set-properties! ssl `((passphrase . ,pp) . ,(properties ssl)))))
      (thread-start!
       (make-thread
        (lambda ()
          (guard
           (err (else (if-debug-ssl (display err (current-error-port)))))
           (let loop ()
             (let ((l (read-string 4 mux-in)))
               (if (not (string-null? l));; (not (eof-object? l))
                   (let ((type (mux-header-type l))
                         (len (mux-header-size l)))
                     (let ((data (read-string len mux-in)))
                       (case type
                         ((#xCC)
                          (if-debug-ssl
                           (format (current-error-port) "*** <<< CONTROL ~s\n" data))
                          (process-control-packet ssl data))
                         ((#xDD)
                          (if-debug-ssl (format (current-error-port)
                                                "*** <<< DATA [~a] ~a\n"
                                                (string-length data) (substring/shared data 0 (min (string-length data) 70))))
                          (if (eq? (certificate-value ssl) 'unknown)
                              (set-certificate! ssl #f))
                          (write-string data #f wpin))
                         (else
                          (error (format "Bad framing header type: ~s ~a" type l))))
                       (loop)))))))
          (set-certificate! ssl #f)
          (set-ssl-process! ssl #f)
          (handle-exceptions ex #f (close-output-port wpin))
          (handle-exceptions ex #f (close-input-port mux-in))
          (ssl-close ssl)
          #;(close-sslmgr-connection cnx1 'c)
          ;; (handle-exceptions ex #f (receive (p f s) (process-wait pid) s))
          (get-rid-of-pid pid))
        (format "ssl[~a].in" (sslmgr-connection-r1 cnx1))))
      ssl))))

(define-values (reg-cert! get-cert ssl-verify-callback-set!)
  (let ((g (lambda (k) #f))
	(s (lambda (k v) #f)))
    (values
     (lambda (k v) (s k v))
     (lambda (k) (g k))
     (lambda (get set)
       (assert (procedure? get))
       (assert (procedure? set))
       (set! g get)
       (set! s set)))))

(define $ca-slave
  (let ((v #f))
    (lambda args
      (if (pair? args)
	  (let ((o v))
	    (set! v (and (car args) #t))
	    o)
	  v))))

(define (process-control-packet ssl data)
  (let ((end (string-index data #\space)))
    (let ((e (do ((i end (add1 i)))
		 ((or (fx= i (string-length data))
		      (not (eqv? (string-ref data i) #\space)))
		  i)))
	  (tag (substring data 0 (or end (string-length data)))))

      (cond
       ((string=? tag "peer-cert")
	(let ((cert (substring data e)))
	  (if (string=? cert "-none-")
	      (set-certificate! ssl #f)
	      (let* ((cert0 (subject-string->mesh-certificate cert))
		     (cert (if ($ca-slave) cert0
			       (and-let* ((cakey (ssl-cakey ssl))) (reg-cert! cakey cert0)))))
		(set-certificate! ssl cert)))))
       ((string=? tag "peer-cakey")
	(let* ((key (substring data e))
	       (entry (get-cert key))
	       (result
		(cond
		 ((mesh-certificate? entry)
		  (format "v1~a/~a" (mesh-cert-l entry) (mesh-cert-o entry)))
		 (entry "v0") ;; deny
		 (else "v1/")))) ;; TOFU
	  (ssl-cakey-set! ssl key)
	  (control-write ssl
			 (string-append
			  (make-mux-header #xCC (string-length result))
			  result))
	  (flush-output-port (mux-out ssl))))
       ((string=? tag "passwd:private")
	(let ((p (let ((p (assq 'ssl (properties ssl))))
		   (and p (cadr p)))))
	  (control-write ssl
			 (string-append
			  (make-mux-header #xCC (+ 1 (string-length p)))
			  "p"
			  p))
	  (flush-output-port (mux-out ssl))))
       ((string=? tag "error:")
	(if-debug-ssl
	 (format (current-error-port) "***sslmgr ~a\n"
		 (substring data e))))
       ((string=? tag "ssl-err")  ;; ssl_choke
	(if-debug-ssl
	 (format (current-error-port) "***sslmgr choke ~a\n"
		 (substring data e))))
       (else
	;; ignore it for Now...
	'ignored)))))

;; -----------------------

(define io-need-restart? (foreign-lambda bool "io_needs_restart"))

(define sslsocket 'sslsocket)

(: tcp-get-next-client ((struct tcp-listener) * -> fixnum string))
(define (tcp-get-next-client tcpl name)
  (let ((fd (##sys#slot tcpl 1)))
    (let loop ()
      (thread-wait-for-i/o! fd #:input)
      (let ((fd (accept fd #f #f)))
	(if (eq? -1 fd)
	    (if (io-need-restart?)
		(loop)
		(network-error 'tcp-accept "could not accept from listener" tcpl (##sys#slot tcpl 1)))
	    (begin
	      (set-open-fd! fd name)
	      (values fd (cond
                          ((eq? name 'sslsocket6) (getpeername6 fd))
                          (else (getpeername fd)))))))) ) )

(: askemos:run-ssl-server
   ((or false :ip-addr:)
    fixnum
    *					; certinfo
    (procedure
     (input-port output-port
     * string) . *)			; connection-handler
    (procedure
     (*) . *)				; exception-handler
    (or false (struct <semaphore>))
    (or false
	(procedure
	 (string input-port output-port
		 (procedure () boolean) (procedure (#!rest) undefined))
	 . *)) ; capture-handler
    *
    -> . *))
(define (askemos:run-ssl-server
         host port certinfo connection-handler exception-handler
         maximum-semaphore capture-handler name)
  (define request-queue (tcp-listen port 4 host))
  (define (exception-handler ex)
    (log-condition (format "run-ssl-server (host ~a port ~a)" host port) ex))
  (define accepted (make-peer-pool))
  (define blocking-connections #f)
  (ssl-peers-set! accepted)
  (let loop ()
    (handle-exceptions
     ex (exception-handler ex)
     (receive
      (cnx peer) (tcp-get-next-client request-queue (if (host-ipv6? host) 'sslsocket6 sslsocket))
#|
      (if (and (eqv? (semaphore-value maximum-semaphore) 0) (not blocking-connections))
	  (begin
	    (set! blocking-connections #t)
	    (logerr "W Blocking connections ~a:~a next ~a\n" host port peer)))
      (if maximum-semaphore (semaphore-wait-by! maximum-semaphore 1))
      (if blocking-connections
	  (begin
	    (logerr "W Unblocking connections ~a:~a now ~a\n" host port peer)
	    (set! blocking-connections #f)))
|#
      (if (and maximum-semaphore (not (add-peer! accepted peer)))
	  (thread-start! (make-thread
			  (lambda ()
			    (logerr "W: More than ~a connection from ~a. (closing after 5'')\n"
				    (ssl-connlimit) peer)
			    (##dbg#connlimitsignal peer)
			    (thread-sleep! 5) (file-close! cnx))
			  peer))
	  (thread-start!
	   (make-thread
	    (lambda ()
	      (let ((ssl #f)
                    (cnx1 (make-sslmgr-connection)))
                (guard
                 (ex (else (close-sslmgr-connection cnx1 #t) (exception-handler ex)))
                 (set! ssl (make-sslmgr cnx1 cnx peer
					(if (string? host)
					    `("-i" ,host . ,certinfo)
					    certinfo))))
		(guard
		 (ex
		  ((eq? ex 'connection-captured)
		   (if capture-handler
		       (handle-exceptions
			ex (exception-handler ex)
			(let ((ssl ssl))
			  (capture-handler
			   (ssl-certificate ssl) (ssl-input-port ssl) (ssl-output-port ssl)
			   (lambda ()
			     (and ssl (ssl-process ssl)
				  (receive (p f s) (process-wait (ssl-process ssl) #t) (eqv? p 0))))
			   (lambda dummy
			     (if ssl (let ((s ssl)) (set! ssl #f) (ssl-close s) #;(close-sslmgr-connection cnx1 'c)))))))
		       (if ssl (begin (handle-exceptions ex #f (ssl-close ssl)) #;(close-sslmgr-connection cnx1 'c))))
		   (set! ssl #f))
		  (else (exception-handler ex)))
		 (if ssl (connection-handler (ssl-input-port ssl) (ssl-output-port ssl)
                                             peer (ssl-certificate ssl))))
		#;(if maximum-semaphore (semaphore-signal-by! maximum-semaphore 1))
		(if maximum-semaphore (release-peer! accepted peer))
		(if ssl (begin (ssl-close ssl) #;(close-sslmgr-connection cnx1 'c)))))
	    name)))
      (loop)))))

(define socks4a-setup!*
  (foreign-lambda*
   integer ((integer fd) ((const c-string) hf) (unsigned-integer pf)) #<<EOF
#define BUFLEN 10
  C_char buffer[BUFLEN+1], *buf=buffer;
  int len = snprintf(buf, BUFLEN + 1,
                     "\x4\x1%c%c00010%s",
                     (C_char) ((pf & 0xff00) >> 8), /* port MSB */
                     (C_char) (pf & 0xff), /* port LSB */
                     hf);
  if( len >= BUFLEN ) {
    buf = alloca( len + 1 );
    snprintf(buf, len + 1,
             "\x4\x1%c%c00010%s",
             (C_char) ((pf & 0xff00) >> 8), /* port MSB */
             (C_char) (pf & 0xff), /* port LSB */
             hf);
  }
  buf[4]='\0';
  buf[5]='\0';
  buf[6]='\0';
  buf[7]='\1';
  buf[8]='\0';
  if (write( fd, buf, len+1 ) != len+1) return(-1);
  len = read( fd, buf, 8 );
  if( len != 8 || buf[0] != 0 ) return(-2);
  if( buf[1] != (C_char) 90 ) return(-((int) buf[1]));
  return(0);
#undef BUFLEN
EOF
))

(define (socks4a-setup! fd host port)
  (case (socks4a-setup!* fd host port)
    ((0) #t)
    ((-1) (raise "socks server connect request"))
    ((-2) (raise "socks server connect"))
    ((-91) (raise "request rejected or failed"))
    ((-92) (raise "rejected; server cannot connect to identd on the client"))
    ((-93) (raise "rejected because the client program and identd report different user-ids"))
    (else (raise "unknown code"))))

(define (askemos:open-ssl-client addr ca cert key socks4a verbose)
  (let ((ssl #f)
        (cnx1 (make-sslmgr-connection)))
    (guard
     (ex
      (ssl
       (handle-exceptions ex #f (ssl-close ssl))
       (raise ex))
      (else
       (if cnx1 (close-sslmgr-connection cnx1 #t))
       (raise ex)))
     (set! ssl (make-sslmgr*
                cnx1
                "connect:" addr
		`(,@(if verbose '("-v") '())
                  ,@(if cert `("-c" ,cert) '())
		  ,@(if key `("-k" ,key) '())
		  ,@(if socks4a	`("-s" ,socks4a) '())
		  ,@(if ($ca-slave) '() '("-w"))
		  . ,(if ca
			 (list "-E" "-A" ca "-T" ca)
			 '()))
		#f #f))
     (set! cnx1 #f)
     (values (ssl-input-port ssl) (ssl-output-port ssl)
	     (and cert
		  (or (ssl-certificate ssl)
		      (if ca (raise 'no-certificate) #f)))
	     (lambda () (and ssl (ssl-process ssl) #t))
	     (lambda (c) (if ssl
			     (let ((x ssl))
			       (if-debug-ssl
				(format (current-error-port) "** SSL client forced close ~a\n" addr))
			       (set! ssl #f) (ssl-close x) #;(close-sslmgr-connection cnx1 'c))))))))

) ;; module sslsocket

(import (prefix sslsocket ssl:))

(define askemos:run-tcp-server ssl:askemos:run-tcp-server)


(define make-sslmgr ssl:make-sslmgr)
(define ssl-close ssl:ssl-close)
(define ssl-certificate ssl:ssl-certificate)
(define debug-ssl ssl:debug-ssl)
(define ssl-input-port ssl:ssl-input-port)
(define ssl-output-port ssl:ssl-output-port)
(define askemos:run-ssl-server ssl:askemos:run-ssl-server)
(define askemos:open-ssl-client ssl:askemos:open-ssl-client)
(define ssl-keep-alive-timeout ssl:ssl-keep-alive-timeout)

(define askemos:run-tcp-server ssl:askemos:run-tcp-server)
