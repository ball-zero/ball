;; (C) 2002, 2003, 2008 J�rg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit http-client)
 ;;
 (usual-integrations)
;;NO (disable-interrupts)			; not proven
 )

(module
 http-client
 (
  httpX-send-message!
  http-response https-response
  https-response-auth
  http-send-message!
  https-send-message!
  ;http-affirm-user-authorization
  ;http-affirm-authorization
  http-cc-send-message!
  *http-transfered-headers*
  http-for-each
  ;;
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:)
	 srfi-19 srfi-34 srfi-35 srfi-45
	 atomic libmagic pcre regex shrdprmtr util timeout mailbox
	 (prefix dns dns-)
	 atomic parallel cache
	 tree notation aggregate place-common place
	 protocol-common protocol-connpool rfc-2616 rfc-2518
	 extras data-structures ports)

(import (only srfi-18 current-thread))

(import (only sslsocket ip-address/port?))

(include "typedefs.scm")

(define-inline (loghttps fmt . rest)
  (if ($https-client-verbose) (apply logerr fmt rest)))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/http/client.scm")

)
