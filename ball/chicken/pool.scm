;; (C) 2002, 2008 Jörg F. Wittenberger

;; chicken module clause for Askemos' central place module.

(declare
 (unit pool)
 ;;
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)

 ;; currently contains define-atomic-type
 (disable-interrupts)

 )

(module
 pool
 (
  ;; place.scm
  $use-well-known-symbols
  bind-place!
  $enforce-frame-oid-match
  ;;;
  initial-mind ; should be private
  locked-frames				; debugging aid
  commit-frame!
  frame-oid-match? frame-oid-must-match?
  make-place! make-place-for-id!
  sync-on-load? sync-on-load
  find-frame-by-id-on find-frames-by-id
  find-frame-by-id/asynchroneous find-frame-by-id/asynchroneous+quorum
  find-frame-by-id/synchronised+quorum find-frame-by-id/resynchronised+quorum
  find-frame-by-id/quorum find-local-frame-by-id
  find-already-loaded-frame-by-id
  document
  make-object-not-available-condition object-not-available-condition?
  missing-object-resource missing-object-source
  ;; protocoll interface
  hosts-lookup-at-least
  host-alive? host-maybe-alive?
  metainfo-remote privilege-info-remote
  resynchronize
  quorum-lookup
  ;;
  quorum-get!
  strict-public-equivalent? rough-public-equivalent? public-equivalent?
  make-context public-context user-context
  public-capabilities
  ;;
  delete-is-sync-flag! is-synced? delete-is-sync-hook
  set-missing-oid!
  pool-init! pool-reset! pool-fold!
  *resync-cache*
  invite-new-supporters!
  resync-now!
  quorum-members-to-inform
  ;; admin; might better go into "icallyou.scm"
  $administrator administrator set-administrator-email!
  check-administrator-password
  set-administrator-password!
  administrator-password-hash
  ;; internal ; mabe already here to be factored out again.
  $insecure-mode
  dump-message!
  table->rdf-bag-sorted
  make-rdf-li-for-link table->rdf-bag
  frame-metadata-links-attlist
  rdf-parsetype-literal-attribute-list
  signature-att-decl

  nu-action call-subject! set-call-subject!
  
  set-working-mind!/lolevel
  mind-waiting-readers
  mind-pool-cleanup!

  set-resync-completion!

  want-links-local? want-body-local?
  ;; init
  set!-commit-frame!
  set!-resync-now!
  message-body-fetch-blob-function
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 condition? with-exception-handler promise?) srfi-34 srfi-35 srfi-45)

(import srfi-1 srfi-13 (except srfi-18 raise) srfi-19 (prefix srfi-43 srfi:) srfi-69
	openssl
	shrdprmtr
	mailbox memoize environments util timeout atomic parallel cache
        protection pcre sqlite3
	ports extras data-structures tree notation function bhob aggregate channel)

(import storage-api place-common)

(import (prefix srfi-13 srfi:))

(import (only lolevel move-memory!))

(include "typedefs.scm")
(define-type :%%find-frame-sync!: (:oid: :place-or-not: :quorum: -> :place-or-not:))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/pool.scm")

)
(import (prefix pool m:))
(define message-body-fetch-blob-function m:message-body-fetch-blob-function)
(define pool-reset! m:pool-reset!)
(define find-frames-by-id m:find-frames-by-id)
(define find-local-frame-by-id m:find-local-frame-by-id)
;; setup
(define (commit-frame! d f c n) (m:commit-frame! d f c n))
(define bind-place! m:bind-place!)
(define frame-oid-match? m:frame-oid-match?)
(define $enforce-frame-oid-match m:$enforce-frame-oid-match)
