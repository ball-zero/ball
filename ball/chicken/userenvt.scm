;; (C) 2011 Jörg F. Wittenberger

(declare
 (unit userenvt)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations))

(module
 userenvt
 (
  dsssl-timestamp
  dsssl-error
  dsssl-pcre
  dsssl-make-load
  iconv
  qrencode
  dsssl-normalise
  dsssl-read-locator
  dsssl-write-locator
  dsssl-sxml
  dsssl-with-exception-handler
  dsssl-x509-text
  dsssl-sql-ref dsssl-sql-ref/default
  dsssl-sql-query dsssl-sql-fold
  dsssl-message-action dsssl-current-contract dsssl-source-contract
  bail-current-message bail-current-place
  bail-form-field
  bail-current-sqlite-db
  dsssl-message-identifier dsssl-self-identifier dsssl-self-affirm-secret
  dsssl-secret-encode
  anon-secret
  dsssl-message-replicates
  sybil?
  replicates-join
  dsssl-message-creator dsssl-message-date dsssl-message-last-modified
  dsssl-message-caller
  dsssl-message-replicates
  dsssl-message-location dsssl-message-destination
  ;;
  dsssl-message-potentialities
  ;;
  dsssl-link-ref dsssl-fold-links dsssl-fold-links/ascending
  dsssl-metainfo dsssl-metainfo/adopt
  ;;
  dsssl-apply-xslt-stylesheet
  ;;
  $dns-user
  $dns-password
  $kernel-debug
  ;;
  loopback-address

  $kernel-debug
  $dns-user
  $dns-password

  askemos-client-bindings

  askemos-register-client-bindings!
  my-mind-exit
  operation-control
  load-or-die
  signal-via-entry! send-via-entry! query-via-entry!
  install-new-private-name!
  ;;
  ball-control-socket-free
  ball-control-socket-service-start!
  ball-control-socket-connect
  )
(import (except scheme force delay vector-fill! vector->list list->vector)
	(except chicken add1 sub1 with-exception-handler condition? promise? vector-copy!)
	(except srfi-18 raise)
	ports extras
	pthreads srfi-13
	srfi-1 srfi-19 srfi-34 srfi-35 srfi-110 srfi-43 srfi-45
	matchable util atomic sslsocket timeout parallel pcre)

(import protection utf8utils tree notation function sqlite3 bhob aggregate
	storage-api place-common pool place pool methods corexml protocol hostmesh
	storage-common storage step trstcntl cntrl nu cgi xmlrpc markdown)

(import notation-charenc wttree notation-xmlrender)

(import (only openssl md5-digest))

;; only for startup helper code, TBD: move the latter into its own module.
(import (only dispatch signal-subject!))

(import qrcodegen stb-image-write)

(include "../mechanism/userenvt.scm")

(import srfi-106)
(import (prefix posix posix:))

(define control-server #f)

(import (only files make-pathname))
(define (ball-control-socket-name dir) (make-pathname dir "control"))

(define (ball-control-socket-free dir)
  (define fn (ball-control-socket-name dir))
  (define max-wait 8)
  (if (posix:socket? fn)
      (let loop ((w 1))
	(let ((s (guard (ex (else #f)) (if (> w 1) (thread-sleep! w)) (unix-connect fn))))
	  (if s
	      (begin (socket-close s) #f)
	      (if (< w max-wait) (loop (* w 2))
		  (begin
		    (delete-file fn)
		    #t)))))
      (not (file-exists? fn))))

(define (ball-control-socket-service-start! dir connection-handler #!key (serial #f))
  (define fn (ball-control-socket-name dir))
  (define (call-handler s)
    (guard
     (ex (else (log-condition 'ball-control-socket-connection ex)))
     (connection-handler s)))
  (if (not (ball-control-socket-free dir))
      (error "rep is busy" dir))
  (cond
   ((not (file-exists? fn))
    (let ((running (make-mutex)))
      (mutex-lock! running)
      (!start
       (guard
        (ex (else
             (log-condition "ball-control-socket-connection listening error" ex)
             (logerr "Exiting 42.\n")
             (exit 42)))
        (call-with-socket
         (unix-listen fn)
         (lambda (s)
           (mutex-unlock! running)
           (do () (#f)
             (let ((s (guard
                       ;; How comes this is ever triggerd?  Let alone always.
                       (ex (else
                            (log-condition "ball-control-socket-connection accept error" ex)
                            #f))
                       (socket-accept s))))
               (and (socket? s)
                    (if serial
                        (call-with-socket s call-handler)
                        (!start (call-with-socket s call-handler) 'ball-control-socket-connection))))))))
       'ball-control-socket-service)
      (mutex-lock! running)))
   (else (error (format "file '~a' exists" fn)))))

(define (ball-control-socket-connect dir)
  (let* ((fn (ball-control-socket-name dir)))
    (cond
     ((posix:socket? fn) (unix-connect fn))
     ((file-exists? fn) (error (format "file '~a' exists and is not a socket" fn)))
     (else #f))))

)

(import (prefix userenvt m:))

(define dsssl-timestamp m:dsssl-timestamp)
(define dsssl-error m:dsssl-error)
(define dsssl-pcre m:dsssl-pcre)
(define dsssl-make-load m:dsssl-make-load)
(define iconv m:iconv)
(define qrencode m:qrencode)
(define dsssl-normalise m:dsssl-normalise)
(define dsssl-read-locator m:dsssl-read-locator)
(define dsssl-write-locator m:dsssl-write-locator)
(define dsssl-sxml m:dsssl-sxml)
(define dsssl-with-exception-handler m:dsssl-with-exception-handler)
(define dsssl-x509-text m:dsssl-x509-text)
(define dsssl-sql-ref m:dsssl-sql-ref)
(define dsssl-sql-ref/default m:dsssl-sql-ref/default)
(define dsssl-sql-query m:dsssl-sql-query)
(define dsssl-sql-fold m:dsssl-sql-fold)
(define dsssl-message-action m:dsssl-message-action)
(define dsssl-current-contract m:dsssl-current-contract)
(define dsssl-source-contract m:dsssl-source-contract)
(define bail-current-message m:bail-current-message)
(define bail-current-place m:bail-current-place)
(define bail-current-sqlite-db m:bail-current-sqlite-db)
(define bail-form-field m:bail-form-field)

(define dsssl-message-identifier m:dsssl-message-identifier)
(define dsssl-self-identifier m:dsssl-self-identifier)
(define dsssl-self-affirm-secret m:dsssl-self-affirm-secret)
(define dsssl-secret-encode m:dsssl-secret-encode)
(define anon-secret m:anon-secret)
(define dsssl-message-replicates m:dsssl-message-replicates)
(define sybil? m:sybil?)
(define replicates-join m:replicates-join)
(define dsssl-message-creator m:dsssl-message-creator)
(define dsssl-message-date m:dsssl-message-date)
(define dsssl-message-last-modified m:dsssl-message-last-modified)
(define dsssl-message-caller m:dsssl-message-caller)
(define dsssl-message-replicates m:dsssl-message-replicates)
(define dsssl-message-potentialities m:dsssl-message-potentialities)
;;
(define dsssl-message-location m:dsssl-message-location)
(define dsssl-message-destination m:dsssl-message-destination)
;;
(define dsssl-link-ref m:dsssl-link-ref)
(define dsssl-fold-links m:dsssl-fold-links)
(define dsssl-fold-links/ascending m:dsssl-fold-links/ascending)

(define dsssl-metainfo m:dsssl-metainfo)
(define dsssl-metainfo/adopt m:dsssl-metainfo/adopt)

(define dsssl-apply-xslt-stylesheet m:dsssl-apply-xslt-stylesheet)

(define signal-via-entry! m:signal-via-entry!)
(define send-via-entry! m:send-via-entry!)
(define query-via-entry! m:query-via-entry!)
(define install-new-private-name! m:install-new-private-name!)

(define $dns-user m:$dns-user)
(define $dns-password m:$dns-password)
(define $kernel-debug m:$kernel-debug)
