;; https://code.google.com/p/lalr-scm/
(declare
 (unit lalr)
 (uses lalrgen lalrdrv)
 (usual-integrations)
 )

(module
 lalr
 *
 (reexport lalrdrv lalrgen)

 )
