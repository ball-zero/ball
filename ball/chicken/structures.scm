;; (C) 2011 Jörg F. Wittenberger

(declare
 (unit structures)
 ;;
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)
 (disable-interrupts)
 )

(module
 structures
 (;; export
  ;; low level; usage deprecated
  empty-binding-set
  binding-set-empty?
  make-binding-set
  binding-set-ref/default
  binding-set-ref			; deprecated
  ;; sequential update
  binding-set-insert
  binding-set-cons		       ; srfi-1::alist-cons compatible
  ;; folding
  binding-set-fold
  ;; setXset
  binding-set-union
  ;; 2nd level
  make-symbolic-environment
  symbol-environment-ref/default
  symbol-environment-ref
  symbol-environment-insert
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 condition? with-exception-handler)
	srfi-34 srfi-35)

(import srfi-1 srfi-13)

(import extras)

(import (prefix llrb-symbol-tree impl:))

(define-type :binding-node: (struct <symbol-binding-node>))

(define empty-binding-set impl:empty-binding-set)

(define binding-set-empty? impl:binding-set-empty?)
(: make-binding-set (#!rest -> :binding-node:))
(define make-binding-set impl:make-binding-set)
(: binding-set-ref/default (:binding-node: symbol * --> *))
(define binding-set-ref/default impl:binding-set-ref/default)
(: binding-set-ref (:binding-node: symbol &optional (procedure () . *) -> *))
(define binding-set-ref impl:binding-set-ref)
(: binding-set-insert (:binding-node: symbol * --> :binding-node:))
(define binding-set-insert impl:binding-set-insert)
(: binding-set-cons (symbol * :binding-node: --> :binding-node:))
(define binding-set-cons impl:binding-set-cons)
(define binding-set-fold impl:binding-set-fold)
(define binding-set-union impl:binding-set-union)

(define %empty-binding-set (empty-binding-set))
(define-inline (%binding-set-ref/default envt k default) (binding-set-ref/default envt k default))

(define-inline (binding-set-update nodeset k update dflt) (impl:binding-set-update nodeset k update dflt))

(include "../mechanism/structures/symbolic-environment.scm")

)
