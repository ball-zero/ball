(declare
 (unit lalrdrv)
 (usual-integrations)
 (strict-types)
 (disable-interrupts)			; loops check interrupts
 )

(module
 lalrdrv
 (
  lr-driver
  glr-driver

  make-source-location
  source-location-input source-location-line source-location-column
  source-location-offset source-location-length
  combine-locations

  make-lexical-token lexical-token-category lexical-token-source lexical-token-value
  )

 (import scheme)
 (import chicken)

 (import (prefix atomic c:))

 (include "typedefs.scm")

 (define-type :lexerp: (-> (or symbol (struct lexical-token))))

 (: make-lexical-token (symbol
			(or boolean string (struct source-location))
			*
			--> (struct lexical-token)))
 (: lexical-token-category ((struct lexical-token) --> symbol))
 (: lexical-token-source ((struct lexical-token) --> (or boolean string (struct source-location))))
 (: lexical-token-value ((struct lexical-token) --> *))

 (: combine-locations ((struct source-location) (struct source-location)
		       --> (struct source-location)))

 (: lr-driver (vector vector vector -> (procedure (:lexerp: procedure) *)))
 (: glr-driver (vector vector vector -> (procedure (:lexerp: procedure) *)))

 (define-inline (chicken-check-interrupts!) (c:chicken-check-interrupts/rarely!))

 (include "../mechanism/notation/Lalr/lalr-driver.scm")

 (define-syntax lalr-parser
   (lambda (x r c)
     (apply gen-lalr-parser (cdr x))))

 )
