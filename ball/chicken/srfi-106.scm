;; (C) 2018 Jörg F. Wittenberger

;; Some of SRFI-106 at least.

(declare
 (unit srfi-106)
 (disable-interrupts)
 )

(module
 srfi-106
 (
  ;; components of a srfi-106 compatible interface
  socket? socket-accept socket-close socket-shutdown
  socket-input-port socket-output-port call-with-socket
  ;; unix-sockets; no longer compatible with Felix's version.
  socket-send-file socket-receive-file
  unix-connect
  unix-listen
  unix-socket-pair unix-socket-pair*
  unix-accept
  unix-close
  )

(import scheme (except chicken errno) srfi-1)
(import (only srfi-18 thread-wait-for-i/o!))
(import (except posix open-input-file* open-output-file* socket?))
(import ports)

(cond-expand
 (chicken
  (define (debug l v)
    (display
     (format "~a ~a: ~a\n" (current-process-id) l v)
     (current-error-port))
    v))
 (else
  (define (debug l v) v)))

(import foreign)
#>
#include <errno.h>
<#

;; Sadly core Chicken does blocking i/o on fd ports.
(define fd-read (foreign-lambda int "read" int scheme-pointer int))
(define fd-write (foreign-lambda int "write" int scheme-pointer int))
(define close (foreign-lambda int "close" int))
(define strerror (foreign-lambda c-string "strerror" int))
(define-foreign-variable errno int)
(define-foreign-variable EWOULDBLOCK int)
(define-constant +buffer-size+ 1024)

;; FIXME: das sollte besser über das normale poll gehen, aber erstmal egal.
(define select-read
  (foreign-lambda* int ((int fd))
    "fd_set in;
     struct timeval tm;
     FD_ZERO(&in);
     FD_SET(fd, &in);
     tm.tv_sec = tm.tv_usec = 0;
     if(select(fd + 1, &in, NULL, NULL, &tm) == -1) return(-1);
     else return(FD_ISSET(fd, &in) ? 1 : 0);") )

(define (basic-io-error loc msg . args)
  (signal
   (make-composite-condition
    (make-property-condition 'exn 'message (string-append msg " - " (strerror errno)) 'location loc 'arguments args)
    (make-property-condition 'unix 'errno errno) ) ) )

(define (make-nonblocking fd) (##sys#file-nonblocking! fd))

(: open-input-file* (fixnum &optional procedure -> input-port))
(define (open-input-file* fd . on-close)
  (unless (make-nonblocking fd)
	  (basic-io-error '*open-input-file* "can not make fd nonblocking") )
  (let* ((buf (make-string +buffer-size+))
	 (data (vector fd #f #f))
	 (buflen 0)
	 (bufindex 0)
	 (in (make-input-port
	      (lambda ()
		(when
		 (and (fx>= bufindex buflen) (fx>= bufindex 0))
		 (let ((n (let loop ()
			    (if (eq? -1 fd)
				-1 #;(if #f
				    (basic-io-error '*open-input-file* "fd closed while reading" fd)
				    -1)
				(let ((n (fd-read fd buf +buffer-size+)))
				  (if (eq? -1 n)
				      (cond
				       ((eq? errno (foreign-value "EINTR" int))
					(##sys#dispatch-interrupt loop))
				       ((eq? errno EWOULDBLOCK)
					(begin
					  (thread-wait-for-i/o! fd #:input)
					  (loop) ) )
				       ((eq? errno (foreign-value "EIO" int)) -1)
				       ((eq? errno (foreign-value "ECONNRESET" int)) -1) ;; maybe we actually better raise the exception?
				       (else (debug 'ReadStuck errno) (basic-io-error '*open-input-file* "can not read from fd" fd)))
				      n))))))
		   (set! buflen n)
		   (set! bufindex 0) ) )
		(if (fx>= bufindex buflen)
		    #!eof
		    (let ((c (##core#inline "C_subchar" buf bufindex)))
		      (set! bufindex (fx+ bufindex 1))
		      c) ) )
	      (lambda ()
		(or (fx< bufindex buflen)
		    (let ((f (select-read fd)))
		      (when (eq? f -1)
			    (basic-io-error '*open-input-file* "can not check fd for input" fd) )
		      (eq? f 1) ) ) )
	      (lambda ()
		(if (eq? fd -1) (basic-io-error 'open-input-file* "input port already closed"))
		(let ((fd1 fd))
		  (set! fd -1)
		  (if (eq? -1 (if (pair? on-close) ((car on-close) fd1) (close fd1)))
		      (basic-io-error '*open-input-file* "can not close input port" fd1))) ) )))
    (##sys#setslot in 3 "(fd)")
    (##sys#setslot in 7 'fd)
    (##sys#set-port-data! in data)
    in ) )

(define ##magic#open-input-file* open-input-file*)

(: open-output-file* (fixnum &optional procedure -> output-port))
(define (open-output-file* fd . on-close)
  (unless (make-nonblocking fd)
	  (basic-io-error '*open-output-file* "can not create unix socket ports") )
  (let ((data (vector fd #f #f))
	(out
	 (make-output-port
	  (lambda (s)
	    (let ((len (##sys#size s)))
	      (let loop ()
		(if (eq? -1 fd)
		    (basic-io-error '*open-output-file* "fd closed while writing" fd)
		    (let ((n (fd-write fd s len)))
		      (cond ((eq? -1 n)
			     (cond
			      ((eq? errno (foreign-value "EINTR" int))
			       (##sys#dispatch-interrupt loop))
			      ((eq? errno EWOULDBLOCK)
			       (thread-wait-for-i/o! fd #:output)
			       (loop))
			      ;;((eq? errno (foreign-value "EIO" int)) #f) ;; exit loop
			      ;;((eq? errno (foreign-value "EPIPE" int)) #f)
			      (else (basic-io-error '*open-output-file* "can not write to fd" fd s))))
			    ((fx< n len)
			     (set! s (substring s n len))
			     (set! len (##sys#size s))
			     (loop))))))))
	  (lambda ()
	    (if (eq? fd -1) (basic-io-error '*open-output-file* "output port already closed"))
	    (let ((fd1 fd))
	      (set! fd -1)
	      (if (eq? -1 (if (pair? on-close) ((car on-close) fd1) (close fd1)))
		  (basic-io-error '*open-output-file* "can not close fd output port" fd1))) ) ) ) )
    (##sys#setslot out 3 "(fd)")
    (##sys#setslot out 7 'fd)
    (##sys#set-port-data! out data)
    out ) )

(define ##magic#open-output-file* open-output-file*)


;; Stolen from unix-sockets, because the latter has a huge dependency list.
#>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#ifdef _WIN32
#error "win32 still not supported; see https://github.com/microsoft/WSL/issues/4240"
# include <winsock2.h>
# include <ws2tcpip.h>
// # include <afunix.h>

/*
 * MinGW does not have sockaddr_un (yet)
 */

# ifndef UNIX_PATH_MAX
#  define UNIX_PATH_MAX 108
struct sockaddr_un {
  ADDRESS_FAMILY sun_family;
  char sun_path[UNIX_PATH_MAX];
};
# endif

/*
# ifndef SHUT_RDWR
#  define SHUT_RD   0x00
#  define SHUT_WR   0x01
#  define SHUT_RDWR 0x02
# endif
*/

static WSADATA wsa;
static int net_startup()
{
 int rc = WSAStartup( MAKEWORD(2,2) , &wsa );
 // fprintf(stderr, "WSAStartup returned version %x\n", wsa.wVersion);
 if(rc == 0) return 0;
 exit(1);
}

#else
static int net_startup() { return 0; }
#include <sys/socket.h>

#ifndef __USE_MISC
/* dancing around changes in the c lib: defining _DEFAULT_SOURCE is
supposed to define __USE_MISC too, as is _BSD_SOURCE but both don't.
We need it for SUN_LEN. */

// #define _DEFAULT_SOURCE 1
// #define _BSD_SOURCE
#define __USE_MISC 1
#include <sys/un.h>
#endif

#endif

#include <stddef.h>
static socklen_t set_abstract_socket_name(struct sockaddr_un *socket_name, const char *filename)
{
  size_t fn_length = strlen(filename), off=1;
  const char prefix[] = "FIXchange";
  const char suffix[] = "FIXMESHOULDbechanged";
  size_t total = sizeof(socket_name->sun_path)-1;
  memset(socket_name->sun_path, (42 + 23), sizeof(socket_name->sun_path));
  socket_name->sun_path[0] = '\0'; /* make it an abstract socket */
  strncpy(socket_name->sun_path+off, prefix, total);
  total -= sizeof(prefix);
  off += sizeof(prefix);
  strncpy(socket_name->sun_path+off, filename, total);
  total -= fn_length;
  off += fn_length;
  strncpy(socket_name->sun_path+off, suffix, total);
  off += sizeof(suffix);
  off += offsetof(struct sockaddr_un, sun_path);
  // return off < sizeof(socket_name->sun_path) ? off : sizeof(socket_name->sun_path);
  return sizeof(struct sockaddr_un);
}

#if defined(__ANDROID__) || defined(_WIN32)
#include <stddef.h>
static socklen_t set_socket_name(struct sockaddr_un *socket_name, const char *filename)
{
  return set_abstract_socket_name(socket_name, filename);
}
#else
static socklen_t set_socket_name(struct sockaddr_un *socket_name, const char *filename)
{
  strncpy (socket_name->sun_path, filename, sizeof (socket_name->sun_path));
  socket_name->sun_path[sizeof (socket_name->sun_path) - 1] = '\0';
  return SUN_LEN(socket_name);
}
#endif

#include <sys/time.h>

static int
create_socket_1(const char *filename, int backlog, int abstract)
{
  int sock;
  struct sockaddr_un socket_name;
  socklen_t size;

  /* Create the socket. */
  sock = socket (PF_UNIX, SOCK_STREAM, 0);

  if (sock < 0) return -1;

  /* Bind a name to the socket. */
  socket_name.sun_family = AF_UNIX;

  size = abstract ? set_abstract_socket_name(&socket_name, filename) : set_socket_name(&socket_name, filename);

  if(bind (sock, (struct sockaddr *) &socket_name, size) < 0) goto err;

  if(listen(sock, backlog) < 0) goto err;

  return sock;
err:
  close(sock);
  return -1;
}

static int
create_socket(const char *filename, int backlog){
  return create_socket_1(filename, backlog, 0);
}

static int
create_abstract_socket(const char *filename, int backlog){
  return create_socket_1(filename, backlog, 1);
}

static int
connect_to_server(const char *filename, int abstract)
{
  int sock;
  struct sockaddr_un socket_name;
  socklen_t size;

  /* Create the socket. */
  sock = socket (PF_UNIX, SOCK_STREAM, 0);

  if (sock < 0) return -1;

  socket_name.sun_family = AF_UNIX;
#if defined(__ANDROID__) || defined(_WIN32)
  abstract = 1; /* ignore argument */
#endif
  size = abstract? set_abstract_socket_name(&socket_name, filename) : set_socket_name(&socket_name, filename);

  /* Connect to the server. */
  if (connect(sock, (struct sockaddr *) &socket_name, size) < 0) {
    close(sock);
    return -1;
  }

  return sock;
}


static int
accept_connection(int sock, char *filename)
{
  int s2;
  struct sockaddr_un socket_name;
  socklen_t size;

  socket_name.sun_family = AF_UNIX;
  size = set_socket_name(&socket_name, filename);
  s2 = accept(sock, (struct sockaddr *)&socket_name, &size);

  return s2;
}

#if defined(_WIN32)
static int sendfd(int sock, int fd) { errno = EOPNOTSUPP; return -1; }
static int recvfd(int sock) { errno = EOPNOTSUPP; return -1; };

/* socketpair compatible, select(2)able ffile descriptors for win32 */

#include <pthread.h>

static pthread_t socketpair_server_id;
static int socketpair_create = -1;
static const char socketpair_server_name[] = "socketpair service";

static void *socketpair_server(void *arg)
{
  int serve = (int) (size_t) arg;
  if( serve < 0 ) {
    socketpair_create = -101;
    return NULL;
  }
  for(;;) {
    int fd = accept(serve, NULL, 0);
    if( fd != -1 ) write(fd, &fd, sizeof(fd));
  }
  return NULL;
}

static int init_socketpair_server_1(void *arg)
{
  int res = net_startup();
  int serve = create_abstract_socket(socketpair_server_name, 0);
  if( serve < 0 ) {
    socketpair_create = -101;
    return -1;
  }
  res = pthread_create(&socketpair_server_id, NULL, socketpair_server, (void*) (size_t) serve);
  if( res != 0 ) {
    socketpair_create = -102;
    perror("pthread_create");
    return res;
  }
  if( pthread_detach(socketpair_server_id) != 0 ) {
    socketpair_create = -103;
    perror("pthread_detach");
    return -1;
  }
  if( socketpair_create == -2 ) socketpair_create = 0;
  return socketpair_create;  /* success */
}

static int init_socketpair_server()
{
  if( socketpair_create == -1 ) {
    socketpair_create = -2;
    return init_socketpair_server_1(NULL);
  } else {
    return socketpair_create < 0;
  }
}

// static int create_socket_pair(___out int *s2) { errno = EOPNOTSUPP; return -1; }
static int create_socket_pair(___out int *s2)
{
  int s1;
  if( init_socketpair_server() ) {
    // fprintf(stderr, "Initialization failed %d\n", socketpair_create);
    errno = EOPNOTSUPP;
    return -1;
  }
  s1 = connect_to_server(socketpair_server_name, 1);
  if ( s1 < 0 ) return -1;
  if( read( s1, s2, sizeof(s1)) != sizeof(s1) ) {
    // fprintf(stderr, "Could not read second fd %s\n", strerror(errno));
    close(s1);
    return -1;
  }
  return s1;
}

int simple_socketpair(___out int s[2])
{
  s[0] = create_socket_pair(s+1);
  return s[0] < 0 ? -1 : 0;
}

#else
static int
create_socket_pair(___out int *s2)
{
  int s1, rc;
  int pair[2];
  rc = socketpair(PF_UNIX, SOCK_STREAM, 0, pair);
  if (rc < 0) return -1;
  s1 = pair[0];
  *s2 = pair[1];
  return s1;
}

int simple_socketpair(___out int s[2])
{
  s[0] = create_socket_pair(s+1);
  return s[0] < 0 ? -1 : 0;
}
static int
sendfd(int sock, int fd)
{
  struct msghdr hdr;
  struct iovec data;

  char cmsgbuf[CMSG_SPACE(sizeof(int))];

  char dummy = '*';
  data.iov_base = &dummy;
  data.iov_len = sizeof(dummy);

  memset(&hdr, 0, sizeof(hdr));
  hdr.msg_name = NULL;
  hdr.msg_namelen = 0;
  hdr.msg_iov = &data;
  hdr.msg_iovlen = 1;
  hdr.msg_flags = 0;

  hdr.msg_control = cmsgbuf;
  hdr.msg_controllen = CMSG_LEN(sizeof(int));

  struct cmsghdr* cmsg = CMSG_FIRSTHDR(&hdr);
  cmsg->cmsg_len   = CMSG_LEN(sizeof(int));
  cmsg->cmsg_level = SOL_SOCKET;
  cmsg->cmsg_type  = SCM_RIGHTS;

  *(int*)CMSG_DATA(cmsg) = fd;

  return sendmsg(sock, &hdr, 0);
}

static int
recvfd(int sock)
{
  struct msghdr msg;
  struct iovec iov;
  char buf[CMSG_SPACE(sizeof(int))];

  iov.iov_base = buf;
  iov.iov_len = 1;

  msg.msg_iov = &iov;
  msg.msg_iovlen = 1;
  msg.msg_control = buf;
  msg.msg_controllen = sizeof(buf);
  msg.msg_name = NULL;
  msg.msg_namelen = 0;

  if (recvmsg(sock, &msg, 0) == -1)
    return -1;

  {
  int result_fd = -1;
  struct cmsghdr *header;

  for (header = CMSG_FIRSTHDR(&msg); header != NULL; header = CMSG_NXTHDR(&msg, header)) {
    if (header->cmsg_level == SOL_SOCKET && header->cmsg_type == SCM_RIGHTS) {
      int i, count = (header->cmsg_len - ((unsigned char *)CMSG_DATA(header) - (unsigned char *)header)) / sizeof(int);
      for (i = 0; i < count; ++i) {
        int fd = ((int *)CMSG_DATA(header))[i];
        if (result_fd == -1) {
          result_fd = fd;
        } else {
          close(fd);
          fprintf(stderr, "recvfd bad message from socket held additional file descriptor\n");
        }
      }
    }
  }

  return result_fd;
  }
}
#endif

<#

(define (unix-error loc msg . args)
  (signal
   (make-composite-condition
    (make-property-condition 'exn 'message (string-append msg " - " (strerror errno)) 'location loc 'arguments args)
    (make-property-condition 'unix 'errno errno) ) ) )

(define-foreign-variable SHUT_RD int)
(define-foreign-variable SHUT_WR int)

(define shutdown (foreign-lambda int "shutdown" int int))

(define-record socket kind fd state)

(: socket-close ((struct socket) -> undefined))
(define (%socket-close s)
  (let ((state (socket-state s)))
    (unless
     (eq? state 0)
     (socket-state-set! s 0) ;; TBD: maybe merge with `file-descriptor`
     (if (= (bitwise-and state 4) 4)
	 (if (eq? (close (socket-fd s)) -1)
	     (basic-io-error 'socket-close "can not close" (socket-fd s)))))))

(define (socket-close s)
  (cond
   ((##sys#structure? s 'unix-listener) (unix-close s))
   (else (%socket-close s))))

(: socket-shutdown ((struct socket) fixnum -> undefined))
(define (socket-shutdown s how)
  (let ((state (socket-state s)))
    (case how
      ((SHUT_RD)
       (when (= (bitwise-and state 1) 1)
	     (shutdown (socket-fd s) SHUT_RD)
	     (set! state (bitwise-xor state 1))))
      ((SHUT_WR)
       (when (= (bitwise-and state 2) 2)
	     (shutdown (socket-fd s) SHUT_WR)
	     (set! state (bitwise-xor state 2)))))
    (if (eq? state 4)
	(begin
	  (socket-state-set! s 0)
	  (if (eq? (close (socket-fd s)) -1)
	      (basic-io-error 'socket-shutdown "can not close" (socket-fd s))))
	(socket-state-set! s state))))

(define (socket-closeside s how)
  (let ((state (socket-state s)))
    (case how
      ((SHUT_RD)
       (when (= (bitwise-and state 1) 1)
	     (set! state (bitwise-xor state 1))))
      ((SHUT_WR)
       (when (= (bitwise-and state 2) 2)
	     (set! state (bitwise-xor state 2)))))
    (if (eq? state 4)
	(begin
	  (socket-state-set! s 0)
	  (if (eq? (close (socket-fd s)) -1)
	      (basic-io-error 'socket-closeside "can not close" (socket-fd s))))
	(socket-state-set! s state))))

(define (socket-send-file s fd)
  (or (and (socket? s) (eq? (socket-kind s) 'unix))
      (error "socket-send-file requires a unix domain socket"))
  (let ((fd fd #;(if (file-descriptor? fd) (file-descriptor-fd fd) fd)))
    (let loop ()
      (let ((result ((foreign-lambda int "sendfd" int int) (socket-fd s) fd)))
	(if (negative? result)
	    (cond
	     ((eq? errno (foreign-value "EINTR" int))
	      (##sys#dispatch-interrupt loop))
	     ((eq? errno EWOULDBLOCK)
	      (thread-wait-for-i/o! (socket-fd s) #:output)
	      (loop))
	     (else (unix-error 'socket-send-file "failed" errno)))
	    result)))))

(define (socket-receive-file s)
  (or (and (socket? s) (eq? (socket-kind s) 'unix))
      (error "socket-receive-file requires a unix domain socket"))
  (let loop ()
    (let ((result ((foreign-lambda int "recvfd" int) (socket-fd s))))
      (if (negative? result)
	  (cond
	   ((eq? errno (foreign-value "EINTR" int))
	    (##sys#dispatch-interrupt loop))
	   ((eq? errno EWOULDBLOCK)
	    (thread-wait-for-i/o! (socket-fd s) #:input)
	    (loop))
	   (else (unix-error 'socket-receive-file "failed" errno)))
	  result))))

(define make-socket
  (let ((%make-socket make-socket))
    (lambda (kind fd)
      (let ((result (%make-socket kind fd 7)))
	(set-finalizer! result socket-close)
	result))))

(define (unix-connect filename
		      #!optional (fail (lambda (errno)
					 (unix-error 'unix-connect "can not connect" filename))))
  (let ((n ((foreign-lambda int "connect_to_server" c-string bool) filename #f)))
    (if (negative? n)
	(fail errno)
	(make-socket 'unix n) )))

(: unix-socket-pair (-> (struct socket) (struct socket)))
(define (unix-socket-pair)
  (let-location
   ((b int))
   (let ((a ((foreign-lambda int "create_socket_pair" (nonnull-c-pointer int))
	     (location b))))
     (if (negative? a)
	 (unix-error 'unix-socket-pair "socketpair(2) failed" errno)
	 (values (make-socket 'unix a) (make-socket 'unix b))))))

(: unix-socket-pair* (-> fixnum fixnum))
(define (unix-socket-pair*)
  (let-location
   ((b int))
   (let ((a ((foreign-lambda int "create_socket_pair" (nonnull-c-pointer int))
	     (location b))))
     (if (negative? a)
	 (unix-error 'unix-socket-pair* "socketpair(2) failed" errno)
	 (values a b)))))

(: socket-input-port ((struct socket) -> input-port))
(define (socket-input-port s)
  (open-input-file* (socket-fd s) (lambda (n) (socket-closeside s SHUT_RD))))

(: socket-output-port ((struct socket) -> output-port))
(define (socket-output-port s)
  (open-output-file* (socket-fd s) (lambda (n) (socket-closeside s SHUT_WR))))

(: call-with-socket ((struct socket) (procedure ((struct socket)) . *) -> . *))
(define (call-with-socket socket proc)
  (handle-exceptions
   ex (begin (socket-close socket) (error ex))
   (receive
    results (proc socket)
    (socket-close socket)
    (apply values results))))

#;(define (unix-connect* filename) ;; obsolete
  (let ((n ((foreign-lambda int "connect_to_server" c-string) filename)))
    (if (negative? n)
	(unix-error 'unix-connect "can not connect" filename)
	(let ((closed (let ((opened 2))
			(lambda (n)
			  (set! opened (sub1 opened))
			  (if (= opened 0) (close n) 0)))))
	  (values
	   (open-input-file* n (lambda (n) (shutdown n SHUT_RD) (closed n)))
	   (open-output-file* n (lambda (n) (shutdown n SHUT_WR) (closed n))))) )))

(define (unix-listen filename #!optional (backlog 10))
  (let ((n ((foreign-lambda int "create_socket" c-string int) filename backlog)))
    (if (negative? n)
	(unix-error 'unix-listen "can not create socket" filename)
	(let ((result (##sys#make-structure 'unix-listener n filename)))
	  ;; FIXME: we ought to be sure that THIS process created the socket!
	  (on-exit (lambda () (if (file-exists? filename) (delete-file filename))))
	  result) ) ) )

#;(define (unix-accept* listener)	;; obsolete
  (##sys#check-structure listener 'unix-listener 'unix-accept)
  (let ((fd (##sys#slot listener 1)))
    (thread-wait-for-i/o! fd #:input)
    (let ((fd ((foreign-lambda int "accept_connection" int c-string)
	       fd (##sys#slot listener 2))))
      (when (negative? fd)
	    (unix-error 'unix-accept "could not accept from listener" listener) )
      (let ((closer (let ((opened 2))
		      (lambda (n)
			(set! opened (sub1 opened))
			(if (= opened 0) (close n) 0)))))
	(values
	 (open-input-file* fd (lambda (fd) (shutdown fd SHUT_RD) (closer fd)))
	 (open-output-file* fd (lambda (fd) (shutdown fd SHUT_WR) (closer fd))))) ) ) )

(define (unix-accept listener)
  (##sys#check-structure listener 'unix-listener 'unix-accept)
  (let ((fd (##sys#slot listener 1)))
    (unless (make-nonblocking fd)
	    (basic-io-error 'unix-accept "can not make accept socket unblocking") )
    (let loop ()
      (let ((n ((foreign-lambda int "accept_connection" int c-string)
		fd (##sys#slot listener 2))))
	(cond
	 ((not (negative? n)) (make-socket 'unix n))
	 ((eq? errno (foreign-value "EINTR" int))
	  (##sys#dispatch-interrupt loop))
	 ((eq? errno EWOULDBLOCK)
	  (thread-wait-for-i/o! fd #:input)
	  (loop))
	 (else (unix-error 'unix-accept "could not accept from listener" listener))) )) ) )

(: socket-accept (* -> (struct socket)))
(define (socket-accept s)
  (cond
   ((##sys#structure? s 'unix-listener) (unix-accept s))
   (else (error "socket-accept unhandled socket type" s))))

(define (unix-close l)
  (##sys#check-structure l 'unix-listener)
  (let ([s (##sys#slot l 1)])
    (when (fx= -1 (close s))
	  (unix-error 'unix-close "can not close unix socket" l) ) ) )


)
