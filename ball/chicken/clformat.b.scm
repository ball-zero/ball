(declare
 (unit clformat))

(module
 clformat
 (
;  set-symbol-case-conv!
;  set-iobj-case-conv!
;  format:expch
;  format:iteration-bounded
;  format:max-iterations
;  format:floats
;  format:complex-numbers
;  format:radix-pref
  #;format:ascii-non-printable-charnames
;  format:fn-max
;  format:en-max
;  format:unprocessed-arguments-error?
  #;format:version
  #;format:iobj->str
  clformat)

(import scheme chicken)

(: set-symbol-case-conv! ((or false (procedure (symbol) symbol)) -> undefined))
(: set-iobj-case-conv! ((or false (procedure (string) string)) -> undefined))
(: clformat (#!rest -> . *))

(define clformat #f)

 )

(module
 b-clformat
 (%!clformat)
 (import scheme (prefix clformat %:))
 (define (%!clformat p) (set! %:clformat p))
)
