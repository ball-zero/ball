;; (C) 2002, 2008 Jörg F. Wittenberger see http://www.askemos.org

;; chicken module clause for the Askemos tree module

(declare
 (unit tree)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;; promises
 (disable-interrupts) ;; loops check interrupts
 ;;
 (usual-integrations)
 (strict-types)
 )

(module
 tree
 (
  define-transformer
  lambda-transformer
  lambda-process
  ;;
  empty-node-list
  %node-list-cons
  %node-list-append
  make-xml-literal
  xml-literal-value
  make-xml-namespace
  xml-namespace?
  xml-namespace-local
  xml-namespace-uri
  make-xml-attribute
  xml-attribute?
  xml-attribute-name
  xml-attribute-ns
  xml-attribute-value
  make-xml-namespace-declaration
  xml-namespace-declaration?
  make-xml-element
  xml-element?
  xml-element-name
  xml-element-ns
  xml-element-attributes
  xml-element-special
  %node-list-cons
  %xml-element-attributes
  %children
  gi
  dsssl-gi
  ns
  dsssl-ns
  attributes
  children dsssl-children
  make-xml-document
  xml-document?
  xml-document-children
  ;;
  make-specialised-xml-element
  register-xml-element-constructor!
  ;;
  find-namespace
  find-namespace-by-prefix
  find-namespace-by-uri
  xml-split-name
  ;;
  find-first-attribute
  ;;
  namespace-xml-str
  namespace-xml
  namespace-xsl-str
  namespace-xsl
  namespace-dsssl1-str
  namespace-dsssl1
  namespace-dsssl3
  namespace-dsssl2-str
  namespace-dsssl2
  namespace-dsssl-str
  namespace-dsssl
  namespace-sync-str
  namespace-sync
  namespace-rdf-str
  namespace-rdf
  namespace-dc-str
  namespace-dc
  namespace-mind-v0
  namespace-mind-str
  namespace-mind
  namespace-html-str
  namespace-xhtml
  namespace-html
  namespace-soap-envelope-str
  namespace-soap-envelope
  namespace-forms-str
  namespace-forms
  namespace-http-str
  namespace-http
  namespace-dav-str
  namespace-dav
  namespace-xsql-str
  namespace-xsql
  namespace-lout-str
  namespace-lout
  namespace-svg-str
  namespace-svg
  namespace-xlink-str
  namespace-xlink
  ;;
  namespace-coreapi-declaration
  ;;
  named-node-list-names
  named-node-list-symbols
  attribute-string
  dsssl-attribute-string
  required-attribute-string
  attribute-ns
  attribute-string-defaulted
  dsssl-make-element
  dsssl-element-key
  dsssl-comment-key
  dsssl-pi-key
  dsssl-make
  copy-attributes
  match-element?
  literal
  make-xml-literal
  xml-literal?
  xml-literal-value
  make-xml-output
  xml-output?
  make-xml-pi
  xml-pi?
  xml-pi-tag
  xml-pi-data
  make-xml-comment
  xml-comment?
  xml-comment-data
  make-xml-doctype
  ;;
  node-list?
  nodeset?
  attribute-node-list?
  fun:empty-node-list
  node-list
  node-list-first
  node-list-empty?
  node-list-rest
  data
  first-child-gi
  node-list-map
  node-list-map-parallel
  node-list-filter
  node-list-reduce
  node-list-fold-left
  node-list-length
  document-element
  node-list-tail
  node-list-head
  select-elements dsssl-select-elements
  select-elements-ns
  node-list=?
  node-list-contains?
  node-list-difference-with-duplicates
  ;;
  node-list->list
  node-list->vector
  ;;
  xml-walk*
  xml-walk*1
  xml-walk
  xml-drop
  xml-copy
  ;;
  dsssl-read
  symbol<?
  node-constant
  string-value
  number-value
  boolean-value
  node-number-op
  sxml-predifined-entities
  sxml-attributes
  sxml-namespaces-annotations
  sxml-find-namespaces-annotations
  shtml-named-char-id
  shtml-numeric-char-id
  sxml
  ntype-names?
  node-nameof?
  sxp:element?
  sxp:literal?
  sxp:pi?
  sxp:comment?
  sxp:true
  node-typeof?
  ntype?
  ntype-namespace-id?
  sxp:invert
  node-eq?
  node-equal?
  sxp:compare-singleton-node
  sxp:compare
  sxp:equal?
  not-equal?
  sxp:not-equal?
  sxp:lt?
  sxp:<
  sxp:lt-eq?
  sxp:<=
  sxp:gt?
  sxp:>
  sxp:gt-eq?
  sxp:>=
  sxp:or
  sxp:and
  sxp:+
  sxp:-
  sxp:mul
  sxp:div
  sxp:mod
  sxp:union
  node-pos
  sxp:filter
  take-until
  take-after
  map-union
  node-reverse
  node-trace
  select-kids reduce-kids
  sxml:attr-list
  sxml:attribute
  node-attributes
  node-namespaces
  sxp:child
  sxml:parent
  node-self
  node-join
  node-reduce
  node-or
  node-closure
  node-descendant-or-self
  node-parent
  sxpath-special-symbols
  sxpath*
  sxpath
  sxml:string-value
  sxml:string
  sxml:boolean
  sxml:number
  sxml:equal?
  sxml:not-equal?
  sxml:relational-cmp
  ;;
  make-soap-sender-fault
  make-soap-receiver-fault
  make-soap-sender-error
  make-soap-receiver-error
  )

(import (except scheme force delay
		vector-fill! vector->list list->vector)
	srfi-1 srfi-13 srfi-43 srfi-45 srfi-69
	(except chicken add1 sub1 condition? promise?
		with-exception-handler vector-copy!)
	ports atomic parallel util srfi-110)
(import extras data-structures)
(import (prefix srfi-43 srfi:))

(include "typedefs.scm")

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define-syntax empty-node-list
  (syntax-rules ()
    ((_) '())))

(define-syntax %node-list-cons
  (syntax-rules ()
    ((_ h t) (cons h t))))

(define-syntax %node-list-append
  (syntax-rules ()
    ((_ h) h)
    ((_ h t ...) (cons* h t ...))))

(define-syntax make-xml-literal
  (syntax-rules ()
    ((_ data) data)))

(define-syntax xml-literal-value
  (syntax-rules ()
    ((_ lit) lit)))

;;* Data Definition

;; make-xml-attribute is to be used in a named node list (see groves),
;; e.g., the attributes of an element.  Slighly different to sdc and
;; the latter should be updated.

;; 2002-01-29 converting the code
;(define (make-xml-attribute name ns value)
;  (cons  name (vector 'xml-attribute-def ns value)))
;(define (xml-attribute-def-value x) (vector-ref x 2))
;(define (xml-attribute-def-type x) (vector-ref x 1))
;(define (xml-attribute? node)
;  (and (pair? node)
;       (symbol? (car node))
;       (vector? (cdr node))
;       (eq? (vector-ref (cdr node) 0) 'xml-attribute-def)))

;(define (make-xml-attribute name ns value)
;  (vector 'xml-attribute name ns value))

(: make-xml-attribute (symbol (or false symbol) string --> :xml-attribute:))
(: xml-attribute? (* --> boolean : :xml-attribute:))
(: xml-attribute-name (:xml-attribute: --> symbol))
(: xml-attribute-ns (:xml-attribute: --> (or false symbol)))
(: xml-attribute-value (:xml-attribute: --> string))
(define-record-type <xml-attribute>
  (make-xml-attribute name ns value)
  xml-attribute?
  (name xml-attribute-name)
  (ns xml-attribute-ns)
  (value xml-attribute-value))

(define-record-printer (<xml-attribute> x out)
  (format out "#<xml-attribute name=\"~a\" nmsp=\"~a\" value=\"~a\">"
	  (xml-attribute-name x) (xml-attribute-ns x) (xml-attribute-value x)))

;;* Namespaces

(: make-xml-namespace
   ((or false symbol) (or false symbol) --> :xml-namespace:))
(: xml-namespace? (* --> boolean : :xml-namespace:))
(: xml-namespace-local (:xml-namespace: --> (or false symbol)))
(: xml-namespace-uri (:xml-namespace: --> symbol))
(define-record-type <xml-namespace>
  (make-xml-namespace local uri)
  xml-namespace?
  (local xml-namespace-local)
  (uri xml-namespace-uri))

;; Beware: The data of xml-element definition does NOT mirror the full
;; DOM structure for a reason.  DOM defines the access to the tree
;; structure only, obvoiusly knowing that this could be implemented as
;; static data structure as well as dynamically (lazy) upon tree
;; traversal.  I do intend the latter for memory and performance
;; reasons.  Hence we see only the document axis (see XPath), which
;; are content (not context~) related.

;(define (make-xml-element gi ns attributes content)
;  (vector 'xml-element gi ns attributes content))

(: make-xml-element
   (symbol
    (or false symbol)
    (or (procedure (&rest) *) (list-of :xml-attribute:))
    :xml-nodelist:
    --> :xml-element:))
(: xml-element? (* --> boolean : :xml-element:))
(: xml-element-name (:xml-element: --> symbol))
(: xml-element-ns (:xml-element: --> (or false symbol)))
(: %xml-element-attributes
   (:xml-element:
    --> (or (procedure (&rest) *) (list-of :xml-attribute:))))
(: %children (:xml-element: --> :xml-nodelist:))
(define-record-type <xml-element>
  (make-xml-element name ns attributes content)
  xml-element?
  (name xml-element-name)
  (ns xml-element-ns)
  (attributes %xml-element-attributes)
  (content %children))

(define (gi obj) (and (xml-element? obj) (xml-element-name obj)))
(define (ns obj) (and (xml-element? obj) (xml-element-ns obj)))

(define (dsssl-gi obj)
  (if (xml-element? obj) (xml-element-name obj)
      (and-let* ((obj (document-element obj))) (xml-element-name obj))))
(define (dsssl-ns obj)
  (if (xml-element? obj) (xml-element-ns obj)
      (and-let* ((obj (document-element obj))) (xml-element-ns obj))))
(define (dsssl-children obj)
  (cond
   ((not obj) (empty-node-list))
   (else (children obj))))

;; TODO: rename xml-element-attributes in all code into attributes.

(: xml-element-attributes (:xml-element: --> (list-of :xml-attribute:)))
(define (xml-element-attributes element)
  (let ((v (%xml-element-attributes element)))
    (if (procedure? v) (v) v)))

(define (attributes node)
  (let ((node (node-list-first node)))
    (and (xml-element? node) (xml-element-attributes node))))

(: xml-element-special (* --> (or false (procedure (&rest) *))))
(define (xml-element-special obj)
  (and (xml-element? obj)
       (let ((v (%xml-element-attributes obj)))
	 (and (procedure? v) v))))

;(define (gi node) (vector-ref node 1))

;; lt. DSSSL gi has an optional node list argument defaulting to
;; current-node.  This is too expensive for the kernel for now.  To
;; comply we would also need to check that node is actually a
;; singleton node list.

;(define (gi node)
;  (let ((node (node-list-first node)))
;    (and (xml-element? node) (name node))))

; (define (make-xml-literal data) data)

(include "../mechanism/syntax/petrofsky-extract.scm")
(include "../mechanism/syntax/keyword-syntax.scm")

(include "../mechanism/structures/tree-syntax.scm")
(include "../mechanism/structures/tree.scm")

  (define (xml-walk*1 place
		      _message_50
		      _root-node0_51
		      _variables_52
		      _namespaces0_53
		      _ancestors0_54
		      _self-node_55
		      _nl_56
		      _mode-choice0_57
		      _proc-chain_58)
    
    (let _cl_100 ((_nl_101 _nl_56) (_chain_102 _proc-chain_58))
      (if (null? _nl_101)
	  _nl_101
	  (if (pair? _nl_101)
	      (let ((_first_103 (_cl_100 (car _nl_101) _chain_102)))
		(if (let ((_x_104 (not _first_103)))
		      (if _x_104 _x_104 (null? _first_103)))
		    (_cl_100 (cdr _nl_101) _chain_102)
		    (cons _first_103 (_cl_100 (cdr _nl_101) _chain_102))))
	      (if (null? _chain_102)
		  _nl_101
		  (if ((caar _chain_102) _nl_101 _root-node0_51 _namespaces0_53)
		      ((cdar _chain_102)
		       place
		       _message_50
		       _root-node0_51
		       _variables_52
		       _namespaces0_53
		       _ancestors0_54
		       _self-node_55
		       _nl_101
		       _mode-choice0_57
		       (lambda (_place_50
				_message_51
				_root-node_52
				_variables_53
				_namespaces_54
				_ancestors_55
				_self-node_56
				_nl_57
				_mode-choice_58)
			 (xml-walk*1 _place_50
				     _message_51
				     _root-node_52
				     _variables_53
				     _namespaces_54
				     _ancestors_55
				     _self-node_56
				     _nl_57
				     _mode-choice_58
				     _proc-chain_58)))
		      (_cl_100 _nl_101 (cdr _chain_102))))))))
)

(import (prefix tree m:))
(define make-xml-namespace m:make-xml-namespace)
(define xml-namespace? m:xml-namespace?)
(define xml-namespace-local m:xml-namespace-local)
(define xml-namespace-uri m:xml-namespace-uri)
(define make-xml-attribute m:make-xml-attribute)
(define xml-attribute? m:xml-attribute?)
(define xml-attribute-name m:xml-attribute-name)
(define xml-attribute-ns m:xml-attribute-ns)
(define xml-attribute-value m:xml-attribute-value)
(define xml-attribute? m:xml-attribute?)
(define make-xml-namespace-declaration m:make-xml-namespace-declaration)
(define xml-namespace-declaration? m:xml-namespace-declaration?)
(define make-xml-element m:make-xml-element)
(define xml-element? m:xml-element?)
(define xml-element-name m:xml-element-name)
(define xml-element-ns m:xml-element-ns)
(define xml-element-attributes m:xml-element-attributes)
(define xml-element-special m:xml-element-special)
;(define %node-list-cons m:%node-list-cons)
(define %xml-element-attributes m:%xml-element-attributes)
(define %children m:%children)
(define gi m:gi)
(define dsssl-gi m:dsssl-gi)
(define ns m:ns)
(define dsssl-ns m:dsssl-ns)
(define dsssl-children m:dsssl-children)
(define attributes m:attributes)
(define children m:children)
(define make-xml-document m:make-xml-document)
(define xml-document? m:xml-document?)
(define xml-document-children m:xml-document-children)
;;
(define make-specialised-xml-element m:make-specialised-xml-element)
(define register-xml-element-constructor! m:register-xml-element-constructor!)
;;
(define find-namespace m:find-namespace)
(define find-namespace-by-prefix m:find-namespace-by-prefix)
(define find-namespace-by-uri m:find-namespace-by-uri)
(define xml-split-name m:xml-split-name)
;;
(define find-first-attribute m:find-first-attribute)
;;
(define namespace-xml-str m:namespace-xml-str)
(define namespace-xml m:namespace-xml)
(define namespace-xsl-str m:namespace-xsl-str)
(define namespace-xsl m:namespace-xsl)
(define namespace-dsssl1-str m:namespace-dsssl1-str)
(define namespace-dsssl1 m:namespace-dsssl1)
(define namespace-dsssl2-str m:namespace-dsssl2-str)
(define namespace-dsssl2 m:namespace-dsssl2)
(define namespace-dsssl-str m:namespace-dsssl-str)
(define namespace-dsssl m:namespace-dsssl)
(define namespace-sync-str m:namespace-sync-str)
(define namespace-sync m:namespace-sync)
(define namespace-rdf-str m:namespace-rdf-str)
(define namespace-rdf m:namespace-rdf)
(define namespace-dc-str m:namespace-dc-str)
(define namespace-dc m:namespace-dc)
(define namespace-mind-v0 m:namespace-mind-v0)
(define namespace-mind-str m:namespace-mind-str)
(define namespace-mind m:namespace-mind)
(define namespace-html-str m:namespace-html-str)
(define namespace-xhtml m:namespace-xhtml)
(define namespace-html m:namespace-html)
(define namespace-soap-envelope-str m:namespace-soap-envelope-str)
(define namespace-soap-envelope m:namespace-soap-envelope)
(define namespace-forms-str m:namespace-forms-str)
(define namespace-forms m:namespace-forms)
(define namespace-http-str m:namespace-http-str)
(define namespace-http m:namespace-http)
(define namespace-dav-str m:namespace-dav-str)
(define namespace-dav m:namespace-dav)
(define namespace-xsql-str m:namespace-xsql-str)
(define namespace-xsql m:namespace-xsql)
(define namespace-lout-str m:namespace-lout-str)
(define namespace-lout m:namespace-lout)
(define namespace-svg-str m:namespace-svg-str)
(define namespace-svg m:namespace-svg)
(define namespace-xlink-str m:namespace-xlink-str)
(define namespace-xlink m:namespace-xlink)
;;
(define namespace-coreapi-declaration m:namespace-coreapi-declaration)
;;
(define named-node-list-names m:named-node-list-names)
(define named-node-list-symbols m:named-node-list-symbols)
(define attribute-string m:attribute-string)
(define dsssl-attribute-string m:dsssl-attribute-string)
(define attribute-ns m:attribute-ns)
(define attribute-string-defaulted m:attribute-string-defaulted)
(define dsssl-make-element m:dsssl-make-element)
(define dsssl-element-key m:dsssl-element-key)
(define dsssl-comment-key m:dsssl-comment-key)
(define dsssl-pi-key m:dsssl-pi-key)
(define dsssl-make m:dsssl-make)
(define copy-attributes m:copy-attributes)
(define match-element? m:match-element?)
(define literal m:literal)
;MACRO(define make-xml-literal m:make-xml-literal)
(define xml-literal? m:xml-literal?)
;MACRO(define xml-literal-value m:xml-literal-value)
(define make-xml-output m:make-xml-output)
(define xml-output? m:xml-output?)
(define make-xml-pi m:make-xml-pi)
(define xml-pi? m:xml-pi?)
(define xml-pi-tag m:xml-pi-tag)
(define xml-pi-data m:xml-pi-data)
(define make-xml-comment m:make-xml-comment)
(define xml-comment? m:xml-comment?)
(define xml-comment-data m:xml-comment-data)
(define make-xml-doctype m:make-xml-doctype)
;;
(define node-list? m:node-list?)
(define nodeset? m:nodeset?)
(define attribute-node-list? m:attribute-node-list?)
(define fun:empty-node-list m:fun:empty-node-list)
(define node-list m:node-list)
(define node-list-first m:node-list-first)
(define node-list-empty? m:node-list-empty?)
(define node-list-rest m:node-list-rest)
(define data m:data)
(define first-child-gi m:first-child-gi)
(define node-list-map m:node-list-map)
(define node-list-map-parallel m:node-list-map-parallel)
(define node-list-filter m:node-list-filter)
(define node-list-reduce m:node-list-reduce)
(define node-list-fold-left m:node-list-fold-left)
(define node-list-length m:node-list-length)
(define document-element m:document-element)
(define node-list-tail m:node-list-tail)
(define node-list-head m:node-list-head)
(define select-elements m:dsssl-select-elements)
(define dsssl-select-elements m:dsssl-select-elements)
(define select-elements-ns m:select-elements-ns)
(define node-list=? m:node-list=?)
(define node-list-contains? m:node-list-contains?)
(define node-list-difference-with-duplicates m:node-list-difference-with-duplicates)
;;
(define node-list->vector m:node-list->vector)
(define node-list=? m:node-list=?) ;; NOBODY wants that!!!
;;
(define xml-walk* m:xml-walk*)
(define xml-walk*1 m:xml-walk*1)
(define xml-walk m:xml-walk)
(define xml-drop m:xml-drop)
(define xml-copy m:xml-copy)
;;
(define dsssl-read m:dsssl-read)
(define symbol<? m:symbol<?)
(define node-constant m:node-constant)
(define string-value m:string-value)
(define number-value m:number-value)
(define boolean-value m:boolean-value)
(define node-number-op m:node-number-op)
(define sxml-predifined-entities m:sxml-predifined-entities)
(define sxml-attributes m:sxml-attributes)
(define sxml-namespaces-annotations m:sxml-namespaces-annotations)
(define sxml-find-namespaces-annotations m:sxml-find-namespaces-annotations)
(define shtml-named-char-id m:shtml-named-char-id)
(define shtml-numeric-char-id m:shtml-numeric-char-id)
(define sxml m:sxml)
(define ntype-names? m:ntype-names?)
(define node-nameof? m:node-nameof?)
(define sxp:element? m:sxp:element?)
(define sxp:literal? m:sxp:literal?)
(define sxp:pi? m:sxp:pi?)
(define sxp:comment? m:sxp:comment?)
(define sxp:true m:sxp:true)
(define node-typeof? m:node-typeof?)
(define ntype? m:ntype?)
(define ntype-namespace-id? m:ntype-namespace-id?)
(define sxp:invert m:sxp:invert)
(define node-eq? m:node-eq?)
(define node-equal? m:node-equal?)
(define sxp:compare-singleton-node m:sxp:compare-singleton-node)
(define sxp:compare m:sxp:compare)
(define sxp:equal? m:sxp:equal?)
(define not-equal? m:not-equal?)
(define sxp:not-equal? m:sxp:not-equal?)
(define sxp:lt? m:sxp:lt?)
(define sxp:< m:sxp:<)
(define sxp:lt-eq? m:sxp:lt-eq?)
(define sxp:<= m:sxp:<=)
(define sxp:gt? m:sxp:gt?)
(define sxp:> m:sxp:>)
(define sxp:gt-eq? m:sxp:gt-eq?)
(define sxp:>= m:sxp:>=)
(define sxp:or m:sxp:or)
(define sxp:and m:sxp:and)
(define sxp:+ m:sxp:+)
(define sxp:- m:sxp:-)
(define sxp:mul m:sxp:mul)
(define sxp:div m:sxp:div)
(define sxp:mod m:sxp:mod)
(define sxp:union m:sxp:union)
(define node-pos m:node-pos)
(define sxp:filter m:sxp:filter)
(define take-until m:take-until)
(define take-after m:take-after)
(define map-union m:map-union)
(define node-reverse m:node-reverse)
(define node-trace m:node-trace)
(define select-kids m:select-kids)
(define sxml:attr-list m:sxml:attr-list)
(define sxml:attribute m:sxml:attribute)
(define node-attributes m:node-attributes)
;MACRO??? (define node-namespaces m:node-namespaces)
(define sxp:child m:sxp:child)
(define sxml:parent m:sxml:parent)
(define node-self m:node-self)
(define node-join m:node-join)
(define node-reduce m:node-reduce)
(define node-or m:node-or)
(define node-closure m:node-closure)
(define node-descendant-or-self m:node-descendant-or-self)
(define node-parent m:node-parent)
(define sxpath-special-symbols m:sxpath-special-symbols)
(define sxpath* m:sxpath*)
(define sxpath m:sxpath)
(define sxml:string-value m:sxml:string-value)
(define sxml:string m:sxml:string)
(define sxml:boolean m:sxml:boolean)
(define sxml:number m:sxml:number)
(define sxml:equal? m:sxml:equal?)
(define sxml:not-equal? m:sxml:not-equal?)
(define sxml:relational-cmp m:sxml:relational-cmp)
;;
(define make-soap-sender-fault m:make-soap-sender-fault)
(define make-soap-receiver-fault m:make-soap-receiver-fault)
(define make-soap-sender-error m:make-soap-sender-error)
(define make-soap-receiver-error m:make-soap-receiver-error)
