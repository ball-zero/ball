;; (C) 2013 Jörg F. Wittenberger

;; This file collects top level bindings from various srfi and other
;; low level modules.

;; To be compiled with -O0 (or alike), at least without -block optimisation.

(declare (unit tl-srfi))


(import (prefix srfi-43 srfi-43:))
;;; * Constructors
;; make-vector                     vector
(define vector-unfold srfi-43:vector-unfold)
(define vector-unfold-right srfi-43:vector-unfold-right)
(define vector-copy srfi-43:vector-copy)
(define vector-reverse-copy srfi-43:vector-reverse-copy)
(define vector-append srfi-43:vector-append)
(define vector-concatenate srfi-43:vector-concatenate)
;;; * Predicates
;; vector?
(define vector-empty? srfi-43:vector-empty?)
(define vector= srfi-43:vector=)
;;; * Selectors
;; vector-ref                      vector-length
;;; * Iteration
(define vector-fold srfi-43:vector-fold)
(define vector-fold-right srfi-43:vector-fold-right)
(define vector-map srfi-43:vector-map)
(define srfi:vector-map srfi-43:vector-map)
(define vector-map! srfi-43:vector-map!)
(define vector-for-each srfi-43:vector-for-each)
(define srfi:vector-for-each srfi-43:vector-for-each)
(define vector-count srfi-43:vector-count)
;;; * Searching
(define vector-index srfi-43:vector-index)
(define vector-skip srfi-43:vector-skip)
(define vector-index-right srfi-43:vector-index-right)
(define vector-skip-right srfi-43:vector-skip-right)
(define vector-binary-search srfi-43:vector-binary-search)
(define vector-any srfi-43:vector-any)
(define vector-every srfi-43:vector-every)
;;; * Mutators
 ; vector-set!
(define vector-swap! srfi-43:vector-swap!)
(define vector-fill! srfi-43:vector-fill!)
(define vector-reverse! srfi-43:vector-reverse!)
;(define vector-copy! srfi-43:vector-copy!)
(define vector-reverse-copy! srfi-43:vector-reverse-copy!)
(define vector-reverse! srfi-43:vector-reverse!)
;;; * Conversion
(define vector->list srfi-43:vector->list)
(define srfi:vector->list srfi-43:vector->list)
(define reverse-vector->list srfi-43:reverse-vector->list)
(define list->vector srfi-43:list->vector)
(define srfi:list->vector srfi-43:list->vector)
(define reverse-list->vector srfi-43:reverse-list->vector)
;;; * Additional
(define vector-sort! srfi-43:vector-sort!)

(import (prefix shrdprmtr shrdprmtr:))
(define make-shared-parameter shrdprmtr:make-shared-parameter)


(import (prefix srfi-35 srfi35:))

(define message-condition? srfi35:message-condition?)
(define condition-message srfi35:condition-message)


(import (prefix srfi-45 srfi-45:))

(define force srfi-45:force)

(import (prefix clformat m:))
(define clformat m:clformat)
