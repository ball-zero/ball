(declare
 (unit threadpool)
 ;; absolutely required:
 (disable-interrupts)

 (no-procedure-checks-for-usual-bindings)
 (not standard-bindings symbol->string)

 )

(module
 threadpool
 (threadpool?
  threadpool-name
  ;; begin debug only
  threadpool-max threadpool-max-set!
  threadpool-threads threadpool-threads-set!
  threadpool-queue
  threadpool-start!
  ;; end debug only
  make-threadpool-requesttype
  make-threadpool
  threadpool-order!
  threadpool-order/blocking!
  )

 (import scheme
	 (except chicken add1 condition? with-exception-handler))
 (import (except srfi-18 raise) srfi-34 srfi-35)
 (import (only extras format))
 (import mailbox)

 (include "typedefs.scm")

 (define-record threadpool name max threads queue fail success)

 (define-record-printer (threadpool x out)
   (format out "<threadpool ~a (~a) ~a>" (threadpool-name x)
	   (threadpool-max x) #;(let ((v (threadpool-max x)))
	     (if (observable? v) (value v) v))
	   (threadpool-queue x)))

 (define-record threadpool-requesttype fail success)
 (define-record threadpool-request root proc args)

 (define-record dequeue waiting block count queue)

 (define-record-printer (dequeue x out)
   (format out "<queue ~a (size ~a count ~a~a)>" (dequeue-name x) (dequeue-size x) (dequeue-count x)
	   (if (thread? (mutex-state (dequeue-block x))) " blocking" "")))
  
 (define dequeue-make
   (let ((make-dequeue make-dequeue))
     (lambda (name)
       (make-dequeue (make-condition-variable name) (make-mutex name) 0 (let ((x (list 0))) (cons x x))))))

 (define (dequeue-empty? queue) (null? (cdar (dequeue-queue queue))))
 (define (dequeue-name queue) (condition-variable-name (dequeue-waiting queue)))
 (define (dequeue-size queue) (caar (dequeue-queue queue)))
 
 (define dequeue-await-message!
   (let ((unlocked (make-mutex)))
     (define (await-message! queue)
       (dequeue-count-set! queue (add1 (dequeue-count queue)))
   ;; this is only safe if mutex-unlock! does not switch threads
   ;; until waiting on the condition variable.
       (mutex-unlock! (dequeue-block queue))
       (mutex-unlock! unlocked (dequeue-waiting queue))
       (dequeue-count-set! queue (sub1 (dequeue-count queue))))
     await-message!))

 (define (dequeue-send! queue job)
   (set! job (list job))
   (set-cdr! (cdr (dequeue-queue queue)) job)
   (set-cdr! (dequeue-queue queue) job)
   (condition-variable-signal! (dequeue-waiting queue))
   #t)

 (define (dequeue-send/blocking! queue job)
   (let loop ()
     (if (> (dequeue-count queue) 0)
	 (begin (dequeue-send! queue job) (mutex-unlock! (dequeue-block queue)))
	 (begin (mutex-lock! (dequeue-block queue)) (loop))))
   #t)

 (define (dequeue-receive! queue)
   (if (dequeue-empty? queue)
       (begin
	 (dequeue-await-message! queue)
	 (dequeue-receive! queue))
       (let* ((queue (dequeue-queue queue))
	      (len (caar queue)))
	 (set-car! queue (cdar queue))
	 (let ((x (caar queue))) (set-car! (car queue) (sub1 len)) x))))


 (define make-threadpool-queue dequeue-make)
 (define threadpool-send-message! dequeue-send!)
 (define threadpool-send-message/blocking! dequeue-send/blocking!)
 (define threadpool-receive-message! dequeue-receive!)
 (define threadpool-queue-empty? dequeue-empty?)
 (define threadpool-queue-count dequeue-count)

 (define-values (make-threadpool threadpool-start! threadpool-order! threadpool-order/blocking!)
   (let ((%make-threadpool make-threadpool))

     (define (threadpool-start! pool)
       (let ((mx (threadpool-max pool)))
	 (if (cond
	      ((number? mx) (not (= mx 0)))
	      ((boolean? mx) mx)
	      (else #;(not (= (swap! mx (lambda (v) (if (> v 0) (sub1 v) v))) 0))
		    (error "haywire observable NYI")))
	     (let ((t (make-thread pool-thread-loop (threadpool-name pool))))
	       (thread-specific-set! t pool)
	       (thread-start! t))))
       pool)

     (define (pool-thread-loop)
       (define pool (thread-specific (current-thread)))
       (if (number? (threadpool-max pool))
	   (threadpool-max-set! pool (sub1 (threadpool-max pool))))
       (threadpool-threads-set! pool (cons (current-thread) (threadpool-threads pool)))
       (do ((entry #f) (exn #f))
	   (#f)
	 (guard
	  (ex (else (set! exn (list ex))))
	  (do ()
	      (#f)
	    (if exn
		(if entry
		    (let ((e entry) (r (car exn)))
		      (set! entry #f)
		      (set! exn #f)
		      ((threadpool-fail pool) (threadpool-request-root e) r))
		    (begin
		      (print-error-message exn (current-error-port) "Thread Pool")
		      (format (current-error-port) "thread pool ~s\n" (threadpool-name pool))
		      (print-call-chain (current-error-port) 0 (current-thread)))))
	    (let ((queue (threadpool-queue pool)))
	      (set! entry (threadpool-receive-message! queue))
	      (or (> (threadpool-queue-count queue) 0)
		  (threadpool-start! pool)))
	    (let ((args (threadpool-request-args entry))
		  (root (threadpool-request-root entry)))
	      (if (null? args)
		  ((threadpool-request-proc entry) root)
		  (apply (threadpool-request-proc entry) (cons root args)))
	      (and-let* ((success (threadpool-success pool))) (success root)))))))

     (define (make-threadpool name max type)
       (let ((pool (%make-threadpool
		    name max '() (make-threadpool-queue name)
		    (threadpool-requesttype-fail type) (threadpool-requesttype-success type))))
	 (threadpool-start! pool)))

     (define (threadpool-order! pool root proc args)
       (threadpool-send-message! (threadpool-queue pool) (make-threadpool-request root proc args))
       pool)
     (define (threadpool-order/blocking! pool root proc args)
       (threadpool-send-message/blocking! (threadpool-queue pool) (make-threadpool-request root proc args))
       pool)
     (values make-threadpool threadpool-start! threadpool-order! threadpool-order/blocking!)))

)

(import (prefix threadpool m:))
(define threadpool-start! m:threadpool-start!)
(define make-threadpool-requesttype m:make-threadpool-requesttype)
(define make-threadpool m:make-threadpool)
(define threadpool-order! m:threadpool-order!)
(define threadpool-order/blocking! m:threadpool-order/blocking!)
;; debug
(define threadpool-queue m:threadpool-queue)
(define threadpool-threads m:threadpool-threads)
(define threadpool-threads-set! m:threadpool-threads-set!)
