;; (C) 2013 Jörg F. Wittenberger

;; This file collects top level bindings from various flow control and
;; other low level modules.

;; To be compiled with -O0 (or alike), at least without -block optimisation.

(declare (unit tl-cntrlflw))


(import (prefix atomic a:))
(import foreign)

(define chicken-enable-interrupts! (foreign-lambda void "C_enable_interrupts"))
(define chicken-disable-interrupts! (foreign-lambda void "C_disable_interrupts"))

(define make-internal-pipe a:make-internal-pipe)
(define chicken-check-interrupts! a:chicken-check-interrupts!)
(define tq-entry? a:tq-entry?)
(define entry-successor a:entry-successor)
(define entry-predecessor a:entry-predecessor)
(define entry-time a:entry-time)
(define entry-value a:entry-value)
(define tq-insert! a:tq-insert!)
(define tq-remove! a:tq-remove!)
(define tq-invalidate! a:tq-invalidate!)
(define make-tq a:make-tq)
(define tq? a:tq?)
(define tq-name a:tq-name)
(define tq-queue a:tq-queue)
(define sudo! a:sudo!)
(define close-all-ports-except a:close-all-ports-except)
(define atomic:make-process-stub a:make-process-stub)
(define process-call a:process-call)
(define save-fork a:save-fork)
;; Debug only!
(define running-processes-fold a:running-processes-fold)
(define open-fds-fold a:open-fds-fold)

;; pthreads

;; sqlite3


(import (prefix sqlite3 sqlite3:))

(define sql-result? sqlite3:sql-result?)
(define sql-field sqlite3:sql-field)
(define sql-index sqlite3:sql-index)
(define sql-with-tupels%-fold-left sqlite3:sql-with-tupels%-fold-left)
(define sqlite3-exec sqlite3:sqlite3-exec)
(define sqlite3-call-with-transaction sqlite3:sqlite3-call-with-transaction)
(define sqlite3-call-test/set sqlite3:sqlite3-call-test/set)
(define sql-close sqlite3:sql-close)
(define sql-value sqlite3:sql-value)
(define sql-connect sqlite3:sql-connect)
(define sql-with-tupels sqlite3:sql-with-tupels)

(define sql-ref sqlite3:sql-ref)
(define sql-fold sqlite3:sql-fold)
(define sqlite3-statement-name sqlite3:sqlite3-statement-name)
(define sqlite3-open-restricted-ro sqlite3:sqlite3-open-restricted-ro)
(define sqlite3-statement-container sqlite3:sqlite3-statement-container)
(define sqlite3-database-prep-cache sqlite3:sqlite3-database-prep-cache)
(define sqlite3-database-name sqlite3:sqlite3-database-name)
(define sqlite3-error? sqlite3:sqlite3-error?)
(define sqlite3-changes sqlite3:sqlite3-changes)
(define sqlite3-statement? sqlite3:sqlite3-statement?)
(define sqlite3-statement-raw-pointer sqlite3:sqlite3-statement-raw-pointer)
(define sqlite3-database-open-statements sqlite3:sqlite3-database-open-statements)
(define sqlite3-error-code sqlite3:sqlite3-error-code)
(define sqlite3-open sqlite3:sqlite3-open)
(define sqlite3-close sqlite3:sqlite3-close)
(define sqlite3-open-restricted sqlite3:sqlite3-open-restricted)
(define sqlite3-error-args sqlite3:sqlite3-error-args)
(define make-sqlite3-statement sqlite3:make-sqlite3-statement)
(define sqlite3-debug-statements sqlite3:sqlite3-debug-statements)

;; environments

(import (prefix environments envt:))

(define make-environment envt:make-environment)
(define environment? envt:environment?)
(define environment-copy envt:environment-copy)
(define environment-ref envt:environment-ref)
(define environment-extend! envt:environment-extend!)

;; structures

(import (prefix structures strctrs:))

;; low level; usage deprecated
(define empty-binding-set strctrs:empty-binding-set)
(define make-binding-set strctrs:make-binding-set)
(define binding-set-ref/default strctrs:binding-set-ref/default)
(define binding-set-ref strctrs:binding-set-ref) ; deprecated
;; sequential update
(define binding-set-insert strctrs:binding-set-insert)
(define binding-set-cons strctrs:binding-set-cons) ; srfi-1::alist-cons compatible
;; folding
(define binding-set-fold strctrs:binding-set-fold)
;; setXset
(define binding-set-union strctrs:binding-set-union)
;; 2nd level
(define make-symbolic-environment strctrs:make-symbolic-environment)
(define symbol-environment-ref/default strctrs:symbol-environment-ref/default)
(define symbol-environment-ref strctrs:symbol-environment-ref)
(define symbol-environment-insert strctrs:symbol-environment-insert)

;; mailbox

(import (prefix mailbox mlbx:))
(define make-vector-queue mlbx:make-vector-queue)
(define vector-queue-empty? mlbx:vector-queue-empty?)
(define vector-queue-size mlbx:vector-queue-size)
(define vector-queue-add! mlbx:vector-queue-add!)
(define vector-queue-remove! mlbx:vector-queue-remove!)
(define make-mailbox mlbx:make-mailbox)
(define send-message! mlbx:send-message!)
(define receive-message! mlbx:receive-message!)

;; lalr


(import (prefix lalrdrv lalr:))
(define lr-driver lalr:lr-driver)
(define glr-driver lalr:glr-driver)

(import (prefix lalr lalr:))
(define gen-lalr-parser lalr:gen-lalr-parser)
(define lr-driver lalr:lr-driver)
(define glr-driver lalr:glr-driver)

;; memoize

;; pcre


(import (prefix pcre pcre:))
(define pcre->proc pcre:pcre->proc)
(define pcre->aproc pcre:pcre->aproc)
(define pcre/a->proc-uncached pcre:pcre/a->proc-uncached)
(define make-pcre-tokeniser pcre:make-pcre-tokeniser)

;; timeout


;; setup
(import (prefix timeout tmt:))
(define respond-timeout-interval tmt:respond-timeout-interval)
(define remote-fetch-timeout-interval tmt:remote-fetch-timeout-interval)
;; debug
(define with-timeout! tmt:with-timeout!)
(define timeout-object? tmt:timeout-object?)
