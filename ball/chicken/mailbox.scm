(declare
 (unit mailbox)
 ;; requirements
 (disable-interrupts)
 ;; promises
 (strict-types)
 (usual-integrations)
 (no-procedure-checks-for-usual-bindings)
 (inline)
 (local)
 (fixnum)

 (no-bound-checks)
 (no-procedure-checks-for-usual-bindings)
 (bound-to-procedure
  ##sys#schedule
  ##sys#current-exception-handler)

  (always-bound
    ##sys#current-thread)
 )
(module
 mailbox
 (
  make-vector-queue
  vector-queue-empty?
  vector-queue-size
  vector-queue-add!
  vector-queue-remove!
  ;;
  make-mailbox
  mailbox?
  mailbox-name
  mailbox-empty?
  mailbox-send!
  mailbox-receive!
  ;;
  mailbox-clone
  mailbox-has-message?
  send-message! receive-message! receive-message-if-present!
  mailbox-drop-matching!
  mailbox-number-of-items

  make-checked-mailbox mailbox-send!

  )

(import scheme extras srfi-18)
(import (except chicken add1 sub1))

(include "typedefs.scm")

(define (dbg l v . args)
  (apply format (current-error-port) "VQ ~a ~a\n"  l v args)
  (newline (current-error-port))
  v)

(: make-vector-queue (--> (vector fixnum fixnum vector)))
(define (make-vector-queue) (vector 0 0 (##sys#make-vector 4 #f)))

(: vector-queue-empty? (vector -> boolean))
(define (vector-queue-empty? q) (fx= (##sys#slot q 0) (##sys#slot q 1)))

(: vector-queue-size (vector -> fixnum))
(define (vector-queue-size q)
  (let ((len (fx- (##sys#slot q 1) (##sys#slot q 0))))
      (if (fx< len 0)
	  (fx+ len (##sys#size (##sys#slot q 2)))
	  len)))

(: vector-queue-first ((vector fixnum fixnum vector) --> *))
(define (vector-queue-first q)
  (let ((first (##sys#slot q 0))
	(v (##sys#slot q 2)))
    (##sys#slot v first)))

(: vector-queue-copy! (vector fixnum fixnum vector -> fixnum))
(define-inline (vector-queue-copy! v first last v2)
  (if (fx<= first last)
      (do ((i 0 (fx+ i 1))
	   (j first (fx+ j 1)))
	  ((fx= j last) i)
	(##sys#setslot v2 i (##sys#slot v j)) )
      (let* ( (max (##sys#size v)) )
	(do ((i (do ((i 0 (fx+ i 1))
		     (j first (fx+ j 1)))
		    ((fx= j max) i)
		  (##sys#setslot v2 i (##sys#slot v j)) )
		(fx+ i 1))
	     (j 0 (fx+ j 1)))
	    ((fx= j last) i)
	  (##sys#setslot v2 i (##sys#slot v j)) ))))

(: vector-queue-grow! ((vector vector) vector fixnum -> . *))
(define-inline (vector-queue-grow! q v)
  (let ((v2 (##sys#make-vector (fx+ (##sys#size v) 16) #f)) )
    (##sys#setislot q 1 (vector-queue-copy! v (##sys#slot q 0) (##sys#slot q 1) v2))
    (##sys#setislot q 0 0)
    (##sys#setslot q 2 v2) ))

(: vector-queue-add! (vector * -> undefined))
(define (vector-queue-add! q d)
;(dbg "vector-queue-add! ~a ~a" (##sys#slot q 0) (##sys#slot q 1))
  (let* ((v (##sys#slot q 2))
	 (pos (##sys#slot q 1)))
    (let ((next (fxmod (fx+ pos 1) (##sys#size v))))
      (if (fx= next (##sys#slot q 0))
	(let ((pos (begin
		     (vector-queue-grow! q v)
		     (##sys#slot q 1))))
	  (##sys#setslot (##sys#slot q 2) pos d)
	  (##sys#setislot q 1 (fx+ pos 1)))
	(begin
	  (##sys#setslot v pos d)
	  (##sys#setislot q 1 next))))))

(: vector-queue-remove! (vector -> *))
(define (vector-queue-remove! q)
;(dbg "vector-queue-remove! ~a ~a" (##sys#slot q 0) (##sys#slot q 1))
  (let ((pos (##sys#slot q 0)))
    (if (fx= (##sys#slot q 1) pos)
	#f ; (raise 'queue-empty) ; either way
	(let* ((v (##sys#slot q 2))
	       (d (##sys#slot v pos)))
	  (##sys#setislot v pos #f)
	  (##sys#setislot q 0 (fxmod (fx+ pos 1) (##sys#size v)))
	  d))))

(define (##test#vector-queue-for-each proc q)
  (let ((first (##sys#slot q 0))
	(last (##sys#slot q 1))
	(v (##sys#slot q 2)))
    (if (fx<= first last)
	(do ((i 0 (fx+ i 1))
	     (j first (fx+ j 1)))
	    ((fx= j last) i)
	  (proc (##sys#slot v j)) )
	(let ( (max (##sys#size v)) )
	  (do ((i (do ((i 0 (fx+ i 1))
		       (j first (fx+ j 1)))
		      ((fx= j max) i)
		    (proc (##sys#slot v j)) )
		  (fx+ i 1))
	       (j 0 (fx+ j 1)))
	      ((fx= j last) i)
	    (proc (##sys#slot v j)) )))))

(: mailbox? (* --> boolean : (struct <mailbox>)))
(define-record-type <mailbox>
  (internal-make-mailbox mutex condition queue pred)
  mailbox?
  (mutex mailbox-mutex)
  (condition mailbox-condition)
  (queue mailbox-queue)
  (pred mailbox-predicate))

(define (##test#mailbox-for-each proc mb)
  (##test#vector-queue-for-each proc (mailbox-queue mb)))

(: make-mailbox (#!rest * --> (struct <mailbox>)))
(define (make-mailbox . name)
  (internal-make-mailbox
   (apply make-mutex name) (apply make-condition-variable name) (make-vector-queue) #f))

(: make-checked-mailbox (* (procedure (*) boolean) -> (struct <mailbox>)))
(define (make-checked-mailbox name pred)
  (internal-make-mailbox
   (make-mutex name) (make-condition-variable name) (make-vector-queue) pred))

(: mailbox-name ((struct <mailbox>) --> *))
(define (mailbox-name mailbox)
  (condition-variable-name (mailbox-condition mailbox)))

(: mailbox-clone ((struct <mailbox>) --> (struct <mailbox>)))
(define (mailbox-clone mailbox)
  (let ((name (mailbox-name mailbox)))
;(dbg "mailbox-clone ~a" name)
    (internal-make-mailbox
     (make-mutex name) (make-condition-variable name)
     (let* ((q (mailbox-queue mailbox))
	    (v (##sys#slot q 2))
	    (v2 (##sys#make-vector (##sys#size v) #f))
	    (s (vector-queue-copy! v (##sys#slot q 0) (##sys#slot q 1) v2)))
       (vector 0 s v2))
     (mailbox-predicate mailbox))))

(: mailbox-has-message? ((struct <mailbox>) * (procedure (* *) boolean) --> boolean))
(define (mailbox-has-message? mailbox message equal)
  (let* ((q (mailbox-queue mailbox))
	 (s (##sys#slot q 2)))
    (let loop ((i (##sys#slot q 0)))
;(dbg "mailbox-has-message? ~a" i mailbox)
      (and (not (fx= i (##sys#slot q 1)))
	   (or (equal (##sys#slot s i) message)
	       (loop (fxmod (add1 i) (##sys#size s))))))))

(: mailbox-empty? ((struct <mailbox>) --> boolean))
(define (mailbox-empty? mb)
  (vector-queue-empty? (mailbox-queue mb)))

(: mailbox-number-of-items ((struct <mailbox>) --> fixnum))
(define (mailbox-number-of-items mb)
  (vector-queue-size (mailbox-queue mb)))

(: %send-message! ((struct <mailbox>) * -> undefined))

(cond-expand
 (never

  ;; Safe if compiled with interrupts enabled.

(define-inline (%send-message! mailbox obj)
  (mutex-lock! (mailbox-mutex mailbox))
  (vector-queue-add! (mailbox-queue mailbox) obj)
  (mutex-unlock! (mailbox-mutex mailbox))
  (condition-variable-signal! (mailbox-condition mailbox))
  (void))

(define (receive-message! mailbox)
;(dbg "receive-message! ~a ~a ~a" (current-thread) (mailbox-name mailbox) (mutex-state (mailbox-mutex mailbox)))
  (mutex-lock! (mailbox-mutex mailbox))
  (if (vector-queue-empty? (mailbox-queue mailbox))
      (begin
        (mutex-unlock! (mailbox-mutex mailbox) (mailbox-condition mailbox))
	;(dbg "receive-message! loop ~a ~a" (current-thread) (mailbox-name mailbox))
        (receive-message! mailbox))
      (let ((obj (vector-queue-remove! (mailbox-queue mailbox))))
        (mutex-unlock! (mailbox-mutex mailbox))
        obj)))

  )
 (else

(define-inline (%send-message! mailbox obj)
  (vector-queue-add! (mailbox-queue mailbox) obj)
  (condition-variable-signal! (mailbox-condition mailbox))
  (void))

(: receive-message! ((struct <mailbox>) -> *))
(define (receive-message! mailbox)
  ;;(dbg "receive-message! ~a ~a ~a" (current-thread) (mailbox-name mailbox) (##sys#slot (mailbox-condition mailbox) 2))
  (if (vector-queue-empty? (mailbox-queue mailbox))
      (let ((ct (current-thread))
	    (cvar (mailbox-condition mailbox)))
	(##sys#call-with-current-continuation
	 (lambda (return)
	   (##sys#setslot cvar 2 (##sys#append (##sys#slot cvar 2) (##sys#list ct)))
	   (##sys#setslot ct 11 cvar)
	   (##sys#setslot
	    ct 1
	    (lambda ()
	      (return (vector-queue-remove! (mailbox-queue mailbox))) ) )
	   (##sys#setslot ct 3 'sleeping)
	   (##sys#schedule))))
      #;(begin
	(mutex-lock! (mailbox-mutex mailbox) #f #f)
        (mutex-unlock! (mailbox-mutex mailbox) (mailbox-condition mailbox))
	;(dbg "receive-message! loop ~a ~a" (current-thread) (mailbox-name mailbox))
        (receive-message! mailbox))
      (let ((obj (vector-queue-remove! (mailbox-queue mailbox))))
        obj)))

  ))

(: send-message! ((struct <mailbox>) * -> undefined))
(define (send-message! mailbox obj)
  #;(assert (or (not (mailbox-predicate mailbox)) ((mailbox-predicate mailbox) obj)))
  (%send-message! mailbox obj))

(: mailbox-send! ((struct <mailbox>) * -> undefined))
(define (mailbox-send! mailbox obj)
  (assert (or (not (mailbox-predicate mailbox)) ((mailbox-predicate mailbox) obj))
	  "message illegal for mailbox" obj)
  (send-message! mailbox obj))

(define mailbox-receive! receive-message!)

(: receive-message-if-present! ((struct <mailbox>) -> * boolean))
(define (receive-message-if-present! mb)
  (if (mailbox-empty? mb) (values #f #f) (values (receive-message! mb) #t)))

(: mailbox-drop-matching! ((struct <mailbox>) (procedure (*) boolean) -> boolean))
(define (mailbox-drop-matching! mailbox pred)
  (let loop ()
;(dbg "mailbox-drop-matching! ~a" (mailbox-name mailbox))
    (cond
     ((mailbox-empty? mailbox) #t)
     ((and-let* ((f (vector-queue-first (mailbox-queue mailbox)))
		 ( (pred f) )
		 (f2 (vector-queue-first (mailbox-queue mailbox))))
		(eq? f f2))
      (receive-message! mailbox) (loop))
     (else #f))))

) ;; module mailbox
