;; (C) 2010, J�rg F. Wittenberger

;; chicken module clause for the Askemos 'nunu' (wiki) implementation
;; module .

(declare
 (unit wiki-serial)
 ;;
 (usual-integrations))

(module
 wiki-serial
 (
  wikipedia-parse
  wikipedia->string nunu-switch-syntax
  ennunu denunu
  ;;
  ;; internal
  ;;
  namespace-nu
  ASIS-str
  template-str
  metasystems-str
  subscribers-str
  nu-error
  nu-destination
  fetch
  nunu-fetch
  nu-get-nu
  nunu-inherited
  nunu-acquire
  use-locking?
  nu-text-attributes
  nu-a-xmlns-attributes
  nu-media-type-default-attribute-list
  nu-xmlns-attributes
  make-nu
  the-empty-nu
  make-nunu-link
  undefined-reply
  nunu-text
  nunu-draft
  nunu-locked?
  ;;
  is-include?
  semi-apply-templates
  asis-transformer
  ;;
  call-wikipedia-parse
  )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 srfi-13 (prefix srfi-13 srfi:)
	srfi-34 srfi-35 srfi-45 srfi-69
	pcre lalr extras data-structures
)
(import util (only timeout timestamp) tree notation utf8utils xpath function (only aggregate &http-effective-condition) place-common pool (only ports call-with-output-string))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-inline (make-a-transient-copy-of-object x) x)

(define drop-pi (cons sxp:pi? xml-drop))
(include "../policy/nu-syntax.scm")
(include "../policy/wiki.scm")
)

(import (prefix wiki-serial m:))

(define wikipedia-parse m:wikipedia-parse)
(define wikipedia->string m:wikipedia->string)
(define nunu-switch-syntax m:nunu-switch-syntax)
(define ennunu m:ennunu)
(define denunu m:denunu)
