;; (C) 2002, 2010 Jörg F. Wittenberger

(declare
 (unit storage-gc)
 ;;
 ;;(uses storage-common storage-sql storage-fsm)
 ;;
 (usual-integrations))

(module
 storage-gc
 (
  fsm-delay
  nufsm-restore nufsm-gc
  fsm-delay
  ;;
  fsm-storage-adaptor
  ;;
  ;; internal to storage
  ;;
  fsm-enforce-restore!
  fsm-run-now
  restore-blob-info!
  nufsm-restore-frame
  fsm-gc-compute-tcl
  nufsm-remove-garbage
  )

(import scheme
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 (except srfi-18 raise)
	srfi-13 irregex ;; replace pcre over time with these
	pcre
	srfi-19 srfi-34 srfi-35
	shrdprmtr
	sqlite3
	mailbox util atomic timeout parallel ports tree
	openssl notation function bhob aggregate storage-api)

(import (except posix file-close create-pipe duplicate-fileno process-wait process-signal))
(import place-common place protocol files extras)

(import storage-common storage-sql storage-fsm)

(include "typedefs.scm")

(include "../mechanism/storage/gc.scm")

)

(import (prefix storage-gc m:))
;; debugging
(define fsm-enforce-restore! m:fsm-enforce-restore!)
(define fsm-run-now m:fsm-run-now)
(define restore-blob-info! m:restore-blob-info!)
(define nufsm-restore-frame m:nufsm-restore-frame)
(define nufsm-restore m:nufsm-restore)
(define fsm-gc-compute-tcl m:fsm-gc-compute-tcl)
(define nufsm-remove-garbage m:nufsm-remove-garbage)
