;;; "format.scm" Common LISP text output formatter for SLIB
; Written 1992-1994 by Dirk Lutzebaeck (lutzeb@cs.tu-berlin.de)
; 2004 Aubrey Jaffer: made reentrant; call slib:error for errors.
;
; This code is in the public domain.

; Authors of the original version (< 1.4) were Ken Dickey and Aubrey Jaffer.
; Please send error reports to the email address above.
; For documentation see slib.texi and format.doc.
; For testing load formatst.scm.
;
; Version 3.1

; Modified for CHICKEN Kon Lovett, Sep 25 2005
;
; - depends on srfi-13 functionality
;
; - no local defines for string & number operations
;
; - unprocessed arguments are not an error
;
; - fix for E format; wasn't leaving off leading 0 when result-len > len
; so considered overflow
;
; - explicit use of fixnum arithmetic
;
; - keeps format:* style naming
;
; - exports configuration symbols
;
; - does not use intermediate string when output is a port
;
; - moved defines to toplevel


(declare
 (unit v-clformat)
 (uses clformat) ;; the binding module
 ;; promises
 (strict-types)
#; (no-bound-checks)
 (block)
#|
 (no-argc-checks)
 (no-procedure-checks)
|#
 )

(require-library extras)

(module
 v-clformat
 (
;  set-symbol-case-conv!
;  set-iobj-case-conv!
;  format:expch
;  format:iteration-bounded
;  format:max-iterations
;  format:floats
;  format:complex-numbers
;  format:radix-pref
  #;format:ascii-non-printable-charnames
;  format:fn-max
;  format:en-max
;  format:unprocessed-arguments-error?
  #;format:version
  #;format:iobj->str
  clformat)

 (import b-clformat)
 (import (except scheme inexact->exact)
	 (except chicken add1 sub1 with-exception-handler)
	 (only srfi-34 raise))
 (import (prefix scheme scheme:))
 (import srfi-13)
 (import data-structures ports)
 (import (except extras format))

 (include "typedefs.scm")

 (define-inline (flush-output-port p) (flush-output p))

 (define (inexact->exact n)
   (cond
    ((##sys#exact? n) n)
    ((and (##sys#integer? n) (##sys#flonum-in-fixnum-range? n))
     (scheme:inexact->exact n))
    (else n)))

 (include "../mechanism/srfi/clformat.scm")

 (%!clformat clformat)

 )
