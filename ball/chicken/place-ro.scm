;; (C) 2010 J�rg F. Wittenberger


(declare
 (unit place-ro)
 ;;
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)

 (disable-interrupts)			; small, no loops

 )

(module
 place-ro
 (
  fetch-other
  ;; views
  action-document secure-action-document
  message-protection
  message-capabilities
  message-content-type
  ;;
  is-meta-form?
  metainfo-request?
  metainfo set-meta-info!
  metainfo-request-element
  public-read-metainfo-request
  ;
  intrinsic-sync-locked
  ;; internal
  default-public-meta-info frame-dummy-date
  read-attribute-list write-attribute-list
  metainterface-not-available-response
  ;; gatekeeper
  with-mind-modification
  with-mind-access with-mind-access-now
  enter-front-court enter-front-court-now
  go-on-access
  set-fast-access!
  
  mind-commit $max-commits $commits-per-expire-ratio
  mind-execute-synchron
  mind-exit
  ;;
  make-mind-gatekeeper
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-34 srfi-35 srfi-45 extras)

(import srfi-1 srfi-13 (except srfi-18 raise) srfi-19
	data-structures
	util mailbox
	timeout tree notation aggregate storage-api place-common pool)

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(include "../mechanism/metacomm.scm")

(: $max-commits (or boolean fixnum))
(include "../mechanism/gatekeeper.scm")

)

(import (prefix place-ro m:))

(define secure-action-document m:secure-action-document)
(define action-document m:action-document)

(define mind-exit m:mind-exit)

(define $max-commits m:$max-commits)
(define $commits-per-expire-ratio m:$commits-per-expire-ratio)
