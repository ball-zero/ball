;; (C) 2002, 2008, 2010 J�rg F. Wittenberger

(declare
 (unit storage-api)
 ;;
 (usual-integrations)

 ;; currently contains define-atomic-type
 (disable-interrupts)

 )

(module
 storage-api
 (
  logagree $agree-verbose agree-verbose
  logagree/effective $logagree/effective
  ;;
  blob->xml xml->blob store-blob-value
  store-blob-notation fetch-blob-notation blob-notation-size
  display-blob-notation
  copy-blob-value
  ;;
  change-signal
  make-ready-message make-echo-message
  sync-message-data? sync-message-phase sync-message-request-chks sync-message-serial
  sync-message-container-serial sync-message-container-messages
  sync-message-chks sync-message-version version-serial version-chks
  ;; are these really used elsewhere?
  resolve-else
  current-echo? current-ready? next-echo? confirms-version?
  matches-request? may-match-request?
  ;;
  make-storage-adaptor
  storage-adaptor?
  commit-mind-into commit-mind commit-frame-into load-frame
  gc-mind *the-registered-stores* *the-remote-stores*
  register-storage-adaptor!
  )

(import scheme
	(except chicken add1 sub1 with-exception-handler) srfi-34)

(import util tree bhob aggregate)

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(include "../mechanism/place-macros.scm")
(include "../mechanism/storage-api.scm")
)

(import (prefix storage-api m:))

(define agree-verbose m:agree-verbose)
(define $logagree/effective m:$logagree/effective)
