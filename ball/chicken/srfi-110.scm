(declare
 (unit srfi-110)
 (fixnum)
 (disable-interrupts)
 (not standard-bindings vector-fill! vector->list list->vector))

(module
 srfi-110
 (
  ;; tier read procedures
  curly-infix-read
  neoteric-read sweet-read
  ;; set read mode
  set-read-mode
  ;; replacing the reader
  replace-read restore-traditional-read
  enable-curly-infix enable-neoteric enable-sweet
  ;; Various writers.
  curly-write-simple neoteric-write-simple
  curly-write curly-write-cyclic curly-write-shared
  neoteric-write neoteric-write-cyclic neoteric-write-shared
  ;;
  sweet-read-all
  )

 (import scheme (except chicken add1 sub1 with-exception-handler condition?))

 (import srfi-1 srfi-4 srfi-13 srfi-34 srfi-69)

 (include "typedefs.scm")

 (define srfi-69-make-hash-table make-hash-table)

 ;; Kill undefined stuff for now

 (define-syntax type?
   (syntax-rules () ((_ x) #f)))

 (define-syntax type-of
   (syntax-rules () ((_ x) #f)))

 (define-syntax slot-ref
   (syntax-rules () ((_ type x i) #f)))

 (define-syntax type-num-slots
   (syntax-rules () ((_ x) 0)))

 (define-syntax type-printer
   (syntax-rules () ((_ x) #f)))

 (include "../mechanism/srfi/srfi-110.scm")

 (set-read-mode 'keyword-style-suffix #f)

 (define (sweet-read-all port)
   (let loop ((expr (sweet-read port)) (result '()))
     (if (eof-object? expr)
	 (reverse! result)
	 (loop (sweet-read port) (cons expr result)))))

)

(import (prefix srfi-110 m:))

(define sweet-read m:sweet-read)
(define sweet-read-all m:sweet-read-all)
