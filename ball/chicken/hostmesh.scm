;; (C) 2010, Jörg F. Wittenberger

;; chicken module for the Askemos ball operation control module.

(declare
 (unit hostmesh)
 ;;
 (usual-integrations))

(module
 hostmesh
 (
  config-save-reqested? ball-trigger-save-config! ball-config-saved!
  $host-map-size
  $external-port
  $external-address
  ;;
  ball-add-host!
  ball-host-map->xml
  write-host-map-as-zone-file
  ball-read-host-map!
  ;;
  askemos-connect-back-function
  askemos-connect-back
  ;;
  askemos-connect
  forward-lookup
  askemos-host-lookup
  askemos-reconnect-all
  ;;
  $dns-update-hook
  $dns-zone				; to become obsolete
 )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 srfi-13 (prefix srfi-13 srfi:)
	(except srfi-18 raise) srfi-19 srfi-34 srfi-35 srfi-45
	pcre ports extras openssl shrdprmtr
	timeout atomic parallel cache util)
(import wttree tree notation aggregate storage-api place-common pool corexml)
(import mesh-cert protocol-common protocol-connpool protocol-clntclls http-client protocol)
(import storage dispatch)
(import (only sslsocket ip-address/port? ipv4-address? ipv4-address/port? ipv6-address? ipv6-address/port?))
(import (prefix dns dns-) (only dns ip-match))

(include "typedefs.scm")


(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/mesh.scm")

)

(import (prefix hostmesh m:))

(define $dns-zone m:$dns-zone)
(define $dns-update-hook m:$dns-update-hook)
(define askemos-host-lookup m:askemos-host-lookup)
(define forward-lookup m:forward-lookup)

(define askemos-connect m:askemos-connect)
(define askemos-reconnect-all m:askemos-reconnect-all)

(define ball-trigger-save-config! m:ball-trigger-save-config!)
;;
(define ball-add-host! m:ball-add-host!)

(define $external-port m:$external-port)
(define $external-address m:$external-address)
