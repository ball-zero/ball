;; (C) 2010 J�rg F. Wittenberger

(declare
 (unit msgcoll)
 ;;
 (usual-integrations))

(module
 msgcoll
 (
  has-been-forwarded-nl
  write-with-sync-answer
  )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 srfi-13 srfi-19 srfi-34 srfi-35 extras
	mailbox timeout util
	tree notation bhob aggregate storage-api place-common)

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/msgcoll.scm")

)
