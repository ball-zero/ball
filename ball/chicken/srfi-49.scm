;;; SRFI 49: Sugar read
(declare
 (unit srfi-49)
 (fixnum)
 (not standard-bindings vector-fill! vector->list list->vector))

(register-feature! 'srfi-49)

(module
 srfi-49
;;; -------- Exported procedure index --------
 (sugar-read sugar-read-all)

(import scheme
	(except chicken add1 sub1 with-exception-handler condition?)
	srfi-1 srfi-34
	util extras)

(define-syntax eof-object
  (syntax-rules ()
    ((_) #!eof)))

(include "typedefs.scm")
(include "../mechanism/srfi/srfi-49.scm")

(define sugar-read-save
  (let ([reverse reverse]
	[list? list?]
	[string-append string-append]
	[string string]
	[char-name char-name]
	[csp case-sensitive]
	[ksp keyword-style]
	[crt current-read-table]
	[kwprefix (string (integer->char 0))]
	[namespace-max-id-len 31]
	(read-marks '())
	(read-error (lambda (port err . args) (raise err)))
	(read-warning (lambda (port w . args) (raise w)))
	(infohandler (xthe (or boolean (procedure (* * *) . *)) #f))
	)
    (lambda (port)
      (let ([terminating-characters '(#\, #\; #\( #\) #\[ #\] #\{ #\} #\' #\")]
	    [csp (csp)]
	    [ksp (ksp)]
	    [crt (crt)]
	    [rat-flag #f] )

	(define (container c)
	  (read-error port "unexpected list terminator" c))

	(define (info class data val)
	  (if infohandler
	      (infohandler class data val)
	      data) )

	(define (skip-to-eol)
	  (let skip ((c (##sys#read-char-0 port)))
	    (if (and (not (##core#inline "C_eofp" c)) (not (eq? #\newline c)))
		(skip (##sys#read-char-0 port)) ) ) )

        (define (readrec)

          (define (r-spaces)
            (let loop ([c (##sys#peek-char-0 port)])
	      (cond ((##core#inline "C_eofp" c))
		    ((eq? #\; c) 
		     (skip-to-eol)
		     (loop (##sys#peek-char-0 port)) )
		    ((char-whitespace? c)
		     (##sys#read-char-0 port)
		     (loop (##sys#peek-char-0 port)) ) ) ) )

          (define (r-usequence u n)
	    (let loop ([seq '()] [n n])
	      (if (eq? n 0)
                (let* ([str (##sys#reverse-list->string seq)]
                       [n (string->number str 16)])
                  (or n
                      (read-error port (string-append "invalid escape-sequence '\\" u str "\'")) ) )
                (let ([x (##sys#read-char-0 port)])
                  (if (or (eof-object? x) (char=? #\" x))
                    (read-error port "unterminated string constant") 
                    (loop (cons x seq) (fx- n 1)) ) ) ) ) )

          (define (r-cons-codepoint cp lst)
            (let* ((s (##sys#char->utf8-string (integer->char cp)))
                   (len (##sys#size s)))
              (let lp ((i 0) (lst lst))
                (if (fx>= i len)
                  lst
                  (lp (fx+ i 1) (cons (##core#inline "C_subchar" s i) lst))))))

          (define (r-string term)
            (if (eq? (##sys#read-char-0 port) term)
		(let loop ((c (##sys#read-char-0 port)) (lst '()))
		  (cond ((##core#inline "C_eofp" c) 
			 (read-error port "unterminated string") )
			((eq? #\\ c)
			 (set! c (##sys#read-char-0 port))
			 (case c
			   ((#\t) (loop (##sys#read-char-0 port) (cons #\tab lst)))
			   ((#\r) (loop (##sys#read-char-0 port) (cons #\return lst)))
			   ((#\b) (loop (##sys#read-char-0 port) (cons #\backspace lst)))
			   ((#\n) (loop (##sys#read-char-0 port) (cons #\newline lst)))
			   ((#\a) (loop (##sys#read-char-0 port) (cons (integer->char 7) lst)))
			   ((#\v) (loop (##sys#read-char-0 port) (cons (integer->char 11) lst)))
			   ((#\f) (loop (##sys#read-char-0 port) (cons (integer->char 12) lst)))
			   ((#\x) 
			    (let ([ch (integer->char (r-usequence "x" 2))])
			      (loop (##sys#read-char-0 port) (cons ch lst)) ) )
			   ((#\u)
			    (let ([n (r-usequence "u" 4)])
			      (if (##sys#unicode-surrogate? n)
                                  (if (and (eqv? #\\ (##sys#read-char-0 port))
                                           (eqv? #\u (##sys#read-char-0 port)))
                                      (let* ((m (r-usequence "u" 4))
                                             (cp (##sys#surrogates->codepoint n m)))
                                        (if cp
                                            (loop (##sys#read-char-0 port)
                                                  (r-cons-codepoint cp lst))
                                            (read-error port "bad surrogate pair" n m)))
                                      (read-error port "unpaired escaped surrogate" n))
                                  (loop (##sys#read-char-0 port) (r-cons-codepoint n lst)) ) ))
			   ((#\U)
			    (let ([n (r-usequence "U" 8)])
			      (if (##sys#unicode-surrogate? n)
                                  (read-error port (string-append "invalid escape (surrogate)" n))
                                  (loop (##sys#read-char-0 port) (r-cons-codepoint n lst)) )))
			   ((#\\ #\' #\")
			    (loop (##sys#read-char-0 port) (cons c lst)))
			   (else
			    (read-warning 
			     port 
			     "undefined escape sequence in string - probably forgot backslash"
			     c)
			    (loop (##sys#read-char-0 port) (cons c lst))) ) )
			((eq? term c) (##sys#reverse-list->string lst))
			(else (loop (##sys#read-char-0 port) (cons c lst))) ) )
		(read-error port (string-append "missing `" (string term) "'")) ) )
                    
	  (define (r-list start end)
	    (if (eq? (##sys#read-char-0 port) start)
		(let ([first #f]
		      [ln0 #f]
		      [outer-container container] )
		  (##sys#call-with-current-continuation
		   (lambda (return)
		     (set! container
		       (lambda (c)
			 (if (eq? c end)
			     (return #f)
			     (read-error port "list-terminator mismatch" c end) ) ) )
		     (let loop ([last '()])
		       (r-spaces)
		       (unless first (set! ln0 (##sys#port-line port)))
		       (let ([c (##sys#peek-char-0 port)])
			 (cond ((##core#inline "C_eofp" c)
				(read-error port "unterminated list") )
			       ((eq? c end)
				(##sys#read-char-0 port) )
			       ((eq? c #\.)
				(##sys#read-char-0 port)
				(let ([c2 (##sys#peek-char-0 port)])
				  (cond [(or (char-whitespace? c2)
					     (eq? c2 #\()
					     (eq? c2 #\))
					     (eq? c2 #\")
					     (eq? c2 #\;) )
					 (unless (pair? last)
					   (read-error port "invalid use of `.'") )
					 (r-spaces)
					 (##sys#setslot last 1 (readrec))
					 (r-spaces)
					 (unless (eq? (##sys#read-char-0 port) end)
					   (read-error port "missing list terminator" end) ) ]
					[else
					 (let* ((tok (##sys#string-append "." (r-token)))
						(n (and (char-numeric? c2) 
							(##sys#string->number tok)))
						(val (or n (resolve-symbol tok))) 
						(node (cons val '())) )
					   (if first 
					       (##sys#setslot last 1 node)
					       (set! first node) )
					   (loop node) ) ] ) ) )
			       (else
				(let ([node (cons (readrec) '())])
				  (if first
				      (##sys#setslot last 1 node)
				      (set! first node) )
				  (loop node) ) ) ) ) ) ) )
		  (set! container outer-container)
		  (if first
		      (info 'list-info (##sys#infix-list-hook first) ln0)
		      '() ) )
		(read-error port "missing token" start) ) )
          
	  (define (r-vector)
	    (let ([lst (r-list #\( #\))])
	      (if (list? lst)
		  (##sys#list->vector lst)
		  (read-error port "invalid vector syntax" lst) ) ) )
          
	  (define (r-number radix)
	    (set! rat-flag #f)
	    (let ([tok (r-token)])
	      (if (string=? tok ".")
		  (read-error port "invalid use of `.'")
		  (let ([val (##sys#string->number tok (or radix 10))] )
		    (cond [val
			   (when (and (##sys#inexact? val) rat-flag)
			     (read-warning port "can not represent exact fraction - coerced to flonum" tok) )
			   val]
			  [radix (read-error port "illegal number syntax" tok)]
			  [else (resolve-symbol tok)] ) ) ) ) )

	  (define (r-number-with-exactness radix)
	    (cond [(char=? #\# (##sys#peek-char-0 port))
		   (##sys#read-char-0 port)
		   (let ([c2 (##sys#read-char-0 port)])
		     (cond [(eof-object? c2) (read-error port "unexpected end of numeric literal")]
			   [(char=? c2 #\i) (##sys#exact->inexact (r-number radix))]
			   [(char=? c2 #\e) (##sys#inexact->exact (r-number radix))]
			   [else (read-error port "illegal number syntax - invalid exactness prefix" c2)] ) ) ]
		  [else (r-number radix)] ) )
          
	  (define (r-number-with-radix)
	    (cond [(char=? #\# (##sys#peek-char-0 port))
		   (##sys#read-char-0 port)
		   (let ([c2 (##sys#read-char-0 port)])
		     (cond [(eof-object? c2) (read-error port "unexpected end of numeric literal")]
			   [(char=? c2 #\x) (r-number 16)]
			   [(char=? c2 #\d) (r-number 10)]
			   [(char=? c2 #\o) (r-number 8)]
			   [(char=? c2 #\b) (r-number 2)]
			   [else (read-error port "illegal number syntax - invalid radix" c2)] ) ) ]
		  [else (r-number 10)] ) )
        
	  (define (r-token)
	    (let loop ([c (##sys#peek-char-0 port)] [lst '()])
	      (cond [(or (eof-object? c)
			 (char-whitespace? c)
			 (memq c terminating-characters) )
		     (##sys#reverse-list->string lst) ]
		    [else
		     (when (char=? c #\/) (set! rat-flag #t))
		     (##sys#read-char-0 port)
		     (loop (##sys#peek-char-0 port) 
		       (cons (if csp
				 c
				 (char-downcase c) )
			     lst) ) ] ) ) )

	  (define (r-digits)
	    (let loop ((c (##sys#peek-char-0 port)) (lst '()))
	      (cond ((or (eof-object? c) (not (char-numeric? c)))
		     (##sys#reverse-list->string lst) )
		    (else
		     (##sys#read-char-0 port)
		     (loop (##sys#peek-char-0 port) (cons c lst)) ) ) ) )

	  (define (r-next-token)
	    (r-spaces)
	    (r-token) )
          
	  (define (r-symbol)
	    (let ((s (resolve-symbol
		      (if (char=? (##sys#peek-char-0 port) #\|)
			  (r-xtoken)
			  (r-token) ) ) ) )
	      (info 'symbol-info s (##sys#port-line port)) ) )

	  (define (r-xtoken)
	    (if (char=? #\| (##sys#read-char-0 port))
		(let loop ((c (##sys#read-char-0 port)) (lst '()))
		  (cond ((eof-object? c) (read-error port "unexpected end of `| ... |' symbol"))
			((char=? c #\\)
			 (let ((c (##sys#read-char-0 port)))
			   (loop (##sys#read-char-0 port) (cons c lst)) ) )
			((char=? c #\|)
			 (##sys#reverse-list->string lst) )
			(else (loop (##sys#read-char-0 port) (cons c lst))) ) )
		(read-error port "missing \'|\'") ) )
          
	  (define (r-char)
	    ;; Code contributed by Alex Shinn
	    (let* ([c (##sys#peek-char-0 port)]
		   [tk (r-token)]
		   [len (##sys#size tk)])
	      (cond [(fx> len 1)
		     (cond [(and (or (char=? #\x c) (char=? #\u c) (char=? #\U c))
				 (##sys#string->number (##sys#substring tk 1 len) 16) )
			    => (lambda (n) (integer->char n)) ]
			   [(and-let* ((c0 (char->integer (##core#inline "C_subchar" tk 0)))
				       ((fx<= #xC0 c0)) ((fx<= c0 #xF7))
				       (n0 (fxand (fxshr c0 4) 3))
				       (n (fx+ 2 (fxand (fxior n0 (fxshr n0 1)) (fx- n0 1))))
				       ((fx= len n))
				       (res (fx+ (fxshl (fxand c0 (fx- (fxshl 1 (fx- 8 n)) 1)) 6)
						 (fxand (char->integer 
							 (##core#inline "C_subchar" tk 1)) 
							#b111111))))
			      (cond ((fx>= n 3)
				     (set! res (fx+ (fxshl res 6)
						    (fxand 
						     (char->integer
						      (##core#inline "C_subchar" tk 2)) 
						     #b111111)))
				     (if (fx= n 4)
					 (set! res (fx+ (fxshl res 6)
							(fxand (char->integer
								(##core#inline "C_subchar" tk 3)) 
							       #b111111))))))
			      (integer->char res))]
			   [(char-name (##sys#intern-symbol tk))]
			   [else (read-error port "unknown named character" tk)] ) ]
		    [(memq c terminating-characters) (##sys#read-char-0 port)]
		    [else c] ) ) )

	  (define (r-comment)
	    (let loop ((i 0))
	      (let ((c (##sys#read-char-0 port)))
		(case c
		  ((#\|) (if (eq? #\# (##sys#read-char-0 port))
			     (if (not (eq? i 0))
				 (loop (fx- i 1)) )
			     (loop i) ) )
		  ((#\#) (loop (if (eq? #\| (##sys#read-char-0 port))
				   (fx+ i 1)
				   i) ) )
		  (else (if (eof-object? c)
			    (read-error port "unterminated block-comment")
			    (loop i) ) ) ) ) ) )

	  (define (r-ext-symbol)
	    (let* ([p (make-string/uninit 1)]
		   [tok (r-token)] 
		   [toklen (##sys#size tok)] )
	      (unless ##sys#enable-qualifiers 
		(read-error port "qualified symbol syntax is not allowed" tok) )
	      (let loop ([i 0])
		(cond [(fx>= i toklen)
                       (read-error port "invalid qualified symbol syntax" tok) ]
		      [(fx= (##sys#byte tok i) (char->integer #\#))
		       (when (fx> i namespace-max-id-len)
			 (set! tok (##sys#substring tok 0 namespace-max-id-len)) )
		       (##sys#setbyte p 0 i)
		       (##sys#intern-symbol
			(string-append p (##sys#substring tok 0 i) (##sys#substring tok (fx+ i 1) toklen)) ) ]
		      [else (loop (fx+ i 1))] ) ) ) )

	  (define (resolve-symbol tok)
	    (let ([len (##sys#size tok)])
	      (cond [(and (fx> len 1)
			  (or (and (eq? ksp #:prefix)
				   (char=? #\: (##core#inline "C_subchar" tok 0)) 
				   (##sys#substring tok 1 len) )
			      (and (eq? ksp #:suffix) 
				   (char=? #\: (##core#inline "C_subchar" tok (fx- len 1)))
				   (##sys#substring tok 0 (fx- len 1)) ) ) )
		     => build-keyword]	; ugh
		    [else (build-symbol tok)])))

	  (define (build-symbol tok)
	    (##sys#intern-symbol tok) )
	  
	  (define (build-keyword tok)
	    (##sys#intern-symbol (##sys#string-append kwprefix tok)) )

	  (r-spaces)
	  (let* ([c (##sys#peek-char-0 port)]
		 [srst (##sys#slot crt 1)]
		 [h (and srst (##sys#slot srst (char->integer c)) ) ] )
	    (if h
		(h c port)
		(case c
		  ((#\')
		   (##sys#read-char-0 port)
		   (list 'quote (readrec)) )
		  ((#\`)
		   (##sys#read-char-0 port)
		   (list 'quasiquote (readrec)) )
		  ((#\,)
		   (##sys#read-char-0 port)
		   (cond ((eq? (##sys#peek-char-0 port) #\@)
			  (##sys#read-char-0 port)
			  (list 'unquote-splicing (readrec)) )
			 (else (list 'unquote (readrec))) ) )
		  ((#\#)
		   (##sys#read-char-0 port)
		   (let ((dchar (##sys#peek-char-0 port)))
		     (if (char-numeric? dchar)
			 (let* ((n (string->number (r-digits)))
				(dchar (##sys#peek-char-0 port))
				(spdrst (##sys#slot crt 3)) 
				(h (and spdrst (##sys#slot spdrst (char->integer dchar)) ) ) )
			   (cond (h (h dchar port n))
				 ((or (eq? dchar #\)) (char-whitespace? dchar)) (##sys#sharp-number-hook port n))
				 (else (read-error port "invalid parameterized read syntax" dchar n) ) ) )
			 (let* ((sdrst (##sys#slot crt 2))
				(h (and sdrst (##sys#slot sdrst (char->integer dchar)) ) ) )
			   (if h
			       (h dchar port)
			       (case (char-downcase dchar)
				 ((#\x) (##sys#read-char-0 port) (r-number-with-exactness 16))
				 ((#\d) (##sys#read-char-0 port) (r-number-with-exactness 10))
				 ((#\o) (##sys#read-char-0 port) (r-number-with-exactness 8))
				 ((#\b) (##sys#read-char-0 port) (r-number-with-exactness 2))
				 ((#\i) (##sys#read-char-0 port) (##sys#exact->inexact (r-number-with-radix)))
				 ((#\e) (##sys#read-char-0 port) (##sys#inexact->exact (r-number-with-radix)))
				 ((#\c)
				  (##sys#read-char-0 port)
				  (let ([c (##sys#read-char-0 port)])
				    (fluid-let ([csp 
						 (cond [(eof-object? c)
							(read-error port "unexpected end of input while reading `#c...' sequence")]
						       [(eq? c #\i) #f]
						       [(eq? c #\s) #t]
						       [else (read-error port "invalid case specifier in `#c...' sequence" c)] ) ] )
				      (readrec) ) ) )
				 ((#\() (r-vector))
				 ((#\\) (##sys#read-char-0 port) (r-char))
				 ((#\|)
				  (##sys#read-char-0 port)
				  (r-comment) (readrec) )
				 ((#\#) 
				  (##sys#read-char-0 port)
				  (r-ext-symbol) )
				 ((#\;) 
				  (##sys#read-char-0 port)
				  (readrec) (readrec) )
				 ((#\') 
				  (##sys#read-char-0 port)
				  (list 'syntax (readrec)) )
				 ((#\`) 
				  (##sys#read-char-0 port)
				  (list 'quasisyntax (readrec)) )
				 ((#\$)
				  (##sys#read-char-0 port)
				  (list 'location (readrec)) )
				 ((#\:) 
				  (##sys#read-char-0 port)
				  (build-keyword (r-token)) )
				 ((#\%)
				  (build-symbol (##sys#string-append "#" (r-token))) )
				 ((#\+)
				  (##sys#read-char-0 port)
				  (let ((tst (readrec)))
				    (list 'cond-expand (list tst (readrec)) '(else)) ) )
				 ((#\!)
				  (##sys#read-char-0 port)
				  (let ((c (##sys#peek-char-0 port)))
				    (cond ((or (char-whitespace? c) (char=? #\/ c))
					   (skip-to-eol)
					   (readrec) )
					  (else
					   (let ([tok (r-token)])
					     (cond [(string=? "eof" tok) #!eof]
						   [(member tok '("optional" "rest" "key"))
						    (build-symbol (##sys#string-append "#!" tok)) ]
                                                   [(string=? "current-line" tok)
                                                       (##sys#slot port 4)]
                                                   [(string=? "current-file" tok)
                                                       (port-name port)]
						   [else 
						    (let ((a (assq (string->symbol tok) read-marks)))
						      (if a
							  ((##sys#slot a 1) port)
							  (read-error port "invalid `#!' token" tok) ) ) ] ) ) ) ) ) )
				 (else (##sys#user-read-hook dchar port)) ) ) ) ) ) )
		  ((#\() (r-list #\( #\)))
		  ((#\{) (r-list #\{ #\}))
		  ((#\[) 
		   (r-list #\[ #\]) )
		  ((#\) #\] #\}) 
		   (##sys#read-char-0 port)
		   (container c) )
		  ((#\") (r-string #\"))
		  ((#\.) (r-number #f))
		  ((#\- #\+) (r-number #f))
		  (else (cond [(eof-object? c) c]
			      [(char-numeric? c) (r-number #f)]
			      [else (r-symbol)] ) ) ) ) ) )

	(readrec) ) ) ) )

) ;; module srfi-49

(import (prefix srfi-49 s49:))

(define sugar-read s49:sugar-read)
(define sugar-read-all s49:sugar-read-all)
