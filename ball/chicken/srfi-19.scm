;; (C) 2002, Jörg F. Wittenberger

;; Support from chicken system towards srfi-19.

;; BEWARE: There's a l10ned srfi 19 egg for chicken now.  We ought to
;; switch to that one.

(declare
 (unit v-srfi-19)
; (uses library)
 (disable-interrupts) ;; almost optional, except for tm:get-time-of-day
 ;;
 ;; promises
 (strict-types)
 (block)
 (usual-integrations))

;(register-feature! 'srfi19)

(module
 v-srfi-19
 ()

(import-for-syntax srfi-35)
(import scheme
	(except chicken add1 sub1 with-exception-handler condition?)
	(rename lolevel (object-copy copy))
	srfi-1 srfi-13
	srfi-34 srfi-35 srfi-69 ports extras
	(only clformat clformat))

(import b-srfi-19)

(include "typedefs.scm")
(include "srfi-19-types.scm")

(import foreign)
(declare
  (foreign-declare #<<EOF
static C_TLS long C_ms;
#define C_get_seconds   C_seconds(&C_ms)
EOF
) )

(define-foreign-variable C_get_seconds long)
(define-foreign-variable C_ms long)


;; This is why this must be compiled with (disable-interrupts)!
(define (tm:get-time-of-day)
  (let ((s C_get_seconds))
    (values s C_ms) ) )

(define (debug l v) (clformat (current-error-port) "Sowas ~a: ~a\n" l v) v)

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-syntax no-values
  (syntax-rules ()
    ((no-values) #f)))

;;---- From chicken egg:

(define-inline (fxabs x) (if (fx< x 0) (fxneg x) x))

;; For storage savings since some aritmetic routines do not
;; return fixnums when possible.

;; ##sys#integer?
;; returns #t for integer fixnum or flonum

;; ##sys#double->number
;; returns a fixnum for the flonum x iff x isa integer in fixnum-range
;; otherwise the flonum x

; When domain is integer and range is fixnum
; Number MUST be a fixnum or flonum

(define-inline (gennum->?fixnum x)
  (cond
   ((fixnum? x) x)
   ((##sys#integer? x) (##core#inline "C_double_to_number" x))
   (else (inexact->exact (floor (debug 'X x))))) )

; When domain is integer and range is flonum-integer
; Conversion attemped only when number is a fixnum or flonum-integer
; Others returned

(define-inline (?genint->?fixnum x) (gennum->?fixnum x) )

;;---- END

;;* Locales

;; german and english

(define (locale:english type . args)
  (case type
    ((number-separator) ".")
    ((am) "AM")
    ((pm) "PM")
    ((word) (car args))))

(define *locale:german-table* (the * #f))       ;(make-table string=? string->hash)

(define (add-german-word word-en word-de)
  (hash-table-set! *locale:german-table* word-en word-de))

(define (locale:german type . args)
  (case type
    ((number-separator) ".")
    ((am pm) "")
    ((word) (or (hash-table-ref/default *locale:german-table* (car args) #f) (car args)))))

(define *the-locales*
  `(("de" . ,locale:german)
    ("en" . ,locale:english)))

(define (find-locale locale)
  (let ((result (assoc locale *the-locales*)))
    (or (and result (cdr result)) locale:english)))

;; Calendar locales

;; (%early-once-only

(set! *locale:german-table* (make-hash-table string=?  string-hash))

(define srfi19:locale-abbr-weekday-vector
  (vector "Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat"))

(for-each (lambda (trans) (apply add-german-word trans))
          '(("Sun" "So") ("Mon" "Mo") ("Tue" "Di") ("Wed" "Mi")
            ("Thu" "Do") ("Fri" "Fr") ("Sat" "Sa")))

(define srfi19:locale-long-weekday-vector
  (vector "Sunday" "Monday" "Tuesday" "Wednesday"
          "Thursday" "Friday" "Saturday"))

(for-each (lambda (trans) (apply add-german-word trans))
          '(("Sunday" "Sontag") ("Monday" "Montag")
            ("Tuesday" "Dienstag") ("Wednesday" "Mittwoch")
            ("Thursday" "Donnerstag") ("Friday" "Freitag")
            ("Saturday" "Samstag")))

;; note empty string in 0th place. 
(define srfi19:locale-abbr-month-vector
  (vector
   ""
   "Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))

(for-each (lambda (trans) (apply add-german-word trans))
          '(("May" "Mai") ("Oct" "Okt") ("Dec" "Dez")))

(define srfi19:locale-long-month-vector
  (vector "" "January" "February" "March" "April" "May" "June"
          "July" "August" "September" "October" "November" "December"))

(for-each (lambda (trans) (apply add-german-word trans))
          '(("January" "Januar") ("February" "Februar") ("March" "März")
            ("June" "Juni") ("July" "Juli") ("October" "Oktober")
            ("December" "Dezember")))

(define tm:iso-8601-date-time-format "~Y-~m-~dT~H:~M:~S~z")

(define tm:nano 1000000000)
(define tm:sid  86400)    ; seconds in a day
(define tm:sihd 43200)    ; seconds in a half day
;; RScheme doesn't want to compile that:
;; (define tm:tai-epoch-in-jd 4881175/2) ; julian day number for 'the epoch'
(define tm:tai-epoch-in-jd 2440587.5) ; julian day number for 'the epoch'

;; ) ;; %early-once-only

;; time-error just encapsulates error handling for this file to easy
;; porting effords.

(define-condition-type &time-error &error
  time-error?
  (caller time-error-caller)
  (type time-error-type))

(define (tm:time-error caller type value)
  (if value
      (raise (make-compound-condition
	      (make-condition &time-error 'caller caller 'type type)
	      (make-condition &message 'message value)))
      (raise (make-condition &time-error 'caller caller 'type type))))

;; A table of leap seconds
;; See ftp://maia.usno.navy.mil/ser7/tai-utc.dat
;; and update as necessary.
;; this procedures reads the file in the abover
;; format and creates the leap second table
;; it also calls the almost standard, but not R5 procedures read-line 
;; & open-input-string
;; ie (set! tm:leap-second-table (tm:read-tai-utc-date "tai-utc.dat"))

(define (tm:read-tai-utc-data filename)
  (define (convert-jd jd)
    (* (- (gennum->?fixnum jd) tm:tai-epoch-in-jd) tm:sid))
  (define (convert-sec sec)
    (?genint->?fixnum sec))
  (let ( (port (open-input-file filename))
	 (table (the list '())) )
    (let loop ((line (read-line port)))
      (if (not (eq? line #!eof))
	  (begin
	    (let* ( (data (read (open-input-string (string-append "(" line ")")))) 
		    (year (car data))
		    (jd   (cadddr (cdr data)))
		    (secs (cadddr (cdddr data))) )
	      (if (>= year 1972)
		  (set! table (cons (cons (convert-jd jd) (convert-sec secs)) table)))
	      (loop (read-line port))))))
    table))

;; each entry is ( tai seconds since epoch . # seconds to subtract for utc )
;; note they go higher to lower, and end in 1972.
;; (%early-once-only

;;(define tm:leap-second-table
;;  '((915148800 . 32)
;;    (867715200 . 31)
;;    (820454400 . 30)
;;    (773020800 . 29)
;;    (741484800 . 28)
;;    (709948800 . 27)
;;    (662688000 . 26)
;;    (631152000 . 25)
;;    (567993600 . 24)
;;    (489024000 . 23)
;;    (425865600 . 22)
;;    (394329600 . 21)
;;    (362793600 . 20)
;;    (315532800 . 19)
;;    (283996800 . 18)
;;    (252460800 . 17)
;;    (220924800 . 16)
;;    (189302400 . 15)
;;    (157766400 . 14)
;;    (126230400 . 13)
;;    (94694400  . 12)
;;    (78796800  . 11)
;;    (63072000  . 10)))

(define tm:leap-second-table (the list '()))

(set! tm:leap-second-table
      (map (lambda (p) (cons (+ (* 10000 (car p)) (cadr p)) (caddr p)))
           '((91514 8800  32)
             (86771 5200  31)
             (82045 4400  30)
             (77302 0800  29)
             (74148 4800  28)
             (70994 8800  27)
             (66268 8000  26)
             (63115 2000  25)
             (56799 3600  24)
             (48902 4000  23)
             (42586 5600  22)
             (39432 9600  21)
             (36279 3600  20)
             (31553 2800  19)
             (28399 6800  18)
             (25246 0800  17)
             (22092 4800  16)
             (18930 2400  15)
             (15776 6400  14)
             (12623 0400  13)
             (9469  4400  12)
             (7879  6800  11)
             (6307  2000  10))))

;; ) ;; %early-once-only

(define (read-leap-second-table filename)
  (set! tm:leap-second-table (tm:read-tai-utc-data filename))
  (no-values))


(define (tm:leap-second-delta utc-seconds)
  (letrec ( (lsd (lambda (table)
		   (cond ((>= utc-seconds (caar table))
			  (cdar table))
			 (else (lsd (cdr table)))))) )
    (if (< utc-seconds  (* (- 1972 1970) 365 tm:sid)) 0
	(lsd  tm:leap-second-table))))

;;* the TIME structure; creates the accessors, too.

;; (define-struct time (type second nanosecond))

;; See SRFI-9 for define-record-type

(define-record-type <srfi:time>
  (internal-make-time type nanosecond second)
  srfi19:time?
  (type time-type)
  (nanosecond time-nanosecond %set-time-nanosecond!)
  (second time-second %set-time-second!))

(define (set-time-nanosecond! t ns)
  (%set-time-nanosecond! t (gennum->?fixnum ns)))

(define (set-time-second! t s)
  (%set-time-second! t (?genint->?fixnum s)))

; (define (make-time type sec ns)
;   (internal-make-time
;    type (inexact->exact (round ns)) (inexact->exact (round sec))))

(define (make-time type ns sec)
  (internal-make-time
   type (gennum->?fixnum ns) (?genint->?fixnum sec)))

(define (copy-time time) (copy time))

(define fixnum-divisor 1000000000)

(define (literal-time-second t)
  (if (##sys#exact? t) (number->string t)
      (let ((h (quotient t fixnum-divisor)))
	(string-append (number->string (inexact->exact h))
		       (number->string (inexact->exact (- t (* fixnum-divisor h))))))))

(define (display-time-second t p)
  (display (literal-time-second t) p))

;;* date

(define-record-type <srfi:date>
  (internal-make-date nanosecond second minute hour day month year zone-offset)
  srfi19:date?
  (nanosecond date-nanosecond %set-date-nanosecond!)
  (second date-second %set-date-second!)
  (minute date-minute %set-date-minute!)
  (hour date-hour %set-date-hour!)
  (day date-day %set-date-day!)
  (month date-month %set-date-month!)
  (year date-year %set-date-year!)
  (zone-offset date-zone-offset %set-date-zone-offset!))

(define-inline (set-date-nanosecond! d ns)
  (%set-date-nanosecond! d (gennum->?fixnum ns)))

(define-inline (set-date-second! d s)
  (%set-date-second! d (gennum->?fixnum s)))

(define-inline (set-date-minute! d m)
  (%set-date-minute! d (gennum->?fixnum m)))

(define-inline (set-date-hour! d h)
  (%set-date-hour! d (gennum->?fixnum h)))

(define-inline (set-date-day! date day)
  (%set-date-day! date (gennum->?fixnum day)))

(define-inline (set-date-month! d m)
  (%set-date-month! d (gennum->?fixnum m)))

(define-inline (set-date-year! d y)
  (%set-date-year! d (gennum->?fixnum y)))

(define-inline (set-date-zone-offset! d z)
  (%set-date-zone-offset! d (gennum->?fixnum z)))

(define (make-date nanosecond second minute hour day month year zone-offset)
  (internal-make-date
   (gennum->?fixnum nanosecond) (gennum->?fixnum second)
   (gennum->?fixnum minute) (gennum->?fixnum hour)
   (gennum->?fixnum day) (gennum->?fixnum month)
   (gennum->?fixnum year) (gennum->?fixnum zone-offset)))

;;* time

;; The commented out version did work for fixnum time in chicken.  Now moved to top.

#;(define (tm:get-time-of-day)
  (let ((tm (current-time)))
    (values (##sys#slot tm 2) (fx* (##sys#slot tm 3) 1000))))

(define-syntax make-string-table
  (syntax-rules ()
    ((make-string-table) (make-hash-table string=? string-hash))))

(define-syntax make-character-table
  (syntax-rules ()
    ((make-character-table) (make-hash-table char=? equal?-hash))))

(include "../mechanism/srfi/srfi19.scm")

(%!set-add-german-word! add-german-word)
(%!set-tm:iso-8601-date-time-format! tm:iso-8601-date-time-format)
(%!set-read-leap-second-table! read-leap-second-table)
(%!set-make-time! make-time)
(%!set-time-type! time-type)
(%!set-time-second! time-second)
(%!set-time-nanosecond! time-nanosecond)
(%!set-srfi19:time?! srfi19:time?)
(%!set-copy-time! copy-time)
(%!set-srfi19:current-time! srfi19:current-time)
(%!set-time-resolution! time-resolution)
(%!set-srfi19:time=?! srfi19:time=?)
(%!set-srfi19:time>?! srfi19:time>?)
(%!set-srfi19:time<?! srfi19:time<?)
(%!set-srfi19:time>=?! srfi19:time>=?)
(%!set-srfi19:time<=?! srfi19:time<=?)
(%!set-time-difference! time-difference)
(%!set-add-duration! add-duration)
(%!set-subtract-duration! subtract-duration)
(%!set-time-tai->time-utc! time-tai->time-utc)
(%!set-time-utc->time-tai! time-utc->time-tai)
(%!set-time-monotonic->time-utc! time-monotonic->time-utc)
(%!set-time-monotonic->time-tai! time-monotonic->time-tai)
(%!set-time-utc->date! time-utc->date)
(%!set-time-tai->date! time-tai->date)
(%!set-time-monotonic->date! time-monotonic->date)
(%!set-make-date! make-date)
(%!set-srfi19:date?! srfi19:date?)
(%!set-date-zone-offset! date-zone-offset)
(%!set-date-year! date-year)
(%!set-date-month! date-month)
(%!set-date-day! date-day)
(%!set-date-hour! date-hour)
(%!set-date-minute! date-minute)
(%!set-date-second! date-second)
(%!set-date-nanosecond! date-nanosecond)
(%!set-leap-year?! leap-year?)
(%!set-date-year-day! date-year-day)
(%!set-date-week-day! date-week-day)
(%!set-date-week-number! date-week-number)
(%!set-date->time-utc! date->time-utc)
(%!set-date->julian-day! date->julian-day)
(%!set-date->modified-julian-day! date->modified-julian-day)
(%!set-date->time-tai! date->time-tai)
(%!set-date->time-monotonic! date->time-monotonic)
(%!set-time-utc->julian-day! time-utc->julian-day)
(%!set-time-utc->modified-julian-day! time-utc->modified-julian-day)
(%!set-time-tai->julian-day! time-tai->julian-day)
(%!set-time-tai->modified-julian-day! time-tai->modified-julian-day)
(%!set-time-tai->time-monotonic! time-tai->time-monotonic)
(%!set-time-monotonic->julian-day! time-monotonic->julian-day)
(%!set-time-monotonic->modified-julian-day! time-monotonic->modified-julian-day)
(%!set-julian-day->time-utc! julian-day->time-utc)
(%!set-julian-day->time-tai! julian-day->time-tai)
(%!set-julian-day->time-monotonic! julian-day->time-monotonic)
(%!set-julian-day->date! julian-day->date)
(%!set-modified-julian-day->date! modified-julian-day->date)
(%!set-modified-julian-day->time-utc! modified-julian-day->time-utc)
(%!set-modified-julian-day->time-tai! modified-julian-day->time-tai)
(%!set-modified-julian-day->time-monotonic! modified-julian-day->time-monotonic)
(%!set-current-date! current-date)
(%!set-current-julian-day! current-julian-day)
(%!set-current-modified-julian-day! current-modified-julian-day)
(%!set-srfi19:date->string! srfi19:date->string)
(%!set-srfi19:string->date! srfi19:string->date)
(%!set-literal-time-second! literal-time-second)


) ;; module srfi-19
