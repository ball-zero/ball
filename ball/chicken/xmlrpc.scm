;; (C) 2011, Jörg F. Wittenberger

;; chicken module for the Askemos ball operation xmlrpc module.

;; WARNING: XMLRPC is deprecated!

;; Currently abused to provide json stuff too.

(declare
 (unit xmlrpc)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations))

(module
 xmlrpc
 (
  make-xmlrpc-connection
  xmlrpc-method
  ;;
  application/json
  make-json-object json-object? json-object-bind! json-object-ref json-object-fold
  json->scheme json->xml
  json-write
  ;;
  bencode-write
  )

(import (except scheme force delay vector-fill! vector->list list->vector)
	(except
	 chicken
	 with-exception-handler condition? promise?
	 vector-copy!)
	(except srfi-18 raise)
	srfi-1 srfi-13 (prefix srfi-13 srfi:) srfi-19 srfi-34 srfi-35
	(except srfi-43)
	(prefix srfi-43 srfi:)
	ports pcre lalr util data-structures)
(import tree notation-xmlparse notation
	aggregate-chicken
	(only bhob a:blob?)
	(only aggregate
	      property
	      make-mind-links fold-links link-ref link-bind!
	      )
	
	place-common http-client)

(import (only extras format))

(import cgi)

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/notation/json.scm")
(include "../policy/xmlrpc.scm")

)

(import (prefix xmlrpc m:))

(define make-xmlrpc-connection m:make-xmlrpc-connection)
(define xmlrpc-method m:xmlrpc-method)
;;
(define make-json-object m:make-json-object)
(define json-object? m:json-object?)
(define json-object-bind! m:json-object-bind!)
(define json-object-ref m:json-object-ref)
(define json-object-fold m:json-object-fold)
(define json->scheme m:json->scheme)
(define json->xml m:json->xml)
(define json-write m:json-write)
;;
(define bencode-write m:bencode-write)
