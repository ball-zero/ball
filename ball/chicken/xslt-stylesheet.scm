;; (C) 2014 Jörg F. Wittenberger

;; XSLT transformation root.

(declare
 (unit xslt-stylesheet)
 ;; promises
 (disable-interrupts)			;; loops check interrupts
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (fixnum)
 (strict-types)
 (usual-integrations))

(module
 xslt-stylesheet
 (
  raise-message				; for xslt internal error messages
  dsssl-exception-handler
  dsssl-init
  string->dsssl-proc
  string->dsssl-proc2
  make-xslt-transformer
  make-xslt-transformer*
  ;;
  xsl-element-default
  xslt-process-node-list
  cleanup-xslt-value bind-xsl-value/xpath-expr bind-xsl-value/walk
  xslt-process-node-list
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 vector-copy! with-exception-handler condition? promise?)
        shrdprmtr
	atomic (except srfi-18 raise) srfi-34 srfi-35 srfi-45
	foreign)

(import srfi-1 srfi-4 srfi-13 srfi-19 srfi-43 srfi-69
	(prefix llrb-string-table string-)
	matchable ports memoize pcre environments timeout parallel cache sqlite3
	protection util utf8utils tree wttree notation xpath extras data-structures)

(import srfi-49 srfi-110)

(import (prefix wttree wt:)
	(prefix srfi-1 srfi:)
	(prefix srfi-13 srfi:)
	(prefix srfi-43 srfi:))

(import function-common dsssl)

(import aggregate-chicken)		; string-llrb-tree

(include "typedefs.scm")

(include "../mechanism/function/xslt-stylesheet.scm")

)
