;; (C) 2002, 2003, 2008 J�rg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit protocol-webdav)
 ;;
 (usual-integrations)
 )

(module
 protocol-webdav
 (
  $http-is-synchronous
  init-webdav!
  webdav-commands
  webdav-process
  webdav-command-expecting-102
  debug-webdav?
  ;; internal
  has-been-forwarded-nl
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition?)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:) (except srfi-18 raise)
	 srfi-19 srfi-34 srfi-35 srfi-69 extras ports
	 shrdprmtr util pcre regex util timeout tree notation aggregate place-common place-ro
	 data-structures webdavlo msgcoll protocol-common rfc-2616)

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/http/webdav.scm")

)
