;; This is a replacement stub of the functionality absolutely required
;; to get Askemos running again, since the original environment code
;; is currently broken due to changes in the chicken core system.

(declare
 (unit environments)
 (usual-integrations)
 (disable-interrupts)			; no loop here
 (fixnum)
 (strict-types)
 (inline)
 (no-bound-checks)
 (no-procedure-checks)
 )

(module
 environments
 (
  make-environment
   environment?
   environment-copy
   environment-ref
   environment-ref/default
   ;; environment-set!
   environment-extend!
   ;; environment-includes?
   ;; environment-set-mutable!
   ;; environment-mutable?
   ;; environment-extendable?
   ;; environment-has-binding?
   ;; environment-remove!
   ;; environment-for-each
   ;; environment-symbols
   )

(import scheme structures)
(import (except chicken add1 sub1))

(define (make-environment . extendable?)
  (##sys#make-structure 'environment
    (make-binding-set)
    (and (pair? extendable?) (car extendable?))))

(define-syntax %environment-table
  (syntax-rules () ((_ x) (##sys#slot x 1))))

(define-syntax %environment-table-set!
  (syntax-rules () ((_ x v) (##sys#setslot x 1 v))))

(define (environment? obj)
  (##sys#structure? obj 'environment) )

(define (environment-copy env . mf+syms)
  (##sys#check-structure env 'environment 'environment-copy)
  (let ([mff (pair? mf+syms)])
    (let-optionals mf+syms ([mf #f] [syms #f])
      #;(when syms (##sys#check-list syms 'environment-copy))
      (##sys#make-structure
       'environment (%environment-table env) mf) ) ) )

(define (environment-ref env sym)
  (##sys#check-structure env 'environment 'environment-ref)
  (##sys#check-symbol sym 'environment-ref)
  (binding-set-ref (%environment-table env) sym))

(define (environment-ref/default env sym default)
  (##sys#check-structure env 'environment 'environment-ref/default)
  (##sys#check-symbol sym 'environment-ref)
  (binding-set-ref/default (%environment-table env) sym default))

(define (environment-extend! env sym . v+m)
  (##sys#check-structure env 'environment 'environment-extend!)
  (##sys#check-symbol sym 'environment-extend!)
  (%environment-table-set! env (binding-set-insert (%environment-table env) sym (car v+m))) )

) ;; module environments
