;; (C) 2002, 2003, 2008 J�rg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit rfc-2616)
 ;;
 (usual-integrations)
 (disable-interrupts))

(module
 rfc-2616
 (
  http-legal-commands
  http-commands-with-body
  http-commands-without-body
  http-commands-without-reply-body
  ;;
  http-status-string
  ;;
  http-entity-headers
  http-request-headers
  http-general-headers
  ;;
  http-write-general-headers
  http-write-entity-headers-maybe-chunked
  http-write-entity-headers
  http-write-response-headers
  http-write-request-headers
  ;;
  http-close
  http-close-match
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:)
	 srfi-19 srfi-34 srfi-35 srfi-69
	 pcre regex util
	 data-structures protocol-common)

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/http/rfc2616.scm")

)
