;; (C) 2010 Jörg F. Wittenberger

(declare
 (unit storage-sql)
 ;;
 (usual-integrations))

(module
 storage-sql
 (
  init-sql-core!
  ;;
  spool-db-select spool-db-select/prepared spool-db-exec spool-db-exec/prepared with-spool-db
  lockable-db-connection *spool-db* init-spool-db! check-spool-db! close-spool-db! db-set!
  vacuum-spool-db!
  ;;
  ;; internal to module storage/gc
  lockable-db-mutex
  with-db-set*
  spool-db-set!
  future-write-sql sql-set-missing-oid! sql-oid-missing
  $synchron-sql
  )

(import scheme
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 (except srfi-18 raise)
	srfi-19 srfi-34 srfi-35
	shrdprmtr util sqlite3 atomic parallel tree
	notation bhob aggregate storage-api place-common place files extras)

(import storage-common)
(import (only pool delete-is-sync-hook))
(import (only timeout *system-time* respond-timeout-interval))

(include "typedefs.scm")

(define (identity x) x)

(include "../mechanism/storage/sql.scm")

)

(import (prefix storage-sql m:))
(define $synchron-sql m:$synchron-sql)
;; debugging
(define with-spool-db m:with-spool-db)
(define check-spool-db! m:check-spool-db!)
(define spool-db-select m:spool-db-select)
(define spool-db-exec m:spool-db-exec)
(define vacuum-spool-db! m:vacuum-spool-db!)
(define spool-db-exec/prepared m:spool-db-exec/prepared)
