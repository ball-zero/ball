;; (C) 2010 Jörg F. Wittenberger


(declare
 (unit corexml)
 ;;
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)

;;NO (disable-interrupts) ; optional and should be safe

 )

(module
 corexml
 (
  askemos-core-add-extension!
  $adopt-grant
  ;;
  version-identifier
  ;;
  right->node-list
  node-list->right
  oid->right-name
  oid->absolute-right-name
  oid->external-right-name
  right->string
  string->right
  right->absolute-string
  right-name->oid

  mind-parse-protection
  ;;
  frame-metadata
  make-capability-statement parse-capability-statement
  frame-signature
  access-signature
  ;;
  is-metainfo-request?
  metaview metactrl $meta-lookup-method
  public-meta-info
  ;; these might be better pushed down
  mime-document->message mime-document->message*
  output-element->message
  xml-document->message
  xml-document/content-type->message
  ;;
  is-core-namespace?
  is-mind-element?
  is-forward-extension?
  is-metainfo-request?
  acquire
  mind-default-lookup
  execute-extensions
  interpret-rw
  ;; these should be elsewhere
  dav-propfind-message is-propfind-request?
  ;;;
  i-call-you!
  make-link-form make-grant-form
  make-new-element
  entry-point-metainfo
  make-local-entry-point!
  support-entry-point!
  $delay-user-replication
  make-entry-point!
  disable-entry-point!
  drop-entry-point!
  entry-name->oid
  update-*reverse-of-public-name-space*
  entry-name
  secret-encode secret-verify
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-34 srfi-35 srfi-45 extras)

(import srfi-1 srfi-13 (prefix srfi-13 srfi:) srfi-19 srfi-69
	data-structures
	shrdprmtr util mailbox sqlite3 openssl
	protection
	timeout parallel
	tree notation function environments
	bhob aggregate storage-api)
;; Bug workaround for Chicken 4.13: Make sure current-place and
;; current-message are now bound.
(import (except place-common current-message current-place))
(import pool place-ro (except place current-message current-place))

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))


(: xml-document/content-type->message
   (:place-accessor:
    :message-accessor:
    *					; xml tree
    string				; content-type
    :date:
    &rest (list-of (struct <property>))
   -> :message:))

(: xml-document->message
   (:place-accessor:
    :message-accessor:
    *					; xml tree
    &rest (struct <property>)
   -> :message:))

(: mime-document->message
   (:place-accessor:
    :message-accessor:
    (or string (struct <blob>))		; data
    string				; content-type
    (struct <property>)			; location
    (struct <property>)			; expires
    :date:
    --> :message:))

(: mime-document->message*
   (:place-accessor:
    :message-accessor:
    (or string (struct <blob>))		; data
    string				; content-type
    :date:
    &rest (struct <property>)
    --> :message:))

(: output-element->message
   (:place-accessor:
    :message-accessor:
    *					; output-framgent
    (list-of (struct <property>))
   -> :message:))

(: interpret-rw
   (:place-accessor: :message-accessor: * -> :message:))

(define-syntax file-as-string
  (ir-macro-transformer
    (lambda (form inject compare)
      (let ((filename (cadr form)))
        (call-with-input-file filename (lambda (p) (read-string #f p)))))))

(include "../mechanism/nunu.scm")
(include "../mechanism/icallyou.scm")

(define internal-metaview-template
  (xml-parse (file-as-string "../policy/metaview.xml")))
(define internal-metactrl-template
  (xml-parse (file-as-string "../policy/metactrl.xml")))

(define (internal-meta-lookup type)
  (case type
    ((read) internal-metaview-template)
    ((write) internal-metactrl-template)
    (else #f)))

($meta-lookup-method internal-meta-lookup)

)

(import (prefix corexml m:))

(define version-identifier m:version-identifier)

(define askemos-core-add-extension! m:askemos-core-add-extension!)

(define right->node-list m:right->node-list)
(define node-list->right m:node-list->right)
(define right->string m:right->string)
(define string->right m:string->right)

(define mind-default-lookup m:mind-default-lookup)
(define acquire m:acquire)

(define entry-name->oid m:entry-name->oid)
(define entry-name m:entry-name)

(define metactrl m:metactrl)
(define metaview m:metaview)
(define $meta-lookup-method m:$meta-lookup-method)

(define is-metainfo-request? m:is-metainfo-request?)
(define make-link-form m:make-link-form)
(define make-new-element m:make-new-element)

(define i-call-you! m:i-call-you!)
(define $delay-user-replication m:$delay-user-replication)

(define $adopt-grant m:$adopt-grant)

;;administration
(define support-entry-point! m:support-entry-point!)
(define entry-point-metainfo m:entry-point-metainfo)
(define make-entry-point! m:make-entry-point!)
(define disable-entry-point! m:disable-entry-point!)
(define drop-entry-point! m:drop-entry-point!)
(define (frame-signature obj) (m:frame-signature obj))
