;; (C) 2008, 2010, 2013 Joerg F. Wittenberger see http://www.askemos.org

;; Try to be nice and mix+match with chickens native error handling.
;; Could probably be much mor efficient if we did not do so.

;; Changes
;;
;; 2013: should be renamed now into r7rs-exceptions or something

(declare
 (unit srfi-34)
 (not standard-bindings with-exception-handler)
 ;; optional
 (disable-interrupts)
 ;; promises
 (strict-types)
 (usual-integrations)
 ;;(block)
#; (no-bound-checks)
#; (no-procedure-checks-for-usual-bindings)
#; (bound-to-procedure
  ##sys#current-exception-handler
  ##sys#abort ##sys#make-structure ##sys#structure?)
 )

(module
 srfi-34
 (
  guard
  with-exception-handler
  raise
  raise-continuable
  )

 (import scheme (only chicken make-parameter :)
	 (prefix chicken chicken:))

 ;; Only to support the "dbg" code:
 (import (only chicken current-error-port)
	 srfi-35
	 (only extras format))

 (: ##sys#current-exception-handler (* -> . *))

#|
 (define (dbg l v)
   (format (current-error-port) "~a ~a\n" l v)
   (cond
    ((##sys#structure? v 'condition)
     (format (current-error-port) "... ~a\n"
	     (map
	      (lambda (k)
		(cons k (let loop ((props (##sys#slot v 2))
				   (res '()))
			  (cond ((null? props)
				 res)
				((eq? k (caar props))
				 (loop (cddr props) (cons (list (cdar props) (cadr props)) res)))
				(else (loop (cddr props) res))))))
	      (##sys#slot v 1))))
    ((condition?* v)
     (format (current-error-port) ":... ~a\n" (condition-type-field-alist v))))
   v)
|#

 (define (error msg . args)
   (##sys#abort
    (##sys#make-structure
     'condition
     '(exn) 
     (list '(exn . message) msg
	   '(exn . arguments) args
	   '(exn . location) #f) ) ))

 (: s34-raise (* -> . *))
 (define (s34-raise obj)
   (error "raise: exception handler returned" obj
	  (##sys#current-exception-handler obj)))

 (: s34-raise-continuable (* -> . *))
 (define (raise-continuable obj)
   (##sys#current-exception-handler obj))

 (set! ##sys#raise s34-raise)

 (define raise s34-raise)

;; (set! ##sys#current-exception-handler ##sys#raise)

 (: with-exception-handler ((procedure (*) . *) (procedure () . *) -> . *))
 (define (with-exception-handler handler thunk)
   (let ((oh ##sys#current-exception-handler))
     (dynamic-wind
	 (lambda ()
	   (set! ##sys#current-exception-handler
		 (lambda (ex)
		   (let ((ceh ##sys#current-exception-handler))
		     (dynamic-wind
			 (lambda () (set! ##sys#current-exception-handler oh))
			 (lambda () (handler ex))
			 (lambda () (set! ##sys#current-exception-handler ceh)))))))
	 thunk
	 (lambda ()
	   (set! ##sys#current-exception-handler oh)))))

 (set! chicken:with-exception-handler with-exception-handler)

 (define-syntax guard
   (syntax-rules ()
     ((guard (var clause ...) e1 e2 ...)
      ((call-with-current-continuation
	(lambda (guard-k)
	  (with-exception-handler
	   (lambda (condition)
	     ((call-with-current-continuation
	       (lambda (handler-k)
		 (guard-k
		  (lambda ()
		    (let ((var condition))      ; clauses may SET! var
		      (guard-aux (handler-k (lambda ()
					      (raise condition)))
				 clause ...))))))))
	   (lambda ()
	     (call-with-values
		 (lambda () e1 e2 ...)
	       (lambda args
		 (guard-k (lambda ()
			    (apply values args)))))))))))))

 (define-syntax guard-aux
   (syntax-rules (else =>)
     ((guard-aux reraise (else result1 result2 ...))
      (begin result1 result2 ...))
     ((guard-aux reraise (test => result))
      (let ((temp test))
	(if temp 
	    (result temp)
	    reraise)))
     ((guard-aux reraise (test => result) clause1 clause2 ...)
      (let ((temp test))
	(if temp
	    (result temp)
	    (guard-aux reraise clause1 clause2 ...))))
     ((guard-aux reraise (test))
      test)
     ((guard-aux reraise (test) clause1 clause2 ...)
      (let ((temp test))
	(if temp
	    temp
	    (guard-aux reraise clause1 clause2 ...))))
     ((guard-aux reraise (test result1 result2 ...))
      (if test
	  (begin result1 result2 ...)
	  reraise))
     ((guard-aux reraise (test result1 result2 ...) clause1 clause2 ...)
      (if test
	  (begin result1 result2 ...)
	  (guard-aux reraise clause1 clause2 ...)))))

 )
