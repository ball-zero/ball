;; (C) 2002, 2003, 2008 Jörg F. Wittenberger

;; chicken module clause for the Askemos function module.

(declare
 (unit dsssl)
 ;; promises
 (disable-interrupts)			;; loops check interrupts
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (fixnum)
 (strict-types)
 (usual-integrations))

(module
 dsssl
 (
  dsssl-environment
  ;;
  dsssl-export!                        ; for initialization only!
  eval-sane eval-sane-string
  eval-thread-safe
  ;;
  macro-expander
  interp:make-toplevel-environment interp:r4rs-environment interp:define-special-form!
  syntax-error-handler interpreter-error-handler
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 vector-copy! with-exception-handler condition? promise?)
        shrdprmtr
	atomic (except srfi-18 raise) srfi-34 srfi-35 srfi-45
	foreign)

(import srfi-1 srfi-4 srfi-13 srfi-19 srfi-43 srfi-69
	matchable ports memoize pcre environments timeout parallel
	protection util utf8utils tree notation xpath extras data-structures)

(import srfi-110)

(import	(prefix srfi-1 srfi:)
	(prefix srfi-13 srfi:)
	(prefix srfi-43 srfi:))

(import aggregate-chicken)		; string-llrb-tree

(include "typedefs.scm")

(include "../mechanism/function/scheme.scm")

(include "../mechanism/function/interp.scm")

(include "scheme-chicken.scm")

(define ##draft#make-compiler make-compiler)

(define ##draft#eval #f)

(define ##draft#special-form-table (hash-table-copy r4rs-special-form-table))
(define ##draft#global-table #f)

(define (##draft#define-special-form! operator compile)
  (hash-table-set! ##draft#special-form-table operator compile))

(define (##draft#reset-globals!)
  (set! ##draft#global-table (hash-table-copy dsssl-environment))
  (set! ##draft#eval
	(let ((g (make-compiler ##draft#global-table ##draft#special-form-table)))
          (lambda (exp . vars)
	    (let ((proc ((g exp) (list (map car vars)))))
	      (apply proc (map cdr vars)))) )))
(define (##draft#empty-rt-envt) '())
(define (##draft#extend-rt-envt e n vals) (extend-envt e n vals))

)

(define (draft-eval) ##draft#eval)
(define draft-reset-globals! ##draft#reset-globals!)
(define draft-extend-rt-envt ##draft#extend-rt-envt)
(define draft-define-special-form! ##draft#define-special-form!)
(define (draft-global-table) ##draft#global-table)
(define (draft-special-form-table) ##draft#special-form-table)
(define make-compiler ##draft#make-compiler)

(import (prefix dsssl m:))

(define dsssl-export! m:dsssl-export!)
(define eval-sane m:eval-sane)
(define interpreter-error-handler m:interpreter-error-handler)
(define syntax-error-handler m:syntax-error-handler)

(import (prefix srfi-13 s13:))

(define srfi:string-join s13:string-join)

(import (prefix scheme s1:))

(define srfi:member s1:member)
