(declare
 (unit channel)
 (fixnum-arithmetic)
;;NO (disable-interrupts) ; optional & not proven to be safe that way
 (usual-integrations)

#; (no-bound-checks)
#; (no-procedure-checks-for-usual-bindings)
 
 )

(module
 channel
 (
  $complete-timeout $broadcast-retries $broadcast-timeout
  locked-frames respond!-mutex
  set-respond-mutex-guard! oid-channel
  oid-channel-empty?
  existing-oid-channel
  close-oid-channel!
  oid-channel-send-message-version! oid-channel-entry-pred
  make-oid-channel-entry<
test-and-set-oid-channel-entry-transaction!
oid-channel-entry-overdue?
  channel-every-entry
  channel-last-messages
  channel-filter-transactions!
  channel-advance-transactions
  oid-channel-respond!
  oid-channel-queue-first-transaction
  oid-channel-wait-for-oid!
  oid-channel-mailbox oid-cannel-mailbox-close! oid-channel-unregister-mailbox!
  oid-channel-pending-transactions
  pending-transactions
  register-transaction! oid-channel-remove-version!
  transaction-request transaction-reply
  wait-for-transaction/version
  oid-channel-wait-for-oid!
  ;;
  debug:stick-to-exception!
  ;;
oid-channel-entry-request
set-oid-channel-version!
oid-channel-entry-state-digest
oid-channel-entry-request-digest
oid-channel-entry-start-time
oid-channel-entry-transaction

  )

(import scheme
	(except chicken add1 sub1 condition? promise? with-exception-handler)
	srfi-1 (except srfi-18 raise) srfi-19
	srfi-34 srfi-35
	(only data-structures sort)	; sort
	shrdprmtr util
	(except atomic with-mutex)
	(prefix atomic atomic:)
	timeout cache parallel mailbox)

(import (only extras format))

(include "typedefs.scm")

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define oid->string symbol->string)

;; since it's safe to call out into "place" when place and channel are
;; compiled with interrupts disabled, we can safe the mutex dance.

(cond-expand
 (never
  (define-syntax with-channel-mutex
    (syntax-rules ()
      ((_ m body ...) (atomic:with-mutex m body ...))))
  (define-syntax retain-channel-mutex
    (syntax-rules ()
      ((_ m body ...) (retain-mutex m body ...)))))
 (else
  (define-syntax with-channel-mutex
    (syntax-rules ()
      ((with-mutex m body ...) (begin body ...))))
  (define-syntax retain-channel-mutex
    (syntax-rules ()
      ((_ m body ...) (begin body ...))))))

(include "../mechanism/channel.scm")

(define (wait-for-transaction-once channel queue pred? tmo frac)
  (or (queue-done? queue)
      (let ((t (thread-start!
		(make-thread
		 (lambda () (wait-for-transaction-once! channel queue pred?))
		 "waitf"))))
	(guard
	 (ex ((join-timeout-exception? ex) (raise (timeout-object))))
	 (thread-join! t (and tmo (fp/ (exact->inexact tmo) (exact->inexact frac))))))))

) ;; module channel

(import (prefix channel m:))

(define locked-frames m:locked-frames)
(define set-respond-mutex-guard! m:set-respond-mutex-guard!)
(define close-oid-channel! m:close-oid-channel!)
(define debug:stick-to-exception! m:debug:stick-to-exception!)
