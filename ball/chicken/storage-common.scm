;; (C) 2002, 2010 J�rg F. Wittenberger

;; chicken module clause for the Askemos storage-common module.

(declare
 (unit storage-common)
 ;;
 (usual-integrations))

(module
 storage-common
 (
  $fsm-verbose
  aggregate-check-blob!
  aggregate-check-blob2!
  logfsm
  spool-directory
  )

(import scheme shrdprmtr util parallel bhob aggregate storage-api place-common place)
(import (except chicken add1 sub1))

(define (identity x) x)

(include "../mechanism/storage/common.scm")

(include "../mechanism/storage/macros.scm")


)

(import (prefix storage-common m:))

(define $fsm-verbose m:$fsm-verbose)
(define spool-directory m:spool-directory)

(import (prefix files files:))
(define make-pathname files:make-pathname)
