;; (C) 2008 Jörg F. Wittenberger

(declare
 (unit webdavlo)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)
 )

(module
 webdavlo
 (
  ;;;
  make-object-not-available-condition
  timeout-condition?
  dav:accept-propertyupdate
  &unauthorised &forbidden
  http-effective-condition? &http-effective-condition http-effective-condition-status
  raise-precondition-failed
  raise-service-unavailable-condition
  short-response-message  ;; HERE may i18n, l10n, c13n want to hook in.
  ;; needed at protocol
  set-options-allowed-commands!
  options-request-element
  put-request-element
  delete-request-element
  trace-request-element
					;connect-request-element
  propfind-request-element
  proppatch-request-element
  mkcol-request-element
  copy/move-request-element
					;lock-request-element
					;unlock-request-element
  ;; for action documents
  dav:collection?
  dav-supported-methods
  supported-dav-level
  is-options-request? dav-options
  is-trace-request? dav-trace
  ;;is-connect-request? dav-connect
  is-propfind-request? dav-propfind-message dav-propfind-element
  accept-right-reply
  is-put-request? make-put-element
  ;;is-delete-request? make-delete-element
  is-proppatch-request? make-proppatch-element
  ;; use one of do-propertyupdate-extention or accept-proppatch-element for
  ;; dav:accept-propertyupdate and move it to nunu.scm
  is-copy-request? make-copy-element
  is-move-request? make-move-element
  is-mkcol-request? make-mkcol-element
  is-collection-request?
  )

(import (except scheme vector-fill! vector->list list->vector)
	(except chicken add1 sub1 vector-copy! condition? promise? with-exception-handler)
	srfi-34 srfi-35)

(import srfi-1 srfi-13
	(except srfi-18 raise)
	srfi-19 srfi-43 srfi-69
	openssl
	mailbox memoize environments util timeout parallel protection pcre
	ports extras data-structures tree notation function aggregate
	place-common pool place corexml)

(import (only storage-api blob->xml))
(import (only bhob a:blob?))

(import (prefix srfi-13 srfi:))

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(include "../mechanism/webdav.scm")
)

(import (prefix webdavlo m:))

(define is-propfind-request? m:is-propfind-request?)
(define propfind-request-element m:propfind-request-element)
(define is-options-request? m:is-options-request?)
(define dav-options m:dav-options)
(define dav-propfind-element m:dav-propfind-element)
(define propfind-request-element m:propfind-request-element)

(define is-copy-request? m:is-copy-request?)
(define make-copy-element m:make-copy-element)
(define is-move-request? m:is-move-request?)
(define make-move-element m:make-move-element)
(define is-proppatch-request? m:is-proppatch-request?)
(define make-proppatch-element m:make-proppatch-element)
(define is-put-request? m:is-put-request?)
(define make-put-element m:make-put-element)
(define is-mkcol-request? m:is-mkcol-request?)
(define make-mkcol-element m:make-mkcol-element)
(define is-collection-request? m:is-collection-request?)
