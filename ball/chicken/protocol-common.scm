;; (C) 2002, 2003, 2008, 2013 Jörg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit protocol-common)
 ;;
 (usual-integrations)
;;NO (disable-interrupts) ; optional, not yet checked
 )

(module
 protocol-common
 (
  log-access! default-log-access
  log-access
  default-log-remote $log-remote log-remote!
  use-chunked-encoding?
  ip/name->ip
  string->inet-socket-addr
  expect-object
  eof-condition? eof-condition-reason
  $log-content-type-source
  $http-bandwidth $http-max-write-length
  ;;
  nameserver 
  ;;
  $immediate-body-size
  large-message-size
  $large-request-size
  $small-request-limit
  $large-request-handler
  $http-max-write-length
  $http-bandwidth
  parallel-requests
  parallel-requests-semaphore parallel-secured-requests-semaphore
  ssl-setup-timeout
  http-read-status-line-timeout
  http-read-header-timeout
  http-read-chunked-header-timeout
  http-read-request-line-timeout
  http-read-to-end-timeout
  body-read-timeout
  http-read-trailer-timeout
  http-keep-alive-timeout
  short-local-timeout
  http-with-channel-timeout
  write-plain-timeout
  write-chunk-timeout
  
  ;;
  register-agreement-handler! http-register-handler
  call-agreement-handler
  ;;
  http-read-date
  http-read-header
  http-read-content
  ensure-request-content-type!
  load-mime.types! mime-types-file
  file-name->content-type
  ;;
  http-form-encode
  http-format-location

  http-write-header
  http-write-headers
  http-write-body-chunked
  http-write-body-plain http-write-body-plain-no-timeout
  write-query-form
  ;; Blocktable Accumulator
  make-blocktable-accumulator
  ;; debug
  http-log-header
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 shrdprmtr
	 srfi-1 srfi-13 (prefix srfi-13 srfi:)
	 (except srfi-18 raise)
	 srfi-19 srfi-34 srfi-35 srfi-69
	 atomic libmagic pcre regex util timeout ports
	 (prefix dns dns-)
	 tree notation bhob aggregate storage-api place-common corexml
	 extras data-structures)
 (import (only sslsocket ip-address/port?))

 ;;; BEGIN Blocktable Accumulator
 (import (only lolevel move-memory!))
 ;;; END Blocktable Accumulator

(include "typedefs.scm")
(define-type :large-request-handler:
  (procedure
   (input-port
    :message: (or boolean fixnum)
    procedure * procedure
    fixnum string
    procedure)
   undefined))

(define-inline (assure-object-is-in-transient-memory! obj length)
  (##core#undefined))

(define-inline (loghttps fmt . rest)
  (if ($https-client-verbose) (apply logerr fmt rest)))

(define-inline (ip-match ip) (dns-ip-match ip))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define eof-condition?
  (let ((exn? (condition-predicate 'exn))
	(i/o? (condition-predicate 'i/o))
	(process? (condition-predicate 'process)))
    (lambda (obj)
      (or (eof-object? obj)
	  (and (chicken-condition? obj) (exn? obj) (or (i/o? obj) (process? obj)))))))

(define eof-condition-reason
  (let ((msg (condition-property-accessor 'exn 'message))
	(args (condition-property-accessor 'exn 'arguments)))
    (lambda (obj)
      (cond ((eof-object? obj) 'eof)
	    ;; ((broken-pipe-error? obj) 'EPIPE)
	    ;; ((partial-read-error? obj) 'partial-read)
	    (else (format "~a ~a" (msg obj) (args obj)))))))

(define string->inet-socket-addr identity)

(define-inline (http-make-display-display str port) ((##sys#slot (##sys#slot port 2) 3) port str))

(: http-write-headers
   (* :message: output-port #!rest (procedure (symbol) *)
    -> *))

(: $large-request-size (#!rest fixnum -> fixnum))
(: $small-request-limit (#!rest fixnum -> fixnum))
(: parallel-requests (#!rest (or boolean fixnum) -> (or boolean fixnum)))
(include "../mechanism/protocol/util.scm")

)

(import (prefix protocol-common m:))
(define default-log-remote m:default-log-remote)
(define $log-remote m:$log-remote)
(define log-remote! m:log-remote!)
(define $large-request-size m:$large-request-size)
(define $small-request-limit m:$small-request-limit)
(define $log-content-type-source m:$log-content-type-source)

(define register-agreement-handler! m:register-agreement-handler!)
(define call-agreement-handler m:call-agreement-handler)
