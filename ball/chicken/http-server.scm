;; (C) 2002, 2003, 2008 Jörg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit http-server)
 ;;
 (usual-integrations)
;;NO (disable-interrupts)			; not proven, things may take time
 )

(module
 http-server
 (
  $http-server-verbose
  $https-server-require-cert
  $http-requests-per-connection $https-requests-per-connection
  $authenticate-www-user the-http-domain
  $user-agent-needs-html
  $http-magic-post-get

  http-redirect-off  
  ;;
  http-server https-server
  http-request-server

  http-parse-query
  rfc2046->node-list
  ;; do we need these?:
  http-format-write-location http-format-read-location form-element-attributes
  http-format-answer

  ;;
  http-property-server
  http-require-auth-message
  *not-authorised*
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 shrdprmtr
	 srfi-1 srfi-13 (prefix srfi-13 srfi:) (only srfi-18 thread-sleep!)
	 srfi-19 srfi-34 srfi-35 srfi-45 srfi-69
	 atomic clformat pcre regex util mesh-cert timeout mailbox
	 (prefix dns dns-)
	 parallel cache sslsocket
	 tree notation bhob aggregate place-common storage-api place corexml
	 protocol-common protocol-connpool protocol-webdav rfc-2616 rfc-2518
	 extras data-structures ports)

 (import (only pool find-frame-by-id/resynchronised+quorum))

 (include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/http/server.scm")

)
