;; (C) 2002, 2010, 2013 Jörg F. Wittenberger

(declare
 (unit storage-remote)
 ;;
 (usual-integrations))

(module
 storage-remote
 (
  $max-replication-retries
  http-find-quorum http-rerequest-digests!
  http-replicate-blob!
  http-storage-adaptor http-replicate-place! http-read! http-sync!

  fsm-remote-timeout
  ;;
  $relaxed-replication
  ;; debuggin aid
  $check-replicate-reply
  )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 (except srfi-18 raise)
	srfi-13				; only string-null?
	(prefix srfi-13 srfi:)		; only srfi:string-join
	srfi-19 srfi-34 srfi-35 srfi-45
	shrdprmtr
	mailbox pcre util atomic timeout parallel ports protection tree
	openssl notation function bhob aggregate storage-api)

(import pool place-common place-ro place corexml protocol files extras)

(import (except posix file-close duplicate-fileno create-pipe process-wait process-signal))

(import storage-common storage-sql storage-gc storage-fsm)

(import (only protocol-common $small-request-limit $large-request-handler))

(include "typedefs.scm")

(define-type :frame-version-spec: :frame-version:)
(define-type :place-state-on-host: (pair :hash: :ip-addr:))

(include "../mechanism/storage/remote.scm")

)

(import (prefix storage-remote m:))

(define $check-replicate-reply m:$check-replicate-reply)
(define $relaxed-replication m:$relaxed-replication)
