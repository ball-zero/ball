;; (C) 2002, 2003, 2008 J�rg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit smtp)
 ;;
 (usual-integrations)
;;NO (disable-interrupts)			; why bother?
 )

(module
 smtp
 (
  sendmail
  send-email
  $smtp-host
  lmtp-server
  lmtp-read-data
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:)
	 srfi-19 srfi-34 srfi-35 srfi-69 extras ports
	 shrdprmtr util pcre regex sslsocket timeout tree notation
	 aggregate place-common corexml
	 data-structures protocol-common (prefix dns dns-))

 (import (only srfi-18 current-thread))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/smtp/client.scm")
(include "../mechanism/protocol/smtp/lmtp.scm")

)
