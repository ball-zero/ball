;; (C) 2002, Jörg F. Wittenberger

;; chicken module clause for the Askemos protection module.
;; Hive (?) might be an idea to easy the generation of module clauses.

(declare
 (unit protection)
 ;; optional
 (disable-interrupts)			; checked, perfomance critical
 ;; promises
 (strict-types)
 (usual-integrations)
 (fixnum)
 )

(module
 protection
 (
  unprotected unprotected?
  dominates?
  serves contains-strictly?
  make-service-level full-right?
  make-general-service-level make-acl-service-level

  $key-ring-type
  init-protection
  )

 (import scheme util)
 (import (except chicken add1 sub1))

 (include "typedefs.scm")

 (: make-general-service-level
    (:prot: :capaset: --> (procedure (#!rest :oid:) (or false :capa:))))
(: make-general-service-level2
   (:prot: :capaset: --> (procedure (#!rest :oid:) (or false :capa:))))
(: make-acl-service-level
   (:prot: :capaset: --> (procedure (#!rest :oid:) (or false :capa:))))
(: make-service-level
   (:prot: :capaset: --> (procedure (#!rest :oid:) (or false :capa:))))


 (include "../mechanism/protection.scm")
)

(import (prefix protection p:))

(define dominates? p:dominates?)
(define serves p:serves)
(define contains-strictly? p:contains-strictly?)
(define (make-service-level protection capabilities) (p:make-service-level protection capabilities))
(define full-right? p:full-right?)
(define make-general-service-level p:make-general-service-level)
(define make-acl-service-level p:make-acl-service-level)

(define $key-ring-type p:$key-ring-type)
(define init-protection p:init-protection)
