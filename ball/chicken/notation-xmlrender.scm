;; (C) 2002, Jörg F. Wittenberger

;; chicken module clause for the Askemos notation module.

(declare
 (unit notation-xmlrender)
 (not standard-bindings symbol->string vector-fill! vector->list list->vector)
 ;;
 (disable-interrupts)			; loops checked (trss)
 (usual-integrations)
 (inline)
 (fixnum)
#; (foreign-declare #<<EOF
#include <errno.h>
#include <string.h>

static void A_mapaddstr9(C_word c, C_word self, C_word k,
                        C_word s_buf, C_word s_i,
                        C_word s_str, C_word s_j, C_word s_l,
                        C_word s_part, C_word s_p,
                        C_word s_mapping,
                        C_word s_flush) C_noret;

static void A_mapaddstr9(C_word c, C_word self, C_word k,
                        C_word s_buf, C_word s_i,
                        C_word s_str, C_word s_j, C_word s_l,
                        C_word s_part, C_word s_p,
                        C_word s_mapping,
                        C_word s_flush)
{
  unsigned int i = C_unfix(s_i), j = C_unfix(s_j),
               p = C_unfix(s_p), n=0, d1,
               /* l =C_header_size(s_str),*/ l = C_unfix(s_l),
               pl=C_header_size(s_part),
               bl=C_header_size(s_buf);
  C_char *ori =  (C_char *)C_data_pointer(s_str);
  register C_char *src =  (C_char *)C_data_pointer(s_part) + p;
  register C_char *dst =  (C_char *)C_data_pointer(s_buf) + i;
  register C_char *end = NULL;

  d1 = pl - p; 
  while( 1 ) {
    n = bl - i;
    n = n < d1 ? n : d1 ;
    end = src + n;
    while ( end != src ) *dst++=*src++;
    i += n;
    p += n;
    if( bl == i || j == l ) break;
    else {
     unsigned int c = ori[j++];
     /* vector ref inline */
     /* BEWARE, the range of the mapping is hard coded here "& 0xff" */
     s_part = C_block_item(s_mapping, c & 0xff);
     src  = (C_char *)C_data_pointer (s_part);
     pl   = C_header_size(s_part);
     p    = 0;
     d1   = pl;
    }
  }

  if( i == bl )
    /* end recursive call of s_flush; self is the current closure */
    ((C_proc11)C_fast_retrieve_proc(s_flush))
     (11, s_flush, k, s_buf, C_fix(i), s_str, C_fix(j), C_fix(l),
          s_part, C_fix(p), s_mapping, self); 
    C_kontinue(k, C_fix(i));  /* call the current continuation */
}
EOF
)

 (foreign-declare #<<EOF
#include <errno.h>
#include <string.h>

static void A_mapaddstr9(C_word c, C_word *av) C_noret;

static void A_mapaddstr9(C_word c, C_word *av)
{
  C_word self = av[0], k = av[1],
  s_buf = av[2], s_i = av[3], s_str = av[4],
  s_j = av[5], s_l = av[6], s_part = av[7], s_p = av[8],
  s_mapping = av[9], s_flush = av[10];
  unsigned int i = C_unfix(s_i), j = C_unfix(s_j),
               p = C_unfix(s_p), n=0, d1,
               /* l =C_header_size(s_str),*/ l = C_unfix(s_l),
               pl=C_header_size(s_part),
               bl=C_header_size(s_buf);
  C_char *ori =  (C_char *)C_data_pointer(s_str);
  register C_char *src =  (C_char *)C_data_pointer(s_part) + p;
  register C_char *dst =  (C_char *)C_data_pointer(s_buf) + i;
  register C_char *end = NULL;

  d1 = pl - p; 
  while( 1 ) {
    n = bl - i;
    n = n < d1 ? n : d1 ;
    end = src + n;
    while ( end != src ) *dst++=*src++;
    i += n;
    p += n;
    if( bl == i || j == l ) break;
    else {
     unsigned int c = ori[j++];
     /* vector ref inline */
     /* BEWARE, the range of the mapping is hard coded here "& 0xff" */
     s_part = C_block_item(s_mapping, c & 0xff);
     src  = (C_char *)C_data_pointer (s_part);
     pl   = C_header_size(s_part);
     p    = 0;
     d1   = pl;
    }
  }

  if( i == bl ) {
    /* end recursive call of s_flush; self is the current closure */
    av[0] = s_flush;
    av[3] = C_fix(i);
    av[5] = C_fix(j);
    av[6] = C_fix(l);
    av[7] = s_part;
    av[8] = C_fix(p);
    av[10] = self;
    C_do_apply(11, av);
  } else {
    C_kontinue(k, C_fix(i));  /* call the current continuation */
  }
}
EOF
)

)

(module
 notation-xmlrender
 (
  dsssl-xml-format xml-format xml-format/list
  xml-format-fragment xml-format-exc-c14n xml-format-exc-c14n-root
  xml-format-at-output-port xml-format-fragment-at-output-port
  xml-format-markdown
  xml-format-fragment-at-current-output-port
  xml-format-at-current-output-port
  xml-quote-display             ; will be dropped
  xml-digest-simple ; sha256 sum of string or tree
  ;;
  html-format-at-output-port
  html-format html-format/list content-type-for-html
  $buffer-size $force-iconv-on-output make-display
          
  ;; these should not be exported, but we just need them so badly in
  ;; the fsm.scm

  xml-quote-display-at-port
  )

(import (except scheme vector-fill! vector->list list->vector symbol->string)
	(except chicken add1 sub1 condition? promise? with-exception-handler vector-copy!)
	foreign
	(except srfi-1 partition) srfi-13 srfi-34 srfi-35
	srfi-43 (prefix srfi-43 srfi:) srfi-69
	structures openssl atomic regex pcre util ports tree extras
	notation-charenc notation-common)

(define-syntax symbol->string
  (syntax-rules ()
    ((_ s) (##sys#slot s 1) #;(##sys#symbol->string s))))

(: partition/cps-sharing
   ((procedure (list list) . *)
    (procedure (*) boolean)
    list
    --> . *))
(define (partition/cps-sharing return pred lis)
  (let recur ((lis lis) (return return))
    (if (null? lis) (return lis lis)
	(let ((elt (car lis))
	      (tail (cdr lis)))
	  (let ((cont (lambda (in out)
			(if (pred elt)
			    (return (if (pair? out) (cons elt in) lis) out)
			    (return in (if (pair? in) (cons elt out) lis))))))
	    (recur tail cont))))))

(: partition/iterative ((procedure (*) boolean) list --> list list))
(define (partition/iterative pred lst)
  (let ((t (cons #f '()))
	(f (cons #f '())))
    (let ((tl t) (fl f))
      (do ((lst lst (cdr lst)))
	  ((null? lst) (values (cdr t) (cdr f)))
	(let ((elt (car lst)))
	  (if (pred elt)
	      (let ((p (cons elt (cdr tl))))
		(set-cdr! tl p)
		(set! tl p))
	      (let ((p (cons elt (cdr fl))))
		(set-cdr! fl p)
		(set! fl p))))))))

(define partition partition/iterative)

(: partition/cps-iterative
   ((procedure (list list) . *)
    (procedure (*) boolean)
    list
    --> . *))
(define (partition/cps-iterative values pred lst)
  (let ((t (cons #f '()))
	(f (cons #f '())))
    (let ((tl t) (fl f))
      (do ((lst lst (cdr lst)))
	  ((null? lst) (values (cdr t) (cdr f)))
	(let ((elt (car lst)))
	  (if (pred elt)
	      (let ((p (cons elt (cdr tl))))
		(set-cdr! tl p)
		(set! tl p))
	      (let ((p (cons elt (cdr fl))))
		(set-cdr! fl p)
		(set! fl p))))))))

(: partition/cps ((procedure (list list) . *)
		  (procedure (*) boolean)
		  list
		  --> . *))
(define partition/cps partition/cps-iterative)

(include "typedefs.scm")

(define-type :render-mapping: vector)
(define-type :render-display:
  ((or symbol string)			; TODO: split into non-generic
   (or boolean :render-mapping:)
   -> void))
(define-type :render-namespace-bindings: (struct <symbol-binding-node>))
(define-type :render-renderer:
  (:render-display:
   (or boolean symbol)			; c14n key
   :render-namespace-bindings:		; seen-nms
   :render-namespace-bindings:		; declared-nms
   (or boolean fixnum)			; indent-prefix
   :xml-node:				; current-node
   (list-of (pair :sxpath-pred: procedure)) ; procedure is actually a recursive :render-renderer:
   -> *))

(include "../mechanism/syntax/petrofsky-extract.scm")
(include "receive-no-values.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

;;** output-buffer

(define (output-buffer:to-string buf i extends)
  (let* ((size (fold (lambda (e i) (fx+ (string-length e) i)) i extends))
         (last (fx- size i))
         (result (make-string/uninit size)))
    (##sys#copy-bytes buf result 0 last i)
    (let loop ((j last) (all extends))
      (if (eq? j 0)
          result
          (let ((i (string-length (car all))))
            (##sys#copy-bytes (car all) result 0 (fx- j i) i)
            (loop (fx- j i) (cdr all)))))))

(define (output-buffer:to-list buf i extends)
  (cons* (fold (lambda (e i) (fx+ (string-length e) i)) i extends)
         (if (eq? (string-length buf) i) buf (substring buf 0 i))
         extends))

(define (output-buffer:make-buffer)
  (define-constant bufsize 256)
  (define-constant copysize (round (/ (* 3 256) 2)))
  (define (addstr buf i str j flush)
    (let* ((n (min (fx- (string-length str) j)
                   (fx- (string-length buf) i)))
           (i2 (fx+ i n)))
      (##sys#copy-bytes str buf j i n)
;      ((foreign-lambda*
;        integer
;        ((c-string str) (c-string buf) (integer j) (integer i) (integer n))
;        "++n; str+j-1; buf+i-1; while( --n ) *++buf=*++str;") str buf j i n)
      (if (eq? i2 bufsize) (flush buf i2 str (fx+ j n) addstr) i2)))
  (let ((buffer (make-string/uninit bufsize))
        (i 0)
        (l (the list '())))
    (define (flush buf i str j addstr)
      (set! l (cons buf l))
      (set! buffer (make-string/uninit bufsize))
      (addstr buffer 0 str j flush))
    (lambda (arg)
      (if (procedure? arg)
          (arg buffer i l)
          (make-output-port
           (lambda (str)
;             (if (fx> (string-length str) copysize)
;                 (begin
;                   (set! l (cons* str (substring buf 0 i) l))
;                   (set! i 0))
;                 (set! i (addstr buffer i str 0 flush)))
             (set! i (addstr buffer i str 0 flush)))
           (lambda () #t))))))

; (set! with-output-to-string output-buffer:with-output-to-string)

;;** render
(define $buffer-size 4096)

(define $force-iconv-on-output #f)

(: mapaddstr9
   (string fixnum string fixnum fixnum string fixnum (vector-of string) procedure
	   -> fixnum))
#;(define (mapaddstr9 buf i str j l part p mapping flush)
  (let* ((n (min (fx- (string-length part) p)
                 (fx- (string-length buf) i)))
         (i2 (fx+ i n)))
    (##sys#copy-bytes part buf p i n)
;      ((foreign-lambda*
;        void
;        ((c-string str) (c-string buf) (integer j) (integer i) (integer n))
;        "++n; str+j-1; buf+i-1; while( --n ) *++buf=*++str;") str buf j i n)
    (if (eq? i2 (string-length buf))
        (flush buf i2 str j l part (fx+ p n) mapping mapaddstr9)
        (if (eq? j l)
	    i2
	    (mapaddstr9
	     buf i2 str (add1 j) l
	     (##sys#slot mapping (char->integer (string-ref str j))) 0
	     mapping flush)))))

(define make-display
  (let ((mapaddstr9 (##core#primitive "A_mapaddstr9")))
    (define (make-display kind encoding)
      (let* ((buffer (make-string/uninit $buffer-size))
	     (cnv (and (or $force-iconv-on-output
			   (not (or (equal? encoding "UTF-8")
				    (equal? encoding "UTF8"))))
		       (iconv-open encoding "UTF-8")))
	     (ibuffer (and cnv (make-string/uninit $buffer-size)))
	     (i 0)
	     (l (if (output-port? kind) #f (list 'dummy)))
	     (r l))

	(define (flush9 buf i str j sl part p mapping continue)
	  (if l
	      (begin
		(set-cdr! r (list buf))
		(set! r (cdr r))
		(set! buffer (make-string/uninit $buffer-size)))
	      (display buf kind))

	  (chicken-check-interrupts!)

	  (continue buffer 0 str j sl part p mapping flush9))

	(define (flush5 buf i str j continue)
	  (if l
	      (begin
		(set-cdr! r (list buf))
		(set! r (cdr r))
		(set! buffer (make-string/uninit $buffer-size)))
	      (display buf kind))

	  (chicken-check-interrupts!)

	  (continue buffer 0 str j flush5))

	(lambda (obj mapping)
	  (cond
	   (mapping
	    (if cnv
		(let loop ((off 0)
			   (n (string-length obj)))
		  (if (= n 0)
		      i
		      (receive (nx sn dn)
			       (iconv-bytes cnv obj off n ibuffer 0 $buffer-size)
			       (set! i (mapaddstr9 buffer i
						   ibuffer 0 (- $buffer-size dn)
						   "" 0 mapping flush9))
			       (if (< sn 0)
				   i
				   (loop (+ off (- n sn)) sn)))))
		(set! i (mapaddstr9 buffer i obj 0 (string-length obj)
				    "" 0 mapping flush9))))
	   ((string? obj) (set! i (string-splice! buffer i obj 0 flush5)))
	   ((symbol? obj)
	    (set! i (string-splice! buffer i (symbol->string obj) 0 flush5)))
	   ((eof-object? obj)
	    (if cnv (iconv-close cnv))
	    (cond
	     ((eq? kind 'string)
	      (set-cdr! r (list (if (eqv? i (string-length buffer)) buffer
				    (substring buffer 0 i))))
	      (apply-string-append (cdr l)))
	     ((eq? kind 'list)
	      (set-cdr! r (list (if (eqv? i (string-length buffer)) buffer
				    (substring buffer 0 i))))
	      (set! buffer #f)
	      (set-car! l (fold (lambda (e i) (fx+ (string-length e) i))
				0 (cdr l)))
	      l)
	     (else (if (eqv? i (string-length buffer))
		       (display buffer kind)
		       (do ((k 0 (add1 k)))
			   ((eqv? k i))
			 (display (string-ref buffer k) kind))))))
	   (else (error "no handler for ~a" obj))))))
    make-display))

(define make-display-final #!eof)

(define (no-make-display port)
  (lambda (obj mapping)
    (cond
     (mapping (string-for-each
               (lambda (c) (display (vector-ref mapping (char->integer c))
                                    port))
               obj))
     ((or (string? obj) (symbol? obj)) (display obj port))
     ((eof-object? obj) #t)
     (else (error "no handler for ~a" obj)))))

;; We should look into savings here along the lines of the rscheme
;; method which has been commented out as the pregexp are horribly
;; slow.

(include "../mechanism/notation/xml/render.scm")
)

(import (prefix notation-xmlrender m:))

(define xml-format-exc-c14n m:xml-format-exc-c14n)
(define xml-format-exc-c14n-root m:xml-format-exc-c14n-root)
(define xml-format-markdown m:xml-format-markdown)
