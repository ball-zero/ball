;; (C) 2010 J�rg F. Wittenberger


(declare
 (unit methods)
 ;;
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)

;;NO (disable-interrupts)			; ought to be fast, though not proven

 )

(module
 methods
 (
  global-proxy-mode
  ;;
  nu-action
  nu-action-init
  $cfg-voters ;; obsolete?
  call-subject-remote-answer-timeout
  call-subject!
  call-local-subject!
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-34 srfi-35 srfi-45 extras)

(import srfi-1 srfi-13 (prefix srfi-13 srfi:) srfi-19
	data-structures ports
	memoize shrdprmtr
	util mailbox sqlite3 openssl
	protection
	timeout parallel
	tree notation srfi-110 function
	aggregate channel storage-api place-common pool place-ro place)

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

;; TODO make-a-transient-copy-of-object should be copy-lisp or
;; something like that.  It's supposed to create a fresh copy equal?
;; to it's argument.  Because it's here used for just one occurence,
;; I'll make it more simple.
(define (make-a-transient-copy-of-object x)
  ;; (map (lambda (i) (map identity i)) x)
  x)

(: call-local-subject!
   (:quorum: :place: symbol :message: -> :message:))

(: call-subject-wait-on-version!
   (:oid: fixnum :quorum: (or number boolean) :message: -> *))

(: call-subject-wait-for-remote-reply!
   (:place: (struct <mailbox>) :message: -> :place-or-not:))

(: call-subject!-implementation :call-subject:)


(include "../mechanism/methods.scm")

)
(import (prefix methods m:))
(define nu-action-init m:nu-action-init)
