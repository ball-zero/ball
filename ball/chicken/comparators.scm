(declare
 (unit comparators)
 (safe-globals)
 (specialize))

(module comparators ()
  (import scheme)
  ;;(import (only chicken use export include case-lambda error define-record-type : define-type #!optional))
  (import chicken)
  (export comparator? comparator-ordered? comparator-hashable?)
  (export make-comparator
          make-pair-comparator make-list-comparator make-vector-comparator
          make-eq-comparator make-eqv-comparator make-equal-comparator)
  (export boolean-hash char-hash char-ci-hash
          string-hash string-ci-hash symbol-hash number-hash)
  (export make-default-comparator default-hash comparator-register-default!)
  (export comparator-test-type comparator-check-type comparator-hash)
  (export =? <? >? <=? >=?)
  (export comparator-if<=>)
  ;; Exports not mandated by srfi, but useful.
  (export comparator-type-test-predicate comparator-equality-predicate
	  comparator-ordering-predicate comparator-hash-function)
  (use srfi-4)
  (use srfi-13)
  (define-type :comparator: (struct comparator))
  (define-type :type-test: (procedure (*) boolean))
  (define-type :comparsion-test: (procedure (* *) boolean))
  (define-type :hash-code: fixnum)
  (define-type :hash-function: (procedure (*) :hash-code:))
  (include "../mechanism/srfi/comparators/r7rs-shim.scm")
  (include "../mechanism/srfi/comparators/comparators-impl.scm")
  (include "../mechanism/srfi/comparators/default.scm")
)
