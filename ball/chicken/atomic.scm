(declare
 (unit atomic)
 ;; required, as the module name suggests
 (disable-interrupts)			; atomic features here
 ;; promises

 ;; This used to compile under (strict-types), however at least
 ;; internal pipes and process-io-ports do NOT COMPLY to strict-type
 ;; restrictions.
 
 (usual-integrations)

; (no-bound-checks)
 (no-procedure-checks-for-usual-bindings)
 (not standard-bindings symbol->string)
 (bound-to-procedure
  ##sys#check-symbol ##sys#symbol->string
  ##sys#thread-yield!
  thread-yield! ##sys#thread-unblock!
  gensym ##sys#thread-block-for-timeout! ##sys#thread-kill!
  ##sys#thread-block-for-termination! make-thread
  ##sys#schedule
  )
 (foreign-declare #<<EOF

#include <errno.h>

static C_word ac_again = C_SCHEME_FALSE;
#define AC_again() ac_again
// #define AC_interrupted() (errno == EINTR)
#define AC_interrupted() 0

static int io_needs_restart()
{
 return
  (errno == EAGAIN) 
  ||
  (errno == EWOULDBLOCK)
  || (errno == EINTR);
}

static int AC_do_read(int fd, C_char* buf, int s)
{
 int r = read(fd, buf, s);
 ac_again = (r==-1) && io_needs_restart() ? C_SCHEME_TRUE : C_SCHEME_FALSE;
 return r;
}
#define AC_read(fd,buf,s) C_fix(AC_do_read(C_unfix(fd), C_c_string(buf), C_unfix(s)))

static int AC_do_write(int fd, C_char* buf, int off, int count)
{
 int r = write(fd, buf+off, count);
 ac_again = (r==-1) && io_needs_restart() ? C_SCHEME_TRUE : C_SCHEME_FALSE;
 return r;
}
#define AC_write(fd,buf,off,count) C_fix(AC_do_write(C_unfix(fd), C_c_string(buf), C_unfix(off), C_unfix(count)))

#define C_close(fd)         C_fix(close(C_unfix(fd)))
#define C_dup(x)            C_fix(dup(C_unfix(x)))
#define C_dup2(x, y)        C_fix(dup2(C_unfix(x), C_unfix(y)))

#include <fcntl.h>
EOF
)

 )

(define (##test#grow-vector v n init)
  (let ([v2 (##sys#make-vector n init)]
	[len (##sys#size v)] )
    (do ([i 0 (fx+ i 1)])
	((fx>= i len) v2)
      (##sys#setslot v2 i (##sys#slot v i)) ) ) )

(define ##test#thread-number 0)

(define (##test#make-thread thunk state name q)
  (set! ##test#thread-number (add1 ##test#thread-number))
  (##sys#make-structure
   'thread
   thunk				; #1 thunk
   #f					; #2 result list
   state				; #3 state
   #f					; #4 block-timeout
   (vector				; #5 state buffer
    ##sys#dynamic-winds
    ##sys#standard-input
    ##sys#standard-output
    ##sys#standard-error
    ##sys#default-exception-handler
    (##test#grow-vector ##sys#current-parameter-vector (##sys#size ##sys#current-parameter-vector) #f) )
   (format "~a-~a" name ##test#thread-number)				; #6 name
   (##core#undefined)			; #7 end-exception
   '()					; #8 owned mutexes
   q					; #9 quantum
   (##core#undefined)			; #10 specific
   #f					; #11 block object (type depends on blocking type)
   '()					; #12 recipients
   #f) )				; #13 unblocked by timeout?

(define (##test#threads-numbers!) (set! ##sys#make-thread ##test#make-thread))

(module
 atomic
 (
  ;; SRFI 51
  rest-values
  ;;
  make-semaphore semaphore? semaphore-value semaphore-delete!
  with-semaphore semaphore-signal semaphore-wait semaphore-wait-by! semaphore-signal-by!
  semaphore-name
  ;;
  with-mutex
  ;; Development Aid
  (retain-mutex *retained-mutex-set*) (yield-mutex-set! *retained-mutex-set*) (hang-on-mutex! *retained-mutex-set* hang-on-mutex-report)
  ;;
  make-internal-pipe
  chicken-check-interrupts! chicken-check-interrupts/rarely!
  yield-system-load
  os-error
  process-io-ports net-io-ports
  process-io-stats
  process-call
  save-fork running-processes-fold get-rid-of-pid process-wait-for-pid
  tq-entry? entry-successor entry-predecessor entry-time entry-value
  tq-insert! tq-remove! tq-invalidate!
  make-tq tq? tq-name tq-queue
  sudo! close-all-ports-except make-process-stub
  ;;
  make-pthread-from-thread-id pthread?
  ;;
  ;; POSIX overwrites - TBD: move to own module
  file-close! file-close
  duplicate-fileno
  create-pipe
  ;;
  process-wait process-signal
  ;; for the time being
  set-open-fd!
  open-fds-fold
  )

(import scheme
	(except chicken add1 condition? with-exception-handler errno)
	foreign
	lolevel extras
	srfi-1 (except srfi-18 raise)
	srfi-34 srfi-35 ports srfi-69 data-structures)

(import (prefix posix posix:))
(import (except
	 posix
	 file-close create-pipe
	 duplicate-fileno
	 ;;
	 process-wait process-signal))

(import (only lolevel mutate-procedure!))

(include "typedefs.scm")

(: make-file-nonblocking! (fixnum -> boolean))
(: make-socket-nonblocking! (fixnum -> boolean))

(define-syntax define-warning-once
  (syntax-rules ()
    ((_ (name arg ...) body ...)
     (define name
       (let ((called #f))
         (lambda (arg ...)
           (unless called
                   (set! called #t)
                   (display
                    (format
                     "WARNING (only once!1!): call to ~a ignored\nThis MAY have all sorts of consequences!!!\n\n"
                     name)
                    (current-error-port)))
           body ...))))))

(cond-expand
 (windows

  ;; FIXME: This probably needs ##net#startup from sslsocket before!
  (foreign-declare "
# include <winsock2.h>
static C_word make_socket_nonblocking (C_word sock) {
  int fd = C_unfix(sock);
  C_return(C_mk_bool(ioctlsocket(fd, FIONBIO, (void *)&fd) != SOCKET_ERROR)) ;
}
")

  (define-warning-once (make-file-nonblocking! fd) "dunnowhattodo")
  (define-inline (make-socket-nonblocking! fd) (##core#inline "make_socket_nonblocking" fd))

  (define-inline (atomic:process-fork)
    (error "atomic:process-fork \"kill\" not available on this platform"))
  (define-inline (atomic:process-signal id sig)
    (error "atomic:process-signal \"kill\" not available on this platform"))
  ) ;; end cond-expand for 'windows'
 (else
  (foreign-declare "
#include <sys/wait.h>
/* copied from posixunix FTHB :-( */
#define C_kill(id, s)       C_fix(kill(C_unfix(id), C_unfix(s)))
")

  (define make-file-nonblocking!
    (foreign-lambda*
     bool ((int fd))
     "int val = fcntl(fd, F_GETFL, 0);"
     "if(val == -1) C_return(0);"
     "C_return(fcntl(fd, F_SETFL, val | O_NONBLOCK) != -1);") )

  (define-inline (make-socket-nonblocking! fd) (make-file-nonblocking! fd))

  (define-inline (atomic:process-fork)
    ((foreign-lambda int "fork")))
  (define-inline (atomic:process-signal id sig)
    (##core#inline "C_kill" id sig))
  ;; http://3e8.org/blog/2011/08/02/take-off-every-sigterm/
  (foreign-code "siginterrupt(SIGTERM, 1);")
  (foreign-code "siginterrupt(SIGCHLD, 1);")
  (foreign-code "siginterrupt(SIGUSR1, 1);")
  (foreign-code "siginterrupt(SIGUSR2, 1);")
  (foreign-code "siginterrupt(SIGPIPE, 1);")
  ))

;;(include "../mechanism/srfi/rest-values.scm")
(define (rest-values rest . default) (error "rest-values no longer supported"))

;(define-inline (dbg . args) (begin (apply print args) (flush-output (current-output-port))))
;(define-inline (dbg fmt . args) (display (apply format #f fmt args) (current-error-port)))
(define-inline (dbg . args) #f)

(define (debug l v)
  (format (current-error-port) "atomic: ~a ~a\n" l v) v)

(define (unsafe-symbol->string s)
  (##sys#check-symbol s 'symbol->string)
  #;(##sys#symbol->string s)
  (##sys#slot s 1))

(mutate-procedure! symbol->string (lambda (orig) unsafe-symbol->string))
;;** with-mutex

(define-syntax with-mutex
  (syntax-rules ()
    ((with-mutex m body ...)
     (let ((mutex m))
       (guard
	(ex (else (mutex-unlock! mutex) (raise ex)))
	(mutex-lock! mutex)
	(receive r (begin body ...)
	  (mutex-unlock! mutex)
	  (if (and (pair? r) (pair? (cdr r)))
	      (apply values r)
	      (car r))))))))

;;*** Development Aid: On possibly dead locks

(cond-expand
 (debug-lock

  (define *retained-mutex-set* (make-parameter '()))

  (define (hang-on-mutex-report/logerr where mutex)
    (format (current-error-port) "WARNING: ~a may deadlock on ~a\n" where (mutex-name mutex)))

  (define (hang-on-mutex-report/raise where mutex)
    (raise (format "WARNING: ~a may deadlock on ~a\n" where (mutex-name mutex))))

  (define hang-on-mutex-report hang-on-mutex-report/logerr)

  (define-syntax retain-mutex
    (syntax-rules ()
      ((_ m body ...)
       (parameterize
	((*retained-mutex-set* (cons m (*retained-mutex-set*))))
	(with-mutex m body ...)))))

  (define-syntax yield-mutex-set!
    (syntax-rules ()
      ((_ body ...)
       (parameterize
	((*retained-mutex-set* '()))
	body ...))))

  (define-syntax hang-on-mutex!
    (syntax-rules ()
      ((_ where muxlist)
       (for-each
	(lambda (m)
	  (if (memq m (*retained-mutex-set*))
	      (hang-on-mutex-report where m)))
	muxlist)))))
 (else

  (define-syntax retain-mutex
    (syntax-rules ()
      ((_ m body ...) (with-mutex m body ...))))

  (define-syntax yield-mutex-set!
    (syntax-rules ()
      ((_ body ...) (begin body ...))))

  (define-syntax hang-on-mutex!
    (syntax-rules ()
      ((_ m where) (begin ))))
  ))

;;** semaphore

(: semaphore? (* --> boolean : (struct <semaphore>)))
(: semaphore-name ((struct <semaphore>) --> *))
(: semaphore-value ((struct <semaphore>) -> fixnum))
(: semaphore-value-set! ((struct <semaphore>) fixnum -> undefined))
(: semaphore-mutex ((struct <semaphore>) --> (struct mutex)))
(: semaphore-condition ((struct <semaphore>) --> (struct condition-variable)))
(define-record-type <semaphore>
  (internal-make-semaphore name n mutex condition)
  semaphore?
  (name semaphore-name)
  (n semaphore-value semaphore-value-set!)
  (mutex semaphore-mutex)
  (condition semaphore-condition))

(define-record-printer (<semaphore> x out)
  (format out "<semaphore ~a ~a>" (semaphore-name x) (semaphore-value x)))

(: semaphore-delete! ((struct <semaphore>) -> undefined))
(define (semaphore-delete! sema)
  (semaphore-value-set! sema -1)
  (condition-variable-broadcast! (semaphore-condition sema)))

(define-syntax semaphore-wait
  (syntax-rules ()
    ((semaphore-wait sema) (semaphore-wait-by! sema 1))))

(define-syntax semaphore-signal
  (syntax-rules ()
    ((semaphore-signal sema) (semaphore-signal-by! sema 1))))

(define-syntax with-semaphore
  (syntax-rules ()
    ((with-semaphore semaphore body ...)
     (let ((sema semaphore))
       (dynamic-wind
	   (lambda () (semaphore-wait-by! sema 1))
	   (lambda () body ...)
	   (lambda () (semaphore-signal-by! sema 1)))))))

(: make-semaphore (* fixnum --> (struct <semaphore>)))
(define (make-semaphore name n)
  (internal-make-semaphore name n (make-mutex name) (make-condition-variable name)))

#;(define (semaphore-wait-by! sema decrement)
  (let ((n (semaphore-value sema)))
    (if (fx>= n decrement)
	(semaphore-value-set! sema (fx- n decrement))
	(let ((cvar (semaphore-condition sema))
	      (ct ##sys#current-thread))
	  (##sys#setslot cvar 2 (##sys#append (##sys#slot cvar 2) (##sys#list ct)))
	  (##sys#setslot ct 11 cvar)
	  (##sys#setslot ct 1 (lambda () (semaphore-wait-by! sema decrement)))
	  (##sys#setslot ct 3 'sleeping)
	  (##sys#schedule)))))

(: semaphore-wait-by! ((struct <semaphore>) fixnum -> undefined))
(define (semaphore-wait-by! sema decrement)
  (mutex-lock! (semaphore-mutex sema))
  (let ((n (semaphore-value sema)))
    (if (fx>= n decrement)
	(begin
	  (semaphore-value-set! sema (fx- n decrement))
	  (mutex-unlock! (semaphore-mutex sema)))
	(begin
	  (if (eq? n -1)
	      (begin
		(mutex-unlock! (semaphore-mutex sema))
		(raise 'abandoned-semaphore)))
	  (mutex-unlock! (semaphore-mutex sema) (semaphore-condition sema))
	  (semaphore-wait-by! sema decrement)))))

(: semaphore-signal-by! ((struct <semaphore>) fixnum -> undefined))
(define (semaphore-signal-by! sema increment)
  (let ((v (semaphore-value sema)))
    (if (eqv? v -1)
	(begin
	  (condition-variable-broadcast! (semaphore-condition sema))
	  (raise 'abandoned-semaphore))
	(let ((n (fx+ v increment)))
	  (semaphore-value-set! sema n)
	  (if (fx> n 0) (condition-variable-broadcast! (semaphore-condition sema)))))))

(: make-string/uninit (fixnum --> string))
(define (make-string/uninit size)
  (##sys#allocate-vector size #t #f #f))

;; internal pipe

(: make-internal-pipe (#!rest -> input-port output-port))
(define (make-internal-pipe . args)
  (define name (or (and (pair? args) (car args))
		   'internal-pipe))
  (let ((off 0)
	(mutex (make-mutex name))
	(condition (make-condition-variable name))
	(flushed (make-condition-variable name))
	(queue (make-queue))
	(buf ""))
    (define (eof?) (eq? off -1))
    (define (buf-empty?) (fx>= off (string-length buf)))
    (define (read-input!)
      (mutex-lock! mutex #f #f)
      (if (and (not (eof?)) (buf-empty?))
	  (if (queue-empty? queue)
	      (begin
		(condition-variable-broadcast! flushed)
		(mutex-unlock! mutex condition)
		(read-input!))
	      (let ((nx (queue-remove! queue)))
		(if (string? nx)
		    (begin (set! buf nx)
			   (set! off 0))
		    (set! off -1))
		(mutex-unlock! mutex)))
	  (mutex-unlock! mutex)))
    (define (read!)
      (if (eof?) #!eof
	  (if (buf-empty?)
	      (begin (read-input!) (read!))
	      (let ((c (string-ref buf off)))
		(set! off (add1 off))
		c))))
    (define (ready?)
      (and (not (eof?))
	   (or (not (buf-empty?))
	       (not (queue-empty? queue)))))
    (define (read-string port n dest start)
      (let loop ((n n) (m 0) (start start))
	(cond ((eof?) m)
	      ((eq? n 0) m)
	      ((buf-empty?) (read-input!) (loop n m start))
	      (else
	       (let* ((rest (fx- (string-length buf) off))
		      (n2 (if (fx< n rest) n rest)))
		 (##core#inline "C_substring_copy" buf dest off (fx+ off n2) start)
		 (set! off (fx+ off n2))
		 (loop (fx- n n2) (fx+ m n2) (fx+ start n2)) ) ) ) ))
    (define (read-line p limit)
      (let loop ((str #f))
	(cond ((eof?) (or str #!eof))
	      ((buf-empty?) (read-input!) (loop str))
	      (else
	       (receive
		(next line . full-line?)
		(##sys#scan-buffer-line
		 buf
		 (string-length buf)
		 off
		 (lambda (pos2)
		   (let* ((n (fx- pos2 off)))
		     (cond ((fx>= n limit)
			    (values #f pos2 #f))
			   ((eof?) (values #f -1 #f))
			   (else (read-input!)
				 (set! limit (fx- limit n))
				 (cond
				  ((eof?) (values #f -1 #f))
				  ((fx< off (string-length buf))
				   (values buf off
					   (fxmin (string-length buf)
						  (fx+ off limit))))
				  (else (values #f off #f))))) ) ) )
		(##sys#setislot p 4 (fx+ (##sys#slot p 4) 1))
		(set! off next)
		line) ) ) ) )
    (define (close)
      (set! off -1) ;; signal eof
      (condition-variable-broadcast! flushed)
      (condition-variable-broadcast! condition))
    (define (write! s)
      (if (eof?)
	  (%process-error 'internal-pipe-write "port is closed" name)
	  (if (or (and (string? s) (fx> (string-length s) 0))
		  (eof-object? s))
	      (begin
		#;(mutex-lock! mutex) ;; both are with interrupts-disabled
		(queue-add! queue s)
		#;(mutex-unlock! mutex)
		(condition-variable-broadcast! condition) ))))
    (define (flush!)
      (if (not (eof?))
	  (begin
	    (mutex-lock! mutex #f #f)
	    (if (eof?)
		(mutex-unlock! mutex)
		(begin
		  (if (buf-empty?)
		      (if (queue-empty? queue)
			  (mutex-unlock! mutex)
			  (mutex-unlock! mutex flushed))
		      (mutex-unlock! mutex flushed))
		  (flush!))))))
    (values
     (make-input-port read! ready? close #f read-string read-line)
     (make-output-port write! (lambda () (write! #!eof)) flush!))))

(define-foreign-variable C_interrupts_enabled bool "C_interrupts_enabled")

(define-foreign-variable C_timer_interrupt_counter long "C_timer_interrupt_counter")

(define chicken-really-check-interrupts!
  (foreign-lambda void "C_paranoid_check_for_interrupt"))

(define chicken-enable-interrupts! (foreign-lambda void "C_enable_interrupts"))
(define chicken-disable-interrupts! (foreign-lambda void "C_disable_interrupts"))

(define (chicken-check-interrupts!)
  (chicken-really-check-interrupts!)
  (if (fx<= 0 C_timer_interrupt_counter) (thread-yield!)))

(define chicken-check-interrupts/rarely!
   (let ((n 1))
     (lambda ()
       (set! n  (if (fx<= n 0)
		    (begin
		      (chicken-check-interrupts!)
		      10000)
		    (sub1 n))))))

(define (yield-system-load)
  #;(let loop ()
    (if (fx< (##sys#idle-time) 5)
	(begin (thread-sleep! (milliseconds->time (+ 40 (current-milliseconds)))) (loop))))
  (chicken-check-interrupts!))

;; TODO: The posix overwrites belong to their own file!

;; posix replacements maintianing the known fd table

;; A blatant copy to avoid confusion.

(define-foreign-variable errno int "errno")
(define strerror (foreign-lambda c-string "strerror" int))

(: os-error (symbol symbol string #!rest -> . *))
(define os-error			; liftet literally from chicken//posix-common.scm
  (let ((string-append string-append) )
    (lambda (type loc msg . args)
      (let ((rn errno))
	(apply ##sys#signal-hook type loc (string-append msg " - " (strerror rn)) args) ) ) ) )

(define (%process-error tag msg arg)
  (##sys#signal-hook #:process-error tag msg arg))

(define fd-setsize 1024 #;(foreign-value "FD_SETSIZE" int))

;; Data in the table is for debug purpose; just #f indicates the fd is unused.

(define open-fds (make-vector fd-setsize #f))

(: open-fds-fold ((procedure (* *) *) * -> *))
(define (open-fds-fold f init)		; intented only for debug purpose
  (let loop ((i (sub1 (vector-length open-fds))) (init init))
    (if (eqv? i -1)
	init
	(loop (sub1 i)
	      (if (vector-ref open-fds i)
		  (f (cons i (vector-ref open-fds i)) init)
		  init)))))

(define-inline (open-fds-close! fd)
  (vector-set! open-fds fd #f))

(define-inline (open-fds-dup! old new)
  (vector-set! open-fds new (or (vector-ref open-fds old) old)))

(define-inline (open-fds-set! fd state)
  (vector-set! open-fds fd state))

(: set-open-fd! (fixnum * -> . *))
(define (set-open-fd! fd state) (open-fds-set! fd state))

;; Initially we do a guess: 0-2 are open.  We just don't know and
;; there is limited reason so far to really care.
(vector-set! open-fds 0 0)
(vector-set! open-fds 1 1)
(vector-set! open-fds 2 2)

(define (close-all-ports-except . ports)
  (do ((i 0 (add1 i)))
      ((eqv? i fd-setsize) #t)
    (cond
     ((not (vector-ref open-fds i))
      )
     ((null? ports) (file-close! i))
     ((memv i ports) (set! ports (delete i ports)))
     (else (file-close! i)))))

(: file-close! (fixnum -> fixnum))
(define file-close!
  (lambda (fd)
    (dbg "PID ~a closing fd ~a\n" (current-process-id) fd)
    (let ((x (##core#inline "C_close" fd)))
      (cond
       ((eq? 0 x) (open-fds-close! fd) 0)
       ((foreign-value "(errno == EINTR)" bool)
	(##sys#dispatch-interrupt (lambda () (file-close! fd))))
       (else x)))) )

(: file-close (fixnum -> undefined))
(define file-close
  (lambda (fd)
    (##sys#check-exact fd 'file-close)
    (when (fx< (file-close! fd) 0)
      (os-error #:file-error 'file-close "cannot close file" fd) ) ) )

(: duplicate-fileno (fixnum #!optional fixnum -> fixnum))
(define duplicate-fileno
  (lambda (old . new)
    (##sys#check-exact old duplicate-fileno)
    (let ([fd (if (null? new)
                  (##core#inline "C_dup" old)
                  (let ([n (car new)])
                    (##sys#check-exact n 'duplicate-fileno)
                    (##core#inline "C_dup2" old n) ) ) ] )
      (if (fx< fd 0)
	  (os-error #:file-error 'duplicate-fileno "cannot duplicate file-descriptor" old)
	  (open-fds-dup! old fd))
      fd) ) )

;; This one is a clumsy hack.

(define create-pipe
  (let ((inp '(pipe input))
	(outp '(pipe output)))
    (lambda ()
      (call-with-values posix:create-pipe
	(lambda (in out)
	  (open-fds-set! in inp)
	  (open-fds-set! out outp)
	  (values in out))))) )

;; process i/o
(define-constant buffer-size 1024)

(set-signal-handler! signal/pipe #f)

#;(define-syntax end-of-file
  (syntax-rules () ((end-of-file) #!eof)))

(define-inline (end-of-file) #!eof)

(define process-io-times (vector 0 1 0))

(define process-io-stats
  (let ((/ /))
    (lambda ()
      (values (/ (vector-ref process-io-times 0) (vector-ref process-io-times 1))
	      (vector-ref process-io-times 2)))))

(: process-io-update-bandwith! (fixnum number -> undefined))
(define process-io-update-bandwith!
  (let ((* *)
	(+ +)
	(- -)
	(/ /)
	(thousend (exact->inexact 1000))
	(ten (exact->inexact 10))
	(one (exact->inexact 1)))
    (lambda (size start-time)
      (let* ((delta (exact->inexact
		     (- start-time (current-milliseconds))))
	     (bps (/ (exact->inexact size) (max delta one))))
	(vector-set! process-io-times 0 (+ (vector-ref process-io-times 0) bps))
	(vector-set! process-io-times 1 (add1 (vector-ref process-io-times 1)))
	(vector-set! process-io-times 2 
		     (let ((x (vector-ref process-io-times 2)))
		       (/ (+ (* x 9) (- bps x)) ten)))))))

(define (make-pthread-from-thread-id tid)
  (tag-pointer tid 'pthread))

(define (pthread? x)
  (and (tagged-pointer? x) (eq? (pointer-tag x) 'pthread)))

;; This is a bad hack to avoid changeing the process-reference from an
;; int into a structure.  However the latter would be much better.
;; 2016: reconsidering, we'll do what RScheme, scsh et al did: the
;; "much better thing", but in the scsh style: try to stay backward
;; compatible.  (Currently bloats the code.  TBD: complete the switch.)

(define-record process pid exit-status ok? waiting)
(define-record-printer (process x out)
  (format out "<process pid ~a ~a ~a>" (process-pid x) (process-ok? x) (process-exit-status x)))

(define-inline (process->values e)
  (values (process-pid e) (process-ok? e) (process-exit-status e)))

(define processes-running (make-hash-table fx= number-hash))

(define-inline (processes-running-ref pid)
  (hash-table-ref/default processes-running pid #f))

(define-inline (processes-running-delete! pid)
  (hash-table-delete! processes-running pid))

(define (running-processes-fold f i)
  (hash-table-fold processes-running (lambda (k v i) (f v i)) i))

(define-inline (process-running pid)
  (let ((process (make-process pid #f #f (make-condition-variable pid))))
    (hash-table-set! processes-running pid process)
    process))

(define-inline (process-unknown-exit! p f s)
  (hash-table-set! processes-running p (make-process p f s (make-condition-variable p))))

(define-foreign-variable C_interrupts_enabled bool "C_interrupts_enabled")
(define (chicken-simple-enable-interrupts!) (set! C_interrupts_enabled #t))

;(chicken-disable-interrupts!)

;(define-inline (chicken-gc-false) (gc #f))
;(define-inline (chicken-gc-true) (gc #t))

(define-inline (chicken-gc-false) #f)
(define-inline (chicken-gc-true) #f)

(define (total-safe-fork)
  (let* ((ie C_interrupts_enabled #;(##sys#fudge 14))
	 (pid (begin
		(if ie (chicken-disable-interrupts!))
		(chicken-gc-true)
		(atomic:process-fork))))
    (case pid
      ((0) (set! processes-running (make-hash-table fx= number-hash)) 0)
      ((-1) (if ie (chicken-simple-enable-interrupts!)) (format (current-error-port) "fork(2) failed") (error "fork(2) failed"))
      (else
       (let ((p (process-running pid)))
	 (dbg "sFork ~a\n" pid)
	 ;; Again to be sure we have enough room when signals are expected.
	 ;; (gc) ;; (gc #f) avoid passing default argument
	 (if ie (chicken-simple-enable-interrupts!))
	 p)))))

(define (half-safe-fork)
  (let ((pid (atomic:process-fork)))
    (case pid
      ((0) (set! processes-running (make-hash-table fx= number-hash)) 0)
      ((-1) -1)
      (else (dbg "nFork ~a\n" pid) (process-running pid)))))

(define save-fork total-safe-fork)

(: process-waitpid-signal-pending! ((struct process) boolean fixnum -> undefined))
(define (process-waitpid-signal-pending! e f s)
  (process-exit-status-set! e s)
  (process-ok?-set! e f)
  (processes-running-delete! (process-pid e))
  (condition-variable-broadcast! (process-waiting e)))

(define (process-waitpid nohang)
  (let loop ()
    (receive
     (p f s) (##sys#process-wait -1 nohang) ; wait for any child process
     (dbg "process-wait ~a\n" p)
     (if (fx> p 0)
	 (let ((e (processes-running-ref p)))
	   (if e
	       (process-waitpid-signal-pending! e f s)
	       (process-unknown-exit! p f s))
	   (loop)))))
  ;; BEWARE: this appears to be required to avoid the mutation_stack
  ;; growing beyond bounds.
  ;; (chicken-gc-false) ;; (gc #f) avoid passing default argument
  )

#|
(define process-mutex (make-mutex "child exit"))
(define (process-signal/chld signum) (mutex-unlock! process-mutex))
(define process-waitpid-thread
  (thread-start!
   (make-thread
    (lambda ()
      (let loop ()
	(process-waitpid #t)
	(mutex-lock! process-mutex 1 #f) ; should be much more and signal handler enabled
	(loop)))
    "wait for children")))
|#

(define process-waitpid-thread
  (thread-start!
   (make-thread
    (lambda ()
      (let ((ct (current-thread)))
	(let loop ()
	  (process-waitpid #t)
	  (##sys#setslot ct 3 'blocked)
	  (##sys#setslot ct 1 loop)
	  (##sys#schedule))))
    "wait for children")))

(define (process-signal/chld signum) (##sys#thread-unblock! process-waitpid-thread))

;; (define (process-signal/chld signum) (process-waitpid #t))

(set-signal-handler! signal/chld process-signal/chld)

(: wait-for-process ((struct process) -> (or fixnum false) boolean (or fixnum false)))
(define (wait-for-process e)
  (if (process-exit-status e)
      (process->values e)
      (receive
       (p f s) (##sys#process-wait (process-pid e) #t)
       (dbg "process-wait-for-pid ~a ~a: <= ~a\n" (current-thread) (process-pid e) p)
       (if (fx= p 0)
	   (begin
	     (mutex-unlock! (make-mutex) (process-waiting e))
	     (wait-for-process e))
	   (begin
	     (if (fx= p -1)
		 (os-error #:process-error 'process-wait "waiting for child process failed" e)
		 ;;(values -1 #f s)
		 (begin
		   (process-waitpid-signal-pending! e f s)
		   (values p f s))))))))

(define (really-process-wait-for-pid pid)
  (##sys#check-exact pid 'process-wait-for-pid)
  (format (current-error-port) "Deprecated code path taken: really-process-wait-for-pid ~a\n" pid)
  (let ((e (hash-table-ref/default processes-running pid #f)))
    (if e
	(wait-for-process e)
	(error "process-wait-for-pid: unknown pid ~" pid))))

(define (process-wait-for-pid p)
  (cond
   ((process? p) (wait-for-process p))
   ((pthread? p) ;; FIXME: this should do something, however now the only use is from sslsocket.
    (values p #f 0))
   (else (really-process-wait-for-pid p))))

(define (really-process-test-pid pid)
  (##sys#check-exact pid 'process-test-pid)
  (format (current-error-port) "Deprecated code path taken: really-process-test-pid ~a\n" pid)
  (let ((gone (processes-running-ref pid)))
    (if gone
	(begin
	  (processes-running-delete! pid)
	  (dbg "testpid AGAIN ~a\n" gone)
	  (process->values gone))
	(receive
	 (p f s) (##sys#process-wait pid #t)
	 (dbg "process-test-pid ~a: <= ~a\n" pid p)
	 (if (and gone (fx= p pid))
	     (process-waitpid-signal-pending! gone f s))
	 (if (fx= p -1)
	     (values pid #f s)
	     (values p f s))))))

(define (process-test-pid pid)
  (cond
   ((process? pid)
    (if (process-exit-status pid) (process->values pid) (values 0 #f #f)))
   ((pthread? pid) ;; FIXME: this should do something, however now the only use is from sslsocket.
    (values pid #f 0))
   (else (really-process-test-pid pid))))

(define (%get-rid-of-pid pid)
  (dbg "get-rid-of-pid ~a ~a\n" (current-thread) (process-pid pid))
  (assert (process? pid))
  (receive
   (p f s) (process-test-pid pid)
   (if (fx= p 0)
       (let ((t (thread-start!
		 (make-thread (lambda () (wait-for-process pid)) (process-pid pid))))
	     (pid (process-pid pid)))
	 (let loop ((turn 1))
	   (guard
	    (ex ((join-timeout-exception? ex)
		 (cond
		  ((fx>= turn 8)
		   (if (fx= (atomic:process-signal pid signal/term) 0)
		       (loop (fx+ turn turn))))
		  ((fx>= turn 16)
		   (if (fx= (atomic:process-signal pid signal/kill) 0)
		       (loop (fx+ turn turn))))
		  ((fx>= turn 64) #f)	; give up
		  (else
		   (loop (fx+ turn turn)))))
		(else ex))
	    (thread-join! t turn)))))))

(define (get-rid-of-pid pid)
  (cond
   ;; ((process? p) (wait-for-process p))
   ((pthread? pid) ;; FIXME: this should do something, however now the only use is from sslsocket.
    #t)
   (else (%get-rid-of-pid pid))))

(: process-wait ((or (struct process) fixnum) #!optional boolean -> fixnum * fixnum))
(define (process-wait pid . rest)
  (if (fixnum? pid)
      (let ((nohang (and (pair? rest) (car rest))))
	(assert (fx>= pid 0))
	((if nohang process-test-pid process-wait-for-pid) pid))
      (let ((nohang (and (pair? rest) (car rest))))
	(if nohang
	    (if (process-exit-status pid) (process->values pid) (values 0 #f #f))
	    (wait-for-process pid)))))

(: process-signal ((or (struct process) fixnum) fixnum -> undefined))
(define (process-signal p s)
  (posix:process-signal (if (process? p) (process-pid p) p) s))

(: process-io-ports ((struct process) fixnum fixnum -> input-port output-port))
(define process-io-ports
  (let ((make-input-port make-input-port)
	(make-output-port make-output-port)
	(substring substring)
	(file-close-fd! file-close!) ;; (foreign-lambda* int ((int fd)) "C_return(close(fd));")
	(bailout %process-error))
    (lambda (pid fdr fdw)
      (make-file-nonblocking! fdw)
      (make-file-nonblocking! fdr)	; should not be required
      (let ((buf (make-string/uninit buffer-size))
	    ;; (data (vector #f #f #f))
	    (buflen 0)
	    (bufindex 0)
	    (iclosed #f)
	    (oclosed #f))
	(define (eof?) iclosed #;(eq? #!eof buf))
	(define (buf-empty?) (fx>= bufindex buflen))
	(define (process-io-read!)
	  (and (not iclosed)
	       (let ((n (##core#inline "AC_read" fdr buf buffer-size)))
		 (cond
		  ((fx= n -1)
		   (cond
		    ((##core#inline "AC_again")
		     (dbg "rewait/read ~a ~a from pid ~a fd ~a\n" (current-process-id) (current-thread) pid fdr)
		     (thread-wait-for-i/o! fdr #:input)
		     (process-io-read!))
		    ((foreign-value "AC_interrupted()" bool)
		     (dbg "interrupted/read ~a ~a from pid ~a fd ~a\n" (current-process-id) (current-thread) pid fdr)
		     (##sys#dispatch-interrupt process-io-read!))
		    (else
		     (close-input)
		     (bailout 'process-io-ports "cannot read" fdr))))
		  ((fx<= n 0)
		   (close-input))
		  (else
		   (set! buflen n)
		   (set! bufindex 0))) )))
	(define (read!)
	  (if (and (not iclosed) (buf-empty?))
	      (process-io-read!))
	  (if (eof?)
	      (end-of-file)
	      (let ((c (##core#inline "C_subchar" buf bufindex)))
		(set! bufindex (fx+ bufindex 1))
		c) ))
	(define (read-string p n dest start)
	  (let loop ((n n) (m 0) (start start))
	    (cond ((eof?) m)
		  ((eq? n 0) m)
		  ((buf-empty?) (process-io-read!) (loop n m start))
		  (else
		   (let* ((rest (fx- buflen bufindex))
			  (n2 (if (fx< n rest) n rest)))
		     (##core#inline "C_substring_copy" buf dest bufindex (fx+ bufindex n2) start)
		     (set! bufindex (fx+ bufindex n2))
		     (loop (fx- n n2) (fx+ m n2) (fx+ start n2)) ) ) ) ))
	(define (close-input)
	  (unless
	   (eof?)
	   (set! iclosed #t)
	   (set! buf #f)
	   (let ((err (and (eq? -1 (file-close-fd! fdr))
			   (string-append
			    "can not close fd input port"
			    (strerror errno) (number->string fdr)))))
	     (if oclosed
		 (receive
		  (p f s) (process-wait-for-pid pid)
		  (if err (bailout 'process-io-ports err fdr))
		  s)
		 (receive (p f s) (process-test-pid pid)
			  (when s ;; (eqv? p pid)
				(when
				 (not oclosed)
				 (set! oclosed #t)
				 (when (eq? -1 (file-close-fd! fdw))
				       (bailout "can not close fd output port"
						(if err
						    (string-append err " AND " (strerror errno))
						    (strerror errno))
						fdw)))
				(if err (bailout 'process-io-ports err fdr))
				s))))))
	(define (close-output)
	  (unless
	   oclosed
	   (set! oclosed #t)
	   (let ((err (and (eq? -1 (file-close-fd! fdw))
			   (string-append
			    "can not close fd input port"
			    (strerror errno) " " (number->string fdr)))))
	     (if iclosed
		 (receive
		  (p f s) (process-wait-for-pid pid)
		  (if err (bailout 'process-io-ports err fdr))
		  s)
		 (receive (p f s) (process-test-pid pid)
			  (when s ;; (eqv? p pid)
				(when (not iclosed)
				      (set! iclosed #t)
				      (if (eq? -1 (file-close-fd! fdr))
					  (bailout
					   (if err
					       (string-append err " AND ")
					       "can not close fd input port")
					   (strerror errno) fdw)))
				(if err (bailout 'close-output err fdr))
				s))))))
	(let ((in
	       ;;(make-input-port read! ready? close #f read-string)
	       (make-input-port
		(lambda ()		; read
		  (when (buf-empty?) (process-io-read!) )
		  (if (or iclosed (buf-empty?))
		      (end-of-file)
		      (let ((c (##core#inline "C_subchar" buf bufindex)))
			(set! bufindex (fx+ bufindex 1))
			c) ) )
		(lambda () (not iclosed)) ; ready
		close-input
		#f
		read-string
		) )
	      (out
	       (make-output-port
		(lambda (s)
		  (define start-time (current-milliseconds))
		  (let process-io-write ((len (##sys#size s))
					 (off 0))
		    (if oclosed
			(error "output port is closed")
			(if (fx> len 0)
			    (let ((n (##core#inline "AC_write" fdw s off len)))
			      (if (eq? -1 n)
				  (cond
				   ((##core#inline "AC_again")
				    (thread-wait-for-i/o! fdw #:output)
				    (process-io-write len off))
				   ((foreign-value "AC_interrupted()" bool)
				    (##sys#dispatch-interrupt (lambda () (process-io-write len off))))
				   (else
				    (bailout "can not write to fd" (strerror errno) fdw) ) )
				  (if (fx< n len)
				      (process-io-write (fx- len n) (fx+ off n))
				      (process-io-update-bandwith! (##sys#size s) start-time))) )))))
		close-output
		(lambda () #t) ) ) )
;	(##sys#setslot (##sys#port-data in) 0 data)
;	(##sys#setslot (##sys#port-data out) 0 data)
;        (set-finalizer! in (lambda (port) (close-input-port port)))
;        (set-finalizer! out (lambda (port) (close-output-port port)))
#|
        ;;
	;; EXPERIMENTAL: This thread might be outright useless and
	;; plain waste.  I'm trying to find why it looks as if chicken
	;; would miss out on gone connections.
	(thread-start!
	 (make-thread
	  (lambda ()
	    (receive
	     (p f s) (process-wait-for-pid pid)
	     (handle-exceptions ex #f (close-output-port out))
	     (handle-exceptions ex #f (close-input-port in))))
	  #f;; (format "[~a] wait" pid)
	  ))
|#
	(values in out) )))))


(: net-io-ports (fixnum			     ; socket
		 *			     ; peer
		 (procedure (fixnum) fixnum) ; close read side
		 (procedure (fixnum) fixnum) ; close write side
		 (procedure (fixnum) fixnum) ; close socket
;;		 (procedure (symbol string #!rest) . *) ; bailout
		 -> input-port output-port))
(define net-io-ports
  (let ((make-input-port make-input-port)
	(make-output-port make-output-port)
	(substring substring)
	(bailout
	 (lambda (tag message . rest)
	   (apply os-error #:network-error tag message rest))))
    (lambda (fd peer shutdown-read shutdown-write closesocket)
      (make-socket-nonblocking! fd)
      (let ((buf (make-string/uninit buffer-size))
	    (buflen 0)
	    (bufindex 0)
	    (iclosed #f)
	    (oclosed #f))
	(define (eof?) iclosed #;(eq? #!eof buf))
	(define (buf-empty?) (fx>= bufindex buflen))
	(define (close-input)
	  (unless (eof?)
		  (set! iclosed #t)
		  (set! buf #f)
		  (when (eq? -1 (shutdown-read fd))
			)
		  (if oclosed (closesocket fd))
		  ))
	(define (close-output)
	  (unless oclosed
		  (set! oclosed #t)
		  (when (eq? -1 (shutdown-write fd))
			)
		  (if iclosed (closesocket fd))) )
	(define (net-io-read!)
	  (and (not iclosed)
	       (let ((n (##core#inline "AC_read" fd buf buffer-size)))
		 (cond
		  ((##core#inline "AC_again")
		   (dbg "rewait/read ~a fd ~a\n" (current-thread) fd)
		   (thread-wait-for-i/o! fd #:input)
		   (net-io-read!))
		  ((foreign-value "AC_interrupted()" bool)
		   (##sys#dispatch-interrupt net-io-read!))
		  ((fx<= n 0) (close-input))
		  (else
		   ;; (print "[rd: " n "]")
		   (set! buflen n)
		   (set! bufindex 0))) )))
	(define (read-string p n dest start)
	  (let loop ((n n) (m 0) (start start))
	    (cond ((eof?) m)
		  ((eq? n 0) m)
		  ((buf-empty?) (net-io-read!) (loop n m start))
		  (else
		   (let* ((rest (fx- buflen bufindex))
			  (n2 (if (fx< n rest) n rest)))
		     (##core#inline "C_substring_copy" buf dest bufindex (fx+ bufindex n2) start)
		     (set! bufindex (fx+ bufindex n2))
		     (loop (fx- n n2) (fx+ m n2) (fx+ start n2)) ) ) ) ))
	(let ((in
	      (make-input-port
	       (lambda ()
		 (when (fx>= bufindex buflen)
		       (net-io-read!))
		 (if (or iclosed (fx>= bufindex buflen))
		     (end-of-file)
		     (let ((c (##core#inline "C_subchar" buf bufindex)))
		       (set! bufindex (fx+ bufindex 1))
		       c) ) )
	       (lambda () (not iclosed))
	       close-input
	       #f
	       read-string
	       ) )
	     (out
	      (make-output-port
	       (lambda (s)
		 (define start-time (current-milliseconds))
		 (let io-write ((len (##sys#size s))
				(off 0))
		   (if oclosed
		       (error "output port is closed")
		       (if (fx> len 0)
			   (let ((n (##core#inline "AC_write" fd s off len)))
			     (if (eq? -1 n)
				 (cond ((##core#inline "AC_again")
					(thread-wait-for-i/o! fd #:output)
					(io-write len off))
				       ((foreign-value "AC_interrupted()" bool)
					(##sys#dispatch-interrupt (lambda () (io-write len off))))
				       (else
					(close-output)
					(bailout 'net-io-write
						 "can not write to fd" fd len) ) )
				 (if (fx< n len)
				     (io-write (fx- len n) (fx+ off n))
				     (process-io-update-bandwith! (##sys#size s) start-time) )) )))))
	       close-output
	       (lambda () #f) ) ) )
	     (values in out) ) ) ) ))

#;(receive
 (i o) (process-io-ports (parent-process-id) 0 1)
 (current-input-port i)
 (current-output-port o))

(: tq-entry? (* --> boolean : (struct <tq-entry>)))

(: entry-successor ((struct <tq-entry>) -> (or false (struct <tq-entry>))))
(: entry-predecessor ((struct <tq-entry>) -> (or false (struct <tq-entry>))))
(: entry-time ((struct <tq-entry>) --> *))
(: entry-value ((struct <tq-entry>) -> *))
(: set-entry-value! ((struct <tq-entry>) * -> undefined))

(define-record-type <tq-entry>
  (%make-tq-entry successor predecessor time value)
  tq-entry?
  (successor entry-successor entry-set-succ!)
  (predecessor entry-predecessor entry-set-pred!)
  (time entry-time)
  (value entry-value set-entry-value!))

(define (tq-entry-remove! lst)
  (if (entry-successor lst)
      (begin
	(entry-set-succ! (entry-predecessor lst) (entry-successor lst))
	(entry-set-pred! (entry-successor lst) (entry-predecessor lst))
	(entry-set-succ! lst #f)
	(entry-set-pred! lst #f)
	(entry-value lst))
      (begin
	;; TODO remove along with the boolean alternative in entry-successor
	(raise 'tq-entry-remove!--called-again)
	(entry-value lst))))

(define-inline (make-tq-entry time value)
  (%make-tq-entry #f #f time value))

(define (tq-entry-insert-after! lst task)
  (entry-set-pred! task lst)
  (entry-set-succ! task (entry-successor lst))
  (entry-set-pred! (entry-successor lst) task)
  (entry-set-succ! lst task)
  task)

(: tq? (* --> boolean : (struct <tq>)))
(: tq-mutex ((struct <tq>) --> (struct mutex)))
(: tq-queue ((struct <tq>) --> (struct <tq-entry>)))
(define-record-type <tq>
  (%make-tq mutex queue)
  tq?
  (mutex tq-mutex)
  (queue tq-queue))

(define (make-tq name)
  (let ((r (make-tq-entry #f #f)))
    (entry-set-succ! r r)
    (entry-set-pred! r r)
    (%make-tq (make-mutex name) r)))

(: tq-name ((struct <tq>) --> *))
(define (tq-name tq)
  (mutex-name (tq-mutex tq)))

(: %entry-insert! ((struct <tq-entry>) (procedure (* *) . *) * * -> *))
(define (%entry-insert! tq pred time task)
  (let loop ((lst (entry-predecessor tq)))
	(cond
	 ((eq? lst tq) (tq-entry-insert-after! lst task))
	 ((pred time (entry-time lst)) (tq-entry-insert-after! lst task))
	 (else (chicken-check-interrupts!)
	       (loop (entry-predecessor lst))))))

(: tq-insert! ((struct <tq>) (procedure (* *) . *) * * -> *))
(define (tq-insert! tq pred time obj)
  (and obj
       (with-mutex (tq-mutex tq)
		   (%entry-insert!
		    (tq-queue tq) pred time obj))))

(: tq-remove! ((struct <tq>) * -> *))
(define (tq-remove! tq obj)
  (let ((v (with-mutex (tq-mutex tq) (tq-entry-remove! obj))))
    (if v (set-entry-value! obj #f))
    v))

(: tq-invalidate! ((struct <tq-entry>) -> undefined))
(define (tq-invalidate! obj)
  (let ((v (entry-value obj)))
    (if v (set-entry-value! obj #f))))

(define (sudo! runuser)
  (and-let* ((fixnum? runuser))
	    (let* ((ui (user-information runuser))
		   (user-id (list-ref ui 2)))
	      (let ((cu (current-user-id)))
		(and-let* (((not (eqv? cu user-id)))
			   (g (list-ref ui 3)))
			  (if (eqv? cu 0)
			      (initialize-groups runuser g))
			  (set! (current-group-id) g)
			  (set! (current-user-id) user-id)
			  #t)))))

(: make-process-stub
   ((list-of string) (or false string) #!rest
    -> fixnum output-port input-port))
(define make-process-stub
  (let ()
    (define (make-user-process-stub runuser thunk dir env)
      ;; FIXME 'env' and 'dir' not yet used
      (receive (tr tw) (create-pipe)
	       (receive (fr fw) (create-pipe)
			(let ((pid (save-fork)))
			  (if (eqv? pid 0)
			      (guard
			       (ex (else (_exit 1)))
			       (let ((pid (current-process-id)))
				 (sudo! runuser)
				 ;; (file-close cr) (file-close cw)
				 (duplicate-fileno tr 0)
				 (duplicate-fileno fw 1)
					;(format (current-error-port) "~a force major gc\n" pid)
					;(chicken-gc-true)
					;(format (current-error-port) "~a close all ports\n" pid)
				 (close-all-ports-except 0 1 2)
					;(format (current-error-port) "~a run ~s\n" pid thunk)
				 (if dir (change-directory dir))
				 (thunk) ;; (##sys#kill-other-threads thunk)
				 (_exit 0)))
			      (begin
				(file-close! tr) (file-close! fw)
					;(make-file-nonblocking! tw)
				(receive (in out) (process-io-ports pid fr tw)
					 (dbg "process ~a out ~a in ~a\n" pid tw fr)
					 (values pid out in))))))))

    (define (make-process-stub args dir . env)
      (make-user-process-stub
       #f
       (if (procedure? args)
           args
           (lambda () (process-execute (car args) (cdr args))))
       dir
       (and (pair? env) (car env))))

    make-process-stub))

;; Unused anyway, but will spell problems if threads running in
;; parallel to THUNK modify global ressources (like files or their
;; descriptors).
;;
;; (define (process-call thunk)
;;   (receive
;;    (pid out in)
;;    (receive (tr tw) (create-pipe)
;; 	    (receive (fr fw) (create-pipe)
;; 		     (gc #f)
;; 		     (let ((pid (process-fork)))
;; 		       (if (eqv? pid 0)
;; 			   (guard
;; 			    (ex (else (_exit 1)))
;; 			    (receive
;; 			     (in out)
;; 			     (process-io-ports (current-process-id) tr fw)
;; 			     (write (thunk) out)
;; 			     (close-output-port out)
;; 			     (close-input-port in)
;; 			     (_exit 0)))
;; 			   (begin
;; 			     (file-close tr) (file-close fw)
;; 			     (receive (in out) (process-io-ports pid fr tw)
;; 				      (values pid out in)))))))
;;    (let ((result (read in)))
;;      (close-output-port out)
;;      (close-input-port in)
;;      result)))

(: process-call ((procedure () *) -> *))
(define (process-call thunk)
  (let ((thunk-result (thunk)))
    (receive
     (pid out in)
     (receive (tr tw) (create-pipe)
	      (receive (fr fw) (create-pipe)
		       (let ((pid (save-fork)))
			 (if (eqv? pid 0)
			     (guard
			      (ex (else (_exit 1)))
			      (receive
			       (in out)
			       (process-io-ports (process-running (current-process-id)) tr fw)
			       (write thunk-result out)
			       (close-output-port out)
			       (close-input-port in)
			       (_exit 0)))
			     (begin
			       (file-close! tr) (file-close! fw)
			       (receive (in out) (process-io-ports pid fr tw)
					(values pid out in)))))))
     (let ((result (read in)))
       (close-output-port out)
       (close-input-port in)
       result))))

) ;; module atomic
