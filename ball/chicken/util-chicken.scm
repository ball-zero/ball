;; (C) 2002, 2003, 2013 Joerg F. Wittenberger see http://www.askemos.org

;; TODO overwrite chicken'S port redirection code with more
;; restrictive versions.  See chicken mailing list 1-3 Mar 2003.

(declare
 (unit util)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;; (fixnum-arithmetic)
 (disable-interrupts) ;; some tings are called from uninterruptible code.
 (not usual-integrations raise signal error current-exception-handler)
 (foreign-declare "#include <time.h>")
 #;(foreign-declare #<<EOF

static void A_addstr5(C_word c, C_word self, C_word k,
                      C_word s_buf, C_word s_i, C_word s_str, C_word s_j,
                      C_word s_flush) C_noret;

static void A_addstr5(C_word c, C_word self, C_word k,
                      C_word s_buf, C_word s_i, C_word s_str, C_word s_j,
                      C_word s_flush)
{
  int i = C_unfix(s_i), j = C_unfix(s_j);
                         /* string-length coded inline */
  unsigned int buf_max = C_header_size(s_buf) - i;
  unsigned int str_max = C_header_size(s_str) - j;
  unsigned int n = buf_max < str_max ? buf_max : str_max;

#if 0
  register C_char *buf = (C_char *)C_data_pointer(s_buf) + i;
  register C_char *str = (C_char *)C_data_pointer(s_str) + j;
  register C_char *end = str + n;

  while(str != end) *buf++=*str++;
#else
  C_memcpy(C_data_pointer(s_buf) + i, C_data_pointer(s_str) + j, n);
#endif
  i += n;
  if( n == buf_max )
    /* end recursive call of s_flush; self is the current closure */
    ((C_proc7)C_fast_retrieve_proc(s_flush))
     (7, s_flush, k, s_buf, C_fix(i), s_str, C_fix(j + n), self);
    C_kontinue(k, C_fix(i));  /* call the current continuation */
}
EOF
)
 (foreign-declare #<<EOF

static void A_addstr5(C_word c, C_word *av) C_noret;
static void A_addstr5(C_word c, C_word *av)
{
  C_word self = av[0], k = av[1],
  s_buf = av[2], s_i = av[3], s_str = av[4],
  s_j = av[5], s_flush = av[6];

  int i = C_unfix(s_i), j = C_unfix(s_j);
                         /* string-length coded inline */
  unsigned int buf_max = C_header_size(s_buf) - i;
  unsigned int str_max = C_header_size(s_str) - j;
  unsigned int n = buf_max < str_max ? buf_max : str_max;

#if 0
  register C_char *buf = (C_char *)C_data_pointer(s_buf) + i;
  register C_char *str = (C_char *)C_data_pointer(s_str) + j;
  register C_char *end = str + n;

  while(str != end) *buf++=*str++;
#else
  C_memcpy(C_data_pointer(s_buf) + i, C_data_pointer(s_str) + j, n);
#endif
  i += n;
  if( n == buf_max ) {
    /* end recursive call of s_flush; self is the current closure */
  av[0] = s_flush;
  av[3] = C_fix(i);
  av[5] = C_fix(j + n);
  av[6] = self;
  C_do_apply(7, av);
  } else {
    C_kontinue(k, C_fix(i));  /* call the current continuation */
  }
}
EOF
)
 )

(module
 util
 (
  dbgname
  ;; internals to be avoided if possible
  flush-output-port
  ;;
  receive* receive-srfi
  ;;
  always-assert
  ;;
  andmap ormap
  string-split/include-empty
  make-string/uninit
  ;;
  monitor-value
  combine list->values arithmetic-shift-right arithmetic-shift-left
  remove-file file-directory? dirname scandir remove-dir remove-dir-recursive link
  cwd file-empty?
  set-file-modification-time!
  copy-file-data! file->string filedata
  filesystem-free find-in-path
  mkdirs within-directory make-temporary-directory call-with-temporary-directory
  thread-list thread-sleep!/ms thread-deliver-signal!
  read-bytes string-splice!
  string-prefix-length+
  not-eof-object? read-all-expressions
  process-kill
  *set-child-uid* run-child-process
  ;;
  condition->fields condition->fields+ condition->string
  ;;
  ;; lolevel
  ;;
  memory-set!
  ;;
  ;; regex
  ;;
  reg-expr->proc
  has-suffix?
  content-type-charset xml-pi-regex xml-pi-encoding
  html-regex
  is-proxy-request?
  http-prefix? http-is-login? http-is-logout? http-is-post-url? http-is-check-url?
  http-cmd-regex http-status-regex http-location-regex
  lmtp-from-regex lmtp-destination-email-regex
  html-only-client
  string-split-last to-string
  broken-303-client user-agent-ie user-agent-amaya user-agent-mozilla
  nunu-regexp nunu-include-regexp bold-regexp url-regexp nunu-oid-regexp
  colon-split-regex
  ;;
  strip-html-suffix
  sql-quote
  lout-collapsable-whitespace-sequence-regexp illegal-lout?
  lout-literal-char-sequence-regexp
  ;;
  ;; general
  ;;
  bind-exit call-with-list-extending call-with-list-extending1
  domain-error
  set-log-output!
  logit flush-log-queue! logerr logapperr logcond logcond-full logcond-short log-condition
  debug user-debug set-user-debug!
  hash-table->key-vector key-sequence count-keys
  to-string* ;; timestamp ; moved to timeout.scm
  map-matches-alternate
  ;;
  rfc-822-time-string-format rfc-822-timestring timezone-offset timezone-tzset
  iso-8601-time-string-format iso-8601-timestring
  date->time-literal
  ;;
  oid? oid=? make-oid-table
  make-symbol-table make-string-table make-fixnum-table make-character-table
  ;;
  mutex-owner mutex-lock!!
  register-location-format!
  read-locator write-locator
  location-format? location-format ;; FIXME, should BOTH not be exported.

  ;;
  reset-trustedcode! register-trustedcode! get-trusted-code
  with-output-through-lout with-output-through-htmldoc
  ;;
  ;; strings
  ;;
  string-replw string-left string-right
  apply-string-append
  ;;
  ;; config
  make-config-parameter
  local-id
  ;; Storage Adaptor and their configuration
  ;; HTTP protocol variables
  $client-lookups?
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 condition? promise? with-exception-handler vector-copy!)
	foreign
	shrdprmtr
	srfi-1 srfi-13 (except srfi-18 raise)
	srfi-19 srfi-34 srfi-35 (prefix srfi-35 srfi-35:) srfi-43 srfi-69
	openssl atomic mailbox clformat
	ports files extras data-structures irregex regex)

(import (only timeout timeout-object?))

(import (only lolevel move-memory!))

(include "typedefs.scm")

(import (except
	 posix
	 file-close create-pipe
	 duplicate-fileno
	 ;;
	 process-wait process-signal))

#;(define-syntax dbgname
  (syntax-rules ()
    ((_ base . rest) base)))

(define-syntax dbgname
  (syntax-rules ()
    ((_ base fmt rest ...) (format fmt base rest ...))))

(define-syntax always-assert
  (syntax-rules ()
    ((_ expr)
     (if (not expr) (raise (format "runtime assertion failed ~a" 'expr))))
    ((_ expr condition)
     (if (not expr) (raise condition)))
    ((_ expr msg ...)
     (if (not expr) (raise (format msg ...))))))

;; receive* is a hack used to migrate from the rscheme 'bind'
;; construct to normal receive.
(define-syntax receive*
  (syntax-rules ()
    ((receive* vars expr body ...)
     (receive vars expr body ...))))

(define-syntax receive-srfi
  (syntax-rules ()
    ((receive-srfi vars expr body ...)
     (receive vars expr body ...))))

(define-syntax flush-output-port
  (syntax-rules ()
    ((flush-output-port port) (flush-output port))))

(define (andmap f first . rest)
  (cond ((null? rest)
         (let loop ((l first))
           (or (null? l)
               (and (f (car l)) (loop (cdr l))))))
        ((null? (cdr rest))
         (let loop ((l1 first) (l2 (car rest)))
           (or (null? l1)
               (and (f (car l1) (car l2)) (loop (cdr l1) (cdr l2))))))
        (else
         (let loop ((first first) (rest rest))
           (or (null? first)
               (and (apply f (car first) (map car rest))
                    (loop (cdr first) (map cdr rest))))))))

(define (ormap f first . rest)
  (cond
   ((null? first) (or))
   ((null? rest) (let loop ((first first))
                   (and (pair? first)
                        (or (f (car first))
                            (loop (cdr first))))))
   (else (let loop ((lists (cons first rest)))
           (and (pair? (car lists))
                (or (apply f (map car lists))
                    (loop (map cdr lists))))))))

(define (string-split/include-empty str delim) (string-split str delim #t))

;(define-library-implementation util
; ;(import scheme chicken-unistd chicken-regex srfi-18)
; (export
;  remove-file dirname file-empty? filedata
;  timestamp read-bytes
;  reg-expr->proc
;  http-prefix? http-is-login? http-is-logout? http-is-post-url?
;  multipart-data-boundary content-disposition->name-regex
;  message-body-offset-regex mime-boundary lws-suffix
;  html-only-client broken-303-client user-agent-ie
;  nunu-regexp nunu-include-regexp bold-regexp url-regexp
;  sql-special-regexp colon-split-regex

;  bind-exit
;  logit logerr default-log-access log-access debug
;  filter to-string*
;  map-matches-alternate
;  oid? oid->hash make-oid-table

;  expect-object read-all
;  register-location-format!
;  read-locator write-locator

;  nunu-owner

;  iso-timestring rfc-822-timestring

;  strip-html-suffix content-disposition->name message-body-offset
;  addlws-suffix-len rfc2046-split colon-split
;  string-quote

;  register-trustedcode! get-trusted-code
;  pilot-makedoc with-output-through-lout with-output-through-htmldoc

;  ))


(define key-sequence
  (let ((ff (lambda (k v i) (cons k i))))
    (define (key-sequence table) (hash-table-fold table ff '()))
    key-sequence))

;(: process-kill (#(procedure #:clean #:enforce) (fixnum #!optional fixnum) undefined))
(: process-kill (fixnum -> undefined))
(define (process-kill p)
  (guard (ex (else #f)) (process-signal p signal/term)))

(: find-in-path (string --> string))
(define find-in-path
  (let ((*path* #f)
	(pathsep "/")			; FIXME integrate with OS path separator
	[getenv get-environment-variable]
	[string-split string-split]
	[string-append string-append]
	[file-execute-access? file-execute-access?])
    (lambda (name)
      (if (not *path*)
	  (set! *path* (string-split (getenv "PATH") ":")))
      (let loop ((p *path*))
	(if (null? p)
	    (error "~a: could not find along PATH" name)
	    (let ((t (string-append (car p) pathsep name)))
	      (if (file-execute-access? t)
		  t
		  (loop (cdr p)))))))))

;;* bind-exit

(define bind-exit call-with-current-continuation)

;;* Lolevel

;; BEWARE: literally copied from chicken/loleven.scm

(define-inline (%generic-pointer? x)
  (or (##core#inline "C_i_safe_pointerp" x)
      (##core#inline "C_locativep" x) ) )

(define memory-set!
  (let ((memset1 (foreign-lambda* int ((c-pointer p) (char fill) (int start) (int len))
				  "return(memset(p+start, fill, len));"))
	(memset2 (foreign-lambda* int ((scheme-pointer p) (char fill) (int start) (int len))
				  "return(memset(p+start, fill, len));"))
	(typerr (lambda (x)
		  (##sys#error-hook
		   (foreign-value "C_BAD_ARGUMENT_TYPE_ERROR" int)
		   'memory-set! x)))
	(slot1structs '(mmap
			u8vector u16vector u32vector s8vector s16vector s32vector
			f32vector f64vector)))
    (define memset
      (lambda (to fill start len)
	(define (sizerr . args)
	  (apply ##sys#error 'memory-set! "number of bytes to set too large" args))
	(define (checkn1 n nmax off)
	  (if (fx<= n (fx- nmax off))
	      n
	      (sizerr off n nmax) ) )
	(cond
	 ((%generic-pointer? to)
	  (memset1 to fill start len))
	 ((or (##sys#bytevector? to) (string? to))
	  (memset2
	   to fill
	   (if (fx>= start 0) start (##sys#error 'memory-set! "negative start" start))
	   (checkn1 len (##sys#size to) start)))
	 ((##sys#generic-structure? to)
	  (if (memq (##sys#slot to 0) slot1structs)
	      (memset (##sys#slot to 1) fill start len)
	      (typerr to) ))
	 (else (typerr to)) )))
    memset))

;;* File System Access

(define file-directory? directory?)
(define remove-file delete-file)
(define (remove-dir dir)
  (and (file-directory? dir) (delete-directory dir)))

(define (remove-dir-recursive dir)
  (define (string->dir s) (string-split s "/"))
  (let ((path (string->dir dir)))
    (do ((f (directory dir) (cdr f)))
        ((null? f) (remove-dir dir))
      (if (not (or (string=? (car f) ".") (string=? (car f) "..")))
          (let ((fullname (make-absolute-pathname path (car f))))
            (if (file-directory? fullname)
                (remove-dir-recursive fullname)
                (remove-file fullname)))))))

(cond-expand
 (windows
  ;; Link is not supported on Windows to theses days.
  (define (link old new) -1))
 (else
  (define (link old new)
    ((foreign-lambda* int ((c-string old) (c-string new))
                    "return(link(old, new));") old new))))

(define (set-file-modification-time! file seconds)
  (set! (file-modification-time file) seconds))

;; Strange HACKS

(import lolevel)
(mutate-procedure!
 change-file-mode
 (lambda (original)
   (lambda (fn m)
     (guard
      (ex (else
	   (logerr "Mode change failed on ~a, ignoring mode changes now!\n" fn)
	   (mutate-procedure! change-file-mode (lambda (changed) (lambda (fn m) #t)))))
      (original fn m)
      (mutate-procedure! change-file-mode (lambda (changed) original))))))

(mutate-procedure!
 set-file-modification-time!
 (let ((overwrite!
        (lambda (fn t)
          (let ((data (filedata fn)))
            (call-with-output-file fn (lambda (p) (write-string data #f p)))))))
   (lambda (original)
     (lambda (fn t)
       (handle-exceptions
        ex
        (begin
          (logerr "Setting modification time failed on ~a. Using overwrite instead!\n" fn)
          (mutate-procedure! set-file-modification-time! (lambda (changed) overwrite!))
          (overwrite! fn t))
        (original fn t)
        (mutate-procedure! set-file-modification-time! (lambda (changed) original)))))))

;; end of HACKS

(define cwd current-directory)

(define scandir directory)

(define (dirname fn)
  (receive (dir name ext) (decompose-pathname fn) dir))
(define (mkdirs path)
  (if (not (file-exists? path))
      (begin
        (and-let* ((d (pathname-directory path))) (mkdirs d))
        (create-directory path))))

(define (within-directory dir thunk)
  (let ((here #f))
    (dynamic-wind
	(lambda ()
	  (set! here (current-directory))
	  (change-directory dir))
	thunk
	(lambda () (change-directory here)))))

;; http://practical-scheme.net/gauche/man/gauche-refe/Filesystem-utilities.html#index-call_002dwith_002dtemporary_002ddirectory
;; https://lists.gnu.org/archive/html/bug-guix/2018-07/msg00078.html
(define (call-with-temporary-directory proc)
  (let ((dir (make-temporary-directory)))
    (if (file-exists? dir)
        (call-with-temporary-directory proc)
        (let ((here #f))
	  (guard
	   (ex (else (change-directory here)
		     (remove-dir-recursive dir)
		     (raise ex)))
	   (set! here (current-directory))
	   (mkdirs dir)
	   (change-directory dir)
	   (receive
	    results (proc dir)
	    (change-directory here)
	    (guard (ex (else #f)) (remove-dir-recursive dir))
	    (apply values results)))))))

(define (file-empty? name)
  (eqv? (file-size name) 0))

(: make-string/uninit (fixnum --> string))
(define (make-string/uninit size)
  (##sys#allocate-vector size #t #f #f))

(: copy-file-data! (string (or string blob) fixnum fixnum fixnum -> (or false fixnum)))

(cond-expand
 (chicken

  (define (copy-file-data! file to n foff toff)
    (if (fx<= n 0) #t
	(let* ((fileno (file-open file (+ open/rdonly open/nonblock)))
	       (size (file-size fileno))
	       (take (min n (fx- size foff))))
	  (if (fx<= take 0)
	      (begin
		(file-close fileno)
		#f)
	      (let ((mmap (map-file-to-memory #f size prot/read (+ map/file map/shared) fileno)))
		(move-memory! (memory-mapped-file-pointer mmap) to take foff toff)
		(unmap-file-from-memory mmap)
		(file-close fileno)
		take))))))

 (else
  
  (define (copy-file-data! file to n foff toff)
    (let* ((fileno (file-open file (+ open/rdonly open/nonblock)))
	   (size (file-size fileno))
	   (take (min n (fx- size foff))))
      (if (fx< take 0)
	  (raise 'copy-file-data!:read-negativ)
	  (let* ((skv (set-file-position! fileno foff seek/set))
		 (took ((if (or (##sys#bytevector? to) (string? to))
			    (foreign-lambda*
			     integer ((integer fd) (scheme-pointer p) (integer off) (integer n))
			     "return(read(fd,((C_char*)p)+off,n));")
			    (foreign-lambda*
			     integer ((integer fd) (c-pointer p) (integer off) (integer n))
			     "return(read(fd,((C_char*)p)+off,n));"))
			fileno to toff take)))
	    (file-close fileno)
	    (if (fx< took 0)
		(raise 'copy-file-data!:read-failed)
		took)))))
  ))

(define (filedata name)
  (let* ((size (file-size name))
         (result (make-string/uninit size)))
    (copy-file-data! name result size 0 0)
    result))

(define file->string filedata)

;; We need to watch for tight disk space at the file system where 'nm'
;; lives.

(define (filesystem-free nm)
;  (define df-block-regex (reg-expr->proc '(seq (prefix (* (not space)))
;                                               (+ space)
;                                               (let blocks (+ digit))
;                                               (+ space)
;                                               (let used (+ digit))
;                                               (+ space)
;                                               (let avail (+ digit))
;                                               (+ any))))
;  (bind ((df (open-input-process
;              (string-append "/bin/df -kP " nm)))
;         (s e b u a (begin (read-line df)
;                           (df-block-regex (read-line df)))))
;        (close-input-port df)
;        (string->number a))
  1e8)

(define (thread-list . args)
#;(print-call-chain (current-error-port))
  (let ((t (current-thread)))
    (print (thread-name t) " current " (thread-state t)))
  (##sys#all-threads
   (lambda (queue arg t i)
     (if (not (eq? (thread-state t) 'dead))
	 (print (thread-name t)
		;; "@" (##test#thread-number t)
		": "
		(thread-state t)
		" " queue " "
		(cond
		 ((eq? queue 'timeout) (- arg (current-milliseconds)))
		 ((and (eq? queue 'sleeping) (mutex? arg))
		  (mutex-name arg))
		 ((and (eq? queue 'sleeping) (condition-variable? arg))
		  (condition-variable-name arg))
		 (else arg))
		" "
		(cond
		 ;; ((thread? arg) (##test#thread-number arg))
		 ((mutex? arg) (mutex-owner arg))
		 ((eq? queue 'timeout) (current-milliseconds))
		 (else (eq?-hash arg)))))
     #f)
   #f))

(define (thread-sleep!/ms ms)
  (thread-sleep! (/ ms 1000.0)))

(define thread-deliver-signal! thread-signal!)

(define-inline (pipe) (create-pipe))


(define (combine . fns) (apply compose (reverse fns)))

(define (list->values args)
  (if (and (pair? args) (pair? (cdr args)))
      (apply values args)
      (car args)))

(define arithmetic-shift-left arithmetic-shift)
(: arithmetic-shift-right (number number --> number))
(define (arithmetic-shift-right i n) (arithmetic-shift i (fx* -1 n)))

;; timezone-offset should actually store the ready made scheme number,
;; not the C number in 'result'.
(define get-timezone-offset
  (foreign-lambda* integer () #<<EOF
 extern long timezone;
 extern int daylight;
 long tz;
 time_t ng, n0 = time(NULL);
 struct tm *now =localtime(&n0);
 // return( (now != NULL) ? /* now->__tm_gmtoff */ -timezone : 0 );
 struct tm gm;
 gmtime_r(&n0, &gm);
 ng = mktime(&gm);
 tz = (long)(n0 - ng);
#ifndef WIN32
 // adjust for daylight saving
 tz += now->tm_isdst? 3600 : 0;
#endif
 C_return( (int) tz );
EOF
))

(define timezone-tzset-intern (foreign-lambda* void () "tzset();"))

(define *timezone-offset* 0)

(define (timezone-offset) *timezone-offset*)

(define (timezone-tzset)
  (timezone-tzset-intern)
  (set! *timezone-offset* (get-timezone-offset)))

(timezone-tzset)

;;;*** Object IDentifiers

;(define-class <oid> (<object>)
;  (string-data))

;; (define-generic string-data)

;(define-method (string-data (obj <object>))
;  (slot-ref obj 'string-data))

;;** Utilities for protocol implementations.

;; read n bytes from port, return as string

(: read-bytes ((or boolean fixnum) input-port -> (or string eof)))
(define (read-bytes n port)
  (let ((r (read-string n port)))
    (if (fx= (string-length r) 0) #!eof r)))

(: string-splice! (string fixnum string fixnum procedure -> fixnum))
(define string-splice! (##core#primitive "A_addstr5"))

#;(define (string-splice! buf i str j continue)
  (let* ((n (min (fx- (string-length str) j)
                 (fx- (string-length buf) i)))
         (i2 (fx+ i n)))
    (##sys#copy-bytes str buf j i n)
;      ((foreign-lambda*
;        void
;        ((c-string str) (c-string buf) (integer j) (integer i) (integer n))
;        "++n; str+j-1; buf+i-1; while( --n ) *++buf=*++str;") str buf j i n)
    (if (eq? i2 (string-length buf))
        (continue buf i2 str (fx+ j n) string-splice!)
        i2)))

;    (define (addstr buf i str j flush)
;      (let ((str-len (string-length str)))
;        (do ((j j (fx+ j 1))
;             (i i (fx+ i 1)))
;            ((or (eq? i $buffer-size) (eq? j str-len))
;             (if (eq? i $buffer-size) (flush buf i str j addstr) i))
;          ;; chicken for (string-set! buf i (string-ref str j))
;          (##core#inline "C_setsubchar" buf i (##core#inline "C_subchar" str j)))))

(define exn? (condition-predicate 'exn))
(define exn-message (condition-property-accessor 'exn 'message))
(define exn-arguments (condition-property-accessor 'exn 'arguments))
(define exn-location (condition-property-accessor 'exn 'location))

(define (condition->string ex)
  (cond
   ((and (chicken-condition? ex) (exn? ex))
    (format "~a ~a ~a" (exn-message ex) (exn-arguments ex) (exn-location ex)))
   ((srfi-35:condition? ex)
    (let ((types (condition-types ex)))
      (call-with-output-string
       (lambda (port)
	 (for-each (lambda (entry)
		     (format port "~a: ~a\\n" (condition-type-name (car entry)) (cdr entry)))
		   (condition-type-field-alist ex))))))
   (else (format "~a" ex))))

(define (condition->fields ex)
  (cond
    ((and (chicken-condition? ex) (exn? ex))
     ;; (values "exception" (exn-message ex) (list (condition->string ex)) '())
     (values (exn-message ex) (list (condition->string ex)) (exn-location ex) '()))
    ((uncaught-exception? ex)
     (condition->fields (uncaught-exception-reason ex)))
    ((timeout-object? ex)
     (values "time out" "timeout" '() '()))
    ((join-timeout-exception? ex)
     (values "time out" "join-timeout-exception" '() '()))
    ((abandoned-mutex-exception? ex)
     (let ((info (list (##sys#slot ex 2))))
       (values "abandoned mutex exception" (##sys#slot ex 2) info info)))
    ((srfi-35:condition? ex)
     (let ((types (condition-types ex)))
       (values "condition"
	       (map condition-type-name types)
	       (map (lambda (entry) (format "~a" entry))
		    (condition-type-field-alist ex))
	       '())))
    (else (values "unknown exception" (format #f "~s" ex) '() '()))))

(define (condition->fields+ ex)
  (let* ((ex (let loop ((ex ex))
	      (or (and (uncaught-exception? ex)
		       (loop (uncaught-exception-reason ex)))
		  ex))))
  (cond
   ((message-condition? ex)
    (values 
     (condition-message ex)
     (condition->string ex)
     '()
     '()))
;   ((eof-condition? ex)
;    (values "eof-condition" (eof-condition-reason ex) (or s '()) '()))
    (else (condition->fields ex)))))

;; TODO Conditions might be better off at a central point.

(define $client-lookups? #f)


(define-inline (regex-matched? p) (car p))

;; obsolete (already?)
(: reg-expr->proc (string --> (procedure (string #!rest fixnum) . *)))
(define (reg-expr->proc regex)
  (let ((match (regexp regex)))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (apply values
                   (caar result) (cadar result)
                   (map (lambda (p) (and (regex-matched? p) (substring str (car p) (cadr p))))
                        (cdr result)))
            #f)))))

;(: strip-html-suffix-regex (string #!rest --> (or boolean fixnum) (or boolean fixnum) (or boolean string)))
(define strip-html-suffix-regex
  (let ((match (regexp "^(.*)\\.[xX]?[hH][tT][mM][lL]?$")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (apply values
                   (caar result) (cadar result)
                   (map (lambda (p) (and (regex-matched? p) (substring str (car p) (cadr p))))
                        (cdr result)))
            (values #f #f #f))))))

(: http-prefix? (string --> boolean))
(define http-prefix?
  (let ((match (regexp "^http://|https://")))
    (lambda (str) (and (string-search-positions match str) #t))))

(: content-type-charset (string --> (or false string)))
(define content-type-charset
  (let ((match (regexp "[^;]+;[[:space:]]*charset[[:space:]]*=([^ ]+)|'([^']*)'|\"([^\"]*)\".*")))
    (lambda (str)
      (and-let* ((r (string-match match str)))
		(or (cadr r) (caddr r) (list-ref r 3))))))

(: xml-pi-encoding (string --> (or false string)))
(define xml-pi-encoding
  (let ((match (regexp "^[[:space:]]*version[[:space:]]*=[[:space:]]*'(?:[^']*)'|\"(?:[^\"]*)\"[[:space:]]*encoding[[:space:]]*=[[:space:]]*'([^']*)'|\"([^\"]*)\"")))
    (lambda (str)
      (and-let* ((r (string-match match str)))
		(or (cadr r) (caddr r))))))

(define (xml-pi-regex str)
  (string=? (substring str 0 (min (string-length str) 5)) "<?xml"))

(: html-regex (string --> boolean))
(define html-regex
  (let ((match (regexp "^[[:space:]\\n\\r]*(:<!--[^-]-->)?<(:html)|(:HTML)")))
    (lambda (str) (and (string-search-positions match str) #t))))

(: is-proxy-request? (string --> boolean))
(define is-proxy-request?
  (let ((match (regexp "(?:http://|https://|ftp://|ldap://).*")))
    (lambda (str) (and (string-match-positions match str) #t))))

(: http-is-login? (string --> boolean))
(define http-is-login?
  (let ((match (regexp "/LOGIN.*")))
    (lambda (str) (and (string-match-positions match str) #t))))

(: http-is-logout? (string --> boolean))
(define http-is-logout?
  (let ((match (regexp "/LOGOUT.*")))
    (lambda (str) (and (string-match-positions match str) #t))))

(define http-is-post-url?
  (let ((match (regexp "/POST(?:=[^/]+)?(.*)")))
    (lambda (str)
      (let ((result (string-match-positions match str)))
	(and result (substring str (caadr result) (cadadr result)))))))

(define http-is-check-url?
  (let ((match (regexp "/CHECK/.*")))
    (lambda (str) (and (string-match-positions match str) #t))))

;; for http server command parsing only
(define http-cmd-regex
  (reg-expr->proc
   "^([[:upper:]]+)[[:blank:]]+([^[:blank:]]+)[[:blank:]](.*)$"))

(define http-status-regex
  (let ((match (regexp (string-append "HTTP/1\\.[01][[:blank:]]+"
                                      "([[:digit:]][[:digit:]][[:digit:]])"
                                      "[[:blank:]]+(.*)"))))
    (lambda (str)
      (let ((result (string-match-positions match str)))
        (if result
            (apply values
                   (caar result) (cadar result)
                   (map (lambda (p) (and (regex-matched? p) (substring str (car p) (cadr p))))
                        (cdr result)))
            (values #f #f #f #f))))))

(define http-location-regex
  (let ((match (regexp (string-append "^(https?://)([^/]*)(.*)$"))))
    (lambda (str)
      (let ((result (string-match-positions match str)))
        (if result
            (apply values
                   (caar result) (cadar result)
                   (map (lambda (p) (and (regex-matched? p) (substring str (car p) (cadr p))))
                        (cdr result)))
            (values #f #f #f #f #f))))))

(define lmtp-from-regex
  (let ((match (regexp "^From (.*@.*) (.+)$")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (apply values
                   (caar result) (cadar result)
                   (map (lambda (p) (and (regex-matched? p) (substring str (car p) (cadr p))))
                        (cdr result)))
            (values #f #f #f #f))))))

(define lmtp-destination-email-regex
  (let ((match (regexp "^ *([^[:blank:]]+)[[:blank:]]+<([^>]+)>")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (apply values
                   (caar result) (cadar result)
                   (map (lambda (p) (and (regex-matched? p) (substring str (car p) (cadr p))))
                        (cdr result)))
            (values #f #f #f #f))))))

(define broken-303-client
  (let ((match (regexp "Mozilla/4\\..*")))
    (lambda (str) (and (string-match-positions match str) #t))))

(define user-agent-amaya
  (let ((match (regexp "amaya/.*")))
    (lambda (str) (and (string-match-positions match str) #t))))

(define user-agent-ie
  (let ((match (regexp "Microsoft.*")))
    (lambda (str) (and (string-match-positions match str) #t))))

(define user-agent-mozilla
  (let ((match (regexp "Mozilla/.*")))
    (lambda (str) (and (string-match-positions match str) #t))))

;; The additional, optional slash enables forward compatible
;; processing of XPath selections within the target node.
;; TODO add XPath support here.

(define nunu-regexp
  (let ((match (regexp "^([[:upper:]][[:alnum:]äÄöÖüÜß]*[[:upper:]][[:alnum:]äÄöÖüÜß]*)/?")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caar result) (cadar result)))
            (values #f #f #f))))))

(define nunu-include-regexp
  (let ((match (regexp "^@([[:upper:]][[:alnum:]äÄöÖüÜß]*[[:upper:]][[:alnum:]äÄöÖüÜß]*)")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caadr result) (cadadr result)))
            (values #f #f #f))))))

(define bold-regexp
  (let ((match (regexp "^\\*([[:alnum:]äÄöÖüÜß]*)\\*")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caadr result) (cadadr result)))
            (values #f #f #f))))))

(define url-pregexp-regexp
  (let ((match (regexp
                (string-append
                 "^(((https?|ftp)://[-[:alnum:]\\.]+(:[[:digit:]]+)?[^[:blank:]]*)"
                 "|(mailto:[-[:alnum:]\\._]+@[-[:alnum:]\\.]+))"
                 ))))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caar result) (cadar result)))
            (values #f #f #f))))))

(define nunu-oid-regexp
  (let ((match (regexp "^(A[[:xdigit:]]{32}(:/[^[:space:]<]+)?)")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caar result) (cadar result)))
            (values #f #f #f))))))

(define url-regexp url-pregexp-regexp)

(define sql-special-regexp
  (reg-expr->proc "'"))

(define colon-split-regex
  (regexp "([^:]+):[[:blank:]]*(.*)$"))

;; We could be less restrictive here.  But why should we?
(define illegal-lout?
  (let ((match (regexp
                (string-append "@(Include|SysInclude|IncludeGraphic|PrependGraphic"
                               "|SysPrependGraphic|Database|SysDatabase|Filter)"))))
    (lambda (str) (and (string-match-positions match str) #t))))

(define lout-literal-char-sequence-regexp
  (let ((match (regexp "^([^\"&{}|#@^~\n\r\t ]+)")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caadr result) (cadadr result)))
            (values #f #f #f))))))

(define lout-collapsable-whitespace-sequence-regexp
  (let ((match (regexp "^[[:blank:]]+")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caar result) (cadar result)))
            (values #f #f #f))))))

;;** Tables

(define (make-symbol-table) (make-hash-table eq? symbol-hash))

(define (make-fixnum-table) (make-hash-table eqv? equal?-hash))

(define (make-character-table) (make-hash-table eqv? equal?-hash))

(define (make-string-table) (make-hash-table string=? string-hash))

;; Mux stuff

(define (mutex-owner mux)
  (let ((s (mutex-state mux)))
    (and (thread? s) s)))

(define ((mutex-lock!! mutex-lock!) x)
  (if (eq? (mutex-state x) (current-thread))
      (begin
	(logerr "relock attempt ~a" (thread-name (current-thread)) (mutex-name x))
	(print-call-chain (current-error-port))
	(error 'relock-attempt (mutex-name x)))
      (mutex-lock! x)))

(define (call-with-list-extending proc)
  (let ((first #f)
	(last (cons 0 '())))
    (set! first last)
    (let ((result (proc (lambda (item)
			  (let ((cell (cons item '())))
			    (set-cdr! last cell)
			    (set! last cell)
			    item)))))
      (values (cdr first) result))))

(define (call-with-list-extending1 proc)
  (let ((first #f)
	(last (cons 0 '())))
    (set! first last)
    (let ((result (proc (lambda (item)
			  (let ((cell (cons item '())))
			    (set-cdr! last cell)
			    (set! last cell)
			    item)))))
      (cdr first))))

;; include the portable code

(: oid? (* --> boolean : :oid:))

(include "../mechanism/util.scm")

(define (logcond-full tag title msg args)
  (print-call-chain (current-error-port))
  (logerr " ~a ~a ~a ~a\n" tag title msg args))

;(include "../mechanism/srfi/strings.scm")
;;** String Manipulation

;;
;; There are things, which could be better supported by the scheme
;; standard :-(.  I think the Perl or even TCL way of implicit
;; converting everything hides too much of the relevant complexity
;; from the programer.  But Scheme is way too poor the other way
;; around.

;; The string library is still in draft state.  The following will
;; probably not become SRFI.  It's here because the code already uses
;; them.

(define (string-replw str width)
  (if (string=? str "")
      ""
      (let ((str-len (string-length str)))
	(let loop ((result "") (size 0))
	  (cond ((= size width) result)
		((> size width) (substring result 0 width))
		(else (loop (string-append result str) (+ size str-len))))))))

(define (string-left s1 width . s2)
  (let ((padding (if (pair? s2) (car s2) " "))
	(str-len (string-length s1)))
    (cond ((> width str-len)
	   (string-append s1 (string-replw padding (- width str-len))))
	  ((< width str-len) (substring s1 0 width))
	  (else s1))))

(define (string-right s1 width . s2)
  (let ((padding (if (pair? s2) (car s2) " "))
	(str-len (string-length s1)))
    (cond ((> width str-len)
	   (string-append (string-replw padding (- width str-len)) s1))
	  ((< width str-len) (substring s1 (- str-len  width) str-len))
	  (else s1))))

(define (has-suffix? suffix str)
  (let ((ql (string-length str)) (sl (string-length suffix)))
    (and (>= ql sl) (string=? suffix (substring str (- ql sl) ql)))))

(define (to-string obj) (format "~a" obj))

;; Some Scheme implementations functions have a limited argument
;; count.  We might need to apply string-append to long lists.

(define (apply-string-append lst)
  (define (return-len buffer i str j continue) i)
  (cond
   ((null? lst) "")
   ((null? (cdr lst)) (car lst))
   (else
    (let* ((length (fold (lambda (s i) (fx+ i (string-length s))) 0 lst))
           (result (make-string/uninit length)))
      (let loop ((i 0) (from lst))
        (if (eqv? i length)
            result
            (loop (string-splice! result i (car from) 0 return-len)
                  (cdr from))))))))

;; search the longest prefix in s1 and s2 terminated by sep 
(define (string-prefix-length+ s1 s2 sep)
  (string-index-right s1 sep 0 (string-prefix-length s1 s2)))

) ;; module util

(import (prefix util util:))

(define local-id util:local-id)
(define make-oid-table util:make-oid-table)

(define *set-child-uid* util:*set-child-uid*) 
(define run-child-process util:run-child-process)

(define call-with-temporary-directory util:call-with-temporary-directory)
(define dirname util:dirname)
(define mkdirs util:mkdirs)
(define file->string util:file->string)
(define find-in-path util:find-in-path)
(define srfi:string-join string-join)
(define andmap util:andmap)
(define ormap util:ormap)
(define content-type-charset util:content-type-charset)
(define set-log-output! util:set-log-output!)
(define debug util:debug)
(define logerr util:logerr)
(define logcond util:logcond)
(define log-condition util:log-condition)
(define logit util:logit)
(define condition->fields util:condition->fields)
(define sql-quote util:sql-quote)
;; time as defined by dsssl 10.???

(define dsssl:time current-seconds)
;; setup
(define bind-exit util:bind-exit)
(define filedata util:filedata)
(define timezone-offset util:timezone-offset)
;; debug/control
(define thread-list util:thread-list)
(define xml-pi-regex util:xml-pi-regex)
(define memory-set! util:memory-set!)
(define condition->string util:condition->string)
