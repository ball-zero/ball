;; (C) 2002, 2003 Jörg F. Wittenberger

;; chicken module clause for the Askemos 'step' (byzantine protocol
;; implementation) module .

(declare
 (unit dispatch)
 ;;
 (usual-integrations))

(module
 dispatch
 (
  ;; export missplaced function, which used to live in main.scm
  askemos-web-user make-askemos-public-user web-handle-authenticated-service
  askemos-sync

  ;; only these belong here
  signal-subject!

  $defer-sync-replies-to-stable-state

  $skip-referrer-check
  approve-eval
  $virtual-host
  $html-error-handler
  )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	(only atomic yield-system-load)
	srfi-1 srfi-13 (except srfi-18 raise) srfi-19 srfi-34 srfi-35 srfi-45 srfi-69
	ports data-structures extras
	openssl mailbox timeout atomic parallel shrdprmtr util sqlite3
	protection tree notation bhob aggregate channel storage-api place-common methods)

(import pool place corexml msgcoll mesh-cert protocol-connpool protocol storage)
(import (only protocol-common
	      register-agreement-handler!
	      call-agreement-handler))
(import (only place-ro metainfo-request?))
(import (only pool find-frame-by-id/synchronised+quorum))
(import (only storage-sql sql-set-missing-oid! spool-db-select/prepared))

(include "typedefs.scm")

(: signal-subject! (:oid: symbol :message: -> :message:))

(: register-agreement-handler!
   (symbol (:oid: :message: -> :message:) -> undefined))

(include "../mechanism/dispatch.scm")

)

(import (prefix dispatch m:))

(define signal-subject! m:signal-subject!)

(define askemos-web-user m:askemos-web-user)
(define make-askemos-public-user m:make-askemos-public-user)
(define web-handle-authenticated-service m:web-handle-authenticated-service)

(define askemos-sync m:askemos-sync)

(define $defer-sync-replies-to-stable-state m:$defer-sync-replies-to-stable-state)

(define $skip-referrer-check m:$skip-referrer-check)
(define approve-eval m:approve-eval)
(define $virtual-host m:$virtual-host)
(define $html-error-handler m:$html-error-handler)
