(declare
 (unit ask-repl-server)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)
 (disable-interrupts) ;; grep disable-interrupts for "Why?"
 )

(module
 ask-repl-server
 ( ;;run run& kill-server
  make-encoding ask-encode ask-decode
  ;;
  wirepacket? wirepacket-auth wirepacket-tag wirepacket-type wirepacket-data wirepacket-values
  make-connection-handler get-connection-handler
  ask-socket? ask-socket-name ask-socket-state ask-socket-time
  ask-socket-connected? ask-socket-active?
  ask-socket-encoding-rpk
  ask-socket-peer-address
  ask-socket-refresh-auth!
  
  ask-send! ask-query! ask-reply! ask-signal!

  ask-socket-close!

  make-evil-connection make-gui-repl
  run
  current-sender current-tag
  ;; client
  make-connection
  ;; keydb - also to be moved into it's own module
  keydb-open!
  keydb-select keydb-exec! init-keydb! keydb-get-keys
  keydb-add-pk! authenticate-pk/index! ;; half baked
  keydb-add-keypair!
  make-twencoding
  ;; debug only
  renonce-keydb! keydb-get-auth
  
  )
 (import scheme (except chicken with-exception-handler))
 (import sslsocket irregex (except srfi-18 raise) srfi-110 extras ports)
 ;; Import specific to BALL's http-eval
 (import srfi-4 srfi-34 timeout tree notation-xmlrender notation-xmlparse aggregate place-common protocol-clntclls)
 (import util threadpool)
 (import matchable)

 (define ##dbg#tw #f)

 (define-syntax ?dbg (syntax-rules () ((_ body ...) (if ##dbg#tw (begin body ...)))))

 (define-record-type <encoding>
   (make-encoding validator encode decode init refresh special)
   encoding?
   (validator encoding-validator)
   (encode encoding-encode); encodingencode-set!)
   (decode encoding-decode); encodingdecode-set!)
   (init encoding-init)
   (refresh encoding-refresh)
   (special encoding-special); encodingspecial-set!)
   )

 (define (ask-encode encoding data)
   ((encoding-encode encoding) (encoding-special encoding) data))
 (define (ask-decode encoding data nonce)
   ((encoding-decode encoding) (encoding-special encoding) data nonce))
 (define (ask-encoding-init! connection)
   (let ((encoding (ask-socket-encoding connection)))
     ((encoding-init encoding) (encoding-special encoding)
      (ask-socket-from connection) (ask-socket-to connection))))

 (define plain-encoding
   (make-encoding
    (lambda (special) #t)
    (lambda (special data) (values data ""))
    (lambda (special data nonce) (values #f data))
    (lambda (special in out) #f)
    (lambda (special in out) #f) ;; refresh
    #f))

 (define-inline (socket-current-milliseconds) (current-milliseconds))

 (define-record ask-socket name encoding state time receiver sender runexcl to from setup setup-args)
 (define-record-printer (ask-socket x to)
   (format
    to "#<x-socket ~a ~a ~a>" (ask-socket-name x)
    (let ((v (ask-socket-state x)))
      (cond
       ((not v) 'new)
       (else v)))
    (ask-socket-time x)))

 (let ((doit make-ask-socket))
   (set! make-ask-socket
	 (lambda (name encoding state time receiver sender runexcl to from setup setup-args)
	   (?dbg (logerr "name ~a encoding ~a state ~a time ~a receiver ~a sender ~a runexcl ~a to ~a from ~a setup ~a setup-args ~a\n" name encoding state time receiver sender runexcl to from setup setup-args))
	   (doit name (or encoding plain-encoding)
		 state time receiver sender runexcl to from setup setup-args))))

 (define (ask-socket-connected? conn)
   (and (eq? (ask-socket-state conn) 'connected)
	(let ((encoding (ask-socket-encoding conn)))
	  ((encoding-validator encoding) (encoding-special encoding)))))
 (define (ask-socket-active? conn) (memq (ask-socket-state conn) '(connected setup)))

 (define (ask-socket-state-now! conn kind)
   (ask-socket-state-set! conn kind)
   (ask-socket-time-set! conn (socket-current-milliseconds)))

 (define (observe-socket-active! conn) (ask-socket-state-now! conn 'connected))

 (define (ask-socket-close! connection)
   (?dbg (logerr "Closing connection ~a\n" connection))
   (if (not (eq? (ask-socket-state connection) 'disconnected))
       ((ask-socket-runexcl connection)
	(lambda ()
	  (if (not (eq? (ask-socket-state connection) 'disconnected))
	      (begin
		(if (ask-socket-to connection) (close-output-port (ask-socket-to connection)))
		(ask-socket-to-set! connection #f)
		;; FIXME: need to sync (how?) for pending input.
		(if (ask-socket-from connection) (close-input-port (ask-socket-from connection)))
		(ask-socket-from-set! connection #f)
		(ask-socket-state-now! connection 'disconnected)
		((get-connection-handler (ask-socket-receiver connection) 'EOF) connection #!eof)
		))))))

 ;; Initiating a request at client side, introducing a TAG and sending the payload.
 (define (ask-send! to tag s . vals)
   ((ask-socket-sender to) to tag 'POST s vals))

 (define (ask-query! to tag s . vals)
   ((ask-socket-sender to) to tag 'GET s vals))

 (define (ask-reply! to tag s . vals)
   ((ask-socket-sender to) to tag 'DATA s vals))

 (define (ask-signal! to tag s . vals)
   ((ask-socket-sender to) to tag 'EXCEPTION s vals))
 
 (define ask-line1-re (irregex "([[:alnum:]]+) ((?:DATA)|(?:EXCEPTION)|(?:GET)|(?:POST)|(?:NOOP)) ([[:digit:]]+) VALUES ([[:digit:]]+)"))
 (define ask-value-regex (irregex "VALUE ([[:digit:]]+) ([[:digit:]]+)"))
 (define ask-opaque-re (irregex "([[:digit:]]+) ([[:digit:]]+)"))

 ;; (define (debug l v) (format (current-error-port) "DR ~a: ~a\n" l v) v)

 (define-record wirepacket auth tag type data values)

 (define-record-printer (wirepacket x out)
   (format out "<wirepacket ~a ~a>" (wirepacket-tag x) (wirepacket-type x)))

 (define (*read-timeout*) 60)
 (define (*read-available-timeout*) 0.2)
 (define (*write-timeout*) 10)
 
 ;; PEER-READ
 ;;
 ;;Wait forever blocking on port!!!
 (define (peer-read* auth from)
   (let ((lln (read-line from)))
     (cond
      ((eof-object? lln) lln)
      ((irregex-match ask-line1-re lln) =>
       (lambda (m)
	 (let* ((l (string->number (irregex-match-substring m 3)))
		(tag (irregex-match-substring m 1))
		(vals (let ((n (string->number (irregex-match-substring m 4))))
			(let loop ((r '()) (i 0))
			  (if (= n i) (reverse r)
			      (loop
			       (cons
				(let* ((l (read-line from))
				       (m (irregex-match ask-value-regex l))
				       (len (string->number (irregex-match-substring m 2)))
				       (r (read-string len from)))
				  (read-line from)
				  r)
				r)
			       (add1 i))))))
		(plain (u8vector->blob/shared
			(let ((v (make-u8vector l 0 (> l 4069))))
			  (read-u8vector! l v from)
			  v))))
	   (read-line from)
	   (let ((packet-type (irregex-match-substring m 2)))
	     (cond
	      ((string=? packet-type "POST") (make-wirepacket auth tag 'POST plain vals))
	      ((string=? packet-type "GET") (make-wirepacket auth tag 'GET plain vals))
	      ((string=? packet-type "EXCEPTION") (make-wirepacket auth tag 'EXCEPTION plain vals))
	      ((string=? packet-type "DATA") (make-wirepacket auth tag 'DATA plain vals))
	      ((string=? packet-type "NOOP") (make-wirepacket auth tag 'NOOP plain vals))
	      (else (error 'peer-read "Unkown packet type" packet-type)))))))
      (else (error 'peer-read "Unintelligible garbage" lln)))))

 (define (peer-read/opaque* from)
   (let ((lln (read-line from)))
     (cond
      ((eof-object? lln) #f)
      ((irregex-match ask-opaque-re lln) =>
       (lambda (m)
	 (let* ((s1 (read-string (string->number (irregex-match-substring m 1)) from))
		(s2 (read-string (string->number (irregex-match-substring m 2)) from)))
	   (cons s1 s2))))
      (else (error 'peer-read/opaque "Broken packet" lln)))))

 (define (peer-read/packet from)
   (let ((tmo (register-timeout-message! (*read-timeout*) (current-thread))))
     (dynamic-wind
	 (lambda () #f)
	 (lambda () (peer-read/opaque* from))
	 (lambda () (cancel-timeout-message! tmo)))))

 (define (peer-read** auth data)
   (and (string? data) (peer-read* auth (open-input-string data))))
 
 (define (peer-read connection)
   (and-let*
    ((raw (peer-read/packet (ask-socket-from connection))))
    (call-with-values
	(lambda () (ask-decode (ask-socket-encoding connection) (cdr raw) (car raw)))
      peer-read**)))
 
 ;; Send to OUT on TAG the OUTPUT (plain data) and RESULTS (encoded).
 (define (prepare-values! out tag flag output results)
   (format
    out "~a ~a ~a VALUES ~a\n" tag
    (case flag
      ((#t) 'DATA)
      ((#f) 'EXCEPTION)
      (else flag))
    (cond
     ((blob? output) (blob-size output))
     ((u8vector? output) (u8vector-length output))
     ((string? output) (string-length output))
     (else (error "send-values! illegal output" output)))
    (length results))
   (let loop ((vals results) (i 1))
     (if (pair? vals)
	 (let ((s (format "~s" (car vals))))
	   (format out "VALUE ~a ~a\n" i (string-length s))
	   (display s out) (newline out)
	   (loop (cdr vals) (add1 i)))))
   (cond
    ((blob? output) (write-u8vector (blob->u8vector/shared output) out))
    ((u8vector? output) (write-u8vector output out))
    (else (display output out)))
   (newline out))

 (define (send-values! connection tag flag output results)
   (let ((encoded
	  (receive
	   (c n) (ask-encode
		  (ask-socket-encoding connection)
		  (call-with-output-string (lambda (out) (prepare-values! out tag flag output results))))
	   (call-with-output-string
	    (lambda (p)
	      (display
	       (cond
		((u8vector? n) (u8vector-length n))
		(else (string-length n)))
	       p)
	      (display #\space p)
	      (display (string-length c) p)
	      (newline p)
	      (cond
	       ((u8vector? n) (write-u8vector n p))
	       (else (display n p)))
	      (display c p)))))
	 (tmo (register-timeout-message! (*write-timeout*) (current-thread))))
     (dynamic-wind
	 (lambda () #f)
	 (lambda () (display encoded (ask-socket-to connection)) (flush-output (ask-socket-to connection)))
	 (lambda () (cancel-timeout-message! tmo))))
   tag)

 (define (pong-handler to packet)
   (if ##dbg#tw (logerr "~s in ~a\n" (wirepacket-values packet) to))
   (if (equal? (wirepacket-values packet) '("PING"))
       ((ask-socket-sender to) to (wirepacket-tag packet) (wirepacket-type packet) "" '(PONG))))

 (define-record connection-handler get post exception data unknown)

 (define-inline (%get-connection-handler type kind)
   (case kind
     ((GET) (connection-handler-get type))
     ((POST) (connection-handler-post type))
     ((EXCEPTION) (connection-handler-exception type))
     ((DATA) (connection-handler-data type))
     ((NOOP) pong-handler)
     (else (connection-handler-unknown type))))

 (define (get-connection-handler type kind)
   (cond
    ((eq? kind 'NOOP) pong-handler)
    ((procedure? type) type)
    (else (%get-connection-handler type kind))))
 

 (define sender (make-parameter #f))
 (define *current-tag* (make-parameter #f))
 (define (current-tag) (*current-tag*))

 (define (current-sender)
   (let ((p (sender)))
     (if p (lambda (tag flag output . results) (p tag flag output results))
	 (error "no client"))))
 
 (define (make-simple-exclusive! . name)
   (let ((mux (make-mutex (if (pair? name) (car name) 'exclusive!))))
     (lambda (thunk)
       (if (eq? (mutex-state mux) (current-thread)) (error "HANG" name (current-thread)))
       (handle-exceptions
	ex
	(begin (mutex-unlock! mux) (raise ex))
	(mutex-lock! mux)
	(let ((result (thunk)))
	 (mutex-unlock! mux)
	 result)))))

 (define (*handle-request! connection repl auth input tag)
   (parameterize
    ((*current-tag* tag)
     (sender (lambda (tag flag output results) (server-connection-send! connection tag flag output results))))
    (let* ((results '())
	   (exn #f)
	   (s (with-output-to-string
		(lambda () (handle-exceptions
			    ex (set! exn ex)
			    (set! results (call-with-input-string (blob->string input) (repl auth))))))))
      (if exn
	  (let ((e (call-with-output-string
		    (lambda (to) (print-error-message exn to "Exception") (print-call-chain to 0 (current-thread))))))
	    (server-connection-send! connection tag #f s (list e)))
	  (server-connection-send! connection tag #t s results)))))

 (define *handler-pool*
   (make-threadpool
    '*handler-pool* 100
    (make-threadpool-requesttype
     (lambda (entry exception)
       (print-error-message exception (current-error-port) "Exception in connection handler")
       (print-call-chain (current-error-port) 0 (current-thread)))
     (lambda (entry) #t))))

 (define (##dbg#server-handler-pool) *handler-pool*)

 #;(define ask-use-handlerpool (make-observable 'ask-use-handlerpool #t))

 (define (csi-repl1 input)
   (let loop ((r '()) (e (read input)))
     (if (eof-object? e) r
	 (loop (receive list (eval e) list) (read input)))))
 (define (csi-repl auth) csi-repl1)

 (define (make-evil-connection receiver . repl)
   (let* ((repl-ro (if (pair? repl) (or (car repl) csi-repl) csi-repl))
	  (repl-rw (if (and (pair? repl) (pair? (cdr repl))) (cadr repl) csi-repl))
	  (check-auth (lambda (packet) (wirepacket-auth packet))))
     (make-connection-handler
      ;; get
      (lambda (connection packet)
	(if (check-auth packet)
	    (*handle-request! connection repl-ro (wirepacket-auth packet) (wirepacket-data packet) (wirepacket-tag packet))
	    (server-connection-send! connection (wirepacket-tag packet) #f "" (list "Not Authorized"))))
      ;; post
      (lambda (connection packet)
	(if (check-auth packet)
	    (*handle-request! connection repl-rw (wirepacket-auth packet) (wirepacket-data packet) (wirepacket-tag packet))
	    (server-connection-send! connection (wirepacket-tag packet) #f "" (list "Not Authorized"))))
      ;; exception
      (lambda (connection packet)
	(receiver #f packet))
      ;; data
      (lambda (connection packet)
	(receiver #t packet))
      ;; unknown
      (lambda (connection packet)
	(receiver #t packet)))))

 (define (server-connection-send! connection tag flag output results)
   ((ask-socket-runexcl connection)
    (lambda ()
      (if (ask-socket-connected? connection)
	  (send-values! connection tag flag output results)
	  (error "lost connection" connection)))))

 (define (handle-conn connection)
   (ask-encoding-init! connection)
   (let loop ((inping #f))
     ((handle-exceptions
       ex
       (cond
	((and (timeout-object? ex) (not inping))
	 (if ##dbg#tw (logerr "Server pinging client ~a\n" connection))
	 (server-connection-send! connection 1 'NOOP "" '(PING))
	 loop)
	(else
	 (if ##dbg#tw (logerr "Server lost client (inping: ~a) ~a\n" inping connection))
	 (raise ex)))
       (let loop ((n 1))
	 (let ((packet (peer-read connection)))
	   (if (wirepacket? packet)
	       (let ((receiver (ask-socket-receiver connection)))
		 (observe-socket-active! connection)
		 (set! inping #f)
		 (threadpool-order/blocking!
		  *handler-pool* connection
		  (get-connection-handler receiver (wirepacket-type packet))
		  (list packet))
		 (loop (add1 n)))
	       (lambda (inping) (if ##dbg#tw (logerr "Server client closed ~a\n" connection)) #t)))))
      #t)))

 (define *connection-pool*
   (make-threadpool
    '*connection-pool* 30
    (make-threadpool-requesttype
     (lambda (entry exception)
       (print-error-message exception (current-error-port) "Exception in server")
       (print-call-chain (current-error-port) 0 (current-thread))
       (ask-socket-close! entry))
     (lambda (entry)
       (ask-socket-close! entry)
       ))))

 (define (##dbg#server-connection-pool) *connection-pool*)

 (define (make-gui-repl1 eval)
   (lambda (input)
     (let ((quorum (make-quorum '())))
       (define (current-quorum)
	 `((quorum ,@(if (quorum-local? (force quorum)) (list (public-oid)) '())
		   . ,(quorum-others (force quorum)))))
       (define (evaluate expr)
	 (if (null? (quorum-others (force quorum)))
	     (eval expr)
	     (if (quorum-local? (force quorum))
		 (begin
		   (http-eval #f (force quorum) expr)
		   (eval expr))
		 (let* ((t0 (socket-current-milliseconds))
			(answer (within-time
				20
				(http-eval #t (force quorum) expr))))
		   (logerr "Request to ~a took ~ams.\n" (force quorum) (- (socket-current-milliseconds) t0))
		   (if (frame? answer)
		       (let ((res (xml-parse
				   (get-slot answer 'mind-body))))
			 (if (eq? (gi (document-element res))
				  'evaluation-result)
			     (begin
			       (display (data ((sxpath '(output)) res)))
			       (let ((vals (map
					    (lambda (r)
					      (let ((d (data r)))
						(format #t "~a" d)
						(guard
						 (ex (else ex))
						 (call-with-input-string
						  d read))))
					    ((sxpath '(results *)) res))))
				 (apply values vals)))
			     (begin
			       ;; (display (xml-format ((sxpath '(Body Fault Reason Text *any*)) res)))
			       (display (xml-format res))
			       (values))))
		       (raise answer))))))
       (let loop ((r '()) (e (sweet-read input)))
	 (if (eof-object? e) r
	     (loop
	      (cond
	       ((eq? e 'q) (current-quorum))
	       ((and (pair? e) (eq? (car e) 'q))
		(let ((arg (cdr e)))
		  (if (pair? arg)
		      (let ((q (eval (cons 'list arg))))
			(set! quorum (cond
				      ((quorum? (car q)) (car q))
				      (else (make-quorum q))))))
		  (current-quorum)))
		(else (receive list (evaluate e) list)))
	       (sweet-read input))))
	 #;(let ((e (sweet-read input))) (if (eof-object? e) e (evaluate e)))
       )))

 (define (make-gui-repl make-eval)
   (lambda (auth) (make-gui-repl1 (make-eval auth))))
 
#; (define (gui-repl input)
  (let loop ((r '()) (e (sweet-read input)))
    (if (eof-object? e) r
	(loop (receive list (eval e) list)
	      (sweet-read input)))))
 
#; (define (run encoding receiver forking port . host)
   (let ((listener (tcp-listen port 100 (or (and (pair? host) (car host)) "localhost"))))
     (let loop ()
       (receive
	(in out) (tcp-accept listener)
	(let ((conn (make-ask-socket
		     (cons port host)
		     (and (procedure? encoding) (encoding))
		     #f 0 receiver
		     server-connection-send! (make-simple-exclusive! out) out in
		     #f	;; setup: would have to wait for next incoming connection
		     (cons port host))))
	  (observe-socket-active! conn)
	  (threadpool-order/blocking!
	   *connection-pool* conn
	   (if forking
	       (lambda (conn)
		 (process-wait (process-fork (lambda () (handle-conn conn)))) #t)
	       handle-conn)
	   '())))
       (loop))))

#; (define (run& encoding receiver forking port . host)
   (process-fork (lambda () (apply run encoding receiver forking port host))))

#; (define (kill-server pid) (process-signal pid) (process-wait pid))

 (define (run encoding receiver port . args)
   (askemos:run-tcp-server
    #f port
    (let ((receiver (cond
		     ((connection-handler? receiver) receiver)
		     ((and (pair? args) (pair? (cdr args)))
		      (make-evil-connection receiver (make-gui-repl (car args)) (make-gui-repl (cadr args))))
		     (else
		      (make-evil-connection receiver (if (pair? args) (car args) (make-gui-repl eval))))))
	  (name args))
      (lambda (in out peer)
	;; (logerr "GUI CONNECT FROM ~a\n" peer)
	(handle-conn
	 (make-ask-socket
	  name encoding
          #f 0 receiver
	  server-connection-send! (make-simple-exclusive! peer) out in
	  #f	;; setup: would have to wait for next incoming connection
	  `(#(,peer) ,port . ,args)))))
    #f #f "gui-access"))

 
 ;; This part belongs into the client (after the split, which is already done in the development area.

 (define (make-connection name encoding receiver setup . args)
   (make-ask-socket
    name encoding
    #f 0 receiver *client-connection-send!
    (make-simple-exclusive! name) #f #f (or setup tcp-connect) args))

 (define connection-state-now! ask-socket-state-now!)
 (define observe-connection-active! observe-socket-active!)
 
 (define client-connection-close! ask-socket-close!)

 (define (%assure-no-port-lost! way x)
   (if x (thread-start! (lambda () (way x)))))

 (define (%refresh-client-connection! conn)
   (if ##dbg#tw (logerr "Client connecting ~a\n" conn))
   (connection-state-now! conn 'setup)
   (let ((tmo (register-timeout-message! (*read-timeout*) (current-thread))))
     (handle-exceptions
      ex (begin
	   (cancel-timeout-message! tmo)
	   ;; Avoid hanging here.
	   (threadpool-order! *client-handler-pool* conn client-connection-close! '())
	   (raise ex))
      (receive
       (in out) (apply (ask-socket-setup conn) (ask-socket-setup-args conn))
       (%assure-no-port-lost! close-input-port (ask-socket-from conn))
       (ask-socket-from-set! conn in)
       (%assure-no-port-lost! close-output-port (ask-socket-to conn))
       (ask-socket-to-set! conn out)
       (ask-encoding-init! conn)
       (cancel-timeout-message! tmo)))
     (connection-state-now! conn 'connected)
     (let ((t (make-thread watch-connection-connloop (ask-socket-name conn))))
       (thread-specific-set! t conn)
       (thread-start! t))))

 (define (refresh-client-connection! conn)
   ((ask-socket-runexcl conn)
    (lambda ()
      (if (case (ask-socket-state conn)
	    ((#f new disconnected closing/read) #t)
	    ((setup) (error "refresh-client-connection! while in setup"))
	    (else #f))
	  (%refresh-client-connection! conn)))))
 
 (define ask-connected? ask-socket-connected?)
 (define ask-active? ask-socket-active?)


 (define *client-handler-pool*
   (make-threadpool
    '*client-handler-pool* 100
    (make-threadpool-requesttype
     (lambda (entry exception)
       (print-error-message exception (current-error-port) "Exception in client connection handler")
       (print-call-chain (current-error-port) 0 (current-thread))
       (if (ask-socket? entry)
	   (client-connection-close! entry)))
     (lambda (entry) #t))))

 (define (##dbg#client-handler-pool) *client-handler-pool*)

 (define (*client-connection-send! connection tag flag data results)
   (or (ask-connected? connection) (refresh-client-connection! connection))
   ((ask-socket-runexcl connection)
    (lambda ()
      #;(if (pair? r) (apply (car r) (cdr r)))
      (if (ask-connected? connection)
	  (send-values! connection tag flag data results)
	  (error "lost connection" connection)))))

 (define (watch-connection-connloop)
   (define connection (thread-specific (current-thread)))
   (handle-exceptions
    exn
    (cond
     ((and (timeout-object? exn) #;(eq? (ask-socket-state connection) 'connected))
      (threadpool-order!
       *client-handler-pool* connection
       (lambda (connection)
	 ((ask-socket-runexcl connection) (lambda () (send-values! connection 1 'NOOP "" '(PING)))))
       '())
      (watch-connection-connloop))
     (else
      (client-connection-close! connection)
      (print-error-message exn (current-error-port) "Exception in watch-connection")
      (print-call-chain (current-error-port) 0 (current-thread))))
    (let loop ()
      (let ((r (peer-read connection)))
	(cond
	 ((wirepacket? r)
	  (threadpool-order/blocking!
	   *client-handler-pool* connection
	   (get-connection-handler (ask-socket-receiver connection) (wirepacket-type r))
	   (list r))
	  (loop))
	 ((procedure? (ask-socket-receiver connection))
	  (threadpool-order/blocking! *client-handler-pool* connection (ask-socket-receiver connection) (list r))))
	(and r
	     (ask-active? connection)
	     (begin (observe-connection-active! connection) (loop)))))
    (if ##dbg#tw (logerr "Client connection lost ~a\n" connection))
    (if (ask-socket-from connection)
	(begin connection
	       (close-input-port (ask-socket-from connection))
	       (ask-socket-from-set! connection #f)))
    (connection-state-now! connection 'closing/read)))

 (define (client-connection-send! connection tag flag data results)
   (handle-exceptions
    ex (let ((receiver (ask-socket-receiver connection)))
	 (if ##dbg#tw (logerr "Client ~a exception ~a\n" connection ex))
	 (client-connection-close! connection)
	 (and receiver (receiver #f (list connection ex)))
	 (raise ex))
    (*client-connection-send! connection tag flag data results)))

(define ##debug#ask--client-connection-send!)

 
 ;; This part belongs into keydb handling module.
(import sqlite3 tweetnacl lolevel)
(define keydb #f)

(define (keydb-open! file)
  (set! keydb (sqlite3-open-restricted file)))

(define (keydb-select query . args)
  (apply sqlite3-exec keydb (sqlite3-prepare keydb query) args))

(define keydb-exec! keydb-select)

(define (init-keydb!)
  (sqlite3-exec keydb "
create table if not exists sk (id integer primary key, sk blob, pk blob, nonce blob, unique(sk,pk) );
create table if not exists pk (
 id integer primary key,
 location text not null, user text not null,
 pk blob not null,
 certified integer default 0,
 request blob,
 unique(location, user, pk)
);
create index if not exists pk_by_blob on pk(pk);
create table if not exists outstanding(
 id integer primary key autoincrement,
 location text, user text,
 issued date, completed date
);
create index if not exists outstanting_by_cause on outstanding(id, location, user);
create index if not exists outstanding_by_issued on outstanding(issued);
"))

(: renonce ((struct <sqlite3-database>) blob -> blob))
(define renonce
  (let ((last-index (sub1 (quotient asymmetric-box-noncebytes 4))))
    (lambda (db sk)
      ;; This results in warnings for type violations - we probably should fix the type decls.
      (sqlite3-call-with-transaction
       db
       (lambda (sql)
	 (let* ((old (sql "select nonce from sk where sk=?1" sk))
		(nv (blob->u32vector/shared (sql-ref old 0 0))))
	   (u32vector-set! nv last-index (add1 (u32vector-ref nv last-index)))
	   (let ((nn (u32vector->blob/shared nv)))
	     (sql "update sk set nonce=?2 where sk=?1" sk nn)
	     nn)))))))

(: renonce-keydb! (blob -> blob))
(define (renonce-keydb! sk) (renonce keydb sk))

(define (authenticate-pk/index! index val)
  (keydb-exec! "update pk set certified=?2 where id=?1" index val))

(define (conv-key key)
  (cond
   ((blob? key) key)
   ((string? key)
    (let ((v (make-u8vector asymmetric-sign-publickeybytes 0)))
      (do ((i 0 (add1 i)) (j 0 (fx+ j 2)))
	  ((fx= i (u8vector-length v)) (u8vector->blob/shared v))
	(u8vector-set! v i (string->number (substring key j (fx+ j 2)) 16)))))
   (else (error "illegal key" key))))

(define (keydb-add-keypair! pk sk . nonce)
  (sqlite3-exec
   keydb "insert into sk(pk,sk,nonce) values(?1, ?2, ?3)" (conv-key pk) (conv-key sk)
   (if (pair? nonce) (car nonce) (u8vector->blob/shared (make-u8vector asymmetric-box-noncebytes 0)))))

(: keydb-add-pk! (blob (or false string) (or false string) (or false blob) -> *))
(define (keydb-add-pk! pk location user request)
  (sqlite3-exec
   keydb "insert or ignore into pk(pk,location,user,request) values(?1,?2,?3,?4)"
   (conv-key pk)
   (or location "-")
   (or user "-")
   (or request (sql-null)))
  #;(cond
   ((and location user)
    (if request
	(sqlite3-exec keydb "insert or ignore into pk(pk,location,user,request) values(?1,?2,?3,?4)" (conv-key pk) location user request)
	(sqlite3-exec keydb "insert or ignore into pk(pk,location,user) values(?1,?2,?3)" (conv-key pk) location user)))
   (user
    (error "keydb-add-pk! user by itself: NYI")
    (if request
	(sqlite3-exec keydb "insert or ignore into pk(pk,user) values(?1,?2)" (conv-key pk) user)
	(sqlite3-exec keydb "insert or ignore into pk(pk,user) values(?1,?2)" (conv-key pk) user)))
   (location
    (if request
	(sqlite3-exec keydb "insert or ignore into pk(pk,location) values(?1,?2,?3) where user is null"
		      (conv-key pk) location request)
	(sqlite3-exec keydb "insert or ignore into pk(pk,location) values(?1,?2)" (conv-key pk) location)))
   (else (sqlite3-exec keydb "insert or ignore into pk(pk) values(?1)" (conv-key pk)))))

;;(: keydb-get-auth (blob (procedure ((or false string) (or false string)) *) -> *))
(define (keydb-get-auth pk create)
(assert (blob? pk))
  (let ((e (keydb-select "select location, user from pk where pk=?1 and certified" pk)))
    (if (> (sql-ref e #f #f) 0)
	(let ((l (let ((l (sql-ref e 0 0))) (and (not (equal? l "-")) l)))
	      (o (let ((u (sql-ref e 0 1))) (and (not (equal? u "-")) u))))
	  (if ##dbg#tw (logerr "tnacl: L: ~a O: ~a for ~a\n" l o pk))
	  (create l o))
	(begin
	  (if ##dbg#tw (logerr "tnacl: no auth for ~a\n" pk))
	  #f))))

(define (keydb-get-keys index)
  (let ((keys (keydb-select "select pk, sk from sk where id=?1" index)))
    (values (sql-ref keys 0 0) (sql-ref keys 0 1))))

(define-record twenc box unbox lpk lsk ln ln1 nn rpk mauth auth)

(define (ask-socket-encoding-rpk c)
  (and-let*
   ((e (ask-socket-encoding c))
    (n (encoding-special e))
    ((twenc? n))
    ;; (((encoding-validator e) n))
    )
   (twenc-rpk n)))

(define (ask-socket-peer-address c) ;; a horrible hack
  (and (ask-socket-connected? c)
       (match
	(ask-socket-setup-args c)
	((#(peer) ...) peer)
	((host port) (list host port))
	(_ #f))))

(define (ask-socket-refresh-auth! c)
  (and (ask-socket-connected? c)
       (let ((e (ask-socket-encoding c)))
	 ((encoding-refresh e) e (ask-socket-from c) (ask-socket-to c)))))

(define make-twencoding
  (begin
    (define (renonce! special)
      (let* ((b (renonce-keydb! (twenc-lsk special)))
	     (c (blob->u32vector/shared b)))
	(u32vector-set! c 0 1)
	(twenc-ln-set! special b)
	(twenc-ln1-set! special c)))
    (define (reserve! special)
      ;; This must actually be compiled with disable-interrupts, since the increment must be atomic.
      (let* ((n0 (twenc-ln1 special))
	     (n (u32vector-ref n0 0)))
	;; TBD: better use add1 "as is"; but should not raise overflow exception...
	(u32vector-set! n0 0 (if (< n 536870912) (add1 n) 0))) )
    (define (valid? special)
      (and-let* ((ln1 (twenc-ln1 special))) (not (= (u32vector-ref ln1 0) 0))))
    (define (encode special data)
      (reserve! special)
      (let ((n (blob->u8vector/shared (twenc-ln special))))
	(values ((twenc-box special) data n) n)))
    (define (decode special data nonce)
      (let ((nv (make-u8vector asymmetric-box-noncebytes)))
	(move-memory! nonce nv (string-length nonce) 0 0)
	(values
	 (twenc-auth special)
	 ((twenc-unbox special) data nv))))
    (define (refresh! special in out)
      (and-let* ((rpk (twenc-rpk special))) (twenc-auth-set! special ((twenc-mauth special) rpk))))
    (define (init! special in out)
      (write-u8vector (blob->u8vector/shared (twenc-lpk special)) out)
      (flush-output out)
      (let ((ypk (u8vector->blob/shared
		  (within-time%1 (*read-available-timeout*) (read-u8vector asymmetric-sign-publickeybytes in)))))
	(if (= (blob-size ypk) asymmetric-sign-publickeybytes)
	    (let ((msk (twenc-lsk special)))
	      (twenc-rpk-set! special ypk)
	      (renonce! special)
	      (twenc-box-set! special (asymmetric-box ypk msk))
	      (twenc-unbox-set! special (asymmetric-unbox ypk msk))
	      (refresh! special in out))
	    (error "invalid public key"))))
    (define (make mpk msk mauth)
      (make-twenc
       #f #f mpk msk #f #f reserve! #f
       (lambda (pk) (keydb-get-auth pk mauth))
       #f))
    (lambda (mpk msk mauth)
      (make-encoding valid? encode decode init! refresh! (make mpk msk mauth)))))
 

 ) (import (prefix ask-repl-server ask-repl-server:))

(define (gui-server encoding port . args)
 (apply ask-repl-server:run encoding (lambda (s v) (logerr "GUI server unexpected: ~a\n" v)) port args))
(define (gui-server2 encoding handler port . args)
  (apply ask-repl-server:run encoding handler port args))
(define make-gui-repl ask-repl-server:make-gui-repl)
(define make-evil-connection ask-repl-server:make-evil-connection)
(define (public-server port encoding get post exn data unknown)
  (ask-repl-server:run encoding (ask-repl-server:make-connection-handler get post exn data unknown) port))
(define make-connection-handler ask-repl-server:make-connection-handler)

;(define run& ask-repl-server:run&)
;;(define kill-server ask-repl-server:kill-server)
(define current-sender ask-repl-server:current-sender)
(define current-tag ask-repl-server:current-tag)

(define wirepacket? ask-repl-server:wirepacket?)
(define wirepacket-auth ask-repl-server:wirepacket-auth)
(define wirepacket-tag ask-repl-server:wirepacket-tag)
(define wirepacket-type ask-repl-server:wirepacket-type)
(define wirepacket-data ask-repl-server:wirepacket-data)
(define wirepacket-values ask-repl-server:wirepacket-values)

(define make-connection ask-repl-server:make-connection)
(define make-encoding ask-repl-server:make-encoding)
(define ask-encode ask-repl-server:ask-encode)
(define ask-decode ask-repl-server:ask-decode)
(define ask-socket? ask-repl-server:ask-socket?)
(define ask-socket-connected? ask-repl-server:ask-socket-connected?)
(define ask-socket-active? ask-repl-server:ask-socket-active?)
(define ask-socket-name ask-repl-server:ask-socket-name)
(define ask-socket-state ask-repl-server:ask-socket-state)
(define ask-socket-time ask-repl-server:ask-socket-time)
(define ask-socket-close! ask-repl-server:ask-socket-close!)
(define ask-socket-refresh-auth! ask-repl-server:ask-socket-refresh-auth!)
(define ask-send! ask-repl-server:ask-send!)
(define ask-query! ask-repl-server:ask-query!)
(define ask-reply! ask-repl-server:ask-reply!)
(define ask-signal! ask-repl-server:ask-signal!)
;; keydb
(define keydb-open! ask-repl-server:keydb-open!)
(define keydb-select ask-repl-server:keydb-select)
(define keydb-exec! ask-repl-server:keydb-exec!)
(define init-keydb! ask-repl-server:init-keydb!)
(define keydb-get-keys ask-repl-server:keydb-get-keys)
(define keydb-add-pk! ask-repl-server:keydb-add-pk!)
(define authenticate-pk/index! ask-repl-server:authenticate-pk/index!)
(define keydb-add-keypair! ask-repl-server:keydb-add-keypair!)
(define make-twencoding ask-repl-server:make-twencoding)

;; debug
;;(define client-connection-send! ask-repl-server:client-connection-send!)
(define client-connection-send! ##debug#ask--client-connection-send!)
(define renonce-keydb! ask-repl-server:renonce-keydb!)
(define keydb-get-auth ask-repl-server:keydb-get-auth)

(define (dbg-tw . args) (let ((s ##dbg#tw)) (if (pair? args) (set! ##dbg#tw (car args))) s))
