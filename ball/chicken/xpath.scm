;; (C) 2002, 2013, 2017 Jörg F. Wittenberger

;; chicken module clause for the Askemos notation module.

(declare
 (unit xpath)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (disable-interrupts)			; checked loops
 (strict-types)
 (usual-integrations)
 (fixnum)
)

(module
 xpath
 (
  set-node-variable-reference!
  make-xpath-selection
  make-xpath-match
  xpath-eval
  xpath:parse
  xpath:parse-expr
  txpath
  ;;
  xpath-optimizable-match?		; from xslt - heavily depends on internals of xpath
  ;;
  xpath-decompile
  )

(import (except scheme vector-fill! vector->list list->vector)
	(except chicken add1 sub1 condition? vector-copy! with-exception-handler)
	srfi-1 srfi-13 srfi-34 srfi-35
	memoize regex pcre util ports tree extras
	matchable environments
	)

(include "typedefs.scm")
;(include "../mechanism/syntax/petrofsky-extract.scm")
;(include "../mechanism/syntax/keyword-syntax.scm")

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(include "../mechanism/notation/xpath.scm")

(define ##test#xpath-decompile xpath-decompile)

(define xpath:axis-specifier-read
  (let ((match (regexp
                (apply string-append
                       (cons* "^(" (car xpath:axis-specifier-list)
                              (fold (lambda (a d) (cons* "|" a d))
                                    '(")[[:blank:]]*::")
                                    (cdr xpath:axis-specifier-list)))))))
    (lambda (str off)
      (let ((result (string-search-positions match str off)))
        (if result
            (values (cadar result)
                    (cdr (assoc (substring str (caadr result) (cadadr result))
                                xpath:axis-specifier-indicators)))
            (values #f #f))))))

(define xpath:literal-regex-dq
  (let ((match (regexp "^\"([^\"]*)\"")))
    (lambda (str off)
      (let ((result (string-search-positions match str off)))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caadr result) (cadadr result)))
            (values #f #f #f))))))

(define xpath:literal-regex-sq
  (let ((match (regexp "^'([^']*)'")))
    (lambda (str off)
      (let ((result (string-search-positions match str off)))
        (if result
            (values (caar result) (cadar result)
                    (substring str (caadr result) (cadadr result)))
            (values #f #f #f))))))

(define xpath:node-type-test
  (let ((match (regexp (apply string-append
			       (cons* "^(" (car xpath:node-type-list)
				      (fold (lambda (a d) (cons* "|" a d))
					    '(")[[:blank:]]*\\(\\)")
					    (cdr xpath:node-type-list)))))))
    (define (xpath:node-type-test str off)
      (let ((result (string-search-positions match str off)))
        (if result
            (values (cadar result)
                    (cdr (assoc (substring str (caadr result) (cadadr result))
                                xpath:node-type-indicators)))
            (values #f #f))))
    xpath:node-type-test))

(define xpath:number
  (let ((match (regexp
                "^((?:[[:digit:]]+(?:\\.[[:digit:]]+)?)|(?:\\.[[:digit:]]+))")))
    (lambda (str off)
      (let  ((result (string-search-positions match str off)))
	(if result
	    (values
	     (cadar result)
	     (string->number (substring str (caadr result) (cadadr result))))
	    (values #f #f))))))

)

(import (prefix xpath m:))

(define txpath m:txpath)
