(declare
 (unit wot)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)
 ;; (disable-interrupts)
 )

(module
 wot
 *
 (import (except scheme force delay))
 (import util tree notation aggregate pool place-common corexml place protocol protocol-common storage-common storage-fsm)
 (import (except chicken promise? with-exception-handler))
 (import srfi-4 srfi-34 srfi-45 srfi-110)
 (import ports extras)
 (import ask-repl-server)
 (import matchable)
 (import llrb-tree)
 (import srfi-1 srfi-19)
 (import sqlite3)
 
 (define (format-body obj)
   (let ((bdy (message-body/plain (aggregate-meta obj))))
     (make-xml-element
      'output namespace-mind
      (list
       (make-xml-attribute
	'method #f "text")
       (make-xml-attribute 'media-type #f "text/plain"))
      ((if (string? bdy) literal body->string) bdy))))

 (define (format-peering-request location user)
  (let ((location (document location))
	(user (and user (document user)))
	(document (lambda (x) (find-local-frame-by-id x 'format-peering-request)))
	(msg (make-xml-element 'message #f '() (empty-node-list))))
    (mime-format
     (if user
	 (let* ((contract (document (get-slot (aggregate-meta user) 'mind-action-document)))
		(mc (document (get-slot (aggregate-meta contract) 'mind-action-document))))
	   (node-list
	    msg
	    (frame-signature location)
	    (format-body location)
	    (frame-signature mc)
	    (format-body mc)
	    (frame-signature contract)
	    (format-body contract)
	    (frame-metadata user)
	    (format-body user)))
	 (node-list
	  msg
	  (frame-signature location)
	  (format-body location))))))

 (define-record peering-request boundary body message location mc contract user)

 (define (peering-request-location-id x)
   (aggregate-entity (vector-ref (peering-request-location x) 2)))

 (define (peering-request-entry-id x)
   (aggregate-entity (vector-ref (debug 'E (peering-request-user x)) 2)))

 (define (read-peering-request body boundary)
   (let* ((boundary2 (multipart-data-boundary boundary))
	  (parts (list->vector
		  (map
		   (lambda (p)
		     (call-with-input-string
		      p
		      (lambda (port) (http-read-content port #t #f ($large-request-handler)))))
		   (begin
		     (or (string? boundary2) (error "illegal boundary" boundary))
		     (rfc2046-split
		      (if (blob? body) (blob->string body) body)
		      boundary2)))))
	  (us? (> (vector-length parts) 3)))
     (define (tof meta body)
       (vector
	meta body
	(let* ((rdf (xml-parse meta))
	       (m2 ((sxpath '(Description)) rdf))
	       (f0 (read-rdf! (spool-directory) (aggregate (make-message) (string->oid (attribute-string 'about (node-list-first m2)))) m2)))
	  ;; (debug 'ReadBody (fget f0 'mind-body))
	  (fset! f0 'mind-body body)
	  f0)))
     (make-peering-request
      boundary body
      (vector (message-body/plain (vector-ref parts 0)) #f #f)
      (tof (message-body/plain (vector-ref parts 1)) (message-body/plain (vector-ref parts 2)))
      (and us? (tof (message-body/plain (vector-ref parts 3)) (message-body/plain (vector-ref parts 4))))
      (and us? (tof (message-body/plain (vector-ref parts 5)) (message-body/plain (vector-ref parts 6))))
      (and us? (tof (message-body/plain (vector-ref parts 7)) (message-body/plain (vector-ref parts 8))))
      )
     ))
 
 (define fineprint-immutable?
   (let* ((select (sxpath '(method programlisting)))
	  (read sweet-read)
	  (pubcode
	   (delay
	     (call-with-input-string
	      (data (select
		     (aggregate-body
		      (or (find-local-frame-by-id (public-oid) 'rough-public-equivalent?)
			  (raise (make-object-not-available-condition
				  (public-oid) (public-oid) 'rough-public-equivalent?))))))
	      read))))
     (lambda (frame)
       (equal?
	(guard
	 (ex ;; ((enable-warnings) (log-condition oid ex) #f)
	  (else (logerr #;logagree "Q ~a public equiv test ~a\n" (aggregate-entity frame) (condition->string ex)) #f))
	 (call-with-input-string
	  (data (select (aggregate-body frame)))
	  read))
	(force pubcode)))))

 (define (check-peering-request x)
   (define (csp rdf)
     (document-element (xml-parse rdf)))
   (define (cs l x)
     ;; (logerr "Test ~a ~a\n" l (if (string? x) x (xml-format x)))
     (xml-digest-simple x))
   (let ((pass #t))
     (define (holds? msg p)
       (let ((r (p)))
	 (if r
	     (format (current-output-port) "Pass: ~a\n" msg)
	     (format (current-output-port) "FAIL: ~a\n" msg))
	 (if (and (not r) pass) (set! pass #f))))
     (holds?
      "fields"
      (lambda ()
	(and (peering-request-location x)
	     (or (not (peering-request-user x))
		 (and (peering-request-contract x) )))))
     (holds?
      "location reproducible"
      (lambda ()
	(equal?
	 (cs "location plain" (csp (vector-ref (peering-request-location x) 0)))
	 (cs "location reproduced" (frame-signature (vector-ref (peering-request-location x) 2))))))
     (holds?
      "location immutable"
      (lambda ()
	(fineprint-immutable? (vector-ref (peering-request-location x) 2))))
     (if (peering-request-user x)
	 (begin
	   (holds?
	    "meta contract reproducible"
	    (lambda ()
	      (equal?
	       (cs "meta contract plain:" (csp (vector-ref (peering-request-mc x) 0)))
	       (cs "meta contract reproduced" (frame-signature (vector-ref (peering-request-mc x) 2))))))
	   (holds?
	    "meta contract immutable"
	    (lambda ()
	      (fineprint-immutable? (vector-ref (peering-request-mc x) 2))))
	   (holds?
	    "contract reproducible"
	    (lambda ()
	      (equal?
	       (cs "contract plain:" (csp (vector-ref (peering-request-contract x) 0)))
	       (cs "contract reproduced" (frame-signature (vector-ref (peering-request-contract x) 2))))))
	   (holds?
	    "contract original"
	    (lambda ()
	      (let ((f (vector-ref (peering-request-contract x) 2)))
		(frame-oid-match? (aggregate-meta f) (aggregate-entity f)))))
	   (holds?
	    "meta contract effective"
	    (lambda ()
	      (equal? (fget (vector-ref (peering-request-contract x) 2) 'mind-action-document)
		      (aggregate-entity (vector-ref (peering-request-mc x) 2)))))
	   (holds?
	    "user reproducible"
	    (lambda ()
	      (equal?
	       (cs "user plain:" (csp (vector-ref (peering-request-user x) 0)))
	       (cs "user reproduced" (frame-metadata (vector-ref (peering-request-user x) 2))))))))
     pass))


 (define *hosts-by-id* (make-table (make-llrb-treetype)))
 ;; FIXME: provide a comparator for blobs intest of conversion.
 (define *hosts-by-pk* (make-table (make-llrb-treetype)))
 (define-inline (ckey->tkey x) (blob->u8vector/shared x))
 
 (define (hosts)
   (table-fold
    *hosts-by-id*
    (lambda (k v i) '((,k . ,v) . ,i))
    '()))

 (define (hosts-insert! id connection)
   (table-update!
    *hosts-by-id*
    id
    (lambda (old)
      (cons connection old))
    (lambda () '()))
   (and-let*
    ((pk (ask-socket-encoding-rpk connection)))
    (table-update!
     *hosts-by-pk*
     (ckey->tkey pk)
     (lambda (old)
       (cons (cons id connection) old))
     (lambda () '()))))

 (define (hosts-delete/connection! c)
   (and-let*
    ((pk (ask-socket-encoding-rpk c))
     (ck (ckey->tkey pk))
     (e (debug 'PKentry (table-ref/default *hosts-by-pk* ck #f)))
     (e1 (debug 'EEconn (find (lambda (e) (eq? (cdr e) c)) e))))
    (if (and (pair? e) (null? (cdr e)))
	(table-delete! *hosts-by-pk* ck)
	(table-update!
	 *hosts-by-pk*
	 ck
	 (lambda (old) (delete e1 old eq?))
	 (lambda () '())))
    (and-let*
     ((e (debug 'ByID (table-ref/default *hosts-by-id* (car e1) #f))))
     (if (and (pair? e) (null? (cdr e)))
	 (table-delete! *hosts-by-id* (car e1))
	 (and-let*
	  ((e2 (assq c e)))
	  (table-update!
	   *hosts-by-id*
	   (car e1)
	   (lambda (old) (delete e2 old eq?))
	   (lambda () '())))))))

 (define (peer-connected! connection packet)
   (let (;;(pk (ask-socket-encoding-rpk connection))
	 (id (blob->string (wirepacket-data packet))))
     (logerr "Certified peer connect from ~a\n" id)
     (hosts-insert! id connection)))

 (define (hosts-refresh-auth! pk)
   (assert (blob? pk))
   (for-each
    (lambda (e) (debug 'RefreSched (ask-socket-refresh-auth! (cdr (debug 'Refresh e)))))
    (table-ref/default *hosts-by-pk* (ckey->tkey pk) '())))

 (define-inline (date->sqlite3 date)
   (srfi19:date->string date "~Y-~m-~d ~H:~M:~S ~z"))

 (define (certify-pk! pk location user)
   (let ((location (if (string? location) location (oid->string location)))
	 (user (and user (if (string? user) location (oid->string user)))))
     (let ((r (if user
		  (keydb-select "select id from pk where location=?1 and user=?2" location user)
		  (keydb-select "select id from pk where location=?1" location))))
       (case (sql-ref r #f #f)
	 ((0) (error "no entry in keyring for" location user))
	 ((1) (logerr "Confirming PK #~a\n" (sql-ref r 0 0)))
	 (else (error "many entries in keyring for" location user (sql-ref r #f #f)))))

     (if user
	 (keydb-exec!
	  "update pk set certified=1 where pk=?1 and location=?2 and user=?3"
	  pk location user)
	 (keydb-exec!
	  "update pk set certified=1 where pk=?1 and location=?2" pk location)))
   (hosts-refresh-auth! pk)
   #t)

 (define (certify-on-open-channel! pk req raw-request)
   (define (store! select enforce)
     (and-let*
      ((frame (vector-ref (select req) 2)))
      (let ((local (if enforce #f (find-local-frame-by-id (aggregate-entity frame) 'certify-on-open-channel!))))
	(if (not local)
	    (commit-frame! #f frame (aggregate-meta frame) '())))))
   (or (check-peering-request req)
       (error "request did not pass verification" 400))
   (store! peering-request-location #f)
   (let ((entry (peering-request-user req)))
     (logerr "Certifying ~a ~a\n" (peering-request-location-id req)
	     (and entry (peering-request-entry-id req)))
     (if entry
	 (begin
	   (store! peering-request-mc #f)
	   (store! peering-request-contract #f)
	   (store! peering-request-user #t)
	   (make-local-entry-point!
	    *local-quorum*
	    (literal (peering-request-entry-id req))
	    (make-xml-element 'id #f '() (literal (peering-request-entry-id req))))))
     (certify-pk! pk (peering-request-location-id req) (and (peering-request-user req) (peering-request-entry-id req)))))
 
 (define open-channel-hook (make-parameter certify-on-open-channel!))

 (define (register-open-channel! pk req raw-request)
   (let ((location (literal (peering-request-location-id req)))
	 (user (and-let* ((user (peering-request-user req))) (literal (aggregate-entity user)))))
     (keydb-add-pk! pk location user raw-request)
     ((open-channel-hook) pk req raw-request)))
 
 (define (wot-send-peering-request! connection packet)
   (receive
    (req boundary) (format-peering-request (public-oid) #f)
    (ask-send! connection (wirepacket-tag packet) req 'open-channel boundary)))

 (define-inline (decode-ask-value x)
   (call-with-input-string x read))

 (define wot-connection-handler
   (make-connection-handler
    ;; get
    (lambda (connection packet)
      (debug 'get (wirepacket-values packet))
      ;;(logerr "req: ~a :: ~a\n" (wirepacket-values packet) (blob->string (wirepacket-data packet)))
      (match
       (wirepacket-values packet)
       (("Hello")
	(if (wirepacket-auth packet)
	    (begin
	      (peer-connected! connection packet)
	      (ask-reply! connection (wirepacket-tag packet) (literal (public-oid)) 'Welcome))
	    (wot-send-peering-request! connection packet)))
       (_ (ask-signal! connection (wirepacket-tag packet) "Nothing to get here."))))
    ;; post
    (lambda (connection packet)
      (logerr "rcv: ~a :: ~a\n" (wirepacket-values packet) (blob->string (wirepacket-data packet)))
      (match
       (wirepacket-values packet)
       (("open-channel" boundary)
	(guard
	 (ex (else (ask-signal! connection (wirepacket-tag packet)
				(format "Registration failed ~a" (condition->string ex)) 'Error)))
	 (let ((pk (or (ask-socket-encoding-rpk connection) (error "no public key found")))
	       (req (read-peering-request (wirepacket-data packet) (decode-ask-value boundary))))
	   (if (register-open-channel! (ask-socket-encoding-rpk connection) req (wirepacket-data packet))
	       (ask-reply! connection (wirepacket-tag packet) "Registration Completed." 'Welcome)
	       (ask-reply! connection (wirepacket-tag packet) "Registered.")))))
       (_ (ask-signal! connection (wirepacket-tag packet) "Nothing to send here."))))
    ;; exception
    (lambda (connection packet)
      (logerr "exn: ~a :: ~a\n" (wirepacket-values packet) (blob->string (wirepacket-data packet))))
    ;; data
    (lambda (connection packet)
      (logerr "dta: \n")
      (match
       (wirepacket-values packet)
       (("Welcome")
	(if (wirepacket-auth packet)
	    (peer-connected! connection packet)
	    (ask-signal! connection (wirepacket-tag packet) "Unexpected 'Welcome'.")))
       (_ (logerr "dta: unhandled ~a :: ~a\n" (wirepacket-values packet) (blob->string (wirepacket-data packet))))))
    ;; unknown
    (lambda (connection packet)
      (logerr "UNKNOWN PACKET: ~a\n" packet)
      (if (eof-object? packet)
	  (debug 'ConDelted (hosts-delete/connection! (debug 'DelConn connection)))))
    ))
 
 (define (wot-server port encoding)
   (run encoding wot-connection-handler port))
 
)
(import (prefix wot wot:))

(define format-peering-request wot:format-peering-request)

(define read-peering-request wot:read-peering-request)

(define check-peering-request wot:check-peering-request)

(define wot-server wot:wot-server)

(define (wot-connection-handler) wot:wot-connection-handler)

(define wot-hosts wot:hosts)
