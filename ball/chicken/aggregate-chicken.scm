(declare
 (unit aggregate-chicken)
 (fixnum-arithmetic)
 (disable-interrupts)			; maybe used from atomic code (+ performance)
 (usual-integrations)
 (strict-types)

 (no-bound-checks)
 (no-procedure-checks-for-usual-bindings)
 (bound-to-procedure
  ##sys#check-symbol
  ##sys#make-structure ##sys#structure? ##sys#check-structure)
  (foreign-declare #<<EOF

#define AC_string_compare(to, from, n, m)   C_mk_bool(C_strncmp(C_c_string(to), C_c_string(from), C_unfix(n)) < C_unfix(m))

#define AC_lessp(x,y) C_mk_bool(x<y)

EOF
))

(module
 aggregate-chicken
 (make-property
  property? property-name property-value
  property-set-fold
  property-set-init! property-lookup property-node-insert! property-delete!
  make-string-ordered-llrbtree
  string-ordered-llrbtree-ref
  string-ordered-llrbtree-ref/default
  string-ordered-llrbtree-set!
  string-ordered-llrbtree-delete!
  string-ordered-llrbtree-for-each
  string-ordered-llrbtree-fold
  string-llrbtree/min
  )
 (import (except scheme symbol->string)
	 (except chicken add1 sub1 vector-copy! with-exception-handler condition? promise?)
	 foreign)
 (import llrb-tree)

 (include "../mechanism/srfi/llrbsyn.scm")

 (: symbol->string (symbol --> string))
 (define-inline (symbol->string s) (##sys#slot s 1))
 
 (define-syntax %string<?
   (syntax-rules ()
     ((_ si1 si2)
      (let ((s1 si1) (s2 si2))
	;;       (##sys#check-string s1 'string-aggregate)
	;;       (##sys#check-string s2 'string-aggregate)
	(let ((len1 (##core#inline "C_block_size" s1))
	      (len2 (##core#inline "C_block_size" s2)) )
	  (cond
	   ((fx= len1 len2)
	    (##core#inline "AC_string_compare" s1 s2 len1 0))
	   ((##core#inline "AC_lessp" len1 len2)
	    (##core#inline "AC_string_compare" s1 s2 len1 1))
	   (else (##core#inline "AC_string_compare" s1 s2 len2 0))) )))))

(define-syntax %string=?
  (syntax-rules ()
    ((_ s1 si2)
     (let ((s2 si2))
       ;; (##sys#check-string s1 'string-aggregate)
       ;; (##sys#check-string s2 'string-aggregate)
       (string=? s1 s2)))))

 (: property? (* --> boolean : (struct <property>)))
 (: property-name ((struct <property>) --> symbol))
 (: property-value ((struct <property>) --> *))
 (: property-set-init! ((struct <property>) -> (struct <property>)))

 (: property-lookup ((struct <property>) symbol --> *))
 (: property-set-fold ((procedure ((struct <property>) *) *) * (struct <property>) -> *))
 (: %property-node-insert! ((struct <property>) (struct <property>) --> (struct <property>)))
 (: property-node-insert! ((struct <property>) symbol (struct <property>) --> (struct <property>)))
 (: property-delete! ((struct <property>) symbol --> (struct <property>)))

 (cond-expand
  (never
   (define-record-type <property>
     (make-property color left right name value)
     property?
     (color property-color property-color-set!)
     (left property-left property-left-set!)
     (right property-right property-right-set!)
     (name property-name property-name-set!)
     (value property-value property-value-set!)))
  (else
   (define (make-property color left right key value)
     (##sys#make-structure '<property> color left right key value))
   (define (property? obj)
     (##sys#structure? obj '<property>))
   (define-inline (property-color n) (##sys#slot n 1))
   (define-inline (property-color-set! n v) (##sys#setslot n 1 v))
   (define-inline (property-left n) (##sys#slot n 2))
   (define-inline (property-left-set! n v) (##sys#setslot n 2 v))
   (define-inline (property-right n) (##sys#slot n 3))
   (define-inline (property-right-set! n v) (##sys#setslot n 3 v))
   (define-inline (%property-name n) (##sys#slot n 4))
   (define (property-name n)
     (##sys#check-structure n '<property> 'property-name)
     (%property-name n))
   (define-inline (property-name-set! n v) (##sys#setslot n 4 v))
   (define-inline (%property-value n) (##sys#slot n 5))
   (define (property-value n)
     (##sys#check-structure n '<property> 'property-value)
     (%property-value n))
   (define-inline (property-value-set! n v) (##sys#setslot n 5 v))))

(define-syntax property-update
  (syntax-rules (left: right: color:)
    ((_ 1 n l r c ())
     (make-property c l r (property-name n) (property-value n)))
    ((_ 1 n l r c (left: v . more))
     (property-update 1 n v r c more))
    ((_ 1 n l r c (right: v . more))
     (property-update 1 n l v c more))
    ((_ 1 n l r c (color: v . more))
     (property-update 1 n l r v more))
    ((_ n . more)
     (property-update 1 n (property-left n) (property-right n) (property-color n) more))
    ))

(define-syntax property-k-n-eq?
  (syntax-rules () ((_ k n) (eq? k (property-name n)))))


(define-syntax property-n-n-eq?
  (syntax-rules () ((_ node1 node2) (eq? (property-name node1) (property-name node2)))))

(define-syntax property-k-n-lt
  (syntax-rules () ((_ k n) (string<? (symbol->string k) (symbol->string (property-name n))))))

(define-syntax property-n-n-lt
  (syntax-rules () ((_ node1 node2) (string<? (symbol->string (property-name node1))
					      (symbol->string (property-name node2))))))

(define-llrbtree/positional
  (ordered pure)
  property-update
  property-set-init!	           ;; defined
  property-lookup		   ;; defined
  #f				   ;; no min defined
  property-set-fold		   ;; defined
  #f				   ;; no for-each defined
  %property-node-insert!	   ;; defined
  property-delete!		   ;; defined
  #f				   ;; no delete-min defined
  property-set-empty?		   ;; defined
  property-k-n-eq?
  property-n-n-eq?
  property-k-n-lt
  property-n-n-lt
  property-left
  property-right
  property-color
  )

(define (property-node-insert! t s p) (%property-node-insert! t p))

 ;; ordered allocation free, impure string llrb tree

(define-type :string-node: (struct <string-binding-node>))
(define-type :string-table: (struct <llrb-string-table>))

(: string-ordered-llrbtree-delete! (:string-table: string -> :string-table:))

(define (string-ordered-llrbtree-delete! t k) (string-table-delete! t k) t)

 (: make-string-ordered-llrbtree (--> :string-table:))

 (define make-string-ordered-llrbtree string-make-table)

 (: string-ordered-llrbtree-set! (:string-table: string * -> *))
 (define string-ordered-llrbtree-set! string-table-set!)

 (: string-ordered-llrbtree-ref/default (:string-table: string * --> *))

 (define string-ordered-llrbtree-ref/default string-table-ref/default)

 (: string-ordered-llrbtree-ref (:string-table: string (procedure () *) -> *))
 (define string-ordered-llrbtree-ref string-table-ref)

 (: string-ordered-llrbtree-fold ((procedure (string * *) *) * :string-table: -> *))
 (define (string-ordered-llrbtree-fold proc init tree)
   (string-table-fold tree proc init))

 (: string-ordered-llrbtree-for-each ((procedure (string *) *) :string-table: -> :string-table:))
 (define (string-ordered-llrbtree-for-each proc tree)
   (string-table-for-each tree proc)
   tree)

 (: string-llrbtree/min (:string-table: (procedure () string *) --> string *))
 (define string-llrbtree/min string-table-min)

 )
