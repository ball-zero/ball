;; (C) 2010, Jörg F. Wittenberger

;; chicken module for the Askemos ball operation control module.

(declare
 (unit trstcntl)
 ;;
 (usual-integrations))

(module
 trstcntl
 (
  tc-genkey
  tc-private-cert-req-key-file x509-text tc-request-text
  tc-certification-for ssl-directory
  tc-store-host-cert! tc-store-host-key!
  tc-ca-key-file tc-ca-cert-file tc-private-cert-file tc-private-key-file tc-private-cert-req-file
  tc-private-cert
  tc-private-subject
  tc-private-end-date $tc-cert-processing-days
  tc-store-cert-for! tc-cert-file-for
  tc-tofu-ssl-init! $tc-tofu
  ;;
  reload-virtual-hosts!
  virtual-host-default-setup!
  virtual-host-add! virtual-host-delete!
  virtual-root-support-add! virtual-root-support-remove!
  virtual-host-for-each
  virtual-host-sync-to-dns!
  init-sqlite3-access-log
  ;;
  use-tan! check-tan!
  store-tans! host-send-tans!
  ;; internal
  tc-make-ca  x509-sign
  tc-certificate-request tc-request-client-key
  tc-store-ca-cert! tc-store-host-cert-req! tc-store-host-cert-req-key!
  tc-make-certificate-request
  x509-subject-hash
  )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 srfi-13 (prefix srfi-13 srfi:)
	srfi-19 srfi-34 srfi-35 srfi-45
	(except posix file-close create-pipe duplicate-fileno process-wait process-signal)
	files ports extras
	irregex openssl shrdprmtr timeout atomic parallel util
	mesh-cert notation aggregate place-common pool corexml
	protocol-connpool protocol-clntclls http-client smtp dispatch)
(import (only data-structures string-split))
(import tree)
(import (only protocol-common register-agreement-handler!))
(import (prefix dns dns-))
(import sqlite3 function storage-sql)		; tan

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../policy/trstcrcl.scm")
(include "../policy/tofu.scm")
(include "../policy/tan.scm")

(import srfi-49 cache protocol-common)

(include "../policy/vhost.scm")

)

(import (prefix trstcntl m:))


(define tc-private-cert-req-key-file m:tc-private-cert-req-key-file)
(define x509-text m:x509-text)
(define tc-request-text m:tc-request-text)
(define tc-certification-for m:tc-certification-for)
(define ssl-directory m:ssl-directory)
(define tc-store-host-cert! m:tc-store-host-cert!)
(define tc-store-host-key! m:tc-store-host-key!)
(define tc-ca-cert-file m:tc-ca-cert-file)
(define tc-private-cert-file m:tc-private-cert-file)
(define tc-private-key-file m:tc-private-key-file)
(define tc-private-subject m:tc-private-subject)
(define tc-private-cert m:tc-private-cert)
(define tc-private-end-date m:tc-private-end-date)
(define $tc-cert-processing-days m:$tc-cert-processing-days)

(define tc-store-cert-for! m:tc-store-cert-for!)
(define tc-cert-file-for m:tc-cert-file-for)

(define reload-virtual-hosts! m:reload-virtual-hosts!)
(define virtual-host-default-setup! m:virtual-host-default-setup!)
(define virtual-host-add! m:virtual-host-add!)
(define virtual-host-delete! m:virtual-host-delete!)
(define virtual-root-support-add! m:virtual-root-support-add!)
(define virtual-root-support-remove! m:virtual-root-support-remove!)
(define virtual-host-for-each m:virtual-host-for-each)
(define init-sqlite3-access-log m:init-sqlite3-access-log)

(define use-tan! m:use-tan!)
(define check-tan! m:check-tan!)
