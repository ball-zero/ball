;; (C) 2002, Jörg F. Wittenberger

(declare
 (unit notation-charenc)
 ;;
 (disable-interrupts)			; loop check interrupts
 (usual-integrations)
 (fixnum)
 (foreign-declare #<<EOF
#include <errno.h>
#include <string.h>
#include <iconv.h>

static size_t ac_iconv_sn, ac_iconv_dn;

EOF
)
)

(module
 notation-charenc
 (
  ;; xml-tree
  normalise-data
  ;;
  ;; These are for performance testing.  I want to figure, which
  ;; implementation is the best one over all.
  call-write-bytes
  ;; iconv itself
  iconv-open iconv-close iconv-bytes
  )

(import (except scheme vector-fill! vector->list list->vector)
	(except chicken add1 sub1 condition? promise? with-exception-handler)
	foreign
	srfi-1 srfi-13 srfi-34 srfi-35
	util ports regex)
(import (except atomic chicken-check-interrupts! chicken-check-interrupts/rarely!)
	(prefix atomic atomic:))


(include "typedefs.scm")

(define-inline (chicken-check-interrupts!) (atomic:chicken-check-interrupts/rarely!))


(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define any-but-eol-regex
  (lambda (str off)
    (let ((r (string-index str #\return off (string-length str))))
      (if r
	  (cond
	   ((eqv? r off)
	    (values #f #f))
	   ((and  (fx> r off)
		  (eqv? (string-ref str (sub1 r)) #\newline))
	    (if (eqv? r (add1 off))
		(values #f #f)
		(values off (sub1 r))))
	   (else (values off r)))
	  (values off (string-length str))))))

(define end-of-line-regex
  (let ((match (regexp "^\r?\n")))
    (lambda (str off)
      (let ((r (string-search-positions match str off)))
	(if r (values (caar r) (cadar r)) (values #f #f))))))

(define (normalized-length str str-len el)
  (let loop ((i 0) (l 0))
    (if (eqv? i str-len)
        l
        (receive (s e) (any-but-eol-regex str i)
		 (if s
		     (loop (min e str-len) (fx+ l (fx- (min e str-len) s)))
		     (receive (s e) (end-of-line-regex str i)
			      (loop (or e str-len) (fx+ el l))))))))

(define (normalize-input str result nl eol)
  (let loop ((i 0) (j 0))

    (chicken-check-interrupts!)

    (if (eqv? j nl)
        result
        (receive (s e) (end-of-line-regex str i)
		 (if s
		     (let lp ((j j) (p 0))
		       (if (eqv? p (string-length eol))
			   (loop e j)
			   (begin (string-set! result j (string-ref eol p))
				  (lp (add1 j) (add1 p)))))
		     (begin
		       (string-set! result j (string-ref str i))
		       (loop (add1 i) (add1 j))))))))

(define-macro (iconv-buflen) 256)

(define (call-write-bytes port str offset len)
  (if (and (eqv? offset 0) (eqv? (string-length str) len))
      (display str port)
      (do ((i 0 (add1 i))
	   (j offset (add1 j)))
	  ((eqv? i len) i)
	(display (string-ref str i) port))))

(: call-write-bytes* (output-port string fixnum fixnum -> . *))
(define-inline (call-write-bytes* port str offset len)
  (if (and (eqv? offset 0) (eqv? (string-length str) len))
      (display str port)
      (error "call-write-bytes* : unintented use")))

(define normalise-date-utf-8 '("UTF-8" "UTF8" "utf8" "utf-8"))

(define (normalise-data:is-utf8? x)
  (and (string? x) (member x normalise-date-utf-8)))

(define (normalise-data str to-encoding from-encoding eol-string)
  (##sys#check-string str)
  (let ((to-encoding (or to-encoding "UTF-8"))
        (from-encoding (or from-encoding "UTF-8")))
    (cond
     ((or (equal? to-encoding from-encoding)
	  (and (normalise-data:is-utf8? to-encoding)
	       (normalise-data:is-utf8? from-encoding)))
      (if eol-string
          (let ((nl (normalized-length str (string-length str)
                                       (string-length eol-string))))
            (if (eqv? nl (string-length str))
                str
                (normalize-input str (make-string/uninit nl) nl eol-string)))
          str))
     (else
      (let ((cnv (or (iconv-open to-encoding from-encoding)
                     (error "iconv-open ~a ~a" to-encoding from-encoding)))
            (buf (make-string/uninit (iconv-buflen))) ; internal buffer
            (o (open-output-string))
            (eol (or eol-string "\n")))
        ;;
        (let loop ((i 0)
                   (n (string-length str)))

	  (chicken-check-interrupts!)

          (if (= n 0)
              (begin
                (iconv-close cnv)
                (close-output-port o))
	      (receive
	       (nx sn dn) (iconv-bytes cnv str i n buf 0 (iconv-buflen))
	       (let* ((bl (- (iconv-buflen) dn))
		      (nl (normalized-length buf bl (string-length eol))))
		 ;; Beware: the conversion works only for the
		 ;; special case of XML, where we know the
		 ;; canonical line delimiter is #\newline at most
		 ;; as long as the one we find in the input stream.
		 (cond
		  ;; BUG this code doesn't even deal with
		  ;; #\return -only line ends.  If that becomes
		  ;; an issue, remove the first case.
		  ((eqv? bl nl) (call-write-bytes* o buf 0 nl))
		  ;; Instead of save memory: save calls.  Allocat a fresh string
		  ;; always and display in one go.
		  #;((<= nl bl)
		   (call-write-bytes*
		    o (normalize-input buf buf nl eol) 0 nl))
		  (else
		   (call-write-bytes*
		    o (normalize-input buf (make-string/uninit nl) nl eol)
		    0 nl)))
		 (loop (+ i (- n sn)) sn))))))))))

(define-foreign-type <iconv-desc> (c-pointer void))

(define-condition-type &iconv-error &message iconv-error?)

(define iconv-open
  (foreign-lambda* <iconv-desc> ((c-string to) (c-string from))
   "iconv_t i = iconv_open(to, from);
    C_return(i == (iconv_t) -1 ? NULL : i);"))

(define iconv-close
  (foreign-lambda void "iconv_close" <iconv-desc>))

(define (iconv-bytes t s soffset slen d doffset dlen)
  (let ((n ((foreign-lambda*
	     int
	     ((<iconv-desc> t)
	      (scheme-object s) (int soffset) (int sn)
	      (scheme-object d) (int doffset) (int dn))
	     "size_t rc;"
	     "ac_iconv_sn=sn;ac_iconv_dn=dn;"
	     "char *sp=C_c_string(s)+soffset, *dp=C_c_string(d)+doffset;"
	     "rc=iconv( t, &sp, &ac_iconv_sn, &dp, &ac_iconv_dn );"
	     "C_return(rc);")
	    t s soffset slen d doffset dlen)))
    (if (and (negative? n)
	     ((foreign-lambda*
	       bool () "C_return((errno != E2BIG) && (errno != EINVAL));")))
	(raise
	 (make-condition
	  &iconv-error
	  'message
	  (string-append
	   "iconv error: "
	   ((foreign-lambda* c-string () "C_return(strerror(errno));")))))
	(values n
		(foreign-value "ac_iconv_sn" unsigned-integer)
		(foreign-value "ac_iconv_dn" unsigned-integer)))))
)
