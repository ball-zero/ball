;; (C) 2002, J�rg F. Wittenberger

;; chicken module clause for the Askemos notation module.

(declare
 (unit notation-common)
 ;;
 (disable-interrupts)			; loops in tree only.
 (usual-integrations)
 (fixnum)
)

(module
 notation-common
 (
  xml-space
  xml-parse-register-xmlns empty-namespace-declaration
  )

(import (except scheme vector-fill! vector->list list->vector)
	(except chicken add1 sub1 condition?)
	srfi-1 srfi-13 srfi-34 srfi-35
	util tree)

(define xml-space
  (let ((preserve-str "preserve"))
    (lambda (attributes default)
      (let ((att (let loop ((atts attributes))
		   (and (pair? atts)
			(or (and (eq? (xml-attribute-name (car atts)) 'space)
				 (eq? (xml-attribute-ns (car atts)) namespace-xml)
				 (car atts))
			    (loop (cdr atts)))))))
	;; don't use just "string=?" - in chicken it will miss the (most common) a=b case
	(if att (equal? (xml-attribute-value att) preserve-str) default)))))

(include "../mechanism/notation/xml/common.scm")
)
