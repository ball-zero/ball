;; (C) 2010 Jörg F. Wittenberger


(declare
 (unit place-common)
 ;;
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)

 (disable-interrupts)			; all loops elsewhere

 )

(module
 place-common
 (
  oid?
  string->oid oid->string
  oid+quorum->string oid-parse
  intern-oid
  ;;
  make-message message-clone
  ;;
  current-place current-message
  ;;
  deed-slots
  interaction-slots
  internal-slots
  message-slots
  known-slots ;; procedure(!)
  stored-slots
  transient-slots
  transient-meta-slots
  serializable-transient-meta-slots
  optional-transient-meta-slots
  optional-transient-reply-meta-slots
  ;;
  message-body/plain message-body/plain-size
  message-body
  ;;
  dsssl-message-body
  dsssl-message-body/plain
  dsssl-message-body/plain-size
  ;;
  aggregate-body/plain aggregate-body
  place/make-block-table
  ;; special/error message constructors
  accept-header-matches-application/soap+xml
  condition->message
  make-html-error
  make-soap-error
  ;; intern
  askemos:dc-date
  ;; totally intern
  slotmap?
  ;;
  null-oid
  one-oid
  my-oid
  public-oid
  public-not-initialised
  set-my-place! set-public-place! ;; not well named
  ;;
  secret-encode secret-verify
  ;;
  host-lookup* host-lookup
  ;;
  make-quorum quorum?
  quorum-size quorum-local? quorum-others quorum-xml
  respond-treshold
  ;;
  ;; intern
  ;;
  replicates-of replicates-of/xml set-local-quorum! *local-quorum*
  ;;
  aggregate-fetch-blob set-aggregate-fetch-blob!
  &blob-missing-condition missing-blob-condition? missing-blob-source
  bhob->string bhob->string/anyway
  body->string ;; deprecated
  body-size
  xml-digest-2-simple
  message-body*
  ;;
  set-bhob->string:re-check-bhob!
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise? vector-copy!)
	srfi-34 srfi-35 srfi-45 extras)

(import srfi-1 srfi-13 (prefix srfi-13 srfi:) (only srfi-18 make-mutex) srfi-19
	lalr openssl
	shrdprmtr util protection
	atomic timeout tree notation function bhob aggregate channel storage-api)

(import (only ports call-with-input-string call-with-output-string)
	(prefix srfi-43 srfi:))

(import (only parallel delay*))

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(cond-expand
 (chicken
;;  (: current-place (#!rest (* #!rest -> *) -> (* #!rest -> *)))
;;  (: current-message (#!rest (symbol -> *) -> (symbol -> *)))

  (: oid->string (:oid: --> string))
  (: oid+quorum->string (:oid: #!rest (struct <quorum>) --> string))
  (: string->oid (string --> (or false :oid:)))
  (: oid-parse (string --> (or false :oid:) (or false (struct <quorum>))))

  (: make-message (#!rest --> (struct <frame>)))

  (: make-quorum ((or (struct <xml-element>) (list-of (or :oid: string))) --> (struct <quorum>)))
  (: quorum? (* --> boolean : (struct <quorum>)))
  (: quorum-size ((struct <quorum>) --> fixnum))
  (: quorum-local? ((struct <quorum>) --> boolean))
  (: quorum-others ((struct <quorum>) --> (list-of (or :oid: string))))
  (: quorum-xml ((struct <quorum>) --> (struct <xml-element>)))

  (: host-lookup (#!rest ((or :oid: string) #!rest symbol -> (or false :ip-addr:))
			 -> ((or :oid: string) #!rest symbol -> (or false :ip-addr:))))

  (: aggregate-fetch-blob
     ((struct <quorum>) :oid: :oid: (struct <blob>) -> (or boolean string)))

  (: body->string (* -> string))
  (: body-size (* -> fixnum))

  (: replicates-of (:place: --> (struct <quorum>)))

  (: message-body* ((struct <frame>) :oid: -> *))
  (: message-body (procedure ((or :place: procedure (struct <frame>))) *))

  (: message-body/plain ((struct <frame>) -> (or boolean (struct <blob>) string list)))
  (: message-body/plain-size ((struct <frame>) -> fixnum))

  (: my-oid (--> (or boolean :oid:)))
  (: public-oid (--> (or boolean :oid:)))


  ) (else))

(include "../mechanism/place-common.scm")

(define-record-printer (<quorum> x out)
  (format out "(quorum ~a ~a)" (if (quorum-local? x) "local and" "remote on") (quorum-others x)))

)
(import (prefix place-common m:))
(define oid? m:oid?)
(define oid->string m:oid->string)
(define string->oid m:string->oid)
(define oid+quorum->string m:oid+quorum->string)
(define oid-parse m:oid-parse)
(define public-oid m:public-oid)
(define my-oid m:my-oid)
(define one-oid m:one-oid)
(define null-oid m:null-oid)

(define set-my-place! m:set-my-place!)
(define set-public-place! m:set-public-place!)
(define set-local-quorum! m:set-local-quorum!)

(define make-quorum m:make-quorum)
(define quorum? m:quorum?)
(define quorum-size m:quorum-size)
(define quorum-local? m:quorum-local?)
(define quorum-others m:quorum-others)
(define quorum-xml m:quorum-xml)
(define respond-treshold m:respond-treshold)

(define xml-digest-2-simple m:xml-digest-2-simple)

(define make-message m:make-message)
(define message-clone m:message-clone)

(define message-body m:message-body)
(define message-body* m:message-body*)
(define message-body/plain m:message-body/plain)
(define message-body/plain-size m:message-body/plain-size)
(define dsssl-body-size m:body-size)
;;
(define dsssl-message-body m:dsssl-message-body)
(define dsssl-message-body/plain m:dsssl-message-body/plain)
;;
(define secret-encode m:secret-encode)
(define secret-verify m:secret-verify)
;; testing
(define place/make-block-table m:place/make-block-table)
