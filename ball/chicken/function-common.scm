;; (C) 2002, 2003, 2008 J�rg F. Wittenberger

;; chicken module clause for the Askemos function module.

(declare
 (unit function-common)
 ;; promises
 (disable-interrupts)			;; loops check interrupts
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (fixnum)
 (strict-types)
 (usual-integrations))

(module
 function-common
 (
  ;; only to be initialized for recursion
  set-function-fetch-other!
  set-function-mind-default-lookup!
  xsl-parameters
  bind-xsl-value fetch-xsl-variable
  fetch-xsl-variable/string fetch-xsl-variable/symbol
  ;; For flexibility and uniformity, we'll switch to export this
  ;; parameter to ease reuse of the code.
  *function-fetch-other*
  ;;
  form-field
  register-tag-transformer! *namespace-environment*
  ;;
  set-function-upcalls!
  function:wt-marshall
  ;;
  ;; These should not be reexported from function!
  function-fetch-other
  bind-xsl-values
  xsl-envt-union
  bind-xsl-value/string
  function-mind-default-lookup
  xsl-environment-variables
  make-xsl-variables
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 vector-copy! with-exception-handler condition? promise?)
        shrdprmtr
	parallel cache (except srfi-18 raise) srfi-34 srfi-35 srfi-45
	extras data-structures)

(import srfi-1 srfi-4 srfi-13 srfi-19 srfi-43 srfi-69
	matchable ports memoize environments
	protection util tree xpath wttree)

(import (prefix wttree wt:)
	(prefix srfi-1 srfi:)
	(prefix srfi-13 srfi:)
	(prefix srfi-43 srfi:))

(import aggregate-chicken)		; string-llrb-tree

(include "typedefs.scm")

(include "../mechanism/function/xslt-tl.scm")

(define function-default-lookup (lambda args (error "function-default-lookup not connected")))
(define function-my-oid (lambda args (error "function-my-oid not connected")))
(define function-right->string (lambda args (error "function-right->string not connected")))

(define (set-function-upcalls! lookup my-oid right->string)
  (set! function-right->string right->string)
  (set! function-my-oid my-oid)
  (set! function-default-lookup lookup))

(define function:wt-marshall
  ;; BUG: repeated here literally
  (let ((is-core-namespace?
	 (let ((corenamespaces `(,namespace-mind ,namespace-mind-v0 #f mind)))
	   (define (is-core-namespace? x)
	     (and (xml-element? x) (memq (ns x) corenamespaces)))
	   is-core-namespace?))
	(value-attr-list (list (make-xml-attribute 'name #f "value")))
	(left-attr-list (list (make-xml-attribute 'name #f "left")))
	(right-attr-list (list (make-xml-attribute 'name #f "right"))))
    (lambda (me msg key-reader key-writer . args)
      (wt:wt-marshall
       (let ((cache (make-cache
		     (me 'get 'id)
		     eq?
		     #f ;; state
		     ;; miss:
		     (lambda (cs k) #t)
		     ;; hit
		     #f ;; (lambda (c es) #t)
		     ;; fulfil
		     #f ;; (lambda (c es results))
		     ;; valid?
		     (lambda (es) #t)
		     ;; delete
		     #f ;; (lambda (cs es) #f)
		     )))
	 (lambda (%make-node oid)
	   (let ((oid (if (string? oid) (string->symbol oid) oid)))
	     (cache-ref
	      cache oid
	      (lambda ()
		(let* ((n (function-fetch-other me oid adopt: #t))
		       (m (me oid 'metainfo-adopt))
		       (l ((sxpath '(Description links Bag li)) m)))
		  (%make-node
		   (key-reader (data ((sxpath '(key)) n)))
		   (string->symbol
		    (attribute-string
		     'href
		     (node-list-first
		      (node-list-filter
		       (lambda (a) (equal? (attribute-string 'resource a) "value"))
		       l))))
		   (let ((a (attribute-string
			     'href
			     (node-list-first
			      (node-list-filter
			       (lambda (a) (equal? (attribute-string 'resource a) "left"))
			       l)))))
		     (if a (string->symbol a) 'empty))
		   (let ((a (attribute-string
			     'href
			     (node-list-first
			      (node-list-filter
			       (lambda (a) (equal? (attribute-string 'resource a) "right"))
			       l)))))
		     (if a (string->symbol a) 'empty))
		   (string->number (data ((sxpath '(weight)) n))))))))))
       (let ((act  (list
		    (let* ((kw (memq action: args))
			   (v (if kw (cadr kw)
				  (function-default-lookup me msg "public"))))
		      (make-xml-attribute
		       'action #f (if (symbol? v) (symbol->string v) (data v)))))))
	 (lambda (k v l r w)
	   (make-xml-element
	    'new namespace-mind
	    (cons (make-xml-attribute
		   'protection #f
		   (function-right->string (append (me 'protection)
						   (list (me 'get 'id) (function-my-oid)))))
		  act)
	    (let ((left (if (node-list-empty? l) #f
			    (make-xml-element 'link namespace-mind left-attr-list l)))
		  (right (if (node-list-empty? r) #f
			     (make-xml-element 'link namespace-mind right-attr-list r)))
		  (val (make-xml-element
			'link namespace-mind value-attr-list
			(cond
			 ((symbol? v)
			  (make-xml-element
			   'id namespace-mind '() (symbol->string v)))
			 ((let ((v (document-element v)))
			    (and (is-core-namespace? v) (memq (gi v) '(id ref new)) v))
			  => identity)
			 (else (make-xml-element
				'new namespace-mind
				(cons 
				 (make-xml-attribute
				  'protection #f (function-right->string (me 'protection)))
				 act)
				v)))))
		  (dta (sxml `(entry (key , (key-writer k)) (weight ,(literal w))))))
	      (cond
	       ((and left right) (list left right val dta))
	       (left (list left val dta))
	       (right (list right val dta))
	       (else (list val dta))))))))
      )))

)

(import (prefix function-common m:))

(define form-field m:form-field)

(define wt-marshall m:function:wt-marshall)
(define set-function-upcalls! m:set-function-upcalls!)

(import wttree)

(define wttree-forms
  '(
    make-wt-tree-type
    string-wt-type number-wt-type
    make-wt-tree
    singleton-wt-tree
    alist->wt-tree
    wt-tree/empty?
    wt-tree/size
    wt-tree/add
    wt-tree/delete
    wt-tree/member?
    wt-tree/lookup
    wt-tree/split<
    wt-tree/split>
    wt-tree/union
    wt-tree/intersection
    wt-tree/difference
    wt-tree/subset?
    wt-tree/set-equal?
    wt-tree/fold
    wt-tree/for-each
    wt-tree/index
    wt-tree/index-datum
    wt-tree/index-pair
    wt-tree/rank
    wt-tree/max
    wt-tree/min
    wt-tree/min-datum
    wt-tree/min-pair
    wt-tree/delete-min

    string-wt-type
    number-wt-type
    wt-marshall

    wt-tree/dump
    wt-tree/restore
    ))
