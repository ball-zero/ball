;; (C) 2002, J�rg F. Wittenberger

;; chicken module clause for the Askemos 'nunu' (wiki) implementation
;; module .

(declare
 (unit nu)
 ;;
 (usual-integrations))

(module
 nu
 (
  nunubacklinks nunulinks nunu-fetch
  nunu-action-get
  nunu-action-write nunu-change-draft nunu-lock nunu-unlock
  literal-member
  )

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 srfi-13 (prefix srfi-13 srfi:)
	(except srfi-18 raise)
 srfi-34 srfi-35 srfi-45
 srfi-49 srfi-69
 extras data-structures
	posix files ports sqlite3
	pcre lalr parallel cache util timeout protection
)
(import tree notation function bhob aggregate storage-api place-common pool place-ro)
(import place corexml protocol dispatch storage step openssl wiki-serial)

(include "typedefs.scm")

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-inline (make-a-transient-copy-of-object x) x)
(include "../policy/nu.scm")
)

(import (prefix nu m:))

(define nunubacklinks m:nunubacklinks)
(define nunulinks m:nunulinks)
(define nunu-fetch m:nunu-fetch)
(define nunu-action-get m:nunu-action-get)
(define nunu-action-write m:nunu-action-write)
(define nunu-change-draft m:nunu-change-draft)
(define nunu-lock m:nunu-lock)
(define nunu-unlock m:nunu-unlock)
(define literal-member m:literal-member)
