;; (C) 2011 J�rg F. Wittenberger

(declare
 (unit bhob)
 ;;
 (usual-integrations)

 (strict-types)
 )

(module
 bhob
 (
  a:make-blob a:blob? blob-sha256
  a:blob-size set-blob-size! blob-block-size blob-blocks-per-blob blob-blocks
  ;;
  bhob?
  $bhob-size $bhob-surrogate
  ;;
  print-blob-usage
  register-blob-ref! unregister-blob-ref! non-transient-blob?
  ;;
  make-block-table block-table?
  block-table-bs block-table-size
  load-block-table! block-table-for-each
  block-table-set!
  block-table-ref block-table-ref/default
  block-table-update! block-table-truncate! block-table-flush!
  block-table-resolve/default
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	(except srfi-18 raise)
	srfi-34 srfi-35 srfi-45)

(import (only openssl sha256-digest))
(import (only lolevel move-memory!))

(import srfi-1 srfi-13 (prefix srfi-43 srfi:)
	shrdprmtr util timeout atomic structures parallel protection
	ports extras data-structures tree notation)

(include "typedefs.scm")

(include "../mechanism/bhob.scm")
(: block-table-set! (:block-table: fixnum string #!rest -> *))
(include "../mechanism/block.scm")

)

(import (prefix bhob m:))

(define bhob? m:bhob?)
(define $bhob-size m:$bhob-size)
(define $bhob-surrogate m:$bhob-surrogate)
(define print-blob-usage m:print-blob-usage)

(define make-block-table m:make-block-table)
(define block-table? m:block-table?)
(define load-block-table! m:load-block-table!)
(define block-table-set! m:block-table-set!)
(define block-table-ref m:block-table-ref)
(define block-table-ref/default m:block-table-ref/default)
(define block-table-resolve/default m:block-table-resolve/default)
(define block-table-update! m:block-table-update!)
(define block-table-truncate! m:block-table-truncate!)
(define block-table-flush! m:block-table-flush!)
