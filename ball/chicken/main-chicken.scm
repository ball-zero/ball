(declare
 (unit main-chicken))

(module
 main-chicken
 *
 (import srfi-1 srfi-13 srfi-34 srfi-35 srfi-43 shrdprmtr)
 (import (except scheme force delay vector-fill! vector->list list->vector)
	 (except chicken add1 sub1 with-exception-handler condition? promise? vector-copy!))
 (import (except srfi-18 raise))
 (import extras files ports)
 (import (except posix file-close duplicate-fileno create-pipe process-wait process-signal socket?))
 (import pthreads shrdprmtr srfi-19 srfi-45 srfi-49 srfi-69 srfi-110
	 matchable structures util atomic sslsocket timeout parallel cache)
 (import (only place-ro default-public-meta-info metainfo-request-element))
 (import protection tree notation function sqlite3 bhob aggregate
	 storage-api aggregate place-common pool place methods corexml protocol dispatch
	 hostmesh
	 storage-common storage storage-remote storage-sql step)
 (import mesh-cert trstcntl cntrl nu cgi xmlrpc markdown userenvt)
 (import ask-repl-server)

 (import protocol-clntclls)

 (import notation-xmlrender)

 (import (only storage-remote $check-replicate-reply))

 (import foreign)



 (include "typedefs.scm")

 (define (underlying-pstore x) x)
 (define root-object underlying-pstore)

 (define-syntax define-macro
   (syntax-rules ()
     ((_ (name . llist) body ...)
      (define-syntax name
	(lambda (x r c)
	  (apply (lambda llist body ...) (cdr x)))))
     ((_ name . body)
      (define-syntax name
	(lambda (x r c) (cdr x))))))

#>
static int
filterfunc(C_word x, C_word arg)
{
  if(C_header_bits(x) == C_STRUCTURE_TYPE && C_block_item(x, 0) == arg) C_return(1);
  else C_return(0);    
}

extern C_regparm C_word C_fcall C_show_trace(C_word n);
<#

(define filter:type
  (foreign-value "filterfunc" c-pointer))

(define (heap-objects type . n)
  (let* ((vec (make-vector (or (and (pair? n) (car n)) 400)))
	 (n (##sys#filter-heap-objects
	     filter:type vec type)))
    ;; "n" is -1 if vector is too small
    (and (> n -1) (vector-copy vec 0 n))))

#;(define (show-trace! f) (##core#inline "C_show_trace" f))

(define-foreign-type buffer-handler
 (function unsigned-integer
           (c-pointer unsigned-integer c-pointer unsigned-integer
            c-pointer)))

;; volatile? is a fake only.  It would give #t if the storage adaptor
;; cannot store a certain object, e.g., procedures.

(define (volatile? obj) #f)

(let* ((killall
	(lambda ()
	  (running-processes-fold
	   (lambda (e i) (process-signal e i) i)
	   signal/term)
	  (thread-sleep! 0.3)
	  (running-processes-fold
	   (lambda (e i)
	     (process-signal e i) i)
	   signal/kill)))
       (proc (lambda (num)
	       (unless (= num 0) (logerr "Signal ~a exit in ~a\n" num (current-process-id)))
	       (exit 1))))
  (for-each (lambda (signum) (set-signal-handler! signum proc))
            (list signal/term signal/int signal/quit))
  (on-exit killall))

#;(let ((t #t))
  (set-signal-handler!
   signal/hup
   (lambda (num) (show-trace! t) (set! t (not t)))))

;; TODO fix the interrupt register scheme.
(define-syntax register-interrupt-handler!
  (syntax-rules () ((_ a b) #f)))

(include "../mechanism/main.scm")

;; GUI support (to be moved elsewhere)

(define (gui-network-status)
  `(netstat
    (hosts . ,(node-list-map
	       (lambda (n) (node-list-map (lambda (n) (data n)) (children n)))
	       (children (http-display-hosts))))
    (channels ,(display-http-channels))))

(define (gui-i/o-state)
  (begin
    (define (f x i) (format #t "~a\n" x) i)
    (f (literal
	(current-date)) #f)
    (open-fds-fold f #f)
    (running-processes-fold  f #f)
    #;(ssl-connections-fold (lambda (k v i) (format #t "~a\n" v)) #f)
    #t))

(define (gui-x509-certificates)
  (display (x509-text (car (map data (ball-info '(#f "cert")))))))

(define (sixup-command expr)
  (case (car expr)
    ((network-status) (gui-network-status))
    ((threadlist) (thread-list 'dummy))
    ((i/o-state) (gui-i/o-state))
    ((x509-certificates) (gui-x509-certificates))
    (else (error "unknown command" expr))))

(define (sixup-command? expr)
  (and-let*
   (((pair? expr))
    ((eq? (car expr) 'bui))
    ((pair? (cdr expr))))
   ;; Decode quoted commands.  Just to make it work with both the workarounds...
   (let ((v (cdr expr)))
     (if (and (pair? (car v)) (eq? (caar v) 'quote))
	 (cons (cadar v) (cdr v))
	 v))))

(define *proposal-debug-file* (make-oid-table))

(define (register-proposal-debug-file on name)
  (if (oid? on)
      (if (string? name)
          (hash-table-set! *proposal-debug-file* on name)
          (hash-table-delete! *proposal-debug-file* on))))

(define (debug-proposal-to-file on this request value)
  (and-let* ((name (hash-table-ref/default 
                    *proposal-debug-file* on #f)))
            (if (file-exists? name) (remove-file name))
            (call-with-output-file (debug 'Writing name)
              (lambda (p)
                (format p "~s ~a\n" value
                        (cond ((frame? value) (message-digest value))
                              ((node-list? value) (xml-digest-2-simple value))
                              (else "not agreeable")))
                (cond
                 ((node-list? value) (display (dsssl-xml-format value mode: 'exc-cn14-root) p))
                 ((condition? value)
                  (receive (title msg stack dummy) (condition->fields value)
                           (format p "~a ~a ~a\n" title msg stack)))
                 (else (format p "~s" value)))
                (format p "Request: ~a\n" (body->string (dsssl-message-body/plain request)))))))

(define (debug-proposal on . name)
  (let ((name (if (pair? name) (car name) (format "/tmp/ball-debug-~a" on))))
    (if (not ($broadcast-debug-proposal))
        ($broadcast-debug-proposal (lambda args (apply debug-proposal-to-file args))))
    (register-proposal-debug-file on name)))

(define (ppp o)				; Print Persistant state Please
  (print (aggregate-meta (find-local-frame-by-id o 'ppp))))

(define (pb o)				; Print Body
  (display (or
	    (and-let* ((b (fget (document o) 'mind-body)))
		      (body->string b))
	    (and-let* ((b (fget (document o) 'body/parsed-xml)))
		      (xml-format b)))))
(define (pds o)				; Print Digest of Signature
  (define frame (find-local-frame-by-id o 'pds))
  (display (fget frame 'version))
  (newline)
  (format #t "OID-Consistent: ~a\n" (frame-oid-match? (aggregate-meta frame) o))
  (xml-digest-simple (frame-signature frame)))

(define (pm o)				; print Public Metainfo
  (display (xml-format (public-meta-info (find-local-frame-by-id o 'pm)))))

(define (pdm o)				; print Public Digest Metainfo
  (xml-digest-simple (public-meta-info (find-local-frame-by-id o 'pdm))))

(define (pmdf f) (default-public-meta-info f))

(define (pq o)				; Print Quorum
  (let ((oo (let ((x (find-local-frame-by-id o 'pq)))
	      (if x (replicates-of x) ((quorum-lookup) o)))))
    (if oo
	(format #t "~a: local ~a ~a\n" o (quorum-local? oo) (quorum-others oo))
	(format #t "~a: nowhere\n" o))
    (values)))

(define (ps o)				; Print Signature
  (define frame (or (find-local-frame-by-id o 'ps) (error "not found" o)))
  (display (xml-format (frame-signature frame))))

#;(define (frame-commit! oid)
  (let ((frame (document oid)))
    (commit-frame! #f frame (aggregate-meta frame) '())))

(define (resync from oid)		; manual (re)sync from singleton quorum
  (or (and-let* ((obj (guard (exception (else #f))
			     (find-local-frame-by-id oid 'resync)))
		 (action (fget obj 'mind-action-document))
		 ((public-equivalent? action)))
		obj)
      (with-mind-modification
       (lambda (false)
	 (spool-db-exec/prepared "delete from missing where oid = ?1" (oid->string oid))
	 (with-mutex
	  (respond!-mutex oid)
	  (and-let* ((frame
		      ((resynchronize) (make-quorum (list from)) oid)))
		    (set-missing-oid! oid)
		    (commit-frame! #f frame (aggregate-meta frame) '())
		    (delete-is-sync-flag! oid)
		    frame)))
       #f)))

;; 2012-04-18: experimental.
(define (backsync from oid) (parameterize (($relaxed-replication 1)) (resync from oid)))

(define (blv arg . c)
  (let ((b (cond
	    ((a:blob? arg) arg)
	    ((string? arg) (a:make-blob (string->symbol arg) #f #f #f #f))
	    ((symbol? arg) (a:make-blob arg #f #f #f #f)))))
    (or (fetch-blob-notation *the-registered-stores* b #f)
	(and (pair? c)
	     (let* ((src (if (aggregate? (car c)) (car c) (find-local-frame-by-id (car c) 'blv)))
		    (oid (aggregate-entity src)))
	       (aggregate-fetch-blob (replicates-of src) oid oid b)
	       (fetch-blob-notation *the-registered-stores* b #f))))))

(define (get-blob context arg . from)
  (let* ((b (cond
	     ((a:blob? arg) arg)
	     ((string? arg) (a:make-blob (string->symbol arg) #f #f #f #f))
	     ((symbol? arg) (a:make-blob arg #f #f #f #f))))
	 (src (if (aggregate? context) context
		  (find-local-frame-by-id context 'get-blob)))
	 (oid (aggregate-entity src))
	 (quorum (if (pair? from)
		     (let ((from1 (car from)))
		       (cond
			((quorum? from1) from1)
			((pair? from1) (make-quorum from1))
			(else (make-quorum from))))
		     (replicates-of src))))
    (aggregate-fetch-blob quorum oid oid b)
    (and (fetch-blob-notation *the-registered-stores* b #f) #t)))

(define (gbf context arg . from)
  (and-let* ((b (cond
		 ((a:blob? arg) arg)
		 ((string? arg) (a:make-blob (string->symbol arg) #f #f #f #f))
		 ((symbol? arg) (a:make-blob arg #f #f #f #f))))
	     (r (http-get-blob context (blob-sha256 b) (host-lookup* (car from)))))
	    (message-body/plain r)))

(define (debug-check-replicate-reply1 checksum frame rdf)
  (let* ((expected
	  (cond
	   ((quorum-local? (replicates-of frame))
	    (frame-signature frame))
	   ((and-let* ((ad (fget frame 'mind-action-document))
		       ((or (eq? (aggregate-entity frame) ad)
			    (if (find-local-frame-by-id ad 'http-check-replicate-reply)
				(public-equivalent? ad)
				(and (memq ad (quorum-others (replicates-of frame)))
				     (find-frame-by-id-on ad (replicates-of frame))
				     (public-equivalent? ad)))))
		       (p (fget frame 'protection)))
		      ((make-service-level p (public-capabilities))
		       (my-oid)))
	    (frame-signature frame))
	   (else rdf)))
	 (received (xml-digest-simple expected)))
    (if (and checksum (not (equal? checksum received)))
	(logerr "~a Expected ~a\n~a\nGot ~a\n~a\n" (aggregate-entity frame) checksum (xml-format expected) received (xml-format rdf)))
    (values expected received)))

(define (debug-check-replicate-reply checksum frame rdf)
  (define (act expected cn a1 a2)
    (let ((received (xml-digest-simple expected)))

      #;(if (and checksum (not (equal? checksum received)))
	  (logerr "~a Case ~a Got ~a\n~a\nExpected ~a\n~a\n" (aggregate-entity frame) cn received (xml-format expected) checksum (xml-format rdf)))

      (if (and checksum (not (equal? checksum received)) a1)
	  (begin
	    ;; In case the protection value is not set, the first test fails.
	    ;;
	    (let* ((alt (a1 frame))
		   (altsum (xml-digest-simple alt)))
	      (if (equal? checksum altsum)
		  (begin
#|
		    ;; (logerr "~a replication ~a recovered by test test a1 ~a\n" cn (aggregate-entity frame) a1)
|#
		    (values alt altsum))
		  (let ((alt (xml-digest-simple (a2 frame))))
		    (if (equal? checksum alt)
			(begin
			  (logerr "~a replication ~a recovered by test of a2 ~a\n" cn (aggregate-entity frame) a2)
			  (values alt received))
			(begin
			  (logerr "~a Case ~a Got ~a\nExpected ~a\n~a\n" (aggregate-entity frame) cn received checksum (xml-format rdf))

			  (values expected received)))))))
	  (values expected received))))
  (cond
   ((quorum-local? (replicates-of frame))
    (act (frame-signature frame) "is local (1)"  frame-metadata default-public-meta-info))
   ((and-let* ((p (fget frame 'protection))
	       ((pair? p))
	       (ad (fget frame 'mind-action-document))
	       ((or (eq? (aggregate-entity frame) ad)
		    (eq? ad one-oid)
		    (and (memq ad (quorum-others (replicates-of frame)))
			 (public-equivalent? ad (replicates-of frame))))))
	      ((make-service-level p (public-capabilities))
	       (my-oid)))
    (act (frame-signature frame) "is public and readable (2)" frame-metadata default-public-meta-info))
   (else (act rdf "else case (3)" frame-signature frame-metadata))))

;; FIXME: Remove this completely once we know it is just in the way.
($check-replicate-reply debug-check-replicate-reply)

(define log-sync!
  (let ((default ($check-replicate-reply)))
    (lambda (id)
      (if id
	  (let ((current ($check-replicate-reply)))
	    ($check-replicate-reply
	     (lambda (c f r)
	       (if (eq? (aggregate-entity f) id)
		   (debug-check-replicate-reply c f r)
		   (current c f r)))))
	  ($check-replicate-reply default)))))

)

(import (prefix main-chicken m:))

;; To be removed:
(define run-cmd ##dev#run-cmd)
;; To be moved to replace `body->string`:
(define blob-value m:blv)

;; setup
(define install-initial-contract m:install-initial-contract)
(define install-x509-default m:install-x509-default)
(define license-file "COPYING.html")
(define (wake-up x) (m:wake-up x))
(define set-wake-up! m:set-wake-up!)
(define start-protection! m:start-protection!)
(define set-start-protection! m:set-start-protection!)
;; operation and parameters
(define set-satelite! m:set-satelite!)
(define $getput-oid m:$getput-oid)
;; GUI
(define sixup-command? m:sixup-command?)
(define sixup-command m:sixup-command)
(define (bui . cmd) (sixup-command cmd))
;; admin/debug
(define @ m:@)
(define init-trustedcode m:init-trustedcode)
(define ball-flush-caches! m:ball-flush-caches!)
(define $startup-verbose m:$startup-verbose)
(define resync m:resync)
(define ps m:ps)
(define pds m:pds)
(define pm m:pm)
(define pdm m:pdm)
(define pmdf m:pmdf)
(define pq m:pq)
(define pb m:pb)
(import (prefix notation n:))
(define xd n:xml-digest-simple)
;;
(define debug-proposal m:debug-proposal)
