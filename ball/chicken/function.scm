;; (C) 2002, 2003, 2008 J�rg F. Wittenberger

;; chicken module clause for the Askemos function module.

(declare
 (unit function)
 ;; promises
 (disable-interrupts)			;; loops check interrupts
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (fixnum)
 (strict-types)
 (usual-integrations))

(module
 function
 (
  ;; Bail/DSSSL
  dsssl-export!                        ; for initialization only!
  eval-sane eval-sane-string
  eval-thread-safe
  ;; only to be initialized for recursion
  set-function-fetch-other!
  set-function-mind-default-lookup!
  xsl-parameters
  bind-xsl-value fetch-xsl-variable
  fetch-xsl-variable/string fetch-xsl-variable/symbol
  ;; For flexibility and uniformity, we'll switch to export this
  ;; parameter to ease reuse of the code.
  *function-fetch-other*
  ;;
  make-xslt-transformer make-xslt-transformer*
  form-field
  ;;
  dsssl-init                           ; reinitialize, esp. for debuging
  ;;
  init-xsql
  xsql-user-database-file
  with-connection
  sql-exec-protected sql-write
  ;;
  set-function-upcalls!
  function:wt-marshall
  ;;
  set-function-metaview! set-function-metactrl!
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 vector-copy! with-exception-handler condition? promise?))

(import dsssl xsql)

(import function-common xslt)

)
