;; (C) 2002, 2003, 2008 J�rg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit rfc-2518)
 (usual-integrations)
 (disable-interrupts))

(module
 rfc-2518
 (
  dav-request-header
  dav-write-entity-headers
  )

 (import-for-syntax lalrgen)
 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:)
	 srfi-19 srfi-34 srfi-35
	 lalr tree notation pcre regex util
	 data-structures protocol-common)

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/http/rfc2518.scm")

)
