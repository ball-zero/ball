#;(import-for-syntax matchable)

(cond-expand
 (chicken-4
  (import-for-syntax (prefix chicken chicken:)))
 (else))

(: xthe-identity-inline (* --> *))
(define-inline (xthe-identity-inline x) x)
#;(define-syntax xthe
  (syntax-rules ()
    ((_ type val) (the type (xthe-identity-inline val)))))
(define-syntax xthe
  (syntax-rules ()
    ((_ type val) (the type val))))

;(define-syntax add1 (syntax-rules () ((_ x) (fx+ x 1))))
(: add1 (fixnum --> fixnum))
(define-inline (add1 x) (fx+ x 1))
(: sub1 (fixnum --> fixnum))
(define-inline (sub1 x) (fx- x 1))

(define-type :time: (struct <srfi:time>))
(define-type :date: (struct <srfi:date>))
(define-type :thunk: (procedure () . *))

(define-type :pathname: string)


(define-type :xml-attribute: (struct <xml-attribute>))
(define-type :xml-attribute-list: (list-of (struct <xml-attribute>)))
(define-type :xml-ns-decl: (struct <xml-attribute>))
(define-type :xml-namespace: (struct <xml-namespace>))
(define-type :xml-element: (struct <xml-element>))
(define-type :xml-literal: string)
(define-type :xml-pi: (vector symbol string string))
(define-type :xml-comment: (vector symbol *))

(define-type :xml-node: (or :xml-element: :xml-literal: :xml-ns-decl: :xml-pi: :xml-comment:))
(define-type :xml-nodelist: (or :xml-node: (list-of :xml-node:)))
(define-type :xml-document: (struct <xml-document>))

(define-type :xml-ns-env: (list-of :xml-namespace:))
(define-type :xml-ns-rev-env: (list-of :xml-namespace:))

(define-type :deprecated-wt-type: (struct <wt-tree>))

(define-type :xpath-variable-environment: :deprecated-wt-type:)

(define-type :xsl-parameter-environment: (list-of (pair symbol *)))
(define-type :xsl-envt: (struct <xsl-parameters-and-variables>))

(define-type :sxpath-node: (or :xml-node: :xml-attribute:))
(define-type :sxpath-nodelist: (or :sxpath-node: (list-of :sxpath-node:)))
(define-type :sxpath-pred: (:sxpath-nodelist: :xml-node: :xsl-envt: --> boolean))
(define-type :sxpath-step: (:sxpath-nodelist: :xml-node: :xsl-envt: --> :sxpath-nodelist:))

(define-type :ip-addr: string)

(define-type :oid: symbol)

(define-type :prot: (list-of :oid:))
(define-type :capa: (pair :oid: :prot:))
(define-type :capaset: (list-of :capa:))

(cond-expand
 (use-vector+assoc-as-frame
  (define-type :property: *)
  (define-type :propertyset: *)
  )
 (else ; use-record+llrbtree-as-frame
  (define-type :property: (struct <property>))
  (define-type :propertyset: (struct <property>))
  ))

(cond-expand
 (use-hashtable-for-links
  ;; FIXME: this one SHOULD not include "boolean"!
  (define-type :linkset: (or false (struct hash-table)))
  ;; A (new) type for "maybe"... to be used in the future.
  (define-type :?linkset: (or false (struct hash-table)))
  )
 (else
  ;; FIXME: cleanup of those needs adjustments in the chicken
  ;; implementation.
  (define-type :linkset: (or false *))
  (define-type :?linkset: (or false *))
  ))


;; BLOB/Blocks
(define-type :blob: (struct <blob>))
(define-type :block-table: (struct <block-table>))

(define-type :hash: string)
(define-type :md5-hash: symbol)
(define-type :blob-hash: symbol)

(define-type :quorum: (struct <quorum>))
(define-type :message: (struct <frame>))
(define-type :place: (pair :message: symbol))
(define-type :place-or-not: (or false :place:))
(define-type :frame-version: (pair number string))
(define-type :reply-channel: (struct <mailbox>))

(define-type :body-plain:
  (or false string :blob:
      ;; TODO: remove this case
      ;; The aggreement protocol and http-server pass "almost
      ;; arbitrary" Scheme data along in this slot
      list))

(define-type :message-path: (or (list-of string) (pair :oid: (list-of string))))

(define-type :place-accessor: (* #!rest --> *))
(define-type :message-accessor: (* --> *))

(define-type :message-digest: (procedure ((struct <frame>)) :hash:))

(define-type :call-subject:
  (:place:				; subject
   :quorum:				; from-quorum
   symbol				; type: read/write
   :message:				; message
   -> :message:))
(define-type :commit-frame!:
  (* :place:
     :message:
     (list-of (pair string *))
     -> undefined))
(define-type :resync!:
  (procedure
   (:oid:
    :place:
    :quorum:)
   boolean))

;; Host Mesh / Connection Pool

(define-type :mesh-cert: (struct <mesh-certificate>))

(define-type :mesh-channel: (struct <http-channel>))
(define-type :mesh-host: (struct <ball-host>))

;; Protocol handlers

(define-type :aggrement-handler: (:oid: :message: -> :message:))

;; Storage API

(define-type :storage-adaptor: (struct <storage-adaptor>))
(define-type :stores: (list-of (pair :storage-adaptor: *)))
