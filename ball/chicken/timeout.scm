;; (C) 2002, 2003, 2008 Jörg F. Wittenberger

;; timeout handling using chicken and srfi-18.

;; (cond-expand [(not csc-compile-shared) (declare (unit timeout))] [else])

(declare
 (unit v-timeout)
 (usual-integrations)
 ;;  will not lock: on swap-queue!
 (disable-interrupts)			; currently required
 ;; promises
 (strict-types)
 (block)
 (local) (inline)
 )

(register-feature! 'timeout)

(module
 v-timeout
 ()

(import (prefix timeout tmt:) b-timeout)
(import scheme)
(import (except chicken add1 sub1 with-exception-handler condition? promise?))
(import shrdprmtr
	srfi-1 (except srfi-18 raise)
	srfi-19 srfi-34 #;atomic mailbox
	(except posix create-pipe duplicate-fileno file-close process-wait)
	(only extras format)
	util)

(import (only timeout timeout-object? timeout-object))

(include "typedefs.scm")
(include "timeout-types.scm")

(define-inline (timeout:time<=? a b) (srfi19:time<=? a b))

(define-inline (timeout:time<? a b) (srfi19:time<? a b))

(define-inline (thread-alive? thread)
  (not (or (eq? 'dead (thread-state thread))
	   (eq? 'terminated (thread-state thread)))) )

(define *timeout-object* (timeout-object))

(define (thread-signal-timeout! t)
  ;; (print-call-chain (current-error-port) 0 t)
  (if (thread-alive? t) (thread-signal! t *timeout-object*)))

(include "../mechanism/srfi/llrbsyn.scm")

(: timeout-queue-entry? (* --> boolean : (struct <timeout-queue>)))

(cond-expand
 (rbtree

  ;; Does currently not work, since define-rbtree is not included.

  (define-record-type <timeout-queue>
    (make-timeout-queue-entry color parent left right time value)
    timeout-queue-entry?
    (color timeout-queue-color timeout-queue-color-set!)
    (parent timeout-queue-parent timeout-queue-parent-set!)
    (left timeout-queue-left timeout-queue-left-set!)
    (right timeout-queue-right timeout-queue-right-set!)
    (time timeout-queue-time timeout-queue-time-set!)
    (value timeout-queue-value timeout-queue-value-set!))

  (define timeout-queue-leftmost timeout-queue-right)

  (define timeout-queue-leftmost-set! timeout-queue-right-set!)

  (define (timeout-queue-before? node1 node2) ;; ordering function
    (timeout:time<? (timeout-queue-time node1) (timeout-queue-time node2)))

  (define-rbtree
    timeout-queue-init!	      ;; defined by define-rbtree
    timeout-queue->rbtree       ;; defined by define-rbtree
    #f			      ;; no lookup by define-rbtree
    timeout-queue-node-fold     ;; defined by define-rbtree
    #f ;; timeout-queue-node-for-each ;; defined by define-rbtree
    timeout-queue-node-insert!  ;; defined by define-rbtree
    timeout-queue-remove!       ;; defined by define-rbtree
    timeout-queue-reposition!   ;; defined by define-rbtree
    timeout-queue-empty?	      ;; defined by define-rbtree
    timeout-queue-singleton?    ;; defined by define-rbtree
    timeout-queue-match?
    timeout-queue-time-before?
    timeout-queue-before?
    timeout-queue-color
    timeout-queue-color-set!
    timeout-queue-parent
    timeout-queue-parent-set!
    timeout-queue-left
    timeout-queue-left-set!
    timeout-queue-right
    timeout-queue-right-set!
    timeout-queue-leftmost
    timeout-queue-leftmost-set!
    #f
    #f))
 (else

  (define-record-type <timeout-queue>
    (make-timeout-queue-entry color left right time value #;ln)
    timeout-queue-entry?
    (color timeout-queue-color timeout-queue-color-set!)
    (left timeout-queue-left timeout-queue-left-set!)
    (right timeout-queue-right timeout-queue-right-set!)
    (time timeout-queue-time timeout-queue-time-set!)
    (value timeout-queue-value timeout-queue-value-set!)
    #;(ln timeout-queue-ln)
    )

(define-record-printer (<timeout-queue> x out)
  (format out "#(tq l ~a r ~a c ~a ~a)"
	  (and (timeout-queue-left x) #t)
	  (and (timeout-queue-right x) #t)
	  (and (timeout-queue-color x) #t)
	  (and (timeout-queue-time x)
	       (rfc-822-timestring (time-utc->date (timeout-queue-time x) (timezone-offset))))) )

  (define-syntax timeout-queue-update!
    (syntax-rules (left: right: color:)
      ((_ n) n)
      ((_ n left: v . more)
       (begin
	 (timeout-queue-left-set! n v)
	 (timeout-queue-update! n . more)))
      ((_ n right: v . more)
       (begin
	 (timeout-queue-right-set! n v)
	 (timeout-queue-update! n . more)))
      ((_ n color: v . more)
       (begin
	 (timeout-queue-color-set! n v)
	 (timeout-queue-update! n . more)))
      ))

  (define-syntax timeout-queue=?
    (syntax-rules ()
      ((_ node1 node2)
       (timeout:time=? (timeout-queue-time node1) (timeout-queue-time node2)))))

  (define-syntax timeout-queue<?
    (syntax-rules ()
      ((_ node1 node2)
       (timeout:time<=? (timeout-queue-time node1) (timeout-queue-time node2)))))

  (define-llrbtree/positional
    ()
    timeout-queue-update!
    timeout-queue-init!	           ;; defined
    #f				   ;; no lookup defined
    #f				   ;; no min defined
    timeout-queue-node-fold	   ;; defined
    #f				   ;; no for-each defined
    timeout-queue-node-insert!	   ;; defined
    #f			           ;; no delete defined
    timeout-queue-node-delete-min! ;; defined
    timeout-queue-empty?	   ;; defined
    #f				   ;; key-node-eq? is unused
    timeout-queue=?
    #f				   ;; key-node<? is unused
    timeout-queue<?
    timeout-queue-left
    timeout-queue-right
    timeout-queue-color
    )

  (define (timeout-queue-fold proc init tree)
    (timeout-queue-node-fold
     (lambda (node init) (proc (timeout-queue-time node) (timeout-queue-value node) init))
     init tree))

  ))

(: timeout-queue-invalidate! ((struct <timeout-queue>) -> undefined))
(define (timeout-queue-invalidate! obj)
  (let ((v (timeout-queue-value obj)))
    (if v (timeout-queue-value-set! obj #f))))

(define-syntax make-entry
  (syntax-rules ()
    ((_ time value #;ln)
     (cond-expand
      (rbtree (make-timeout-queue-entry #f #f #f #f time value))
      (else (make-timeout-queue-entry #f #f #f time value #;,ln))))))

(define-syntax entry-init!
  (syntax-rules () ((_ q) (timeout-queue-init! q))))

;;*** general code

(cond-expand
 (rbtree

  (define-record-type <timeout-queue>
    (%make-timeout-queue mutex queue)
    timeout-queue?
    (mutex timeout-queue-mutex))
  (define (make-timeout-queue name)
    (let ((r (make-entry #f #f)))
      (timeout-queue-init! r)
      (%make-timeout-queue (make-mutex name) r))))

 (else

  (define-record-type <timeout-queue>
    (%make-timeout-queue mutex leftmost queue)
    timeout-queue?
    (mutex timeout-queue-mutex)
    (leftmost timeout-queue-leftmost timeout-queue-leftmost-set!)
    (queue timeout-queue-queue))
  (define (make-timeout-queue name)
    (let ((r (make-entry #f #f #;#f)))
      (timeout-queue-init! r)
      (%make-timeout-queue (make-mutex name) #f r)))))

(define *stop-list* (make-timeout-queue 'stopset))

(define (timeout-queue-name tq)
  (mutex-name (timeout-queue-mutex tq)))

(define-syntax entry-time (syntax-rules () ((_ obj) (timeout-queue-time obj))))
(define-syntax entry-value (syntax-rules () ((_ obj) (timeout-queue-value obj))))
(define-syntax set-entry-value!
  (syntax-rules () ((_ obj v) (timeout-queue-value-set! obj v))))

#|
(define (entry-insert! obj)
  (with-mutex (timeout-queue-mutex *stop-list*)
	      `(timeout-queue-node-insert! (timeout-queue-queue *stop-list*) ,obj)))
|#

(cond-expand
 (rbtree
  (define-syntax entry-insert!
    (syntax-rules ()
      ((_ task)
       (timeout-queue-node-insert! (timeout-queue-queue *stop-list*) task)))))
 (else
  (define-syntax entry-insert!
    (syntax-rules ()
      ((_ task)
       (let ((top (timeout-queue-leftmost *stop-list*)))
	 (if (not top)
	     (timeout-queue-leftmost-set! *stop-list* task)
	     (if (timeout:time<? (timeout-queue-time top) (timeout-queue-time task))
		 (timeout-queue-node-insert! (timeout-queue-queue *stop-list*) task)
		 (begin
		   (timeout-queue-node-insert! (timeout-queue-queue *stop-list*) top)
		   (timeout-queue-leftmost-set! *stop-list* task))))))))))

(cond-expand
 (rbtree
  (define-syntax %entry-remove!
    (syntax-rules ()
      ((_ lst)
       (begin (timeout-queue-remove! lst)
	      (and-let* ((v (entry-value lst)))
			(set-entry-value! lst #f)
			v))))))
 (else
  (define-syntax %entry-remove!
    (syntax-rules ()
      ((_ lst)
       (and-let* ((top (timeout-queue-leftmost *stop-list*)))
		 (timeout-queue-leftmost-set!
		  *stop-list* (timeout-queue-node-delete-min! (timeout-queue-queue *stop-list*)))
		 (and-let* ((v (entry-value top)))
			   (set-entry-value! top #f)
			   v)))))))

#|
(define (entry-remove! obj)
  (with-mutex (timeout-queue-mutex *stop-list*) (%entry-remove! obj)))
|#

(define-syntax entry-remove! (syntax-rules () ((_ obj) (%entry-remove! obj))))

(cond-expand
 (double-linked-list
  (define (display-stop-list)
    (cons
     'timeoutlist
     (let loop ((lst (entry-successor (timeout-queue-queue *stop-list*))))
       (if (not (eq? lst (timeout-queue-queue *stop-list*)))
	   (if (not (entry-value lst))
	       (loop (entry-successor lst))
	       `((timeoutlistentry
		  (timeoutdate
		   ,(rfc-822-timestring
		     (time-utc->date (entry-time lst) (timezone-offset))))
		  (timeoutobject ,(format #f "~s" (entry-value lst))))
		 . ,(loop (entry-successor lst))))
	   '())))))
 (rbtree
  (define (display-stop-list)
    (cons
     'timeoutlist
     (timeout-queue-node-fold
      (lambda (e i)
	(if (not (entry-value e))
	    i
	    `((timeoutlistentry
	       (timeoutdate
		,(rfc-822-timestring
		  (time-utc->date (entry-time e) (timezone-offset))))
	       (timeoutobject ,(format "~s" (entry-value e))))
	      . ,i)))
      '()
      (timeout-queue-queue *stop-list*)))))
 (else
  (define (display-stop-list)
    (cons
     'timeoutlist
     (fold-right
      (lambda (e i)
	(if (entry-value e)
	    `((timeoutlistentry
	       (timeoutdate
		,(rfc-822-timestring
		  (time-utc->date (entry-time e) (timezone-offset))))
	       (timeoutobject ,(format "~s" (entry-value e))))
	      . ,i)
	    i))
      '()
      (let* ((r (timeout-queue-node-fold cons '() (timeout-queue-queue *stop-list*)))
	     (t (timeout-queue-leftmost *stop-list*)))
	(if t (cons t r) r)))))))

(define (handle-timeout-queue! now)
  (let loop ((lst (timeout-queue-leftmost *stop-list*)))
    (if (and (cond-expand
	      ((or double-linked-list rbtree)
	       (not (eq? lst (timeout-queue-queue *stop-list*))))
	      (else (and lst (entry-time lst))))
	     (srfi19:time<=? (entry-time lst) now))
	(let ((success (entry-remove! lst)))
	  (cond
	   ((not success))
	   ((thread? success) (thread-signal-timeout! success))
	   ((procedure? success)
	    (thread-start!
	     (make-thread
	      (lambda ()
		(guard (exception
			(else (log-condition "Timeout ran into exception"
					     exception)))
		       (success)))
	      "time-shifted-execution")))
	   ((mailbox? success) (send-message! success (timeout-object)))
	   (else (logerr "internal error watchdog saw ~s\n" success)))
	  (loop (timeout-queue-leftmost *stop-list*))))))

#|
(: *execute-at* (struct <mailbox>))
(define *execute-at* (make-mailbox 'execute-at))

(: swap-queue! (-> . *))
(define (swap-queue!)
  (let ((x *execute-at*))
    (set! *execute-at* (make-mailbox 'execute-at))
    #;(let loop ()
      (if (not (mailbox-empty? x))
	  (let ((task (receive-message! x)))
	    #;(logerr "TMO: ~a ~a\n" (timeout-queue-ln task) (entry-value task))
	    (if (entry-value task) (entry-insert! task))
	    (loop))))
    (##test#mailbox-for-each
     (lambda (task) (if (entry-value task) (entry-insert! task)))
     x)))

(: execute-at (:time: :defer-target: #;* -> :timeout-reference:))
(define (execute-at time obj #;ln)
  (let ((task (make-entry time obj #;ln)))
    (send-message! *execute-at* task)
    (if (fx> (mailbox-number-of-items *execute-at*) 2048)
	(begin
	  ;;(logerr "Warning: huge delayed queue\n")
	  (swap-queue!)
	  (guard
	   (ex (else (logerr "huge delay handing caught exception ~a\n" ex)))
	   (handle-timeout-queue! (srfi19:current-time 'time-utc)))
	  (check-watchdog!)
	  (thread-yield!)))
    task))
|#

(: *execute-at* (vector fixnum fixnum vector))
(define *execute-at* (make-vector-queue))

(: ckeck+store ((struct <timeout-queue>) -> undefined))
(define (ckeck+store task)
  (if (entry-value task) (entry-insert! task)))

(: swap-queue! (-> . *))
(define (swap-queue!)
  (let ((x *execute-at*))
    (set! *execute-at* (make-vector-queue))
    #;(let loop ()
      (if (not (mailbox-empty? x))
	  (let ((task (receive-message! x)))
	    #;(logerr "TMO: ~a ~a\n" (timeout-queue-ln task) (entry-value task))
	    (if (entry-value task) (entry-insert! task))
	    (loop))))
    (##test#vector-queue-for-each ckeck+store x)))

(: swap-queue0! (-> . *))
(define (swap-queue0!)
  (let ((x *execute-at*)
	(y (make-vector-queue)))
    (set! *execute-at* y)
    #;(let loop ()
      (if (not (mailbox-empty? x))
	  (let ((task (receive-message! x)))
	    #;(logerr "TMO: ~a ~a\n" (timeout-queue-ln task) (entry-value task))
	    (if (entry-value task) (entry-insert! task))
	    (loop))))
    (##test#vector-queue-for-each
     (lambda (task)
       (if (entry-value task) (vector-queue-add! y task)))
     x)))


(: execute-at (:time: :defer-target: #;* -> :timeout-reference:))
(define (execute-at time obj #;ln)
  (let ((task (make-entry time obj #;ln)))
    (vector-queue-add! *execute-at* task)
    (if (fx> (vector-queue-size *execute-at*) 512)
	(begin
	  ;;(logerr "Warning: huge delayed queue\n")
	  (swap-queue0!)
	  #;(guard
	   (ex (else (logerr "huge delay handing caught exception ~a\n" ex)))
	   (run-time-procedure (srfi19:current-time 'time-utc)))
	#;(check-watchdog!)
	  ))
    task))

(: queue:timeout-value (* --> (or false :time:)))
(define (queue:timeout-value time)
  (cond ((and (number? time) (> time 0))
	 (add-duration 
	  (srfi19:current-time 'time-utc)
;; 	  (make-time 'time-duration
;; 		     (* (- time (floor time))
;; 			1000 1000 1000)
;; 		     (floor time))
	  (let ((sec (floor time)))
	    (make-time 'time-duration  (* (- time sec) 1000000000) sec))))
	((srfi19:time? time)
	 (case (time-type time)
	   ((time-duration)
	    (and (or (> (time-second time) 0)
		     (and (eqv? (time-second time) 0)
			  (> (time-nanosecond time) 0)))
		 (add-duration 
		  (srfi19:current-time 'time-utc) time)))
	   ((time-utc)
	    (and (srfi19:time>? 
		  time (srfi19:current-time 'time-utc))
		 time))
	   ((time-tai)
	    (and (srfi19:time>?
		  time (srfi19:current-time 'time-tai))
		 (time-tai->time-utc time)))
	   ((time-monotonic)
	    (and (srfi19:time>?
		  time (srfi19:current-time 'time-monotonic))
		 (time-monotonic->time-utc time)))
	   (else #f)))
	(else #f)))

(: queue:register-timeout-message!
   (:timeout: :defer-target: -> (or false :timeout-reference:)))
(define (queue:register-timeout-message! time mbox)
  (and-let* ((time (queue:timeout-value time)))
	    (execute-at time mbox #;'queue:register-timeout-message!)))

(: queue:cancel-timeout-message! ((or boolean (struct <timeout-queue>)) -> boolean))
(define (queue:cancel-timeout-message! obj)
  (if obj (timeout-queue-invalidate! obj))
  #f)

(: threads:timeout-value (* --> (or false :time:)))
(define (threads:timeout-value time)
  (cond ((and (number? time) (> time 0)) time)
	((srfi19:time? time)
	 (case (time-type time)
	   ((time-duration)
	    (let* ((ms (+ (* (time-second time) 1000)
			  (quotient (time-nanosecond time) 1000000))))
	      (and (> ms 0) (/ ms 1000))))
	   ((time-utc time-tai time-monotonic)
	    (let* ((v (time-difference
		       time (srfi19:current-time 
			     (time-type time))))
		   (ms (+ (* (time-second v) 1000)
			  (quotient (time-nanosecond v) 1000000))))
	      (and (> ms 0) (/ ms 1000))))
	   (else #f)))
	(else #f)))

(define (threads:register-timeout-message!* timeout target)
  (let ((parent (current-thread)))
    (thread-start!
     (make-thread
      (lambda ()
	(handle-exceptions
	 ex
	 (if (join-timeout-exception? ex)
	     ((if (mailbox? target) mailbox-send! thread-signal!)
	      target (timeout-object))
	     (raise ex))
	 (thread-join! parent timeout))
	#;(begin (thread-sleep! time)
	(send-message! mbox (timeout-object))))
      (format "~a wait ~a s" (current-thread) timeout)))))

(define (threads:register-timeout-message! time target)
  (and-let* ((ms (threads:timeout-value time)))
	    (threads:register-timeout-message!* ms target)))

(define (threads:cancel-timeout-message! obj)
  ;; (if (and obj (entry-value obj)) (entry-remove! obj))
  (if obj (timeout-queue-invalidate! obj))
  #f)

(: *time-procedure* (or false (procedure (:time:) . *)))
(define *time-procedure* #f)
(%!*system-time* (srfi19:current-time 'time-utc))
(define *system-date* (time-utc->date tmt:*system-time* 0))

(define (timestamp) *system-date*)

(define (register-time-procedure! proc)
  (set! *time-procedure* proc))

(: run-time-procedure (:time: -> undefined))
(define (run-time-procedure now)
  ;; We should check the timezone just at 2 a.m.?
  (if (eqv? (modulo (time-second now) 3600) 0)
      (timezone-tzset))
  (set! *system-date* (time-utc->date now 0))
  (if *time-procedure*
      (thread-start!
       (make-thread
	(lambda ()
	  (guard
	   (exception
	    (else (receive
		   (title msg args rest) (condition->fields exception)
		   (logcond 'system-clock title msg args))))
	   (*time-procedure* now)))
	"clocktick")))
  (handle-timeout-queue! now))

(define parent-process
  (guard
   (ex (else #f))
   (parent-process-id)))
;; Create a procedure, which takes a thunk to be serialized with
;; possible timeout.
 
(define (send-alive-signal) (process-signal parent-process signal/cont))

(define (update-system-time! now dead-parent)
  (if (not (eqv? (time-second tmt:*system-time*) (time-second now)))
      (let ((now (make-time 'time-utc 0 (time-second now))))
	(%!*system-time* now)          ; update system wide cache
        (and parent-process
             (guard
              (ex (else (logerr " Failed to send alive signal.\n")
                        (dead-parent)))
              (send-alive-signal)))
        (guard
         (ex (else (log-condition "watchdog caught exception ~a\n" ex)))
         (run-time-procedure now))
        (swap-queue!))))

;; Debugging situation: there is (since 20110516) a situation, when
;; the watchdog thread (in chicken only) disappears from the listing
;; and the whole thing blocks.  Trying to get a hand on it I'm putting
;; safety belts around.
;;
;; It appears to be the case that chicken sometimes does not properly
;; remove black llrb nodes with the left branch black too.  However
;; the same update sequence is handled properly by the very same code
;; in unit test environments.

(: *watchdog* (or false (struct thread)))
(define *watchdog* #f)

(define (make-watchdog! dead-parent)
  (set!
   *watchdog*
   (thread-start!
    (make-thread
     (lambda ()
       (thread-start!
	(let ((wd (current-thread)))
	  (lambda ()
	    (guard (ex (else #f)) (thread-join! wd))
	    (logerr "watchdog dead\n")
	    (exit 1))))
       (do ((now (srfi19:current-time 'time-utc) (srfi19:current-time 'time-utc)))
	   (#f)
	 (update-system-time! now dead-parent)
	 (thread-sleep!
	  (/ (if (fx>= (time-nanosecond now) 950000000)
		 (add1 (quotient (- 1000000000 (time-nanosecond now)) 2000000))
		 (fx- 990 (quotient (time-nanosecond now) 1000000)))
	     1000.0))))
     "watchdog"))))

(define (check-watchdog!)
  (if *watchdog*
      (let ((watchdog *watchdog*))
	(if (not (thread? watchdog))
	    (begin
	      (logerr "Illegal watchdog!\n")
	      (logerr "Watchdog is ~a\n" watchdog)
	      (exit 1)))
	(logerr "Watchdog is ~a\n" watchdog)
	(logerr "Watchdog state ~a\n" (thread-state watchdog))
	(logerr "Watchdog timeout ~a\n" (##sys#slot watchdog 4))
	(let ((tm (##sys#slot watchdog 4)))
	  ;; If the modified scheduler is active, the slot contains an
	  ;; entry into the priority queue while native chicken keeps
	  ;; the timeout value as a flownum.
	  (if (and tm (not (number? tm))) (set! tm (##sys#slot tm 3)))
	  (cond
	   ((and (eq? (thread-state watchdog) 'blocked)
		 tm
		 (< (+ tm 100)
		    (current-milliseconds)))
	    (logerr "Unblocking watchdog (overdue ~a ms)\n"
		    (- (current-milliseconds) tm))
	    (##sys#thread-unblock! watchdog)
	    (thread-yield!))
	   (else
	    (logerr "Watchdog ~a reason ~a tm ~a\n"
		    (thread-state watchdog) (##sys#slot watchdog 11)
		    (and tm (- tm (current-milliseconds)))))))))

  ;;(thread-signal-timeout! (current-thread))
  )

(%!check-watchdog! check-watchdog!)

(%!respond-timeout-interval (make-config-parameter #f))

(%!remote-fetch-timeout-interval (make-config-parameter #f))

(%!timeout
 (lambda (time-out method access request)
   (tmt:with-timeout time-out (lambda () (method access request)))))


(define (threads:with-timeout time-out thunk . make-signal)
  (if time-out
      (let ((pending (xthe (or false :timeout-reference:) #f)))
	(dynamic-wind
	    (lambda ()
	      (set! pending
		    (queue:register-timeout-message!
		     time-out
		     (if (null? make-signal)
			 (current-thread)
			 (let ((t (current-thread)))
			   (always-assert (procedure? (car make-signal)))
			   (lambda ()
			     (if (thread-alive? t)
				 (thread-signal! t ((car make-signal))))))))))
	    thunk
	    (lambda () (queue:cancel-timeout-message! pending))))
      (thunk)))

(define (threads:with-timeout! time-out thunk)
  (if time-out
      (let ((pending (xthe (or false :timeout-reference:) #f)))
	(dynamic-wind
	    (lambda ()
	      (set! pending (queue:register-timeout-message! time-out (current-thread))))
	    (lambda ()
	      (thread-join! (thread-start! (make-thread thunk (thread-name (current-thread))))))
	    (lambda () (queue:cancel-timeout-message! pending))))
      (thunk)))

(define (with-timeout!+ time-out thunk)
  (if time-out
      (let ((pending (xthe (or false :timeout-reference:) #f))
	    (thread (make-thread thunk (thread-name (current-thread)))))
	(dynamic-wind
	    (lambda ()
	      (set! pending (queue:register-timeout-message! time-out (current-thread))))
	    (lambda ()
	      (guard
	       (ex ((timeout-object? ex)
		    (thread-signal-timeout! thread)
		    (raise ex)))
	       (thread-join! (thread-start! thread))))
	    (lambda () (queue:cancel-timeout-message! pending))))
      (thunk)))

(define (with-timeout+ time-out thunk)
  (if time-out
      (let ((pending (xthe (or false :timeout-reference:) #f))
	    (thread (make-thread thunk (thread-name (current-thread)))))
	(dynamic-wind
	    (lambda ()
	      (set! pending (queue:register-timeout-message! time-out (current-thread))))
	    (lambda ()
	      (thread-join! (thread-start! thread)))
	    (lambda ()
	      (thread-terminate! thread)
	      (queue:cancel-timeout-message! pending))))
      (thunk)))

(define (queue:with-timeout time-out thunk ;;; #!optional (ln #f)
			    )
  (let ((time (queue:timeout-value time-out)))
    (if time
	(let* ((task (execute-at time (current-thread) #;ln))
	       (success (the * #f))
	       (result
		(dynamic-wind (lambda () #f)
		    (lambda () (call-with-values thunk list))
		    (lambda () (set! success (entry-value task)) (timeout-queue-invalidate! task)))))
	  (if success (apply values result)
	      (raise (timeout-object))))
	(thunk))))

(define (timeout-second arg)
  (cond ((and (srfi19:time? arg)
	      (eq? (time-type arg) 'time-duration))
	 (let ((s (time-second arg))
	    (ns (if (> (time-nanosecond arg) 1000000)
		    (* (time-nanosecond arg) 0.000000001)
		    0)))
	   (if (> s 0) (+ s ns) (if (> ns 0) ns #f))))
	((and (number? arg)
	      (> arg 0))
	 arg)
	(else #f)))

(%!register-time-procedure! register-time-procedure!)
(%!thread-signal-timeout! thread-signal-timeout!)
(%!timeout-object? timeout-object?)
(%!make-watchdog! make-watchdog!)
(%!update-system-time! update-system-time!)
(%!send-alive-signal send-alive-signal)
(%!timestamp timestamp)
(%!display-stop-list display-stop-list)
(%!timeout-second timeout-second)

(define *timeout-handling* 'threads)

(define (timeout-handling-policy . how)
  (if (pair? how)
      (begin
        (case (car how)
          ((threads)
           (set! *timeout-handling* (car how))
           (%!with-timeout threads:with-timeout)
           (%!with-timeout! threads:with-timeout!)
           (%!register-timeout-message! queue:register-timeout-message!)
           (%!cancel-timeout-message! queue:cancel-timeout-message!))
          ((queue)
           (set! *timeout-handling* (car how))
           (%!with-timeout queue:with-timeout)
           (%!with-timeout! queue:with-timeout)
           (%!register-timeout-message! queue:register-timeout-message!)
           (%!cancel-timeout-message! queue:cancel-timeout-message!))
          )))
  *timeout-handling*)

(%!timeout-handling-policy timeout-handling-policy)
;;(timeout-handling-policy 'threads)
(timeout-handling-policy 'queue)

) ;; module timeout
