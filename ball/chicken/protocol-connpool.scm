;; (C) 2002, 2003, 2008 Jörg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit protocol-connpool)
 ;;
 (usual-integrations)
;;NO (disable-interrupts) ; optional
 )

(module
 protocol-connpool
 (
  $https-client-verbose
  $http-client-connections-maximum
  https-client-open
  http-client-allocate-connection
  http-client-dispose-connection
  bidirectional-connection?
  $topology-verbose logtopo
  http-host-alive?
  http-host-maybe-alive?
  http-update-alive-time!
  update-alive-time-from-server! update-alive-time-from-callback-server!
  http-set-channel-callback!
  http-close-one-old-connection!
  https-client-open-sslmgr
  http-client-open
  *http-alive-cache*
  
  ;; Sollbruchstelle

  http-with-channel
  display-http-channels
  http-display-hosts
  *http-routeable-hosts*
  http-find-channel
  make-host-entry
  http-close-channel!
  host-id host-protocol host-address host-cert
  init-http-voters!
  host-cert-lookup
  http-host-lookup
  http-host-lookupV2
  http-all-hosts
  http-host-add! http-host-remove!
  http-collect-call-function
  host-seems-dead?
  ;;
  $connect-back
  $https-socks4a-server $https-use-socks4a
  http-connect-back
  https-certification-for
  host-collect-call


  *http-voters* *http-auths*
  %allocate-bidirectional-connection
  http-channel-connection-cache
  connection-name connection-certificate
  connection-input-port connection-output-port
  bidirectional-connection-cache-number-of-items
  connection-close!
  ;;
  set-http-do-connect-back!
  ;;
  ;;;
  voter-auth
  http-affirm-authorization http-affirm-user-authorization
  http-connect-back-magic? http-connect-back-magic
  host-seems-dead-duration
  host-seems-alive-duration
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:)
	 (except srfi-18 raise)
	 srfi-19 srfi-34 srfi-35 srfi-69
	 (only ports call-with-input-string)
	 shrdprmtr util timeout atomic parallel cache mailbox
	 mesh-cert sslsocket
	 (only tree literal)
	 pcre protocol-common)
;; place-common ought not to be imported here!
(import place-common)
(import (only place $broadcast-update-statistics))

(import tree notation place-common)

(include "typedefs.scm")

(define-type :mesh-connection: (struct <bidirectional-connection>))

(define-inline (loghttps fmt . rest)
  (if ($https-client-verbose) (apply logerr fmt rest)))

(define-syntax logtopo
  (syntax-rules ()
    ((_ fmt rest ...)
     (if ($topology-verbose) (logerr fmt rest ...)))))

(define-inline (process-id x) x)

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/connpool.scm")

)

(import (prefix protocol-connpool m:))
(define $topology-verbose m:$topology-verbose)
