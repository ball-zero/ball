;; (C) 2015 Joerg F. Wittenberger see http://www.askemos.org

(declare
 (unit mesh-cert)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;; (fixnum-arithmetic)
 (disable-interrupts) ;; some tings are called from uninterruptible code.
 (not usual-integrations raise signal error current-exception-handler)
 
 )

(module
 mesh-cert
 (
  make-mesh-certificate
  make-mesh-cert
  mesh-certificate?
  mesh-cert-l mesh-cert-o
  subject-string->mesh-certificate
  mesh-cert-subject->string
  mesh-cert=?
  )

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken ;; add1
		sub1 condition? promise? with-exception-handler vector-copy!)
	srfi-34 srfi-35
	openssl atomic clformat irregex pcre lalr openssl util)

;; FIXME KLUDGE
;; We need to insulate the oid syntax.
;;(import (only place-common string->oid))

(define (string->oid id)
  (let ((l (string-length id)))
    (and (eqv? l 33)
         (and (char=? (string-ref id 0) #\A)
	      (let loop ((i 1))
		(and (or (eqv? i 33)
			 (let ((c (string-ref id i)))
			   (and (or (and (char>=? c #\0) (char<=? c #\9))
				    (and (char>=? c #\a) (char<=? c #\f)))
				(loop (add1 i)))))
		     ;; FIXME KLUDGE
		     ;;
		     ;; THIS SHOULD BE (intern-oid id)
		     ;;
		     ;; - which is unavialable -
		     (string->symbol id)
		     ))))))

(include "typedefs.scm")

(include "../mechanism/protocol/certs.scm")
(define-record-printer (<mesh-certificate> x out) (clformat out "<mesh-certificate ~a>" (mesh-cert-raw-subject x)))
) ;; module mesh-cert

(import (prefix mesh-cert m:))

(define subject-string->mesh-certificate m:subject-string->mesh-certificate)
(define make-mesh-certificate m:make-mesh-certificate)
(define make-mesh-cert m:make-mesh-cert)
(define mesh-certificate? m:mesh-certificate?)
(define mesh-cert-l m:mesh-cert-l)
(define mesh-cert-o m:mesh-cert-o)
(define mesh-cert=? m:mesh-cert=?)
