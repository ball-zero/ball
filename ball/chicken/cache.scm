;; (C) 2008, 2010 Joerg F. Wittenberger see http://www.askemos.org

(declare
 (unit cache)
 (fixnum-arithmetic)
 (disable-interrupts)			; optional, but no loops here.
 (not standard-bindings with-exception-handler)
 (not usual-integrations raise signal error current-exception-handler)
 (foreign-declare #<<EOF

#define AC_string_compare(to, from, n, m)   C_mk_bool(C_strncmp(C_c_string(to), C_c_string(from), C_unfix(n)) < C_unfix(m))

#define AC_lessp(x,y) C_mk_bool(x<y)

EOF
))

(module
 cache
 (make-cache cache-size
  cache-ref cache-ref/default
  cache-invalid! cache-invalid/abort!
  cache-reref
  cache-set! cache-fold cache-cleanup! cache-state-update!
  )
 (import (except scheme force delay)
	 foreign
	 (except chicken add1 sub1 condition? promise? with-exception-handler)
	 (except srfi-18 raise)
	 (only srfi-1 delete) srfi-34 srfi-35 srfi-45 threadpool atomic util)

 (import (only extras format))

 (include "typedefs.scm")

 (define-type :ce: (struct <cache-entry>))

 (: cache-entry? (* --> boolean :ce:))
 (: cache-entry-color (:ce: -> boolean))
 (: cache-entry-color-set! (:ce: boolean -> undefined))
 (: cache-entry-left (:ce: -> :ce:))
 (: cache-entry-left-set! (:ce: :ce: -> undefined))
 (: cache-entry-right (:ce: -> :ce:))
 (: cache-entry-right-set! (:ce: :ce: -> undefined))
 (: cache-entry-key/s (:ce: -> string))
 (: cache-entry-key/s-set! (:ce: string -> undefined))
 (: cache-entry-key (:ce: -> *))
 (: cache-entry-state (:ce: -> *))
 (: cache-entry-state-set! (:ce: * -> undefined))
 (: cache-entry-mutex (:ce: -> (struct mutex)))
 (: cache-entry-avail (:ce: -> (struct condition)))
 (: cache-entry-thread (:ce: -> (or false (struct thread))))
 (: cache-entry-thread-set! (:ce: (or false (struct thread)) -> undefined))
 (: cache-entry-thunk (:ce: -> :thunk:))
 (: cache-entry-thunk-set! (:ce: :thunk: -> undefined))
 (: cache-entry-cont (:ce: -> (or false procedure)))
 (: cache-entry-cont-set! (:ce: (or false procedure) -> undefined))
 (: cache-entry-values (:ce: -> list))
 (: cache-entry-values-set! (:ce: list -> undefined))

 (include "../mechanism/srfi/llrbsyn.scm")

(cond-expand
 (never
  (define-record-type <cache-entry>
    (%make-cache-entry color left right key/s key
		       state mutex avail thread thunk cont values)
     cache-entry?
     (color cache-entry-color cache-entry-color-set!)
     (left cache-entry-left cache-entry-left-set!)
     (right cache-entry-right cache-entry-right-set!)
     (key/s cache-entry-key/s cache-entry-key/s-set!)
     (key cache-entry-key)
     (state cache-entry-state cache-entry-state-set!)
     (mutex cache-entry-mutex)
     (avail cache-entry-avail)
     (thread cache-entry-thread cache-entry-thread-set!)
     (thunk cache-entry-thunk cache-entry-thunk-set!)
     (cont cache-entry-cont cache-entry-cont-set!)
     (values cache-entry-values cache-entry-values-set!)))
 (chicken

(define-inline (%make-cache-entry color left right key/s key
				  es mutex avail thread thunk cont values)
  (##sys#make-structure
   '<cache-entry>
   color left right key/s key
   es mutex avail thread thunk cont values))
(define-inline (cache-entry-color n) (##sys#slot n 1))
(define-inline (cache-entry-color-set! n v) (##sys#setslot n 1 v))
(define-inline (cache-entry-left n) (##sys#slot n 2))
(define-inline (cache-entry-left-set! n v) (##sys#setslot n 2 v))
(define-inline (cache-entry-right n) (##sys#slot n 3))
(define-inline (cache-entry-right-set! n v) (##sys#setslot n 3 v))
(define-inline (cache-entry-key/s n) (##sys#slot n 4))
(define-inline (cache-entry-key/s-set! n v) (##sys#setslot n 4 v))
(define-inline (cache-entry-key n) (##sys#slot n 5))

(define-inline (cache-entry-state n) (##sys#slot n 6))
(define-inline (cache-entry-state-set! n v) (##sys#setslot n 6 v))
(cond-expand
 (lock-free
  (define-inline (cache-entry-owner n) (##sys#slot n 7))
  (define-inline (cache-entry-owner-set! n v) (##sys#setslot n 7 v)))
 (else (define-inline (cache-entry-mutex n) (##sys#slot n 7))))
(define-inline (cache-entry-avail n) (##sys#slot n 8))
(define-inline (cache-entry-thread n) (##sys#slot n 9))
(define-inline (cache-entry-thread-set! n v) (##sys#setslot n 9 v))
(define-inline (cache-entry-thunk n) (##sys#slot n 10))
(define-inline (cache-entry-thunk-set! n v) (##sys#setslot n 10 v))
(define-inline (cache-entry-cont n) (##sys#slot n 11))
(define-inline (cache-entry-cont-set! n v) (##sys#setslot n 11 v))
(define-inline (cache-entry-values n) (##sys#slot n 12))
(define-inline (cache-entry-values-set! n v) (##sys#setslot n 12 v))
))

(cond-expand
 (lock-free
  (define-inline (cache-entry-lock! entry) #f)
  (define-inline (cache-entry-owned? entry) (number? (cache-entry-owner entry)))
  (define unlocked (make-mutex))
  (define cache-entry-aquire!
    (let ((n 0))
      (lambda (entry t)
	(set! n (add1 n))
	(cache-entry-owner-set! entry n)
	n)))
  (define-inline (cache-entry-release! entry)
    (cache-entry-thread-set! entry #f)
    (cache-entry-owner-set! entry #f))
  (define-inline (cache-entry-unlock! entry) #f)
  (define-inline (cache-entry-unlock/wait! entry)
    (mutex-unlock! unlocked (cache-entry-avail entry)))
  )
 (else
  (define-inline (cache-entry-owner entry) (cache-entry-thread entry))
  (define-inline (cache-entry-owned? entry) (thread? (cache-entry-thread entry)))
  (define-inline (cache-entry-aquire! entry t) (cache-entry-thread-set! entry t))
  (define-inline (cache-entry-release! entry) (cache-entry-thread-set! entry #f))
  (define-inline (cache-entry-lock! entry) (mutex-lock! (cache-entry-mutex entry)))
  (define-inline (cache-entry-unlock! entry) (mutex-unlock! (cache-entry-mutex entry)))
  (define-inline (cache-entry-unlock/wait! entry)
    (mutex-unlock! (cache-entry-mutex entry) (cache-entry-avail entry)))
  ))

(cond-expand
 (debug-lock
  (define *retained-cache-entries* (make-parameter '()))
  (define-syntax with-cache-entry
    (syntax-rules ()
      ((_ entry body ...)
       (let ((sofar (*retained-cache-entries*)))
	 (if (memq entry sofar)
	     (raise (format "recursion on cache entry ~a" (cache-entry-key entry)))
	     (parameterize
	      ((*retained-cache-entries* (cons entry sofar)))
	      (retain-mutex (cache-entry-mutex entry) body ...)))))))
  (define-syntax with-cache-entry-aquired
    (syntax-rules ()
      ((_ entry body ...)
       (begin entry body ... (cache-entry-release! entry) entry))))
  (define-syntax yield-cache-entry
    (syntax-rules ()
      ((_ entry body ...)
       (parameterize
	((*retained-cache-entries* (delete entry (*retained-cache-entries*))))
	(yield-mutex-set! body ...)))))
  )
 (lock-free
  (define-syntax with-cache-entry
    (syntax-rules ()
      ((_ entry body ...)
       (begin body ...))))
  (define-syntax with-cache-entry-aquired
    (syntax-rules ()
      ((_ entry body ...)
       (begin (cache-entry-aquire! entry #t) body ... (cache-entry-release! entry) entry))))
  (define-syntax yield-cache-entry
    (syntax-rules ()
      ((_ entry body ...)
       (begin body ...)))
  ))
 (else
  (define-syntax with-cache-entry
    (syntax-rules ()
      ((_ entry body ...)
       (retain-mutex (cache-entry-mutex entry) body ...))))
  (define-syntax with-cache-entry-aquired
    (syntax-rules ()
      ((_ entry body ...)
       (begin entry body ... (cache-entry-release! entry) entry))))
  (define-syntax yield-cache-entry
    (syntax-rules ()
      ((_ entry body ...)
       (yield-mutex-set! body ...)))
  )))

(define-syntax with-cache-entry-owned
  (syntax-rules ()
    ((_ entry owner body ...)
     (with-cache-entry entry
		       (if (eq? owner (cache-entry-owner entry))
			   (begin body ...)
			   (cond-expand
			    (chicken
			     (if (not (cache-entry-owner entry))
			       (begin
				 (logerr "Conflict action ~a/~a entry ~a owned by ~a\n"
					 (current-thread) owner (cache-entry-key entry) (cache-entry-owner entry))
				 #;(begin body ...))))
			    (else (begin)))
			   )))))

(define-syntax %string<?
  (syntax-rules ()
    ((_ si1 si2)
     (let ((s1 si1) (s2 si2))
       (let ((len1 (##core#inline "C_block_size" s1))
	     (len2 (##core#inline "C_block_size" s2)) )
	 (cond
	  ((fx= len1 len2)
	   (##core#inline "AC_string_compare" s1 s2 len1 0))
	  ((##core#inline "AC_lessp" len1 len2)
	   (##core#inline "AC_string_compare" s1 s2 len1 1))
	  (else (##core#inline "AC_string_compare" s1 s2 len2 0))) )))))

;; don't use just "string=?" - in chicken it will miss the (most
;; common) a=b case

(define-syntax %string=?
  (syntax-rules ()
    ((_ si1 si2)
     (let ((s1 si1) (s2 si2))
       (or (eq? s1 s2)
	   (let ((len1 (##core#inline "C_block_size" s1)) )
	     (and (fx= len1 (##core#inline "C_block_size" s2))
		  (fx= (##core#inline "C_string_compare" s1 s2 len1) 0)) ))))))

(define-syntax cache-entry-update!
  (syntax-rules (left: right: color:)
    ((_ n) n)
    ((_ n left: v . more)
     (begin
       (cache-entry-left-set! n v)
       (cache-entry-update! n . more)))
    ((_ n right: v . more)
     (begin
       (cache-entry-right-set! n v)
       (cache-entry-update! n . more)))
    ((_ n color: v . more)
     (begin
       (cache-entry-color-set! n v)
       (cache-entry-update! n . more)))
    ))

(define-syntax cache-entry-key-node-eq?
  (syntax-rules ()
    ((_ key node) (%string=? key (cache-entry-key/s node)))))

(define-syntax string-node-key-node-<?
  (syntax-rules () ((_ key node) (%string<? key (cache-entry-key/s node)))))

(define-syntax cache-entry-node-node-=?
  (syntax-rules ()
    ((_ node1 node2) (%string=? (cache-entry-key/s node1) (cache-entry-key/s node2)))))

(define-syntax cache-entry-node-node<?
  (syntax-rules ()
    ((_ node1 node2) (%string<? (cache-entry-key/s node1) (cache-entry-key/s node2)))))

(define-llrbtree/positional
   (ordered)
   cache-entry-update!
   cache-node-init!    ;; defined
   cache-node-lookup	   ;; defined
   #f			   ;; cache-min not defined
   cache-node-fold	   ;; defined
   #f ;; cache-node-for-each	   ;; undefined
   cache-node-insert!	   ;; defined
   cache-node-delete!	   ;; defined
   #f			   ;; cache-node-delete-min! not defined
   cache-empty?		   ;; defined
   cache-entry-key-node-eq? ;; key-node-eq?
   cache-entry-node-node-=? ;; node comparison function
   string-node-key-node-<?  ;; key-node-<? ordering function
   cache-entry-node-node<?  ;; before? node ordering function
   cache-entry-left
   cache-entry-right
   cache-entry-color
   )

(define-inline (make-cache-root)
  (cache-node-init!
   (%make-cache-entry
    #f #f #f  #f ;; color left right, key/s
    #f #f	 ;; key state
    #f #f #f     ;; mutex/owner avail thread
    #f #f #f	 ;; thunk cont values
    )))

(define-inline (make-cache-entry cache-name es key/s key thunk)
  (let ((nm (dbgname key "~a-entry-~a" cache-name)))
    (%make-cache-entry
     #f #f #f  key/s ;; color left right, key
     key es	     ;; state
     (cond-expand
      (lock-free #f)
      (else (make-mutex nm)))
     (make-condition-variable nm) #f
     thunk #f #f)))

(define-record-type <cache>
  (%make-cache name key2string mutex index state miss hit fulfil valid? delete)
  cache?
  (name cache-name)
  (key2string cache-key2string)
  (mutex cache-mutex)
  (index cache-index)
  (state cache-state)
  (miss cache-miss)
  (hit cache-hit)
  (fulfil cache-fulfil)
  (valid? cache-valid?)
  (delete cache-delete))

(define (default-hit-handler cache value-state) #f)

(define (default-fulfil-handler cache value-state values-or-false) #f)

(define (default-delete-handler cache-state value-state) #f)

;; temporary KLUDGE to get it working; the key->string handling MUST be moved
;; to the external interface exclusively
(define (cache-key-str obj)
  (if (##sys#check-symbol obj 'symbol->string)
      (##sys#symbol->string obj)
      (raise (format "cache-key-str not a symbol ~a" obj))))

(define (identity x) x)

(: make-cache
   (*
    (procedure (* *) boolean)		; eq?
    *					; cache-state
    (procedure (* *) . *)		; (miss cache-state key)
    (or false (procedure ((struct <cache>) *) . *)) ; hit
    (or false (procedure ((struct <cache>) * *) . *)) ; fulfil
    (procedure (*) boolean)		; valid?
    (or false (procedure ((struct <cache>) *) . *))
    --> (struct <cache>)))
(define (make-cache name eq state miss hit fulfil valid? delete)
  (%make-cache
   name
   (cond
    ((eq? eq eq?) cache-key-str)
    ((eq? eq string=?) identity)
    (else (error "make-cache: only symbols and strings as key so far")))
   (make-mutex name)
   (make-cache-root)
   state miss
   (or hit default-hit-handler)
   (or fulfil default-fulfil-handler)
   valid?
   (or delete default-delete-handler)))

(: cache-size ((struct <cache>) -> fixnum))
(define (cache-size cache) -1)		; currently not supported

(define-inline (cache-lookup cache key)
  (cache-node-lookup (cache-index cache) ((cache-key2string cache) key)))

(define-inline (cache-value-fulfiled! cache entry values)
  ((cache-fulfil cache) cache (cache-entry-state entry) values))

(: set-entry-value! ((struct <cache-entry>) list procedure -> *))
(define-inline (set-entry-value! entry new avail)
  (cache-entry-values-set! entry new)
  (cache-entry-cont-set! entry avail)
  (cache-entry-release! entry)
  (condition-variable-broadcast! (cache-entry-avail entry)))

;; Compute the value, run trigger and signal completion.

(cond-expand
 (lock-free (: cache-entry-force! ((struct <cache>) * :ce: number -> undefined)))
 (else (: cache-entry-force! ((struct <cache>) * :ce: (struct thread) -> undefined))))
(define (cache-entry-force! cache key entry owner)
  (guard
   (exception
    (else
     (let ((new (list (if (condition? exception) exception
			  (make-condition
			   &message 'message
			   (format "~a ~a ~a" key (cache-entry-thunk entry) exception))))))
       (with-cache-entry-owned
	entry owner
	(cache-value-fulfiled! cache entry #f)
	(set-entry-value! entry new raise)))))
   (call-with-values (cache-entry-thunk entry)
     (lambda result
       (with-cache-entry-owned
	entry owner
	(cache-value-fulfiled! cache entry result)
	(set-entry-value! entry result values))))))

(define-inline (cache-entry-delete! cache entry)
  (cache-value-fulfiled! cache entry #f)
  (set-entry-value! entry '(cache-entry-deleted) raise)
  ;; FIXME: should we t(h)erminate/grill the thread somehow?
  (cache-entry-release! entry)
  (cache-node-delete! (cache-index cache) (cache-entry-key/s entry)))

;; Arrange to compute the value if needed and return it.

;; returns no known value

(cond-expand
 ((and lock-free threadpool)
  (define (!cache-entry-force cache key entry)
    (or (cache-entry-owned? entry)
	(pool-cache-entry-force! cache key entry (cache-entry-aquire! entry #t))))
  )
 (lock-free
  (define-inline (!cache-entry-force cache key entry)
    (or
     (cache-entry-owned? entry)
     (let ((owner (cache-entry-aquire! entry #t)))
       (cache-entry-thread-set!
	entry
	(thread-start!
	 (make-thread
	  (lambda ()
	    (cache-entry-force! cache key entry owner))
	  (dbgname key "~a-ref"))))))))
 (else
  (define-inline (!cache-entry-force cache key entry)
    (or
     (cache-entry-owned? entry)
     (cache-entry-aquire!
      entry
      (thread-start!
       (make-thread
	(lambda ()
	  (cache-entry-force! cache key entry (current-thread)))
	(dbgname key "~a-ref"))))))))

(cond-expand
 (debug-lock
  (define-syntax with-cache-index
    (syntax-rules ()
      ((_ cache body ...)
       (retain-mutex (cache-mutex cache) body ...)))))
 (else
  (define-syntax with-cache-index
    (syntax-rules ()
      ((_ cache body) body)
      ((_ cache body ...)
       (begin body ...))))))

;; Wait for computed value and return it.

(define (raise-cache-abandon-exception cache key)
  (raise (make-condition
	  &message 'message
	  (format "cache-entry-wait ~a aborted on ~a" (cache-name cache) key))))

(define (cache-entry-wait cache key entry)
  (let ((avail (cache-entry-cont entry)))
    (if (procedure? avail)
	(apply avail (cache-entry-values entry))
	(begin
	  (cache-entry-lock! entry)
	  (let ((avail (cache-entry-cont entry)))
	    (if (and (procedure? avail)
		     ((cache-valid? cache) (cache-entry-state entry)))
		(let ((results (cache-entry-values entry)))
		  (cache-entry-unlock! entry)
		  (apply avail results))
		(let loop ()
		  (yield-mutex-set! (!cache-entry-force cache key entry))
		  (cache-entry-unlock/wait! entry)
		  (let ((results (cache-entry-values entry))
			(avail  (cache-entry-cont entry)))
		    (if (procedure? avail)
			(apply avail results)
			#;(let ((new (with-cache-index cache (cache-lookup cache key))))
			  (if (and new (not (eq? new entry)))
			      (cache-entry-wait cache key new)
			      (raise-cache-abandon-exception cache key)))
			(loop)
			#;(raise-cache-abandon-exception cache key))))))))))

(define-inline (cache-find!1 cache key key/s thunk)
  (or (cache-node-lookup (cache-index cache) key/s)
      (let ((entry (make-cache-entry
		    (cache-name cache)
		    ((cache-miss cache) (cache-state cache) key)
		    key/s key
		    thunk)))
	(cache-node-insert! (cache-index cache) entry)
	entry)))

(define-inline (cache-find! cache key thunk)
  (cache-find!1 cache key ((cache-key2string cache) key) thunk))

(define-inline (cache-find cache key thunk)
  (hang-on-mutex! 'cache-find (list (cache-mutex cache)))
  (let ((key/s ((cache-key2string cache) key)))
    (or (cache-node-lookup (cache-index cache) key/s)
	(with-cache-index cache (cache-find!1 cache key key/s thunk)))))

(define-inline (cache-entry-ref cache key entry)
  (hang-on-mutex! 'cache-entry-ref (list (cache-entry-mutex entry)))
  (let ((avail (cache-entry-cont entry)))
    (if (procedure? avail)
	(if ((cache-valid? cache) (cache-entry-state entry))
	    (let ((results (cache-entry-values entry)))
	      ((cache-hit cache) cache (cache-entry-state entry))
	      (apply avail results))
	    (begin
	      (with-cache-entry
	       entry
	       (let ((results (cache-entry-values entry))
		     (avail (cache-entry-cont entry)))
		 (if (and (procedure? avail)
			  ((cache-valid? cache) (cache-entry-state entry)))
		     (begin
		       ((cache-hit cache) cache (cache-entry-state entry))
		       (apply avail results))
		     (if (not (cache-entry-owned? entry))
			 (with-cache-entry-aquired
			  entry
			  (cache-entry-cont-set! entry #f)
			  ((cache-delete cache) (cache-state cache) (cache-entry-state entry))
			  (cache-entry-state-set! entry ((cache-miss cache) (cache-state cache) key))
			  (cache-entry-values-set! entry #f))))))
	      (cache-entry-wait cache key entry)))
	(cache-entry-wait cache key entry))))

(define-inline (cache-entry-ref/default cache key entry default)
  (hang-on-mutex! 'cache-entry-ref/default (list (cache-entry-mutex entry)))
  (let ((avail (cache-entry-cont entry)))
    (if (procedure? avail)
	(if ((cache-valid? cache) (cache-entry-state entry))
	    (let ((results (cache-entry-values entry)))
	      ((cache-hit cache) cache (cache-entry-state entry))
	      (apply avail results))
	    (with-cache-entry
	     entry
	     (let ((results (cache-entry-values entry))
		   (avail (cache-entry-cont entry)))
	       (if (and (procedure? avail)
			((cache-valid? cache) (cache-entry-state entry)))
		   (begin
		     ((cache-hit cache) cache (cache-entry-state entry))
		     (apply avail results))
		   (if (cache-entry-owned? entry)
		       default
		       (begin
			 (with-cache-entry-aquired
			  entry
			  (cache-entry-cont-set! entry #f)
			  ((cache-delete cache) (cache-state cache) (cache-entry-state entry))
			  (cache-entry-state-set! entry ((cache-miss cache) (cache-state cache) key))
			  (cache-entry-values-set! entry #f)
			  (yield-mutex-set! (!cache-entry-force cache key entry)))
			 default))))))
	(begin
	  (yield-mutex-set! (!cache-entry-force cache key entry))
	  default))))

(define (cache-state-update! cache state proc)
  (hang-on-mutex! 'cache-state-update! (list (cache-mutex cache)))
  (with-cache-index cache (proc (cache-state cache) state)))

(define-inline (thread-alive? thread)
  (not (or (eq? 'dead (thread-state thread))
	   (eq? 'terminated (thread-state thread)))) )

(define-inline (%%cache-set! cache entry owner cont results) ; preco: entry is locked
  (if (eq? owner (cache-entry-owner entry))
      (begin
	#;(if (and (thread? owner) (thread-alive? owner))
	    (thread-signal! t 'cache-entry-killed))
	((cache-delete cache) (cache-state cache) (cache-entry-state entry))
	(cache-value-fulfiled! cache entry (and (eq? cont values) results))
	(set-entry-value! entry results cont)
	;; (if (and owner (thread-alive? owner)) (thread-signal! owner cache-abandon-exception))
	))
  entry)

(define (%cache-set! cache key job . res)
  (hang-on-mutex! '%cache-set!
		  (let ((entry (cache-lookup cache key)))
		    (if entry (list (cache-entry-mutex entry)) '())))
  (let ((entry (cache-lookup cache key)))
    (if entry
	(let ((owner (cache-entry-owner entry)))
	  ;; Kill the owner if we can, that is, if it does not yet hold the entry.
	  (if owner
	      (with-cache-entry
	       entry
	       (with-cache-entry-aquired
		entry
		(let ((owner (cache-entry-owner entry)))
		  (%%cache-set! cache entry owner job res))))
	      (with-cache-entry
	       entry
	       (with-cache-entry-aquired
		entry
		(let ((owner (cache-entry-owner entry)))
		  (%%cache-set! cache entry owner job res))))))
	(cache-find! cache key (lambda () (apply job res))))))

;; (cache-set! cache key . job+res)
;;
;; if (null? job+res): remove entry
;; if (null? (cdr job+res)):
;;   (eq? (car job+res) #t): reset to last valid, unfulfiled last thunk
;;   (procedure? (car job+res)): reset to valid, unfilfiled (car job+res)
;; else: set to (apply (car job+res) (cdr job+res))

(: cache-set! ((struct <cache>) * #!rest -> . *))
(define (cache-set! cache key . job+res)  
  (cond
   ((null? job+res)
    (and-let* ((entry (cache-lookup cache key)))
	      ((cache-delete cache) (cache-state cache) (cache-entry-state entry))
	      (cache-entry-delete! cache entry)))
   ((eq? (car job+res) #t) (cache-invalid! cache key))
   ((null? (cdr job+res)) (cache-invalid! cache key (car job+res)))
   (else (apply %cache-set! cache key job+res))))

#|

Keep the full code until the RScheme version (at least) is forked.

(define (cache-invalid/check! check cache key . thunk)
  (let ((entry (cache-lookup cache key)))
    (if (and entry (check entry))
	(with-cache-index
	 cache
	 (let ((entry (cache-lookup cache key))) ;; TODO remove the lock and double lookup
	   (if (and entry (check entry))
	       (with-cache-entry-aquired
		entry
		(cache-entry-cont-set! entry #f)
		;; FIXME: should we t(h)erminate/grill the thread somehow?
		(cache-entry-thread-set! entry #f)
		((cache-delete cache) (cache-state cache) (cache-entry-state entry))
		(cache-entry-state-set! entry ((cache-miss cache) (cache-state cache) key))
		(if (pair? thunk) (cache-entry-thunk-set! entry (car thunk)))
		entry))))
	(begin
	  (if (and (not entry) (pair? thunk))
	      (let ((entry (cache-find cache key (car thunk))))
		(cache-entry-ref/default cache key entry #f)
		entry)
	      entry)))))
|#

(define (cache-invalid/check! check cache key . thunk)
  ;; may hang on the 'cache' itself, even though the current code will not
  (hang-on-mutex! 'cache-invalid/check! (list (cache-mutex cache)))
  (hang-on-mutex! 'cache-invalid/check!
		  (let ((entry (cache-lookup cache key)))
		    (if entry (list (cache-entry-mutex entry)) '())))
  (let ((entry (cache-lookup cache key)))
    (if (and entry (check entry))
	(with-cache-entry
	 entry
	 (with-cache-entry-aquired
	  entry
	  (cache-entry-cont-set! entry #f)
	  ;; FIXME: should we t(h)erminate/grill the thread somehow?
	  (cache-entry-thread-set! entry #f)
	  ((cache-delete cache) (cache-state cache) (cache-entry-state entry))
	  (cache-entry-state-set! entry ((cache-miss cache) (cache-state cache) key))
	  (if (pair? thunk) (cache-entry-thunk-set! entry (car thunk)))
	  entry))
	(begin
	  (if (and (not entry) (pair? thunk))
	      (let ((entry (cache-find cache key (car thunk))))
		(cache-entry-ref/default cache key entry #f)
		entry)
	      entry)))))

;; (cache-invalid! cache key . thunk)
;;
;; Invalidate the cached values.  If there's a running computation,
;; leave it running.  If thunk is given, it's arranged to be called,
;; otherwise default is returned.

(: cache-invalid! ((struct <cache>) * #!rest procedure -> *))
(define (cache-invalid! cache key . thunk)
  (define (check entry)
    (and-let* ((avail (cache-entry-cont entry)))
	      ((cache-valid? cache) (cache-entry-state entry))))
  (apply cache-invalid/check! check cache key thunk))

;; (cache-invalid/abort! cache key . thunk)
;;
;; Invalidate the cached values and abort any running computation. If
;; thunk is given, it's arranged to be called, otherwise default is
;; returned.

(: cache-invalid/abort! ((struct <cache>) * #!rest procedure -> *))
(define (cache-invalid/abort! cache key . thunk)
  (define (check entry)
    (let ((avail (cache-entry-cont entry)))
      (or (not avail)
	  ((cache-valid? cache) (cache-entry-state entry)))))
  (apply cache-invalid/check! check cache key thunk))

;; (cache-ref/default cache key thunk default)
;;
;; Returns current cached value or default.  Does never wait.  If
;; thunk is given, it's arranged to be called, otherwise default is
;; returned.

(: cache-ref/default ((struct <cache>) * (or false (procedure () . *)) * -> . *))
(define (cache-ref/default cache key thunk default)
  (if thunk
      (cache-entry-ref/default cache key (cache-find cache key thunk) default)
      (let ((entry (cache-lookup cache key)))
	(if entry (cache-entry-ref/default cache key entry default) default))))

;; (cache-ref cache key thunk . default)
;;
;; If default is given falls back to cache-ref/default.  Otherwise
;; returns the last valid cached values.  Always waits for valid
;; values, possibly arranging thunk to produce them.

(: cache-ref ((struct <cache>) * (procedure () . *) -> . *))
(define (cache-ref cache key thunk)
  (cache-entry-ref cache key (cache-find cache key thunk)))

(: cache-reref ((struct <cache>) * (procedure () . *) -> . *))
(define (cache-reref cache key thunk)
  (cache-entry-ref cache key (cache-invalid! cache key thunk)))

;; (cache-fold cache f nil)
;;
;; fold f(key value nil) over cache content

(: cache-fold ((struct <cache>) (procedure (* * *) . *) * -> . *))
(define (cache-fold cache f nil)
  (cache-node-fold (lambda (v nil)
		     (if (eq? (cache-entry-cont v) values)
			 (f (cache-entry-key v) (car (cache-entry-values v)) nil)
			 nil))
		   nil
		   (cache-index cache)))

;; (cache-cleanup! cache [valid?] [used?])
;;
;; valid? : default: (cache-valid cache)
;; used?  : no default; of cache result values arity.
;;
;; Remove all entries, which are not "valid?" and "used?" (if given).
;; Used is applied to the cached values.

(: cache-cleanup! ((struct <cache>) #!rest (procedure (*) boolean) procedure -> undefined))
(define (cache-cleanup! cache . predicates)
  (hang-on-mutex! 'cache-cleanup! (list (cache-mutex cache)))
  (let ((valid? (if (and (pair? predicates) (procedure? (car predicates)))
		    (car predicates) (cache-valid? cache)))
	(used? (if (and (pair? predicates) (pair? (cdr predicates)))
		   (cadr predicates) #t))
	(index (cache-index cache))
	(del (cache-delete cache)))
    (let ((removeable (cache-node-fold
		       (lambda (entry init)
			 (if (or (cache-entry-owned? entry)
				 (and (valid? (cache-entry-state entry))
				      (pair? (cache-entry-values entry))
				      (or (eq? used? #t)
					  (apply used? (cache-entry-values entry)))))
			     init
			     (begin
			       (with-cache-entry-aquired
				entry
				(guard
				 (ex (else ;; (log-condition (cache-name cache) ex)
				      #f))
				 (del (cache-state cache) (cache-entry-state entry))
				 ;; FIXME: should we t(h)erminate/grill the thread somehow?
				 (cache-entry-thread-set! entry #f)))
			       (cons entry init))))
		       '()
		       index)))
      (with-cache-index
       cache
       (for-each
	(lambda (entry) (cache-entry-delete! cache entry))
	removeable)))))

#|

 (import srfi-69 mailbox)

 (define (!apply proc args)
  (thread-join! (thread-start! (make-thread (lambda () (apply proc args)) "!apply"))))
 (include "../mechanism/srfi/cache.scm")
|#

 (define cache-request-type
  (make-threadpool-requesttype
   (lambda (req exception)
     (define-values (cache entry owner) (apply values req))
     (with-cache-entry-owned
      entry owner
      (cache-value-fulfiled! cache entry #f)
      (set-entry-value!
       entry
       (list exception #;(if (condition? exception) exception
	     (make-condition
	     &message 'message
	     (format "~a ~a ~a" cache entry exception))))
       raise)))
   (lambda (req) #f)))

 (define *cache-pool* (make-threadpool '*cache-pool* -1 cache-request-type))
 (define ##test#cache-pool *cache-pool*)

 (define (pool-cache-entry-force! cache key entry owner)
  (threadpool-order!
   *cache-pool* cache-request-type
   (list cache entry owner)
   (lambda (req entry)
     (define-values (cache entry1 owner) (apply values req))
     (call-with-values (cache-entry-thunk entry)
       (lambda result
	 (with-cache-entry-owned
	  entry owner
	  (cache-value-fulfiled! cache entry result)
	  (set-entry-value! entry result values)))))
   (list entry)))

)

(import (prefix cache m:))

(define make-cache m:make-cache)
(define cache-size m:cache-size)
(define cache-ref m:cache-ref)
(define cache-set! m:cache-set!)
(define chache-fold m:cache-fold)
(define cache-cleanup! m:cache-cleanup!)
