;; (C) 2011, J�rg F. Wittenberger

;; chicken module clause for the Askemos' markdown support.

(declare
 (unit markdown)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (strict-types)
 (usual-integrations))


(module
 markdown
 (
  markdown-parse
  markdown-format
  markdown->string
  )

(import (except scheme force delay vector-fill! vector->list list->vector)
	(except chicken add1 sub1 with-exception-handler condition? promise? vector-copy!)
	srfi-1 srfi-13 (prefix srfi-13 srfi:)
	srfi-34 srfi-35
	srfi-43 (prefix srfi-43 srfi:)
	srfi-45 srfi-69
	extras data-structures
)
(import util parallel tree notation notation-xmlrender function
	(only ports call-with-output-string)
	xmlrpc				; actually JSON only
	(only place-common body->string) ; is deprecated anyway!
	)

(import foreign (except srfi-18 raise)) ;; single-binary

(include "typedefs.scm")

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(foreign-declare #<<EOF

#include "../../libupskirt-1.0/markdown.h"
#include "../../libupskirt-1.0/renderers.h"

/* stolen from libupskirt */
#define READ_UNIT 1024
#define OUTPUT_UNIT 64
static int lace()
{

  struct buf *ib, *ob;
  size_t ret;
  ib = bufnew(READ_UNIT);
  bufgrow(ib, READ_UNIT);
  while ((ret = fread(ib->data + ib->size, 1,
                      ib->asize - ib->size, stdin)) > 0) {
    ib->size += ret;
    bufgrow(ib, ib->size + READ_UNIT);
  }
  ob = bufnew(OUTPUT_UNIT);
  markdown(ob, ib, &mkd_xhtml);
  fwrite(ob->data, 1, ob->size, stdout);
  bufrelease(ib);
  bufrelease(ob);
  exit(0);
}

/* single process version using static data -- requires protection
against multi threaded race conditions! */

static struct buf *the_lace_ib = NULL, *the_lace_ob = NULL;

static size_t lace_sp_start(C_word in, size_t size)
{
  the_lace_ib = bufnew(size);
  bufgrow(the_lace_ib, size);
  C_memcpy(the_lace_ib->data, C_c_string(in), size);
  the_lace_ib->size = size;
  the_lace_ob = bufnew(OUTPUT_UNIT);
  markdown(the_lace_ob, the_lace_ib, &mkd_xhtml);
  return the_lace_ob->size;
}

static C_word lace_sp_end(C_word out) {
  C_memcpy(C_c_string(out), the_lace_ob->data, the_lace_ob->size);
  bufrelease(the_lace_ib);
  bufrelease(the_lace_ob);
  the_lace_ib = the_lace_ob = NULL;
  return out;
}

EOF
)

(define libupskirt-parse-markdown1
  ;; FIXME: For Windows compatibility this should become a call to the
  ;; executable, which dispatchs to the procedure.
  (foreign-lambda int "lace"))

(define libupskirt-parse-markdown
  (let ((mutex (make-mutex 'markdown-parser)))
    (lambda (str)
      ;; Note: we are sure there is neither exception or continuation
      ;; capture in the dynamic-wound thunk, are we?!
      (dynamic-wind
          (lambda () (mutex-lock! mutex))
          (lambda ()
            (let* ((rs ((foreign-lambda size_t "lace_sp_start" scheme-object size_t)
                        str (string-length str)))
                   (res (make-string/uninit rs)))
              ((foreign-lambda scheme-object "lace_sp_end" scheme-object) res)))
          (lambda () (mutex-unlock! mutex))))))

(include "../policy/markdown.scm")
)

(import (prefix markdown m:))

(define markdown-parse m:markdown-parse)
(define markdown-format m:markdown-format)
(define markdown->string m:markdown->string)
