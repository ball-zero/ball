;; (C) 2002, J�rg F. Wittenberger

;; chicken module clause for the Askemos notation module.

(declare
 (unit notation-xmlparse)
 ;;
 (disable-interrupts)			; loops checked
 (usual-integrations)
 ;; promises
 (fixnum)
 (strict-types)
)

(module
 notation-xmlparse
 (
  xml-encoding
  xml-parse
  html-parse-permissive
  integer->utf8string
  )

(import (except scheme vector-fill! vector->list list->vector)
	(except chicken add1 sub1 condition? with-exception-handler)
	foreign
	srfi-1 srfi-13 srfi-34 srfi-35
	regex pcre util ports atomic tree extras notation-charenc notation-common)

(include "typedefs.scm")

(define-type :ssax:input: (pair (or char eof boolean) input-port))
(define-type :ssax:seed: *)
(define-type :ssax:token: (pair symbol *))
(define-type :ssax:unres-name: (or symbol (pair symbol symbol)))

(define-type :ssax:namespace: (pair symbol (pair (or boolean symbol)
						 (or null (or boolean symbol)))))

(define-type :ssax:namespaces: (list-of :ssax:namespace:))

(define-type :ssax:ns-decls: :ssax:namespaces:)

(define-type :ssax:decl-attr:
  (list :ssax:unres-name:
	(or symbol (list-of string))
	symbol
	*; report bug: chicken does not ecept this: (or string #f)
	))

(define-type :ssax:decl-attrs: (list-of :ssax:decl-attr:))

(define-type :ssax:elem-content-model: symbol)

(define-type :ssax:decl-elem:
  (list :ssax:unres-name:
	:ssax:elem-content-model:
	:ssax:decl-attrs:))

(define-type :ssax:decl-elems: (list-of :ssax:decl-elem:))

(define-type :ssax:ANY/decl-elems: (or boolean :ssax:decl-elems:))

(define-type :ssax:attlist: (list-of (pair :ssax:unres-name: string)))

(define-type :ssax:char-ref: string)

(define-type :ssax:entity-defs: (list-of (pair symbol string)))

(define-type :ssax:entity-cnt-handler:
  (procedure (input-port :ssax:entity-defs: :ssax:seed:) :ssax:seed:))
(define-type :ssax:str-handler: (procedure (string string :ssax:seed:) :ssax:seed:))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define xml-encoding
  (let ((m (pcre->aproc "<\\?xml(?:[[:space:]]+version=(?:'[^']*')|(?:\"[^\"]*\"))?[[:space:]]+encoding=(?:(?:'([^']*)')|(?:[^\"]*))")))
    (lambda (str)
      (let ((x (m str)))
	(and x (or (cadr x) (caddr x)))))))

;;** SSAX

(define (ssax:warn port msg . more)
  (for-each (lambda (m) (display m (current-error-port)))
	    (cons msg more))
  (newline (current-error-port)))

(define ssax-attrib-value-delimiters-strict
  '(#\newline #\return #\space #\tab #\< #\&))

(define ssax-attrib-value-delimiters-keep-lines
  '(#\< #\&))

(define ssax-attrib-value-delimiters
  ssax-attrib-value-delimiters-keep-lines)

(define (ssax-attributes-strict . flag)
  (if (pair? flag)
      (let ((old (eq? ssax-attrib-value-delimiters
		      ssax-attrib-value-delimiters-strict)))
	(set! ssax-attrib-value-delimiters
	      (if (car flag)
		  ssax-attrib-value-delimiters-strict
		  ssax-attrib-value-delimiters-keep-lines))
	old)
      (eq? ssax-attrib-value-delimiters ssax-attrib-value-delimiters-strict)))

(define ssax-make-xml-element make-specialised-xml-element)

(include "../mechanism/syntax/petrofsky-extract.scm")
(include "ssax-chicken.scm")
(include "../mechanism/notation/htmlprag.scm")

(define ssax-empty-namespace-declaration
  `((xml . ,namespace-xml-str)))

(define (html-parse-permissive obj)
  (guard
   (ex (else (htmlprag obj)))
   (ssax:xml->stucture obj ssax-empty-namespace-declaration)))

(define (hackerly-xml-parse-all from)
  (let ((n (ssax:xml->stucture from ssax-empty-namespace-declaration)))
    (cond
     ((node-list-empty? n) n)
     (else
      (let loop ((c (peek-char from)))
	(cond
	 ((eof-object? c) n)
	 ((memv c ssax:S-chars) (read-char from) (loop (peek-char from)))
	 (else (append n (hackerly-xml-parse-all from)))))))))

(: xml-parse ((or input-port string) #!rest -> :xml-nodelist:))
(define (xml-parse obj . rest)
  (let ((mode (and-let* ((mode-p (memq mode: rest))
                         ((pair? (cdr mode-p))))
                        (cadr mode-p)))
        (from (cond
	       ((port? obj) obj)
	       ((string? obj) (open-input-string (normalise-data obj #t #t "\n")))
	       (else (error (format "xml-parse can't parse ~a" obj))))))
    (case mode
      ((#f) (hackerly-xml-parse-all from))
      ((strict) (ssax:xml->stucture from ssax-empty-namespace-declaration))
      ((permissive) (html-parse-permissive from))
      ((html) (htmlprag from))
      (else (error (string-append
                    "Unknown parse mode " (symbol->string mode)))))))
; (include "xml-parse.scm")

)
