;; (C) 2008 Joerg F. Wittenberger see http://www.askemos.org

(declare
 (unit parallel)
 (fixnum-arithmetic)
 (not standard-bindings with-exception-handler)
 (not usual-integrations raise signal error current-exception-handler)
 )

(module
 parallel
 (delay* delay*thunk
  !start
  !order !order/thunk
  !after>
  !apply
  !call-with-values
  !map ;; experimental, don't use yet
  !mapfold ;; also experimental
  map-parallel
  )
 (import (except scheme force delay)
	 (except chicken add1 sub1 condition? promise? with-exception-handler)
	 (except srfi-18 raise)
	 srfi-34 srfi-35 srfi-45 atomic mailbox timeout util)

 (include "typedefs.scm")
 (include "../mechanism/srfi/parallel.scm")
;; (include "../mechanism/srfi/cache.scm")

)

(import (prefix parallel p:))

(define !order/thunk p:!order/thunk)
(define !apply p:!apply)
(define !call-with-values p:!call-with-values)
(define !map p:!map)
(define !mapfold p:!mapfold)
(define map-parallel p:map-parallel)
