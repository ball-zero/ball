;; (C) 2002, 2003, 2008 J�rg F. Wittenberger; All rights reserved.

;; chicken module clause for the Askemos protocol module.

(declare
 (unit protocol-clntclls)
 ;;
 (usual-integrations)
 (disable-interrupts))

(module
 protocol-clntclls
 (
  is-text/scheme?
  http-property-text/scheme
  http-property-post
  http-property-soap+xml
  ;;
  http-agreement-headers
  ;;
  http-replicate-call
  http-replicate-reply
  http-forward-call
  ;;
  http-get-state
  http-echo-call
  http-send-ready
  http-send-one-shot
  http-invite-new-supporters!
  ;;
  http-get-digest
  http-get-meta
  http-get-blob
  http-get-place
  http-get-privileges
  ;;
  http-get-reply
  ;;
  http-get-ca
  http-send-certificate-request
  ;;
  http-eval
  )

 (import (except scheme force delay)
	 (except chicken add1 sub1 with-exception-handler condition? promise?)
	 srfi-1 srfi-13 (prefix srfi-13 srfi:) (except srfi-18 raise)
	 srfi-19 srfi-34 srfi-35
	 atomic libmagic pcre regex util timeout mailbox
	 (prefix dns dns-)
	 atomic parallel cache
	 tree notation aggregate storage-api place-common place
	 protocol-common protocol-connpool
	 http-client
	 extras data-structures ports)

(include "typedefs.scm")

(define-inline (loghttps fmt . rest)
  (if ($https-client-verbose) (apply logerr fmt rest)))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../mechanism/protocol/http/clntclls.scm")

)
