;; (C) 2002, 2011 J�rg F. Wittenberger

;; chicken module clause for the Askemos CGI and SCGI interface.

(declare
 (unit cgi)
 ;;
 ;;
 (usual-integrations))

(module
 cgi
 (*cgi-dir* run-cgi scgi-response test-cgi)

(import (except scheme force delay)
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-1 srfi-13 (prefix srfi-13 srfi:)
	(except srfi-18 raise)
 srfi-34 srfi-35 srfi-45
 extras data-structures
	posix files ports sqlite3
	pcre util sslsocket timeout protection
)
(import tree notation bhob aggregate storage-api place-common pool place-ro)
(import place corexml protocol-common protocol openssl)

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(include "../policy/cgi.scm")
)

(import (prefix cgi m:))

(define *cgi-dir* m:*cgi-dir*)
(define run-cgi m:run-cgi)
(define scgi-response m:scgi-response)
(define test-cgi m:test-cgi)
