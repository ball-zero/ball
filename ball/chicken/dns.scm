;; (C) 2003, Jörg F. Wittenberger

;; Asynchronous DNS lookup.  Ported from PLT Scheme.

;; STATE: That's something to just get the job done, because blocking
;; dns kills Askemos performance.  I would write a different code.

;; TODO: leave only the interface from PLT.

(declare
 (unit dns)
 (usual-integrations)
;;NO (disable-interrupts) ; not proven, slow code
 )

(register-feature! 'dns-lookup)

(module
 dns
 (
  set-dns-nameserver!
  get-address get-addresses set-address!
  delete-address! add-address!
  get-name
  get-mail-exchanger get-mail-exchangers
  find-nameserver
  ;;
  ip-match
  )
(import scheme
	(except chicken add1 sub1 with-exception-handler condition? promise? vector-copy!)
	srfi-1 srfi-13 atomic
	(only srfi-18 make-mutex mutex? mutex-lock! mutex-unlock!)
	srfi-34 srfi-35 srfi-69
	regex sslsocket extras util data-structures)

(include "typedefs.scm")

(define etc-resolve.conf-nameserver-line-regex
  (regexp "nameserver[ \\t]+([[:digit:]]+\\.[[:digit:]]+\\.[[:digit:]]+\\.[[:digit:]]+)[[:space:]]*"))

(define-condition-type &dns-error &message dns-error?)

(define (raise-dns-error what)
  (raise (make-condition &dns-error 'message what)))

(define (raise-dns-eof-error what)
  (raise (make-condition &message 'message what)))

(define types
  '((a 1)
    (ns 2)
    (md 3)
    (mf 4)
    (cname 5)
    (soa 6)
    (mb 7)
    (mg 8)
    (mr 9)
    (null 10)
    (wks 11)
    (ptr 12)
    (hinfo 13)
    (minfo 14)
    (mx 15)
    (txt 16)
    (rp 17)
    (aaaa 28)
    (dname 39)
    (* 255)))

(define classes
  '((in 1)
    (cs 2)
    (ch 3)
    (hs 4)
    (none 254)
    (* 255)))

(define (cossa i l)
  (cond
   ((null? l) #f)
   ((equal? (cadar l) i)
    (car l))
   (else (cossa i (cdr l)))))

(define-syntax type->number
  (syntax-rules ()
    ((_ type)
     (let ((tmp (assoc type types)))
       (if tmp (cadr tmp) (raise-dns-error type))))))

(define-syntax class->number
  (syntax-rules ()
    ((_ class)
     (let ((tmp (assoc class classes)))
       (if tmp (cadr tmp) (raise-dns-error class))))))

(define (dns-rcode->string rcode)
  (case rcode
    ((1) "format error")
    ((2) "server failure")
    ((3) "name error")
    ((4) "not implemented")
    ((5) "refused")
    ((9) "not authoritative")))

(define (number->octet-pair n)
  (list (integer->char (arithmetic-shift-right n 8))
        (integer->char (modulo n 256))))

(define (octet-pair->number a b)
  (+ (arithmetic-shift-left (char->integer a) 8)
     (char->integer b)))

(define (octet-quad->number a b c d)
  (+ (arithmetic-shift-left (char->integer a) 24)
     (arithmetic-shift-left (char->integer b) 16)
     (arithmetic-shift-left (char->integer c) 8)
     (char->integer d)))

(define dotted-match
  (let ((match (regexp "^([^.]*)[.](.*)")))
    (define (dotted-match x)
      (string-match match x))
    dotted-match))

(define (name->octets s)
  (let ((do-one (lambda (s)
                  (cons
                   (integer->char (string-length s))
                   (string->list s)))))
    (let loop ((s s))
      (let ((m (dotted-match s)))
        (if m
            (append
             (do-one (cadr m))
             (loop (caddr m)))
            (append
             (do-one s)
             (list #\nul)))))))

(define (make-std-query-header id question-count)
  (append
   (number->octet-pair id)
   (list #\x1 #\nul) ; Opcode & flags (recusive flag set)
   (number->octet-pair question-count)
   (number->octet-pair 0)
   (number->octet-pair 0)
   (number->octet-pair 0)))

;; http://www.rfc-archive.org/getrfc.php?rfc=2136

(define (make-update-query-header update-count)
  (append
   (number->octet-pair (random 256))
   (list #\( ; (integer->char (arithmetic-shift-left 5 4))
	 #\nul) ; Opcode & flags
   (number->octet-pair 1)
   (number->octet-pair 0) ; prerequisite section
   (number->octet-pair update-count)
   (number->octet-pair 0) ; additional data section
   ))

(define (make-query id name type class)
  (append
   (make-std-query-header id 1)
   (name->octets name)
   (number->octet-pair (type->number type))
   (number->octet-pair (class->number class))))

(define (make-rr name type class ttl rdata)
  (append
   (name->octets name)
   (number->octet-pair (type->number type))
   (number->octet-pair (class->number class))
   (number->octet-pair (arithmetic-shift-right ttl 16))
   (number->octet-pair (bitwise-and ttl #xffff))
   (number->octet-pair ((if (pair? rdata) length string-length) rdata))
   (if (pair? rdata) rdata (string->list rdata))
   ))

(define (make-add-update class name type ttl rdata)
  (make-rr name type class ttl (ip->dns-ip rdata)))

(define (make-delete-rrset name type)
  (make-rr name type '* 0 ""))

(define (make-delete-name name)
  (make-rr name '* '* 0 ""))

(define (make-delete-rr class name type rdata)
  (make-rr name type class 0 rdata))

(define (make-update-section class command . rest)
  (case command
    ((add) (apply make-add-update class rest))
    ((delete-rrset) (apply make-delete-rrset rest))
    ((delete-name) (apply make-delete-name rest))
    ((delete-rr) (apply make-delete-rr rest))
    (else (raise-dns-error command))))

(define (make-update-call name class rest)
  (append
   (make-update-query-header (length rest))
   (name->octets name)
   (number->octet-pair (type->number 'soa))
   (number->octet-pair (class->number class))
   (fold-right
    (lambda (x i) (append (apply make-update-section class x) i))
    '()
    rest)
   ))

(define (add-size-tag m)
  (append (number->octet-pair (length m)) m))

(define (make-query-string query)
  (list->string (add-size-tag query)))

(define (##test#dns-make-query id name type class)
  (make-query-string (make-query id name type class)))

(define (rr-data rr)
  (cadddr (cdr rr)))

(define (rr-ttl rr)
  (cadddr rr))

(define (rr-class rr)
  (caddr rr))

(define (rr-type rr)
  (cadr rr))

(define (rr-name rr)
  (car rr))

(define (parse-name start reply)
  (let ((v (char->integer (car start))))
    (cond
     ((zero? v)
      ;; End of name
      (values #f (cdr start)))
     ((zero? (bitwise-and #xc0 v))
      ;; Normal label
      (let loop ((len v)(start (cdr start))(accum '()))
        (cond
         ((zero? len) 
          (receive (s start) (parse-name start reply)
                   (let ((s0 (list->string (reverse! accum))))
                     (values (if s
                                 (string-append s0 "." s)
                                 s0)
                             start))))
         (else (loop (sub1 len) (cdr start) (cons (car start) accum))))))
     (else
      ;; Compression offset
      (let ((offset (+ (arithmetic-shift-left (bitwise-and #x3f v) 8)
                       (char->integer (cadr start)))))
        (receive (s ignore-start)  (parse-name (list-tail reply offset) reply)
                 (values s (cddr start))))))))

(define (parse-rr start reply)
  (receive
   (name start) (parse-name start reply)
   (let ((type (car (cossa (octet-pair->number (car start) (cadr start)) types)))
         (start (cddr start)))
     (let ((class (car (cossa (octet-pair->number (car start) (cadr start)) classes)))
           (start (cddr start)))
       (let ((ttl (octet-quad->number (car start) (cadr start)
                                      (caddr start) (cadddr start)))
             (start (cddddr start)))
         (let ((len (octet-pair->number (car start) (cadr start)))
               (start (cddr start)))
					; Extract next len bytes for data:
           (let loop ((len len)(start start)(accum '()))
             (if (zero? len)
                 (values (list name type class ttl (reverse! accum))
                         start)
                 (loop (sub1 len) (cdr start) (cons (car start) accum))))))))))

(define (parse-ques start reply)
  (receive (name start) (parse-name start reply)
           (let ((type (car (cossa (octet-pair->number (car start) (cadr start)) types)))
                 (start (cddr start)))
             (let ((class (car (cossa (octet-pair->number (car start) (cadr start)) classes)))
                   (start (cddr start)))
               (values (list name type class) start)))))

(define (parse-n parse start reply n)
  (let loop ((n n)(start start)(accum '()))
    (if (zero? n)
        (values (reverse! accum) start)
        (receive (rr start) (parse start reply)
                 (loop (sub1 n) start (cons rr accum))))))

(define (dns-parse-reply auth? reply)
  (apply
   (lambda (x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 . start)
     (let ((qd-count (octet-pair->number x4 x5))
	   (an-count (octet-pair->number x6 x7))
	   (ns-count (octet-pair->number x8 x9))
	   (ar-count (octet-pair->number x10 x11)))
       
       (receive
	(qds start) (parse-n parse-ques start reply qd-count)
	(receive
	 (ans start) (parse-n parse-rr start reply an-count)
	 (receive
	  (nss start) (parse-n parse-rr start reply ns-count)
	  (receive
	   (ars start) (parse-n parse-rr start reply ar-count)
	   (if (not (null? start))
	       (raise-dns-error "error parsing server reply"))
	   (values auth? qds ans nss ars reply)))))))
   reply))

(define (update-prescan zname zclass rrs)
  (for-each
   (lambda (rr)
     (define rr.class (rr-class rr))
     (define rr.type (rr-type rr))
     (and (eq? zclass rr.class) (memq rr.type '(* axfr maila mailb))
	  (raise-dns-error "formerr"))
     (and (eq? rr.class '*)
	  (or (not (eqv? (rr-ttl rr) 0))
	      ;; FIXME: test rdlength too!
	      (memq rr.type '(axfr maila mailb)))
	  (raise-dns-error "formerr"))
     (and (eq? rr.class 'none)
	  (or (not (eqv? (rr-ttl rr) 0))
	      (memq rr.type '(* axfr maila mailb)))
	  (raise-dns-error "formerr")))
   rrs)
  rrs)

(define (do-dns-query r w before after query)

  (let ((reply
	 (dynamic-wind
	     before
	     
	     (lambda ()
	       (display (make-query-string query) w)
	       (flush-output-port w)
	       
	       (let ((a (read-char r))
		     (b (read-char r)))
		 (if (eof-object? b)
		     (raise-dns-eof-error "unexpected EOF from DNS server"))
		 (let ((len (octet-pair->number a b)))
		   (let ((s (read-bytes len r)))
		     (if (not (eqv? len (string-length s)))
			 (raise-dns-eof-error "unexpected EOF from DNS server"))
		     (string->list s)))))
	     
	     after)))

    ;; First two bytes must match sent message id:
    (if (not (and (char=? (car reply) (car query))
                  (char=? (cadr reply) (cadr query))))
        (raise-dns-eof-error "bad reply id from server"))

    (let ((v0 (caddr reply))
          (v1 (cadddr reply)))
 					; Check for error code:
      (let ((rcode (bitwise-and #xf (char->integer v1))))
        (if (not (zero? rcode))
            (raise-dns-error
	     (string-append
	      "error from server: " (dns-rcode->string rcode)))))

      (dns-parse-reply (positive? (bitwise-and #x4 (char->integer v0))) reply))))

(: *query-connection* (vector (struct mutex) (or false string) (or false input-port) (or false output-port)))
(define *query-connection*
  (vector (make-mutex 'dns-query) #f #f #f))

(define (query-connection-close!)
  (let ((ns (vector-ref *query-connection* 1))
	(in (vector-ref *query-connection* 2))
	(out (vector-ref *query-connection* 3)))
    (if (and ns in out)
	(with-mutex
	 (vector-ref *query-connection* 0)
	 (if (and (eq? (vector-ref *query-connection* 1) ns)
		  (eq? (vector-ref *query-connection* 2) in))
	     (begin
	       (logerr "closing dns connection to ~a\n" ns)
	       (vector-set! *query-connection* 1 #f)
	       (vector-set! *query-connection* 2 #f)
	       (vector-set! *query-connection* 3 #f)
	       (close-input-port in)
	       (close-output-port out)))))))

(define (init-query-connection nameserver)
  (if (vector-ref *query-connection* 1)
      (query-connection-close!))
  (let ((mux (vector-ref *query-connection* 0)))
    (guard
     (ex (else (mutex-unlock! mux)
	       (log-condition (format "init dns connection to ~a" nameserver) ex)
	       (raise ex)))
     (mutex-lock! mux #f #f)
     (if (not (vector-ref *query-connection* 2))
	 (receive
	  (r w)
	  (tcp-connect
	   (or nameserver
	       ;; TODO we should raise-service-unavailable-condition
	       ;; but the dns resolutio is currently compiled within
	       ;; "util" which is wrong; therefore we can not raise
	       ;; this yet undefined condition.
	       (error "DNS nameserver not configured"))
	   53)
	  (vector-set! *query-connection* 1 nameserver)
	  (vector-set! *query-connection* 2 r)
	  (vector-set! *query-connection* 3 w)))
     (mutex-unlock! mux)))
  nameserver)

;; FIXME: Read up the DNS RFC.  Is this connection cache useful at all?

(define (lock-query-connection!)
  (mutex-lock! (vector-ref *query-connection* 0) #f #f))
(define (unlock-query-connection!)
  (mutex-unlock! (vector-ref *query-connection* 0)))
(define (nothing) #t)

(define (dns-query nameserver query)
  (let ((cns (vector-ref *query-connection* 1)))
    (cond
     ((not cns)
      (init-query-connection nameserver)
      (dns-query nameserver query))
     ((equal? cns nameserver)
      (let loop ((n 2))
	(guard
	 (ex ((not (dns-error? ex))
	      (log-condition 'dns-query ex)
	      (init-query-connection nameserver)
	      (if (fx>= n 0) (loop (sub1 n)) (raise ex))))
	 (do-dns-query
	  (vector-ref *query-connection* 2)
	  (vector-ref *query-connection* 3)
	  lock-query-connection!
	  unlock-query-connection!
	  query))))
     (else
      (receive
       (r w)
       (tcp-connect
	(or nameserver
	    ;; TODO we should raise-service-unavailable-condition
	    ;; but the dns resolutio is currently compiled within
	    ;; "util" which is wrong; therefore we can not raise
	    ;; this yet undefined condition.
	    (error "DNS nameserver not configured"))
	53)
       (do-dns-query
	r w
	nothing
	(lambda ()
	  (close-input-port r)
	  (close-output-port w))
	query))))))

(define cache (the * #f))

(: *nameserver* *)
(define *nameserver* (make-mutex 'nameserver))

(define (set-dns-nameserver! str)
  (set! *nameserver* str))

(define (dns-flush-cache)
  (set! cache (make-symbol-table))
  (set! *nameserver* (make-mutex 'nameserver)))

(dns-flush-cache)

(define (dns-query/cache nameserver addr type class)
  (let ((key (string->symbol (format "~a;~a;~a;~a" nameserver addr type class))))
    (let ((v (hash-table-ref/default cache key #f)))
      (if v
          (list->values v)
          (receive
           (auth? qds ans nss ars reply)
	   (dns-query nameserver (make-query (random 256) addr type class))
           (hash-table-set! cache key (list auth? qds ans nss ars reply))
           (values auth? qds ans nss ars reply))))))

(define (ip->string s)
  (format "~a.~a.~a.~a" 
          (char->integer (list-ref s 0))
          (char->integer (list-ref s 1))
          (char->integer (list-ref s 2))
          (char->integer (list-ref s 3))))

; (define (ormap f lst)
;   (and (pair? lst) (let ((x (f (car lst)))) (or x (ormap f (cdr lst))))))

(define (try-forwarding k nameserver)
  (let loop ((nameserver nameserver)(tried (list nameserver)))
    ;; Normally the recusion is done for us, but it's technically optional
    (receive(v ars auth?) (k nameserver)
            (or v
                (and (not auth?)
                     (let* ((ns (ormap
                                 (lambda (ar)
                                   (and (eq? (rr-type ar) 'a)
                                        (ip->string (rr-data ar))))
                                 ars)))
                       (and ns
                            (not (member ns tried))
                            (loop ns (cons ns tried)))))))))

(define ip-match
  (let ((match (regexp "([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+).*")))
    (define (ip-match x)
      (string-match match x))
    ip-match))

(define (ip->in-addr.arpa ip)
  (let ((result (ip-match ip)))
    (and result (apply format "~a.~a.~a.~a.in-addr.arpa"
		       (reverse! (cdr result))))))

(define (ip->dns-ip ip)
  (let ((result (ip-match ip)))
    (and result (map (lambda (i) (integer->char (string->number i))) (cdr result)))))

(define get-ptr-list-from-ans
  (lambda (ans)
    (filter (lambda (ans-entry)
              (eq? (list-ref ans-entry 1) 'ptr))
            ans)))

(define get-name 
  (lambda (nameserver ip)
    (or (and (string=? ip "127.0.0.1") "localhost") ; save work and worries
	(try-forwarding
         (lambda (nameserver)
           (receive
            (auth? qds ans nss ars reply)
            (dns-query/cache nameserver (ip->in-addr.arpa ip) 'ptr 'in)
            (values (and (positive? (length (get-ptr-list-from-ans ans)))
                         (let ((s (rr-data (car (get-ptr-list-from-ans ans)))))
                           (receive (name rest) (parse-name s reply) name)))
                    ars auth?)))
         nameserver)
        (raise-dns-error (string-append "dns-get-name: bad ip address: " ip)))))

(define get-a-list-from-ans
  (lambda (ans)
    (filter (lambda (ans-entry)
              (eq? (list-ref ans-entry 1) 'a))
            ans)))

(define (get-address nameserver addr)
  (or (try-forwarding
       (lambda (nameserver)
         (receive
          (auth? qds ans nss ars reply)
          (dns-query/cache nameserver addr 'a 'in)
          (values (let ((a-list (get-a-list-from-ans ans)))
		    (and (positive? (length a-list))
			 (let ((s (rr-data (car a-list))))
			   (ip->string s))))
                  ars auth?)))
       nameserver)
      (raise-dns-error (string-append "dns-get-address: bad address" addr))))

(define (get-addresses nameserver addr)
  (or (try-forwarding
       (lambda (nameserver)
         (receive
          (auth? qds ans nss ars reply)
          (dns-query/cache nameserver addr 'a 'in)
          (values (map (lambda (e) (ip->string (rr-data e))) (get-a-list-from-ans ans))
                  ars auth?)))
       nameserver)
      (raise-dns-error (string-append "dns-get-address: bad address" addr))))

(define (set-address! nameserver name addr)
  (let ((zone (string-index name #\.)))
    (dns-flush-cache)
    (and zone
	 (dns-query
	  nameserver
	  (make-update-call
	   (if (string-index name #\. (add1 zone))
	       (substring name (add1 zone) (string-length name))
	       name)
	   'in `((delete-rrset ,name a)
		 (add ,name a 3600 ,addr)))))))

(define (delete-address! nameserver name old)
  (dns-flush-cache)
  (let ((zone (string-index name #\.)))
    (if zone
	(dns-query
	 nameserver
	 (make-update-call
	  (if (string-index name #\. (add1 zone))
	      (substring name (add1 zone) (string-length name))
	      name)
	  'in `((delete-rr ,name a ,old))))
	(raise-dns-error (format "dns:delete-address no zone in ~a" name)))))

(define (add-address! nameserver name addr)
  (dns-flush-cache)
  (let ((zone (string-index name #\.)))
    (if zone
	(dns-query
	 nameserver
	 (make-update-call
	  (if (string-index name #\. (add1 zone))
	      (substring name (add1 zone) (string-length name))
	      name) 'in `((add ,name a 3600 ,addr))))
	(raise-dns-error (format "dns:add-address no zone in ~a" name)))))

(define plus-infinite 70000)
(define (get-mail-exchanger nameserver addr)
  (or (try-forwarding
       (lambda (nameserver)
         (receive
          (auth? qds ans nss ars reply)
          (dns-query/cache nameserver addr 'mx 'in)
          (values (let loop ((ans ans)(best-pref plus-infinite)(exchanger #f))
                    (cond
                     ((null? ans)
                      (or exchanger
                          ;; Does 'soa mean that the input address is fine?
                          (and (ormap
                                (lambda (ns) (eq? (rr-type ns) 'soa))
                                nss)
                               addr)))
                     (else
                      (let ((d (rr-data (car ans))))
                        (let ((pref (octet-pair->number (car d) (cadr d))))
                          (if (< pref best-pref)
                              (receive(name start) (parse-name (cddr d) reply)
                                      (loop (cdr ans) pref name))
                              (loop (cdr ans) best-pref exchanger)))))))
                  ars auth?)))
       nameserver)
      (raise-dns-error (string-append "dns-get-mail-exchanger: bad address" addr))))

(define (get-mail-exchangers nameserver addr)
  (or (try-forwarding
       (lambda (nameserver)
         (receive
          (auth? qds ans nss ars reply)
          (dns-query/cache nameserver addr 'mx 'in)
          (values (map cdr
		       (sort
			(map
			 (lambda (ans)
			   (let ((d (rr-data ans)))
			     (receive(name start) (parse-name (cddr d) reply)
				     (cons (octet-pair->number (car d) (cadr d))
					   name))))
			 ans)
			(lambda (a b) (< (car a) (car b)))))
                  ars auth?)))
       nameserver)
      (raise-dns-error (string-append "dns-get-mail-exchanger: bad address" addr))))

(define (dns-find-nameserver*)
  (guard
   (ex (else #f))
   (call-with-input-file "/etc/resolv.conf"
     (lambda (port)
       (let loop ()
         (let ((l (read-line port)))
           (or (and (string? l)
                    (let ((m (string-match
                              etc-resolve.conf-nameserver-line-regex l)))
                      (and m (cadr m))))
               (and (not (eof-object? l))
                    (loop)))))))))
(define (find-nameserver)
  (if (mutex? *nameserver*)
      (with-mutex
       *nameserver*
       (if (mutex? *nameserver*)
	   (begin
	     (set! *nameserver* (dns-find-nameserver*))
	     ;;(if *nameserver* (init-query-connection *nameserver*))
	     )
	   *nameserver*))
      *nameserver*))

) ;; module dns

(import (prefix dns dns:))
(define set-dns-nameserver! dns:set-dns-nameserver!)
(define dns-get-address dns:get-address)
(define dns-get-addresses dns:get-addresses)
(define dns-set-address! dns:set-address!)
(define dns-delete-address! dns:delete-address!)
(define dns-add-address! dns:add-address!)
(define dns-get-name dns:get-name)
(define dns-get-mail-exchanger dns:get-mail-exchanger)
(define dns-get-mail-exchangers dns:get-mail-exchangers)
(define dns-find-nameserver dns:find-nameserver)
(define ip-match dns:ip-match)
