;; (C) 2002, 2010 Jörg F. Wittenberger

(declare
 (unit storage-fsm)
 ;;
 (usual-integrations))

(module
 storage-fsm
 (
  fsm-user-sqlite-file
  nufsm-write nufsm-read
  nufsm-dump
  ;;
  zip-system-core
  ;;
  ;; internal to storage
  ;;
  $fsm-precise
  $fsm-blob-paranoia
  make-fsm-component
  ;;
  fsm-display-blob-notation fsm-fetch-blob-notation
  fsm-blob-notation-size fsm-store-blob-notation
  fsm-store-blob-value
  fsm-copy-blob-value
  ;;
  read-rdf!
  nufsm-commit-changes nufsm-commit-all
  )

(import scheme
	(except chicken add1 sub1 condition? promise? with-exception-handler)
	srfi-1
	srfi-13				; only string-null?
	(except srfi-18 raise)
	srfi-19 srfi-34 srfi-35
	shrdprmtr pcre util atomic parallel cache ports structures tree
	openssl notation function bhob aggregate storage-api place-common pool corexml
	files
	(except posix file-close create-pipe duplicate-fileno process-wait process-signal)
	extras)

(import (only timeout *system-time*))
(import (only place make-look-ahead-reader))

(import storage-common storage-sql)

(include "typedefs.scm")

(define-inline (make-sure-string-is-there data l) #f)

(include "../mechanism/storage/fsm.scm")

)

(import (prefix storage-fsm m:))
(define $fsm-precise m:$fsm-precise)
(define $fsm-blob-paranoia m:$fsm-blob-paranoia)
