;; (C) 2013 Joerg F. Wittenberger see http://www.askemos.org

(declare
 (unit shrdprmtr)
 (usual-integrations)
 ;; required
 (disable-interrupts)	       ; atomic updates to globals
 ;; promises
 (strict-types)
 ;; (block)
)

(module
 shrdprmtr
 (export make-shared-parameter)
 (import (except scheme vector-ref))
 (import chicken)
 (import (only srfi-18 current-thread))
 (import (only srfi-43 vector-copy))

(define-syntax vector-ref (syntax-rules () ((_ x i) (##core#inline "C_i_block_ref" x i))))

(: make-shared-parameter (* #!optional procedure -> procedure))

(define make-shared-parameter
  (let ((count 0)
	(defaults '#())
	(vals (make-parameter '(#f . #())))
	(undefined '(undefined)))
    (lambda (init #!optional (sane (lambda (x) x)))
      (let* ((val (sane init))
	     (i count))
	(set! count (fx+ count 1))
	(when (fx>= i (vector-length defaults))
	  (set! defaults (vector-resize defaults (fx+ i 1) undefined) ) )
	(vector-set! defaults i val)
	(lambda arg
	  (let* ((e (vals))
		 (current (cdr e))
		 (n (vector-length current)))
	    (if (pair? arg)
		(if (null? (cdr arg)) ;;  direct call
		    (let ((newval (sane (car arg))))
		      (if (or (fx>= i n)
			      (eq? (vector-ref current i) undefined))
			  (vector-set! defaults i newval)
			  (vector-set! current i newval))
		      newval)
		    ;; call from parameterize (or manual fake bypassing API)
		    (let* ((convert? (cadr arg))
			   (set? (caddr arg))
			   (newval
			    ;; Restore call as per chicken's internals. Therefore
			    ;; we know/"take for granted" it's local anyway.
			    (if convert? (sane (car arg)) (car arg))))
		      (cond
		       ((fx>= i n)
			(set! current (vector-resize current (fx+ i 1) undefined) )
			(vals (cons (current-thread) current)))
		       ((not (eq? (car e) (current-thread)))
			(set! current (vector-copy current 0 (vector-length current)) )
			(vals (cons (current-thread) current))))
		      (if set? (vector-set! current i newval))
		      newval))
		(cond
		 ((fx>= i n) (vector-ref defaults i))
		 (else (let ((val (vector-ref current i)))
			 (if (eq? val undefined)
			     (vector-ref defaults i) 
			     val))))) ) ) ) ) ) )

)
