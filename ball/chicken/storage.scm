;; (C) 2002, 2010 J�rg F. Wittenberger

;; chicken module clause for the Askemos storage module.

(declare
 (unit storage)
 ;;
 (usual-integrations))

(module
 storage
 (
  in-core-storage-adaptor
  pstore-storage-adaptor
  pstore-open-mind compact-database
  pstore-register-class!
  fsm-storage-adaptor fsm-delay
  fsm-user-sqlite-file
  nufsm-write nufsm-read
  nufsm-dump nufsm-restore nufsm-gc
  $max-replication-retries
  http-find-quorum http-rerequest-digests!
  http-replicate-blob!
  http-storage-adaptor http-replicate-place! http-read! http-sync!

  fsm-remote-timeout fsm-delay
  init-sql-core!
  ;;
  spool-db-select spool-db-exec
  lockable-db-connection *spool-db* init-spool-db! check-spool-db! close-spool-db! db-set!
  zip-system-core
  )

(import scheme
	(except chicken add1 sub1 with-exception-handler condition? promise?)
	srfi-19 srfi-34 srfi-35
	ports util atomic timeout cache
	openssl tree notation bhob aggregate)
(import storage-api place)

(import storage-common storage-sql storage-fsm storage-gc storage-remote)

(import (only lolevel move-memory!))

(import (only parallel delay*))

(define (identity x) x)

(define (pstore-register-class! . classes) #f)

(define (pstore-open-mind mind-file-name . create) ; EXPORT
  (error "pstore-open-mind not implemented."))

(define (compact-database set-working-mind! mind)
  ;; (set-working-mind! mind)
  #f)

(define (commit args)
  (error "commit not implemented."))

(define (pstore-commit-into mind-file-name mind)
  (error "pstore-commit-into not implemented."))

(define (pstore-close mind)
  (error "pstore-close not implemented."))

(define pstore-storage-adaptor
  (make-storage-adaptor open:        pstore-open-mind
                        commit:      commit
                        commit-into: pstore-commit-into
                        close:       pstore-close))

(include "../mechanism/storage/incore.scm")
)

(import (prefix storage m:))

(define in-core-storage-adaptor m:in-core-storage-adaptor)
(define pstore-storage-adaptor m:pstore-storage-adaptor)
(define compact-database m:compact-database)
(define fsm-storage-adaptor m:fsm-storage-adaptor)
(define fsm-delay m:fsm-delay)
(define fsm-user-sqlite-file m:fsm-user-sqlite-file)
(define nufsm-write m:nufsm-write)
(define nufsm-read m:nufsm-read)
(define nufsm-dump m:nufsm-dump)
(define nufsm-restore m:nufsm-restore)
(define nufsm-gc m:nufsm-gc)
(define $max-replication-retries m:$max-replication-retries)
(define http-find-quorum m:http-find-quorum)
(define http-rerequest-digests! m:http-rerequest-digests!)
(define http-storage-adaptor m:http-storage-adaptor)
(define http-replicate-place! m:http-replicate-place!)
(define http-read! m:http-read!)
(define http-sync! m:http-sync!)

(define fsm-remote-timeout m:fsm-remote-timeout)
(define fsm-delay m:fsm-delay)

(define zip-system-core m:zip-system-core)
(define init-sql-core! m:init-sql-core!)
(define spool-db-select m:spool-db-select)
(define spool-db-exec m:spool-db-exec)
;;
(import (prefix files files:))
(define make-pathname files:make-pathname)
