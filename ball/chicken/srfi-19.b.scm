;; (C) 2013 Jörg F. Wittenberger

;; The binding modules for SRFI-19.

(declare (unit srfi-19))

(module
 srfi-19
 (
  ;;
  ;; srfi19
  ;;
  add-german-word
  ;;
  tm:iso-8601-date-time-format ;; tm:current-time-ms-time 
  ;;
  read-leap-second-table
  make-time
  time-type time-second time-nanosecond
  srfi19:time?
  copy-time
  srfi19:current-time time-resolution
  srfi19:time=? srfi19:time>? srfi19:time<? srfi19:time>=? srfi19:time<=?
  time-difference add-duration subtract-duration
  time-tai->time-utc time-utc->time-tai
  time-monotonic->time-utc time-monotonic->time-tai
  time-utc->date time-tai->date time-monotonic->date
  date->time-utc date->time-tai date->time-monotonic
  date-year-day
  make-date srfi19:date?
  date-zone-offset date-year date-month date-day date-hour
  date-minute date-second date-nanosecond
  leap-year?
  date-year-day date-week-day date-week-number
  ;;
  date->julian-day date->modified-julian-day
  time-utc->julian-day time-utc->modified-julian-day
  time-tai->julian-day time-tai->modified-julian-day time-tai->time-monotonic
  time-monotonic->julian-day time-monotonic->modified-julian-day
  julian-day->time-utc julian-day->time-tai julian-day->time-monotonic
  julian-day->date modified-julian-day->date
  modified-julian-day->time-utc modified-julian-day->time-tai
  modified-julian-day->time-monotonic
  current-date current-julian-day current-modified-julian-day
  srfi19:date->string srfi19:string->date
  ;;
  literal-time-second
  )

(import scheme chicken)

(include "typedefs.scm")

(include "srfi-19-types.scm")

;;
(: literal-time-second (number --> string))

(define add-german-word #f)
(define tm:iso-8601-date-time-format #f)
(define read-leap-second-table #f)
(define make-time #f)
(define time-type #f)
(define time-second #f)
(define time-nanosecond #f)
(define srfi19:time? #f)
(define copy-time #f)
(define srfi19:current-time #f)
(define time-resolution #f)
(define srfi19:time=? #f)
(define srfi19:time>? #f)
(define srfi19:time<? #f)
(define srfi19:time>=? #f)
(define srfi19:time<=? #f)
(define time-difference #f)
(define add-duration #f)
(define subtract-duration #f)
(define time-tai->time-utc #f)
(define time-utc->time-tai #f)
(define time-monotonic->time-utc #f)
(define time-monotonic->time-tai #f)
(define time-utc->date #f)
(define time-tai->date #f)
(define time-monotonic->date #f)
(define make-date #f)
(define srfi19:date? #f)
(define date-zone-offset #f)
(define date-year #f)
(define date-month #f)
(define date-day #f)
(define date-hour #f)
(define date-minute #f)
(define date-second #f)
(define date-nanosecond #f)
(define leap-year? #f)
(define date-year-day #f)
(define date-week-day #f)
(define date-week-number #f)
(define date->time-utc #f)
(define date->julian-day #f)
(define date->modified-julian-day #f)
(define date->time-tai #f)
(define date->time-monotonic #f)
(define time-utc->julian-day #f)
(define time-utc->modified-julian-day #f)
(define time-tai->julian-day #f)
(define time-tai->modified-julian-day #f)
(define time-tai->time-monotonic #f)
(define time-monotonic->julian-day #f)
(define time-monotonic->modified-julian-day #f)
(define julian-day->time-utc #f)
(define julian-day->time-tai #f)
(define julian-day->time-monotonic #f)
(define julian-day->date #f)
(define modified-julian-day->date #f)
(define modified-julian-day->time-utc #f)
(define modified-julian-day->time-tai #f)
(define modified-julian-day->time-monotonic #f)
(define current-date #f)
(define current-julian-day #f)
(define current-modified-julian-day #f)
(define srfi19:date->string #f)
(define srfi19:string->date #f)
(define literal-time-second #f)

)

;; setters



(module
 b-srfi-19
 (
  ;;
  ;; srfi19
  ;;
  %!set-add-german-word!
  ;;
  %!set-tm:iso-8601-date-time-format! ;; %!set-tm:current-time-ms-time! 
  ;;
  %!set-read-leap-second-table!
  %!set-make-time!
  %!set-time-type! %!set-time-second! %!set-time-nanosecond!
  %!set-srfi19:time?!
  %!set-copy-time!
  %!set-srfi19:current-time! %!set-time-resolution!
  %!set-srfi19:time=?! %!set-srfi19:time>?! %!set-srfi19:time<?! %!set-srfi19:time>=?! %!set-srfi19:time<=?!
  %!set-time-difference! %!set-add-duration! %!set-subtract-duration!
  %!set-time-tai->time-utc! %!set-time-utc->time-tai!
  %!set-time-monotonic->time-utc! %!set-time-monotonic->time-tai!
  %!set-time-utc->date! %!set-time-tai->date! %!set-time-monotonic->date!
  %!set-date->time-utc! %!set-date->time-tai! %!set-date->time-monotonic!
  %!set-date-year-day!
  %!set-make-date! %!set-srfi19:date?!
  %!set-date-zone-offset! %!set-date-year! %!set-date-month! %!set-date-day! %!set-date-hour!
  %!set-date-minute! %!set-date-second! %!set-date-nanosecond!
  %!set-leap-year?!
  %!set-date-year-day! %!set-date-week-day! %!set-date-week-number!
  ;;
  %!set-date->julian-day! %!set-date->modified-julian-day!
  %!set-time-utc->julian-day! %!set-time-utc->modified-julian-day!
  %!set-time-tai->julian-day! %!set-time-tai->modified-julian-day! %!set-time-tai->time-monotonic!
  %!set-time-monotonic->julian-day! %!set-time-monotonic->modified-julian-day!
  %!set-julian-day->time-utc! %!set-julian-day->time-tai! %!set-julian-day->time-monotonic!
  %!set-julian-day->date! %!set-modified-julian-day->date!
  %!set-modified-julian-day->time-utc! %!set-modified-julian-day->time-tai!
  %!set-modified-julian-day->time-monotonic!
  %!set-current-date! %!set-current-julian-day! %!set-current-modified-julian-day!
  %!set-srfi19:date->string! %!set-srfi19:string->date!
  ;;
  %!set-literal-time-second!
  )

(import scheme chicken (prefix srfi-19 s19:))


(define (%!set-add-german-word! p) (set! s19:add-german-word p))
(define (%!set-tm:iso-8601-date-time-format! p) (set! s19:tm:iso-8601-date-time-format p))
(define (%!set-read-leap-second-table! p) (set! s19:read-leap-second-table p))
(define (%!set-make-time! p) (set! s19:make-time p))
(define (%!set-time-type! p) (set! s19:time-type p))
(define (%!set-time-second! p) (set! s19:time-second p))
(define (%!set-time-nanosecond! p) (set! s19:time-nanosecond p))
(define (%!set-srfi19:time?! p) (set! s19:srfi19:time? p))
(define (%!set-copy-time! p) (set! s19:copy-time p))
(define (%!set-srfi19:current-time! p) (set! s19:srfi19:current-time p))
(define (%!set-time-resolution! p) (set! s19:time-resolution p))
(define (%!set-srfi19:time=?! p) (set! s19:srfi19:time=? p))
(define (%!set-srfi19:time>?! p) (set! s19:srfi19:time>? p))
(define (%!set-srfi19:time<?! p) (set! s19:srfi19:time<? p))
(define (%!set-srfi19:time>=?! p) (set! s19:srfi19:time>=? p))
(define (%!set-srfi19:time<=?! p) (set! s19:srfi19:time<=? p))
(define (%!set-time-difference! p) (set! s19:time-difference p))
(define (%!set-add-duration! p) (set! s19:add-duration p))
(define (%!set-subtract-duration! p) (set! s19:subtract-duration p))
(define (%!set-time-tai->time-utc! p) (set! s19:time-tai->time-utc p))
(define (%!set-time-utc->time-tai! p) (set! s19:time-utc->time-tai p))
(define (%!set-time-monotonic->time-utc! p) (set! s19:time-monotonic->time-utc p))
(define (%!set-time-monotonic->time-tai! p) (set! s19:time-monotonic->time-tai p))
(define (%!set-time-utc->date! p) (set! s19:time-utc->date p))
(define (%!set-time-tai->date! p) (set! s19:time-tai->date p))
(define (%!set-time-monotonic->date! p) (set! s19:time-monotonic->date p))
(define (%!set-make-date! p) (set! s19:make-date p))
(define (%!set-srfi19:date?! p) (set! s19:srfi19:date? p))
(define (%!set-date-zone-offset! p) (set! s19:date-zone-offset p))
(define (%!set-date-year! p) (set! s19:date-year p))
(define (%!set-date-month! p) (set! s19:date-month p))
(define (%!set-date-day! p) (set! s19:date-day p))
(define (%!set-date-hour! p) (set! s19:date-hour p))
(define (%!set-date-minute! p) (set! s19:date-minute p))
(define (%!set-date-second! p) (set! s19:date-second p))
(define (%!set-date-nanosecond! p) (set! s19:date-nanosecond p))
(define (%!set-leap-year?! p) (set! s19:leap-year? p))
(define (%!set-date-year-day! p) (set! s19:date-year-day p))
(define (%!set-date-week-day! p) (set! s19:date-week-day p))
(define (%!set-date-week-number! p) (set! s19:date-week-number p))
(define (%!set-date->time-utc! p) (set! s19:date->time-utc p))
(define (%!set-date->julian-day! p) (set! s19:date->julian-day p))
(define (%!set-date->modified-julian-day! p) (set! s19:date->modified-julian-day p))
(define (%!set-date->time-tai! p) (set! s19:date->time-tai p))
(define (%!set-date->time-monotonic! p) (set! s19:date->time-monotonic p))
(define (%!set-time-utc->julian-day! p) (set! s19:time-utc->julian-day p))
(define (%!set-time-utc->modified-julian-day! p) (set! s19:time-utc->modified-julian-day p))
(define (%!set-time-tai->julian-day! p) (set! s19:time-tai->julian-day p))
(define (%!set-time-tai->modified-julian-day! p) (set! s19:time-tai->modified-julian-day p))
(define (%!set-time-tai->time-monotonic! p) (set! s19:time-tai->time-monotonic p))
(define (%!set-time-monotonic->julian-day! p) (set! s19:time-monotonic->julian-day p))
(define (%!set-time-monotonic->modified-julian-day! p) (set! s19:time-monotonic->modified-julian-day p))
(define (%!set-julian-day->time-utc! p) (set! s19:julian-day->time-utc p))
(define (%!set-julian-day->time-tai! p) (set! s19:julian-day->time-tai p))
(define (%!set-julian-day->time-monotonic! p) (set! s19:julian-day->time-monotonic p))
(define (%!set-julian-day->date! p) (set! s19:julian-day->date p))
(define (%!set-modified-julian-day->date! p) (set! s19:modified-julian-day->date p))
(define (%!set-modified-julian-day->time-utc! p) (set! s19:modified-julian-day->time-utc p))
(define (%!set-modified-julian-day->time-tai! p) (set! s19:modified-julian-day->time-tai p))
(define (%!set-modified-julian-day->time-monotonic! p) (set! s19:modified-julian-day->time-monotonic p))
(define (%!set-current-date! p) (set! s19:current-date p))
(define (%!set-current-julian-day! p) (set! s19:current-julian-day p))
(define (%!set-current-modified-julian-day! p) (set! s19:current-modified-julian-day p))
(define (%!set-srfi19:date->string! p) (set! s19:srfi19:date->string p))
(define (%!set-srfi19:string->date! p) (set! s19:srfi19:string->date p))
(define (%!set-literal-time-second! p) (set! s19:literal-time-second p))

)
