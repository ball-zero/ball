(declare
 (unit timeout))

(module
 timeout
 (
  timeout-handling-policy
  register-timeout-message!
  register-time-procedure!
  cancel-timeout-message!
  thread-signal-timeout!
  timeout-object?
  within-time within-time! within-time%1
  with-timeout
  with-timeout!
  make-watchdog!
  check-watchdog!
  update-system-time!
  send-alive-signal
  *system-time*
  timestamp
  respond-timeout-interval
  remote-fetch-timeout-interval
  display-stop-list
  timeout-second
  ;;
  timeout-object
  )
 

 (import scheme chicken)

 (include "typedefs.scm")
 (include "timeout-types.scm")

 (define *local-timeout-symbol* '(timeout))

 (define (timeout-object) *local-timeout-symbol*)

 (: timeout deprecated)

 (define timeout-handling-policy #f)
 (define register-timeout-message! #f)
 (define register-time-procedure! #f)
 (define cancel-timeout-message! #f)
 (define thread-signal-timeout! #f)
 ;; (define timeout-object? #f)
 (define (timeout-object? obj)
   (eq? *local-timeout-symbol* obj))
 (define timeout #f)
 (define with-timeout #f)
 (define with-timeout! #f)
 (define with-timeout! #f)
 (define make-watchdog! #f)
 (define check-watchdog! #f)
 (define update-system-time! #f)
 (define send-alive-signal #f)
 (define *system-time* #f)
 (define timestamp #f)
 (define respond-timeout-interval #f)
 (define remote-fetch-timeout-interval #f)
 (define display-stop-list #f)
 (define timeout-second #f)
 

#;(define (get-line-number sexp)
  (and ##sys#line-number-database
       (pair? sexp)
       (let ([head (car sexp)])
	 (and (symbol? head)
	      (cond [(##sys#hash-table-ref ##sys#line-number-database head)
		     => (lambda (pl)
			  (let ([a (assq sexp pl)])
			    (and a (cdr a)) ) ) ]
		    [else #f] ) ) ) ) )

(define-syntax within-time
   (syntax-rules ()
     ((_ timeout body ...) (with-timeout timeout (lambda () body ...)))))

(define-syntax within-time%1
   (syntax-rules ()
     ((_ timeout body ...)
      (let* ((handle (register-timeout-message! timeout (current-thread)))
	     (result (begin body ...)))
	(cancel-timeout-message! handle)
	result))))

#;(: with-timeout ((or boolean :timeout:) :thunk: #!optional * -> . *))
#;(define-syntax within-time
  (er-macro-transformer
   (lambda (form r c)
     (let ((tmo (cadr form))
	   (body (cddr form))
	   (ln (get-line-number form)))
       `(with-timeout ,tmo (lambda () . ,body) ,ln)))))

(define-syntax within-time!
  (syntax-rules () ((_ timeout body ...) (with-timeout! timeout (lambda () body ...)))))

 )

(module
 b-timeout
 (
  %!timeout-handling-policy
  %!register-timeout-message!
  %!register-time-procedure!
  %!cancel-timeout-message!
  %!thread-signal-timeout!
  %!timeout-object?
  %!timeout
  %!with-timeout
  %!with-timeout!
  %!make-watchdog!
  %!check-watchdog!
  %!update-system-time!
  %!send-alive-signal
  %!*system-time*
  %!timestamp
  %!respond-timeout-interval
  %!remote-fetch-timeout-interval
  %!display-stop-list
  %!timeout-second
  )
 (import scheme (prefix timeout %:))


(define (%!timeout-handling-policy x) (set! %:timeout-handling-policy x))
(define (%!register-timeout-message! x) (set! %:register-timeout-message! x))
(define (%!register-time-procedure! x) (set! %:register-time-procedure! x))
(define (%!cancel-timeout-message! x) (set! %:cancel-timeout-message! x))
(define (%!thread-signal-timeout! x) (set! %:thread-signal-timeout! x))
(define (%!timeout-object? x) (set! %:timeout-object? x))
(define (%!timeout x) (set! %:timeout x))
(define (%!with-timeout x) (set! %:with-timeout x))
(define (%!with-timeout! x) (set! %:with-timeout! x))
(define (%!make-watchdog! x) (set! %:make-watchdog! x))
(define (%!check-watchdog! x) (set! %:check-watchdog! x))
(define (%!update-system-time! x) (set! %:update-system-time! x))
(define (%!send-alive-signal x) (set! %:send-alive-signal x))
(define (%!*system-time* x) (set! %:*system-time* x))
(define (%!timestamp x) (set! %:timestamp x))
(define (%!respond-timeout-interval x) (set! %:respond-timeout-interval x))
(define (%!remote-fetch-timeout-interval x) (set! %:remote-fetch-timeout-interval x))
(define (%!display-stop-list x) (set! %:display-stop-list x))
(define (%!timeout-second x) (set! %:timeout-second x))

)
