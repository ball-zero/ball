;; (C) 2013 Jörg Wittenberger - released into public domain

(define-syntax receive/values-syntax
  (syntax-rules ()
    ((_ bindings expr body ...)
     (let ((receiver (lambda bindings body ...)))
       (let-syntax
	   ((rewrite (syntax-rules ()
		       ((_ (*values *values/syntax *list->values *begin) *expr)
			(letrec-syntax
			    ((*values (syntax-rules <...> ()
						    ((_ rv <...>) (receiver rv <...>))))
			     (*values/syntax (syntax-rules <...> ()
						    ((_ rv <...>) (receiver rv <...>))))
			     (*begin (syntax-rules <...> ()
						   ((_ (f a <...>)) (f receiver a <...>))))
			     (*list->values
			      (syntax-rules ()
				((_ list)
				 (*list->values bindings list ()))
				((_ "reverse" res ())
				 (receiver . res))
				((_ "reverse" res (x . tail))
				 (*list->values "reverse" (x . res) tail))
				((_ () list vals)
				 (*list->values "reverse" () vals))
				((_ (x . m) lst vals)
				 (let ((x (car lst)) (r (cdr lst)))
				   (*list->values m r (x . vals)))))))
			  *expr)))))
	 (extract (values values/syntax list->values begin) expr (rewrite () expr)))))))

(define-syntax define/rewrite-values
  (syntax-rules ()
    ((_ (name . formals) body)
     (define (name receiver . formals)
       (let-syntax
	   ((rewrite (syntax-rules ()
		       ((_ (*values) body_)
			(let-syntax
			    ((*values (syntax-rules () ((_ . vals) (receiver . vals)))))
			  body_)))))
	 (extract (values) body (rewrite () body)))))))
