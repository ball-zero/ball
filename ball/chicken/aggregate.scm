;; (C) 2002, 2008 Jörg F. Wittenberger

(declare
 (unit aggregate)
 ;;
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (usual-integrations)
 )

(module
 aggregate
 (
  ;; (with-a-ref refp refv) (pass-a-ref refp refv)
  with-a-ref pass-a-ref
  has-a-ref? print-a-refs
  ;; (currently) macros
  property property? property-name property-value make-property
  aggregate? aggregate aggregate-meta aggregate-entity
  set-aggregate-entity! set-aggregate-meta!
  aggregate-associate
  ;;
  ;; place.scm
  %replace-association %remove-association
  make-property-set properties-set properties-get
  make-frame frame-clone
  frame?
  set-slot! get-slot
  %set-slot! %get-slot
  frame-version set-frame-version!
  frame-protection set-frame-protection!
  frame-body/plain set-frame-body/plain!
  frame-content-type set-frame-content-type!
  frame-replicates set-frame-replicates!
  frame-capabilities set-frame-capabilities!
  frame-body/xml set-frame-body/xml!
  frame-reply-channel set-frame-reply-channel!
  frame-links set-frame-links!
  frame-other set-frame-other!
  fget fset!
  ;;
  make-mind-links link-bind! link-ref link-delete! fold-links fold-links-sorted links-for-each links-merge!
  ;; then "crazy" API
  make-link-folder make-link-folder-ascending frame-resolve-link
  ;; DEPRECATED.  resolve-mind-links might be removed in the future.
  resolve-mind-links
  ;;
  timeout-condition?
  dav:accept-propertyupdate
  &unauthorised &forbidden
  http-effective-condition? &http-effective-condition http-effective-condition-status
  raise-precondition-failed
  &service-unavailable service-unavailable-condition?
  raise-service-unavailable-condition
  ;; These should not be here at all!
  &agreement-timeout agreement-timeout?
  agreement-ready agreement-result agreement-digest
  agreement-required-echos agreement-uninformed agreement-required agreement-missing
  ;; debug help ONLY!
  property-set-fold
  )

(import (prefix aggregate-chicken c:))
(import (except aggregate-chicken
		make-property property? property-name property-value
		property-set-fold property-lookup
		property-delete! property-node-insert! property-set-init!))

(import (except scheme vector-fill! vector->list list->vector force delay)
	(except chicken add1 sub1 condition? promise? with-exception-handler vector-copy!)
	srfi-34 srfi-35 srfi-45)

(import srfi-1 srfi-13 srfi-43 (prefix srfi-43 srfi:)
	environments util openssl)

(import (only timeout timeout-object?))

(import protection)
(import bhob)

(import atomic)
(import tree (except srfi-18 raise) extras)				; for WebDAV HACK only!

(import (only lolevel move-memory!))

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(cond-expand
 (use-record+llrbtree-as-frame
  (define property? c:property?)
  (define property-name c:property-name)
  (define property-value c:property-value)
  (define property-set-fold c:property-set-fold)
  (define make-property c:make-property)
  (define property-lookup c:property-lookup)
  (define property-delete! c:property-delete!)
  (define property-node-insert! c:property-node-insert!)
  (define property-set-init! c:property-set-init!)
  )
 (else
  (define make-property cons)))

(include "typedefs.scm")

(include "../mechanism/aggregate-macros.scm")
(include "../mechanism/aggregate.scm")

)

(import (prefix aggregate m:))
(import (prefix aggregate-chicken c:))

(cond-expand
 (use-record+llrbtree-as-frame
  (define property m:property)
  (define property? c:property?)
  (define property-name c:property-name)
  (define property-value c:property-value)
  (define property-set-fold c:property-set-fold)
  (define make-property c:make-property)
;  (define aggregate? m:aggregate?)
;  (define aggregate m:aggregate)
;  (define aggregate-meta m:aggregate-meta)
;  (define aggregate-entity m:aggregate-entity)
  )
 (else))

(define frame? m:frame?)
(define %get-slot m:%get-slot)
(define %set-slot! m:%set-slot!)
(define %replace-association m:%replace-association)
(define %remove-association m:%remove-association)
(define frame-version m:frame-version)
(define set-frame-version! m:set-frame-version!)
(define frame-protection m:frame-protection)
(define set-frame-protection! m:set-frame-protection!)
(define frame-body/plain m:frame-body/plain)
(define set-frame-body/plain! m:set-frame-body/plain!)
(define frame-content-type m:frame-content-type)
(define set-frame-content-type! m:set-frame-content-type!)
(define frame-replicates m:frame-replicates)
(define set-frame-replicates! m:set-frame-replicates!)
(define frame-capabilities m:frame-capabilities)
(define set-frame-capabilities! m:set-frame-capabilities!)
(define frame-body/xml m:frame-body/xml)
(define set-frame-body/xml! m:set-frame-body/xml!)
(define frame-reply-channel m:frame-reply-channel)
(define set-frame-reply-channel! m:set-frame-reply-channel!)
(define frame-links m:frame-links)
(define set-frame-links! m:set-frame-links!)
(define frame-other m:frame-other)
(define set-frame-other! m:set-frame-other!)
(define properties-set m:properties-set)
(define properties-get m:properties-get)

(define make-mind-links m:make-mind-links)
(define link-bind! m:link-bind!)
(define link-ref m:link-ref)
(define link-delete! m:link-delete!)
(define fold-links m:fold-links)
(define fold-links-sorted m:fold-links-sorted)
(define links-for-each m:links-for-each)
(define links-merge! m:links-merge!)
