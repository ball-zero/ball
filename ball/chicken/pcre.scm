;; (C) 2001, 2008, 2013 Joerg F. Wittenberger see http://www.askemos.org

(declare
  (unit pcre)
  (uses memoize)
  (usual-integrations)
  (disable-interrupts)
;  (generic) ; PCRE options use lotsa bits
;  (disable-warning var)
#;  (bound-to-procedure
    ;; Forward reference
    regex-chardef-table? make-anchored-pattern
    ;; Imports
    get-output-string open-output-string
    string->list list->string string-length string-ref substring make-string string-append
    reverse list-ref
    char=? char-alphabetic? char-numeric? char->integer
    set-finalizer!
    ##sys#slot ##sys#setslot ##sys#size
    ##sys#make-structure ##sys#structure?
    ##sys#error ##sys#signal-hook
    ##sys#substring ##sys#fragments->string ##sys#make-c-string ##sys#string-append
    ##sys#write-char-0 ) )

(module
 pcre
 (
  pcre-compile
  pcre->proc pcre->aproc
  pcre/a->proc-uncached
  make-pcre-tokeniser
  )

 (import
  scheme
  (except chicken add1 sub1 with-exception-handler condition? promise?)
  foreign (except srfi-18 raise) srfi-34 srfi-35
  lalrdrv memoize
  extras
  )
 
#>
#include "pcre.h"

static const char *C_regex_error;
static int C_regex_error_offset;
<#

(define (C_regex_error) (foreign-value "C_regex_error" c-string))

(define (C_regex_error_offset) (foreign-value "C_regex_error_offset" int))

(define-foreign-type <pcre> (c-pointer "pcre"))

(define-condition-type &pcre-error &message
   pcre-error?
   (offset pcre-error-offset))

(define %pcre-compile
  (foreign-lambda*
   <pcre>
   ((c-string patt))
   "C_return(pcre_compile(patt, PCRE_UTF8 /* |PCRE_NO_UTF8_CHECK */, &C_regex_error, &C_regex_error_offset, NULL));"))

(define pcre-free
  (foreign-lambda void "pcre_free" c-pointer) )

(define (pcre-compile patt)
  (let ((rx (%pcre-compile patt)))
    (if rx
	(begin
	  (set-finalizer! rx pcre-free)
	  rx)
	(raise
	 #;(condition (&pcre-error (message (C_regex_error)) (offset (C_regex_error_offset))))
	 (make-property-condition 'exn 'message (C_regex_error)	'location patt 'arguments (list (C_regex_error) patt (C_regex_error_offset)))
	 ))))

;;; Captured results vector:

;; Match positions vector (PCRE ovector)

#>
#define OVECTOR_LENGTH_MULTIPLE 3
#define STATIC_OVECTOR_LEN 256
static int C_regex_ovector[OVECTOR_LENGTH_MULTIPLE * STATIC_OVECTOR_LEN];
<#

;;

(define ovector-start-ref
  (foreign-lambda* int ((int i))
    "C_return(C_regex_ovector[i * 2]);") )

(define ovector-end-ref
  (foreign-lambda* int ((int i))
    "C_return(C_regex_ovector[(i * 2) + 1]);") )

;;; Gather matched result strings or positions:

(define (gather-result-positions mc cc)
  (and (fx> mc 0)
       (let loop ([i 0])
	 (cond [(fx>= i cc)
		'()]
	       [(fx>= i mc)
		(cons #f (loop (fx+ i 1)))]
	       [else
		(let ([start (ovector-start-ref i)])
		  (cons (and (fx>= start 0)
			     (cons start (ovector-end-ref i)))
			(loop (fx+ i 1)) ) ) ] ) ) ) )

(define gather-results
  (let ([substring substring])
    (lambda (str ps)
      (and ps (cons (car ps) (##sys#map (lambda (poss) (and poss (substring str (car poss) (cdr poss)))) (cdr ps))) ) ) ) )


(define %pcre-exec
  (foreign-lambda*
   integer
   (((const <pcre>) regex)
    (nonnull-scheme-pointer subject)
    (integer sl)
    (integer start)
    (integer options))
   "C_return(pcre_exec(
         regex,          /* result of pcre_compile() */
         NULL,           /* we didn't study the pattern */
         subject,        /* the subject string */
         sl,             /* the length of the subject string */
         start,          /* start at offset 0 in the subject */
         options,        /* default options */
         C_regex_ovector,           /* vector for substring information */
         STATIC_OVECTOR_LEN * OVECTOR_LENGTH_MULTIPLE));"))

(define re-match-capture-count
  (foreign-lambda* int (((const <pcre>) code) ((const c-pointer) extra))
    "int cc;"
    "pcre_fullinfo(code, extra, PCRE_INFO_CAPTURECOUNT, &cc);"
    "C_return(cc + 1);") )

(define (pcre-exec regex subject sl start options)
  (gather-results
   subject
   (gather-result-positions
    (%pcre-exec regex subject sl start options)
    (re-match-capture-count regex #f))))

(define cached-pcre-compile (memoize pcre-compile string=?))

(define (pcre-string-match match str . rest)
  (pcre-exec match str (string-length str) (if (pair? rest) (car rest) 0) 0)
  #;(let ((result (string-match-positions match str (if (pair? rest) (car rest) 0) 0)))
    (if result
	(cons
	 (cons (caar result) (cadar result))
	 (map (lambda (p) (and p (substring str (car p) (cadr p))))
	      (cdr result)))
	#f)))

(define (pcre-string-anchor-match match str . rest)
  (pcre-exec match str (string-length str) (if (pair? rest) (car rest) 0) #x10)
  #;(let ((result (string-match-positions match str (if (pair? rest) (car rest) 0) #x10)))
    (if result
	(cons
	 (cons (caar result) (cadar result))
	 (map (lambda (p) (and p (substring str (car p) (cadr p))))
	      (cdr result)))
	#f)))

#;(define pcre->proc
  (memoize
   (lambda (str)
     (let ((match (regexp str '(extended utf8))))
       (lambda (str . off) (apply pcre-string-match match str off))))
   string=?))

(define (pcre->proc regex)
  (let ((matcher (cached-pcre-compile regex)))
    (lambda (subject . rest)
      (pcre-exec matcher subject (string-length subject)
                 (or (and (pair? rest) (car rest)) 0)
                 0))))

#;(define pcre->aproc
  (memoize
   (lambda (str)
     (let ((match (regexp str '(extended utf8 anchored))))
       (lambda (str . off) (apply pcre-string-match match str off))))
   string=?))

(define (pcre->aproc regex)
  (let ((matcher (cached-pcre-compile regex)))
    (lambda (subject . rest)
      (pcre-exec matcher subject (string-length subject)
                 (or (and (pair? rest) (car rest)) 0)
                 #x10))))

#;(define (pcre/a->proc-uncached regex)
  (let ((match (pcre-compile regex '(extended utf8 anchored))))
    (lambda (str . off) (apply pcre-string-match match str off))))

(define (pcre/a->proc-uncached regex)
  (let ((matcher (pcre-compile regex)))
    (lambda (subject . rest)
      (pcre-exec matcher subject (string-length subject)
                 (or (and (pair? rest) (car rest)) 0)
                 #x10))))

(define (make-pcre-tokeniser lst)
  (let ((initial-cases (map
			(lambda (x) (cons (pcre->aproc (car x)) (cdr x)))
			lst)))
    (lambda (str)
      (let ((offset 0))
	(values
         (lambda ()
           (let loop ((cases initial-cases))
             (cond
              ((eqv? offset (string-length str)) '*eoi*)
              ((null? cases)
	       (raise (condition
		       (&pcre-error (message
				     (format #f "unknown token at pos: ~a '~a'" offset
					     (substring
					      str offset
					      (min (+ offset 30) (string-length str)))))
				    (offset offset)))))
              (else (let ((match ((caar cases) str offset)))
                      (if match
                          (begin
                            (set! offset (cdar match))
                            (if (null? (cdar cases))
                                (loop initial-cases)
                                (if (null? (cdr match))
                                    (cadar cases)
                                    (make-lexical-token
				     (cadar cases)
				     str
				     (cdr match)))))
                          (loop (cdr cases))))))))
         (lambda () offset))))))


 )
