;; (C) 2002, 2013 Jörg F. Wittenberger

;; chicken module clause for the Askemos notation module.

(declare
 (unit notation)
 (not standard-bindings vector-fill! vector->list list->vector)
 ;;
 (disable-interrupts)			; checked loops
 (strict-types)
 (usual-integrations)
 (fixnum)
)

(module
 notation
 (
  ;; iconv
  iconv-open iconv-close iconv-bytes
  ;;
  dsssl-xml-format xml-format xml-format/list
  xml-parse-register-xmlns empty-namespace-declaration
  xml-format-fragment xml-format-exc-c14n xml-format-exc-c14n-root
  xml-format-at-output-port xml-format-fragment-at-output-port
  xml-format-fragment-at-current-output-port
  xml-format-at-current-output-port
  xml-quote-display             ; will be dropped
  xml-digest-simple ; sha256 sum of string or tree
  ;; unused: xml-encoded-string?
  ;; regex-case
  xml-parse
  ;; xml-tree
  xml-encoding normalise-data
  html-format html-format/list content-type-for-html node-list-media-type
  $buffer-size $force-iconv-on-output make-display
          
  lout-format-output-element
  lout-format-at-current-output-port
  pdf-format
  lout-pdf-format

  pem-encode-string pem-decode-string
  pem-encode pem-decode
  parse-basic-authorization
  mime-format register-output-method!
  content-disposition->name multipart-data-boundary
  make-boundary-for  ;; we better don't export this one
  message-body-offset
  text/xml application/soap+xml application/rdf+xml
  application/xml
  text/plain text/html xml-parseable? html-parseable?
  soap+xml-content?
  ;;
  register-mime-converter! mime-converter
  ;;
  rfc2046-split 

  ;; these should not be exported, but we just need them so badly in
  ;; the fsm.scm

  xml-quote-display-at-port
  uri-quote uri-quote-all uri-parse
  form-quote form-parse
  set-uri-reserved-chars!
  ;;
  parsed-locator
  ;;
  error-for-lalr pcre-lalr-parser
  ;;
  uri ;; deprecated
  string->uri uri? uri-scheme uri-authority uri-path uri-query uri-fragment
  ;; these ought to be renamed for the sake of consistency
  absolute-uri? absoluteURI? split-uri
  ;;
  quoted-printable-encode-string quoted-printable-encode-header
  quoted-printable-decode-string
  )

(import-for-syntax lalr)
(import (except scheme vector-fill! vector->list list->vector)
	(except chicken add1 sub1 condition? vector-copy! with-exception-handler)
	srfi-1 srfi-13 srfi-34 srfi-35
	srfi-43 (prefix srfi-43 srfi:) srfi-69
	openssl atomic parallel memoize regex pcre util ports tree extras
	notation-charenc notation-common notation-xmlparse notation-xmlrender
	notation-lout)

(include "typedefs.scm")

(define-type :xml2plain: (:xml-element: --> string))
(define-type :plain2xml: (string --> :xml-nodelist:))

(define-type :mime-converter: ((or string :xml-nodelist:) --> *))

(define (identity x) x)

(define-syntax define-macro
  (syntax-rules ()
    ((_ (name . llist) body ...)
     (define-syntax name
       (lambda (x r c)
	 (apply (lambda llist body ...) (cdr x)))))
    ((_ name . body)
     (define-syntax name
       (lambda (x r c) (cdr x))))))

(define-syntax %early-once-only
  (syntax-rules ()
    ((%early-once-only body ...) (begin body ...))))

(define-syntax pcre-lalr-parser
  (syntax-rules ()
    ((pcre-lalr-parser scan argument ...)
     (let ((parser (lalr-parser argument ...))
	   (scanner (make-pcre-tokeniser 'scan)))
       (lambda (str)
	 (receive (tokens position) (scanner str)
		  (guard
		   (condition
		    (else
		     (receive
		      (title msg args rest) (condition->fields condition)
		      (raise (make-condition
			      &message 'message (format "~a\n~a in:\n~a\n~a" title msg str args))))))
		   (parser tokens error-for-lalr))))))))


(define (error-for-lalr msg . obj)
  (if (pair? obj)
      (error (format "~a ~a" msg obj))
      (error (format "~a" msg))))

#;(define-syntax pcre-lalr-parser
  (syntax-rules ()
    ((pcre-lalr-parser scan argument ...)
     (let ((parser (lalr-parser argument ...))
	   (scanner (make-pcre-tokeniser 'scan)))
       (lambda (str)
	 (receive (tokens position) (scanner str)
		  (guard
		   (condition
		    (else
		     (receive
		      (title msg args rest) (condition->fields condition)
		      (error (format "~a\n~a in:\n~a\n~a" title msg str args)))))
		   (parser tokens error-for-lalr))))))))

(define-macro (logical-shift-right pattern offset)
  `(arithmetic-shift ,pattern (fx- 0 ,offset)))

(define-macro (logical-shift-left pattern offset)
  `(arithmetic-shift ,pattern ,offset))

(include "../mechanism/notation/rfc1421.scm")
(include "../mechanism/notation/rfc1522.scm")

;; MIME

;(define multipart-data-boundary
;  (reg-expr->proc
;   (string-append
;    "^[[:blank:]]*multipart/(?:[[:alpha:]-]+[[:blank:];]+)"
;    "boundary=\"?([^\"]*)\"?"
;    "[[:blank:];]*$")))

(define multipart-data-boundary
  (let ((match (regexp (string-append
                        "^[[:blank:]]*multipart/[[:alpha:]-]+[[:blank:];]+"
                        "boundary=\"?([^\"]*)\"?"
                        "[[:blank:];]*$"))))
    (lambda (str)
      (let  ((result (string-search-positions match str)))
        (and result (substring str (caadr result) (cadadr result)))))))

;; FIXME the regular expression SHOULD actually just match Identifiers
;; accoring to XML syntax plus the special strings "*xmlns*" and
;; "*name*".

(define content-disposition->name
  (let ((match (regexp (string-append
                        "^[[:blank:]]*form-data[[:blank:];]+"
                        "name=\"?(\\*?[-[:alnum:]._]+\\*?)\"?"
                        "(?:[[:blank:];]+filename=\"?([^\"]*))?"))))
    (lambda (str)
      (let ((result (string-search-positions match str)))
        (if result
            (values (substring str (caadr result) (cadadr result))
                    (and (caaddr result) ; (pair? (caddr result))
			 (substring str (caaddr result) (cadr (caddr result)))))
            (values #f #f))))))

(define message-body-offset-regex
  (let ((match (regexp "(?m)\r?\n\r?\n")))
    (lambda (str) (let ((v (string-search-positions match str)))
		    (if v
			(apply values (car v))
			(values #f #f))))))

(define lws-suffix
  (let ((match (regexp "^(?:--)?[[:blank:]]*\r?\n")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
	    (values (caar result) (cadar result))
	    (values #f #f))))))

(define parse-basic-authorization
  (let ((match (regexp "Basic[[:blank:]]*([^[:blank:]]*)")))
    (lambda (str . off)
      (let ((result (string-search-positions match str (if (pair? off) (car off) 0))))
        (if result
	    (let ((p (cdr result)))
	      (if (car p)		; matched?
		  (let ((b (caar p))
			(e (cadar p)))
		    (if (and b e)
			(values b e (substring str b e))
			(values #f #f #f)))
		  (values #f #f #f)))
            (values #f #f #f))))))

(include "../mechanism/notation/mime.scm")
(include "../mechanism/notation/uri.scm")

(define-record-printer (<uri> x out) (display-uri x out))

)

(import (prefix notation m:))

(define normalise-data m:normalise-data)
(define xml-format m:xml-format)
(define dsssl-xml-format m:dsssl-xml-format)
(define xml-parse m:xml-parse)
(define pem-encode m:pem-encode)
(define pem-decode m:pem-decode)
(define mime-converter m:mime-converter)
(define parsed-locator m:parsed-locator)
(define uri-parse m:uri-parse)

(define uri m:uri)
(define uri? m:uri?)
(define uri-scheme m:uri-scheme)
(define uri-authority m:uri-authority)
(define uri-path m:uri-path)
(define uri-query m:uri-query)
(define uri-fragment m:uri-fragment)
(define absolute-uri? m:absolute-uri?)
(define split-uri m:split-uri)

(define uri-parse m:uri-parse)
(define uri-quote m:uri-quote)
(define uri-quote-all m:uri-quote-all)
(define set-uri-reserved-chars! m:set-uri-reserved-chars!)

(define form-quote m:form-quote)
(define form-parse m:form-parse)

(define mime-format m:mime-format)
(define (register-mime-converter! from to proc)
  (assert (string? from))
  (assert (string? to))
  (assert (procedure? proc))
  (m:register-mime-converter! from to proc))
(define (register-output-method! key proc)
  (assert (string? key))
  (assert (procedure? proc))
  (m:register-output-method! key proc))
