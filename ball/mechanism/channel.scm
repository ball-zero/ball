(define $complete-timeout (make-config-parameter 2))

(define $broadcast-retries (make-config-parameter 2))
(define $broadcast-timeout (make-config-parameter 3))

(cond-expand
 ((or chicken))
 (else
  (define-macro (with-channel-mutex m . b) `(with-mutex ,m . ,b))
  (define-macro (retain-channel-mutex m . b) `(retain-mutex ,m . ,b))))

;; As it turned out, the "commit on retry" is a slightly more
;; complicated as foreseen.  It appears to be infeasable (at least for
;; debugging) to keep the whole protocol state within a single
;; closure, hence we keep it here completely.  BEWARE: we will have
;; transitional code during the rewrite.  

(: oid-channel-entry? (* --> boolean : (struct <oid-channel-entry>)))
(: set-oid-channel-version!
   ((struct <oid-channel>) fixnum -> undefined))
(: oid-channel-entry-start-time
   ((struct <oid-channel-entry>) --> :time:))
(: oid-channel-entry-due
   ((struct <oid-channel-entry>) --> (or false :time:)))
(: oid-channel-entry-state-digest ((struct <oid-channel-entry>) --> :hash:))
(: oid-channel-entry-request
   ((struct <oid-channel-entry>) --> (or false :message:)))
(: oid-channel-entry-transaction
   ((struct <oid-channel-entry>) --> (or false (struct <transaction>))))

(define-record-type <oid-channel-entry>
  (%make-oid-channel-entry start-time due state request request-digest transaction)
  oid-channel-entry?
  (start-time oid-channel-entry-start-time)
  (due oid-channel-entry-due)
  (state oid-channel-entry-state-digest)
  (request oid-channel-entry-request
	   intern-set-oid-channel-entry-request! ; to be removed, if possible
	   )
  (request-digest intern-oid-channel-entry-request-digest
		  set-oid-channel-entry-request-digest!)
  (transaction oid-channel-entry-transaction
	       set-oid-channel-entry-transaction!)
  )

(: make-oid-channel-entry
   (:hash:
    :message:
    (or false :hash:)
    (or false (struct <transaction>))
    --> (struct <oid-channel-entry>)))
(define (make-oid-channel-entry state-digest request request-digest transaction)
  (let ((now
	 ;;*system-time*
	 (srfi19:current-time 'time-utc)
	 )
	(tmo ($complete-timeout)))
    (%make-oid-channel-entry
     now (if (boolean? tmo) #f (add-duration now (make-time 'time-duration 0 tmo)))
     state-digest request request-digest transaction)))

(: oid-channel-entry-request-digest
   ((struct <oid-channel-entry>) :message-digest: -> :hash:))
(define (oid-channel-entry-request-digest entry message-digest)
  (if (not (intern-oid-channel-entry-request-digest entry))
      (if (oid-channel-entry-request entry)
	  (set-oid-channel-entry-request-digest!
	   entry (message-digest (oid-channel-entry-request entry)))
	  (raise 'oid-channel-entry-request-digest:no-request)))
  (intern-oid-channel-entry-request-digest entry))

(: set-oid-channel-entry-request!
   ((struct <oid-channel-entry>) :message: :message-digest: -> *))
(define (set-oid-channel-entry-request! entry request message-digest)
  (intern-set-oid-channel-entry-request! entry request)
  (set-oid-channel-entry-request-digest! entry (delay* (message-digest request) 'md)))

(define (test-and-set-oid-channel-entry-transaction! entry old new)
  (let ((current (oid-channel-entry-transaction entry)))
    (if (eq? current old)
	(begin (set-oid-channel-entry-transaction! entry new) new)
	old)))

(: oid-channel-entry-overdue? ((struct <oid-channel-entry>) --> boolean))
(define (oid-channel-entry-overdue? entry)
  (and-let* ((due (oid-channel-entry-due entry)))
	    (srfi19:time>? *system-time* due)))

(: make-oid-channel-entry<
   ((procedure ((struct <transaction>)) boolean)
    :message-digest:
    --> (procedure ((struct <oid-channel-entry>) (struct <oid-channel-entry>))
		   boolean)))
(define (make-oid-channel-entry< transaction-ready? message-digest)
  (begin
    (define (<.3 a b)
      (string<? (oid-channel-entry-request-digest a message-digest)
		(oid-channel-entry-request-digest b message-digest)))
    (define (<.2 a b)
      (srfi19:time>? (oid-channel-entry-start-time a)
		     (oid-channel-entry-start-time b)))
    (define (<.1 a b) (or (<.2 a b) (<.3 a b)))
    (define (oid-channel-entry< a b)
      (and (oid-channel-entry-transaction a) (oid-channel-entry-request a)
	   (or (not (oid-channel-entry-transaction b))
	       (not (oid-channel-entry-request b))
	       (cond
		((and (transaction-ready? (oid-channel-entry-transaction a))
		      (transaction-ready? (oid-channel-entry-transaction b)))
		 (<.1 a b))
		((or (and (transaction-ready? (oid-channel-entry-transaction a)) 'a)
		     (and (transaction-ready? (oid-channel-entry-transaction b)) 'b))
		 => (lambda (c) (case c ((a) #t) ((b) #f))))
		(else (<.1 a b))))))
    oid-channel-entry<))

(define-record-type <oid-channel-queue>
  (internal-make-oid-channel-queue version completed mailbox result quorum subscriptions emux entries)
  oid-channel-queue?
  (version oid-channel-queue-version)
  (completed oid-channel-queue-completed %set-oid-channel-queue-completed!)
  (mailbox oid-channel-queue-mailbox)
  (result oid-channel-queue-result set-oid-channel-queue-result!)
  (quorum oid-channel-queue-caller-quorum set-oid-channel-queue-caller-quorum!)
  (subscriptions oid-channel-queue-subscriptions)
  (emux oid-channel-queue-entries-mutex)
  (entries oid-channel-queue-entries set-oid-channel-queue-entries!)
  )

(define (make-oid-channel-queue name version entries)
  (internal-make-oid-channel-queue
   version
   (make-condition-variable (dbgname name "~a##~a" version))
   (make-mailbox `(mb ,name ,version))
   #f '()
   (list (make-mutex (dbgname name "~a-channel-queue##~a" version)))
   (make-mutex (dbgname name "~a-channel-emux##~a" version))
   entries))

(define (reset-oid-channel-queue-completed! queue name version)
  (set-oid-channel-queue-result! queue #f)
  (set-oid-channel-queue-caller-quorum! queue '())
  (let ((cv (oid-channel-queue-completed queue)))
    (%set-oid-channel-queue-completed!
     queue (make-condition-variable (dbgname name "~a##~a" version)))
    (if (condition-variable? cv) (condition-variable-broadcast! cv))))

(define (oid-channel-entry-pred pred?)
  (lambda (e)
    (and-let*
     ((t (oid-channel-entry-transaction e))
      ((pred? t)))
     e)))

(define (queue-waiting? queue)
  (condition-variable? (oid-channel-queue-completed queue)))

(define (queue-done? queue)
  (not (condition-variable? (oid-channel-queue-completed queue))))

(: oid-channel-queue-first-transaction
   ((struct <oid-channel-queue>)
    (procedure ((struct <transaction>)) (or false (struct <transaction>)))
    --> (or false (struct <transaction>))))
(define (oid-channel-queue-first-transaction queue pred?)
  (ormap (oid-channel-entry-pred pred?) (oid-channel-queue-entries queue)))

(define (oid-channel-oldest-queue channel pred?)
  (find (lambda (queue)
	  (ormap
	   (lambda (e)
	     (let ((t (oid-channel-entry-transaction e)))
	       (and t
		    (pred? t)
		    (not (oid-channel-entry-overdue? e)))))
	   (oid-channel-queue-entries queue)))
	(reverse (cdr (oid-channel-queue channel)))))

(define (queue-fold-last-messages
	 queue transaction-digest message-digest ready? oid-channel-entry< fold-echo fold-ready init)
  (let ((ready (any ready? (oid-channel-queue-entries queue))))
    (if ready
	(let ((entries (sort (oid-channel-queue-entries queue) oid-channel-entry<)))
	  (fold
	   (lambda (entry init)
	     (if (and (oid-channel-entry-request entry)
		      (oid-channel-entry-transaction entry))
		 (fold-echo (oid-channel-entry-request-digest entry message-digest)
			    (oid-channel-entry-state-digest entry)
			    init)
		 init))
	   (fold-ready
	    (oid-channel-entry-request-digest (car entries) message-digest)
	    (transaction-digest (oid-channel-entry-transaction (car entries)))
	    init)
	   (cdr entries)))
	(fold
	 (lambda (entry init)
	   (if (and (oid-channel-entry-request entry)
		    (oid-channel-entry-transaction entry))
	       (fold-echo
		(oid-channel-entry-request-digest entry message-digest)
		(oid-channel-entry-state-digest entry) init)
	       init))
	 init
	 (oid-channel-queue-entries queue)))))

(define-record-type <oid-channel>
  (internal-make-oid-channel
   mutex
   extended
   driver version pred queue)
  oid-channel?
  (mutex oid-channel-mutex)		; receiver/update side
  (extended oid-channel-extended)
  (driver oid-channel-driver-mutex)	; avoid running multiple copies of the driver
  (version oid-channel-version set-oid-channel-version!)
  (pred oid-channel-pred)
  (queue oid-channel-queue))

(define (channel-every-entry pred channel)
  (andmap
   (lambda (queue)
     (andmap pred (oid-channel-queue-entries queue)))
   (cdr (oid-channel-queue channel))))

(define-macro (oid->string x) `(symbol->string ,x))

(define (make-oid-channel oid version-number state request pred)
  (internal-make-oid-channel
   (make-mutex (dbgname (oid->string oid) "~a lock"))
   (make-condition-variable (dbgname (oid->string oid) "~a growth"))
   (make-mutex (dbgname (oid->string oid) "~a driver"))
   version-number
   pred
   (list (make-mutex (dbgname (oid->string oid) "~a-channel"))
	 (make-oid-channel-queue
	  (dbgname (oid->string oid) "~a-~a" version-number)
	  version-number
	  (if request
	      (list (make-oid-channel-entry state request #f #f))
	      '())))))

(: channel-last-messages
   (:oid:
    ((struct <transaction>) -> :hash:)	; transaction-digest
    :message-digest:
    (procedure ((struct <transaction>)) boolean) ; ready?
    ((struct <oid-channel-entry>) (struct <oid-channel-entry>) -> boolean) ; oid-channel-entry<
    (procedure (* * *) list)			 ; fold-echo
    (procedure (* * *) list)			 ; fold-ready
    *						 ; init
    --> list))
(define (channel-last-messages oid transaction-digest message-digest ready? oid-channel-entry< fold-echo fold-ready init)
  (let ((channel (existing-oid-channel oid)))
    (if channel
	(fold
	 (lambda (queue init)
	   (queue-fold-last-messages
	    queue transaction-digest message-digest ready? oid-channel-entry< fold-echo fold-ready init))
	 init
	 (cdr (oid-channel-queue channel)))
	'())))

(define (queue%filter-transactions! channel queue pred)
  (if (pair? (oid-channel-queue-entries queue))
      (set-oid-channel-queue-entries!
       queue
       (filter pred (oid-channel-queue-entries queue))))
  (null? (oid-channel-queue-entries queue)))

(define (queue%advance-transaction
	 transaction-completed? transaction-done? complete-transaction-entry!
	 oid-channel-entry-ready?
	 on frame channel queue message)
  (queue%filter-transactions!
   channel queue
   (lambda (e)
     (and (oid-channel-entry-transaction e)
	  (not (transaction-completed? (oid-channel-entry-transaction e)))
	  (not (complete-transaction-entry!
		on frame queue (oid-channel-entry-state-digest e)
		;; 		 (not (ormap
		;; 		       oid-channel-entry-ready?
		;; 		       (oid-channel-queue-entries queue)))
		#f			; abandon
		e message))
	  (not (oid-channel-entry-overdue? e))))))

(define (channel-filter-transactions! channel pred)
  ;; precondition
  (hang-on-mutex! 'channel-filter-transactions!
		  (map oid-channel-queue-entries-mutex
		       (cdr (oid-channel-queue channel))))

  (let ((queues (cdr (oid-channel-queue channel)))
	(f (lambda (queue)
	     (retain-channel-mutex (oid-channel-queue-entries-mutex queue)
			   (queue%filter-transactions! channel queue pred)))))
    (cond
     ((null? queues) #t)
     ;; Maybe this would (still) lock up?
     ;; ((null? (cdr queues)) (f (car queues)))
     (else (map-parallel #f #f 0 #f f queues)))))

(define (oid-channel-queue-ref key lst)
  (let loop ((lst lst))
    (and (pair? lst)
	 (or (and (eqv? key (oid-channel-queue-version (car lst)))
		  (car lst))
	     (and (fx>= key (oid-channel-queue-version (car lst)))
		  (loop (cdr lst)))))))

(define (oid-channel-queue-insert datum lst)
  (let loop ((lst lst))
    (cond
     ((null? lst) (list datum))
     ((fx>= (oid-channel-queue-version datum)
	    (oid-channel-queue-version (car lst)))
      (cons (car lst) (loop (cdr lst))))
     (else (cons datum lst)))))


(define (oid-channel-select-queue* channel version)
  (or (oid-channel-queue-ref version (cdr (oid-channel-queue channel)))
      (let ((queue (make-oid-channel-queue
		    (format "~a-~a"
			    (mutex-name (car (oid-channel-queue channel)))
			    version)
		    version
		    '())))
	(set-cdr! (oid-channel-queue channel)
		  (oid-channel-queue-insert
		   queue (cdr (oid-channel-queue channel))))
	(condition-variable-broadcast! (oid-channel-extended channel))
	queue)))

(define (oid-channel-select-queue channel version)
  ;; precondition
  (hang-on-mutex! 'oid-channel-select-queue
		  (list (car (oid-channel-queue channel))))

  ;; We used to restrict the selection like this:
  ;;  (and (fx>= version (oid-channel-version channel))
  ;;       ...)
  ;; However a single (fake) future request would lock us out from
  ;; completing our transactions.
  (or (oid-channel-queue-ref version (cdr (oid-channel-queue channel)))
      (retain-channel-mutex
       (car (oid-channel-queue channel))
       (oid-channel-select-queue* channel version))))

(define (set-oid-channel-queue-completed! channel queue request-id result quorum)
  (hang-on-mutex! 'set-oid-channel-queue-completed!
		  (list (car (oid-channel-queue channel))))
  (and-let* (((queue-waiting? queue))
	     (cv (retain-channel-mutex
		  (car (oid-channel-queue channel))
		  ;; isn't (oid-channel-queue-result queue) always #f here?
		  (if (and result (not (oid-channel-queue-result queue)))
		      (set-oid-channel-queue-result! queue result))
		  (if quorum (set-oid-channel-queue-caller-quorum! queue quorum))
		  (and-let* ((cv (oid-channel-queue-completed queue))
			     ((queue-waiting? queue)))
			    (%set-oid-channel-queue-completed! queue request-id)
			    cv))))
	    (condition-variable-broadcast! cv))
  (condition-variable-broadcast! (oid-channel-extended channel)))

(cond-expand
 (rscheme (define (normal:is-to-be-cleaned-up? v cv) (fx>= v cv)))
 (else (define normal:is-to-be-cleaned-up? fx>=)))
;; This alternative will stick to the last exception.  Useful only in
;; debug situations, where one needs to retrieve the exception later manually.
(: debug:is-to-be-cleaned-up? (fixnum fixnum --> boolean))
(define (debug:is-to-be-cleaned-up? v cv)
  (cond
   ((eq? v cv) #f)
   (else (fx>= v cv))))

(define is-to-be-cleaned-up? normal:is-to-be-cleaned-up?)

(: debug:set-stick-to-exception! (boolean -> boolean))
(define (debug:stick-to-exception! flag)
  (let ((was (eq? is-to-be-cleaned-up? debug:is-to-be-cleaned-up?)))
    (set! is-to-be-cleaned-up? (if flag debug:is-to-be-cleaned-up? normal:is-to-be-cleaned-up?))
    was))

(define (oid-channel-drop-queues channel version)
  (let loop ((lst (cdr (oid-channel-queue channel))))
    (if (and (pair? lst)
	     (is-to-be-cleaned-up? version (oid-channel-queue-version (car lst))))
	(begin
	  (and-let* ((queue (car lst))
		     ((queue-waiting? queue))
		     (cv (oid-channel-queue-completed queue)))
		    (%set-oid-channel-queue-completed! queue #f)
		    (logerr "unexpected queue drop ~a ~a\n" (oid-channel-queue-version queue) version)
		    (condition-variable-broadcast! cv))
	  (loop (cdr lst)))
	lst)))

(: channel-advance-transactions
   ((procedure ((struct <transaction>)) boolean) ; transaction-completed?
    (procedure ((struct <transaction>)) boolean) ; transaction-done?
    (procedure (symbol				 ; on
		(pair (struct <frame>) symbol)	 ; frame
		(struct <oid-channel-queue>)	 ; queue
		:hash:				 ; digest
		boolean				 ; abandon
		(struct <oid-channel-entry>)	 ; entry
		:message:			 ; message
		)
	       boolean)				 ; complete-transaction-entry!
    (procedure ((struct <transaction>)) boolean) ; oid-channel-entry-ready?
    (procedure ((struct <transaction>)
		* :message:)
	       boolean)		                 ; transaction-precheck-and-force!
    symbol					 ; on
    :place:					 ; frame
    (struct <oid-channel>)			 ; channel
    :message:					 ; message
    fixnum
    *
    -> boolean))
(define (channel-advance-transactions
	 transaction-completed? transaction-done? complete-transaction-entry!
	 oid-channel-entry-ready? transaction-precheck-and-force!
	 on frame channel message version
	 call-indicator)
  ;; precondition
  (hang-on-mutex! call-indicator
		  (map oid-channel-queue-entries-mutex
		       (cdr (oid-channel-queue channel))))


  (guard
   (ex ((timeout-object? ex) #f))
   (let loop ()
     ;; (mutex-lock! (car (oid-channel-queue channel)))
     (if (and (eqv? (oid-channel-version channel) version)
	      (let ((queue (oid-channel-select-queue channel version)))
		(and (queue-waiting? queue)
		     (or (null? (oid-channel-queue-entries queue))
			 (not (any oid-channel-entry-request (oid-channel-queue-entries queue)))))))
	 (begin
	   ;;(logerr "Q DBG ~a channel-advance-transactions waiting on empty queue version ~a\n" on version)
	   (mutex-lock! (car (oid-channel-queue channel)))
	   (within-time
	    ($broadcast-timeout)
	    (mutex-unlock! (car (oid-channel-queue channel)) (oid-channel-extended channel)))
	   ;;(logerr "Q DBG ~a channel-advance-transactions queue reached version ~a\n" on version)
	   (loop))
	 ;; (mutex-unlock! (car (oid-channel-queue channel)))
	 )))

  (fold
   (lambda (queue init)
     (if (> (oid-channel-queue-version queue) version) init
	 (cons
	  (begin
	    (for-each
	     (lambda (e)
	       (and-let* ((t (oid-channel-entry-transaction e)))
			 (transaction-precheck-and-force! t (oid-channel-entry-state-digest e) message)))
	     (oid-channel-queue-entries queue))
	    (retain-channel-mutex (oid-channel-queue-entries-mutex queue)
			  (queue%advance-transaction
			   transaction-completed? transaction-done? complete-transaction-entry!
			   oid-channel-entry-ready?
			   on frame channel queue message)))
	  init)))
   '()
   (cdr (oid-channel-queue channel))))

(define *oid-channel*
  (make-cache
   "*oid-channel*"
   oid=?
   #f ;; state
   ;; miss:
   (lambda (cs k) #t)
   ;; hit
   #f ;; (lambda (c es) #t)
   ;; fulfil
   #f ;; (lambda (c es results) #f)
   ;; valid?
   (lambda (es) #t)
   ;; delete
   #f ;; (lambda (cs es) #f)
   ))

(define (oid-channel oid version state request pred)
  (cache-ref *oid-channel* oid (lambda () (make-oid-channel oid version state request pred))))

(define (existing-oid-channel oid)
  (cache-ref/default *oid-channel* oid #f #f))

(define (existing-oid-channel-mutex oid)
  (and-let* ((c (cache-ref/default *oid-channel* oid #f #f))) (oid-channel-mutex c)))

(define (oid-channel-empty? oid)
  (let ((c (cache-ref/default *oid-channel* oid #f #f)))
    (or (not c)
	(and (null? (cdr (oid-channel-queue c)))
	     (not (mutex-owner (oid-channel-mutex c)))
	     (not (mutex-owner (oid-channel-driver-mutex c)))))))

(define (close-oid-channel! oid) (cache-invalid! *oid-channel* oid))

(define respond!-mutex-pred #f)

(define (set-respond-mutex-guard! pred)
  (set! respond!-mutex-pred pred))

(define (respond!-mutex oid)
  (oid-channel-mutex (oid-channel oid 0 '(0 . "unloaded" ) #f respond!-mutex-pred)))

(define (locked-frames)
  (cache-fold
   *oid-channel*
   (lambda (k v i)
     (if (thread? (mutex-state (oid-channel-mutex v)))
	 (cons (cons k v) i) i))
   '()))

(define (queue-unregister-mailbox! queue mbox)
  ;; precondition
  (hang-on-mutex! 'queue-unregister-mailbox!
		  (list (car (oid-channel-queue-subscriptions queue))))
  (retain-channel-mutex (car (oid-channel-queue-subscriptions queue))
		;; TODO: we SHOULD actually use "delete!" here.
		(set-cdr! (oid-channel-queue-subscriptions queue)
			  (delete mbox
				  (cdr (oid-channel-queue-subscriptions queue))))
		mbox))

(define (oid-channel-unregister-mailbox! channel version mbox)
  (and-let*
   ((queue (oid-channel-queue-ref version (cdr (oid-channel-queue channel)))))
   (queue-unregister-mailbox! queue mbox))
  #f)

(: oid-channel-send-message-version!
   (:oid: fixnum :message: (procedure (* *) boolean) -> undefined))
(define (oid-channel-send-message-version! oid version-number message equal?)
  ;; precondition
  (assert (respond!-mutex-pred message))
  (let ((q (oid-channel-select-queue
	    (oid-channel oid version-number #f #f respond!-mutex-pred)
	    version-number)))
    (or (mailbox-has-message? (oid-channel-queue-mailbox q) message equal?)
	(with-channel-mutex (car (oid-channel-queue-subscriptions q))
		    (send-message! (oid-channel-queue-mailbox q) message)
		    (for-each (lambda (mbox) (send-message! mbox message))
			      (cdr (oid-channel-queue-subscriptions q)))))))

(define (oid-cannel-mailbox-close! oid version mbox)
  (and-let*
   ((channel (existing-oid-channel oid)))
   (oid-channel-unregister-mailbox! channel version mbox)))

(define (oid-channel-mailbox oid version-number)
  (and-let*
   ((channel (oid-channel oid version-number #f #f respond!-mutex-pred))
    (queue (oid-channel-select-queue channel version-number)))
   (let ((subscriptions (oid-channel-queue-subscriptions queue)))
     (with-channel-mutex (car subscriptions)
		 (let ((mbox (mailbox-clone (oid-channel-queue-mailbox queue))))
		   (set-cdr! subscriptions (cons mbox (cdr subscriptions)))
		   mbox)))))

(: pending-transactions
   (:oid: fixnum --> (or false (list-of (struct <oid-channel-entry>)))))
(define (pending-transactions oid version)
  (and-let* ((c (existing-oid-channel oid))
	     (queue (oid-channel-queue-ref version (cdr (oid-channel-queue c))))
	     (entries (oid-channel-queue-entries queue))
	     ((pair? entries)))
	    entries))

(: oid-channel-pending-transactions (:oid: --> (list-of (struct <transaction>))))
(define (oid-channel-pending-transactions oid)
  (and-let* ((c (existing-oid-channel oid)))
	    (ormap (lambda (queue)
		     (pair? (oid-channel-queue-entries queue)))
		   (cdr (oid-channel-queue c)))))

;; Backward compatible, restricted (imcomplete) API.

(: transaction-reply (:oid: fixnum :oid: -> *))
(define (transaction-reply oid version caller)
  (and-let* ((channel (existing-oid-channel oid))
	     (queue (oid-channel-queue-ref version (cdr (oid-channel-queue channel))))
	     ((member caller (oid-channel-queue-caller-quorum queue))))
	    (oid-channel-queue-result queue)))

(: transaction-request (:oid: fixnum --> (or (struct <frame>) false)))
(define (transaction-request oid version)
  (and-let* ((transactions (pending-transactions oid version)))
	    (oid-channel-entry-request (car transactions))))

;; unused
(: register-transaction!
   (:oid: fixnum :hash: (struct <frame>) :message-digest: -> (struct <frame>)))
(define (register-transaction! oid version state-digest request message-digest)
  ;; (assert (frame? request)) -- not possible since frame? is not in scope
  ;; (assert request) ;;-- don't check anyway as it results in compile time warning.
  (let ((x (oid-channel oid version state-digest request respond!-mutex-pred)))
    ;; precondition
    (hang-on-mutex! 'register-transaction!
		    (map oid-channel-queue-entries-mutex
			 (cdr (oid-channel-queue x))))
    (let ((queue (oid-channel-select-queue x version)))
      (if (not queue)
	  (error "register-transaction! on ~a ~a have ~a" oid version
		 (oid-channel-version x)))
      ;; Sanity check, might be needed for backward compatibility.
      (let loop ((entries (oid-channel-queue-entries queue))
		 (empty #f))
	(cond
	 ((null? entries)
	  (if empty
	      (begin
		(logerr "Filling empty request on ~a ~a.\n" oid version)
		(set-oid-channel-entry-request! empty request message-digest)
		#f)
	      (let ((n (make-oid-channel-entry state-digest request #f #f)))
		(with-channel-mutex (oid-channel-queue-entries-mutex queue)
				    (set-oid-channel-queue-entries!
				     queue (cons n
						 (oid-channel-queue-entries queue)))
				    (condition-variable-broadcast! (oid-channel-extended x))
				    #f))))
	 ((not (oid-channel-entry-request (car entries)))
	  (loop (cdr entries) (car entries)))
	 ((eq? (oid-channel-entry-request (car entries)) request)
	  #t)
	 (else (loop (cdr entries) empty)))))
;;     (set-oid-channel-version! x version)
;;     (with-channel-mutex
;;      (car (oid-channel-queue x))
;;      (set-cdr! (oid-channel-queue x)
;; 	       (remove
;; 		(lambda (e) (< (car e) version))
;; 		(cdr (oid-channel-queue x)))))
    request))

;; This should be redone.  We should not have to loop to find the
;; pending transaction.  But then it needs to be an argument and thus
;; will incure rewrites elsewhere.

(define (queue-register-proposal! oid channel queue version state request proposal)
  ;; precondition
  (hang-on-mutex! 'queue-register-proposal!
		  (list (oid-channel-queue-entries-mutex queue)))
  ;; (logerr "queue-register-proposal! ~a ~a ~a ~a ~a ~a ~a\n" oid channel queue version state request proposal)
  (retain-channel-mutex
   (oid-channel-queue-entries-mutex queue)
   ;; This looks all to strange.  Should this ever been done?
   ;; (if (and (oid-channel-queue-result queue)
   ;; 	    (eqv? (oid-channel-queue-version queue) (oid-channel-version channel)))
   ;;     (reset-oid-channel-queue-completed! queue oid version))
   (or (oid-channel-queue-result queue)
       (let loop ((entries (oid-channel-queue-entries queue)))
	 (cond
	  ((null? entries)
	   (let ((entry (make-oid-channel-entry state request #f proposal)))
	     (set-oid-channel-queue-entries!
	      queue (cons entry (oid-channel-queue-entries queue)))
	     (condition-variable-broadcast! (oid-channel-extended channel))
	     entry))
	  ((eq? (oid-channel-entry-request (car entries)) request)
	   (set-oid-channel-entry-transaction! (car entries) proposal)
	   (condition-variable-broadcast! (oid-channel-extended channel))
	   (car entries))
	  (else (loop (cdr entries))))))))

(: oid-channel-cleanup-version!
   ((procedure ((struct <oid-channel-entry>)) boolean)
    (struct <oid-channel>)
    fixnum
    (or false (struct <frame>))
   -> boolean))

(define (oid-channel-cleanup-version! oid-channel-entry-ready? channel version result)
  (with-channel-mutex
   (car (oid-channel-queue channel))
   (let ((rest (oid-channel-drop-queues channel version)))
     (if (and (pair? rest)
	      result
	      (eqv? version (oid-channel-queue-version (car rest)))
	      (let ((queue (car rest))
		    (f (lambda (queue)
			 (filter
			  oid-channel-entry-ready?
			  (oid-channel-queue-entries queue)))))
		(with-channel-mutex
		 (oid-channel-queue-entries-mutex queue)
		 (set-oid-channel-queue-entries! queue (f queue)))
		(null? (oid-channel-queue-entries queue))))
	 (let ((cv (oid-channel-queue-completed (car rest))))
	   (if (condition-variable? cv)
	       (begin
		 (%set-oid-channel-queue-completed! (car rest) #f)
		 (condition-variable-broadcast! cv)))
	   (set-cdr! (oid-channel-queue channel) (cdr rest)))
	 (let ((cv (and (pair? rest) (oid-channel-queue-completed (car rest)))))
	   (if (condition-variable? cv) (condition-variable-broadcast! cv))
	   (set-cdr! (oid-channel-queue channel) rest))))
   #f))

;; oid-channel-remove-version!
;;
;; Recently changed to accept a "result-id" too.  So far this is not
;; completely supported.
;;
;; RETURN no (useful) value.

(: oid-channel-remove-version!
   ((procedure ((struct <oid-channel-entry>)) boolean)
    procedure
    :oid: fixnum
    (or false :hash:)
    (or false :message:)
    boolean
    (or false :quorum:)
   -> boolean))
(define (oid-channel-remove-version! oid-channel-entry-ready? transaction-completed?
				     oid version request-id result completed quorum)
  ;; (logerr "oid-channel-remove-version! ~a ~a ~a ~a\n" oid version result quorum)
  (let ((channel (existing-oid-channel oid)))
    ;; precondition
    (hang-on-mutex! 'oid-channel-remove-version!
		    (if channel (list (car (oid-channel-queue channel))) '()))
    (cond
     ((not channel) #f)
     ((or (not version) (fx>= (oid-channel-version channel) version))
      (if (not version) (set! version (oid-channel-version channel)))
      (and-let* ((result)
		 (completed)
		 (queue (oid-channel-queue-ref version (cdr (oid-channel-queue channel)))))
		(set-oid-channel-queue-completed! channel queue request-id result quorum))
      (let loop ((lst (cdr (oid-channel-queue channel))))
	(cond
	 ((null? lst) lst)
	 ((fx< (oid-channel-queue-version (car lst)) version)
	  (for-each (lambda (s)
		      (send-message! s (timeout-object)))
		    (cdr (oid-channel-queue-subscriptions (car lst))))
	  (set-oid-channel-queue-completed! channel (car lst) #f #f #f)
	  (loop (cdr lst)))
	 ((eqv? (oid-channel-queue-version (car lst)) version)
	  (if (not (and result completed))
	      (begin
		(for-each (lambda (s)
			    (send-message! s (timeout-object)))
			  (cdr (oid-channel-queue-subscriptions (car lst))))
		(set-oid-channel-queue-completed! channel (car lst) #f #f #f)))
	  (loop (cdr lst)))
	 (else lst)))
      (or (null? (cdr (oid-channel-queue channel)))
	  (!after>
	   ($complete-timeout)
	   (dbgname (oid->string oid) "~a remove-transaction!_1 ~a" `(,oid ,version ,result ,quorum))
	   (oid-channel-cleanup-version! oid-channel-entry-ready? channel version result))))
     (else
      (set-oid-channel-version! channel version)
      (and-let* ((result)
		 (completed)
		 (queue (oid-channel-queue-ref version (cdr (oid-channel-queue channel)))))
		(set-oid-channel-queue-completed! channel queue request-id result quorum))
      (let loop ((lst (cdr (oid-channel-queue channel))))
	(cond
	 ((null? lst) '())
	 ((fx< (oid-channel-queue-version (car lst)) version)
	  (for-each (lambda (s)
		      (send-message! s (timeout-object)))
		    (cdr (oid-channel-queue-subscriptions (car lst))))
	  (set-oid-channel-queue-completed! channel (car lst) #f #f #f)
	  (loop (cdr lst)))
	 ((eqv? (oid-channel-queue-version (car lst)) version)
	  (if (not (and result completed))
	      (begin
		(for-each (lambda (s)
			    (send-message! s (timeout-object)))
			  (cdr (oid-channel-queue-subscriptions (car lst))))
		(set-oid-channel-queue-completed! channel (car lst) #f #f #f))
	      ;; TODO: try copying pending transactions into next queue.
	      (let ((pending (filter (lambda (e)
				       (not (transaction-completed? (oid-channel-entry-transaction e))))
				     (oid-channel-queue-entries (car lst)))))
		(if (pair? pending)
		    (let ((result 412))
		      (logerr "NYI: Still ~a of ~a Pending to be restarted: ~a waiting ~a\n"
			      (length pending) (length (oid-channel-queue-entries (car lst)))
			      (map oid-channel-entry-ready? pending) (queue-waiting? (car lst)))
		      (for-each (lambda (s)
				  (send-message! s result))
				(debug 'Subscripts (cdr (oid-channel-queue-subscriptions (car lst)))))
		      )))
	      )
	  (loop (cdr lst)))
	 (else lst)))
      (or (null? (cdr (oid-channel-queue channel)))
	  (!after>
	   ($complete-timeout)
	   (dbgname (oid->string oid) "~a remove-transaction!_2 ~a" `(,oid ,version ,result ,quorum))
	   (with-channel-mutex
	    (car (oid-channel-queue channel))
	    (set-cdr! (oid-channel-queue channel) (oid-channel-drop-queues channel version)))))))))

;; Sh* this can hang!
;; (define (wait-for-transaction! channel queue)
;;   (define mux (car (oid-channel-queue channel)))
;;   (mutex-lock! mux)
;;   (if (ormap
;;        (lambda (e)
;; 	 (let ((t (oid-channel-entry-transaction e)))
;; 	   (and t
;; 		(not (transaction-completed? t))
;; 		(not (oid-channel-entry-overdue? e)))))
;;        (oid-channel-queue-entries queue))
;;       (begin
;; 	;; (mutex-lock! mux) ; - pure update/unsafe
;; 	(mutex-unlock! mux (oid-channel-queue-completed queue))
;; 	#t)
;;       (begin
;; 	(mutex-unlock! mux)
;; 	#f)))

(define (oid-channel-queue-any-entry-pending queue pred?)
  (ormap
   (lambda (e)
     (let ((t (oid-channel-entry-transaction e)))
       (and t
	    (not (pred? t))
	    (not (oid-channel-entry-overdue? e)))))
   (oid-channel-queue-entries queue)))

(define (wait-for-transaction-once! channel queue pred?)
  ;; precondition
  (hang-on-mutex! 'wait-for-transaction-once!
		  (list (car (oid-channel-queue channel))))
  (or (queue-done? queue)
      (and (oid-channel-queue-any-entry-pending queue pred?)
	   (and-let* (((queue-waiting? queue))
		      (mux (car (oid-channel-queue channel))))
	     (mutex-lock! mux) ; - pure update/unsafe
	     (let ((cv (oid-channel-queue-completed queue)))
	       (if (condition-variable? cv)
		   (begin
		     ;; (logerr "~a waiting on ~a\n" (current-thread) (condition-variable-name cv))
		     (mutex-unlock! mux cv)
		     (wait-for-transaction-once! channel queue pred?))
		   (begin
		     (mutex-unlock! mux)
		     #t)))))))

(define (wait-for-transaction-once channel queue pred? tmo round)
  (or (queue-done? queue)
      (within-time tmo (wait-for-transaction-once! channel queue pred?))))

(define wait-for-transaction! wait-for-transaction-once!)

(: wait-for-transaction
   ((struct <oid-channel>) (struct <oid-channel-queue>)
    (procedure ((struct <transaction>)) boolean) ; done?
    (or false number)
    -> boolean))
(define (wait-for-transaction channel queue pred? tmo)
  (if tmo
      (within-time tmo (wait-for-transaction! channel queue pred?))
      (wait-for-transaction! channel queue pred?)))

(: wait-for-transaction/version
   (:oid: fixnum
	  (procedure ((struct <transaction>)) boolean) ; done?
	  (or number false)
	  -> boolean))
(define (wait-for-transaction/version oid version pred? tmo)
  (and-let* ((channel (oid-channel oid version #f #f respond!-mutex-pred))
	     (queue (oid-channel-select-queue channel version)))
	    (wait-for-transaction channel queue pred? tmo)))

(: oid-channel-respond!
   (:oid:
    fixnum
    (pair fixnum :hash:)
    (struct <frame>)
    (procedure ((struct <transaction>)) boolean) ; done?
    *						 ; proposal
    (procedure () . *)				 ; todo
    -> . *))
(define (oid-channel-respond! oid version-number state request done? proposal thunk)
  (let* ((channel (oid-channel oid version-number state request respond!-mutex-pred))
	 (queue (oid-channel-select-queue channel version-number)))
    (if proposal
	(queue-register-proposal! oid channel queue version-number state request proposal))
    (thunk)
    (or (oid-channel-queue-result queue)
	(and (eq? (oid-channel-queue-completed queue) request)
	     (oid-channel-queue-result queue))
	(begin
	  (wait-for-transaction channel queue done? (* ($broadcast-retries) ($broadcast-timeout)))
	  (and (eq? (oid-channel-queue-completed queue) request)
	       (oid-channel-queue-result queue))))))

;; The "driver": try to complete pending transactions.  That is, *ask*
;; the peers for their state.  Does NOT infer with the actual
;; evaluation.

(: oid-channel-wait-for-oid!
   ((procedure (:oid: symbol) :place-or-not:) ; find-frame
    (procedure ((struct <transaction>)) boolean)	; done?
    (procedure (:oid:
		:place:
		(struct <oid-channel>)
		) undefined)		; rerequest-state
    symbol
    -> :place-or-not:))
(define (oid-channel-wait-for-oid! find-frame done? channel-rerequest-state oid)
  (and-let* ((channel (existing-oid-channel oid))
	     (queue (oid-channel-oldest-queue channel (lambda (t) (not (done? t))))))
	    ;; precondition
	    (hang-on-mutex! 'oid-channel-wait-for-oid!
			    (list (oid-channel-driver-mutex channel)))
	    (retain-mutex
	     (oid-channel-driver-mutex channel)
	     (let loop ((retry (add1 ($broadcast-retries))))
	       (and
		(fx< 0 retry)
		(cond
		 ((queue-done? queue) (find-frame oid 'oid-channel-wait-for-oid!))
		 ;; ((channel-every-entry oid-channel-entry-overdue? channel) #f)
		 ((not (oid-channel-queue-any-entry-pending queue done?)) #f)
		 ;; ((null? (oid-channel-queue-entries queue))
		 ;;  (remove-transaction! oid (oid-channel-queue-version queue #t))
		 ;;  (loop ($broadcast-retries)))
		 (else
		  (channel-rerequest-state oid (find-frame oid 'oid-channel-wait-for-oid!) channel)
		  (if (guard
		       (ex ((timeout-object? ex)
			    (if (eq? (mutex-state (car (oid-channel-queue channel)))
				     (current-thread))
				(mutex-unlock! (car (oid-channel-queue channel))))
			    #f))
		       (wait-for-transaction-once channel queue done? ($broadcast-timeout) retry))
		      (loop ($broadcast-retries))
		      (loop (sub1 retry))))))))))
