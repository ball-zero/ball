;; (C) 2000-2008 Jörg F. Wittenberger see http://www.askemos.org

;; Note: macros moved to place-rscheme.scm for porting efforts.

;; TODO once again this module has to  be split.


(define (frame-changeset-modified? cs orig)
  (not (and (equal? (frame-version cs) (frame-version orig))
            (eq? (frame-replicates cs) (frame-replicates orig))
            (eq? (frame-links cs) (frame-links orig))
            (equal? (frame-content-type cs) (frame-content-type orig))
            (or (and (frame-body/xml cs)
                     (eq? (frame-body/xml cs) (frame-body/xml orig)))
                (and (frame-body/plain cs)
                     (equal? (frame-body/plain cs) (frame-body/plain orig)))
                (not (frame-body/plain orig)))
	    (equal? (frame-capabilities cs) (frame-capabilities orig))
	    (equal? (frame-protection cs) (frame-protection orig))
            (eq? (frame-other cs) (frame-other orig)))))

;; (define (set-frame-changeset-modified! cs orig)
;;   (set-frame-version! cs (add1 (version-serial (frame-version orig)))))


;;*** Transactions

;; make(-*)?-access encapsulates the frame access in so far defines
;; the semantic invariants and constraints of the client language.

;; For instance transaction are here, i.e., we buffer the setter's
;; effect until successful completion of agreement about "method".

;; Here we have a version, which makes sure that client code never
;; hands out the setter function and attempts to call it after the
;; method is left.

;; There are two versions, one for read only and one for r/w access.
;; Both share some access code (which btw. can depend on the slot in
;; question, for instance we cache XML parse results).  This access
;; code is *only* to be used down until make-access (where we ought to
;; end the file once this gets broken up).

;; The general case: the slot is either a symbol or a user defined
;; name for either an internal state of the user document or a strong
;; link to another document.  In the latter case the client gets the
;; document id only, which is enough to send a message...

(define (%resolve-else slot properties-snapshot)
  (if (symbol? slot)
      (get-slot properties-snapshot slot)
      (frame-resolve-link properties-snapshot slot #f)))

(define (get-my-rights-from place message oid)
  (let ((mc (place 'capabilities))
	(mp (or (place 'potentialities) '()))
	(target (find-frame-by-id/quorum oid (or (place 'replicates) *local-quorum*))))
    (let ((q (replicates-of target)))
      (if (quorum-local? q)
	  (append
	   (filter
	    (lambda (has) (or (dominates? has mc) (dominates? has mp)))
	    (or (fget target 'potentialities) '()))
	   (filter
	    (lambda (has) (or (dominates? has mc) (dominates? has mp)))
	    (fget target 'capabilities)))
	  (let* ((from-others
		  (quorum-others (or (place 'replicates) *local-quorum*))))
	    ((privilege-info-remote)
	     (place 'get 'id)
	     (filter (lambda (x) (member x from-others)) (quorum-others q))
	     (append mc mp)
	     oid))))))
;; BLOB

(: make-block-handler (:block-table: --> vector))
(define make-block-handler
  (let ()

    (define (cbread t to n off)
      (define bs (block-table-bs t))
      (let loop ((blkno (quotient off bs))
		 (foff (modulo off bs))
		 (toff 0)
		 (n n))
	(let ((x (cond-expand
		  ((or chicken rscheme) (block-table-ref/default t blkno #f))
		  (else (block-table-resolve/default t blkno #f)))))
	  (cond
	   ((not x)
	    (memory-set! to #\x0 toff n)
	    'SQLITE_IOERR_SHORT_READ)
	   ;; ((string? x);; old/slow case
	   ;;  (logerr "make-block-handler/cbread unexpected case\n")
	   ;;  (let* ((avail (fx- (string-length x) foff))
	   ;; 	   (take (min n avail)))
	   ;;    (move-memory! x to take foff toff)
	   ;;    (if (fx< take n)
	   ;; 	  (loop (add1 blkno)
	   ;; 		0
	   ;; 		(fx+ toff take)
	   ;; 		(fx- n take))
	   ;; 	  'SQLITE_OK)))
	   (else
	    (let* ((take (or (copy-blob-value *the-registered-stores* x to n foff toff)
			     (and
			      (blob-notation-size *the-registered-stores* x #f)
			      (copy-blob-value *the-registered-stores* x to n foff toff)))))
	      (if (boolean? take)
		  (if take
		      (raise 'negative-n-should-not-happen)
		      (begin
		    (memory-set! to #\x0 toff n)
		    'SQLITE_IOERR_SHORT_READ))
		  (if (fx< take n)
		      (loop (add1 blkno)
			    0
			    (fx+ toff take)
			    (fx- n take))
		      'SQLITE_OK))) )))))

    (define (cbwrite t from n at)
      (define bs (block-table-bs t))
      (define blkno (quotient at bs))
      (define offset (modulo at bs))
      (define fb (min (fx- bs offset) n))
      (if (fx< 0 offset)
	  (let ((to (make-string/uninit fb)))
	    (move-memory! from to fb 0 0)
	    (block-table-update! t at to)
	    (set! blkno (add1 blkno))
	    (set! offset fb)
	    (set! at (fx+ at fb))
	    (set! n (fx- n fb))))
      (let loop ((n n) (at at) (foff offset))
	(if (eqv? n 0)
	    'SQLITE_OK
	    (let* ((cbs (min n bs))
		   (to (make-string/uninit cbs)))
	      (move-memory! from to cbs foff 0)
	      (block-table-update! t at to)
	      (loop (fx- n cbs) (fx+ at cbs) (fx+ foff cbs))))))

    (define (cbclose tbl) #t)

    (define (make-block-handler tbl)
      (vector tbl
	      block-table-bs block-table-size
	      cbread
	      cbwrite
	      block-table-truncate!
	      cbclose))
    make-block-handler))

;; Here the read only accessor:

(: make-reader (:message: --> (symbol -> *)))
(define (make-reader frame)
  (lambda (slot)
    (case slot
      ((body/parsed-xml) (message-body frame))
      ((mind-body) (message-body/plain frame))
      ((mind-links) #f)            ; hidden!
      ((cached:mind-body) (get-slot frame 'mind-body))
      ((cached:body/parsed-xml) (get-slot frame 'body/parsed-xml))
      ((debug) (lambda args (user-debug (get-slot frame 'dc-creator) args)))
      ((replicates) (get-slot frame 'replicates))
      (else (get-slot frame slot)))))

;;; There was this idea to make some data available to SQLite.
;;; However there is no way to reference the "temp" database from
;;; views triggers etc. in the main database.  --  Useless.
(define (sqlite3-setup-environment+ db frame message)
  ;;; (sqlite3-exec db "create temp table current_message (k text not null, v text)")
  ;;; (sqlite3-exec
  ;;;  db (format "insert into current_message values ('date','~a')"
  ;;; 	      (srfi19:date->string (message 'dc-date) "~Y-~m-~dT~H:~M:~S.~N")))
  db)

(define (sqlite3-open-ro/f_m_b frame message block-table)
  (let ((db (sqlite3-open-restricted-ro
	     (literal (aggregate-entity frame))
	     "askemos"
	     (make-block-handler block-table))))
   (sqlite3-setup-environment+ db frame message)))

(define (sqlite3-open/f_m_b frame message block-table)
  (and-let*
   ((db (sqlite3-open-restricted
	 (literal (aggregate-entity frame))
	 "askemos"
	 (make-block-handler block-table))))
   ;; (sqlite3-exec db "pragma journal_mode=OFF")
   ;; synchronous seems do damage:
   ;; (sqlite3-exec db "pragma synchronous=OFF")
   (sqlite3-exec db "pragma journal_mode=MEMORY")
   (sqlite3-setup-environment+ db frame message)))

;; This is not nice, we should shadown the original binding, not
;; temporary work around by appendng a "2" to the name and change call
;; sites.  Too bad: later added even more dispatch here to support prepared queries.
(define (sql-exec-protected2 db query)
  (and db (if (vector? query)
	      (apply sqlite3-exec db (sqlite3-prepare db (vector-ref query 0)) (vector-ref query 1))
	      (sql-exec-protected db query))))

(: make-reader-access (:place: (procedure ((or symbol string)) *) -> procedure))
(define (make-reader-access frame message)
  (let ((properties-snapshot (aggregate-meta frame))
	(sqlite-read-connection (xthe :db-conn-state: (make-mutex 'sqlite-read))))

    (define (get-sqlite-read-connection)
      (let ((v sqlite-read-connection))
	(if (mutex? sqlite-read-connection)
	    (with-mutex
	     v
	     (if (eq? v sqlite-read-connection)
		 (set! sqlite-read-connection
		       (and-let* ((blob (resolve-else 'sqlite3 properties-snapshot)))
				 (sqlite3-open-ro/f_m_b
				  frame message
				  (place/make-block-table
				   (aggregate-entity frame)
				   (resolve-else 'replicates properties-snapshot)
				   blob)))))
	     sqlite-read-connection)
	    v)))

    (define (reader-access slot . system)
      (if (null? system)
          (case slot
            ((body/parsed-xml)
             (aggregate-body frame))
            ((mind-body) (aggregate-body/plain frame))
            ((fold-links) (make-link-folder properties-snapshot))
            ((fold-links-ascending) (make-link-folder-ascending properties-snapshot))
            ((mind-links)  (resolve-mind-links properties-snapshot))
            ((cached:mind-body) (get-slot properties-snapshot 'mind-body))
            ((cached:body/parsed-xml)
             (get-slot properties-snapshot 'body/parsed-xml))
            ((replicates) (replicates-of frame))
            ((secret) (error "A secret is a secret."))
            (else (resolve-else slot properties-snapshot)))
          (case (car system)
            ;; TODO restrict to master place when distributing
            ;; until slave is in sync.
            ((reader) (make-sender #f frame message (message 'dc-date)))
            ((id) (aggregate-entity frame))
            ((my-rights)
	     (if (eq? (aggregate-entity frame) slot)
		 (append (get-slot properties-snapshot 'capabilities)
			 (get-slot properties-snapshot 'potentialities))
		 (get-my-rights-from reader-access message slot)))
            ((serve) (apply (make-service-level (fget frame 'protection)
                                                (message 'capabilities))
                            slot))
            ((xslt-transform)
	     (if (pair? slot)
		 (apply make-xslt-transformer* reader-access message slot)
		 (make-xslt-transformer reader-access message)))
	    ;; FIXME remove that again
	    ((sqlite-read-connection) (get-sqlite-read-connection))
	    ((sql-query) (sql-exec-protected2 (get-sqlite-read-connection) slot))
            ((TrustedCode)
             (lambda (arg)
               (let ((f (get-trusted-code (or (and (string? slot) slot)
                                              (symbol->string slot))
                                          (aggregate-entity frame)
                                          #f)))
                 (if f (f arg) (error (format #f "No trust for ~a" slot))))))
            ((metainfo) (metainfo reader-access message slot))
            ((metainfo-adopt) (metainfo reader-access message slot adopt: #t))
            ((replicates) (if (eq? slot text/xml) (replicates-of/xml frame) (replicates-of frame)))
	    ((finalize)
	     (if (not (mutex? sqlite-read-connection))
		 (begin
		   (if sqlite-read-connection (sqlite3-close sqlite-read-connection))
		   (set! sqlite-read-connection #f))))
            (else (error (format #f "unsupported: ~a on ~a (in r/o mode)"
                         (car system) (aggregate-entity frame)))))))
    reader-access))

;;**** commiting frames

;; FIXME: values beyond zero have turned to be risky since replication
;; became dependent on blobs beeing listed in the data base.

(define $max-direct-string-length 0)

;; We've got a lengthy commit action.  This might even change.

;; Here we write into the storage system.  Multiple network
;; transparent storage (like freenet etc.) will be pluged in here
;; ASAP.

;; look-ahead-reader resolve as if the commit action already happened.
;; That's a bit tricky because we want caching effects.  We need such
;; a beast, when performing 'write to storage "media" actions'.

;; Return 3 values: the old value, new value and a change signal.  The
;; change signal can be: 'update 'insert 'delete and #f.  The latter
;; for unchanged slots.

;; TODO: make "make-look-ahead-reader" work for changed mind-body!

(define-macro (direct-cache-resolve-slot frame0 slot frame)
  `(case ,slot
     ((body/parsed-xml) (message-body* ,frame (aggregate-entity ,frame0)))
     ((mind-body) (message-body/plain ,frame))
     (else (get-slot ,frame ,slot))))

(define (make-look-ahead-reader deleted-slot frame changes new-links)
  (lambda (key)
    (if
     (symbol? key)
     (let ((new-value (case key
			((mind-links) (and (pair? new-links) new-links))
			(else (direct-cache-resolve-slot frame key changes)))))
       (cond
        ((and new-value (eq? new-value deleted-slot))
         (change-signal #f (resolve-else key frame)))
        (else (change-signal new-value (direct-cache-resolve-slot frame key frame)))))
     (let ((new-link (assoc key new-links)))
       (if new-link
           (change-signal (cdr new-link)
                          (resolve-else key frame))
           (let ((v (resolve-else key frame))) (change-signal v v)))))))

(define (make-forceful-look-ahead-reader frame)
  (lambda (key)
    (let ((v (resolve-else key frame)))
      (values v v 'update))))

(define (cache-commit-frame! deleted-slot frame changes new-links)
  ;; Separate BLOB's
  (if $max-direct-string-length
      (let ((body (message-body/plain changes)))
	(if (and (string? body)
		 (> (string-length body) $max-direct-string-length))
	    (let ((blob (a:make-blob
			 (string->symbol (sha256-digest body))
			 (string-length body)
			 (string-length body)
			 1
			 #f)))
	      (set-frame-body/plain!
	       changes (store-blob-value
			*the-registered-stores* blob body))
	      ;; If the body is set to whatever value, we try to
	      ;; unify it with a stored BLOB notation, otherwise
	      ;; keep it as a fresh one.  It might be
	      ;; interesting to clean the slot instead of this
	      ;; unification.  Profiling should tell.
	      (if (frame-body/xml changes)
		  (let ((ot (fetch-blob-notation
			     *the-registered-stores* blob text/xml)))
		    (if ot
			(set-frame-body/xml! changes ot)
			(store-blob-notation
			 *the-registered-stores* blob text/xml
			 (delay* (frame-body/xml changes) (blob-sha256 blob))))))))))
  ;; TODO: request (and maybe convert) bhob surrogates
  )

;; Make sure everything is dumped down into any activated storage system.
(define (commit-frame-into-the-registered-stores! deleted-slot frame changes new-links)
  (let ((r (if (eq? changes (aggregate-meta frame))
	       (make-forceful-look-ahead-reader (aggregate-meta frame))
	       (make-look-ahead-reader deleted-slot (aggregate-meta frame) changes new-links)))
	(e (aggregate-entity frame)))
    (commit-frame-into *the-registered-stores* e r new-links)))

(: default-commit-frame! :commit-frame!:)
(define (default-commit-frame! deleted-slot frame changes new-links)
  (if (or (pair? new-links)
          (eq? changes (aggregate-meta frame)) ; this is a forced write
          (frame-changeset-modified? changes (aggregate-meta frame)))
      (begin
	(cache-commit-frame! deleted-slot frame changes new-links)
	(commit-frame-into-the-registered-stores! deleted-slot frame changes new-links)
	;; reduce blob/block-table change signals to current value
	(for-each
	 (lambda (slot)
	   (let ((v (get-slot changes slot)))
	     (if (and (vector? v) (a:blob? (vector-ref v 0)))
		 (set-slot! changes slot (vector-ref v 0)))))
	 '(sqlite3 mind-body))
        ;; Now we modify the primary data base.
        (if (not (null? new-links))
            (let* ((old (get-slot (aggregate-meta frame) 'mind-links))
                   (links (make-mind-links)))
              (if old (links-merge! links old))
              (set-slot! changes 'mind-links links)
              (for-each (lambda (link)
                          (if (cdr link)
                              (link-bind! links (car link) (cdr link))
                              (link-delete! links (car link))))
                        new-links)))

	;; Commented out here the intented way.  This fails with
	;; asynchronous commits, since the frame might have been moved
	;; into the persistant area.
	;;
        ;; (set-aggregate-meta! frame changes)
	;;
	;; FIXME: need to change the signature all the way up, since
	;; "mind-pool" SHOULD be passed as a parameter.  This way we
	;; break the abstraction barrier accessing "mind-pool" from
	;; another module (even though it's still at the TODO list to
	;; complete this module separation).
	;;
	(set-aggregate-meta! frame changes)
	(bind-place! (aggregate-entity frame) frame #t)
	;; (commit-frame-into-the-registered-stores! #f frame (aggregate-meta frame) '())

        (mind-commit))))

;;(set!-commit-frame! default-commit-frame!)

(define (frame-commit! oid)
  (let ((frame (document oid)))
    (commit-frame! #f frame (aggregate-meta frame) '())))

;;**** SQL updates.

;; FIXME the newly implemented sqlite3 asynchronous call mechanism
;; doesn't work anymore since sqlite3 now checks that all calls come
;; from the same thread.  Hence we need to rewrite the interface code
;; completely.  Hope this will fix the FreeBSD version too.
;;
;; As a workaround we resort to 'sqlite3-exec' instead of
;; 'sqlite3-async-exec'.  Which blocks and thus is BAD.

(define $default-sql-block-size (make-parameter 32768))
(define $default-sql-bpb (make-parameter 200))

(define (do-commit-sql! frame message changes sql-requests)
  (if (pair? sql-requests)
      (let* ((last (fget frame 'sqlite3))
	     (tbl (place/make-block-table (aggregate-entity frame)
					  (replicates-of frame)
					  last
					  ($default-sql-block-size)
					  ($default-sql-bpb)))
	     (db (sqlite3-open/f_m_b frame message tbl))
	     (execution-state 0))
	(guard
	 (c (else
	     (log-condition "commit-sql" c)
	     ;; Worse: At least with SQLite 3.21 a failed rollback
	     ;; will result in a SIGSEGV in `btreeLockCarefully` :-/
	     (if (fx< execution-state 1)
		 (sqlite3-exec db "ROLLBACK"))
	     (if (fx< execution-state 2)
		 (sqlite3-close db))
	     (raise c)))
	 (sqlite3-exec db "PRAGMA auto_vacuum = 1")
	 (sqlite3-exec db "BEGIN IMMEDIATE TRANSACTION")
	 (let ((result (map
			(lambda (request)
			  ;; (sqlite3-async-exec db request)
			  (guard
			   (c (else (log-condition (format "~a" request) c)
				    (raise c)))
			   (apply sqlite3-apply db request))
			  )
			(reverse! sql-requests))))
	   (sqlite3-exec db "COMMIT")
	   (set! execution-state 2) ;; don't close on exception
	   ;; (sqlite3-bugworkaround-reset-restrictions db) ;; "less risky"
	   ;; (sqlite3-exec db "VACUUM") ;; "less risky"
	   (set! execution-state 1) ;; don't rollback on exception
	   (sqlite3-close db)
	   (let ((result (call-with-values
			     (lambda () (block-table-flush! tbl last))
			   vector)))
	     (set-slot! changes 'sqlite3 result)
	     result))))
      sql-requests))

(define (no-commit-sql! frame message changes sql-requests) sql-requests)

(define commit-sql!.1 do-commit-sql!)

(define commit-sql!.2 no-commit-sql!)

;; I'm factoring that one out of the big dispatcher closure.  It's
;; also required for "late commit".

;; This
;; (define commit-sequence '(1 2))
;; would be safer - I guess.  But quite a slow down

(define commit-sequence '(2 1))

(define (commit-proposal! cache? commit-date frame message version proposal)
  (receive
   (digest date changes new-links sql messages)
   (list->values proposal)

   ;; KLUDGE, we have a strange situation (2004-04-09), where
   ;; approximately once a day 'complete-transaction' is apparently
   ;; called twice concurrentely.  That's bad, as it seems clearly
   ;; protected by semaphore.  See 'respond!' where it is only ever
   ;; called.  To trace the bug back and make it ineffective the same
   ;; time, we do some more sanity checks here:
   (let ((next (and-let* ((old (get-slot (aggregate-meta frame) 'version)))
                         (add1 (car old)))))
     (cond
      ((and version (eqv? version next))
       (set-slot!
        changes 'version
        (cons next (or digest (error "commit-proposal missing digest"))))
       (set-slot! changes 'last-modified date))
      ((not version))
      (else (error " ~a commit-proposal ~a version ~a recommit attempt ~a\n"
                   (current-thread) (aggregate-entity frame)
                   (get-slot (aggregate-meta frame) 'version) proposal))))

   (if (eq? cache? 'commit-cache)
       (begin
	 (commit-sql!.2 frame message changes sql)
	 (commit-frame! %deleted-slot frame changes new-links))
       (let loop ((x commit-sequence))
	 (cond
	  ((null? x) #f)
	  ((eqv? (car x) 1)
	   (let ((old-quorum (get-slot (aggregate-meta frame) 'replicates)))
	     (commit-sql!.2 frame message changes sql)
	     (commit-frame! %deleted-slot frame changes new-links)
	     ((invite-new-supporters!)
	      (aggregate-entity frame) old-quorum (get-slot changes 'replicates)))
	   (loop (cdr x)))
	  ((eqv? (car x) 2)
	   (for-each
	    (lambda (args)
	      (apply (make-sender #t frame message date) args))
	    messages)
	   (loop (cdr x))))))))


;;**** make-access

;; Make-access essentially implements the semantic invariants of all
;; the client languages.

;; You see four things here woven into each other:
;;
;; a) some caching between parsed and plain body data for sake of
;; speed
;;
;; b) hiding the mechanism ("mind-links"), which implements the
;; name/value mapping each context (place) has
;;
;; c) permission invariants.
;;
;; d) transaction reset. (This feature was never used, since there's
;; an outer transaction restart mechanism, which will assure that the
;; closure made by "make-access" is discarded immediately after the
;; reset.  Since I've found the "reset-transaction" code kind of
;; strange and the other solution much more elegant, I'm leaving it
;; here merly for safe codeing.)

;; Yes, that's a bit complicated.  I should separate the aspects.

(define (signal-access-violation frame)
  (error (format #f "Illegal access on ~a\n" (aggregate-entity frame)))
  ;;(if #f (send-email $administrator
  ;;         (format #f "Illegal access on ~a\n" frame)
  ;;           `((From ,$administrator)
  ;;             (Subject "Illegal access"))))
  )

(define %deleted-slot '(deleted-slot))

(define *dump-message-signal* '(*dump-message-signal*))

(define (make-access frame message)
  (let ((properties-snapshot (aggregate-meta frame))
        (new-links '())
        ;; cache slot changes until commit:
        (changes (frame-clone (aggregate-meta frame)))
        (messages '())
        (sql '())
	(sqlite-read-connection (xthe :db-conn-state: (make-mutex 'sqlite-read)))
        (islocked #t))

    (define (get-sqlite-read-connection)
      (let ((v sqlite-read-connection))
	(if (mutex? v)
	    (with-mutex
	     v
	     (if (eq? v sqlite-read-connection)
		 (set! sqlite-read-connection
		       (let* ((last (resolve-else 'sqlite3 properties-snapshot))
			      (tbl (place/make-block-table
				    (aggregate-entity frame) (replicates-of frame)
				    last
				    ($default-sql-block-size)
				    ($default-sql-bpb))))
			 (sqlite3-open-ro/f_m_b frame message tbl))))
	     sqlite-read-connection)
	    v)))

    (define (proposed-changes)
      (list (askemos:dc-date (message 'dc-date))
            changes new-links (commit-sql!.1 frame message changes sql) messages))

    (define (reset-transaction proposal)
      (receive
       (date c nl s m)
       (list->values (transaction-effects proposal))
       (set! changes c)
       (set! new-links nl)
       (set! sql s)
       (set! messages m)
       (set! islocked #t)))

    (define (writer slot value)
      (if islocked
          (case slot
            ((body/parsed-xml)
	     (let ((old (or (frame-body/xml changes) (frame-body/xml properties-snapshot))))
	       (if (not (and value old (eq? (document-element old) (document-element value))))
		   (begin
		     (set-frame-body/plain! changes #f)
		     (set-frame-body/xml! changes value)))))
            ((mind-body)
             (if
              (not (eq? (frame-body/plain changes) value))
              (begin
                (set-frame-body/xml! changes #f)
                (set-frame-body/plain! changes value))))
            ((mind-links proposal)
             (error "forbidden to write mind-links or proposal"))
            ((capability: capability)
             ;; Rethink: these checks:
             ;; Direct trust: message must originate (for now) directly
             ;; from the user, no itermediaries.
             (if (or (not (eq? (message 'caller) (message 'dc-creator)))
                     ;; don't accept the full right of the sender
                     (and (full-right? (list (message 'dc-creator))
                                       (list value))
                          ;; except for the special "grant" to self, when
                          ;; we enable saved potentialities.
                          ;; This seems to deserve a cleanup.
                          (not (eq? (message 'dc-creator)
                                    (aggregate-entity frame)))))
                 (error (format #f "can't grant ~a" value)))
             (let ((new
                    (accept-right
                     access message value
                     (get-slot changes 'capabilities))))
               (if new (set-slot! changes 'capabilities new)) new))
            ((potentiality:)
             (if (not (resolve-else 'secret properties-snapshot))
                 (error "Potentialities are only defined for entry points."))
             (let ((new
                    (accept-right
                     access message value
                     (or (resolve-else 'potentialities changes)
                         (resolve-else 'potentialities properties-snapshot)
                         '()))))
               (if new (set-slot! changes 'potentialities new) new)))
            ((potentialities)
             ;; We could just throw and exception to protect the slot
             ;; from arbitrary changes.  But then we would need another
             ;; call to remove it.  The potentialities slot should
             ;; only be used in the rights section, so we assume
             ;; implementors to know about the missnomer.
             ;; (error "Slot potentialities used by system.")
             (let ((new (filter
                         (lambda (p) (not (equal? p value)))
                         (or (get-slot changes 'potentialities)
                             (resolve-else 'potentialities properties-snapshot)
                             '()))))
	       (set-slot! changes 'potentialities new)))
            ((capabilities: capabilities)
             (let ((new (self-enslave access message value)))
	       (set-slot! changes 'capabilities new) new))
            ((secret: secret)
             (if value (set-slot! changes 'secret value)) #t)
            ((replicates)
	     (let ((q (if (quorum? value) value (make-quorum value))))
	       (set-slot! changes 'replicates q) q))
            (else (if value
                      (set-slot! changes slot value)
                      (set-slot! changes slot %deleted-slot)) value))
          (signal-access-violation frame)))

    (define (access slot . system)
      (if (null? system)
          (case slot
            ((body/parsed-xml)
	     (or (get-slot properties-snapshot 'body/parsed-xml)
		 (let ((p (message-body*
			   properties-snapshot (aggregate-entity frame))))
		   (set-slot! changes 'body/parsed-xml p) p)))
            ((mind-body)
	     (or (get-slot properties-snapshot 'mind-body)
		 (let ((p (message-body* properties-snapshot (aggregate-entity frame))))
		   (set-slot! changes 'mind-body p) p)))
            ((cached:mind-body) (get-slot properties-snapshot 'mind-body))
            ((cached:body/parsed-xml)
             (get-slot properties-snapshot 'body/parsed-xml))
            ((fold-links) (make-link-folder properties-snapshot))
            ((fold-links-ascending) (make-link-folder-ascending properties-snapshot))
            ;; DEPRECIATED mind-links is going to be removed.
            ((mind-links) (resolve-mind-links properties-snapshot))
            ((secret) (error "A secret is a secret."))
            ((replicates) (replicates-of frame))
            (else (resolve-else slot properties-snapshot)))
          (case (car system)
            ((writer set!)
             ;; (set-frame-changeset-modified! changes properties-snapshot)
             writer)
            ((sql) (set! sql (cons slot sql)) #t)
            ((linker link!)
             (lambda (name value)
               ;; Rscheme can't store procedures persistent. Die here.
               (if (procedure? value) (error "can't link procedures")
                   (if islocked
                       (set! new-links `((,name . ,value) . ,new-links))
                       (signal-access-violation frame)))))
            ((sender send!)
             (lambda args
               (if islocked
                   (set! messages (cons args messages))
                   (signal-access-violation frame))))
            ((reader)
	     (make-sender (eq? slot *dump-message-signal*)
			  frame message (askemos:dc-date (message 'dc-date))))
            ((replicates) (if (eq? slot text/xml) (replicates-of/xml frame) (replicates-of frame)))
            ((resynchronize)
             (access 'do 'rollback)
             ((resynchronize) (access 'quorum 'replicates) (aggregate-entity frame)))
            ;; CAUTION user-context is not official.  It's too strange
            ;; to code such things here.  Use the user-context
            ;; function only!
            ((user-context) (make-context (message 'dc-creator)))

            ;; ** Internal Operations **
            ;;
            ;; Further versions might lock out user code from calling these.

            ;; commit signals the end of the transaction.
            ;; If we where to optimize the writer as discussed above,
            ;; we would have to modify the frame at once here.
            ((commit-cache commit-version)
             (if islocked
                 (begin
                   (set! islocked #f)
		   (if (not (mutex? sqlite-read-connection))
		       (begin
			 (if sqlite-read-connection (sqlite3-close sqlite-read-connection))
			 (set! sqlite-read-connection #f)))
                   (commit-proposal!
		    (car system)
                    ;; KLUDGE find out whether we actually
                    ;; have cases, where the slot value is
                    ;; not a date and fix those calls.
                    (vector-ref slot 0)
                    frame message
                    (and-let* (((eq? (car system) 'commit-version))
                               (current (resolve-else
                                         'version properties-snapshot)))
                              (add1 (version-serial current)))
                    (cons
		     (if (eq? (car system) 'commit-version)
			 (vector-ref slot 1)
			 (version-chks (resolve-else 'version properties-snapshot)))
		     (proposed-changes))))
                 (error "too late to commit changes")))
	     ;; Restore state from previously stored proposal in "re-transaction".
            ((restore-proposal-from-cache)
             (if islocked
                 (reset-transaction slot)
                 (error "commit-proposal-from-cache: not locked")))
	    ((commit-export-proposal) (proposed-changes))
            ((rollback)
	     (set! islocked #f)
	     (if (not (mutex? sqlite-read-connection))
		 (begin
		   (if sqlite-read-connection (sqlite3-close sqlite-read-connection))
		   (set! sqlite-read-connection #f)))
	     #f)
            ((affirm) (secret-verify (resolve-else 'secret properties-snapshot) slot))
            ((my-rights)
	     (if (eq? (aggregate-entity frame) slot)
		 (append (get-slot properties-snapshot 'capabilities)
			 (get-slot properties-snapshot 'potentialities))
		 (get-my-rights-from access message slot)))
            ((serve) (apply (make-service-level
                             (resolve-else 'protection properties-snapshot)
                             (message 'capabilities))
                            slot))
            ((id) (aggregate-entity frame))
            ((metainfo) (metainfo access message slot))
            ((metainfo-adopt) (metainfo access message slot adopt: #t))
            ((xslt-transform) 
	     (if (pair? slot)
		 (apply make-xslt-transformer* access message slot)
		 (make-xslt-transformer access message)))
	    ;; FIXME remove that again
	    ((sqlite-read-connection) (get-sqlite-read-connection))
	    ((sql-query) (sql-exec-protected2 (get-sqlite-read-connection) slot))
            ((TrustedCode)
             (lambda (arg)
               (let ((f (get-trusted-code slot
                                          (aggregate-entity frame)
                                          islocked)))
                 (if f (f arg) (error (format #f "No trust for ~a" slot))))))
	    ((finalize)
	     (if (not (mutex? sqlite-read-connection))
		 (begin
		   (if sqlite-read-connection (sqlite3-close sqlite-read-connection))
		   (set! sqlite-read-connection #f))))
            (else
             (error
              (format #f
               "Internal Error: ~a Accessor called with illegal arguments: ~a: ~s"
               (aggregate-entity frame)
               slot
              (car system)))))))
    access))

;; 'respond' - a synchronous answer to a request.  This must not be
;; exposed to client code or we could easily write a dead lock.

(define (respond-local me method restart request)
  (values
   (within-time
    (respond-timeout-interval)
    (if (pair? method)
	((or (cdr method) (error "respond-local invalid method"))
	 me request
	 (if (car method) ((car method) me request) 'dummy))
	(method me request)))
   #f))

;; BEWARE better avoid use of this KLUDGE alltogether.  However I need
;; to work around the wiki not loading within time and I can't really
;; see why it get's blocked.  It looks a bit as if the semaphore is
;; not freed, when the load runs into a timeout.  But that could be
;; equally well a false guess.

(define (respond-local-no-timeout me method restart request)
  (values
   (if (pair? method)
       ((or (cdr method) (error "respond-local invalid method"))
	me request
	(if (car method) ((car method) me request) 'dummy))
       (method me request))
   #f))

;; TODO see whether we better change the API to support passing of
;; cache/no-version vs. new-version type commits.  I just found that
;; we need to distinguish and have to do the quick fix for now.

(define respond-cache respond-local)

;; BEWARE The byzantine agreement code is once again much under
;; reconstruction.  And here we prepare for future changes.

(define (respond-digest tree)
  (md5-digest (if (string? tree) tree (xml-format tree))))

(define (respond-replicated access method restart request)
  (respond-local access method restart request))

(define respond-making-laune respond-replicated)

;;** ProcessStep

;;*** The actual stepper.

;; For nested commits see: http://lambda-the-ultimate.org/node/1539
;;
;; In global computing applications the availability of a mechanism
;; for some form of committed choice can be useful, and sometimes
;; necessary. It can conveniently handle, e.g., contract stipulation,
;; distributed agreements, and negotiations with nested choice points
;; to be carried out concurrently. We propose a linguistic extension
;; of the Join calculus for programming nested commits, called
;; Committed Join (cJoin).  It provides primitives for explicit abort,
;; programmable compensations and interactions between ongoing
;; negotiations. We give the operational semantics of cJoin in the
;; reflexive CHAM style. Then we discuss its expressiveness on the
;; basis of a few examples and of the cJoin encoding of other
;; paradigms with similar aims but designed in different contexts,
;; namely AKL and Zero-Safe nets.

(define $eager-force-transaction (make-shared-parameter #t))

(define (quorum-make-transaction/frame-request frame message)
  (let ((quorum (replicates-of frame))
	(on (aggregate-entity frame))
	(request (make-reader message)))
    (define access (make-access frame request))
    (define treshold (respond-treshold quorum))
    (let* ((method (nu-action (fget frame 'mind-action-document)
			     'write frame (let ((context-quorum quorum))
					    context-quorum)))
	   (job (lambda ()
		  (let ((version (access 'version)))
		    (receive
		     (result . digest)
		     (within-time
		      (respond-timeout-interval)
		      (parameterize
		       ((current-place access)
			(current-message request))
		       (if (pair? method)
			   ((car method) access request)
			   (method access request))))
		     (values
		      (let ((proposal (access 'do 'commit-export-proposal)))
			(access 'do 'rollback)
			proposal)
		      result
		      (cons (version-serial version)
			    (if (pair? digest) (car digest) (respond-digest result))))))))
	   (fulljob (if (and ($eager-force-transaction) $agree-verbose)
			(lambda () (guard (ex (else (log-condition on ex) (raise ex)))
					  (job)))
			job))
	   (t (%make-transaction
	       on
	       (if ($eager-force-transaction)
		   (!order/thunk fulljob (dbgname (oid->string on) "compute ~a"))
		   (delay*thunk fulljob (dbgname (oid->string on) "compute ~a")))
	       #f		; not ready
	       (reliable-broadcast-required-echos quorum treshold)
	       (quorum-others quorum) ; uninformed
	       (quorum-required-readys quorum treshold)
	       (quorum-others quorum) ; missing
	       )))
      t)))

(define (respond! frame method agreement transaction request)
  (let ((oid (aggregate-entity frame)))
    (if transaction
	(let ((versioned (eq? agreement respond-replicated))
	      (context (replicates-of frame)))
	  (if (and versioned (pair? (quorum-others context)))
	      (let ((version (or (fget frame 'version) (cons 0 oid))))
		(or (oid-channel-respond!
		     oid (version-serial version) version request transaction-completed?
		     (quorum-make-transaction/frame-request frame request)
		     (lambda ()
		       (find-frame-by-id/resynchronised+quorum oid context)))
		    (let ((ex (make-condition
			       &service-unavailable
			       'message (literal "response failed at " (oid->string oid)) 'status 503)))
		      (quorum-send-result! frame version request (condition->message #f request ex))
		      (raise ex))))
	      (let ((mutex (respond!-mutex oid))
		    (version (fget frame 'version))
		    (racc (make-reader request)))
		(let ((access (make-access frame racc)))
		  (retain-mutex
		   mutex
		   (receive
		    (result digest)
		    (parameterize
		     ((current-place access)
		      (current-message racc))
		     (agreement access method raise racc))
		    ;; KLUDGE beware
		    (access (vector
			     (askemos:dc-date (get-slot request 'dc-date))
			     (or digest "--no audit--")) ;; result
			    (if versioned 'commit-version 'commit-cache))
		    (quorum-send-result! frame version request result)
		    result))))))
	(let loop ((frame frame) (retry 10) (ex #f))
	  (if (fx>= retry 1)
	      (let* ((request (make-reader request))
		     (access (make-reader-access frame request))
		     (result (guard
			      (ex (else
				   (access 'do 'finalize)
				   (cond
				    ;; FIXME: this case should only
				    ;; handle timeouts originating
				    ;; from object load, not from
				    ;; exessive computation.
				    ((timeout-object? ex)
				     (let ((retry (fx- retry 5)))
				       (if (fx>= retry 1)
				     	   (thread-sleep!
				     	    (or
				     	     (and-let* ((tmo (respond-timeout-interval)))
				     		       (min (/ tmo 4) 0.2))
				     	     0.2)))
				       (loop frame retry ex)))
				    ((missing-blob-condition? ex)
				     (loop
				      (begin (delete-is-sync-flag! oid)
					     (find-frame-by-id/resynchronised+quorum
					      oid (replicates-of frame)))
				      (sub1 retry)
				      ex))
				    (else (raise ex)))))
			      (within-time (respond-timeout-interval)
					   (parameterize
					    ((current-place access)
					     (current-message request))
					    (method access request))))))
		(access 'do 'finalize)
		result
		;; Maybe it would be a good idea to decorate read replies
		;; with the caller (sender, the one who replied) as
		;; well.  However this is at odds with our way to
		;; deliver constant places as they are, since we would
		;; modify those constants from several threads.
		)
	      (raise
	       (cond
		((timeout-object? ex)
		 (make-condition
		  &http-effective-condition
		  'status 408
		  'message (format "Read operation ~a timeout on ~a ."
				   (action-document oid) oid)))
		(else ex))))))
    ))

;; The procedure make-sender constructs is probably the most important
;; single procedure.  It's worth to note, that this is the only method
;; which can create a message to another object or create a new object
;; by sending a message without a receiver argument.  It doesn't
;; return any value, not even when the receiver is not found.  This
;; models the real world, not the traditional way of programming.

;; Please don't feel offended and live with that fact, it's so
;; important for the big picture, which we can't see just from the
;; source.

;;   FIXME the 2nd paragraph above cheats a bit about the return
;;   value.  There is a special message type "call", which returns the
;;   result of a read request to the receiver.  TODO I'm by no means
;;   convinced that we need the whole collect-results ~black
;;   magchanic.  But sometimes it's so convenient to just read an
;;   object instead of sending it a read message which's result is
;;   then stored.  Try to avoid the feature, maybe we can get along
;;   without it.  It's anyway only a shortcut and restricted to "read"
;;   messages.

;;   TODO I guess I better clean this piece of code a little bit.  It
;;   appear to be right, but it doesn't read simple.

(define (make-sender/resolve-name frame name)
  (cond ((oid? name) name)
        ((string? name)
         (let ((n (string->oid name)))
           (if n n (resolve-else name (aggregate-meta frame)))))
        (else #f)))

;(define (make-sender/resolve-name frame name)
;  (resolve-else name (aggregate-meta frame)))

(define (make-condition-message from condition)
  (make-message
   (property 'capabilities (public-capabilities))
   (property 'dc-creator from)
   (property 'dc-date (timestamp))
   (property 'content-type text/xml)
   (property
    'body/parsed-xml
    (receive
     (title msg args rest) (condition->fields condition)
     (make-soap-receiver-fault (oid->string from) title msg args)))))

;; Create a function, which sends a single request to another place.
;; TODO: Rethink collect-results ~black magchanic.
(define (make-sender-send-message
         collect-results                ; boolean synchronous operation
	 at				; replicates
         transaction                    ; boolean transaction flag
         from                           ; responsible oid
         free)                          ; other, free slots
  (lambda (to)
    (define t0 (car to))
    (let ((msg (message-clone free)))
      (set-slot! msg 'destination (cdr to))
      (if collect-results
	  (call-subject!
	   ;;  (if transaction find-frame-by-id/synchronised+quorum find-frame-by-id/quorum) ???
	   (or (find-frame-by-id/quorum	t0 at)
	       (raise (make-object-not-available-condition t0 at 'synchroneous-request)))
	   at (if transaction 'write 'read) msg)
	  (begin
	    (pass-a-ref			; in it's own thread
	     from
	     (with-mind-access
	      (lambda (msg)
		(guard
		 (c ((timeout-condition? c)
		     (let* ((text (format "~a ~a" from c))
			    (err (make-condition
				  &service-unavailable 'message text 'status 503)))
		       (user-debug from text)
		       (and-let* ((c (get-slot msg 'reply-channel)))
				 (mailbox-send! c err))
		       err))
		    (else
		     (if (or (eq? t0 from) (public-equivalent? from))
			 (if (enable-warnings)
			     (log-condition "Async send" (condition->string c)))
			 (guard
			  (c (else (log-condition "Async error notify" c)))
			  (let ((msg (make-condition-message from c)))
			    ;; (logerr "Error notify ~a ~a\n" msg c)
			    (user-debug
			     from
			     (list
			      "Asynchronous send operation failed."
			      (xml-format (get-slot msg 'body/parsed-xml))
			      (condition->string c)))
			    ;; The policy to deliver to the user seems
			    ;; to hinder WebDAV since the root is
			    ;; mostly busy completing transactions.
			    (let ((result
				   (if #f
				       (call-subject! (find-frame-by-id/quorum from at) at
						      'write msg)
				       msg)))
			      (logerr "sync send abort ~a\n" from)
			      result))))))
		 (call-subject!
		  (or (find-frame-by-id/quorum t0 at)
		      (raise (make-object-not-available-condition t0 at 'asynchroneous-request)))
		  at
		  (if transaction 'write 'read) msg)))
	      msg)
	     (oid->string t0))
	    #f)))))

(define $pishing-prevention #t)

(define (use-mandatory-capabilities v) (set! $pishing-prevention v))

(define prevent-pishing
  (begin
    (define (prevent-pishing public frame request)
      $pishing-prevention)
    prevent-pishing))

(define pishing-mode-compatible-forward-call
  '(pishing-mode-compatible-forward-call))

(define $use-inherited-protection (make-shared-parameter #t))

(define (make-sender r/o frame request dc-date)
  (define mandatory-capabilities-are-in-use
    (prevent-pishing public-oid frame request))
  (define (make-sender:make-msg+defaults request)
    (let ((result (make-message)))
      (set-slot! result 'accept (request 'accept))
      (set-slot! result 'reply-channel (request 'reply-channel))
      (set-slot! result 'dav-if (request 'dav-if))
      (set-slot! result 'location-format (request 'location-format))
      (let ((content-type (request 'content-type)))
	(cond                ; Should we really compute arbitrary
                                        ; default values?
	 ((xml-parseable? content-type)
	  (set-slot! result 'content-type content-type)
	  (let ((bx (request 'cached:body/parsed-xml))
		(bp (request 'cached:mind-body)))
	    (set-slot! result 'body/parsed-xml bx)
	    (set-slot! result 'mind-body bp)))
	 (else
	  (set-slot! result 'content-type content-type)
	  (set-slot! result 'mind-body (request 'mind-body)))))
      result))
  (lambda args
    (let ((to (xthe (list-of list) '()))
          (from (aggregate-entity frame))
	  (at (fget frame 'replicates))
	  (dc-creator (request 'dc-creator))
          (transaction (xthe boolean #f))
          (collect-results (xthe boolean #f))
          (capabilities
	   (if mandatory-capabilities-are-in-use
	       (cons
		(list (aggregate-entity frame) (my-oid))
		(append (public-capabilities)
			(let ((max (request 'capabilities)))
			  (filter (lambda (c)
				    (dominates? c max))
				  (fget frame 'capabilities)))))
	       (request 'capabilities)))
          (free (make-sender:make-msg+defaults request)))
      (do ((rest args (cddr rest)))
          ((null? rest)                 ; now build the message
           (set-slot! free 'dc-creator (or dc-creator (aggregate-entity frame))) ;; Maybe enforce it initially above?
           (set-slot! free 'capabilities capabilities)
           (set-slot! free 'dc-date dc-date)
           ;; Nervous systems and communication depend (trust) their
           ;; assumption of the sender of the signal.
           ;; TODO move reference: for VLSI implementation see
           ;; http://www.ini.unizh.ch:80/%7Eamw/scx/aeprotocol.html
           (set-slot! free 'caller from)
           ;; TODO: Rethink collect-results ~black magchanic.
           (if (null? to)
               (if transaction
                   (begin
		     ;; When creating a normal place without a given 
		     ;; protection we inherit it from the creating place
		     ;;
		     ;; KLUDGE: That's debatable, why should we expend
		     ;; any processing time to default to anything?
		     ;; This incures processing time, code size,
		     ;; documentation and metal effort on the user
		     ;; side.  I would prefer to leave this kind of
		     ;; defaults to a thin layer right before the user
		     ;; code get's to work with values, not deep down
		     ;; in the message handling system.
		     ;;
		     ;; So I'll leave it commented out until I'm
		     ;; convinced of the need.
		     ;;
		     ;; (and ($use-inherited-protection)
		     ;;      (not (get-slot free 'protection))
		     ;;      (not (get-slot free 'secret)))
		     ;;      (set-slot! free 'protection (fget frame 'protection)))

		     ;; Discretionary capabilities carry all possible
		     ;; intentions, be it by lazyness, fraud or
		     ;; accident.  That's just too much, hence we
		     ;; remove them altogether.  See also dump-message.
		     (if (not mandatory-capabilities-are-in-use)
			 (set-slot! free 'capabilities '()))

		     ;; XXX If there are no replicates given, inherit
		     ;; it from the creating place:
		     (if (not (get-slot free 'replicates))
			 (set-slot! free 'replicates (replicates-of frame)))

		     (dump-message! from free)) ; returns id
                   (error "'new' must only be called from transactions"))
               (map (make-sender-send-message
                     collect-results at transaction dc-creator free)
                    to)))
        (if (null? (cdr rest))
            (domain-error 'make-sender "send!: key ~a without argument" (car rest)))
        (case (car rest)
          ((to:)
           (let* ((v (cadr rest))
                  ;; Hm, that's not the default lookup.  No good
                  ;; reason. Confusing?
                  (oid (make-sender/resolve-name
                        frame (if (pair? v) (car v) v))))
             (if (oid? oid)
                 (set! to (cons (if (pair? v) (cons oid (cdr v)) (list oid))
                                to))
		 (raise (make-object-not-available-condition
			 v (aggregate-entity frame) 'make-sender)))))
          ((type:)
           (let ((arg (cadr rest)))
	     (if (not r/o)
		 (set! arg (and (or (eq? arg 'call)
				    (eq? arg pishing-mode-compatible-forward-call))
				arg)))
	     (cond
	      ((member arg '(#t write "PUT" "POST"))
	       (set! transaction #t)
	       (set! collect-results #f))
	      ((eq? arg 'call)
	       (set! transaction #f)
	       (set! collect-results #t))
	      ((eq? arg pishing-mode-compatible-forward-call)
	       (set! transaction #f)
	       (set! dc-creator (request 'dc-creator))
	       (set! capabilities (request 'capabilities))
	       (set! collect-results #t))
	      ;;why that? asyncron read may be usefull for cache prefetches
	      (else (set! transaction #f)
		    (set! collect-results #f)))))
          ;; Adopt accepts either a hard coded desired capability,
          ;; which the server (creator of the place) must have active
          ;; and had active when creating the place or just #t in
          ;; which case all the capabilities assigned to the place are
          ;; used as the desired capability.
          ((adopt: adopt)
           (if (cadr rest)
               (let ((max (fget frame 'capabilities))
		     (public (public-capabilities))
                     (maximum-from-server
                      (delay
			(or (fget (or (find-frame-by-id/quorum (fget frame 'dc-creator) at)
				      (raise-service-unavailable-condition
				       (oid->string (fget frame 'dc-creator))))
				  'capabilities)
			    (public-capabilities)))))
                 (set! capabilities
                       (cons (list (aggregate-entity frame) (my-oid))
			     (filter
			      (let ((topological
				     (list (list (aggregate-entity frame)))))
				(lambda (c) (or
					     (dominates? c topological)
					     (dominates? c public)
					     (and (dominates? c max)
						  (dominates? c (force maximum-from-server))))))
			      (or (and (pair? (cadr rest)) (cadr rest))
				  (append public max)))))
                 (set! dc-creator from))))
          ((capabilities:)
           (for-each (lambda (c)
                       (if (not ((make-service-level c capabilities)))
                           (error (format #f "can't set capability ~a" c))))
                     (cadr rest))
           (set! capabilities (cadr rest)))
          ((dc-date dc-creator capabilities)
           (error "cheating forbidden"))
          ((body:)                      ; Experimental definition,
                                        ; please report about usability.
           (cond
            ;; FIXME what about output elements?
            ((node-list? (cadr rest))
             (set-slot! free 'mind-body #f)
             (set-slot! free 'body/parsed-xml (cadr rest))
             (set-slot! free 'content-type text/xml))
            ((string? (cadr rest))
             (set-slot! free 'body/parsed-xml #f)
             (set-slot! free 'mind-body (cadr rest)))
            (else 
             (set-slot! free 'body/parsed-xml #f)
             (set-slot! free 'mind-body #f))))
          ((body/parsed-xml)            ; DEPRECIATED, will be removed
           (set-slot! free 'mind-body #f)
           (set-slot! free 'body/parsed-xml (cadr rest)))
          ((mind-body)
           (set-slot! free 'body/parsed-xml #f)
           (set-slot! free 'mind-body (cadr rest)))
	  ((dav-if)
	   (error "dav-if slot is read only"))
          ;; Relief! Just remember the value for general slots.
	  (else (set-slot! free (car rest) (cadr rest))))))))

;;* Rights Section

;; potentialities: "disabled" capabilities

;; Add capability to the set if this is legal.

(define (accept-right place message value old)
  (if (not (or (full-right? value (message 'capabilities))
               ;; if the message is too weak we might have it saved.
               (full-right? value (place 'potentialities))))
      (raise-precondition-failed (format #f "Capa ~a not in ~a"  value (message 'capabilities))))
  (if (not (member value old))
      (cons value old)
      #f))

(define (self-enslave place message capabilities)
  ;; Acceptable if all desired capabilities are covered by the
  ;; capabilities of the message.  This might be not enough.  If one
  ;; was to join forces, we need the same effect as with "adopt".  But
  ;; this should be explicit and not here.
  (if (let loop ((c (message 'capabilities))
                 (v capabilities))
        (cond
         ((null? v) #t)
         ((full-right? (car v) c) (loop c (cdr v)))
         (else #f)))
      capabilities
      ;; (place 'capabilities) ??? would a silent fail be better?  for
      ;; the time being we cry, 2002-02-23: I think now we better cry.
      (raise-precondition-failed (format #f "capabilities ~a not covered" capabilities))))
