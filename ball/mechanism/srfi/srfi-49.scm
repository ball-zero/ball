;; Egil Möller (2003) ported to RScheme by Jörg F. Wittenberger

;; TODO This code should be rewritten to use VALUES instead of CONS to
;; return multiple values from 'readblock' etc.  I tried to convert it
;; but without trying to understand the code completely...since I just
;; found a side effect I've overlooked in the first pass and this one
;; has rather strange implications.  Lookk for "set! keep" in
;; 'readblock-clean'.

(define group 'group)
(define dot (list 'dot))

(define sugar-stop (list group))

(define sugar-read-save read)

(define (readquote readblock-clean level port qt)
  (let ((char (peek-char port)))
    (if (or (eqv? char #\space)
	    (eqv? char #\newline)
	    (eqv? char #\return)
	    (eqv? char #\tab))
	(receive (level block)
		 (readblocks readblock-clean level port)
		 (cons qt block))
	(list qt (sugar-read-save port)))))

(define (readitem readblock-clean level port)
  (case (peek-char port)
    ((#\`)
     (read-char port) (readquote readblock-clean level port 'quasiquote))
    ((#\')
     (read-char port) (readquote readblock-clean level port 'quote))
    ((#\,)
     (read-char port)
     (if (eqv? (peek-char port) #\@)
	 (begin
	   (read-char port)
	   (readquote readblock-clean (add1 level) port 'unquote-splicing))
	 (readquote readblock-clean level port 'unquote)))
    ((#\.)
     (read-char port) (readquote readblock-clean level port dot))
    (else (sugar-read-save port))))

(define (indentationlevel port)
  (let loop ((level 0))
    (if (eq? (peek-char port) #\space)
	(begin
	  (read-char port)
	  (loop (+ level 1)))
	(if (eof-object? (peek-char port))
	    -1
	    level))))

(define (clean line)
  (cond
   ((not (pair? line))
    line)
   ((eq? (car line) 'group)
    (cdr line))
   ((null? (car line))
    (cdr line))
   ((pair? (car line))
    (if (or (equal? (car line) '(quote))
	    (equal? (car line) '(quasiquote))
	    (equal? (car line) '(unquote)))
	(if (and (pair? (cdr line)) (null? (cddr line)))
	    (cons
	     (car (car line))
	     (cdr line))
	    (list
	     (car (car line))
	     (cdr line)))
	(cons
	 (clean (car line))
	 (cdr line))))
   (else line)))

;; Reads all subblocks of a block
(define (readblocks readblock-clean level port)
  (receive (next-level block) (readblock-clean readblock-clean level port)
           (if (eqv? next-level level)
               (receive (next-next-level next-blocks)
                        (readblocks readblock-clean level port)
                        (if (eq? block sugar-stop)
                            (if (pair? next-blocks)
                                (values next-next-level (car next-blocks))
                                (values next-next-level next-blocks))
                            (if (eq? (car next-blocks) sugar-stop)
				(values next-level block)
				(values next-next-level (cons block next-blocks)))))
               (values next-level (list block)))))

;; Read one block of input
(define (readblock readblock-clean level port)
  (let ((char (peek-char port)))
    (cond
     ((eof-object? char)
      (values -1 '()))
     ((eq? char #\return) (read-char port)
      (readblock readblock-clean level port))
     ((or (eqv? char #\newline)
          (eqv? char #\;))
      (if (eqv? (read-char port) #\;) (read-line port)) ; ignore comment
      (let ((next-level (indentationlevel port)))
        (if (> next-level level)
            (readblocks readblock-clean next-level port)
            (values next-level '()))))
     ((or (eqv? char #\space)
	  (eqv? char #\tab))
      (read-char port)
      (readblock readblock-clean level port))
     (else
      (let ((first (readitem readblock-clean level port)))
        (if (and (pair? first) (eq? (car first) dot))
	    (values level (cdr first))
	    (receive (level block) (readblock readblock-clean level port)
		     (cond
		      ((or (eq? first sugar-stop) (eof-object? first))
		       (values level
			       (if (pair? block) (car block) block)))
		      (else (values level
				    (cons first
					  (if (pair? block)
					      (if (eq? (car block) dot)
						  (cadr block)
						  (if (and (null? (cdr block))
							   (pair? (car block))
							   (eq? (caar block) dot))
						      (cadar block)
						      block))
					      block))))))))))))

;; reads a block and handles group, (quote), (unquote) and
;; (quasiquote).

(define (make-readblock-clean keep)
  (lambda (readblock-clean level port)
    (receive (next-level block) (readblock readblock-clean level port)
             (cond
              ((null? block) (values next-level sugar-stop))
              ((and (pair? block) (null? (cdr block)))
               (values next-level (car block)))
              (else (set! keep (cons block keep))
                    (values next-level (clean block)))))))

(define (sugar-read . port)
  (receive (level block)
           (let ((recurse (make-readblock-clean '())))
             (recurse recurse 0 (if (null? port)
                                    (current-input-port)
                                    (car port))))
           (cond
            ((eqv? level -1)
             (if (equal? block '(group)) (eof-object) block))
            ((eq? block sugar-stop) (apply sugar-read port))
            (else block))))

;; Not from srfi 49:

(define (sugar-read-all port)
  (let loop ((expr (sugar-read port)) (result '()))
    (if (eof-object? expr)
        (reverse! result)
        (loop (sugar-read port) (cons expr result)))))
