;;** String Manipulation

;;
;; There are things, which could be better supported by the scheme
;; standard :-(.  I think the Perl or even TCL way of implicit
;; converting everything hides too much of the relevant complexity
;; from the programer.  But Scheme is way too poor the other way
;; around.

;; The string library is still in draft state.  The following will
;; probably not become SRFI.  It's here because the code already uses
;; them.

(define (string-null? str) (equal? str ""))

(define (string-map! proc str . start+end)
  (let ((start (if (pair? start+end) (car start+end) 0))
	(end (if (and (pair? start+end) (pair? (cdr start+end)))
		 (cadr start+end)
		 (sub1 (string-length str)))))
    (do ((i (sub1 (string-length str)) (sub1 i)))
	((< i 0) str)
      (string-set! str i (proc (string-ref str i))))))

(define (string-map proc s . start+end)
  (apply string-map! proc (string-copy s) start+end))

(define (string-downcase! s . start+end)
  (apply string-map! char-downcase s start+end))

(define (string-downcase str . start+end) (apply string-map char-downcase str start+end))

(define (string-upcase! str . start+end) (apply string-map char-upcase str start+end))

(define (string-upcase str . start+end) (apply string-map char-upcase str start+end))

(define (string-replw str width)
  (if (string=? str "")
      ""
      (let ((str-len (string-length str)))
	(let loop ((result "") (size 0))
	  (cond ((= size width) result)
		((> size width) (substring result 0 width))
		(else (loop (string-append result str) (+ size str-len))))))))

(define (string-left s1 width . s2)
  (let ((padding (if (pair? s2) (car s2) " "))
	(str-len (string-length s1)))
    (cond ((> width str-len)
	   (string-append s1 (string-replw padding (- width str-len))))
	  ((< width str-len) (substring s1 0 width))
	  (else s1))))

(define (string-right s1 width . s2)
  (let ((padding (if (pair? s2) (car s2) " "))
	(str-len (string-length s1)))
    (cond ((> width str-len)
	   (string-append (string-replw padding (- width str-len)) s1))
	  ((< width str-len) (substring s1 (- str-len  width) str-len))
	  (else s1))))

(define (has-suffix? suffix str)
  (let ((ql (string-length str)) (sl (string-length suffix)))
    (and (>= ql sl)
	 (let loop ((i (- ql sl)) (j 0))
	   (or (eqv? j sl)
	       (and (eqv? (string-ref str i) (string-ref suffix j))
		    (loop (add1 i) (add1 j))))))))

(define (has-prefix? prefix str)
  (let ((ql (string-length str)) (sl (string-length prefix)))
    (and (>= ql sl)
	 (let loop ((i 0) (j 0))
	   (and (not (eqv? j sl))
		(eqv? (string-ref str i) (string-ref prefix j))
		(loop (add1 i) (add1 j)))))))

;; Some Scheme implementations functions have a limited argument
;; count.  We might need to apply string-append to long lists.

(define make-string/uninit make-string)

(define (apply-string-append lst)
  (define (return-len buffer i str j continue) i)
  (cond
   ((null? lst) "")
   ((null? (cdr lst)) (car lst))
   (else
    (let* ((length (fold (lambda (s i) (fx+ i (string-length s))) 0 lst))
           (result (make-string/uninit length)))
      (let loop ((i 0) (from lst))
        (if (eqv? i length)
            result
            (loop (string-splice! result i (car from) 0 return-len)
                  (cdr from))))))))

;; string-join from draft SRFI 13.
;; slighly rewritten.
;;; (string-join string-list [delimiter grammar]) => string
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Paste strings together using the delimiter string.
;;;
;;; (join-strings '("foo" "bar" "baz") ":") => "foo:bar:baz"
;;;
;;; DELIMITER defaults to a single space " "
;;; GRAMMAR is one of the symbols {prefix, infix, strict-infix, suffix} 
;;; and defaults to 'infix.
;;;
;;; I could rewrite this more efficiently -- precompute the length of the
;;; answer string, then allocate & fill it in iteratively. Using 
;;; STRING-CONCATENATE is less efficient.

(define (srfi:string-join strings . delim+grammar)
  (let ((delim (or (and (pair? delim+grammar) (car delim+grammar)) " "))
        (grammar (or (and  (pair? delim+grammar)
                           (pair? (cdr delim+grammar))
                           (cadr delim+grammar))
                     'infix)))
    (let ((buildit (lambda (lis final)
                     (let recur ((lis lis))
                       (if (pair? lis)
                           (cons delim (cons (car lis) (recur (cdr lis))))
                           final)))))

      (cond ((pair? strings)
             (apply-string-append
              (case grammar

                ((infix strict-infix)
                 (cons (car strings) (buildit (cdr strings) '())))

                ((prefix) (buildit strings '()))

                ((suffix)
                 (cons (car strings) (buildit (cdr strings) (list delim))))

                (else (error "~a:Illegal join grammar ~a"
                             'string-join grammar)))))

             ((not (null? strings))
              (error "~a:STRINGS parameter not list: ~a" string-join strings))

             ;; STRINGS is ()

             ((eq? grammar 'strict-infix)
              (error
               "~a: Empty list cannot be joined with STRICT-INFIX grammar."
               string-join))

             (else "")))))              ; Special-cased for infix grammar.

;; string-index-right s char/char-set/pred [start end] -> integer or #f 
(define (string-index-right s char/char-set/pred . rest)
  (let ((start (if (pair? rest) (car rest) 0))
	(end   (if (and (pair? rest) (pair? (cdr rest)))
		   (cadr rest) (string-length s))))
    (let loop ((i start) (last #f))
      (let ((x (string-index s char/char-set/pred i)))
	(if x (loop (add1 x) (if (fx< x end) x last)) last)))))


(define (string-skip str criteria . maybe-start+end)
  (let ((start (if (pair? maybe-start+end) (car maybe-start+end) 0))
	(end   (if (and (pair? maybe-start+end) (pair? (cdr maybe-start+end)))
		   (cadr maybe-start+end) (string-length str))))
    (cond ((char? criteria)
	   (let lp ((i start))
	     (and (fx< i end)
		  (if (char=? criteria (string-ref str i))
		      (lp (add1 i))
		      i))))
	  ;; ((char-set? criteria)
	  ;;  (let lp ((i start))
	  ;;    (and (fx< i end)
	  ;; 	  (if (char-set-contains? criteria (string-ref str i))
	  ;; 	      (lp (add1 i))
	  ;; 	      i))))
	  ((procedure? criteria)
	   (let lp ((i start))
	     (and (fx< i end)
		  (if (criteria (string-ref str i)) (lp (add1 i))
		      i))))
	  (else (error "string-skip: Second param ~a is neither /*NYI: char-set*/, char, or predicate procedure."
		       criteria)))))

(define (string-skip-right str criteria . maybe-start+end)
  (let ((start (if (pair? maybe-start+end) (car maybe-start+end) 0))
	(end   (if (and (pair? maybe-start+end) (pair? (cdr maybe-start+end)))
		   (cadr maybe-start+end) (string-length str))))
    (cond ((char? criteria)
	   (let lp ((i (sub1 end)))
	     (and (fx>= i 0)
		  (if (char=? criteria (string-ref str i))
		      (lp (sub1 i))
		      i))))
	  ;; ((char-set? criteria)
	  ;;  (let lp ((i (sub1 end)))
	  ;;    (and (fx>= i 0)
	  ;; 	  (if (char-set-contains? criteria (string-ref str i))
	  ;; 	      (lp (sub1 i))
	  ;; 	      i))))
	  ((procedure? criteria)
	   (let lp ((i (sub1 end)))
	     (and (fx>= i 0)
		  (if (criteria (string-ref str i)) (lp (sub1 i))
		      i))))
	  (else (error "string-skip-right: CRITERIA param ~a is neither char-set or char."
		       criteria)))))

;; search the longest prefix in s1 and s2 terminated by sep 
(define (string-prefix-length+ s1 s2 sep)
  (string-index-right s1 sep 0 (string-prefix-length s1 s2)))

