;; (C) 2002, Jörg F. Wittenberger

;; Low level support from rscheme system towards srfi-19.

;;* Locales

;; german and english

(define (locale:english type . args)
  (case type
    ((number-separator) ".")
    ((am) "AM")
    ((pm) "PM")
    ((word) (car args))))

(define *locale:german-table* #f)       ;(make-table string=? string->hash)

(define (add-german-word word-en word-de)
  (hash-table-set! *locale:german-table* word-en word-de))

(define (locale:german type . args)
  (case type
    ((number-separator) ".")
    ((am pm) "")
    ((word) (or (table-lookup *locale:german-table* (car args)) (car args)))))

(define *the-locales*
  `(("de" . ,locale:german)
    ("en" . ,locale:english)))

(define (find-locale locale)
  (let ((result (assoc locale *the-locales*)))
    (or (and result (cdr result)) locale:english)))

;; Calendar locales

(%early-once-only

(set! *locale:german-table* (make-hash-table string=? string->hash))

(define srfi19:locale-abbr-weekday-vector
  (vector "Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat"))

(for-each (lambda (trans) (apply add-german-word trans))
          '(("Sun" "So") ("Mon" "Mo") ("Tue" "Di") ("Wed" "Mi")
            ("Thu" "Do") ("Fri" "Fr") ("Sat" "Sa")))

(define srfi19:locale-long-weekday-vector
  (vector "Sunday" "Monday" "Tuesday" "Wednesday"
          "Thursday" "Friday" "Saturday"))

(for-each (lambda (trans) (apply add-german-word trans))
          '(("Sunday" "Sontag") ("Monday" "Montag")
            ("Tuesday" "Dienstag") ("Wednesday" "Mittwoch")
            ("Thursday" "Donnerstag") ("Friday" "Freitag")
            ("Saturday" "Samstag")))

;; note empty string in 0th place. 
(define srfi19:locale-abbr-month-vector
  (vector
   ""
   "Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))

(for-each (lambda (trans) (apply add-german-word trans))
          '(("May" "Mai") ("Oct" "Okt") ("Dec" "Dez")))

(define srfi19:locale-long-month-vector
  (vector "" "January" "February" "March" "April" "May" "June"
          "July" "August" "September" "October" "November" "December"))

(for-each (lambda (trans) (apply add-german-word trans))
          '(("January" "Januar") ("February" "Februar") ("March" "März")
            ("June" "Juni") ("July" "Juli") ("October" "Oktober")
            ("December" "Dezember")))

(define tm:iso-8601-date-time-format "~Y-~m-~dT~H:~M:~S~z")

(define tm:nano (* 1000 1000 1000))
(define tm:sid  86400)    ; seconds in a day
(define tm:sihd 43200)    ; seconds in a half day
;; RScheme doesn't want to compile that:
;; (define tm:tai-epoch-in-jd 4881175/2) ; julian day number for 'the epoch'
(define tm:tai-epoch-in-jd 2440587.5) ; julian day number for 'the epoch'

) ;; %early-once-only

;; time-error just encapsulates error handling for this file to easy
;; porting effords.

(define (tm:time-error caller type value)
  (if value
      (error "~a TIME-ERROR type ~a: ~a " caller type value)
      (error "~a TIME-ERROR type ~a" caller type)))

(define-macro (display-time-second t p) `(display ,t ,p))

(define (literal-time-second t) (number->string t))

;; A table of leap seconds
;; See ftp://maia.usno.navy.mil/ser7/tai-utc.dat
;; and update as necessary.
;; this procedures reads the file in the abover
;; format and creates the leap second table
;; it also calls the almost standard, but not R5 procedures read-line 
;; & open-input-string
;; ie (set! tm:leap-second-table (tm:read-tai-utc-date "tai-utc.dat"))

(define (tm:read-tai-utc-data filename)
  (define (convert-jd jd)
    (* (- (inexact->exact jd) tm:tai-epoch-in-jd) tm:sid))
  (define (convert-sec sec)
    (inexact->exact sec))
  (let ( (port (open-input-file filename))
	 (table '()) )
    (let loop ((line (read-line port)))
      (if (not (eq? line (eof-object)))
	  (begin
	    (let* ( (data (read (open-input-string (string-append "(" line ")")))) 
		    (year (car data))
		    (jd   (cadddr (cdr data)))
		    (secs (cadddr (cdddr data))) )
	      (if (>= year 1972)
		  (set! table (cons (cons (convert-jd jd) (convert-sec secs)) table)))
	      (loop (read-line port))))))
    table))

;; each entry is ( tai seconds since epoch . # seconds to subtract for utc )
;; note they go higher to lower, and end in 1972.
(%early-once-only

;;(define tm:leap-second-table
;;  '((915148800 . 32)
;;    (867715200 . 31)
;;    (820454400 . 30)
;;    (773020800 . 29)
;;    (741484800 . 28)
;;    (709948800 . 27)
;;    (662688000 . 26)
;;    (631152000 . 25)
;;    (567993600 . 24)
;;    (489024000 . 23)
;;    (425865600 . 22)
;;    (394329600 . 21)
;;    (362793600 . 20)
;;    (315532800 . 19)
;;    (283996800 . 18)
;;    (252460800 . 17)
;;    (220924800 . 16)
;;    (189302400 . 15)
;;    (157766400 . 14)
;;    (126230400 . 13)
;;    (94694400  . 12)
;;    (78796800  . 11)
;;    (63072000  . 10)))

(define tm:leap-second-table '())

(set! tm:leap-second-table
      (map (lambda (p) (cons (+ (* 10000 (car p)) (cadr p)) (caddr p)))
           '((91514 8800  32)
             (86771 5200  31)
             (82045 4400  30)
             (77302 0800  29)
             (74148 4800  28)
             (70994 8800  27)
             (66268 8000  26)
             (63115 2000  25)
             (56799 3600  24)
             (48902 4000  23)
             (42586 5600  22)
             (39432 9600  21)
             (36279 3600  20)
             (31553 2800  19)
             (28399 6800  18)
             (25246 0800  17)
             (22092 4800  16)
             (18930 2400  15)
             (15776 6400  14)
             (12623 0400  13)
             (9469  4400  12)
             (7879  6800  11)
             (6307  2000  10))))

) ;; %early-once-only

(define (read-leap-second-table filename)
  (set! tm:leap-second-table (tm:read-tai-utc-data filename))
  (values))


(define (tm:leap-second-delta utc-seconds)
  (letrec ( (lsd (lambda (table)
		   (cond ((>= utc-seconds (caar table))
			  (cdar table))
			 (else (lsd (cdr table)))))) )
    (if (< utc-seconds  (* (- 1972 1970) 365 tm:sid)) 0
	(lsd  tm:leap-second-table))))

;;* the TIME structure; creates the accessors, too.

;; (define-struct time (type second nanosecond))

(define-class <srfi:time> (<object>)
  second nanosecond)

(define-class <time:utc> (<srfi:time>))
(define-class <time:tai> (<srfi:time>))
(define-class <time:monotonic> (<srfi:time>))
(define-class <time:duration> (<srfi:time>))

(define (make-time type ns sec)
  (make (case type
          ((time-utc) <time:utc>)
          ((time-utc) <time:tai>)
          ((time-utc) <time:monotonic>)
          ((time-duration) <time:duration>)
	  ((time-monotonic) <time:monotonic>))
    second: (inexact->exact sec)
    nanosecond: (inexact->exact ns)))

(define time-second second)
(define time-nanosecond nanosecond)

(define-method time-type ((self <srfi:time>))
  (cond
   ((instance? self <time:utc>) 'time-utc)
   ((instance? self <time:tai>) 'time-tai)
   ((instance? self <time:monotonic>) 'time-monotonic)
   ((instance? self <time:duration>) 'time-duration)
   (else #f)))

(define (srfi19:time? obj) (instance? obj <srfi:time>))

(define (copy-time time) (clone time))

;;* date

(define-class <srfi:date> (<object>)
  nanosecond second minute hour day month year zone-offset)

(define (make-date nanosecond second minute hour day month year zone-offset)
  (make <srfi:date>
     nanosecond: (inexact->exact nanosecond) second: (inexact->exact second)
     minute: (inexact->exact minute) hour: (inexact->exact hour)
     day: (inexact->exact day) month: (inexact->exact month)
     year: (inexact->exact year) zone-offset: (inexact->exact zone-offset)))

(define (srfi19:date? obj) (instance? obj <srfi:date>))

(define date-zone-offset zone-offset)
(define date-year year)
(define date-month month)
(define date-day day)
(define date-hour hour)
(define date-minute minute)
(define date-second second)
(define date-nanosecond nanosecond)

(define set-date-nanosecond! set-nanosecond!)
(define set-date-second! set-second!)

(define set-date-hour! set-hour!)
(define set-date-zone-offset! set-zone-offset!)
(define set-date-day! set-day!)
(define set-date-month! set-month!)
(define set-date-minute! set-minute!)
(define set-date-year! set-year!)

;;* time

(define (tm:get-time-of-day)
  (let* ((v0 (time->epoch-seconds (time)))
	 (v1 (inexact->exact (floor v0))))
    (values v1 (inexact->exact (* (- v0 v1) tm:nano)))))
