;; Copyright (C) 2002 Jörg F. Wittenberger

;; Straight from the SRFI-9 document.

; Definition of DEFINE-RECORD-TYPE

(define-syntax define-record-fields
  (syntax-form
   ((field-tag accessor . more) . more-fields)
   ((define-record-field field-tag accessor . more)
    . (define-record-fields . more-fields)))
  (syntax-form () '()))

(define-syntax define-record-initializer
  (syntax-form (x)
    (string->symbol (string-append (symbol->string x) ":"))))

(define-syntax define-record-initializers
  (syntax-form () '())
  (syntax-form (tag . more)
               ((define-record-initializer tag) tag
                . (define-record-initializers . more))))

(define-syntax define-record-type
  (syntax-form
   (type
    (constructor constructor-tag . more-c-tags)
    predicate
    (field-tag accessor . more) . more-fields)
   (begin
     (define-class type (<object>)
       (define-record-field field-tag accessor . more)
       . (define-record-fields . more-fields))
     (define (constructor . constructor-tags)
         (make type
           . (define-record-initializers . constructor-tags)))
     (define (predicate obj)
       (instance? obj type)))))

; An auxilliary macro for define field accessors and modifiers.
; This is needed only because modifiers are optional.

(define-syntax define-record-field
  (syntax-form (field-tag accessor)
    (field-tag getter: accessor))
  (syntax-form (field-tag accessor modifier)
    (field-tag getter: accessor setter: modifier)))
