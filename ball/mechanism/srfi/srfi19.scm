;; (C) 2002,2005 Jörg F. Wittenberger

;; Heavily based on the original SRFI-19 document.  The original
;; document can't be used as it is.  Several bugs.

;; -- Bug fixes.
;;
;; MAKE-TIME had parameters seconds and nanoseconds reversed; change all
;;           references in file to match.  Will F: 2002-10-15
;;
;; DATE-YEAR-DAY returned the wrong day; tm:year-day fixed to do the right
;;               thing. Will F: 2002-10-15
;;               It also called an undefined error procedure.

(define (tm:current-time-utc)
  (receive (seconds ns) (tm:get-time-of-day)
	   (make-time 'time-utc ns seconds)))

(define (tm:current-time-tai)
  (receive (seconds ns) (tm:get-time-of-day)
	   (make-time 'time-tai
                      ns
                      (+ seconds (tm:leap-second-delta seconds)))))

;; -- we define it to be the same as TAI.
;;    A different implemation of current-time-montonic
;;    will require rewriting all of the time-monotonic converters,
;;    of course.

(define (tm:current-time-monotonic)
  (receive (seconds ns) (tm:get-time-of-day)
	   (make-time 'time-monotonic
                      ns
                      (+ seconds (tm:leap-second-delta seconds)))))

(define (tm:current-time-thread)
  (tm:time-error 'current-time 'unsupported-clock-type 'time-thread))

(define (tm:current-time-process)
  (tm:time-error 'current-time 'unsupported-clock-type 'time-process))

(define (tm:current-time-gc)
  (tm:time-error 'current-time 'unsupported-clock-type 'time-gc))

(define (srfi19:current-time . clock-type)
  (let ( (clock-type (if (pair? clock-type) (car clock-type) 'time-utc)) )
    (cond
     ((eq? clock-type 'time-tai) (tm:current-time-tai))
     ((eq? clock-type 'time-utc) (tm:current-time-utc))
     ((eq? clock-type 'time-monotonic) (tm:current-time-monotonic))
     ((eq? clock-type 'time-thread) (tm:current-time-thread))
     ((eq? clock-type 'time-process) (tm:current-time-process))
     ((eq? clock-type 'time-gc) (tm:current-time-gc))
     (else (tm:time-error 'current-time 'invalid-clock-type clock-type)))))

;; time resolution

;; -- Time Resolution
;; This is the resolution of the clock in nanoseconds.
;; This will be implementation specific.

(define (time-resolution . clock-type)
  (let ((clock-type (if (pair? clock-type) (car clock-type) 'time-utc)))
    (cond
     ((eq? clock-type 'time-tai) 10000)
     ((eq? clock-type 'time-utc) 10000)
     ((eq? clock-type 'time-monotonic) 10000)
     ((eq? clock-type 'time-thread) 10000)
     ((eq? clock-type 'time-gc) 10000)
     (else (tm:time-error 'time-resolution 'invalid-clock-type clock-type)))))

;;* time comparison

(define (tm:time-compare-check time1 time2 caller)
  (cond
   ((not (srfi19:time? time1))
    (tm:time-error caller 'not-a-srfi-time-type time1))
   ((not (srfi19:time? time2))
    (tm:time-error caller 'not-a-srfi-time-type time2))
   ((not (eq? (time-type time1) (time-type time2)))
    (tm:time-error caller 'incompatible-time-types #f))
   (else #t)))

(define (tm:time-compare time1 time2 proc caller)
  (tm:time-compare-check time1 time2 caller)
  (or (proc (time-second time1) (time-second time2))
      (and (eqv? (time-second time1) (time-second time2))
           (proc (time-nanosecond time1) (time-nanosecond time2)))))

(define (tm:time-compare= time1 time2 > >= caller)
  (tm:time-compare-check time1 time2 caller)
  (or (> (time-second time1) (time-second time2))
      (and (eqv? (time-second time1) (time-second time2))
           (>= (time-nanosecond time1) (time-nanosecond time2)))))

(define (srfi19:time=? time1 time2)
  (tm:time-compare-check time1 time2 'time=?)
  (and (eqv? (time-second time1) (time-second time2))
       (eqv? (time-nanosecond time1) (time-nanosecond time2))))

(define (srfi19:time>? time1 time2)
  (tm:time-compare time1 time2 > 'time>?))

(define (srfi19:time<? time1 time2)
  (tm:time-compare time1 time2 < 'time<?))

(define (srfi19:time>=? time1 time2)
  (tm:time-compare= time1 time2 > >= 'time>=?))

(define (srfi19:time<=? time1 time2)
  (tm:time-compare= time1 time2 < <= 'time<=?))

;;* time arithmetic

(define (time-difference time1 time2)
  (if (or (not (and (srfi19:time? time1) (srfi19:time? time2)))
	  (not (eq? (time-type time1) (time-type time2))))
      (tm:time-error 'time-difference 'incompatible-time-types #f)
      (let ( (sec-diff (- (time-second time1) (time-second time2)))
	     (nsec-diff (- (time-nanosecond time1) (time-nanosecond time2))) )
	(if (negative? nsec-diff)
	    (make-time 'time-duration (+ tm:nano nsec-diff) (- sec-diff 1))
	    (make-time 'time-duration nsec-diff sec-diff)))))

(define (add-duration time1 duration)
  (if (not (and (srfi19:time? time1) (srfi19:time? duration)))
      (tm:time-error 'add-duration 'incompatible-time-types #f))
  (if (not (eq? (time-type duration) 'time-duration))
      (tm:time-error 'add-duration 'not-duration duration)
      (let ( (sec-plus (+ (time-second time1) (time-second duration)))
	     (nsec-plus (+ (time-nanosecond time1) (time-nanosecond duration))) )
	(let ((r (remainder nsec-plus tm:nano))
	      (q (quotient nsec-plus tm:nano)))
	  (if (negative? r)
	      (make-time (time-type time1) (+ tm:nano r) (+ sec-plus q -1))
	      (make-time (time-type time1) r (+ sec-plus q)))))))

(define (subtract-duration time1 duration)
  (if (not (and (srfi19:time? time1) (srfi19:time? duration)))
      (tm:time-error 'add-duration 'incompatible-time-types #f))
  (if (not (eq? (time-type duration) 'time-duration))
      (tm:time-error 'add-duration 'not-duration duration)
      (let ( (sec-minus  (- (time-second time1) (time-second duration)))
	     (nsec-minus (- (time-nanosecond time1) (time-nanosecond duration))) )
	(let ((r (remainder nsec-minus tm:nano))
	      (q (quotient nsec-minus tm:nano)))
	  (if (negative? r)
	      (make-time (time-type time1) (+ tm:nano r) (- sec-minus q 1))
	      (make-time (time-type time1) r (- sec-minus q)))))))

;;* time converters

(define (time-tai->time-utc time-in)
  (if (not (or (eq? (time-type time-in) 'time-tai)
               ;; BEWARE the second check depends on time-monotonic
               ;; haveing the same definition as time-tai !!
               (eq? (time-type time-in) 'time-monotonic)))
      (tm:time-error 'time-tai->time-utc 'incompatible-time-types time-in))
  (make-time 'time-utc
             (time-nanosecond time-in)
             (- (time-second time-in)
                (tm:leap-second-delta 
                 (time-second time-in)))))

(define (time-utc->time-tai time-in)
  (if (not (eq? (time-type time-in) 'time-utc))
      (tm:time-error 'time-utc->time-tai 'incompatible-time-types time-in))
  (make-time 'time-tai
             (time-nanosecond time-in)
             (+ (time-second time-in)
                (tm:leap-second-delta 
                 (time-second time-in)))))

;; BEWARE these depend on time-monotonic having the same definition as
;; time-tai!
(define (time-monotonic->time-utc time-in)
  (if (not (eq? (time-type time-in) 'time-monotonic))
      (tm:time-error 'time-monotonic->time-utc 'incompatible-time-types time-in))
  (time-tai->time-utc time-in))

(define (time-monotonic->time-tai time-in)
  (if (not (eq? (time-type time-in) 'time-monotonic))
      (tm:time-error 'time-monotonic->time-tai 'incompatible-time-types time-in))
  (make-time 'time-tai (time-nanosecond time-in) (time-second time-in)))

(define (time-utc->time-monotonic time-in)
  (if (not (eq? (time-type time-in) 'time-utc))
      (tm:time-error 'time-utc->time-monotonic 'incompatible-time-types time-in))
  ;; KLUDGE this pure temporary object is really not needed.
  ;; we could/should use the side effects from the srfi ref impl. here.
  (let ((ntime (time-utc->time-tai time-in)))
    (make-time 'time-monotonic (time-nanosecond ntime) (time-second ntime))))

(define (time-tai->time-monotonic time-in)
  (if (not (eq? (time-type time-in) 'time-utc))
      (tm:time-error 'time-tai->time-monotonic 'incompatible-time-types time-in))
  (make-time 'time-monotonic (time-nanosecond time-in) (time-second time-in)))

;;* date

;; gives the julian day which starts at noon.
(define (tm:encode-julian-day-number day month year)
  (let* ((a (quotient (- 14 month) 12))
	 (y (- (+ year 4800) a (if (negative? year) -1  0)))
	 (m (- (+ month (* 12 a)) 3)))
    (+ day
       (quotient (+ (* 153 m) 2) 5)
       (* 365 y)
       (quotient y 4)
       (- (quotient y 100))
       (quotient y 400)
       -32045)))

(define (tm:split-real r)
  (if (integer? r) (values r 0)
      (let ((l (truncate r)))
	(values l (- r l)))))

;; gives the seconds/date/month/year 
(define (tm:decode-julian-day-number jdn)
  (let* ((days (inexact->exact (truncate jdn)))
	 (a (+ days 32044))
	 (b (quotient (+ (* 4 a) 3) 146097))
	 (c (- a (quotient (* 146097 b) 4)))
	 (d (quotient (+ (* 4 c) 3) 1461))
	 (e (- c (quotient (* 1461 d) 4)))
	 (m (quotient (+ (* 5 e) 2) 153))
	 (y (+ (* 100 b) d -4800 (quotient m 10))))
    (values ; seconds date month year
     (inexact->exact (truncate (* (- jdn days) tm:sid)))
     (+ e (- (quotient (+ (* 153 m) 2) 5)) 1)
     (+ m 3 (* -12 (quotient m 10)))
     (if (>= 0 y) (- y 1) y))
    ))

;;* time->date

(define (tm:leap-second? second)
  (and (assoc second tm:leap-second-table) #t))

;; special thing -- ignores nanos
(define (tm:time->julian-day-number seconds tz-offset)
  (+ (/ (+ seconds
	   tz-offset
	   tm:sihd)
	tm:sid)
     tm:tai-epoch-in-jd))

(define (time-utc->date time offset)
  (if (not (eq? (time-type time) 'time-utc))
      (tm:time-error 'time->date 'incompatible-time-types  time))
  (let* ( (is-leap-second (tm:leap-second? (+ offset (time-second time)))) )
    (call-with-values
	(lambda ()
	  (if is-leap-second
	      (tm:decode-julian-day-number (tm:time->julian-day-number (- (time-second time) 1) offset))
	      (tm:decode-julian-day-number (tm:time->julian-day-number (time-second time) offset))))
      (lambda (secs date month year)
	(let* ( (hours    (quotient secs (* 3600)))
		(rem      (remainder secs (* 3600)))
		(minutes  (quotient rem 60))
		(seconds  (remainder rem 60)) )
	  (make-date (time-nanosecond time)
		     (if is-leap-second (+ seconds 1) seconds)
		     minutes
		     hours
		     date
		     month
		     year
		     offset))))))


(define (time-tai->date time offset)
  (if (not (eq? (time-type time) 'time-tai))
      (tm:time-error 'time->date 'incompatible-time-types  time))
  (let* ( (seconds (- (time-second time) (tm:leap-second-delta (time-second time))))
	  (is-leap-second (tm:leap-second? (+ offset seconds))) )
    (call-with-values
	(lambda ()
	  (if is-leap-second
	      (tm:decode-julian-day-number (tm:time->julian-day-number (- seconds 1) offset))
	      (tm:decode-julian-day-number (tm:time->julian-day-number seconds offset))) )
      (lambda (secs date month year)
	;; adjust for leap seconds if necessary ...
	(let* ( (hours    (quotient secs (* 60 60)))
		(rem      (remainder secs (* 60 60)))
		(minutes  (quotient rem 60))
		(seconds  (remainder rem 60)) )
	  (make-date (time-nanosecond time)
		     (if is-leap-second (+ seconds 1) seconds)
		     minutes
		     hours
		     date
		     month
		     year
		     offset))))))

(define time-monotonic->date time-tai->date)

(define (date->time-utc date)
  (let ( (nanosecond (date-nanosecond date))
	 (second (date-second date))
	 (minute (date-minute date))
	 (hour (date-hour date))
	 (day (date-day date))
	 (month (date-month date))
	 (year (date-year date))
	 (offset (date-zone-offset date)))
    (let ( (jdays (- (tm:encode-julian-day-number day month year)
		     tm:tai-epoch-in-jd)) )
      (make-time 
       'time-utc
       nanosecond
       ;; BEWARE again RScheme has a problem with 1/2
       (+ (* (- jdays 0.5) 24 60 60) ;; (* (- jdays 1/2) 24 60 60)
	  (* hour 60 60)
	  (* minute 60)
	  second
	  (- 0 offset))))))

(define (date->time-tai date)
  ;; KLUDGE should be "time-utc->time-tai!"
  (time-utc->time-tai (date->time-utc date)))

(define (date->time-monotonic date)
  ;; KLUDGE should be "time-utc->time-monotonic!"
  (time-utc->time-monotonic (date->time-utc date)))

(define (date-year-day date)
  (tm:year-day (date-day date)
               (date-month date)
               (date-year date)))

;;* Date accessors

(define (tm:leap-year? year)
  (or (= (modulo year 400) 0)
      (and (= (modulo year 4) 0) (not (= (modulo year 100) 0)))))

(define (leap-year? date)
  (tm:leap-year? (date-year date)))

(define  tm:month-assoc '((1 . 0)  (2 . 31)   (3 . 59)   (4 . 90) 
			  (5 . 120) (6 . 151)  (7 . 181)  (8 . 212)
			  (9 . 243) (10 . 273) (11 . 304) (12 . 334)))

(define (tm:year-day day month year)
  (let ((days-pr (assoc month tm:month-assoc)))
    (if (not days-pr)
	(tm:time-error 'date-year-day 'invalid-month-specification month))
    (if (and (tm:leap-year? year) (> month 2))
	(+ day (cdr days-pr) 1)
	(+ day (cdr days-pr)))))

;; from calendar faq 
(define (tm:week-day day month year)
  (let* ((a (quotient (- 14 month) 12))
	 (y (- year a))
	 (m (+ month (* 12 a) -2)))
    (modulo (+ day y (quotient y 4) (- (quotient y 100))
	       (quotient y 400) (quotient (* 31 m) 12))
	    7)))

(define (date-week-day date)
  (tm:week-day (date-day date) (date-month date) (date-year date)))

(define (tm:days-before-first-week date day-of-week-starting-week)
    (let* ( (first-day (make-date 0 0 0 0
				  1
				  1
				  (date-year date)
				  (date-zone-offset date)))
	    (fdweek-day (date-week-day first-day))  )
      (modulo (- day-of-week-starting-week fdweek-day)
              7)))

(define (date-week-number date day-of-week-starting-week)
  (quotient (- (+ 6 (date-year-day date))
                (tm:days-before-first-week  date day-of-week-starting-week))
            7))

;;* current date

(define (current-date . tz-offset)
  (time-utc->date (srfi19:current-time 'time-utc)
                  (or (and (pair? tz-offset) (car tz-offset))
                      0)))

;; given a 'two digit' number, find the year within 50 years +/-
(define (tm:natural-year n)
  (let* ( (current-year (date-year (current-date)))
	  (current-century (* (quotient current-year 100) 100)) )
    (cond
     ((>= n 100) n)
     ((<  n 0) n)
     ((<=  (- (+ current-century n) current-year) 50)
      (+ current-century n))
     (else
      (+ (- current-century 100) n)))))

;;* Julian Day

(define julian-day-half 0.49991)

(define (date->julian-day date)
  (let ( (nanosecond (date-nanosecond date))
	 (second (date-second date))
	 (minute (date-minute date))
	 (hour (date-hour date))
	 (day (date-day date))
	 (month (date-month date))
	 (year (date-year date))
	 (tzo (date-zone-offset date)))
    (cond-expand
     (chicken
      (fp+ (fp- (exact->inexact (tm:encode-julian-day-number day month  
							     year))
		julian-day-half)
	   (fp/ (fp+ (exact->inexact
		      (fx+ (fx+ (fx* hour 3600)
				(fx+ (fx* minute 60) second))
			   (fxneg tzo)))
		     (fp/ (exact->inexact nanosecond) 1000000000.0))
		86400.0)))
     (else (+ (tm:encode-julian-day-number day month year)
	      (- julian-day-half) ;; (- 1/2)
	      (+ (/ (+ (* hour 3600)
		       (* minute 60)
		       second
		       (- tzo)
		       (/ nanosecond tm:nano))
		    tm:sid)))))))

(define tm:julian-delta
  ;; 4800001/2
  2400000.5)

(define (date->modified-julian-day date)
  (- (date->julian-day date) tm:julian-delta))

(define (time-utc->julian-day time)
  (if (not (eq? (time-type time) 'time-utc))
      (tm:time-error 'time->date 'incompatible-time-types  time))
  (+ (/ (+ (time-second time) (/ (time-nanosecond time) tm:nano))
	tm:sid)
     tm:tai-epoch-in-jd))

(define (time-utc->modified-julian-day time)
  (- (time-utc->julian-day time) tm:julian-delta))

(define (time-tai->julian-day time)
  (if (not (eq? (time-type time) 'time-tai))
      (tm:time-error 'time->date 'incompatible-time-types  time))
  (+ (/ (+ (- (time-second time) 
	      (tm:leap-second-delta (time-second time)))
	   (/ (time-nanosecond time) tm:nano))
	tm:sid)
     tm:tai-epoch-in-jd))

(define (time-tai->modified-julian-day time)
  (- (time-tai->julian-day time) tm:julian-delta))

;; this is the same as time-tai->julian-day
(define (time-monotonic->julian-day time)
  (if (not (eq? (time-type time) 'time-monotonic))
      (tm:time-error 'time->date 'incompatible-time-types  time))
  (+ (/ (+ (- (time-second time) 
	      (tm:leap-second-delta (time-second time)))
	   (/ (time-nanosecond time) tm:nano))
	tm:sid)
     tm:tai-epoch-in-jd))

(define (time-monotonic->modified-julian-day time)
  (- (time-monotonic->julian-day time) tm:julian-delta))

;; from julian day

(define (julian-day->time-utc jdn)
 (let ( (secs (* tm:sid (- jdn tm:tai-epoch-in-jd))) )
    (receive (seconds parts)
	     (tm:split-real secs)
	     (make-time 'time-utc (truncate (* parts tm:nano)) seconds))))

(define (julian-day->time-tai jdn)
  (time-utc->time-tai (julian-day->time-utc jdn)))
			 
(define (julian-day->time-monotonic jdn)
  (time-utc->time-monotonic (julian-day->time-utc jdn)))

(define (julian-day->date jdn tz-offset)
  (time-utc->date (julian-day->time-utc jdn) tz-offset))

(define (modified-julian-day->date jdn tz-offset)
  (julian-day->date (+ jdn tm:julian-delta) tz-offset))

(define (modified-julian-day->time-utc jdn)
  (julian-day->time-utc (+ jdn tm:julian-delta)))

(define (modified-julian-day->time-tai jdn)
  (julian-day->time-tai (+ jdn tm:julian-delta)))

(define (modified-julian-day->time-monotonic jdn)
  (julian-day->time-monotonic (+ jdn tm:julian-delta)))

(define (current-julian-day)
  (time-utc->julian-day (srfi19:current-time 'time-utc)))

(define (current-modified-julian-day)
  (time-utc->modified-julian-day (srfi19:current-time 'time-utc)))

;;* Date formatting procedures

;; returns a string rep. of number N, of minimum LENGTH,
;; padded with character PAD-WITH. If PAD-WITH if #f, 
;; no padding is done, and it's as if number->string was used.
;; if string is longer than LENGTH, it's as if number->string was used.

(define (tm:padding n pad-with length)
  (let* ( (str (literal-time-second n))
	  (str-len (string-length str)) )
    (if (or (>= str-len length)
            (not pad-with))
	str
	(let* ( (new-str (make-string length pad-with))
		(new-str-offset (- (string-length new-str)
				   str-len)) )
	  (do ((i 0 (+ i 1)))
	      ((>= i (string-length str)))
		(string-set! new-str (+ new-str-offset i) 
			     (string-ref str i)))
	  new-str))))

(define (tm:last-n-digits i n)
  (abs (remainder i (expt 10 n))))

(define (tm:locale-abbr-weekday locale n) 
  (locale 'word (vector-ref srfi19:locale-abbr-weekday-vector n)))

(define (tm:locale-long-weekday locale n)
  (locale 'word (vector-ref srfi19:locale-long-weekday-vector n)))

(define (tm:locale-abbr-month locale n)
  (locale 'word (vector-ref srfi19:locale-abbr-month-vector n)))


(define (tm:locale-long-month locale n)
  (locale 'word (vector-ref srfi19:locale-long-month-vector n)))

; (define (tm:vector-find needle haystack comparator)
;   (let ((len (vector-length haystack)))
;     (define (tm:vector-find-int index)
;       (cond
;        ((>= index len) #f)
;        ((comparator needle (vector-ref haystack index)) index)
;        (else (tm:vector-find-int (+ index 1)))))
;     (tm:vector-find-int 0)))

;(define (tm:locale-abbr-weekday->index string)
;  (tm:vector-find string srfi19:locale-abbr-weekday-vector string=?))

(define (tm:locale-abbr-weekday->index string)
  (hash-table-ref/default tm:locale-abbr-weekday-table string #f))

(%early-once-only
 (define tm:locale-abbr-weekday-table (make-string-table))
 (do ((i 0 (add1 i)))
     ((eqv? i (vector-length srfi19:locale-abbr-weekday-vector)))
  (hash-table-set! tm:locale-abbr-weekday-table
		   (vector-ref srfi19:locale-abbr-weekday-vector i) i)))

;(define (tm:locale-long-weekday->index string)
;  (tm:vector-find string srfi19:locale-abbr-weekday-vector string=?))

(define (tm:locale-long-weekday->index string)
  (hash-table-ref/default tm:locale-long-weekday-table string #f))

(%early-once-only
 (define tm:locale-long-weekday-table (make-string-table))

 (do ((i 0 (add1 i)))
     ((eqv? i (vector-length srfi19:locale-long-weekday-vector)))
   (hash-table-set! tm:locale-long-weekday-table
		    (vector-ref srfi19:locale-long-weekday-vector i) i)))

;(define (tm:locale-abbr-month->index string)
;  (tm:vector-find string srfi19:locale-abbr-month-vector string=?))

(define (tm:locale-abbr-month->index string)
  (hash-table-ref/default tm:locale-abbr-month-table string #f))

(%early-once-only
 (define tm:locale-abbr-month-table (make-string-table))
 (do ((i 0 (add1 i)))
     ((eqv? i (vector-length srfi19:locale-abbr-month-vector)))
  (hash-table-set! tm:locale-abbr-month-table
		   (vector-ref srfi19:locale-abbr-month-vector i) i)))

;(define (tm:locale-long-month->index string)
;  (tm:vector-find string srfi19:locale-long-month-vector string=?))

(define (tm:locale-long-month->index string)
  (hash-table-ref/default tm:locale-abbr-month-table string #f))

(%early-once-only
 (define tm:locale-long-month-table (make-string-table))
 (do ((i 0 (add1 i)))
     ((eqv? i (vector-length srfi19:locale-long-month-vector)))
  (hash-table-set! tm:locale-long-month-table
		   (vector-ref srfi19:locale-long-month-vector i) i)))

;; do nothing. 
;; Your implementation might want to do something...
;; 
(define (tm:locale-print-time-zone date port)
  (begin))

;; Again, locale specific.
(define (tm:locale-am/pm locale hr)
  (if (> hr 11) (locale 'pm) (locale 'am)))

(define (tm:tz-printer locale offset port)
  (cond
   ((= offset 0) (display "Z" port))
   ((negative? offset) (display "-" port))
   (else (display "+" port)))
  (if (not (= offset 0))
      (let ( (hours   (abs (quotient offset (* 60 60))))
	     (minutes (abs (quotient (remainder offset (* 60 60)) 60))) )
	(display (tm:padding hours #\0 2) port)
	(display (tm:padding minutes #\0 2) port))))

;; A table of output formatting directives.
;; the first time is the format char.
;; the second is a procedure that takes the date, a padding character
;; (which might be #f), and the output port.
;;
(%early-once-only

(define tm:directives 
  (list
   (cons #\~ (lambda (locale date pad-with port) (display #\~ port)))

   (cons #\a (lambda (locale date pad-with port)
	       (display (tm:locale-abbr-weekday locale (date-week-day date))
			port)))
   (cons #\A (lambda (locale date pad-with port)
	       (display (tm:locale-long-weekday locale (date-week-day date))
			port)))
   (cons #\b (lambda (locale date pad-with port)
	       (display (tm:locale-abbr-month locale (date-month date))
			port)))
   (cons #\B (lambda (locale date pad-with port)
	       (display (tm:locale-long-month locale (date-month date))
			port)))
   (cons #\c (lambda (locale date pad-with port)
	       (tm:time-error 'date->string 'unsupported-format #\c)))
;   (cons #\c (lambda (locale date pad-with port)
;              (tm:print-date date tm:locale-date-time-format locale port)))
   (cons #\d (lambda (locale date pad-with port)
	       (display (tm:padding (date-day date)
				    #\0 2)
			    port)))
   (cons #\D (lambda (locale date pad-with port)
	       (tm:print-date date "~m/~d/~y" locale port)))
   (cons #\e (lambda (locale date pad-with port)
	       (display (tm:padding (date-day date)
				    #\space 2)
			port)))
   (cons #\f (lambda (locale date pad-with port)
	       (if (> (date-nanosecond date)
		      tm:nano)
		   (display (tm:padding (+ (date-second date) 1)
					pad-with 2)
			    port)
		   (display (tm:padding (date-second date)
					pad-with 2)
			    port))
	       (receive (i f) 
			(tm:split-real (/ 
					(date-nanosecond date)
					tm:nano 1.0))
			(let* ((ns (number->string f))
			       (le (string-length ns)))
			  (if (> le 2)
			      (begin
				(display (locale 'number-separator) port)
				(display (substring ns 2 le) port)))))))
   (cons #\h (lambda (locale date pad-with port)
	       (tm:print-date date "~b" locale port)))
   (cons #\H (lambda (locale date pad-with port)
	       (display (tm:padding (date-hour date)
				    pad-with 2)
			port)))
   (cons #\I (lambda (locale date pad-with port)
	       (let ((hr (date-hour date)))
		 (if (> hr 12)
		     (display (tm:padding (- hr 12)
					  pad-with 2)
			      port)
		     (display (tm:padding hr
					  pad-with 2)
			      port)))))
   (cons #\j (lambda (locale date pad-with port)
	       (display (tm:padding (date-year-day date)
				    pad-with 3)
			port)))
   (cons #\k (lambda (locale date pad-with port)
	       (display (tm:padding (date-hour date)
				    #\space 2)
			    port)))
   (cons #\l (lambda (locale date pad-with port)
	       (let ((hr (if (> (date-hour date) 12)
			     (- (date-hour date) 12) (date-hour date))))
		 (display (tm:padding hr  #\space 2)
			  port))))
   (cons #\m (lambda (locale date pad-with port)
	       (display (tm:padding (date-month date)
				    pad-with 2)
			port)))
   (cons #\M (lambda (locale date pad-with port)
	       (display (tm:padding (date-minute date)
				    pad-with 2)
			port)))
   (cons #\n (lambda (locale date pad-with port)
	       (newline port)))
   (cons #\N (lambda (locale date pad-with port)
	       (display (tm:padding (date-nanosecond date)
				    pad-with 7)
			port)))
   (cons #\p (lambda (locale date pad-with port)
	       (display (tm:locale-am/pm locale (date-hour date)) port)))
   (cons #\r (lambda (locale date pad-with port)
	       (tm:print-date date "~I:~M:~S ~p" locale port)))
   (cons #\s (lambda (locale date pad-with port)
	       (display-time-second (time-second (date->time-utc date)) port)))
   (cons #\S (lambda (locale date pad-with port)
	       (if (> (date-nanosecond date)
		      tm:nano)
	       (display (tm:padding (+ (date-second date) 1)
				    pad-with 2)
			port)
	       (display (tm:padding (date-second date)
				    pad-with 2)
			port))))
   (cons #\t (lambda (locale date pad-with port)
	       (display #\tab port)))
   (cons #\T (lambda (locale date pad-with port)
	       (tm:print-date date "~H:~M:~S" locale port)))
   (cons #\U (lambda (locale date pad-with port)
	       (if (> (tm:days-before-first-week date 0) 0)
		   (display (tm:padding (+ (date-week-number date 0) 1)
					#\0 2) port)
		   (display (tm:padding (date-week-number date 0)
					#\0 2) port))))
   (cons #\V (lambda (locale date pad-with port)
	       (display (tm:padding (date-week-number date 1)
				    #\0 2) port)))
   (cons #\w (lambda (locale date pad-with port)
	       (display (date-week-day date) port)))
   (cons #\x (lambda (locale date pad-with port)
	       (tm:time-error 'date->string 'unsupported-format #\x)))
;   (cons #\x (lambda (locale date pad-with port)
;	       (display (srfi19:date->string date tm:locale-short-date-format locale) port)))
   (cons #\X (lambda (locale date pad-with port)
	       (tm:time-error 'date->string 'unsupported-format #\X)))
;   (cons #\X (lambda (locale date pad-with port)
;	       (display (date->string date tm:locale-time-format) port)))
   (cons #\W (lambda (locale date pad-with port)
	       (if (> (tm:days-before-first-week date 1) 0)
		   (display (tm:padding (+ (date-week-number date 1) 1)
					#\0 2) port)
		   (display (tm:padding (date-week-number date 1)
					#\0 2) port))))
   (cons #\y (lambda (locale date pad-with port)
	       (display (tm:padding (tm:last-n-digits 
				     (date-year date) 2)
				    pad-with
				    2)
			port)))
   (cons #\Y (lambda (locale date pad-with port)
	       (display (date-year date) port)))
   (cons #\z (lambda (locale date pad-with port)
	       (tm:tz-printer locale (date-zone-offset date) port)))
   (cons #\Z (lambda (locale date pad-with port)
	       (tm:locale-print-time-zone date port)))
   (cons #\1 (lambda (locale date pad-with port)
	       (tm:print-date date "~Y-~m-~d" locale port)))
   (cons #\2 (lambda (locale date pad-with port)
	       (tm:print-date date "~H:~M:~S~z" locale port)))
   (cons #\3 (lambda (locale date pad-with port)
	       (tm:print-date date "~H:~M:~S" locale port)))
   (cons #\4 (lambda (locale date pad-with port)
	       (tm:print-date date "~Y-~m-~dT~H:~M:~S~z" locale port)))
   (cons #\5 (lambda (locale date pad-with port)
	       (tm:print-date date "~Y-~m-~dT~H:~M:~S" locale port)))
   ))

(define tm:directives-table (make-character-table))

(do ((l tm:directives (cdr l)))
     ((null? l))
   (hash-table-set! tm:directives-table (caar l) (cdar l)))

) ;; %early-once-only

(define (tm:get-formatter char)
;  (let ( (associated (assoc char tm:directives)) )
;    (if associated (cdr associated) #f))
  (hash-table-ref/default tm:directives-table char #f))

(define (tm:date-printer locale date index format-string str-len port)
  (let recurse
      ((locale locale) (date date) (index index)
       (format-string format-string) (str-len str-len) (port port))
    (if (>= index str-len)
	(begin)
	(let ( (current-char (string-ref format-string index)) )
	  (if (not (char=? current-char #\~))
	      (begin
		(display current-char port)
		(recurse locale date (fx+ index 1) format-string str-len port))
	      (if (= (fx+ index 1) str-len) ; bad format string.
		  (tm:time-error 'tm:date-printer 'bad-date-format-string 
				 format-string)
		  (let ( (pad-char? (string-ref format-string (fx+ index 1))) )
		    (cond
		     ((char=? pad-char? #\-)
		      (if (= (fx+ index 2) str-len) ; bad format string.
			  (tm:time-error 'tm:date-printer 'bad-date-format-string 
					 format-string)
			  (let ( (formatter (tm:get-formatter 
					     (string-ref format-string
							 (fx+ index 2)))) )
			    (if (not formatter)
				(tm:time-error 'tm:date-printer 'bad-date-format-string 
					       format-string)
				(begin
				  (formatter locale date #f port)
				  (recurse locale date (fx+ index 3)
					   format-string str-len port))))))
		     
		     ((char=? pad-char? #\_)
		      (if (= (fx+ index 2) str-len) ; bad format string.
			  (tm:time-error 'tm:date-printer 'bad-date-format-string 
					 format-string)
			  (let ( (formatter (tm:get-formatter 
					     (string-ref format-string
							 (fx+ index 2)))) )
			    (if (not formatter)
				(tm:time-error 'tm:date-printer 'bad-date-format-string 
					       format-string)
				(begin
				  (formatter locale date #\space port)
				  (recurse locale date (fx+ index 3)
					   format-string str-len port))))))
		     (else
		      (let ( (formatter (tm:get-formatter 
					 (string-ref format-string
						     (fx+ index 1)))) )
			(if (not formatter)
			    (tm:time-error 'tm:date-printer 'bad-date-format-string 
					   format-string)
			    (begin
			      (formatter locale date #\0 port)
			      (recurse locale date (fx+ index 2)
				       format-string str-len port)))))))))))))

(: tm:print-date (:date: string procedure output-port -> . *))
(define (tm:print-date date fmt-str locale port)
  (tm:date-printer locale date 0 fmt-str (string-length fmt-str) port))

(define (srfi19:date->string date . rest)
  (call-with-output-string
    (lambda (port)
      (let ( (fmt-str (if (pair? rest) (car rest) "~c"))
             (locale (find-locale (and (pair? rest) (pair? (cdr rest))
                                       (cadr rest)))) )
        (tm:date-printer
         locale date 0
         fmt-str (string-length fmt-str)
         port)))))

;;* string->date

;(define (tm:char->int ch)
;    (cond
;     ((char=? ch #\0) 0)
;     ((char=? ch #\1) 1)
;     ((char=? ch #\2) 2)
;     ((char=? ch #\3) 3)
;     ((char=? ch #\4) 4)
;     ((char=? ch #\5) 5)
;     ((char=? ch #\6) 6)
;     ((char=? ch #\7) 7)
;     ((char=? ch #\8) 8)
;     ((char=? ch #\9) 9)
;     (else (tm:time-error 'tm:char->int 'bad-date-template-string
;			  (list "Non-integer character" ch)))))


;; This will almost always work, but it's much faster than the
;; original version.

(define tm:char->int
  (let ((z (char->integer #\0))) (lambda (c) (fx- (char->integer c) z))))

;; read an integer upto n characters long on port; upto -> #f if any length
(define (tm:integer-reader upto port)
    (define (accum-int port accum nchars)
      (let ((ch (peek-char port)))
	(if (or (eof-object? ch)
		(not (char-numeric? ch))
		(and upto (>= nchars  upto )))
	    accum
	    (accum-int port (+ (* accum 10) (tm:char->int (read-char
							   port))) (+
								    nchars 1)))))
    (accum-int port 0 0))

(define (tm:make-integer-reader upto)
  (lambda (port)
    (tm:integer-reader upto port)))

;; read *exactly* n characters and convert to integer; could be padded
(define (tm:integer-reader-exact n port)
  (let ( (padding-ok (the boolean #t)) )
    (define (accum-int port accum nchars)
      (let ((ch (peek-char port)))
	(cond
	 ((>= nchars n) accum)
	 ((eof-object? ch) 
	  (tm:time-error 'string->date 'bad-date-template-string 
			 "Premature ending to integer read."))
	 ((char-numeric? ch)
	  (set! padding-ok #f)
	  (accum-int port (+ (* accum 10) (tm:char->int (read-char
							   port)))
		     (+ nchars 1)))
	 (padding-ok
	  (read-char port) ; consume padding
	  (accum-int port accum (+ nchars 1)))
	 (else ; padding where it shouldn't be
	  (tm:time-error 'string->date 'bad-date-template-string 
			  "Non-numeric characters in integer read.")))))
    (accum-int port 0 0)))

(define (tm:make-integer-exact-reader n)
  (lambda (port)
    (tm:integer-reader-exact n port)))

(define (tm:float-reader port)
  (let loop ((accum 0.)
	     (fraction #f))
    (let ((ch (peek-char port)))
      (cond ((eof-object? ch) accum)
	    ((or (char=? #\, ch)
		 (char=? #\. ch))
	     (if (not fraction)
		 (begin
		   (read-char port)
		   (loop accum -1))
		 accum))
	    ((not (char-numeric? ch))
	     accum)
	    (else
	     (if (not fraction)
		 (loop (+ (* accum 10) (tm:char->int (read-char port))) 
		       fraction)
		 (loop (+ accum (* (tm:char->int (read-char port)) 
				   (expt 10 fraction)))
		       (sub1 fraction))))))))

(%early-once-only
 (define tm:named-tz-offset-table (make-string-table))
 (do ((l
       '(("UT" 0) ("GMT" 0) ("EST" -18000) ("EDT" 14400)
         ("CST" 21600) ("CDT" 18000) ("MST" 25200) ("MDT" 21600)
         ("PST" 2880) ("PDT" 25200)
         ("CET" -3600) ("CEST" -7200)
         ("Z" 0))
       (cdr l)))
     ((null? l))
   (hash-table-set! tm:named-tz-offset-table (caar l) (cadar l))))

(define (tm:named-tz->offset str)
  (hash-table-ref/default tm:named-tz-offset-table str #f))

(define (tm:zone-reader port) 
  (let ( (offset 0) 
	 (positive? (the boolean #f)) )
    (let ( (ch (read-char port)) )
      (cond
       ((eof-object? ch)
        (tm:time-error 'string->date 'bad-date-template-string
                       (list "EOF instead of time zone number")))
       ((char-alphabetic? ch)
        (let loop ((s (list ch)))
          (let ((ch (peek-char port)))
            (if (and (char? ch) (char-alphabetic? ch))
                (loop (cons (read-char port) s))
                (let ((str (apply string (reverse! s))))
                  (or (tm:named-tz->offset str)
                      (tm:time-error
                       'string->date 'bad-date-string
                       (list "Invalid time zone" str))))))))
       (else
         (cond
          ((char=? ch #\+) (set! positive? #t))
          ((char=? ch #\-) (set! positive? #f))
          (else (tm:time-error 'string->date 'bad-date-template-string
                               (list "Invalid time zone +/-" ch))))
         (let ((ch (read-char port)))
           (if (eof-object? ch)
               (tm:time-error 'string->date 'bad-date-template-string
                              (list "EOF in time zone number")))
           (set! offset (* (tm:char->int ch)
                           10 60 60)))
         (let ((ch (read-char port)))
           (if (eof-object? ch)
               (tm:time-error 'string->date 'bad-date-template-string
                              (list "EOF in time zone number")))
           (set! offset (+ offset (* (tm:char->int ch)
                                     60 60))))
         (let ((ch (read-char port)))
           (if (eof-object? ch)
               (tm:time-error 'string->date 'bad-date-template-string
                              (list "EOF in time zone number")))
           (set! offset (+ offset (* (tm:char->int ch)
                                     10 60))))
         (let ((ch (read-char port)))
           (if (eof-object? ch)
               (tm:time-error 'string->date 'bad-date-template-string
                              (list "EOF in time zone number")))
           (set! offset (+ offset (* (tm:char->int ch)
                                     60))))
         (if positive? offset (- offset)))))))

;; looking at a char, read the char string, run through indexer, return index
(define (tm:locale-reader port indexer)
  (let* ( (str (call-with-output-string
                 (lambda (to-port)
		   (let loop ((port port) (to-port to-port))
		     (let ((ch (peek-char port)))
		       (if (char-alphabetic? ch)
			   (begin (write-char (read-char port) to-port) 
				  (loop port to-port)))))))) 
          (index (indexer str)) )
    (if index index
	(tm:time-error 'string->date
		       'bad-date-template-string
		       (list "Invalid string ~a for " str indexer)))))

(define (tm:make-locale-reader indexer)
  (lambda (port)
    (tm:locale-reader port indexer)))

(define (tm:make-char-id-reader char)
  (lambda (port)
    (if (char=? char (read-char port))
	char
	(tm:time-error 'string->date
		       'bad-date-template-string
		       "Invalid character match."))))

;; A List of formatted read directives.
;; Each entry is a list.
;; 1. the character directive; 
;; a procedure, which takes a character as input & returns
;; 2. #t as soon as a character on the input port is acceptable
;; for input,
;; 3. a port reader procedure that knows how to read the current port
;; for a value. Its one parameter is the port.
;; 4. a action procedure, that takes the value (from 3.) and some
;; object (here, always the date) and (probably) side-effects it.
;; In some cases (e.g., ~A) the action is to do nothing

; (%early-once-only

(define tm:read-directives 
  (let ( (ireader4 (tm:make-integer-reader 4))
	 (ireader2 (tm:make-integer-reader 2))
	 (ireaderf (tm:make-integer-reader #f))
	 (eireader2 (tm:make-integer-exact-reader 2))
	 (eireader4 (tm:make-integer-exact-reader 4))
	 (locale-reader-abbr-weekday (tm:make-locale-reader
				      tm:locale-abbr-weekday->index))
	 (locale-reader-long-weekday (tm:make-locale-reader
				      tm:locale-long-weekday->index))
	 (locale-reader-abbr-month   (tm:make-locale-reader
				      tm:locale-abbr-month->index))
	 (locale-reader-long-month   (tm:make-locale-reader
				      tm:locale-long-month->index))
	 (char-fail (lambda (ch) #t))
	 (do-nothing (lambda (val object) (begin)))
	 )
		    
  (list
   (list #\~ char-fail (tm:make-char-id-reader #\~) do-nothing)
   (list #\a char-alphabetic? locale-reader-abbr-weekday do-nothing)
   (list #\A char-alphabetic? locale-reader-long-weekday do-nothing)
   (list #\b char-alphabetic? locale-reader-abbr-month
	 (lambda (val object)
	   (set-date-month! object val)))
   (list #\B char-alphabetic? locale-reader-long-month
	 (lambda (val object)
	   (set-date-month! object val)))
   (list #\d char-numeric? ireader2 (lambda (val object)
					       (set-date-day!
						object val)))
   (list #\e char-fail eireader2 (lambda (val object)
					       (set-date-day! object val)))
   (list #\f char-numeric? tm:float-reader 
	 (lambda (val object)
	   (receive
	    (second nano)
	    (tm:split-real val)
	    (set-date-second! object (floor second))
	    (set-date-nanosecond! object (inexact->exact (floor (* tm:nano nano)))))))
   (list #\h char-alphabetic? locale-reader-abbr-month
	 (lambda (val object)
	   (set-date-month! object val)))
   (list #\H char-numeric? ireader2 (lambda (val object)
							(set-date-hour! object val)))
   (list #\k char-fail eireader2 (lambda (val object)
					       (set-date-hour! object val)))
   (list #\m char-numeric? ireader2 (lambda (val object)
					       (set-date-month! object val)))
   (list #\M char-numeric? ireader2 (lambda (val object)
					       (set-date-minute!
						object val)))
   (list #\N char-numeric? ireaderf (lambda (val object)
					       (set-date-nanosecond!
						object val)))
   (list #\S char-numeric? ireader2 (lambda (val object)
				      (set-date-second! object val)))
   (list #\y char-fail eireader2 
	 (lambda (val object)
	   (set-date-year! object (tm:natural-year val))))
   (list #\Y char-numeric? ireader4 (lambda (val object)
					       (set-date-year! object val)))
   (list #\z (lambda (c) (and (char? c) (or (char-alphabetic? c)
                                            (char=? c #\-)
                                            (char=? c #\+))))
	 tm:zone-reader (lambda (val object)
			  (set-date-zone-offset! object val)))
   (list #\Z (lambda (c) (and (char? c) (or (char-alphabetic? c)
                                            (char=? c #\-)
                                            (char=? c #\+))))
	 tm:zone-reader (lambda (val object)
			  (set-date-zone-offset! object val)))
   )))

(define tm:read-directives-table (make-character-table))

(for-each (lambda (d) (hash-table-set! tm:read-directives-table (car d) d))
          tm:read-directives)

; ) ;; %early-once-only

(define (tm:string->date date index format-string str-len port template-string input-string)
  (define (skip-until port skipper)
    (let ((ch (peek-char port)))
      (if (eof-object? ch)
	  (tm:time-error 'string->date 'date-format-string-exausted template-string)
	  (if (not (skipper ch))
	      (begin (read-char port) (skip-until port skipper))))))
  (let loop ((index index))
      (if (fx>= index str-len)
	  (begin)
	  (let ( (current-char (string-ref format-string index)) )
	    (if (not (char=? current-char #\~))
		(let ((port-char (read-char port)))
		  (if (or (eof-object? port-char)
			  (not (char=? current-char port-char)))
		      (tm:time-error 'string->date 'unexpected-char (format "~a expected ~a from ~a at ~a[~a]" port-char current-char format-string input-string index)))
		  (loop (add1 index)))
		;; otherwise, it's an escape, we hope
		(let ((nidx (add1 index)))
		  (if (> nidx str-len)
		    (tm:time-error 'string->date 'bad-date-format-string template-string)
		    (let* ( (format-char (string-ref format-string nidx))
			    (format-info ;; (assoc format-char tm:read-directives)
			     (hash-table-ref tm:read-directives-table
					     format-char
					     (lambda ()
					       (tm:time-error
						'string->date 'unknown-format format-char)))) )
		      (let ((skipper (cadr format-info))
			    (reader  (caddr format-info))
			    (actor   (cadddr format-info)))
			(skip-until port skipper)
			(let ((val (reader port)))
			  (if (eof-object? val)
			      (tm:time-error 'string->date 'failed-to-read format-char)
			      (actor val date)))
			(loop (add1 nidx)))))))))))

(define (srfi19:string->date input-string template-string . tz-offset)
  (define (tm:date-ok? date)
    (and (date-nanosecond date)
	 (date-second date)
	 (date-minute date)
	 (date-hour date)
	 (date-day date)
	 (date-month date)
	 (date-year date)
	 (date-zone-offset date)))
  (let ( (newdate (make-date 0 0 0 0
                             0 0 0 ; #f #f #f
                             (if (pair? tz-offset) (car tz-offset) 0))) )
    (tm:string->date newdate
		     0
		     template-string
		     (string-length template-string)
		     (open-input-string input-string)
		     template-string input-string)
    (if (tm:date-ok? newdate)
	newdate
	(tm:time-error 'string->date 'bad-date-format-string (list "Incomplete date read. " newdate template-string)))))
