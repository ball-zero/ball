;; (C) 1999-2006, Joerg F. Wittenberger see http://www.askemos.org

;; Even though the file is named receive.scm it contains some more
;; things wich ought to go to their own files.

;; Porting dependancy section.  Uhm, well not really.

(define-macro (fx+ a b) `(fixnum+ ,a ,b))
(define-macro (fx- a b) `(fixnum- ,a ,b))
(define-macro (fx>= a b) `(fixnum>=? ,a ,b))
(define-macro (fx< a b) `(fixnum<? ,a ,b))

(define bind-exit call-with-current-continuation)
(define (make-character-table) (make-table eqv? immob->hash))
(define-macro (receive vars expr . body)
  `(call-with-values (lambda () ,expr)
     (lambda ,vars . ,body)))

;; Like let* but allowing for multiple-value bindings
(define-macro (let-values* bindings . body)
  (if (null? bindings) (cons 'begin body)
      (apply
       (lambda (vars initializer)
	 (let ((cont 
		(cons 'let-values* 
		      (cons (cdr bindings) body))))
	   (cond
	    ((not (pair? vars))		; regular let case, a single var
	     `(let ((,vars ,initializer)) ,cont))
	    ((null? (cdr vars))		; single var, see the prev case
	     `(let ((,(car vars) ,initializer)) ,cont))
	    (else			; the most generic case
	     `(receive ,vars ,initializer ,cont)))))
       (car bindings))))

;(define (addstr (buf <string>) i (str <string>) j flush)
;  (do (((j <fixnum>) j (add1 j))
;       ((i <fixnum>) i (add1 i)))
;      ((or (eqv? i 512) (eqv? j (string-length str)))
;       (if (eqv? i 512) (flush buf i str j addstr) i))
;    (string-set! buf i (string-ref str j))))

(define-glue (string-splice! buf si str sj cont)
 literals: ((& string-splice!))
{
  UINT_32 i=fx2int(si), j=fx2int(sj), n, m,
          l=string_length(str),  bl=string_length(buf);
  register char *dst = string_text(buf) + i;
  register char *src = string_text(str) + j;
  register char *end = NULL;
  n = bl-i; m = l-j;
  n = n < m ? n : m;
  end = src + n;
  while ( src != end ) *dst++=*src++;
  i += n;
  if( i == bl ) {
    obj now = cont;
    si = int2fx(i);
    sj = int2fx(j+n);
    cont = TLREFB(0);
    APPLY(5, now);
  } else {
    REG0 = int2fx(i);
    RETURN1();
  }
})

;; string-prefix-length    s1 s2 [start1 end1 start2 end2] -> integer
(define-safe-glue (string-prefix-length-intern (s1 <string>) (s2 <string>)
					       (start1 <raw-int>) (end1 <raw-int>)
					       (start2 <raw-int>) (end2 <raw-int>))
{

  char *s = string_text(s1);
  char *t = string_text(s2);
  int result=0;

  /* skip common prefix */
  while( start1 < end1 && start2 < end2 && s[start1] == t[start2] )
    ++start1, ++start2, ++result;

  REG0 = int2fx(result);
  RETURN(1);
})

(define (string-prefix-length s1 s2 . rest)
  (if (pair? rest)
      (apply string-prefix-length-intern s1 s2 rest)
      (string-prefix-length-intern
       s1 s2 0 (string-length s1) 0 (string-length s2))))

;;** from SRFI 1

(define-macro (null-list? a) `(null? ,a))

(define (list= = . lists)
  (or (null? lists) ; special case

      (let lp1 ((list-a (car lists)) (others (cdr lists)))
        (or (null? others)
            (let ((list-b (car others))
                  (others (cdr others)))
              (if (eq? list-a list-b)        ; EQ? => LIST=
                  (lp1 list-b others)
                  (let lp2 ((la list-a) (lb list-b))
                    (if (null-list? la)
                        (and (null-list? lb)
                             (lp1 list-b others))
                        (and (not (null-list? lb))
                             (= (car la) (car lb))
                             (lp2 (cdr la) (cdr lb)))))))))))

(define (srfi:cons* first . rest)
  (let recur ((x first) (rest rest))
    (if (pair? rest)
	(cons x (recur (car rest) (cdr rest)))
	x)))

;;; take & drop

(define (take lis k)
  (let recur ((lis lis) (k k))
    (if (zero? k) '()
	(cons (car lis)
	      (recur (cdr lis) (- k 1))))))

(define (drop lis k)
  (let iter ((lis lis) (k k))
    (if (zero? k) lis (iter (cdr lis) (sub1 k)))))

(define (take! lis k)
  (if (zero? k) '()
      (begin (set-cdr! (drop lis (- k 1)) '())
	     lis)))

;;; TAKE-RIGHT and DROP-RIGHT work by getting two pointers into the list, 
;;; off by K, then chasing down the list until the lead pointer falls off
;;; the end.

(define (take-right lis k)
  (let lp ((lag lis)  (lead (drop lis k)))
    (if (pair? lead)
	(lp (cdr lag) (cdr lead))
	lag)))

(define (drop-right lis k)
  (let recur ((lag lis) (lead (drop lis k)))
    (if (pair? lead)
	(cons (car lag) (recur (cdr lag) (cdr lead)))
	'())))

;;; In this function, LEAD is actually K+1 ahead of LAG. This lets
;;; us stop LAG one step early, in time to smash its cdr to ().

(define (drop-right! lis k)
  (let ((lead (drop lis k)))
    (if (pair? lead)

	(let lp ((lag lis)  (lead (cdr lead)))	; Standard case
	  (if (pair? lead)
	      (lp (cdr lag) (cdr lead))
	      (begin (set-cdr! lag '())
		     lis)))

	'())))	; Special case dropping everything -- no cons to side-effect.

(define (unzip2 lis)
  (let recur ((lis lis))
    (if (pair? lis)
	(let ((elt (car lis)))
	  (receive (a b) (recur (cdr lis))
		   (values (cons (car  elt) a)
			   (cons (cadr elt) b))))
	(values '() '()))))

;; (define (every? p? ls)
;;   (cond ((null? ls) #t)
;;         ((pair? ls) (and (p? (car ls))
;;                          (every? p? (cdr ls))))
;;         (else #f)))

(define (find pred list)
  (cond ((find-tail pred list) => car)
	(else #f)))

(define (find-tail pred list)
  (let lp ((list list))
    (and (not (null? list))
	 (if (pred (car list)) list
	     (lp (cdr list))))))

(define any any?)
(define every every?)

(define (alist-cons key datum alist)
  (cons (cons key datum) alist))

(define (alist-ref key alist)
  (cond ((assq key alist) => cdr)
        (else #f)))

(define (alist-delete key alist)             
  (cond ((null? alist) 
	 '())
	((eq? (caar alist) key) 
	 (alist-delete key (cdr alist)))
	(else 
	 (cons (car alist)
	       (alist-delete key (cdr alist))))))

(define (alist-delete-first key alist)             
  (cond ((null? alist) 
	 '())
	((eq? (caar alist) key)
	 (cdr alist))
	(else 
	 (cons (car alist)
	       (alist-delete-first key (cdr alist))))))

(define (alist-remove-duplicates alist)
  (define (rem alist already)
    (cond ((null? alist)               '())
	  ((memq (caar alist) already) (rem (cdr alist) already))
	  (else                        (cons (car alist)
					     (rem (cdr alist)
						  (cons (caar alist)
							already))))))
  (rem alist '()))

(define (sorted-alist-cons < key datum alist)
  (let ((entry (cons key datum)))
    (let loop ((alist alist))
      (if (null? alist)
	  (list entry)
	  (if (< key (caar alist))
	      (cons entry alist)
	      (cons (car alist) (loop (cdr alist))))))))

;; Return a list of elements from lst, which satisfy pred.
#|
(define (filter pred lst)
  (let loop ((lst lst))
    (if (pair? lst)
        (if (pred (car lst))
            (cons (car lst) (loop (cdr lst)))
            (loop (cdr lst)))
        lst)))
|#

(define (filter pred lis)
  (let recur ((lis lis))		
    (if (pair? lis)
	(let ((head (car lis))
	      (tail (cdr lis)))
	  (if (pred head)
	      (let ((new-tail (recur tail))) ; Replicate the RECUR call so
		(if (eq? tail new-tail) lis
		    (cons head new-tail)))
	      (recur tail)))
	lis)))

(define (remove pred lst)
  (let loop ((lst lst))
    (if (pair? lst)
        (if (not (pred (car lst)))
            (cons (car lst) (loop (cdr lst)))
            (loop (cdr lst)))
        lst)))

(define (delete x lst . =)
  (let ((compare (if (pair? =) (car =) equal?)))
    (let loop ((lst lst))
      (if (pair? lst)
          (if (compare (car lst) x)
              (loop (cdr lst))
              (cons (car lst) (loop (cdr lst))))
          lst))))

(define (partition pred lst)
  (let loop ((lst lst) (a '()) (b '()))
    (if (pair? lst)
        (if (pred (car lst))
            (loop (cdr lst) (cons (car lst) a) b)
            (loop (cdr lst) a (cons (car lst) b)))
        (values (reverse! a) (reverse! b)))))

(define (cdrs lists)
  (call-with-current-continuation
   (lambda (abort)
     (let recur ((lists lists))
       (if (pair? lists)
	   (let ((lis (car lists)))
	     (if (null-list? lis) (abort '())
		 (cons (cdr lis) (recur (cdr lists)))))
	   '())))))

(define (cars+cdrs lists)
  (call-with-current-continuation
    (lambda (abort)
      (let recur ((lists lists))
        (if (pair? lists)
	    (receive (list other-lists) (values (car lists) (cdr lists))
	      (if (null? list) (abort '() '()) ; LIST is empty -- bail out
		  (receive (a d) (values (car list) (cdr list))
		    (receive (cars cdrs) (recur other-lists)
		      (values (cons a cars) (cons d cdrs))))))
	    (values '() '()))))))

(define (cars+ lists last-elt) ; (append! (##sys#map car lists) (list last-elt))
  (let recur ((lists lists))
    (if (pair? lists) (cons (caar lists) (recur (cdr lists))) (list last-elt))))

;;; Like CARS+CDRS, but we pass in a final elt tacked onto the end of
;;; the cars list. What a hack.

(define (cars+cdrs+ lists cars-final)
  (bind-exit
    (lambda (abort)
      (let recur ((lists lists))
        (if (pair? lists)
	    (receive (list other-lists) (values (car lists) (cdr lists))
	      (if (null? list) (abort '() '()) ; LIST is empty -- bail out
		  (receive (a d) (values (car list) (cdr list))
		    (receive (cars cdrs) (recur other-lists)
		      (values (cons a cars) (cons d cdrs))))))
	    (values (list cars-final) '()))))))

;; Bidirectional fold:
;; http://www.dcs.gla.ac.uk/publications/paperdetails.cfm?id=7063

(define (fold kons knil lis1 . lists)
  (if (pair? lists)
      (let lp ((lists (cons lis1 lists)) (ans knil))	; N-ary case
	(receive (cars+ans cdrs) (cars+cdrs+ lists ans)
                 (if (null? cars+ans) ans ; Done.
                     (lp cdrs (apply kons cars+ans)))))
      
      (let lp ((lis lis1) (ans knil))			; Fast path
	(if (null? lis) ans
	    (lp (cdr lis) (kons (car lis) ans))))))

(define (fold-right kons knil lis1 . lists)
  (if (pair? lists)
      (let recur ((lists (cons lis1 lists)))		; N-ary case
	(let ((cdrs (cdrs lists)))
	  (if (null? cdrs) knil
	      (apply kons (cars+ lists (recur cdrs))))))

      (let recur ((lis lis1))				; Fast path
	(if (null-list? lis) knil
	    (let ((head (car lis)))
	      (kons head (recur (cdr lis))))))))

(define (append-reverse rev-head tail)
  (let lp ((rev-head rev-head) (tail tail))
    (if (null? rev-head) tail
	(lp (cdr rev-head) (cons (car rev-head) tail)))))

(define orig.member member)

(define (srfi:member x lis . maybe-=)
  (if (pair? maybe-=)
      (let ((= (if (pair? maybe-=) (car maybe-=) equal?)))
	(find-tail (lambda (y) (= x y)) lis))
      (orig.member x lis)))

(define (lset2<= = lis1 lis2) (every (lambda (x) (srfi:member x lis2 =)) lis1))

(define (lset<= = . lists)
  (or (not (pair? lists)) ; 0-ary case
      (let lp ((s1 (car lists)) (rest (cdr lists)))
	(or (not (pair? rest))
	    (let ((s2 (car rest))  (rest (cdr rest)))
	      (and (or (eq? s2 s1)	; Fast path
		       (lset2<= = s1 s2)) ; Real test
		   (lp s2 rest)))))))

(define (lset= = . lists)
  (or (not (pair? lists)) ; 0-ary case
      (let lp ((s1 (car lists)) (rest (cdr lists)))
	(or (not (pair? rest))
	    (let ((s2   (car rest))
		  (rest (cdr rest)))
	      (and (or (eq? s1 s2)	; Fast path
		       (and (lset2<= = s1 s2) (lset2<= = s2 s1))) ; Real test
		   (lp s2 rest)))))))

(define (lset-adjoin = lis . elts)
  (fold (lambda (elt ans) (if (srfi:member elt ans =) ans (cons elt ans)))
	lis elts))

(define (lset-union = . lists)
  (reduce (lambda (lis ans)		; Compute ANS + LIS.
	    (cond ((null? lis) ans)	; Don't copy any lists
		  ((null? ans) lis) 	; if we don't have to.
		  ((eq? lis ans) ans)
		  (else
		   (fold (lambda (elt ans)
			   (if (any (lambda (x) (= x elt)) ans)
					       ans
					       (cons elt ans)))
			 ans lis))))
	  '() lists))

(define (lset-intersection = lis1 . lists)
  (let ((lists (delete lis1 lists eq?))) ; Throw out any LIS1 vals.
    (cond ((any null? lists) '())		; Short cut
	  ((null? lists)          lis1)		; Short cut
	  (else (filter (lambda (x)
			  (every (lambda (lis) (srfi:member x lis =)) lists))
			lis1)))))

(define (lset-difference = lis1 . lists)
  (let ((lists (filter pair? lists)))	; Throw out empty lists.
    (cond ((null? lists)     lis1)	; Short cut
	  ((memq lis1 lists) '())	; Short cut
	  (else (filter (lambda (x)
			  (every (lambda (lis) (not (srfi:member x lis =)))
				 lists))
			lis1)))))

(define (lset-xor = . lists)
  (reduce (lambda (b a)			; Compute A xor B:
	    ;; Note that this code relies on the constant-time
	    ;; short-cuts provided by LSET-DIFF+INTERSECTION,
	    ;; LSET-DIFFERENCE & APPEND to provide constant-time short
	    ;; cuts for the cases A = (), B = (), and A eq? B. It takes
	    ;; a careful case analysis to see it, but it's carefully
	    ;; built in.

	    ;; Compute a-b and a^b, then compute b-(a^b) and
	    ;; cons it onto the front of a-b.
	    (receive
	     (a-b a-int-b)
	     (lset-diff+intersection = a b)
	     (cond ((null? a-b)     (lset-difference = b a))
		   ((null? a-int-b) (append b a))
		   (else (fold (lambda (xb ans)
				 (if (srfi:member xb a-int-b =) ans (cons xb ans)))
			       a-b
			       b)))))
	  '() lists))

(define (lset-diff+intersection = lis1 . lists)
  (cond ((every null? lists) (values lis1 '()))	; Short cut
	((memq lis1 lists)   (values '() lis1))	; Short cut
	(else (partition (lambda (elt)
			   (not (any (lambda (lis) (srfi:member elt lis =))
				     lists)))
			 lis1))))

;;** from SRFI 13

(define string-index string-search)

;; A dummy for our purpose.

(define (string-trim-both str . criterion)
  (trim-whitespace str))
