;; (C) 2000, 2001, 2004 Jörg F. Wittenberger see http://www.askemos.org

;; *** garbage collector for place space and (possible synchronisation
;; with other accessors to this space.

;; This code needs a rewrite. (JFW, 2013)

(: next-gc-time :time:)
(define next-gc-time *system-time*)

(define (mind-run-gc-kernel-now)
  (if (and (public-oid)
	   (srfi19:time<? next-gc-time *system-time*))
      (begin
	(mind-pool-cleanup!)
	(set! next-gc-time
	      (add-duration
	       *system-time*
	       (make-time
		'time-duration 0 (or (respond-timeout-interval) 10)))))))

;;*** Gate Keeper

;; The gate keeper locks out readers of the global variables
;; comprising the persistent store while they might be modified from
;; activities like "commit"s or "compact-database".

;;   TODO The mailbox *access-request* should be obsolete.  The
;;   *front-court* took it over.  But I just tried to send the
;;   release, commit and exit messages that way and failed.  Shouldn't
;;   be done tonight.

(define *front-court* (make-mailbox '*front-court*))
(define *front-door* (make-mutex '*front-door*))

(define (close-front-door) (mutex-lock! *front-door*))
(define (open-front-door) (mutex-unlock! *front-door*))

(define *access-acknowledge* (make-mailbox '*access-acknowledge*))

;; Every procedure, which want's to modify the mind in any way
;; must go through this synchronization step.
;; That is "respond!" might be allowed to reimplement complying behavior
;; for sake of efficiency.

(: access-lower-half ((procedure (*) . *) -> . *))
(define (access-lower-half proc)
  (guard
   (e (else (send-message! *front-court* 'access-release)
	    (raise e)))
   (receive r (proc (receive-message! *access-acknowledge*))
	    (send-message! *front-court* 'access-release)
	    (list->values r))))

;; with-mind-access is used by procedures, which already have data
;; which might contain persistent references.  These references could
;; become invalid when the store is closed/replaced or places are
;; garbage collected.  In that case these messages are sent to the
;; front court, where the gatekeeper will copy them entirely into the
;; persistent store.  Only those requests, which are already in
;; progress (i. ., went through enter-front-court before) are allowed
;; to send this type of request usually to prolong access for spawned
;; threads.  This restriction is required, because the gatekeeper will
;; have to empty this queue before compacting and after waiting for
;; all actors to leave the store.

(: with-mind-access-now ((procedure (*) . *) * -> . *))
(define (with-mind-access-now proc data)
  (send-message! *front-court* data)
  (access-lower-half proc))

(: with-mind-access ((procedure (*) . *) * -> . *))
(define with-mind-access with-mind-access-now) ; EXPORT

;; enter-front-court is used by protocol adaptors etc.  In addition to
;; what with-mind-access does, it will first check that the
;; *front-door* is open, indicating that the gatekeeper does not
;; already wants everybody to leave the persistent store.

(: enter-front-court-now ((procedure (*) . *) * -> . *))
(define (enter-front-court-now proc data)
  (apply
   (lambda (data)
     (mutex-lock! *front-door*)
     (send-message! *front-court* data)
     (mutex-unlock! *front-door*)
     (access-lower-half proc))
   data))

(: enter-front-court ((procedure (*) . *) * -> . *))
(define enter-front-court enter-front-court-now) ; EXPORT

;; with-mind-modification is used to wrap the critical section, in
;; which the persistant store is updated.
;;
;; BEWARE There is a protocol issue.  This critical section MUST be
;; nested inside of with-mind-access or enter-front-court.

(: go-on-access ((procedure (*) . *) * -> . *))
(define (go-on-access proc data)
  (proc data))

(define with-mind-modification go-on-access) ; EXPORT

(define (set-fast-access! flag)
  (case flag
    ((late)
     (set! with-mind-access go-on-access)
     (set! enter-front-court go-on-access)
     (set! with-mind-modification enter-front-court-now))
    ((early)
     (set! with-mind-access with-mind-access-now)
     (set! enter-front-court enter-front-court-now)
     (set! with-mind-modification go-on-access))
    ((free)
     (set! with-mind-access go-on-access)
     (set! enter-front-court go-on-access)
     (set! with-mind-modification go-on-access))))

(define (ordinary-exit)
  (logerr "Ordinary exit.\n")
  ;; (thread-terminate! (current-thread))
  (exit 0))

;; The gatekeeper watches that only a few commit requests are in the backlog,
;; than the compact-database is run, while no further access is granted.

;; CONFIG (define $max-commits 30)

(: $max-commits (or boolean fixnum))
(define $max-commits #f)
(define $commits-per-expire-ratio 25)

(define (is-commit-signal? x) (eq? x 'commit))

(define (make-mind-gatekeeper volatile?
                              set-working-mind!
                              compact-database
                              storage-access-mode
                              ) ; EXPORT
;;   (define (grantall2 left-p left-v)
;;     (do ((left-p left-p (cdr left-p))
;;          (left-v left-v (cdr left-v))
;;          (n 0 (add1 n)))
;;         ((null? left-p) n)
;;       (let ((msg (apply make-message (car left-p))))
;;         (for-each (lambda (s) (set-slot! msg (car s) (cdr s))) (car left-v))
;;         (send-message! *access-acknowledge* msg))))
;; 
  (define (grantall2 left-p left-v)
    (do ((left-p left-p (cdr left-p))
         (left-v left-v (cdr left-v))
         (n 0 (add1 n)))
        ((null? left-p) n)
      (for-each (lambda (x) (fset! (car left-p) (car x) (cdr x))) (car left-v))
      (send-message! *access-acknowledge* (car left-p))))
  (define (grantall1 left)
    (do ((left left (cdr left))
         (n 0 (add1 n)))
        ((null? left) n)
      (send-message! *access-acknowledge* (car left))))
  (define synchron-request #f)

  (define compact-preserves-address-space
    (memq storage-access-mode '(late free)))

  (define (store-ready? readers) (and (eqv? readers 0)
				      (or compact-preserves-address-space
					  (mailbox-empty? *front-court*))))



  (set-fast-access! storage-access-mode)

  (lambda ()
    (let loop ((commits 0)
               (commit-request #f)
               (readers 0)
               (waiting-readers '()))
      ;;(logerr "C: ~a R: ~a WR: ~a\n" commits readers (length waiting-readers))
      ;;(monitor-value 'commits commits)
      ;;(monitor-value 'commit-request commit-request)
      ;;(monitor-value 'readers readers)
      ;;(monitor-value 'waiting-readers waiting-readers)
      (if (< readers 0)
          (begin
            (logerr "GATEKEEPER FATAL ERROR ~a readers\n" readers)
            (exit 1)))
      (cond
;;        ((and $max-commits (>= commits $max-commits) (store-ready? readers))
;;         (force last-trash)
;;         (receive
;;          (p v) (split-all-request-for-tmp-storage volatile? waiting-readers)
;;          (set-cdr! mind-waiting-readers p)
;;          ;; FIXME terrible KLUDGE we do the compact only if it's not a
;;          ;; string argument, which currently indicates the only other
;;          ;; StorageAdaptor, fsm.  This code will break with the next
;;          ;; storage adaptor.
;;          (if (not (string? (cdar *the-registered-stores*)))
;;              (receive
;;               (new-store new-mind)
;;               (compact-database set-working-mind!
;;                                 (cdar *the-registered-stores*))
;;               (set-cdr! (car *the-registered-stores*) new-store)
;;               new-mind))
;; 	 (set-cdr! mind-waiting-readers '())
;;          (open-front-door)
;;          (loop 0 #f (grantall2 (cdr mind-waiting-readers) v) '())))
       ((and commit-request (store-ready? readers))
	(commit-mind *the-registered-stores*)
	(mind-run-gc-kernel-now)
	;; clear redundant commit requests
	(mailbox-drop-matching! *front-court* is-commit-signal?)
	(set! synchron-request (delete mind-run-gc-kernel-now synchron-request))
	(if (null? synchron-request) (set! synchron-request #f))
	(open-front-door)
	(loop 0 #f (grantall1 (reverse! waiting-readers)) '()))
       ((and synchron-request (store-ready? readers))
        (receive
         (p v)
         (split-all-request-for-tmp-storage volatile? waiting-readers)
         (set-cdr! mind-waiting-readers p)
         ;; (logerr "Sync ~a\n" (with-output-to-string
         ;;                      (lambda () (thread-list 'envt))))
         (for-each (lambda (f) (f)) synchron-request)
         (set! synchron-request #f)
         (open-front-door)
         (loop 0 #f (grantall2 (cdr mind-waiting-readers) v) '()))))
      (let ((msg (receive-message! *front-court*)))
        (case msg
          ((access-release)
           ;; (gc-now) ; maybe good, but did not bye any visible effect
           (loop commits commit-request (sub1 readers) waiting-readers))
          ((commit)
           (if (and (not commit-request)
                    (eqv? (modulo commits $commits-per-expire-ratio) 0))
	       (begin
		 (if (not (or commit-request synchron-request))
		     (close-front-door))
		 (set! synchron-request
		       (cond
			((not synchron-request) (list mind-run-gc-kernel-now))
			((memq mind-run-gc-kernel-now synchron-request))
			(else (cons mind-run-gc-kernel-now synchron-request))))))
           (loop commits #t readers waiting-readers))
          (else
           (cond
            ((procedure? msg)
             (if (and (not commit-request) (not synchron-request))
                 (begin
                   (close-front-door)))
             (set! synchron-request
		   (cond
		    ((not synchron-request) (list msg))
		    ((memq msg synchron-request))
		    (else (cons msg synchron-request))))
             (loop commits commit-request readers waiting-readers))
            ((or commit-request
                 (and $max-commits (>= commits $max-commits))
                 synchron-request)
             (loop commits commit-request
                   readers (cons msg waiting-readers)))
            (else 
             (send-message! *access-acknowledge* msg)
             (loop commits commit-request
                   (add1 readers) waiting-readers)))))))))

(define (mind-commit)                   ; EXPORT
  (send-message! *front-court* 'commit))

(define (mind-execute-synchron thunk)   ; EXPORT
  (send-message! *front-court* thunk))

(define (mind-exit)                     ; EXPORT
  (mind-execute-synchron ordinary-exit))

;; This start to become ugly.  We need to seperate storable and
;; none-storable content from requests, if they are to be preserved
;; over gc time.

(define (split-request-for-tmp-storage volatile? request)
  (if (frame? request)
      (let loop ((left (frame-other request))
		 (persists '())
		 (volatile '()))
	(if (null? left)
	    (values persists volatile)
	    (if (debug 'VOL (volatile? (cdar left)))
		(loop (cdr left) persists (cons (car left) volatile))
		(loop (cdr left) (cons (car left) persists) volatile))))
      (values #f '())))

(define (split-all-request-for-tmp-storage volatile? all)
  (let loop ((left all)
             (persists '())
             (volatile '()))
    (if (null? left)
        (values persists volatile)
        (receive (p v) (split-request-for-tmp-storage volatile? (car left))
		 (for-each (lambda (x) (set-slot! (car left) (car x) #f)) v)
                 (loop (cdr left)
		       (cons (car left) persists)
		       (cons v volatile))))))
