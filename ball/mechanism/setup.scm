;; (C) 2000, 2001, 2003, 2007 Jörg F. Wittenberger see http://www.askemos.org

;; It's rather boring to see these messages.  However if the setup
;; fails, this is the first thing to turn on.

;; (define $https-client-verbose #t)
(enable-warnings #t)
($fsm-verbose #t)
($https-client-verbose #f)
($http-server-verbose #t)
($http-is-synchronous #t)
($enforce-frame-oid-match #t)
;;

(set-administrator-email! "jerry@localhost")
(define administrator-password "sesam")
(set-administrator-password! administrator-password #t)
(set-control-password! "exit" #t)

(ball-client-mode #t)
($http-client-connections-maximum 100)

;; For the legacy ACL used to set up the initial rights situation.
(set! $acl-file-name (make-pathname '("mechanism") "ACL.xml"))

(define default-respond-timeout-interval 5)
(define default-remote-fetch-timeout-interval 20)
(define default-short-local-timeout 0.200)

;; We - currently - expect the hostname to use and a port number to
;; start from on stdin.  Multiple of 1000 are recommented as start
;; port, since up to 999 port numbers might be grabed by one
;; representative (proxy/server).

(call-with-values
    (lambda ()
      (let ((in (read-line (current-input-port))))
	(if (or (eof-object? in)
		(equal? in ""))
	    (values "localhost" #f)
	    (let ((m ((pcre->proc "([^:]+):([[:digit:]]+)") in)))
	      (if m
		  (values (cadr m) (string->number (caddr m)))
		  (values in #f))))))
  (lambda (name port)
    (local-id name)
    (if port
	(begin
	  ($control-port (+ port 70))
	  ($http-server-port (+ port 80))
	  ($https-server-port (+ port 443))
	  ($external-port ($https-server-port))
	  ($lmtp-server-port (+ port 24))))))

;; Mechanism Setup

;; This setup installs just enough to get a trustworthy system, where
;; entities can find their code and communicate with each other.

;; As a debuging aid it still leaves a file "/tmp/setup.log" with some
;; information gathered.

;; TODO use some values from configuration and clean that up a lot.

(define setup-results 'Setup-Not-Run)

(define application-setup-procedure
  (lambda level
    (logerr "

Setup warning: application-setup-procedure not defined.
Applicatin not beeing set up.

")))

(define (s . level)                     ; shorthand for debugging
  (!order
   (begin (load (make-pathname '("mechanism") "setup.scm"))
          (load (make-pathname '("app") "setup.scm"))
          (if (pair? level)
              (setup-procedure (car level))
              (setup-procedure 1)))
   'setup))

(define (setup-procedure . level)
  (define (make-new name tree . att)
    (make-link-form name (apply make-new-element tree att)))

  (define (make-link-id name id)
    (make-link-form name name (make-xml-element
			       'id #f '()
			       (make-xml-literal
				(oid->string id)))))
  (define (make-output data content-type)
    (make-xml-element 
     'output #f 
     (list (make-xml-attribute 'space namespace-xml "preserve")
	   (make-xml-attribute 'method #f "text")
	   (make-xml-attribute 'media-type #f content-type))
     (literal data)))
  (define (plain tree) (xml-format tree))
  ;; run-sgmls-xml has problems with attribute namespaces.
  ;; (define (parsed-xml str) (run-sgmls-xml str))
  (define (parsed-xml str) (document-element (xml-parse str)))
  (define xml-content (property 'content-type "text/xml"))

  (define host (format #f "127.0.0.1:~a" ($http-server-port)))
  (define host-prop (property 'host host))
  (define write-prop (property 'type 'post))
  (define get-prop (property 'type 'read))

  ;; Mapped over a list of lists, where the car is an integer.  When
  ;; the level argument passed into setup-procedure indicates a lower
  ;; init level than this number, the request is returned, otherwise
  ;; #f telling the driver to skip the request.  0 is the virgin start
  ;; up and -1 evvectivly disables the request alltogether.
  ;;
  ;; cadr is the log message to emit and cddr are argments to
  ;; http-send...!
  (define setup-ve
    (let ((level (if (pair? level) (car level) 0)))
      (lambda (e)
        (delay (if (>= (car e) level)
                   (begin
                     (format #t (cadr e))
                     (logerr (cadr e))
                     (cddr e))
                   #f)))))
  (bind-exit
   (lambda (return)

;;FIXME: remove here and from init-http!, if bopcntrl.scm handles this
     (short-local-timeout #f)

     (thread-sleep! 3)                   ; time for initialization
     (http-for-each
      (lambda (c)
	(call-with-values
	    (lambda ()
	      (condition->fields (if (uncaught-exception? c)
				     (uncaught-exception-reason c)
				     c)))
	  (lambda (title msg args rest)
	    (logcond 'setup-procedure title msg args)))
	(logerr "EXIT\n" c) (return c))
      (map
       setup-ve
       `((-10 " setting up secret part.\n"
	    ,(make-message
	      xml-content
	      (property
	       'mind-body
	       (plain (make-new
		       "public"
		       (parsed-xml
			(filedata (make-pathname '("mechanism") "taboo-place" "xml")))
		       (cons 'secret ""))))
	      (property 'dc-identifier "")
	      (property 'dc-date (current-date (timezone-offset)))
	      write-prop host-prop)
	    ,(administrator) ,administrator-password)
	 (0 " setting up public rights text.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'dc-date (current-date (timezone-offset)))
	      (property
	       'mind-body
	       (plain
		(sxml
		 `(api:reply
		   (@ (@ (*NAMESPACES* (,namespace-mind ,namespace-mind-str api))))
		   (letseq
		    (bindings)
		    (link
		     (@ (name "public"))
		     (new
		      (@ (secret ""))
		      #|
		      2017-12-11: FIXME For the time being we do not attach anything as
		      it seems to be the culprit why we still find objects without creator.

		      This is one of the reasons why I want a rewrite.
		      Once we figured out the culprit we likely remove the latter when we
		      simpify the consensus implementation.  Experiments with STM based on

		      the chicken egg "hopefully" look promising.
		      However this is going to be a major rewrite and unlikely to be
		      backward compatible ever.

		      2018-01-20: Issue seems to be fixed; even without a rewrite.
                      It works, but it is still a bad idea, since puts pressure on replication.

		      (link
		       (@ (name "favicon.ico"))
		       (new (@ (action ,(literal one-oid)))
			    ($$ ,make-output ,(filedata (make-pathname '("app") "favicon" "ico")) "image/x-icon")))
		      |#
		      (output (@ (method "text") (media-type "text/xml")) ($$ ,filedata ,(debug 'FILE license-file))))))))))
	      host-prop write-prop)
	    ,(administrator) ,administrator-password)
	 (-1 " reading back temporary license page\n"
	    ,(make-message (property 'dc-identifier "") host-prop get-prop))
	 (-1 " setting up metaview.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"metaview"
				(parsed-xml
				 (filedata (make-pathname '("policy") "metaview" "xml")))
				(cons 'action "public"))))
	      host-prop write-prop))
	 (-1 " setting up metactrl.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"metactrl"
				(parsed-xml
				 (filedata (make-pathname '("policy") "metactrl" "xml")))
				(cons 'action "public"))))
	      host-prop write-prop))
	 (-1 " setting xslt up method.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"xslt-method"
				(parsed-xml
				 (filedata (make-pathname '("mechanism") "xslt-method" "dsl")))
				(cons 'action "public"))))
	      host-prop write-prop))
	 (-1 " setting up collection-method.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"collection-method"
				(parsed-xml
				 (filedata (make-pathname '("policy") "webdav" "dsl")))
				(cons 'action "public"))))
	      host-prop write-prop))
	 (-1 " setting up custom-collection-method.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"custom-collection-method"
				(parsed-xml
				 (filedata (make-pathname '("policy") "webdav-custom" "dsl")))
				(cons 'action "public"))))
	      host-prop write-prop))
	 (-1 " setting up nunu-method.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new "nunu-method"
					 (parsed-xml
					  (filedata (make-pathname '("policy") "nunu-method" "dsl")))
					 (cons 'action "public"))))
	      host-prop write-prop))
	 (-1 " setting up xslt-user.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"xslt-user"
				(parsed-xml
				 (filedata (make-pathname '("app") "xslt-user" "xml")))
				(cons 'action "public"))))
	      host-prop write-prop))
	 (-1 " setting up entry point create form.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"create-entry"
				(parsed-xml
				 (filedata (make-pathname '("policy") "create-entry" "xml")))
				(cons 'action "xslt-method"))))
	      host-prop write-prop))
	 (-1 " setting up system control page\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"system"
				(parsed-xml
				 (filedata (make-pathname '("policy") "sysinf" "xml")))
				(cons 'action "xslt-method"))))
	      host-prop write-prop))
	 (-1 " setting up welcome page\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body (filedata (make-pathname '("app") "welcome" "xml")))
	      host-prop write-prop))
	 (-1 " setting up private rights text.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property 'mind-body
			(plain (make-new
				"private"
				(parsed-xml
				 (filedata (make-pathname '("mechanism") "private-place" "xml")))
				(cons 'action "public"))))
	      host-prop write-prop))
	 (-1 " setting up public rights text.\n"
	    ,(make-message
	      xml-content
	      (property 'dc-identifier "")
	      (property
	       'mind-body
	       (plain
		(sxml
		 `(api:reply
		   (@ (@ (*NAMESPACES* (,namespace-mind ,namespace-mind-str api))))
		   (letseq
		    (bindings
		     ;;(bind (@ (name "public")) (id ($$ ,literal ,one-oid)))
		     (bind
		      (@ (name "metaview"))
		      (new
		       (@ (action "public"))
		       (output
			($$ ,parsed-xml ,(filedata (make-pathname '("policy") "metaview" "xml"))))))
		     (bind
		      (@ (name "metactrl"))
		      (new
		       (@ (action "public"))
		       (output
			($$ ,parsed-xml ,(filedata (make-pathname '("policy") "metactrl" "xml")))))))
		    (link (@ (name "favicon.ico"))
			  (new (@ (action "public"))
			   ($$ ,make-output ,(filedata (make-pathname '("app") "favicon" "ico")) "image/x-icon")))
		    (link (@ (name "metaview")) (ref "metaview"))
		    (link (@ (name "metactrl")) (ref "metactrl")))
		   (become
		    (output (@ (method "text") (media-type "text/xml")) ($$ ,filedata ,(debug 'FILE license-file))))))))
	      host-prop write-prop)))))
     (nu-action-init read-only-plain-transformation transaction-private-plain-transformation!)
     ($use-well-known-symbols (not (public-oid)))
     (if (not patch-in-zero-state) (start-protection!))
     (if #f
	 (with-output-to-file "/tmp/setup.log"
	   (lambda ()
	     (print-a-refs)
	     (format #t "public ~a me ~a\n" (public-oid) (my-oid))
	     (format #t "users ~a\n"
		     (fold-links
		      (lambda (k v i) (cons k i))
		      '() (fget (document (my-oid)) 'mind-links)))
	     (format #t "public names ~a\n"
		     (fold-links
		      (lambda (k v i) (cons k i))
		      '() (fget (document (public-oid)) 'mind-links)))
	     (format #t "public body\n~a\n"
		     (body->string (or (fget (document (public-oid)) 'mind-body)
				       "not yet avail")))
	     (format #t "public rights\n~a\n"
		     (fget (document (public-oid)) 'capabilities)))))

     ;; Learned enough now.
     (parameterize
      ((respond-timeout-interval default-respond-timeout-interval)
       (remote-fetch-timeout-interval default-remote-fetch-timeout-interval)
       (short-local-timeout default-short-local-timeout))
      (ball-save-config))
     (logerr "application setup follows\n")
     (guard
      (c (else (log-condition "exception in application-setup-procedure" c)))
      (apply application-setup-procedure level))

     (logerr "FSM Test. This may take quite some time.\n")
     (fsm-delay 0)
     (fsm-enforce-restore! (spool-directory))
     ;; Signal normal exit when done.
     (mind-exit)
     )))

(if (not (my-oid))
    (set! setup-results
	  (!order/thunk
	   (lambda () (with-output-to-string (lambda () (setup-procedure 0))))
	   'initsetup)))
