;; (C) 2000, 2001 Jörg F. Wittenberger see http://www.askemos.org

;;* Introduction

;; Definitions

;; Some important definitions given in the code here.

;; TODO fix the use of the terms (See "DFN frame", "DFN place" and
;; "DFN message") in the code adn bring an abstract version here.

;;* Module Import

;; We use some modules defined by the RScheme system.  Ports have to
;; implement parts of these services.  When breaking up this script in
;; compiled modules, the grouping giving here should be used as a
;; outline how to break things up.

;; Every environment we port to should at least support some constant
;; time mapping between keys and values.  I don't know by now why
;; syscalls is here, we need it for networking, but this seems to be a
;; different issue.

,(use tables)

;; A threading system is also pretty important because we have long
;; running actions and a lot of simultanous users.  Although threading
;; is easily implemented at the scheme level *if* the scheme in
;; question has full call/cc.  Unfortunately only a few
;; implementations don't warn you about performance impacts with
;; call/cc.  Note: This script is not (yet) prepared to work with
;; cooperative (nonpreemptive) threading.

,(use rs.sys.threads.manager)
,(use parallel)
;; The client code, which everybody can create, which also can travel
;; over the network, must be run from within protected environments (a
;; so called sandbox) und must not allow any side effects.  All good
;; schemes know some way to implement such a thing, but life is hard
;; and things are different.

;; The implementation SHOULD be module "function" but this is
;; intermixed implemented with place.

;; ,(use repl compiler mlink)

;; The rstore module implements a persistent storage.  See related
;; documentation.  It's exceptionally easy to use.  When porting any
;; storage system will do, if it allows to store the rdf data model
;; (see www.w3.org).

;; ,(use rstore)

;; Calendar is mostly for application support.

,(use calendar)

;; Finally we need to store files, run external programs and the like.

;; The regex module is something I'd like to avoid.  It has it's own
;; proprietary (albeit powerful) syntax instead of POSIX.

;; ,(use syscalls paths regex unixm)

,(use unixm)

;;;;;;;;;;

,(use protection)
,(use srfis structures pltsockets util tree notation function)
,(use bhob aggregate channel place webdavlo nu storage protocol)

;; ,(use notation_sgml)

;(load "../config.scm")

;;* Utilities

;(load "util.scm")


;;* Module Structure

;;** Utilities -- skip this module until you are in doubt.  It's a
;;   collection of basic functions most of which I'd like to see in a
;;   SRFI or S*RS.

;;(load "srfi/strings.scm")
;;(load "function/memoize.scm")

;;** Timeout -- there is user supplied code to execute and it's
;; possible to encode endless loops or heavy work.  The former is a
;; denial of service attack the latter a bad design.  We stop
;; execution of user code after a configurable (CFG_RESPOND_TIMEOUT)
;; time.

,(use timeout)

;;** SGML/XML Handling

;; {X/SG}ML-tree definition, construction queries and traversal.
;(load "tree.scm")

;;*** SGML Parsing

;; We use currently nsgmls to parse everything.

;; That's surely the most costly thing to do.  It shows that the whole
;; thing is capable to work with full SGML.  But for efficiency we'll
;; have TODO our own restricted parser for most xml data.

;(load "format/xml/render.scm")
;(load "format/sgml.scm")
;;*** XSLT

;; The XSLT transformation supports some additiona tags in the dsssl
;; name space.  These work like the XSLT tags with the same name, but
;; all XPath expressions are replaced with "DSSSL" (alike)
;; expressions.  Please don't rely too much on the dsssl extention.
;; It's just simpler to implement and intended for feature experiments.

;; The XSLT counterpart, including XPath is supposed to be supported soon.

;(load "function/xslt.scm")              ; XSLT and "DSSSL" transformation
;(load "place.scm")                      ; places, partial cacheing "nunu"

;;** Data Network

;;*** Basic Definitions

;; Properties -- some definitions to handle grove plans, the data
;;   definition for the persistent store.

;;   Most of these things stem from an attempt to reengineer sdc.

;;   Some ideas stem from the FramerD project.  A weekend hack for
;;   feasibility testing originaly lead to the desicion that FramerD
;;   would be a good tree database.  Maybe it is, but it's extended
;;   scheme implementation is way to slow and buggy.

;; Message Passing -- send-subject! a request (and respond! to it) to a
;;   place-chain.

;; Extension Mechanism

;(load "nunu.scm")                       ; data definition

;(load "nu.scm")                         ; name space handling

;;*** Evaluation

;; Evaluation of user provided expressions MUST only be done using a
;; restricted, side effect free environment (sandbox) using eval-sane.

;(load "function/scheme.scm")

;; Method Tables
;(load "methods.scm")                    ; import: tree, scheme, memoize

;;** Connectivity / Networking

;; Connectivity / Networking -- SMTP, HTTP aka WebDAV, debug evaluator.

;; All code in these modules is considered glue code, which envolves
;; over time with the requirements of the deployment environment.

;; A few helpers...
;(load "protocol/util.scm")

;; The API's are as simple as possible and SHOULD be kept stable.  But
;; that's not a dogma here.  The world changes...

;;*** SMTP
;(load "protocol/smtp/client.scm")

;; MISSING: Here we should define how to tunnel "send-subject!".  Proposed
;; prototype: (send!-email frame)

;;*** RFC 1421
;(load "protocol/rfc1421.scm")

;;*** HTTP

;; I feel a bit sorry incorporating HTTP server code, but having a
;; different process do this simple job would a) introduce another
;; dependency on the server program b) either require CGI scripting,
;; which is slow or make things even more complicated when using
;; fastcgi or alike tricks.  Some superflous copying would be
;; unavoidable in any of these cases.

;; After all the implementation here took less time than expected for
;; a fastcgi setup.

;;**** Abstract Support

;; The functions which are candidates to be factored out are imported
;; from ../util.scm.
;;**** HTTP server
;(load "protocol/http/server.scm")
;;**** HTTP client
;(load "protocol/http/client.scm")

;;** Persitent Data Storage

;; Essentially everything, which can be reached from the
;; "mind"-Variable defined later is somehow to be stored.  Be it a
;; data base, a distributed file system or a news group.

;;*** rscheme pstore

;; The pstore facility currently just keeps it persistent in one big
;; data base file.

;(load "storage/pstore.scm")

;;*** File System Mirror

;; The file system mirror is sort of an example how a second backing
;; store should be added.

;(load "storage/fsm.scm")

;;*** unixODBC connection

;; deemed to be a bad idea, isn't it?
;(define (isql-parse port)
;  (let loop ((r '()))
;    (let ((ln (read-line port)))
;      (if (or (eof-object? ln) (eqv? (string-length ln) 0))
;          (xml-parse (apply string-append (reverse! r)))
;          (loop (cons ln r))))))

;(define (init-odbc)
;  (set! call-odbc (make-process-stub "isql DSN UID" isql-parse))
;  (call-odbc (lambda (port)  (display "password" port))))

;;** access control, web access, initialization

;; Protection Mechanism -- A pretty general scheme to express all
;; sorts of protection schemes.

;; Web Access -- dispatch and default handling

,(use rs.db.rstore) ;; use rstore until nov 2000, no longer!
,(use protocol dispatch step hostmesh)

;; For late exported things:

,(use sql.generic pcregex)

,(use syscalls)                         ; only for operation-control
,(use rs.gcrypt)
,(use rs.gnutls)
,(use clformat)
,(use xmlrpc)				; xmlrpc is DEPRECIATED
,(use markdown)				; EXPERIMENTAL
,(use compiletime)
(load "userenvt.scm")
(define license-file "COPYING.html")
(load "main.scm")
