;; (C) 1999, 2000, 2001 J�rg F. Wittenberger see http://www.askemos.org

;; Run (n)sgmls from the sp (aka jade's parser) package to parse an
;; sgml document from the open port.  See nsgmls documentation for
;; it's output.  See sdc, where this code is copied from for more
;; events (which are droped here) and lazy eval (force/delay).

;; The code is a bit tedious and boring skip it if you don't need
;; details.  Furthermore, I'm sorry about the shape of this code.  It
;; has been refined to support tree- instead of stream output and
;; somewhat for xml name spaces.  So it became a mess and should be
;; rewritten.

;; CAUTION This is a literal copy from notation/xmp/parse to avoid
;; another dependancy.  Ugly KLUDGE, I know.

;; TODO think about code duplication with rscheme thread manager and
;; portability concern.

(define (xml-parse-register-xmlns attributes outer)
  (if (null? attributes)
      outer
      (let ((att (car attributes)))
        (cond
         ((eq? (xml-attribute-ns att) 'xmlns)
          (let ((nmsp (string->symbol (xml-attribute-ns att)))
                (old (assq (car att) outer)))
            (xml-parse-register-xmlns
             (cdr attributes)
             (if (and old (eq? (cdr old) nmsp))
                 outer
                 (cons (cons (gi att) nmsp) outer)))))
         ((eq? (gi att) 'xmlns)
          (let ((nmsp (string->symbol (xml-attribute-ns att)))
                (old (assq #f outer)))
            (xml-parse-register-xmlns
             (cdr attributes)
             (if (and old (eq? (cdr old) nmsp))
                 outer
                 (cons (cons #f nmsp) outer)))))
         (else (xml-parse-register-xmlns (cdr attributes) outer))))))

;; another literal copy

(define empty-namespace-declaration     ; xml names 4.0
  `((xml . ,namespace-xml)
    (#f . #f)))

;; Trying to use lots of feature for speed here.  Sorry.
;; FIXME: This really should be C code, as it originaly used to be.
;; That's even shorter!
(define char-0->integer (char->integer #\0))
;; You won't believe that this is feasable with interpreted code!
;; Please understand that ceck in of lange documents takes some time.
;; Every line of text is scanned 1by1 char for escaped characters.
;; Sdc does this in "C" and
(define (sgmls--! line initial next)
  (let ((sl (string-length line)))
    (let loop ((escape 0) (ccode 0) (ci 0) (co 0))
      (if (eqv? ci sl)
          (next (cons (make-xml-literal (substring line 0 co)) initial))
          (if (> escape 0)
              (case (string-ref line ci)
                ((#\n)             ; FIXME
                 ;; Strange thing is, we get (sometimes?) newlines like
                 ;; this and immediatly following \012
                 ;; which will bring in another one.  I "fix" this here
                 ;; by forgetting about one version.  This is probably
                 ;; blatantly wrong
                 (string-set! line co #\newline)
                                        ;(loop 0 0 (add1 ci) (add1 co))
                 (loop 0 0 (add1 ci) co)
                 )
                ((#\\) 
                 (string-set! line co #\\)
                 (loop 0 0 (add1 ci) (add1 co)))
                ((#\|)             ; FIXME SDATA entity
                 (sgmls--! (substring line (add1 ci) sl)
                           (cons (substring line 0 (add1 co)) initial)
                           next))
                (else
                 (let ((c (- (char->integer (string-ref line ci))
                             char-0->integer)))
                   (if (and (>= c 0) (<= c 9))
                       (let ((ccode (bitwise-or (logical-shift-left ccode 3)
                                                c)))
                         (if (< escape 3)
                             (loop (add1 escape) ccode (add1 ci) co)
                             (begin
                               (string-set! line co (integer->char ccode))
                               (loop 0 0 (add1 ci) (add1 co)))))
                       (begin      ; error, not enough chars
                         (string-set! line co (integer->char ccode))
                         (loop 0 0 ci (add1 co)))))))
              (let ((c (string-ref line ci)))
                (if (eqv? c #\\)
                    (loop 1 0 (add1 ci) co)
                    (begin
                      (string-set! line co c)
                      (loop 0 0 (add1 ci) (add1 co))))))))))

(define (sgmls-entity-file-name line) line)

(define (parse-arg s)
  (let* ((length (string-length s))
         (s1 (string-search s #\space 0))
         (colon (let ((pos (string-search s #\: 0)))
                  (if (and pos (< pos s1)) pos #f)))
         (s2x (string-search s #\space (add1 s1)))
         (s2a (if s2x s2x length))
         (s2b (if s2x (add1 s2x) length))
         (name (string->symbol
                (if colon
                    (substring s (add1 colon) s1)
                    (string-downcase! (substring s 0 s1)))))
         (type (string->symbol (if (and colon
                                        (not (string=?
                                              (substring s (add1 s1) s2a)
                                              "IMPLIED")))
                                   (substring s 0 colon)
                                   (string-downcase!
                                    (substring s (add1 s1) s2a)))))
         (rawval (substring s s2b length))
         ;; Sdc distinguishes between argument types, which is only
         ;; usable if there's a DTD.  If we had a well formed XML
         ;; document without DTD, we where lost.  Hence we skip that
         ;; (for now) and keep a code example as reminder.
         ;(val (case type
         ;                  ((CDATA) rawval)
         ;                  ((ID)  (string-split rawval " "))
         ;                  ((TOKEN ENTITY) (string-split rawval " "))
         ;                  ((NOTATION) (string->symbol rawval))
         ;                  ((IMPLIED) #f)
         ;                  (else (error "parse-arg: argument type unknown.")
         ;                        rawval)))
         )
    (make-xml-attribute name type (if (eq? type 'token)
                                      (string-downcase! rawval)
                                      rawval))))

(define (parse-sgmls port name-spaces)
  (letrec ((sysid #f) (file #f) (args '()))
    (let loop ((tluser '()))
      (let ((t (read-char port) (read-char port))
            (line (read-line port) (read-line port)))
        (if (eof-object? t)
            (reverse! tluser)
            (case t
              ((#\-) (sgmls--! line tluser loop))
              ((#\?) (loop (cons (make-xml-pi 'FIXME line) tluser)))
              ((#\A) ;; "optimization": don't store empty implied atts
               (let ((att (parse-arg line)))
                 (if (not (and
                           (eq? (xml-attribute-ns att) 'implied)
                           (equal? (xml-attribute-value att) "")))
                     (set! args (cons att args))))
               (loop tluser))
              ((#\()
               (receive
                (gi ns) (xml-split-name (string-downcase! line))
                (let* ((inner (xml-parse-register-xmlns args name-spaces))
                       (this-name-space (assq ns inner)))
                  (if (not this-name-space)
                      (error "namespace \"~a\" not defined" ns))
                  (let ((elem (make-xml-element
                               gi (cdr this-name-space) args
                               (parse-sgmls port inner))))
                    (set! args '())
                    (loop (cons elem tluser))))))
              ((#\)) (reverse! tluser)) ; should we test? no, sgmls did.
              ((#\s) (set! sysid line) (loop tluser))
              ((#\f)
               (set! file (sgmls-entity-file-name line)) (loop tluser))
              ((#\E)
               (let* ((l (string-length line))
                      (s1 (string-search line #\space 0))
                      (s2a (string-search line #\space (add1 s1)))
                      (s2 (if s2a s2a (- l 1))))
                 (loop
                  (cons
                   (vector
                    'External-Definition
                    (string->symbol (substring line 0 s1))
                    (vector
                     (string->symbol (substring line (+ s1 1) s2))
                     (string->symbol (substring line (+ s2 1) l))
                     sysid
                     file))
                   tluser))))
              ((#\&)
               (loop (cons
                      (vector 'External-Reference (string->symbol line))
                      tluser)))
              ((#\N)
               (loop (cons (vector 'NOTATION (string->symbol line) sysid)
                           tluser)))
              ((#\S) (loop tluser))  ; drop it, #\{ delivers the same
              ((#\{)
               (loop (cons (vector  'STARTSUBDOC
                                    (string->symbol line)
                                    (vector 'SUBDOC
                                            ""
                                            sysid
                                            file))
                           tluser)))
              ((#\})
               (loop (cons (vector 'ENDSUBDOC (string->symbol line))
                           tluser)))
              ((#\#)		; FIXME drop additional sp info
               (loop tluser))
              ((#f #\C) (reverse! tluser))
              (else (loop tluser))))))))

;; Run the (n)sgmls command "cmd" if there are args, dump them to the
;; parser on stdin.  Return the parsed tree.

(define (run-sgmls cmd . args)
  (receive
   (r-s w-s) (pipe)
   (receive
    (e-r-s e-w-s) (pipe)
    (let (; we forget about that for now
          (writer (fork)))
      (if writer
          (begin                          ; Parse the (n)sgmls ouput.
            (fd-close e-w-s) (fd-close w-s)
            (let* ((iport (open-mbox-input-port r-s))
                   (result (parse-sgmls iport empty-namespace-declaration)))
              (close-input-port iport)    ; don't forget that! ~> 100%CPU
              (fd-close e-r-s) (fd-close r-s)
              (wait-for writer)
              result))
          (if (null? args)                ; Just run the "cmd".
              (begin
                                        ;(fd-dup2 e-w-s 2)
                (fd-dup2 w-s 1)
                (fd-close e-r-s) (fd-close r-s)
                (let ((result (system cmd)))
                  (fd-close w-s) (fd-close e-w-s)
                  (fd-close 1)
                  (process-exit result)))
              ;; Fork a dumper and run "cmd".  Beware: you can't use
              ;; threads.  The "system" of RScheme would block the
              ;; thread system.
              (begin
                (fd-close r-s) (fd-close e-r-s)
                (receive
                 (r-d w-d) (pipe)
                 (let ((dumper (fork)))
                   (if dumper
                       (begin
                                        ;(fd-dup2 e-w-s 2)
                         (fd-dup2 w-s 1) (fd-dup2 r-d 0)
                         (fd-close w-d)
                         (let ((result (system cmd)))
                           (fd-close r-d) (fd-close w-s)
                           (fd-close e-w-s)
                           (wait-for dumper)
                           (process-exit result)))
                       (let ((port (open-queued-output w-d)))
                         (fd-close e-w-s)
                         (fd-close w-s)
                         (fd-close r-d)
                         (for-each (lambda (i) (display i port)) args)
                         (close-output-port port)
                         (fd-close w-d)
                         (fd-close 1)
                         (process-exit 0))))))))))))

;; Run (n)sgmls with the XML system declaration.

;; FIXME: The declaration is as distributed from w3(?), but appears to
;; have a error, line ends somehow result in a (interger->character 0)
;; (or is this a bug in the de-escape code for text in run-sgmls?)

(define sgmls-base "/tmp/fsm")
(define (run-sgmls-xml . args)          ; EXPORT
  (apply
   run-sgmls
   (string-append
    "nsgmls -wno-valid -m " sgmls-base "CATALOG -D " sgmls-base " "
    sgmls-base "xml.dcl -")
   args))

(define (run-sgmls-sgml . args)         ; EXPORT
  (apply
   run-sgmls (string-append
              "nsgmls -m " sgmls-base "CATALOG -D " sgmls-base)
   args))
