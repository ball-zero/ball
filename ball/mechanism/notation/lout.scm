;; (C) 2000, 2001 J�rg F. Wittenberger see http://www.askemos.org

;;** Lout Rendering

(%early-once-only
(define lout-encoding (make-vector 255))

(do ((i 0 (add1 i)))
    ((eqv? i 255) #t)
  (let ((v (string (integer->char i))))
    (vector-set! lout-encoding i v)))

(vector-set! lout-encoding (char->integer #\") "\"\\\"\"")
(vector-set! lout-encoding (char->integer #\&) "\"&\"")
(vector-set! lout-encoding (char->integer #\{) "\"{\"")
(vector-set! lout-encoding (char->integer #\}) "\"}\"")
(vector-set! lout-encoding (char->integer #\|) "\"|\"")
(vector-set! lout-encoding (char->integer #\\) "\"\\\\\"")
(vector-set! lout-encoding (char->integer #\/) "\"/\"")
(vector-set! lout-encoding (char->integer #\#) "\"#\"")
(vector-set! lout-encoding (char->integer #\@) "\"@\"")
(vector-set! lout-encoding (char->integer #\^) "\"^\"")
(vector-set! lout-encoding (char->integer #\~) "\"~\"")
)

;; I should use map-matches-alternate from nu.scm (after moving it to
;; util).  For the time being, this stupid version will do.
(define (lout-quote-display str port)
  (let ((l (string-length str)))
    (do ((i 0 (add1 i)))
        ((eqv? i l) #t)
      ;;(display (case (string-ref str i)
      ;;           ((#\") "\"\\\"\"")
      ;;           ((#\&) "\"&\"")
      ;;           ((#\{) "\"{\"")
      ;;           ((#\}) "\"}\"")
      ;;           ((#\|) "\"|\"")
      ;;           ((#\\) "\"\\\\\"")
      ;;           ((#\/) "\"/\"")
      ;;           ((#\#) "\"#\"")
      ;;           ((#\@) "\"@\"")
      ;;           ((#\^) "\"^\"")
      ;;           ((#\~) "\"~\"")
      ;;           (else (string-ref str i))) port)
      (write-string
       port
       (vector-ref lout-encoding (char->integer (string-ref str i)))))))

(define (lout-keep-first head initial)
  (cons
   (vector-ref lout-encoding (char->integer (string-ref head 0)))
   initial))

(define (lout-keep-all head initial)
  (let loop ((result initial)
             (index (sub1 (string-length head))))
    (if (eqv? index -1)
        result
        (loop (cons (vector-ref lout-encoding
                                (char->integer (string-ref head index)))
                    result)
              (sub1 index)))))

(define (lout-special-char-handler head initial)
  (if (eqv? (string-length head) 1)
      (lout-keep-first head initial)
      (map-matches-alternate
       lout-collapsable-whitespace-sequence-regexp
       lout-keep-first
       lout-keep-all
       head
       initial)))

(define (lout-quote-display-collapse-space str port)
  (for-each
   (lambda (str) (write-string port str))
   (map-matches-alternate
    lout-literal-char-sequence-regexp
    cons
    lout-special-char-handler
    str
    '())))

(define-transformer lout-format-literal "lout:literal"
  ;; (environment) holds preserve space property here.
  (begin
    ((if (environment) lout-quote-display lout-quote-display-collapse-space)
     (xml-literal-value (sosofo))
     (current-output-port))
    (empty-node-list)))

(define-transformer lout-format-pi "lout:pi"
  (begin
    (if (eq? 'lout (xml-pi-tag (sosofo)))
	(let* ((data (xml-pi-data (sosofo)))
	       (len (string-length data)))
	  (if (illegal-lout? data)
	      (error "illegal lout ~a" data)
	      (begin
		;; To be documented: the processing instruction should
		;; find and leave lout in the "ignoring whithespace"
		;; mode.  We suround the output with such ignored space
		;; here to prevent run together instructions to form new
		;; tokens.
		(display #\space)
		(do ((n 0 (add1 n))) ((= len n)) (display (string-ref data n)))
		(display #\space)))))
    (empty-node-list)))

;; (define-transformer lout-format-element-maybe-we-want-this?_?_?
;;   (if (eq? (ns nl) namespace-lout)
;;       (begin
;;         (display "\n@")
;;         (display (gi nl))
;;         (for-each
;;          (lambda (att)
;;            (if (and (not (string-null? (xml-attribute-value att)))
;;                     (not (memq (xml-attribute-ns att) '(implied IMPLIED))))
;;                (begin
;;                  (display "\n ")
;;                  (display (car att))
;;                  (display " {")
;;                  (lout-quote-display (xml-attribute-value att)
;;                                      (current-output-port))
;;                  (display "}\n"))))
;; 	 (xml-element-attributes nl))
;;         (display "\n{")
;;         (xml-walk-down sosofos: (children nl))
;;         (display "}"))
;;       (xml-walk-down sosofos: (children nl)))
;;   (empty-node-list))

(define lout-setup-definitions
  (append
   (map (lambda (file) (cons file file))
        '(;; standard lout packages
	  "doc" "book" "report" "slides" "letter"
	  ;; extended definition, to use this you have to install
	  ;; policy/lout/letter.lout as /usr/share/lout/include/letter
	  ;;"letter"
	  ))))

(define-transformer lout-format-element "lout:element"
  (begin
    (if (eq? (ns (sosofo)) namespace-lout)
	(case (gi (sosofo))
	  ((setup)
	   (let ((str (assoc (data (sosofo)) lout-setup-definitions)))
	     (if str 
		 (begin
		   (display "\n@SysInclude{")
		   (display (cdr str))
		   (display "}")))))
	  (else (error "unknown lout directive ~a" (gi (sosofo)))))
	(transform
	 sosofos: (children (sosofo))
	 variables: (xml-space (xml-element-attributes (sosofo)) (environment))))
    (empty-node-list)))

(define lout-format-walk-rules
  (list
   (cons sxp:literal? lout-format-literal)
   (cons sxp:element? lout-format-element)
   (cons sxp:pi? lout-format-pi)
   (cons sxp:comment? xml-drop)))

(define (lout-format-fragment-at-current-output-port tree)
  ((xml-walk*
    #f #f
    'no-root
    (and (xml-element? tree)            ; variables holds "preserve space"
         (xml-space (xml-element-attributes tree) #f))
    '()					; namespaces
    '()                                 ; ancestor
    '()                                 ; self-node
    (if (pair? tree) tree (list tree))  ; what to walk
    #f                                  ; mode-choice
    )
   lout-format-walk-rules)
  tree)

(define (lout-format-fragment tree)
  (with-output-to-string
    (lambda () (lout-format-fragment-at-current-output-port tree))))

(define (lout-format-at-current-output-port tree)
  (lout-format-fragment-at-current-output-port tree)
  tree)

(define (lout-format-output-element tree) ; EXPORT
  (with-output-through-lout
   (attribute-string 'media-type tree)
   (lambda () (lout-format-at-current-output-port (children tree)))))

(define (lout-pdf-format tree)
  (with-output-through-lout
   "application/pdf"
   (lambda () (lout-format-at-current-output-port tree))))

(define pdf-format lout-pdf-format)
