;; (C) 2000, 2001, 2002, 2004 Jörg F. Wittenberger see http://www.askemos.org

(define xml-element-attribute-list-fold fold)

(define (xml-parse-register-xmlns attributes outer)
  (xml-element-attribute-list-fold
   (lambda (att outer)
     (cond
      ((eq? (xml-attribute-ns att) 'xmlns)
       (if (eq? (xml-attribute-name att) 'xml)
	   ;; ignore overwrite attempts for xml prefix (see XPointer 5.2.1)
	   outer
	   (let ((nmsp (string->symbol (xml-attribute-value att)))
		 (old (find-namespace-by-prefix
		       (xml-attribute-name att) outer)))
	     (if (and old (eq? (xml-namespace-uri old) nmsp))
		 outer
		 (cons (make-xml-namespace (xml-attribute-name att) nmsp)
		       outer)))))
      ((eq? (xml-attribute-name att) 'xmlns)
       (let ((nmsp (string->symbol (xml-attribute-value att)))
	     (old (find-namespace-by-prefix #f outer)))
	 (if (and old (eq? (xml-namespace-uri old) nmsp))
	     outer
	     (cons (make-xml-namespace #f nmsp) outer))))
      (else outer)))
   outer
   attributes))

(define empty-namespace-declaration     ; xml names 4.0
  (list
   (make-xml-namespace 'xml namespace-xml)
   (make-xml-namespace 'xmlns 'xmlns)
   (make-xml-namespace #f #f)))
