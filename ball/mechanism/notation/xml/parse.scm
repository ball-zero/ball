;; (C) 1999 - 2002 Jörg F. Wittenberger see http://www.askemos.org

;;; Module xml-parse

;; History: ported from bigloo module taken out of sdc 1.1.  You'll
;; see some references to e.g., "rdp".  That's a lazy tree walker from
;; sdc, which will be ported one fine day.  Than we'll have a config
;; option to work either as lazy stream or immediate tree.

;; This source looks a bit complicated because it inherited the
;; ability to change the parsing style from applicative order to
;; normal order and return either a tree or a stream of tokens (aside
;; from providing an almost sax alike interface).

(define-macro (ws) `(list 'or #\space #\tab #\newline #\cr)) ; [3] S

(define xml-encoding-regex
  (reg-expr->proc `(prefix (seq "<?xml"
                                ;; (* (or (not #\?) (seq #\? (not #\>))))
                                (? (seq (* ,(ws))
                                        "version" (* ,(ws)) #\= (* ,(ws))
                                        (or (seq #\' (* (not #\')) #\')
                                            (seq #\" (* (not #\")) #\"))))
                                (? (seq (* ,(ws))
                                        "encoding" (* ,(ws)) #\=
                                        (* ,(ws))
                                        (or (seq #\" (save (* (not #\"))) #\")
                                            (seq #\' (save (* (not #\'))) #\'))))
                                ;; "?>"
                                ))))
(define (xml-encoding str)
  (bind ((s e c d (xml-encoding-regex str)))
        (or c d)))

;; Signal stop unfolding.

(define-macro (unfold-terminate) #f)

;;* 2.11 End-of-Line Handling

(define any-but-eol-rs-regex
  (reg-expr->proc `(prefix (+ (not (or #\cr #\newline))))))

(define end-of-line-rs-regex
  (reg-expr->proc
   `(prefix (or (seq #\cr #\newline) (seq #\newline #\cr) #\newline #\cr))))

(define (make-source str)
  (let ((str (normalise-data str #f (xml-encoding str) "\n")))
    (vector
     str                                ; the actual source, constant
     (string-length str)
     0                                  ; current position
     )))

;;* Input stream
;;
;; Bigloo has a nice feature: regular grammars (forget it's lalr
;; stuff, its not thread save).  RScheme misses it.  Instead of
;; extending RScheme, which would be the better way to go, we emulate
;; it at the cost of memory.  But it'll be done tomorrow.

(define-macro (regex-case str . clauses)
  (let ((tmp (gensym)))
    `(let ((,tmp ,str))
       ,(let fold ((clauses clauses))
	  (if (null? clauses)
	      '(no-values)
	      (let ((match (gensym))
		    (c (car clauses)))
		(if (eq? 'else (car c))
		    `(let () ,@(cdr c))
		    `(let ((,match (,(car c) ,tmp)))
                      (if ,match
                          (apply (lambda ,(cadr c) ,@(cddr c)) ,match)
                          ,(fold (cdr clauses)))))))))))

(define-macro (match0 source test expr rest)
  `(receive* (start end) (,test (vector-ref ,source 0) (vector-ref ,source 2))
             (if start
                 (begin
                   (vector-set! ,source 2 end)
                   ,expr)
                 ,rest)))

(define-macro (match1 source test expr rest)
  `(bind ((start end submatch
                 (,test (vector-ref ,source 0) (vector-ref ,source 2))))
         (if start
             (begin
               (vector-set! ,source 2 end)
               ,expr)
             ,rest)))

(define-macro (match2 source test expr rest)
  `(bind ((start end submatch-1 submatch-2
                 (,test (vector-ref ,source 0) (vector-ref ,source 2))))
         (if start
             (begin
               (vector-set! ,source 2 end)
               ,expr)
             ,rest)))

(define-macro (regular-matches source lst alternate)
  (let loop ((lst lst))
    (if (null? lst)
       alternate
       (case (caar lst)
         ((0) `(match0 ,source ,(cadar lst) ,(caddar lst) ,(loop (cdr lst))))
         ((1) `(match1 ,source ,(cadar lst) ,(caddar lst) ,(loop (cdr lst))))
         ((2) `(match2 ,source ,(cadar lst) ,(caddar lst) ,(loop (cdr lst))))))))

;{{{ *** API ***: we define some signals the lexer can send.

; If we had a low level event based processor (like SAX) this where a
; superset of signals the application had to act upon.

; FIXME: Check whether we could match that with the SAX.

; The creator and gettor-functions might be exported, if we need them.
; I'm not convinced that any modifiers should ever leave this scope.

;; This macro was used once instead of normalizing before parsing.
;; The problem is, it doesn't do the trick always, but drops explicit
;; included return characters.
;(define-macro (clean-cr data)
;  `(if (string-search ,data #\cr)
;       (to-string* (string-split ,data #\cr))
;       ,data))

(define-macro (clean-cr data) data)

(define-macro (signal-processing-instruction tag instruction)
  `(make-xml-pi ,tag (clean-cr ,instruction)))
(define-macro (signal-doctype-declaration data)
  `(make-xml-doctype ,data))
(define (signal-error condition)
  (error "xml parse ~a" condition))
(define-macro (signal-comment data)
  `(make-xml-comment (clean-cr ,data)))
(define-macro (signal-name namespace name)
  `(values 'NAME ,namespace ,name))
(define-macro (signal-cdata data)
  `(make-xml-literal (clean-cr ,data)))

;; signal-stago and signal-etag are only internally used.

;; Coding remark: The first is the version, which was developed at
;; first.  The second is something to show how to change those
;; signal-* functions.  We save a vector allocation here.

;;(define-inline (signal-stago namespace name)
;;  (vector 'STAGO namespace name))
(define-macro (signal-stago namespace name)
  `(values 'STAGO ,namespace ,name))

(define-macro (signal-etag namespace name) ; [42]
  `(values 'etag ,namespace ,name))

;}}}
;{{{ Entity handling

;; ** XML specifics

;; There are some entity definitions by default.  Those should be
;; least specific.  We create an entity-resolver, which attempts a
;; lookup in the context.  If that fails the default resolve mechanism
;; is used.

;; Be careful: if the signal-whatever one day starts to send signals
;; to an application, we have to generate them dynamically.

(%early-once-only

(define xml-default-entities
  `(("lt" . ,(signal-cdata "<"))
    ("gt" . ,(signal-cdata ">"))
    ("amp" . ,(signal-cdata "&"))
    ("quot" . ,(signal-cdata "\""))
    ("apos" . ,(signal-cdata "'"))
    ;; The nbsp is not incorrect but required to handle xhtml gracefully
    ("nbsp" . ,(signal-cdata "\xc2\xa0"))
    ))

(define html-default-entities
  (append
   xml-default-entities
;;    `(("nbsp" . ,(signal-cdata (string #xc2 #xa0)))
;;      ("auml" . ,(signal-cdata (string #xc3 #xa4)))
;;      ("ouml" . ,(signal-cdata (string #xc3 #xb6)))
;;      ("uuml" . ,(signal-cdata (string #xc3 #xbc)))
;;      ("Auml" . ,(signal-cdata (string #xc3 #x84)))
;;      ("Ouml" . ,(signal-cdata (string #xc3 #x96)))
;;      ("Uuml" . ,(signal-cdata (string #xc3 #x9c)))
;;      ("szlig" . ,(signal-cdata (string #xc3 #x9f)))
;;      ("copy" . ,(signal-cdata (string #xc2 #xa9)))
;;      )
   `(("nbsp" . ,(signal-cdata "\xc2\xa0"))
     ("auml" . ,(signal-cdata "\xc3\xa4"))
     ("ouml" . ,(signal-cdata "\xc3\xb6"))
     ("uuml" . ,(signal-cdata "\xc3\xbc"))
     ("Auml" . ,(signal-cdata "\xc3\x84"))
     ("Ouml" . ,(signal-cdata "\xc3\x96"))
     ("Uuml" . ,(signal-cdata "\xc3\x9c"))
     ("szlig" . ,(signal-cdata "\xc3\x9f"))
     ("copy" . ,(signal-cdata "\xc2\xa9"))
     )
   ))
)

(define (xml-add-default-entity-resolver default-entities resolver)
  (lambda (name)
    (let ((ctxdft (resolver name)))
      (if
       ctxdft
       ctxdft
       (let ((dflt (assoc name default-entities)))
	 (if dflt
	     (cdr dflt)
;; FIXME: The "not found" case is actually an error.  But it's
;; generally undecided yet, how to cope with errors.
;;
;; What do we do in the case of unresolved entities?  Sure, we signal
;; an error, but we want to continue somewhat.  Here we deliver the
;; source text.  This seems to be common sense these days.
	     (signal-cdata (string-append "&" name ";"))))))))

;}}}
;{{{ Regular Parsing  (compare with xml grammar)

(define-macro (signal-source-error msg source)
  `(signal-error
    (string-append
     ,msg
     " line: "
     (let loop ((line 1) (offset 0))
       (cond
        ((eqv? (vector-ref ,source 2) offset) (number->string line))
        ((eqv? (string-ref (vector-ref ,source 0) offset) #\newline)
         (loop (add1 line) (add1 offset)))
        (else (loop line (add1 offset)))))
     " \""
     (substring (vector-ref ,source 0) (vector-ref ,source 2)
                (if (> (+ (vector-ref ,source 2) 20) (vector-ref ,source 1))
                    (vector-ref ,source 1) (+ (vector-ref ,source 2) 20)))

     "\"... in \n"
     (vector-ref ,source 0))))

;; Here the original bigloo grammar.  FIXME xml-eref-grammar MUST comply
;;  (regular-grammar
;;   ((namestart	(in ("AZaz") (#"\200\377") #\_)) ; [5] Name
;;    (namechar	(in #\- #\. #\_ ("AZaz09") (#"\200\377")))
;;    (name (: namestart (* namechar))))
;;   ((: (submatch name) #\;) (the-submatch 1))
;;   ((: (submatch name) (out (or alnum #\_)))
;;    (the-submatch 1))			; complain about missing ";"?
;;   (else "&"))		        ; should we deliver an error here?

;{{{ xml-content-grammar

(define match-ws (reg-expr->proc `(prefix (+ ,(ws)))))
(define namestart '(or alpha #\_))      ; [5] Name
(define namechar '(or alpha digit #\- #\. #\_)) ; FIXME
(define name-g `(seq ,namestart (* ,namechar)))
(define name-m (reg-expr->proc `(prefix (save ,name-g))))
(define name-:m (reg-expr->proc `(prefix (seq #\: (save ,name-g)))))
(define cdata (reg-expr->proc '(prefix (+ (not (or #\& #\<))))))
(define pcdata (reg-expr->proc '(prefix (save (+ (not (or #\< #\&)))))))
(define stago                           ; [40]
  (reg-expr->proc
   `(prefix (seq #\< (? (seq (save ,name-g) #\:)) (save ,name-g)))))
(define stago0 (reg-expr->proc `(prefix (seq #\< (save ,name-g)))))
(define etag                            ; [42]
  (reg-expr->proc
   `(prefix (seq #\< #\/
                 (? (seq (save ,name-g) #\:))
                 (save ,name-g)
                 (* ,(ws)) #\>))))
(define etag0                            ; [42] part up to colon
  (reg-expr->proc `(prefix (seq #\< #\/ (save ,name-g)))))
(define etag1                            ; [42] colon and closing part
  (reg-expr->proc `(prefix (seq #\: (save ,name-g) (* ,(ws)) #\>))))
(define etag2                            ; [42] closing part without colon
  (reg-expr->proc `(prefix (seq (* ,(ws)) #\>))))
(define eref                            ; [68]
  (reg-expr->proc
   '(prefix (seq #\&
                 (save (seq alpha (* (or alpha digit #\- #\. #\_))))
                 #\;))))
(define tagc (reg-expr->proc `(prefix #\>)))
(define eref-num (reg-expr->proc '(prefix (seq #\& #\# (save (+ digit)) #\;))))
(define eref-numx
  (reg-expr->proc '(prefix (seq #\& #\# #\x (save (+ hex-digit)) #\;))))
(define pio
  (reg-expr->proc `(prefix (seq "<?" (save (+ (not (or #\? . ,(cdr (ws))))))
                                (? ,(ws))
                                (save (* (or (not #\?)
                                             (seq #\? (not #\>)))))
                                "?>"))))
(define commento
  (reg-expr->proc
   '(prefix (seq"<!--"
                (save (* (or (not #\-) (seq #\- (not #\-) (not #\>)))))
                "-->"))))

;; TODO a strict parser should enforce the rule that ther must be no
;; "--" sequence in xml comments.  While logical and consitent for
;; SGML this is an anachronism for xml.  Most tools don't understand
;; the issue anyway and will happily create broken xml, which is
;; accepted here.  Hence we need both versions somehow - this is left
;; to be done.

(define commento
  (reg-expr->proc
   '(prefix (seq"<!--"
                (save (* (or (not #\-)
                             (seq #\- (not #\-))
                             (seq #\- #\- (not #\>)))))
                "-->"))))
(define cdsection
;  (reg-expr->proc '(prefix (seq "<![CDATA["
;                                (save (* (or (not #\])
;                                             (seq #\] (not #\]) (not #\>)))))
;                                "]]>"))))
  (let* ((CDATA "<![CDATA[")
	 (CDL (string-length CDATA))
	 (CDE "]]>"))
    (lambda (str offset)
      (if (eqv? CDL (string-prefix-length CDATA str 0 CDL offset (string-length str)))
	  (let ((end (string-search str CDE offset)))
	    (if end 
		(values offset (+ end 3) (substring str (+ offset CDL) end))
		(values)))
	  (values)))))

(define invalid-cdata (reg-expr->proc '(prefix (save (or #\& #\<)))))

(define (xml-content-shift-nostrip source resolve-entity string->symbol)
  (regular-matches
   source
   ((1 pcdata (signal-cdata submatch))
    (2 stago (signal-stago (if submatch-1 (string->symbol submatch-1) #f)
                           (string->symbol submatch-2)))
    (1 stago0 (let ((tmp submatch))
                (regular-matches
                 source
                 ((1 name-:m (signal-stago (string->symbol tmp)
                                           (string->symbol submatch))))
                 (signal-stago #f (string->symbol tmp)))))
;    (2 etag (signal-etag (if submatch-1 (string->symbol submatch-1) #f)
;                         (string->symbol submatch-2)))
    (1 etag0 (let ((tmp submatch))
                (regular-matches
                 source
                 ((1 etag1 (signal-etag (string->symbol tmp)
                                        (string->symbol submatch)))
                  (0 etag2 (signal-etag #f (string->symbol tmp))))
                 (signal-source-error "illegal invalid end tag" source))))
    (1 eref-num (signal-cdata
                 (integer->utf8string (string->number submatch))))
    (1 eref-numx (signal-cdata
                  (integer->utf8string (string->number submatch 16))))
    (1 eref (resolve-entity submatch))
    (2 pio (signal-processing-instruction submatch-1 submatch-2))
    (1 commento (signal-comment submatch))
    (1 cdsection (signal-cdata submatch)) ; [19] open
    ;; recover from invalid situation.  must be last rule
    (1 invalid-cdata (signal-cdata submatch)))
   (if (eqv? (vector-ref source 1) (vector-ref source 2))
       (unfold-terminate)
       (values #t (signal-source-error "illegal XML data" source) #f))))

;; TODO find a better way to express the next three functions.  The
;; match is applied two times to the string.  But the second version
;; is only good for stirng end.

(define stripable-ws                    ; xslt[3.4]
  ;; be careful, can match one char too much
  (reg-expr->proc `(prefix (seq (+ ,(ws)) #\<))))

(define entire-stripable-ws             ; xslt[3.4]
  ;; be careful, can match one char too much
  (reg-expr->proc `(entire (seq (+ ,(ws))))))

(define (xml-content-shift source preserve-whitespace
                           resolve-entity string->symbol)
  (if preserve-whitespace
      (xml-content-shift-nostrip source resolve-entity string->symbol)
      (bind ((start end (stripable-ws (vector-ref source 0)
                                      (vector-ref source 2))))
            (if start
                (begin
                  (vector-set! source 2 (sub1 end))
                  (xml-content-shift-nostrip
                   source resolve-entity string->symbol))
                ;; has been just... but than we don't ignore ws at end.
                ;; (xml-content-shift-nostrip source resolve-entity string->symbol)
                (if (entire-stripable-ws (vector-ref source 0)
                                         (vector-ref source 2))
                    (begin
                      (vector-set! source 2 (vector-ref source 1))
                      (unfold-terminate))
                    (xml-content-shift-nostrip
                     source resolve-entity string->symbol))))))

;}}}

(define (xml-normalize-string resolve-entity unparsed-data)
  (let ((source (make-source unparsed-data)))
    (define (listup add)
      (define (loop unused)
        (if (eqv? (vector-ref source 1) (vector-ref source 2))
            #t
            (loop
             (add (regular-matches
                   source
                   ((1 pcdata submatch)
                    (1 eref-num
                       (integer->utf8string (string->number submatch)))
                    (1 eref-numx
                       (integer->utf8string (string->number submatch 16)))
                    (1 eref (data (resolve-entity submatch)))) ; [19] open
                   (error "illegal attribute value \"~a\""
                          unparsed-data))))))
      (loop #t))
    (to-string* (call-with-list-extending1 listup))))

;;(define xml-character-range		; actually unused, but good reference
;;  '(#u0009 #u000A #u000D
;;	  (#u0020 #ufffd)
;;	  ; (#u0020 #uD7FF)  FIXME: bigloo complains about undefined
;;	  ; (#uE000 #uFFFD)         UCS-2 characters here!
;;	  ; (#u10000 #u10FFFF)
;;	   ))

(define xml-reserved?
  (reg-expr->proc `(prefix (seq (or #\x #\X) (or #\m #\M) (or #\l #\L)))))

;{{{ xml-markup-grammar

;; TODO make this a validating parser and provide a way to select
;; between speedy none validating version and validating parser.

(define str-sp                          ; see: [10] AttValue
  (reg-expr->proc
   `(prefix (seq #\' (save (* (not (or #\' #\& #\<) ))) #\'))))
(define str-dp
  (reg-expr->proc
   `(prefix (seq #\" (save (* (not (or #\" #\& #\<) ))) #\"))))

(define str-sf
  (reg-expr->proc
   `(prefix (seq #\' (save (* (not #\'))) #\'))))
(define str-df
  (reg-expr->proc
   `(prefix (seq #\" (save (* (not #\"))) #\"))))

(define empty-tagc                      ; [44] EmptyElemTag
  (reg-expr->proc `(prefix (seq #\/ #\>))))
(define name-ns
  (reg-expr->proc
   `(prefix (seq (? (seq (save ,name-g) #\:)) (save ,name-g)))))
(define match-= (reg-expr->proc `(prefix (seq (* ,(ws)) #\=))))

(define (xml-markup-shift source resolve-entity string->symbol)
  (regular-matches
   source
   ((1 match-ws (xml-markup-shift source resolve-entity string->symbol))
    (1 empty-tagc 'xml-clause-44)
    (1 tagc 'signal-close)
;    (2 name-ns (signal-name (if submatch-1 (string->symbol submatch-1) #f)
;                            (string->symbol submatch-2)))
    (1 name-m (let ((tmp submatch))
                (regular-matches
                 source
                 ((1 name-:m (signal-name (string->symbol tmp)
                                          (string->symbol submatch))))
                 (signal-name #f (string->symbol tmp)))))
    ;; [40]after #\< [41] before #\=
    (1 str-dp submatch)
    (1 str-sp submatch)
    (1 str-df (xml-normalize-string resolve-entity submatch))
    (1 str-sf (xml-normalize-string resolve-entity submatch)))
   (signal-source-error "illegal markup content" source)))

(define (xml-markup-shift-= source resolve-entity)
  (regular-matches
   source
   ((1 match-ws (xml-markup-shift-= source resolve-entity))
    (1 match-= 'signal-eq))
   #f))

;}}}
;{{{ Analyze content of start tags

;; See XML[A.2] about namespace partitions.
;; Returns #f for the "per-element-type partition" or the correct
;; symbol.

;; (define-macro (attribute-prefix->namespace-partition source prefix namespaces)
;;   `(and
;;     ,prefix
;;     (let ((nms (assq ,prefix ,namespaces)))
;;       (if (not nms)
;;           (signal-source-error
;;            (format #f "attribute namespace prefix \"~a\" not declared" ,prefix)
;;            ,source))
;;       nms)))

(define (xml-resolve-xmlns source namespaces attributes unresolved)
  (if (null? unresolved)
      attributes
      (cons
       (make-xml-attribute
        (vector-ref (car unresolved) 0)
        (let ((nms (find-namespace-by-prefix
		    (vector-ref (car unresolved) 1) namespaces)))
          (and nms (xml-namespace-uri nms)))
        (vector-ref (car unresolved) 2))
       (xml-resolve-xmlns source namespaces attributes (cdr unresolved)))))

;; collection-fold-left

(define (xml-parse-stag namespaces resolve-entity source)
  (let loop ((attributes '()) (unresolved '()))
    (bind ((signal nmspace name (xml-markup-shift
                                 source resolve-entity string->symbol)))
          (cond
           ((or (eq? signal 'xml-clause-44) (eq? signal 'signal-close))
            (let ((inner (xml-parse-register-xmlns attributes namespaces)))
              (values (xml-resolve-xmlns source inner attributes unresolved)
                      inner signal)))
           ((eq? signal 'NAME)
            (if (xml-markup-shift-= source resolve-entity)
                (let ((val (xml-markup-shift
                            source resolve-entity string->symbol)))
                  (if (string? val)
                      (if nmspace
                          (let ((nms (find-namespace-by-prefix
				      nmspace namespaces)))
                            (if nms
                                (loop (cons (make-xml-attribute
                                             name (xml-namespace-uri nms)
                                             (clean-cr val))
                                            attributes)
                                      unresolved)
                                (loop attributes
                                      `(#(,name ,nmspace ,(clean-cr val))
                                        . ,unresolved))))
                          (loop (cons (make-xml-attribute
                                       name #f (clean-cr val))
                                      attributes)
                                unresolved))
                      (signal-source-error "expected attrib value" source)))
                (signal-source-error "missing '=' in attrib assign" source)))
           (else (signal-source-error "unexpected token" source))))))

(define html-unquoted-literal
  (reg-expr->proc
   `(prefix (save (* (or alpha digit #\- #\+ #\. #\_))))))

(define (html-shift-attrib-value source resolve-entity)
  (regular-matches
   source
   ((1 match-ws (xml-markup-shift source
                                  resolve-entity string-downcase!->symbol))
    (1 str-dp submatch)
    (1 str-sp submatch)
    (1 str-df (xml-normalize-string resolve-entity submatch))
    (1 str-sf (xml-normalize-string resolve-entity submatch))
    (1 html-unquoted-literal submatch))
   (signal-source-error "illegal markup content" source)))

;; W3C defines html to be case insensitive and recomments lower case.
;; Consequentely we convert into lower case here.  We convert the
;; newly allocated string "name" lower case here:

(define (string-downcase!->symbol str)
  (string->symbol (string-downcase! str)))

(define (html-parse-stag namespaces resolve-entity source)
  (let loop ((attributes '()) (unresolved '()))
    (bind ((signal nmspace name
              (xml-markup-shift source resolve-entity
                                string-downcase!->symbol)))
          (cond
           ((or (eq? signal 'xml-clause-44) (eq? signal 'signal-close))
            (let ((inner (xml-parse-register-xmlns attributes namespaces)))
              (values (xml-resolve-xmlns source inner attributes unresolved)
                      inner signal)))
           ((eq? signal 'NAME)
            (if (xml-markup-shift-= source resolve-entity)
                (let ((val (html-shift-attrib-value source resolve-entity)))
                  (if (string? val)
                      (if nmspace
                          (let ((nms (find-namespace-by-prefix
				      nmspace namespaces)))
                            (if nms
                                (loop (cons (make-xml-attribute
                                             name (xml-namespace-uri nms)
                                             (clean-cr val))
                                            attributes)
                                      unresolved)
                                (loop attributes
                                      `(#(,name ,nmspace ,(clean-cr val))
                                        . ,unresolved))))
                          (loop (cons (make-xml-attribute
                                       name #f (clean-cr val))
                                      attributes)
                                unresolved))
                      (signal-source-error "expected attrib value" source)))
                ;; Attributes with defaulted value.
                (loop (cons (make-xml-attribute name #f (symbol->string name))
                            attributes)
                      unresolved)))
           (else (signal-source-error "unexpected token" source))))))

;}}}

;; This code should be rewritten to use SRFI-44 style enometation.

;{{{ Analyze elements

; very old code; just to keep the idea I had. /jfw
;(define (signal-empty-element namespace name attributes)
;  (signal-element
;   namespace name
;   (aggregate (apply make-property-set
;		     `(xmlns ,namespace) `(gi ,name) attributes)
;	      (delay ""))))		; DSSSL 10.2.3

; Return for unfold/free:
; a) a continuation next
; b) an "virtual" end tag before whatever next would suggest

(define (insert-signal-etag ancestor namespace name next)
  (lambda (ancestor namespaces preserve-space)
    (values next 'etag namespace name)))

;; Parse the content of a start tag.  Yields either a start tag and an
;; end tag signal (for empty elements) or just the start tag.

;; This probably only good for the next procedure.

;; FIXME per DSSSL clause 9.3.2 The name of a node
;; (Attributes here) is also the value of one of it's properties
;; - need to add that property, which one is it?

(define (xml-content-unfolder source resolve-entity)
  (define (loop ancestor namespaces preserve-space)
    (bind ((token namespace name
                  (xml-content-shift source preserve-space
                                     resolve-entity string->symbol)))
          (case token
            ((STAGO)
             (bind ((attributes inner-namespaces
                     end-token
                     ;; This used to be
                     ;; (xml-parse-stag namespace resolve-entity source)
                     ;; but I forgot that defaulted name space don't apply
                     ;; to attributes.
                     (xml-parse-stag namespaces resolve-entity source)))
                   (case end-token
                     ((xml-clause-44)
                      (values
                       (insert-signal-etag ancestor namespace name loop)
                       'STAGO namespace name
                       attributes inner-namespaces))
                     ((signal-close)
                      (values loop 'STAGO namespace name attributes
                              inner-namespaces))
                     (else (error "unclosed start tag ~a" name)))))
            ((#f) (unfold-terminate))
            (else (values loop token namespace name)))))
  loop)

;; The detection what's to be closed is out of my head heuristic.
;; This should be derived from a xml schema.

(define html-implicit-closing-block
  '(p pre ul ol dl address table h1 h2 h3 h4 h5 h6))

(define html-tr-ancestors '(table tbody thead tfoot))

(define (closes-implicit token ancestor)
  (cond
   ((memq token html-implicit-closing-block)
    (and (pair? ancestor)
         (memq (car ancestor) html-implicit-closing-block)
         (cdr ancestor)))
   ((eq? token 'li)
    (and (not (or (eq? (car ancestor) 'ul) (eq? (car ancestor) 'ol)))
         (memq 'li ancestor)))
   ((or (eq? token 'dd) (eq? token 'dt))
    (and (not (eq? (car ancestor) 'dl))
         (let loop ((anc ancestor))
           (cond
            ((null? anc) #f)
            ((or (eq? (car anc) 'dt) (eq? (car anc) 'dd)) anc)
            (else (loop (cdr anc)))))))
   ((or (eq? token 'td) (eq? token 'th))
    (let loop ((anc ancestor))
      (cond
       ((or (null? anc) (eq? (car anc) 'tr)) #f)
       ((or (eq? (car anc) 'td) (eq? (car anc) 'th)) anc)
       (else loop (cdr anc)))))
   ((eq? token 'tr)
    (let loop ((anc ancestor))
      (cond
       ((or (null? anc) (memq (car anc) html-tr-ancestors)) #f)
       ((eq? (car anc) 'tr) anc)
       (else loop (cdr anc)))))
   (else #f)))

(define (implicit-close-to next stop
                           token namespace name attributes namespaces)
  (define (loop ancestor namespaces preserve-space)
    (if (eq? ancestor stop)
        (values next token namespace name attributes namespaces)
        (values loop 'etag #f #f #f namespaces)))
  loop)

(define (html-content-unfolder source resolve-entity)
  (define (loop ancestor namespaces preserve-space)
    (bind ((token namespace name
                  (xml-content-shift source preserve-space
                                     resolve-entity string-downcase!->symbol)))
          (case token
            ((STAGO)
             (bind ((attributes inner-namespaces
                     end-token
                     ;; This used to be
                     ;; (xml-parse-stag namespace resolve-entity source)
                     ;; but I forgot that defaulted name space don't apply
                     ;; to attributes.
                     (html-parse-stag namespaces resolve-entity source)))
                   (cond
                    ((or (html-element-empty? name)
                         (eq? end-token 'xml-clause-44))
                     (values
                      (insert-signal-etag ancestor namespace name loop)
                      'STAGO namespace name
                      attributes inner-namespaces))
                    (else
                     (let ((unclosed (closes-implicit name ancestor)))
                       (if unclosed
                           (implicit-close-to
                            loop unclosed
                            token namespace name attributes namespaces)
                           (values loop 'STAGO namespace name attributes
                                   inner-namespaces)))))))
            ((etag)
             (cond
              ((and (pair? ancestor) (eq? name (car ancestor)))
               (values loop token namespace name))
              ((memq name ancestor)
               (implicit-close-to loop (memq name ancestor)
                                  token namespace name #f namespaces))
              (else (loop ancestor namespaces preserve-space))))
            ((#f) (unfold-terminate))
            (else (values loop token namespace name)))))
  loop)

;}}}

;; FIXME xml clause 22 only patially implemented.

(define doctypedecl
  (reg-expr->proc
   `(prefix (seq "<!DOCTYPE" (+ ,(ws)) (save ,name-g)
                 ;; xml clause 75 "ExternalID"
                 (? (seq (+ ,(ws))
                         (or
                          (seq "PUBLIC"
                               (+ ,(ws)) #\" (* (not #\")) #\"
                               (+ ,(ws)) #\" (* (not #\")) #\")
                          (seq "SYSTEM"
                               (+ ,(ws)) #\" (* (not #\")) #\"))))
                 (* ,(ws)) ">"))))

(define (xml-prolog-shift source)
  (regular-matches
   source
   ((1 match-ws (xml-prolog-shift source))
    (2 pio (signal-processing-instruction submatch-1 submatch-2))
    (1 doctypedecl (values 'DOCTYPE submatch))
    (1 commento (signal-comment submatch)))
   #f))

(define (xml-unfolder source resolve-entity)
  (define (loop ancestor namespaces preserve-space)
    (bind ((token data (xml-prolog-shift source)))
          (case token
            ((DOCTYPE)
             (values (xml-content-unfolder source resolve-entity)
                     (values (signal-doctype-declaration data))))
            ((#f) ((xml-content-unfolder source resolve-entity)
                   ancestor namespaces preserve-space))
            (else (values loop token)))))
  loop)

(define html-doctypedecl
  (reg-expr->proc
   `(prefix (seq (or "<!DOCTYPE" "<!doctype") (+ ,(ws)) (save ,name-g)
                 ;; xml clause 75 "ExternalID"
                 (? (seq (+ ,(ws))
                         (or
                          (seq (or "PUBLIC" "public")
                               (+ ,(ws)) #\" (* (not #\")) #\"
                               (? (seq (+ ,(ws)) #\" (* (not #\")) #\")))
                          (seq (or "SYSTEM" "system")
                               (+ ,(ws)) #\" (* (not #\")) #\"))))
                 (* ,(ws)) ">"))))

(define (html-prolog-shift source)
  (regular-matches
   source
   ((1 match-ws (xml-prolog-shift source))
    (2 pio (signal-processing-instruction submatch-1 submatch-2))
    (1 html-doctypedecl (values 'DOCTYPE submatch))
    (1 commento (signal-comment submatch)))
   #f))

(define (html-unfolder source resolve-entity)
  (define (loop ancestor namespaces preserve-space)
    (bind ((token data (html-prolog-shift source)))
          (case token
            ((DOCTYPE)
             (values (html-content-unfolder source resolve-entity)
                     (values (signal-doctype-declaration data))))
            ((#f) ((html-content-unfolder source resolve-entity)
                   ancestor namespaces preserve-space))
            (else (values loop token)))))
  loop)

;}}}

(define preserve-str "preserve")

(define (xml-space attributes default)
  (let ((att (let loop ((atts attributes))
               (and (pair? atts)
                    (or (and (eq? (xml-attribute-name (car atts)) 'space)
                             (eq? (xml-attribute-ns (car atts)) namespace-xml)
                             (car atts))
                        (loop (cdr atts)))))))
    (if att (string=? (xml-attribute-value att) preserve-str) default)))

;; Code style remark: Here we find the advantage of all that "von
;; hinten durch die Brust in's Auge" work: we do virtually nothing to
;; change from applicative order to normal order (delayed) evaluation.
;; There is a unfold procedure for each, which takes care of that.

(define (no-seed a n p) #f)

(define (unfold/tree gio nso seed ancestor name-spaces preserve-space source)
  (bind ((seed h s n a inner-name-spaces
               (seed ancestor name-spaces preserve-space)))
        (cond
         ((not seed) (values (empty-node-list) #f))
         ((eq? h 'etag)
          (let ((nms (find-namespace-by-prefix s name-spaces)))
            (values
             (if (or (not n)            ; implicit, unnamed end tag.
                     (and (eq? n gio) (eq? (and nms (xml-namespace-uri nms)) nso)))
                 (empty-node-list)
                 (signal-source-error
                  (format #f "tag ~a:~a closed open is ~a:~a." s n nso gio)
                  source))
             seed)))
         ((eq? h 'STAGO)
          (let ((nms (find-namespace-by-prefix s inner-name-spaces)))
            (if (not nms)
                (signal-source-error
                 (format #f "namespace prefix \"~a\" not declared" s) source))
            (bind ((this next (unfold/tree
                               n (xml-namespace-uri nms) seed
                               (cons n ancestor) inner-name-spaces
                               (xml-space a preserve-space) source))
                   (rest left (unfold/tree
                               gio nso
                               (or next
                                   (if (null? ancestor)
                                       no-seed
                                       (signal-source-error
                                        "missing end" source)))
                               ancestor name-spaces preserve-space source)))
                  (values (cons (make-specialised-xml-element
                                 n (xml-namespace-uri nms) a this) rest)
                          left))))
         (else (bind ((this next (unfold/tree gio nso seed
                                              ancestor name-spaces
                                              preserve-space source)))
                     (values (cons h this) next))))))

(define (unfold/tree-old gio nso seed ancestor name-spaces preserve-space source)
  (bind ((seed h s n a (seed ancestor name-spaces preserve-space)))
        (cond
         ((not seed) (values (empty-node-list) #f))
         ((eq? h 'etag)
          (let ((nms (find-namespace-by-prefix s name-spaces)))
            (values
             (if (or (not n)            ; implicit, unnamed end tag.
                     (and (eq? n gio) (eq? (and nms (xml-namespace-uri nms)) nso)))
                 (empty-node-list)
                 (signal-source-error
                  (format #f "tag ~a:~a closed open is ~a:~a." s n nso gio)
                  source))
             seed)))
         ((eq? h 'STAGO)
          (let* ((inner-name-spaces (xml-parse-register-xmlns a name-spaces))
                 (nms (find-namespace-by-prefix s inner-name-spaces)))
            (if (not nms)
                (signal-source-error
                 (format #f "namespace prefix \"~a\" not declared" s) source))
            (bind ((this next (unfold/tree
                               n (xml-namespace-uri nms) seed
                               (cons n ancestor) inner-name-spaces
                               (xml-space a preserve-space) source))
                   (rest left (unfold/tree
                               gio nso
                               (or next
                                   (if (null? ancestor)
                                       no-seed
                                       (signal-source-error
                                        "missing end" source)))
                               ancestor name-spaces preserve-space source)))
                  (values (cons (make-xml-element n (xml-namespace-uri nms) a this) rest)
                          left))))
         (else (bind ((this next (unfold/tree gio nso seed
                                              ancestor name-spaces
                                              preserve-space source)))
                     (values (cons h this) next))))))

(define impossible-name '(none))
(define (unresolved-entity name) #f)

(define (xml-tree str)
  (let ((source (make-source str)))
    (unfold/tree
     impossible-name impossible-name
     (xml-unfolder
      source
      (xml-add-default-entity-resolver xml-default-entities unresolved-entity))
     '()                                ; ancestor
     empty-namespace-declaration #f source)))

(define (xml-tree-preserve-space str)
  (let ((source (make-source str)))
    (unfold/tree
     impossible-name impossible-name
     (xml-unfolder
      source
      (xml-add-default-entity-resolver xml-default-entities unresolved-entity))
     '()                                ; ancestor
     empty-namespace-declaration #t source)))

(define (html-tree str)
  (let ((source (make-source str)))
    (unfold/tree
     impossible-name impossible-name
     (html-unfolder
      source
      (xml-add-default-entity-resolver
       html-default-entities unresolved-entity))
     '()                                ; ancestor
     empty-namespace-declaration #t source)))

;; Parse a XML document from port into a token list.

;(define (xml-token-list str)
;  (unfold/free (xml-content-unfolder (make-source str))))

;; Parse a XML document from port into a token stream.

;(define (xml-token-stream str)
;  (stream-unfold/free (xml-content-unfolder (make-source str))))

(define (xml-parse-strict str) (xml-tree str))

(define (xml-parse-permissive str)
  (guard (c (else (htmlprag str))) (xml-tree str)))

(define (html-parse-permissive str)
  (guard (c (else (htmlprag str))) (html-tree str)))

(define (xml-parse str . rest)
  (let ((mode (and-let* ((mode-p (memq mode: rest))
                         ((pair? (cdr mode-p))))
                        (cadr mode-p))))
    ((case mode
       ((#f strict) xml-tree)
       ((strict-preserve) xml-tree-preserve-space)
       ((permissive) xml-parse-permissive)
       ((html) html-parse-permissive)
       (else (error (string-append
                     "Unknown parse mode " (symbol->string mode)))))
     str)))
