;; (C) 2000, 2001, 2002, 2004, 2013 Jörg F. Wittenberger see http://www.askemos.org

(: xml-format-run-i
   (:render-display:
    (or boolean symbol)			             ; exc-c14n
    :render-namespace-bindings:			     ; seen-nms
    :render-namespace-bindings:			     ; rev-nms
    (or fixnum boolean)				     ; indent
    :xml-nodelist:				     ; tree
    (list-of (pair :sxpath-pred: :render-renderer:)) ; walk-rules
    -> null))
(define (xml-format-run-i display exc-c14n seen-nms rev-nms indent tree walk-rules)
  (do ((tree tree (node-list-rest tree)))
      ((node-list-empty? tree) (empty-node-list))
    (let ((node (node-list-first tree)))
      (let next ((rules walk-rules))
        (if (pair? rules)
            (if ((caar rules) node 'no-root empty-namespace-rev-declaration)
                ((cdar rules)
                 display exc-c14n seen-nms rev-nms indent node
                 walk-rules)
                (next (cdr rules))))))))

(define-syntax define-renderer
  (syntax-rules ()
    ((_ name body)
     (begin
       (: name :render-renderer:)
       (define (name
		place-out			; display routine
		message			; c14n flag (symbol?)
		namespaces		; seen-nms
		ancestor			; rev-nms
		self-node			; indent-prefix (fixnum)
		nl			; current node
		mode-choice		; walk-rules
		)
	 (let-syntax
	     ((rewrite
	       (syntax-rules ()
		 ((_ (*deprecated-place
		      *place
		      *c14n
		      *rendered-namespaces
		      *declared-namespaces
		      *indent-prefix
		      *current-node
		      *render-children
		      )
		     body_)
		  (let-syntax
		      (
		       (*deprecated-place
			(syntax-rules () ((_) place-out)))
		       (*place
			(syntax-rules () ((_ value transform) (place-out value transform))))
		       (*render-children
			(syntax-rules ()
			  ((_ c14n_ ns-rendered_ ns-bindings_ next-indent_ nl_childs_)
			   (xml-format-run-i place-out c14n_ ns-rendered_ ns-bindings_
					     next-indent_ nl_childs_ mode-choice))))
		       (*c14n (syntax-rules () ((_) message)))
		       (*rendered-namespaces (syntax-rules () ((_) namespaces)))
		       (*declared-namespaces (syntax-rules () ((_) ancestor)))
		       (*indent-prefix (syntax-rules () ((_) self-node)))
		       (*current-node (syntax-rules () ((_) nl)))
		       )
		    body_)))))
	   (extract
	    (%place
	     place
	     c14n
	     rendered-namespaces
	     declared-namespaces
	     indent-prefix
	     current-node
	     render-children
	     )
	    body
	    (rewrite () body))))))))

(: encoding-for-xml (:xml-nodelist: --> string))
(define (encoding-for-xml nl)
  (let ((xmlpi (node-list-first nl)))
    (if (eq? (xml-pi-tag xmlpi) 'xml)
      (or (xml-pi-encoding (xml-pi-data xmlpi)) "UTF-8")
      "UTF-8")))

(: content-type-for-html* (:xml-nodelist: --> string))
(define (content-type-for-html* nl)
  (let ((ct (node-list-first
             (node-list-filter
              (lambda (node)
                (let ((att (or (attribute-string 'http-equiv node)
                               (attribute-string 'HTTP-EQUIV node))))
                  (and att (or (equal? att "Content-Type")
                               (equal? (string-downcase att)
                                       "content-type")))))
              (children
               (let ((head (select-elements (children nl) 'head)))
                 (if (node-list-empty? head)
                     (select-elements (children nl) 'HEAD)
                     head)))))))
    (or (and (not (node-list-empty? ct))
             (or (attribute-string 'content ct)
                 (attribute-string 'CONTENT ct)))
        (if (eq? (ns (document-element nl)) namespace-xhtml)
            "text/html; charset=UTF-8"
            "text/html; charset=ISO-8859-1"))))

(: content-type-for-html* ((or false :xml-nodelist:) --> string))
(define (content-type-for-html nl)
  (if nl (content-type-for-html* nl) "text/html; charset=ISO-8859-1"))

(: content-type-for-html ((or false :xml-nodelist:) --> string))
(define (encoding-for-html nl)
  (or (content-type-charset (content-type-for-html nl))
      (if (eq? (ns (document-element nl)) namespace-xhtml)
          "UTF-8"
          "ISO-8859-1")))

;; FIXME make sure follow http://www.w3.org/TR/xml-c14n completely and
;; also have something fast and easy to generate some html for the
;; browser.

;;** tables, which could be a porting problem

;; FIXME the values here derive partially from supporting nsgml as
;; front end parser.  TODO before: come up with a compatible data
;; definition, which should be compatible to the possible extend.

(define xml-unprefixed-attribute-spaces-list '(#f id cdata token))

(%early-once-only
(define xml-unprefixed-attribute-spaces (make-symbol-table))
(for-each (lambda (tag) (hash-table-set! xml-unprefixed-attribute-spaces tag #t))
          (cdr xml-unprefixed-attribute-spaces-list))
)

(define (prefixed-att-space? tag)
  (and tag (not (hash-table-ref/default xml-unprefixed-attribute-spaces tag #f))))


(define html-empty-elements-list
  '(area base basefont br col frame hr img input isindex link meta param
    AREA BASE BASEFONT BR COL FRAME HR IMG INPUT ISINDEX LINK META PARAM))

(%early-once-only
(define html-empty-elements (make-symbol-table))
(for-each (lambda (tag) (hash-table-set! html-empty-elements tag #t))
          html-empty-elements-list)
)

(define (html-element-empty? tag) (hash-table-ref/default html-empty-elements tag #f))

;;** Rendering

(%early-once-only
(define xml-attribute-encoding (make-vector 256 "[unknown]"))
(define xml-content-encoding (make-vector 256 "[unknown]"))
(define html-attribute-encoding (make-vector 256 "[unknown]"))
(define html-content-encoding (make-vector 256 "[unknown]"))

(do ((i 0 (add1 i)))
    ((eqv? i 256) #t)
  (let ((v (string (integer->char i))))
    (vector-set! xml-attribute-encoding i v)
    (vector-set! xml-content-encoding i v)
    (vector-set! html-attribute-encoding i v)
    (vector-set! html-content-encoding i v)))

;(do ((i 0 (add1 i)))
;    ((eqv? i 32) #t)
;  (if (not (memv i '(9 10)))
;      (let ((v (format #f "&#x~02x;" i)))
;        (vector-set! xml-attribute-encoding i v)
;        (vector-set! xml-content-encoding i v)
;        (vector-set! html-encoding i v))))

(vector-set! xml-attribute-encoding (char->integer #\") "&quot;")
(vector-set! xml-attribute-encoding (char->integer #\') "&apos;")
(vector-set! xml-attribute-encoding (char->integer #\&) "&amp;")
(vector-set! xml-attribute-encoding (char->integer #\<) "&lt;")
(vector-set! xml-attribute-encoding (char->integer #\>) "&gt;")
;; (vector-set! xml-attribute-encoding 10 "&#x0A;")
(vector-set! xml-attribute-encoding 13 "&#x0D;")

(vector-set! xml-content-encoding (char->integer #\&) "&amp;")
(vector-set! xml-content-encoding (char->integer #\<) "&lt;")
(vector-set! xml-content-encoding (char->integer #\>) "&gt;")
(vector-set! xml-content-encoding 13 "&#x0D;")

(vector-set! html-attribute-encoding (char->integer #\") "&quot;")
(vector-set! html-attribute-encoding (char->integer #\&) "&amp;")
(vector-set! html-content-encoding (char->integer #\&) "&amp;")
(vector-set! html-attribute-encoding (char->integer #\<) "&lt;")
(vector-set! html-content-encoding (char->integer #\<) "&lt;")
(vector-set! html-attribute-encoding (char->integer #\>) "&gt;")
;; (vector-set! html-content-encoding 160 "&nbsp;")
)

(: xml-quote-display (:render-display: string -> *))
(define (xml-quote-display display str)
;;  (if (not (string? str)) (set! str (format #f "~a" str)))
  (display str xml-attribute-encoding))

;; FIXME I'm so sorry that this definition is here at all.
(: xml-quote-display-at-port (* output-port -> *))
(define (xml-quote-display-at-port obj port)
  (let ((d (make-display port "UTF-8")))
    (xml-quote-display d (if (string? obj) obj (format "~a" obj)))
    (d make-display-final #f)))

(: xml-content-quote-display (:render-display: string -> *))
(define (xml-content-quote-display display str)
;  (if (not (string? str)) (set! str (format #f "~a" str)))
  (display str xml-content-encoding))

(define-renderer xml-format-literal
  (xml-content-quote-display (%place) (xml-literal-value (current-node))))

(: html-quote-content-display (:render-display: string -> *))
(define (html-quote-content-display display str)
;  (if (html-escape-needed? str)         ; still saves around 20%
;      (display str html-encoding)
;      (display str #f))
  (display str html-content-encoding))

(: html-quote-attribute-display (:render-display: string -> *))
(define (html-quote-attribute-display display str)
;  (if (html-escape-needed? str)         ; still saves around 20%
;      (display str html-encoding)
;      (display str #f))
  (display str html-attribute-encoding))

;; BEWARE html-quote-display proved to be the most expensive function.
;; With RScheme the escape-needed? check saves 50% of execution time
;; withe a real world example.  Good candidate for hand coded
;; optimizations.

; moved to util due to use of regex.
;(define html-escape-needed?
;  (reg-expr->proc '(or #\& #\< #\> #\")))

;(define (html-quote-display display str)
;  (if (html-escape-needed? str)         ; still saves around 20%
;      (display str html-encoding)
;      (display str #f)))

(define-renderer html-format-literal
  (html-quote-content-display (%place) (xml-literal-value (current-node))))

(define-renderer xml-format-output
  (place (vector-ref (current-node) 1) #f) ; FIXME breaking abstraction
  )

(define empty-ns-decl (make-xml-attribute 'xmlns #f ""))
(define empty-ns-reg (make-xml-namespace #f #f))

#|
(define (make-ns-declaration nmsp)
  (cond
   ((not nmsp) (values empty-ns-reg empty-ns-decl))
   ((eq? nmsp namespace-xhtml)
    ;; It's usually wrong to have a generated prefix here.  So if
    ;; nothing was explicitely given, we change the default
    ;; namespace.
    (values (make-xml-namespace #f nmsp)
	    (make-xml-attribute 'xmlns #f namespace-html-str)))
   (else
    (let ((prefix (string->symbol
		   (string-append "gen-"
				  (md5-digest (symbol->string nmsp))))))
      (values (make-xml-namespace prefix nmsp)
	      (make-xml-attribute prefix 'xmlns (symbol->string nmsp)))))))
|#

;; self-node is abused in xml-format-element to contain the depth to
;; which the element content should be indented.

(: make-indent (fixnum --> string))
(define (make-indent depth)
  (make-string depth #\space))

(%early-once-only
 (define indent-strings
   (apply vector "" (map make-indent '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15))))
)

;; indent-string and indent-depth ought to be inlined.  They used to
;; be low level macros, which is bad for portability.


(: indent-string (fixnum --> string))
(define (indent-string depth)
  (case depth
    ((#f) (vector-ref indent-strings 0))
    ((0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)
     (vector-ref indent-strings depth))
    (else (make-indent depth))))

(: indent-depth ((or false fixnum) :xml-attribute-list: :xml-nodelist:
		 --> (or false fixnum)))
(define (indent-depth current-depth atts nl)
  (and current-depth
       (not (xml-space atts #f))	; not xml:space="preserve"
       (not (node-list-empty? nl))
       (let loop ((rest nl))
	 (if (node-list-empty? rest)
	     (add1 current-depth)
	     (and  (not (xml-literal? (node-list-first rest)))
		   (loop (node-list-rest rest)))))))

(: xml-render-register-rev-xmlns
   (:xml-attribute-list:
    :render-namespace-bindings:
    --> :render-namespace-bindings:))
(define (xml-render-register-rev-xmlns attributes outer)
  (fold
   (lambda (att outer)
     (cond
      ((eq? (xml-attribute-ns att) 'xmlns)
       (if (eq? (xml-attribute-name att) 'xml)
	   ;; ignore overwrite attempts for xml prefix (see XPointer 5.2.1)
	   outer
	   (let* ((nmsp (string->symbol (xml-attribute-value att)))
		  (old (binding-set-ref/default outer nmsp #f)))
	     (if (eq? old (xml-attribute-name att)) outer
		 (binding-set-insert outer nmsp (xml-attribute-name att))))))
      ((eq? (xml-attribute-name att) 'xmlns)
       (let* ((nmsp (string->symbol (xml-attribute-value att)))
	      (old (binding-set-ref/default outer nmsp #f)))
	 (if (eq? old #f) outer
	     (binding-set-insert outer nmsp #f))))
      (else outer)))
   outer
   attributes))

;; canonical xml can be found at
;; http://www.w3.org/TR/2001/REC-xml-c14n-20010315

;; The spec says: XML canonicalization MUST report an operation
;; failure on documents containing relative namespace URIs. FIXME, we
;; ommit that check for now.

(: xml-c14n-att<? (:xml-attribute: :xml-attribute: --> boolean))
(define (xml-c14n-att<? a b)
  (cond
   ;; the default namespace node, if one exists, has no local name and
   ;; is therefore lexicographically least)
   ((eq? (xml-attribute-name a) 'xmlns) #t)
   ((eq? (xml-attribute-name b) 'xmlns) #f)
   ;; Namespace nodes have a lesser document order position than
   ;; attribute nodes.
   ((eq? (xml-attribute-ns a) 'xmlns)
    (if (eq? (xml-attribute-ns b) 'xmlns)
        ;; An element's namespace nodes are sorted lexicographically
        ;; by local name (the default ...).
        (string<? (symbol->string (xml-attribute-name a))
                  (symbol->string (xml-attribute-name b)))
        #t))
   ((eq? (xml-attribute-ns b) 'xmlns) #f)
   ;; Same namespace? compare by local name.
   ((eq? (xml-attribute-ns a) (xml-attribute-ns b))
    (string<? (symbol->string (xml-attribute-name a))
              (symbol->string (xml-attribute-name b))))
   ((not (xml-attribute-ns a)) #t)
   ((not (xml-attribute-ns b)) #f)
   (else (string<? (symbol->string (xml-attribute-ns a))
                   (symbol->string (xml-attribute-ns b))))))


(: make-xml-attribute-formatter
   (:render-display: :render-namespace-bindings: -> *))
(define (make-xml-attribute-formatter display namespaces)
  (lambda (i att)
    (if (not (eq? (xml-attribute-ns att) 'implied))
        (begin
          (display " " #f)
          (if (prefixed-att-space? (xml-attribute-ns att))
              (begin
                (display
		 (binding-set-ref/default
		  namespaces (xml-attribute-ns att)
		  (xml-attribute-ns att))
		 #f)
                (display ":" #f)))
          (display (xml-attribute-name att) #f)
          (display "=\"" #f)
          (xml-quote-display display (xml-attribute-value att))
          (display "\"" #f)))))

;; The following is a (non-normative) method for implementing the
;; Exclusive XML Canonicalization method for many straightforward
;; cases -- it assumes a well-formed subset and that if an element is
;; in the node-set, so is all of its namespace axis; if the element is
;; not in the subset, neither is its namespace axis.
;;
;;   1. Recursively process the entire tree (from which the XPath
;;   node-set was selected) in document order starting with the
;;   root. (The operation of copying ancestor xml: namespace
;;   attributes into output apex element nodes is not done.)
;;   2. If the node is not in the XPath subset, continue to process
;;   its children element nodes recursively.
;;
;; ;; OK, we implemented the selection such, that the result tree
;; ;; does not contain anything not part of the selected node-set.
;;
;;   3. If the element node is in the XPath subset then output the
;;   node in accordance with Canonical XML except for namespace nodes

;;   which are rendered as follows:
;;         1. ns_rendered is a copy of a dictionary, off the top of
;;         the state stack, of prefixes and their values which have
;;         already been rendered by an output ancestor of the
;;         namespace node's parent element.
;;         2. Render each namespace node if and only if all of the
;;         conditions are met:
;;               1. it is visibly utilized by the immediate parent
;;               element or one of its attributes, or is present in
;;               InclusiveNamespacesPrefixList, and
;;               2. its prefix and value do not appear in ns_rendered.
;;         3. Render xmlns="" if and only if all of the conditions are
;;         met:
;;         4.
;;               1. The default namespace is visibly utilized by the
;;               immediate parent element node, or the default prefix
;;               token is present in InclusiveNamespaces PrefixList,
;;               and
;;               2. the element does not have a namespace node in the
;;               node-set declaring a value for the default namespace,
;;               and
;;               3. the default namespace prefix is present in the
;;               dictionary ns_rendered.
;;         5. Insert all the rendered namespace nodes (including
;;         xmlns="") into the ns_rendered dictionary, replacing any
;;         existing entries. Push ns_rendered onto the state stack and
;;         recurse.
;;         6. After the recursion returns, pop the state stack.

;; http://www.w3.org/TR/xquery/#id-ns-nodes-on-elements

(define (split-xmlns-exc-c14n-reduce
	 values
	 node
	 ns-rendered
	 new-bindings
	 exc-c14n inclusive-prefixes ns-decls atts)
  (let loop ((ns-rendered ns-rendered)
	     (new-decls (if (pair? inclusive-prefixes)
			    (filter (lambda (d)
				      (memq (xml-attribute-name d) inclusive-prefixes))
				    ns-decls)
			    '()))
	     (utilised (fold
			(lambda (x i)
			  (if (memq (xml-attribute-ns x) i) i (cons (xml-attribute-ns x) i)))
			(list (ns node))
			atts)))
    (if (null? utilised)
	(values ns-rendered new-bindings new-decls atts)
	(let ((uri (car utilised)))
	  (if uri
	      (cond
	       ((binding-set-ref/default ns-rendered uri #f)
		(loop ns-rendered new-decls (cdr utilised)))
	       ((binding-set-ref/default new-bindings uri #f) =>
		(lambda (local)
		  (loop (binding-set-insert ns-rendered uri local)
			(cons
			 (if uri
			     (if local
				 (make-xml-attribute local 'xmlns (symbol->string uri))
				 (make-xml-attribute 'xmlns #f (symbol->string uri)))
			     empty-ns-decl)
			 new-decls)
			(cdr utilised))))
	       (else
		(let* ((uri-str (symbol->string uri))
		       (local-sym (string->symbol
				   (string-append
				    "gen-" (md5-digest uri-str)))))
		  (loop
		   (binding-set-insert ns-rendered uri local-sym)
		   (cons (make-xml-attribute local-sym 'xmlns uri-str)
			 new-decls)
		   (cdr utilised)))))
	      (loop ns-rendered new-decls (cdr utilised)))))))

(define (split-xmlns values node ns-rendered ns-bindings exc-c14n inclusive-prefixes)
  (define (split-xmlns/lower-half ns-decls atts)
    (let ((new-bindings (xml-render-register-rev-xmlns ns-decls ns-bindings)))
      (if (eq? exc-c14n 'exc-c14n)
	  (split-xmlns-exc-c14n-reduce
	   values
	   node
	   ns-rendered
	   new-bindings
	   exc-c14n inclusive-prefixes ns-decls atts)
	  (values new-bindings new-bindings ns-decls atts))))
  (partition/cps
   split-xmlns/lower-half
   (lambda (n)
     (or (eq? (xml-attribute-name n) 'xmlns)
	 (eq? (xml-attribute-ns n) 'xmlns)))
   (attributes node)))

(: make-xml-exc-c14n-attribute-formatter
   (:render-display: :render-namespace-bindings: -> *))
(define (make-xml-exc-c14n-attribute-formatter display namespaces)
  (lambda (att)
    (display " " #f)
    (let ((registration (binding-set-ref/default namespaces (xml-attribute-ns att) '())))
      (if (null? registration)
	  (display (xml-attribute-ns att) #f)
	  (if registration
	      (begin
		(display registration #f)
		(display ":" #f)))))
    (display (xml-attribute-name att) #f)
    (display "=\"" #f)
    (xml-quote-display display (xml-attribute-value att))
    (display "\"" #f)))

(define (for-each-n/sorted pred proc lst)
  ;; vector-sort! is side effect free cause 'apply vector' allocates a
  ;; fresh vector.
  (cond
   ((null? lst) lst)
   ((pair? (cdr lst))
    (srfi:vector-for-each proc (vector-sort! (apply vector lst) pred)))
   (else (proc 0 (car lst)))))

(define-renderer xml-format-element
  (receive/values-syntax
   (ns-rendered ns-bindings ns-decls atts)
   ;; magic `begin` from receive/values-syntax
   (begin (split-xmlns (current-node) (declared-namespaces) (declared-namespaces) (c14n) '()))
   (let ((nms (and (xml-element-ns (current-node))
		   (binding-set-ref/default ns-bindings (xml-element-ns (current-node)) #f)))
	 (indent (indent-string (indent-prefix)))
	 (next-indent (indent-depth (indent-prefix)
				    (xml-element-attributes (current-node))
				    (children (current-node)))))
     (place indent #f)
     (place "<" #f)
     (if nms
	 (begin
	   (place nms #f)
	   (place ":" #f)))
     (place (gi (current-node)) #f)
     ;; FIXME The spec states that "superfluous namespace
     ;; declarations are removed from each element", but this
     ;; requires substantial testing, which is not desirable.  Need
     ;; to come up with a solution.  Probably one more formatter,
     ;; which adheres to the spec.
     (if (or (pair? ns-decls) (pair? atts))
	 (let ((format (make-xml-attribute-formatter (%place) ns-bindings)))
	   (for-each-n/sorted xml-c14n-att<? format ns-decls)
	   (for-each-n/sorted xml-c14n-att<? format atts)))
     (place (if next-indent ">\n" ">") #f)
     (render-children (case (c14n)
			((exc-c14n exc-c14n-root) 'exc-c14n)
			(else (c14n)))
		      ns-rendered ns-bindings next-indent (children (current-node)))
     (if nms
	 (begin
	   (if next-indent (place indent #f))
	   (place "</" #f)
	   (place nms #f)
	   (place ":" #f)
	   (place (gi (current-node)) #f)
	   (place (if (indent-prefix) ">\n" ">") #f))
	 (begin
	   (if next-indent (place indent #f))
	   (place "</" #f)
	   (place (gi (current-node)) #f)
	   (place (if (indent-prefix) ">\n" ">") #f)))

     ;; An older version used just the empty tags notation.  Both should be
     ;; mix, according to the schema, which applies, but we don't yet
     ;; have support for those.

     ;;      (let ((childs (children (current-node))))
     ;;        (if (or (node-list-empty? childs)
     ;;                (and (xml-literal? (node-list-first childs))
     ;;                     (string=? "" (data (node-list-first childs)))
     ;;                     (node-list-empty? (node-list-rest childs))))
     ;;            (place "/>" #f)
     ;;            (begin (place ">" #f)
     ;;                   (render-children place (c14n) variables namespaces next-indent
     ;;                                    childs)
     ;;                   (if (and nms (cdr nms))
     ;;                       (begin
     ;;                         (if next-indent (place indent #f))
     ;;                         (place "</" #f)
     ;;                         (place (cdr nms) #f)
     ;;                         (place ":" #f)
     ;;                         (place (gi (current-node)) #f)
     ;;                         (place ">" #f))
     ;;                       (begin
     ;;                         (if next-indent (place indent #f))
     ;;                         (place "</" #f)
     ;;                         (place (gi (current-node)) #f)
     ;;                         (place ">" #f))))))
     )))

(define-renderer html-format-element
  (let ((is-empty (html-element-empty? (gi (current-node)))))
    (place "<" #f)
    (place (gi (current-node)) #f)
    (for-each
     (lambda (att)
       (if (and (not (string-null? (xml-attribute-value att)))
                (not (eq? (xml-attribute-ns att) 'implied)))
           (begin
             (place " " #f)
             (place (xml-attribute-name att) #f)
             (place "=\"" #f)
             (html-quote-attribute-display (%place) (xml-attribute-value att))
             (place "\"" #f))))
     (xml-element-attributes (current-node)))
    (place ">" #f)                      ; could contain \n for shorter lines
    (if (not (node-list-empty? (children (current-node))))
        (render-children (c14n) (rendered-namespaces) (declared-namespaces)
			 (indent-prefix) (children (current-node))))
    (if (not is-empty)
        (begin
          (place "</" #f)
          (place (gi (current-node)) #f)
          (place "\n>" #f)))
    ))


;; FIXME format-pi is wrong: Also, a trailing #xA is rendered after
;; the closing PI symbol for PI children of the root node with a
;; lesser document order than the document element, and a leading #xA
;; is rendered before the opening PI symbol of PI children of the root
;; node with a greater document order than the document element.

(define-renderer xml-format-pi
  (begin
    (place "<?" #f)
    (place (vector-ref (current-node) 1) #f)
    (place " " #f)
    (place (vector-ref (current-node) 2) #f)
    (place (if (indent-prefix) "?>\n" "?>") #f)))
(define-renderer xml-format-comment
  (begin
    (place "<!--" #f)
    (place (vector-ref (current-node) 1) #f)
    (place (if (indent-prefix) "-->\n" "-->") #f)))

(define xml-format-walk-rules
  (list
   (cons sxp:literal? xml-format-literal)
   (cons sxp:element? xml-format-element)
   ;; Disabled, maybe we don't need it.  If we do, just uncomment.
   ;; (cons xml-output? xml-format-output)
   (cons sxp:pi? xml-format-pi)
   (cons sxp:comment? xml-format-comment)))

(: html-format-walk-rules (list-of (pair :sxpath-pred: :render-renderer:)))
(define html-format-walk-rules
  (list
   (cons sxp:literal? html-format-literal)
   (cons sxp:element? html-format-element)
   ;; Disabled, maybe we don't need it.  If we do, just uncomment.
   ;; (cons xml-output? xml-format-output)
   (cons sxp:pi? xml-format-pi)
   (cons sxp:comment? xml-format-comment)))

(define empty-namespace-rev-declaration     ; xml names 4.0
  (binding-set-insert (empty-binding-set) namespace-xml 'xml))

(define (xml-format-run display exc-c14n walk-rules namespaces tree)
  (xml-format-run-i display exc-c14n
                    namespaces
		    namespaces
                    (indent-depth -1 '() tree)
                    tree
                    walk-rules)
  (display make-display-final #f))

(define (xml-format-run-inline display exc-c14n walk-rules namespaces tree)
  (xml-format-run-i display exc-c14n
                    namespaces
		    namespaces
                    #f
                    tree
                    walk-rules)
  (display make-display-final #f))

(: xml-format-markdown (:xml-node: output-port -> :xml-node:))
(define (xml-format-markdown tree port)
  (xml-format-run-inline
   (make-display port "UTF-8")
   #f xml-format-walk-rules empty-namespace-rev-declaration
   tree))

(define (xml-format-fragment display exc-c14n namespaces tree)
  (xml-format-run display exc-c14n xml-format-walk-rules namespaces tree))

(define (html-format-fragment display tree)
  (xml-format-run display #f html-format-walk-rules empty-namespace-rev-declaration tree))

(define (xml-format-fragment-at-output-port tree port)
  (xml-format-fragment (make-display port (encoding-for-xml tree))
		       #f empty-namespace-rev-declaration tree)
  tree)

(define (xml-format-fragment-at-current-output-port tree)
  (xml-format-fragment
   (make-display (current-output-port) (encoding-for-xml tree))
  'exc-c14n-root
  empty-namespace-rev-declaration
  tree)
  tree)

(define (xml-format-document display exc-c14n enc tree)
  (if (and (not exc-c14n)
	   tree (not (node-list-empty? tree))
	   (not (or (xml-pi? (node-list-first tree))
		    (let loop ((t tree))
		      (if (node-list-empty? t)
			  #f
			  (and (not (xml-literal? (node-list-first t)))
			       (loop (node-list-rest t))))))))
      (begin
        (display "<?xml version=\"1.0\" encoding=\"" #f)
        (display enc #f)
        (display "\" ?>\n" #f)))
  (xml-format-fragment display exc-c14n empty-namespace-rev-declaration tree))

(define (xml-format-exc-c14n tree . namespaces)
  (xml-format-fragment
   (make-display 'string "UTF-8") 'exc-c14n
   (if (pair? namespaces) (car namespaces) empty-namespace-rev-declaration)
   tree))

(define (xml-format-exc-c14n-root tree . namespaces)
  (xml-format-fragment
   (make-display 'string "UTF-8") 'exc-c14n-root
   (if (pair? namespaces) (car namespaces) empty-namespace-rev-declaration)
   tree))

(define (html-format-fragment-at-current-output-port tree)
  (html-format-fragment
   (make-display (current-output-port) (encoding-for-html tree)) tree)
  tree)

(define (xml-format-at-output-port tree port)
  (let ((enc (encoding-for-xml tree)))
    (xml-format-document (make-display port enc) #f enc tree))
  tree)

(define (xml-format-at-current-output-port tree)
  (let ((enc (encoding-for-xml tree)))
    (xml-format-document (make-display (current-output-port) enc) #f enc tree))
  tree)

(define (html-format-at-output-port tree port)
  (let ((enc (encoding-for-html tree)))
    (html-format-document (make-display port enc) enc tree))
  tree)

(define (html-format-at-current-output-port tree)
  (html-format-at-output-port tree (current-output-port)))

(define (html-format-document display enc tree)
;;   (display "<?xml version=\"1.0\" encoding=\"" #f)
;;   (display enc #f)
;;   (display "\" ?>\n" #f)
  (cond
   ((eq? (ns tree) namespace-xhtml)
    (begin
      (display "<?xml version=\"1.0\" encoding=\"" #f)
      (display enc #f)
      (display "\" ?>\n" #f))
    (display "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"
\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n" #f)
    (xml-format-fragment display #f empty-namespace-rev-declaration tree))
   (else
    (display "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\"
\"http://www.w3.org/TR/html4/strict.dtd\">\n" #f)
    (html-format-fragment display tree))))

(: html-format (:xml-nodelist: --> string))
(define (html-format tree)               ; EXPORT
  (let* ((enc (encoding-for-html tree))
	 (display (make-display 'string enc)))
    (html-format-document display enc tree)))

(: xml-format (:xml-nodelist: --> string))
(define (xml-format tree)               ; EXPORT
  (let ((enc (encoding-for-xml tree)))
    (xml-format-document (make-display 'string enc) #f enc tree)))

(define (dsssl-xml-format tree . opt)
  (xml-format-fragment
   (make-display 'string "UTF-8")
   (and-let* ((x (memq mode: opt))) (cadr x))
   (or (and-let* ((x (memq namespaces: opt))
		  (f (cadr x)))
		 (cond
		  ((null? f) empty-namespace-rev-declaration)
		  ((eq? (car f) '*NAMESPACES*)
		   (fold
		    (lambda (sd init)
		      (binding-set-insert
		       init
		       (car sd)
		       (if (symbol? (cadr sd))
			   (cadr sd) (string->symbol (cadr sd)))))
		    empty-namespace-rev-declaration
		    (cdr f)))
		  (else f)))
       empty-namespace-rev-declaration)
   (node-list-filter
    (lambda (node)
      (not (eq? (xml-pi-tag node) 'xml)))
    tree)))

(: xml-format/list (:xml-nodelist: --> (pair number (list-of string))))
(define (xml-format/list tree)          ; EXPORT
  (let ((enc (encoding-for-xml tree)))
    (xml-format-document (make-display 'list enc) #f enc tree)))

(: html-format/list (:xml-nodelist: --> (pair number (list-of string))))
(define (html-format/list tree)         ; EXPORT
  (let ((enc (encoding-for-html tree)))
    (html-format-document (make-display 'list enc) enc (document-element tree))))

(: xml-digest-simple (:xml-nodelist: --> :hash:))
(define (xml-digest-simple tree)
  (sha256-digest
   (cond
    ((string? tree) tree)
    ((node-list? tree) (xml-format-exc-c14n-root tree))
    (else (error "not a node-list ~a" tree)))))
