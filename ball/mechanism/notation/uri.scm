;; URI Handling
;; move this code to notation/.. and include ther the uri-encode -decode routines
(define-record-type <uri>
  (%make-uri scheme authority path query fragment)
  uri?
  (scheme uri-scheme)
  (authority uri-authority)
  (path uri-path)
  (query uri-query)
  (fragment uri-fragment))

(define (absolute-uri? uri)
  (or (and (uri? uri) (uri-scheme uri) #t)
      (and (string? uri) ((pcre->proc URIschema) uri) #t)))

(define (absoluteURI? uri . of-schema)
  (if (null? of-schema)
      (absolute-uri? uri)
      (let ((s (cond ((uri? uri) (uri-scheme uri))
		     ((string? uri)
		      (let ((m ((pcre->proc URIschema) uri)))
			(and m (string->symbol (cadr m)))))
		     (else #f))))
	(and (member s (if (pair? (car of-schema)) (car of-schema) of-schema))
	     #t))))

;URI:
;[ scheme ":" ] "//" authority [ "/"  path_segments ] [ "?" query ] [ "#" fragment ]
;[ scheme ":" ]                  "/"  path_segments   [ "?" query ] [ "#" fragment ]
;  scheme ":"   uric_noslash
;               uric_no[/?#]                          [ "?" query ] [ "#" fragment ]
(define URIschema    "^([[:alpha:]][[:alpha:][:digit:]+-.]*):")
(define URIauthority "//([^/?#]*)")
(define URIabs_path  "(/[^?#]*)")
(define URIrel_path  "([^?#]*)")
(define URIquery     "[?]([^#]*)")
(define URIfragment  "#(.*)")
(define split-uri
  (let ((schema-match (pcre->proc URIschema))
	(opaque-match (pcre->aproc "([^/].*)"))
	(relpath-match (pcre->aproc URIrel_path))
	(auth-match (pcre->aproc URIauthority))
	(abs-match (pcre->aproc URIabs_path))
	(query-match (pcre->proc URIquery))
	(fragment-match (pcre->proc URIfragment)))
    (define (split-uri str)
      (let* ((s  (schema-match str))
	     (ns (and s (cdar s)))
	     (r (if s 
		    (opaque-match str ns) ;opaque
		    (relpath-match str)))  ;rel
	     (nr (and r (cdar r)))
	     (a (auth-match str (or ns 0)))
	     (na (and a (cdar a)))
	     (p (abs-match str (or na ns 0)))
	     (np (and p (cdar p)))
	     (q (query-match str (or nr 0)))
	     (nq (and q (cdar q)))
	     (f (fragment-match str (or nr 0))))
	(values
	 (and s (cadr s))                  ; schema
	 (and a (cadr a))                  ; authority
	 (or (and p (cadr p))              ;  abs_path
	     (and r s (cadr r))            ;  opaque
	     (and r (not a) (cadr r)))     ;  rel_path
	 (and q (cadr q))                  ; query
	 (and f (cadr f)))))               ; fragment
    split-uri))

;; need to add uri-decode
(define (uri->tree uri-str . base-tree)
  (receive
   (bscheme bauthority bpath bquery bfragment)
   (if (pair? base-tree) (list->values (car base-tree))
       (values #f #f #f #f #f))
   (receive 
    (scheme authority path query fragment)
    (split-uri uri-str)
    (let ((uri-empty? (and (or (not path) (string=? path ""))
			   (not (or scheme authority query)))))
      (list (or (and scheme (string->symbol scheme)) bscheme)
	    (or authority bauthority)
	    (if uri-empty? (or bpath "") path)
	    (or query (and uri-empty? bquery))
	    (or fragment (and uri-empty? bfragment)))))))

(define (uri str)
  (apply %make-uri (uri->tree str)))

(define (string->uri str)
  (receive 
   (scheme authority path query fragment)
   (split-uri str)
   (let ((uri-empty? (and (or (not path) (string=? path ""))
			  (not (or scheme authority query)))))
     (%make-uri
      (and scheme (string->symbol scheme))
      authority
      (or path "")
      query
      fragment))))

(define (make-uri scheme authority path query fragment)
  (%make-uri (if (string? scheme) (string->symbol scheme) scheme)
	     authority path query fragment))

;; need to add uri-encode

(define (display-uri uri port)
  (and-let* ((s (uri-scheme uri)))
	    (display s port)
	    (display ":" port))
  (and-let* ((a (uri-authority uri)))
	    (display "//" port)
	    (display a port))
  (and-let* ((p (uri-path uri)))
	    (display p port))
  (and-let* ((q (uri-query uri)))
	    (display "?" port)
	    (display q port))
  (and-let* ((f (uri-fragment uri)))
	    (display "#" port)
	    (display f port)))

(define (uri->string uri)
  (call-with-output-string (lambda (port) (display-uri uri port))))
