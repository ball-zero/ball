;;* MIME (see RFC 1341) handling

(define text/xml "text/xml")
(define application/xml "application/xml")
(define application/rdf+xml "application/rdf+xml")
(define application/soap+xml "application/soap+xml")
(define text/plain "text/plain")
(define text/html "text/html")
(define xml-parseable-types
  (list text/xml application/rdf+xml application/soap+xml application/xml
        "application/xhtml+xml" "application/xslt+xml" "text/mathml"
        "image/svg+xml" "application/rss+xml"))

(define soap+xml-content? (pcre/a->proc-uncached "application/soap\\+xml"))

;;


(: htmldoc-format-output-element :xml2plain:)
(define (htmldoc-format-output-element tree) ; EXPORT
  (with-output-through-htmldoc
   (lambda (port) (html-format-at-output-port (children tree) port))))

(: xmlroff-format-output-element :xml2plain:)
(define (xmlroff-format-output-element tree)
  (let ((temporary-diretory (make-temporary-directory)))
    (run-child-process
     (lambda (port) (xml-format-at-output-port (children tree) port))
     (lambda (port)
       (let ((body (read-bytes #f port))
             (error-value (let ((file (string-append temporary-diretory
                                                     "/errors")))
                            (and (not (file-empty? file)) (filedata file)))))
         ;; remove the directory as the "nobody" user.
         (!start (run-child-process identity identity
                                    `("/bin/rm" "-rf" ,temporary-diretory) #f)
                 temporary-diretory)
         (if error-value
             (error error-value)
             body)))
     `("/bin/sh" "-c"
       ,(string-append "mkdir " temporary-diretory
                       "&& cd " temporary-diretory
                       "&& cat - > input && xmlroff -c9 input 2> errors && cat layout.pdf"))
     #f)))

(: pdfxmltex-format-output-element :xml2plain:)
(define (pdfxmltex-format-output-element tree)
  (let ((temporary-diretory (make-temporary-directory)))
    (run-child-process
     (lambda (port) (xml-format-at-output-port (children tree) port))
     (lambda (port)
       (let ((body (read-bytes #f port))
             (error-value (let ((file (string-append temporary-diretory
                                                     "/errors")))
                            (and (not (file-empty? file)) (filedata file)))))
         ;; remove the directory as the "nobody" user.
         (!start (run-child-process identity identity
                                    `("/bin/rm" "-rf" ,temporary-diretory) #f)
                 temporary-diretory)
         (if error-value
             (error error-value)
             body)))
     `("/bin/sh" "-c"
       ,(string-append "mkdir " temporary-diretory
                       "&& cd " temporary-diretory
                       "&& touch utf8.xmt && cat - > input.fo && pdfxmltex input.fo 2> errors && cat input.pdf"))
     #f)))
;;
(: mime-convert-output-element :xml2plain:)
(define (mime-convert-output-element tree)
  (let ((mime-type (attribute-string 'media-type tree)))
    ((or (mime-converter
          (let ((semicolon (string-index mime-type #\;)))
            (if semicolon (substring mime-type 0 semicolon) mime-type))
          text/xml)
         identity)
     tree)))


(: output-methods (list-of (pair string :xml2plain:)))
(define output-methods
  `(("xml"  . ,(lambda (output)
                 (xml-format (if (and (xml-element? output)
                                      (eq? (gi output) 'output))
                                 (children output)
                                 output))))
    ("html" . ,(lambda (output) (html-format (children output))))
    ("text" . ,data)
    ("x-mime-convert" . ,mime-convert-output-element)
    ("x-xmlroff" . ,xmlroff-format-output-element)
    ("x-pdfxmltex" . ,pdfxmltex-format-output-element)
    ("x-htmldoc" . ,htmldoc-format-output-element)
    ("x-lout" . ,lout-format-output-element)
    ;; FIXME These method bindings are not standard conformant.  They
    ;; are left inhere for bugwise backward compatibility.
    ;; ("htmldoc" . ,htmldoc-format-output-element)
    ("lout" . ,lout-format-output-element)
    ))

(: apply-output-method (* --> string))
(define (apply-output-method node)      ; EXPORT
  (if (xml-element? node)
      (if (match-element? 'output node)
          (let ((method (assoc (or (attribute-string 'method node) "xml")
                               output-methods)))
            (if method
                ((cdr method) node)
                (domain-error 'apply-output-method "unsupported method"
			      (attribute-string 'method node))))
;;           (if (eq? (gi node) 'html)
;;               (html-format node)
;;               (xml-format node))
          (xml-format node))
      (empty-node-list)))

;; Only for initialization.  Not thread safe.  Side effect accepted.

(: register-output-method! (string :xml2plain: -> *))
(define (register-output-method! key function) ; EXPORT
  (let ((entry (assoc key output-methods)))
    (if entry
        (set-cdr! entry function)
        (set! output-methods (cons (cons key function) output-methods))))
  #t)

;; Parser/Serialiser Registration

(%early-once-only
 (define *mime-converters* (make-string-table)))

(: register-mime-converter! (string string :mime-converter: -> *))
(define (register-mime-converter! to-type from-type converter)
  (let ((table (or (hash-table-ref/default *mime-converters* to-type #f)
                   (let ((new (make-string-table)))
                     (hash-table-set! *mime-converters* to-type new)
                     new))))
    (hash-table-set! table from-type converter)))

(: mime-converter (string string --> (or false :mime-converter:)))
(define (mime-converter to-type from-type)
  (if (equal? to-type from-type)
      (let* ((table (hash-table-ref/default *mime-converters* to-type #f))
             (parser (and table (hash-table-ref/default table from-type #f))))
        (or parser identity))
      (and-let* ((table (or (hash-table-ref/default *mime-converters* to-type #f)
			    (and-let* ((semicolon (string-index to-type #\;)))
				      (hash-table-ref/default
				       *mime-converters*
				       (substring to-type 0 semicolon)
				       #f)))))
		(or (hash-table-ref/default table from-type #f)
		    (let ((semicolon (string-index from-type #\;)))
			  (and semicolon
			      (hash-table-ref/default
			       table (substring from-type 0 semicolon) #f)))))))

(: xml-parse-or-plain (* --> :xml-nodelist:))
(define (xml-parse-or-plain obj)
  (guard
   (ex (else (node-list obj)))
   (xml-parse obj)))

(: mime-converter-xml (* --> :xml-nodelist:))
(define (mime-converter-xml obj)
  (cond
   ((string? obj) (xml-parse-or-plain obj))
   ((string? (node-list-first obj)) (xml-parse-or-plain (node-list-first obj)))
   ((node-list? obj) obj)
   (else (error (to-string* (list "can't xml-parse " obj))))))

(: mime-converter-html (* --> :xml-nodelist:))
(define (mime-converter-html obj)
  (cond
   ((string? obj) (html-parse-permissive obj))
   ((string? (node-list-first obj))
    (html-parse-permissive (node-list-first obj)))
   (else (mime-converter-xml obj))))

;; FIXME Need to put some intelligence into the computation of the
;; boundary.  For the time being we rely on ignorance to increase
;; the probability that it works for the vast majority of people.
(define optimistic-boundary
  "Nature reigns mankind, not free will. (random ")

(: make-boundary-for ((list-of *) --> string))
(define (make-boundary-for serialized)
  (string-append optimistic-boundary (number->string (length serialized)) (number->string (random 268435456)) ")"))

(: multipart-mixed-of (string --> string))
(define (multipart-mixed-of boundary)
  (string-append "multipart/mixed; boundary=\"" boundary "\""))

(: mime-format-multipart-output-element :xml2plain:)
(define (mime-format-multipart-output-element tree)
  (let* ((serialized (node-list-map apply-output-method (children tree)))
	 (boundary (make-boundary-for serialized)))
    (apply-string-append
     ;; FIXME This loop works only if node-list-map returns
     ;; normalized lists (no nested lists).  Clean this up!
     (let loop ((s serialized)
		(o (children tree))
		(tluser '()))
       (cond
	((node-list-empty? s)
	 (reverse! (cons
		    (string-append "\r\n--" boundary "--\r\n")
		    tluser)))
	;; HACK 'serialized' contains exactly one result from
	;; applying an output method to an output element.  We
	;; filter out the other elements here.
	((not (xml-element? (node-list-first o)))
	 (loop s (node-list-rest o) tluser))
	(else
	 (loop (node-list-rest s)
	       (node-list-rest o)
	       (cons
		(node-list-first s)
		(cons
		 (string-append
		  "\r\n--" boundary "\r\nContent-Type: "
		  (or (attribute-string 'media-type (node-list-first o))
		      "text/xml")
		  "\r\n\r\n")
		 tluser)))))))))

(begin ;; %early-once-only
 (for-each
  (lambda (xml-encoded)
    (register-mime-converter! text/xml xml-encoded mime-converter-xml))
  xml-parseable-types)
 (register-mime-converter! text/xml text/html html-parse-permissive)
 (register-mime-converter! "multipart/mixed" text/xml
                           mime-format-multipart-output-element)
 )

(: xml-parseable? ((or false string) --> boolean))
(define (xml-parseable? x)
  (or (and (string? x) (string-suffix? "+xml" x))
      (and x (eq? (mime-converter text/xml x) mime-converter-xml))))

(: xml-parseable? ((or false string) --> boolean))
(define (html-parseable? x)
  (and x (let ((semicolon (string-index x #\;)))
           (equal? (if semicolon (substring x 0 semicolon) x)
                   text/html))))

;; Find the next possible mime boundary.  TODO use SRFI-13 string
;; search.

(define (mime-boundary str . off)
  (let ((off (or (and (pair? off) (car off)) 0))
        (len (string-length str)))
    (cond
     ((< (- len off) 2) (values #f #f))
     ((and (eqv? (string-ref str off) #\-)
           (eqv? (string-ref str (fx+ off 1)) #\-))
      (values off (fx+ 2 off)))
     (else
      (let loop ((off off))
        (let ((r (string-index str #\return off)))
          (if r
              (if (and (fx>= (- len r) 3)
                       (eqv? (string-ref str (fx+ r 1)) #\newline)
                       (eqv? (string-ref str (fx+ r 2)) #\-)
                       (eqv? (string-ref str (fx+ r 3)) #\-))
                  (values r (fx+ r 4))
                  (loop (fx+ r 1)))
              (let ((n (string-index str #\newline off)))
                (if (and (fx>= (- len n) 2)
                         (eqv? (string-ref str (fx+ n 1)) #\-)
                         (eqv? (string-ref str (fx+ n 2)) #\-))
                    (values n (fx+ n 3))
                    (loop (fx+ n 1)))))))))))

(%early-once-only

 (define *ns->ct* (make-symbol-table))
 (define *gi->ct* (make-symbol-table))

 (for-each (lambda (e) (hash-table-set! *ns->ct* (car e) (cadr e)))
	   `((,namespace-xhtml "application/xhtml+xml")))

 (for-each (lambda (e) (hash-table-set! *gi->ct* (car e) (cadr e)))
	   `((html "text/html")
	     (HTML "text/html")))

 )

(: node-list-media-type (:xml-nodelist: --> (or false string)))
(define (node-list-media-type nl)
  (let ((nl (document-element nl)))
    (and (xml-element? nl)
	 (or (and (ns nl)
		  (hash-table-ref/default *ns->ct* (ns nl) #f))
	     (and (gi nl)
		  (hash-table-ref/default *gi->ct* (gi nl) #f))
	     text/xml))))

(: singleton-element-node-list (:xml-nodelist: --> (or false :xml-element:)))
(define (singleton-element-node-list nl)
  (if (node-list-empty? nl) #f
      (let loop ((nl nl))
	(let ((first (node-list-first nl)))
	  (if (xml-element? first)
	      (let loop ((nl (node-list-rest nl)))
		(if (node-list-empty? nl)
		    first
		    (if (xml-element? (node-list-first nl)) #f (loop (node-list-rest nl)))))
	      (loop (node-list-rest nl)))))))

(define (mime-format output-fragment . target-mime-type) ; EXPORT
  (let ((singleton (singleton-element-node-list output-fragment)))
    (if singleton
	(call-with-values (lambda () (apply-output-method singleton))
	  (lambda (bytes . content-type)
	    (values (data bytes)
		    (or (and (pair? content-type) (car content-type))
			(attribute-string 'media-type singleton) "text/xml"))))
	(let ((serialized (node-list-map apply-output-method output-fragment)))
	  (cond
	   ((node-list-empty? serialized)
	    (values "" "text/plain"))
	   ((node-list-empty? (node-list-rest serialized))
	    (values (data (node-list-first serialized))
		    (let ((doc (document-element (node-list-first serialized))))
		      (if (eq? 'html (gi doc))
			  (content-type-for-html doc)
			  (or (attribute-string
			       'media-type
			       (document-element output-fragment))
			      "text/xml")))))
	   (else
	    (let ((boundary (make-boundary-for serialized)))
	      (values
	       (apply-string-append
		;; FIXME This loop works only if node-list-map returns
		;; normalized lists (no nested lists).  Clean this up!
		(let loop ((s serialized)
			   (o output-fragment)
			   (tluser '()))
		  (cond
		   ((node-list-empty? s)
		    (reverse! (cons
			       (string-append "\r\n--" boundary "--\r\n")
			       tluser)))
		   ;; HACK 'serialized' contains exactly one result from
		   ;; applying an output method to an output element.  We
		   ;; filter out the other elements here.
		   ((not (xml-element? (node-list-first o)))
		    (loop s (node-list-rest o) tluser))
		   (else
		    (loop (node-list-rest s)
			  (node-list-rest o)
			  (cons
			   (node-list-first s)
			   (cons
			    (string-append
			     "\r\n--" boundary "\r\nContent-Type: "
			     (or (attribute-string 'media-type (node-list-first o))
				 "text/xml")
			     "\r\n\r\n")
			    tluser)))))))
	       (multipart-mixed-of boundary)))))))))

;;

(%early-once-only

;; Obsolete RFC 2396
(define rfc-2396.uri-reserved-chars
  `(;; reserved
    #\; #\/ #\? #\: #\@ #\& #\= #\+ #\$ #\,
    ;; space
    #\space
    ;; delims
    #\< #\> #\# #\% #\"
    ;; unwise
    #\{ #\} #\| #\\ #\^ #\[ #\] #\`
    ;; control
    . ,(map integer->char '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
                              17 18 19 20 21 22 23 24 25 26 27 28 29
                              30 31 127))))

;; RDF 3986
(define rfc-3986.uri-reserved-chars
  `(;; reserved
    ;; gen-delims
    #\:	#\/ #\?	#\# #\[ #\] #\@
    ;; sub-delims
    #\!	#\$ #\& #\' #\( #\) #\* #\+ #\, #\; #\=
    ;; space
    #\space
    ;; control
    . ,(map integer->char '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
                              17 18 19 20 21 22 23 24 25 26 27 28 29
                              30 31 127))))

(define *char-uri-reserved-table*  (make-character-table))
(define *char-uri-reserved-table128*  (make-character-table))
(define *char-form-reserved-table* (make-character-table))

(define init-uri-reserved-chars!
  (let ((hex '#(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8
		#\9 #\A #\B #\C #\D #\E #\F)))
    (lambda (uri-reserved-chars)
      (for-each
       (lambda (c)
	 (let ((re (let ((x (char->integer c)))
		     (string #\%
			     (vector-ref hex (quotient x 16))
			     (vector-ref hex (remainder x 16))))))
	   (hash-table-set! *char-uri-reserved-table* c re)
	   (hash-table-set! *char-uri-reserved-table128* c re)
	   (hash-table-set! *char-form-reserved-table* c re)))
       uri-reserved-chars)

    (hash-table-set! *char-form-reserved-table* #\space "+")

    (do ((i 128 (add1 i)))
	((> i 255))
      (hash-table-set! *char-uri-reserved-table128* (integer->char i)
		       (string #\%
			       (vector-ref hex (quotient i 16))
			       (vector-ref hex (remainder i 16))))))))

(init-uri-reserved-chars! rfc-2396.uri-reserved-chars)

(define (set-uri-reserved-chars! obj)
  (let ((obj (cond
	      ((pair? obj) obj)
	      ((string? obj) (string->list obj))
	      ((eq? obj 'rfc-2396) rfc-2396.uri-reserved-chars)
	      ((eq? obj 'rfc-3986) rfc-3986.uri-reserved-chars)
	      (else (error "set-uri-reserved-chars! illegal argument"))) ))
    (set! *char-uri-reserved-table*  (make-character-table))
    (set! *char-uri-reserved-table128*  (make-character-table))
    (set! *char-form-reserved-table* (make-character-table))
    (init-uri-reserved-chars! obj)))

) ; %early-once-only

(define (quote-using table str)
  (let ((sl (string-length str)))
    ;;(call-with-output-string (lambda (port)
    (let loop ((i 0) (n 0) (reuse #t))
      (if (eqv? i sl)
	  (if reuse
	      str
	      (let ((result (make-string/uninit n)))
		(let loop ((i 0) (j 0))
		  (if (eqv? i sl)
		      result
		      (let ((re (hash-table-ref/default table (string-ref str i) #f)))
			(if re
			    (do ((k 0 (add1 k))
				 (j j (add1 j)))
				((eqv? k (string-length re)) (loop (add1 i) j))
			      (string-set! result j (string-ref re k)))
			    (begin
			      (string-set! result j (string-ref str i))
			      (loop (add1 i) (add1 j)))))))))
	  (let ((re (hash-table-ref/default table (string-ref str i) #f)))
	    (loop (add1 i)
		  (+ n (if re (string-length re) 1))
		  (and reuse (not re))))))))

(define (uri-quote str)
  (quote-using *char-uri-reserved-table* str))

(define (uri-quote-all str)
  (quote-using *char-uri-reserved-table128* str))

(define (form-quote str)
  (quote-using *char-form-reserved-table* str))

(: uri-parse (string --> string))
(define (uri-parse ent)
  (let loop ((i 0)
	     (r '()))
    (let ((n (string-index ent #\% i)))
      (if n
	  (loop (+ n 3)
		(cons (string
		       (integer->char
			(string->number (substring ent (+ n 1) (+ n 3))	16)))
		      (cons (substring ent i n) r)))
	  (if (null? r)
	      ent
              (to-string*
               (reverse! (cons (substring ent i (string-length ent)) r))))))))

(: form-parse (string --> string))
(define (form-parse ent)
  (apply-string-append  ;; or srfi:string-join ?
   (let loop ((pos 0))
     (let* ((plus (string-index ent #\+ pos))
	    (percent (string-index ent #\% pos))
	    (where (if plus (min plus (or percent plus)) percent))) 
       (if where
	   (if (eq? (string-ref ent where) #\+)
	       (cons (substring ent pos where)
                     (cons " "
			   (loop (+ where 1))))
	       (cons (substring ent pos where)
	             (cons 
		      (string
		       (integer->char
			(or (string->number 
			     (substring 
			      ent (+ where 1) (+ where 3)) 16)
			    (error "form-parse: invalid escape sequence ~s"
				   (substring 
				    ent where (+ where 3)) ))))
                           (loop (+ where 3)))))
	   (list (if (eq? pos 0) ent
		     (substring ent pos))))))))
;;

(: message-body-offset (string --> fixnum))
(define (message-body-offset str)
  (receive* (s e) (message-body-offset-regex str) (or e 0)))

(: addlws-suffix-len (string fixnum --> fixnum))
(define (addlws-suffix-len str offset)
  (receive* (s e) (lws-suffix str offset) (if s e 0)))

(: rfc2046-split (string string --> (or false (list-of string))))
(define (rfc2046-split body boundary)
  (let ((total (string-length body))
        (blen (string-length boundary)))
    (let part-loop ((offset 0) (tluser '()))
      (let start-loop ((e0 offset))
        (receive*
         (s e) (mime-boundary body e0)
         (if s
             (let end-loop ((match-len 0) (match e))
               (if (eqv? match-len blen)
                   (part-loop (addlws-suffix-len body match)
                              (cons (substring body offset s) tluser))
                   (if (and (< match total)
                            (eqv? (string-ref body match)
                                  (string-ref boundary match-len)))
                       (end-loop (add1 match-len) (add1 match))
                       (start-loop e))))
             (if (null? tluser)
                 #f
                 ;; cdr: skip the stuff in front of the first boundary
                 (cdr (reverse! tluser)))))))))


(: parsed-locator (string --> (list-of string)))
(define (parsed-locator path)
  ;; (xpath:parse path)
  (cond 
   ((or (string=? path "")
	(string=? path "/"))
    '())
   (else
    (map uri-parse 
	 (string-split/include-empty
	  (if (eqv? #\/ (string-ref path 0))
	      (substring path 1) path) "/")))))
