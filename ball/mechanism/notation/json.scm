#|
The problem is that JSON arrays and objects are disjoint, and the egg uses
lists to represent JSON arrays.  Since lists and a-lists are obviously
not disjoint, something else has to be done.

One alternative would be to represent JSON objects as a-lists and
JSON arrays as vectors.  This is the approach taken by the json-abnf
egg, and by the medea egg by default, although medea's representation
is customizable.  They differ in their representation of JSON null,
however: json-abnf uses (), whereas medea's default is 'null; the json
egg uses the undefined value.

I am going to be adding JSON support to my JSO egg when I can steal
a few cycles for it.  JSOs are specially marked a-lists that work
like JavaScript objects, so I will use them to represent JSON objects.
JSON null will be represented by a unique empty JSO.  I haven't yet settled
on the representation of JSON arrays; I might use specially marked JSOs,
or I might use lists (which would require the caller to test for arrays
before objects) or even vectors.
|#

;;; (C) 2011 JFW

;;; http://tools.ietf.org/html/rfc4627

(define application/json "application/json")


(define-record-type <json-object>
  (%make-json-object dict)
  json-object?
  (dict json-object-dict))
(define (make-json-object)
  (%make-json-object (make-string-ordered-llrbtree)))
(define (json-object-bind! o key value)
  (string-ordered-llrbtree-set! (json-object-dict o) key value)
  o)
(define (json-object-ref o key)
  (string-ordered-llrbtree-ref/default (json-object-dict o) key '()))
(define (json-object-fold p i o)
  (string-ordered-llrbtree-fold p i (json-object-dict o)))
(define (json-object-for-each p o)
  (string-ordered-llrbtree-for-each p (json-object-dict o)))

#|
(define hexchar->integer
  (let ((c0 (char->integer #\0))
	(c9 (char->integer #\9))
	(la (char->integer #\a))
	(lf (char->integer #\f))
	(lsub (fx- (char->integer #\a) 10))
	(ua (char->integer #\A))
	(uf (char->integer #\F))
	(usub (fx- (char->integer #\A) 10)))
    (lambda (c)
      (let ((cv (char->integer c)))
	(cond
	 ((and (fx>= cv c0) (fx>= c9 cv)) (fx- cv c0))
	 ((and (fx>= cv la) (fx>= lf cv)) (fx- cv lsub))
	 ((and (fx>= cv ua) (fx>= uf cv)) (fx- cv usub)))))))

(define (json-fold-escape-sequence str off init cont)
  (set! off (add1 off))
  (if (fx>=  off (string-length str)) (raise 'json-invalid-escape-char))
  (if (eqv? (string-ref str off) #\u)
      (let ((num 0) (limit (fx+ off 5)) (second #f))
	(if (> limit (string-length str)) (raise 'json-escape-invalid))
	(do ((i (add1 off) (add1 i)))
	    ((fx>= i limit)
	     ;; FIXME: http://tools.ietf.org/html/rfc4627 section
	     ;; 2.5. "To escape an extended character that is not in
	     ;; the Basic Multilingual Plane, the character is
	     ;; represented as a twelve-character sequence," is not yet handled!!!
	     (cont limit (cons (integer->utf8string num) init)))
	  (set! num (+ (* num 16) (hexchar->integer (string-ref str i))))))
      (case (string-ref str off)
	((#\\) (cont (add1 off) (cons "\\" init)))
	((#\") (cont (add1 off) (cons "\"" init)))
	((#\b) (cont (add1 off) (cons "\b" init)))
	((#\n) (cont (add1 off) (cons "\n" init)))
	((#\r) (cont (add1 off) (cons "\r" init)))
	((#\t) (cont (add1 off) (cons "\t" init)))
	(else (raise 'json-escape-invalid-key)))))
|#

(define json-fold-escape-sequence
  (let ((solidus "\\")
	(quot "\"")
	(backlash "\b")
	(nl "\n")
	(return "\r")
	(tab "\t"))
    (lambda (str off init cont)
      (set! off (add1 off))
      (if (fx>=  off (string-length str)) (raise 'json-invalid-escape-char))
      (if (eqv? (string-ref str off) #\u)
	  ;; FIXME: http://tools.ietf.org/html/rfc4627 section
	  ;; 2.5. "To escape an extended character that is not in the
	  ;; Basic Multilingual Plane, the character is represented as
	  ;; a twelve-character sequence," is not yet handled!!!
	  (let ((limit (fx+ off 5)))
	    (if (> limit (string-length str)) (raise 'json-escape-invalid))
	    (cont limit (cons (integer->utf8string (string->number (substring str (add1 off) limit) 16)) init)))
	  (case (string-ref str off)
	    ((#\\) (cont (add1 off) (cons solidus init)))
	    ((#\") (cont (add1 off) (cons quot init)))
	    ((#\b) (cont (add1 off) (cons backlash init)))
	    ((#\n) (cont (add1 off) (cons nl init)))
	    ((#\r) (cont (add1 off) (cons return init)))
	    ((#\t) (cont (add1 off) (cons tab init)))
	    (else (raise 'json-escape-invalid-key)))))))

(define (json-string-escaped->utf8-string str) ; precondition: str MUST contain at least one escape
  (let loop ((offset 0) (tluser '()))
    (if (eqv? offset (string-length str))
	(apply-string-append (reverse! tluser))
	(let ((escape (string-index str #\\ offset)))
	  (if escape
	      (json-fold-escape-sequence
	       str escape (cons (substring str offset escape) tluser) loop)
	      (apply-string-append
	       (reverse! (cons (substring/shared str offset (string-length str)) tluser))))))))

(define (make-json-parser
	 make-dict
	 dict-bind!
	 dict-cast
	 list->array
	 string->number
	 value-cast
	 )
  (pcre-lalr-parser
   (("[ \\t\\n\\r]+")			; ws (dropped)
    ("\\[" begin-array)
    ("]" end-array)
    ("{" begin-object)
    ("}" end-object)
    (":" name-separator)
    ("," value-separator)
    ("(-?[[:digit:]]+(?:\\.[[:digit:]]+)?(?:[eE][-+]?[[:digit:]]+)?)" number)
    ("\"([^\"\\\\]*)\"" string-plain)
    ("\"([^\"]*)\"" string-escaped)
    ("false" false)
    ("null" null)
    ("true" true)
    )
   (begin-array end-array begin-object end-object name-separator value-separator
		false null true number string-plain string-escaped)
   (json (object) : $1
	 (array) : $1)

   (object (begin-object members) : (dict-cast $2))
   (members (end-object) : (make-dict)
	    (member more-members) : (dict-bind! $2 (car $1) (cdr $1)))
   (more-members (end-object) : (make-dict)
		 (value-separator members) : $2)

   (member (string name-separator value) : (cons $1 $3))

   (array (begin-array array-elements) : (list->array $2))
   (array-elements (end-array) : '()
		   (value more-array-elements) : (cons (value-cast $1) $2))
   (more-array-elements (end-array) : '()
			(value-separator array-elements) : $2)

   (value (false) : #f
	  (true) : #t
	  (null) : '()
	  (object) : $1
	  (array) : $1
	  (number) : (string->number (car $1))
	  (string) : $1)

   (string (string-plain) : (car $1)
	   (string-escaped) : (json-string-escaped->utf8-string (car $1)))
   ))

(define json->scheme
  (make-json-parser
   make-json-object json-object-bind! identity
   list->vector
   string->number
   identity				; value-cast
   ))

(define json->xml
  (make-json-parser
   (lambda () (vector (empty-node-list)))
   (lambda (dict n v)
     (vector-set!
      dict 0
      (cons (make-xml-element 'li #f (list (make-xml-attribute 'name #f n)) v)
	    (vector-ref dict 0))))
   (lambda (dict) (make-xml-element 'Bag #f '() (vector-ref dict 0)))
   (lambda (nl) (make-xml-element 'Sequence #f '() nl))
   identity				; string->number
   (lambda (nl)
     (make-xml-element
      'li #f '()
      (cond
       ((eq? nl #f) "false")
       ((eq? nl #t) "true")
       (else nl))))))

(define (json-write-string str port)
  (display #\" port)
  (do ((i 0 (add1 i)))
      ((eqv? i (string-length str)) (display #\" port))
    (let* ((c (string-ref str i))
	   (x (char->integer c)))
      (if (or (< x #x20)
	      (and (> x #x20) (< x #x23))
	      (and (> x #x5b) (< x #x5d)))
	  (begin
	    (display "\\u" port)
	    (do ((i 4096 (quotient i 16)))
		((eqv? i 0))
	      (let ((n (quotient x i)))
		(if (fx>= n 10)
		    (display (integer->char (fx+ n 97)) port)
		    (display (integer->char (fx+ n 48)) port)))))
	  (display c port)))))

(define (json-write obj port)
  (cond
   ((vector? obj)
    (display "[" port)
    (srfi:vector-for-each
     (lambda (i el)
       (json-write el port)
       (if (< (add1 i) (vector-length obj)) (display "," port)))
     obj)
    (display "]" port))
   ((pair? obj)
    (display "[" port)
    (json-write (car obj) port)
    (for-each
     (lambda (el)
       (display "," port)
       (json-write el port))
     (cdr obj))
    (display "]" port))
   ((number? obj) (display obj port))
   ((boolean? obj) (display (if obj "true" "false") port))
   ((string? obj) (json-write-string obj port))
   ((symbol? obj) (json-write-string (symbol->string obj) port))
   ((json-object? obj)
    (json-object-fold
     (lambda (k v i)
       (if i (display #\, port))
       (json-write-string k port)
       (display #\: port)
       (json-write v port)
       #t)
     #f obj))
   (else (raise (format "json-write: unsupported data type ~a" obj)))))

;; sorry, but bencode-write is not worth a file of it's own.

(define (bencode-write obj port)
  (cond
   ((integer? obj) (display #\i port) (display obj port) (display #\e port))
   ((number? obj) (bencode-write (inexact->exact obj) port))
   ((string? obj) (display (string-length obj) port) (display obj port))
   ((symbol? obj) (bencode-write (symbol->string obj) port))
   ((vector? obj)
    (display "l" port)
    (srfi:vector-for-each (lambda (i el) (bencode-write el port)) obj)
    (display "e" port))
   ((pair? obj)
    (display "l" port)
    (for-each (lambda (el) (bencode-write el port)) obj)
    (display "e" port))
   ((boolean? obj) (display (if obj "i1e" "i0e") port))
   ((json-object? obj)
    ;; ASSERT!  We know, assume and rest on the knowledge, that a 
    (display "d" port)
    (json-object-for-each
     (lambda (k v)
       (bencode-write k port)
       (bencode-write v port))
     obj)
    (display "e" port))
   (else (raise (format "bencode-write: unsupported data type ~a" obj)))))
