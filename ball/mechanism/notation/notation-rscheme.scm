;; (C) 2002, 2003 J�rg F. Wittenberger see http://www.askemos.org

;; BEWARE the code currently abuses the define-transformer macro to
;; terrible extent.  This is really bad for understanding the code and
;; shall be fixed.  Now it's so because I'm trying some optimizations.

(define make-display-final (with-input-from-string "" read))

(define-glue (mapaddstr9 buf si str sj sl part sp mapping cont)
 literals: ((& mapaddstr9))
{
  UINT_32 i=fx2int(si), j=fx2int(sj), p=fx2int(sp), n=0, d1,
          l =fx2int(sl),
          pl=string_length(part),
          bl=string_length(buf);
  unsigned char *ori = string_text (str);
  register char *src = string_text (part) + p;
  register char *dst = string_text (buf) + i;
  register char *end = NULL;

  d1 = pl - p; 
  while( 1 ) {
    n = bl - i;
    n = n < d1 ? n : d1 ;
    end = src + n;
    while ( src != end ) *dst++=*src++;
    i += n;
    p += n;
    if( bl == i || j == l ) break;
    else {
     UINT_32 c = ori[j++];
     /* part = vector_ref(mapping, int2fx(c)); */
     part = gvec_ref(mapping, FXWORDS_TO_RIBYTES(int2fx(c)));
     src  = string_text (part);
     pl   = string_length(part);
     p    = 0;
     d1   = pl;
    }
  }
  if( i == bl ) {
    obj now = cont;
    si = int2fx(i);
    sj = int2fx(j);
    sl = int2fx(l);
    sp = int2fx(p);
    cont = TLREFB(0);
    APPLY(9, now);
  } else {
    REG0 = int2fx(i);
    RETURN1();
  }
})

;; Changing this $buffer-size configuration parameter seems not to
;; change more than 5% of the overall performance of the xml
;; serialization.

(define $buffer-size 4096) ;; 16384 was way to large for M$ to deal with.

;; If set, all output will be run through iconv, if #f the internal
;; representation will just pass through.
;;
;; There are at least two caveats: a) debugging: skip the output check
;; on the expense of broken encoding b) performance optimisation:
;; since broken encoding affects only safety, not security, one might
;; want to trade those.

(define $force-iconv-on-output #f)

(define (make-display kind encoding)
  (let* ((buffer (make-string $buffer-size))
         (cnv (and (or $force-iconv-on-output
                       (not (or (equal? encoding "UTF-8")
                                (equal? encoding "UTF8"))))
                   (iconv-open encoding "UTF-8")))
         (ibuffer (and cnv (make-string $buffer-size)))
         (i 0)
         (l (if (output-port? kind) #f (list 'dummy)))
         (r l))

    (define (flush9 buf i str j sl part p mapping continue)
      (if l
          (begin
            (set-cdr! r (list buf))
            (set! r (cdr r))
            (set! buffer (make-string $buffer-size)))
          (display buf kind))
      (continue buffer 0 str j sl part p mapping flush9))

    (define (flush5 buf i str j continue)
      (if l
          (begin
            (set-cdr! r (list buf))
            (set! r (cdr r))
            (set! buffer (make-string $buffer-size)))
          (display buf kind))
      (continue buffer 0 str j flush5))

    (lambda (obj mapping)
      (cond
       (mapping
        (if cnv
            (let loop ((off 0)
                       (n (string-length obj)))
              (if (= n 0)
                  i
                  (receive (nx sn dn)
                           (iconv-bytes cnv obj off n ibuffer 0 $buffer-size)
                           (set! i (mapaddstr9 buffer i
                                               ibuffer 0 (- $buffer-size dn)
                                               "" 0 mapping flush9))
                           (if (< sn 0)
                               i
                               (loop (+ off (- n sn)) sn)))))
            (set! i (mapaddstr9 buffer i obj 0 (string-length obj)
                                "" 0 mapping flush9))))
       ((string? obj) (set! i (string-splice! buffer i obj 0 flush5)))
       ((symbol? obj)
        (set! i (string-splice! buffer i (symbol->string obj) 0 flush5)))
       ((eof-object? obj)
        (if cnv (iconv-close cnv))
        (cond
         ((eq? kind 'string)
          (set-cdr! r (list (if (eqv? i (string-length buffer)) buffer
                                (substring buffer 0 i))))
          (apply-string-append (cdr l)))
         ((eq? kind 'list)
          (set-cdr! r (list (if (eqv? i (string-length buffer)) buffer
                                (substring buffer 0 i))))
          (set! buffer #f)
          (set-car! l (fold (lambda (e i) (fx+ (string-length e) i))
                            0 (cdr l)))
          l)
         (else (if (eqv? i (string-length buffer))
                   (display buffer kind)
                   (do ((k 0 (add1 k)))
                       ((eqv? k i))
                     (display (string-ref buffer k) kind))))))
       (else (error "no handler for ~a" obj))))))

;; Parsing

(define (error-for-lalr msg . obj)
  (if (pair? obj)
      (error "~a ~a" msg obj)
      (error "~a" msg)))

(define-macro (pcre-lalr-parser scan . arguments)
  `(let ((parser ,(with-module
		   compiletime
		   (apply gen-lalr-parser
			  ; (map cadr (filter (lambda (x) (pair? (cdr x))) scan))
			  arguments)))
         (scanner (make-pcre-tokeniser ',scan)))
     (lambda (str)
       (receive (tokens position) (scanner str)
                (guard
                 (condition
                  (else
                   (receive
                    (title msg args rest) (condition->fields condition)
		    (raise (make-condition
			    &message 'message (format "~a\n~a in:\n~a\n~a" title msg str args))))))
                 (parser tokens error-for-lalr))))))

;; Definitions of the required regular expressions for the notation
;; module. in rscheme's syntax.

(define html-escape-needed?
  (reg-expr->proc '(or #\& #\< #\> #\")))

;; TODO this "save" is discharded immediately, try to save the
;; memory allocation.

(define xpath:axis-specifier-re
  (reg-expr->proc
   `(prefix (seq (save (or ,@xpath:axis-specifier-list))
                 (* (or #\space #\newline #\return #\tab))
                 "::"))))

(define (xpath:axis-specifier-read str off)
  (receive*
   (s e v) (xpath:axis-specifier-re str off)
   (values e (and v (cdr (assoc v xpath:axis-specifier-indicators))))))

(define xpath:literal-regex-dq
  (reg-expr->proc '(prefix (seq #\" (save (* (not #\"))) #\"))))
(define xpath:literal-regex-sq
  (reg-expr->proc '(prefix (seq #\' (save (* (not #\'))) #\'))))

;; TODO this "save" is discharded immediately, try to save the
;; memory allocation.
(define xpath:node-type-re
  (reg-expr->proc
   `(prefix (seq (save (or ,@xpath:node-type-list))
                 (* (or #\space #\newline #\return #\tab))
                 #\())))

(define (xpath:node-type-test str off)
  (receive* (s e v) (xpath:node-type-re str off)
            (values e (and v (cdr (assoc v xpath:node-type-indicators))))))

;; Again a 'save' to be saved

(define xpath:number
  (let ((re (reg-expr->proc
             '(prefix (save (or (seq (+ digit) (? (seq #\. (* digit))))
                                (seq #\. (+ digit))))))))
    (lambda (a b)
      (receive* (s e v) (re a b)
                (if s (values e (string->number v)) (values #f #f))))))

;; MIME

(define multipart-data-boundary
  (let ((matcher (reg-expr->proc
                  '(entire (seq (* (or #\space #\tab))
                                "multipart/"
                                (+ (or alpha #\-))
                                (+ (or #\space #\tab #\;))
                                "boundary="
                                (? #\") (save (+ (not #\"))) (? #\")
                                (* (or #\space #\tab)))))))
    (lambda (str)
      (receive*
       (start end boundary) (matcher str)
       boundary))))

(define content-disposition->name-regex
  (reg-expr->proc
   '(entire (seq (* (or #\space #\tab))
                 "form-data"
                 (+ (or #\space #\tab #\;))
                 "name=" (? #\")
                 (save (or (+ (or alpha digit #\. #\- #\_))
                           "*xmlns*" "*name*"))
                 (? #\")
                 (? (seq (+ (or #\space #\tab #\;))
                         "filename=" (? #\")
                         (save (* (not #\")))
                         (? #\")
                         (* any)))))))

(define (content-disposition->name line)
  (receive* (s e name filename) (content-disposition->name-regex line)
            (if filename
		(let ((r (string-index-right filename #\\)))
		  (values name (if r
				   (substring filename (add1 r) (string-length filename))
				   filename)))
		(values name filename))))

(define message-body-offset-regex
  (reg-expr->proc '(seq (? #\cr) #\newline (? #\cr) #\newline)))

(define mime-boundary
  (reg-expr->proc '(or (prefix "--")
                       (seq (? #\cr) #\newline "--"))))

(define lws-suffix
  (reg-expr->proc '(prefix (seq (? "--")
                                (* (or #\tab #\space))
                                (? #\cr)
                                (? #\newline)))))

;; rfc1421

(define parse-basic-authorization
  (let ((matcher (reg-expr->proc '(seq "Basic"
				       (* space)
				       (let encoded-string (* (not space)))))))
    (lambda (str)
      (call-with-values (lambda () (matcher str))
	(lambda (start . rest)
	  (if start (values start (car rest) (cadr rest)) (values #f #f #f)))))))
