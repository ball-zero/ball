;; (C) 2000, 2001 Jörg F. Wittenberger see http://www.askemos.org

;;*** RFC 1421

;; This code is mostly copied from RScheme's http server (which BTW
;; I'd loved to resuse if it was a bit cleaner).

;; There are slight modification to reduce complexity, those should be
;; fed back into rscheme!

(define $pem-encoding
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")

(define (pem-encode-char num) ;; (num <fixnum>))
  (string-ref $pem-encoding num))

(define pem-decode-pos
  (let ((ua (char->integer #\A))
        (wua (- (char->integer #\A)))
        (uz (char->integer #\Z))
        (la (char->integer #\a))
        (wla (- 26 (char->integer #\a)))
        (lz (char->integer #\z))
        (c0 (char->integer #\0))
        (w0 (- 52 (char->integer #\0)))
        (c9 (char->integer #\9))
        (cp (char->integer #\+))
        (cs (char->integer #\/)))
    (lambda (str i)
      (let ((cc (char->integer (string-ref str i))))
        (cond
         ((eqv? cc cs) 63)
         ((eqv? cc cp) 62)
         ((and (>= cc c0) (<= cc c9)) (+ cc w0))
         ((and (>= cc la) (<= cc lz)) (+ cc wla))
         ((and (>= cc ua) (<= cc uz)) (+ cc wua))
         (else
          (error "pem-decode-pos: invalid PEM encoding char: ~s position ~a"
		 (string-ref str i) i)))))))

(define (pem-encode-string str) ; (str <string>))
  (let* ((n (* 4 (quotient (+ (string-length str) 2) 3)))
	 (result (make-string n #\=)))
    (let loop ((i 2)
	       (j 0))

      (cond-expand
       (chicken (chicken-check-interrupts!))
       (else (begin)))

      (cond
       ((< i (string-length str))
	  ;; full input quantum available
        (let ((num (+ (logical-shift-left (char->integer (string-ref str (- i 2))) 16)
                      (logical-shift-left (char->integer (string-ref str (- i 1))) 8)
                      (char->integer (string-ref str i)))))
          (let ((a (logical-shift-right num 18))
                (b (bitwise-and 63 (logical-shift-right num 12)))
                (c (bitwise-and 63 (logical-shift-right num 6)))
                (d (bitwise-and 63 num)))
            (string-set! result j (pem-encode-char a))
            (string-set! result (+ j 1) (pem-encode-char b))
            (string-set! result (+ j 2) (pem-encode-char c))
            (string-set! result (+ j 3) (pem-encode-char d))
            (loop (+ i 3) (+ j 4)))))
       ;; not a full input quantum available
       ((< (- i 1) (string-length str))
        ;; two input bytes
        (let ((num
               (+ (logical-shift-left (char->integer (string-ref str (- i 2))) 10)
                  (logical-shift-left (char->integer (string-ref str (- i 1))) 2))))
          (let ((a (bitwise-and 63 (logical-shift-right num 12)))
                (b (bitwise-and 63 (logical-shift-right num 6)))
                (c (bitwise-and 63 num)))
            (string-set! result j (pem-encode-char a))
            (string-set! result (+ j 1) (pem-encode-char b))
            (string-set! result (+ j 2) (pem-encode-char c))
            result)))
       ((< (- i 2) (string-length str))
        ;; one input byte
        (let ((num (char->integer (string-ref str (- i 2)))))
          (let ((a (bitwise-and 63 (logical-shift-right num 2)))
                (b (bitwise-and 63 (logical-shift-left num 4))))
            (string-set! result j (pem-encode-char a))
            (string-set! result (+ j 1) (pem-encode-char b))
            result)))
       (else result)))));; none

(define (pem-decode-string str) ; (str <string>))
  (if (not (eq? (remainder (string-length str) 4) 0))
      (error "pem-decode-string: not a valid PEM RFC-1421 (~a mod 4 = ~a) encoding: ~s"
	     (string-length str) (remainder (string-length str) 4)
	     (if (fx>= (string-length str) 101) (substring str 0 100) str)))
  (if (string=? str "")
      str
      (let ((n (quotient (string-length str) 4))
	    (skips (if (eq? (string-ref str (- (string-length str) 2)) #\=)
		       2
		       (if (eq? (string-ref str (- (string-length str) 1)) #\=)
			   1
			   0))))
	(let ((result (make-string (- (* n 3) skips) #\space)))
	  (let loop ((i 0)
		     (j 0))
	    ;(format #t "~d ~d : ~s\n" i j (substring str i j))


	    (cond-expand
	     (chicken (chicken-check-interrupts!))
	     (else (begin)))

	    (if (< j (string-length str))
		(let ((num (+ (logical-shift-left (pem-decode-pos str j) 18)
			      (logical-shift-left (pem-decode-pos str (+ j 1)) 12)
			      (if (< (+ i 1) (string-length result))
				  (logical-shift-left (pem-decode-pos str (+ j 2)) 6)
				  0)
			      (if (< (+ i 2) (string-length result))
				  (pem-decode-pos str (+ j 3))
				  0))))
		  ;(format #t "   num = ~06x\n" num)
		  (string-set! result i
                               (integer->char (logical-shift-right num 16)))
		  (if (< (+ i 1) (string-length result))
		      (begin
			(string-set!
                         result 
                         (+ i 1)
                         (integer->char
                          (bitwise-and 255 (logical-shift-right num 8))))
			(if (< (+ i 2) (string-length result))
			    (string-set!
                             result
                             (+ i 2)
                             (integer->char (bitwise-and num 255))))))
		  (loop (+ i 3) (+ j 4)))
		result))))))

(define (pem-encode x)
  (cond
   ((string? x) (pem-encode-string x))
   ((node-list? x) (pem-encode-string (data x)))
   (else (pem-encode-string (format #f "~a" x)))))

(define (pem-decode x)
  (cond
   ((string? x) (pem-decode-string x))
   ((node-list? x) (pem-decode-string (data x)))
   (else (error "not pem encoded ~a" x))))
