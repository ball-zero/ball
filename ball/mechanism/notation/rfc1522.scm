(define *quoted-printable-default-max-col* 76)

;; RFC1522 quoting for headers would need to escaping ? and _ always
(define (qp-encode str start-col max-col separator)
  (define (hex i) (integer->char (+ i (if (<= i 9) 48 55))))
  (let ((end (string-length str))
        (buf (make-string/uninit max-col)))
    (let lp ((i 0) (col start-col) (res '()))
      (cond
        ((eqv? i end)
         (if (pair? res)
	     (call-with-output-string
	      (lambda (port)
		(display (substring buf 0 col) port)
		(display separator port)
		(do ((res res (cdr res)))
		    ((null? res))
		  (display (car res) port)
		  (display separator port))))
           (substring buf start-col col)))
        ((>= col (- max-col 3))
         (lp i 0 (cons (substring buf (if (pair? res) 0 start-col) col) res)))
        (else
         (let ((c (char->integer (string-ref str i))))
           (cond
             ((and (<= 33 c 126) (not (memq c '(61 63 95))))
              (string-set! buf col (integer->char c))
              (lp (+ i 1) (+ col 1) res))
             (else
              (string-set! buf col #\=)
              (string-set! buf (+ col 1) (hex (arithmetic-shift-left c 4)))
              (string-set! buf (+ col 2) (hex (bitwise-and c #b1111)))
              (lp (+ i 1) (+ col 3) res)))))))))

(define (quoted-printable-encode-string src . rest)
  (let ((start (if (pair? rest) (car rest) 0))
	(max (if (and (pair? rest) (pair? (cdr rest)))
		 (cadr rest)
		 *quoted-printable-default-max-col*)))
    (qp-encode src start max "=\r\n")))

(define (quoted-printable-encode-header encoding src . rest)
  (apply
   (lambda (start max nl)
     (let* ((prefix (string-append "=?" encoding "?Q?"))
	    (prefix-length (+ 2 (string-length prefix)))
	    (separator (string-append "?=" nl "\t" prefix))
	    (effective-max-col (- max prefix-length)))
       (string-append prefix
		      (qp-encode src start effective-max-col separator)
		      "?=")))
   (let loop ((rest rest)
	      (defaults (list 0 *quoted-printable-default-max-col* "\r\n")))
     (if (null? rest) defaults
	 (cons (car rest) (loop (cdr rest) (cdr defaults)))))))

(define (quoted-printable-decode-string str . rest)
  (define (hex? c) (or (char-numeric? c) (<= 65 (char->integer c) 70)))
  (define (unhex1 c)
    (let ((i (char->integer c))) (if (>= i 65) (- i 55) (- i 48))))
  (define (unhex c1 c2)
    (integer->char (+ (arithmetic-shift-left (unhex1 c1) 4) (unhex1 c2))))
  (let ((mime-header? (and (pair? rest) (car rest)))
	(end (string-length str)))
    (call-with-output-string
     (lambda (port)
       (let loop ((i 0))
	 (if (< i end)
	     (let ((c (string-ref str i)))
	       (case c
		 ((#\=) ; = escapes
		  (if (< (+ i 2) end)
		      (let ((c2 (string-ref str (+ i 1))))
			(cond
			 ((eq? c2 #\newline) (loop (+ i 2)))
			 ((eq? c2 #\return)
			  (loop (if (eq? (string-ref str (+ i 2)) #\newline)
				    (+ i 3)
				    (+ i 2))))
			 ((hex? c2)
			  (let ((c3 (string-ref str (+ i 2))))
			    (if (hex? c3) (display (unhex c2 c3) port))
			    (loop (+ i 3))))
			 (else (loop (+ i 3)))))))
		 ((#\_) ; maybe translate _ to space
		  (display (if mime-header? #\space c) port)
		  (loop (+ i 1)))
		 ((#\space #\tab) ; strip trailing whitespace
		  (let loop2 ((j (+ i 1)))
		    (if (not (eqv? j end))
			(case (string-ref str j)
			  ((#\space #\tab) (loop2 (+ j 1)))
			  ((#\newline)
			   (loop (+ j 1)))
			  ((#\return)
			   (let ((k (+ j 1)))
			     (loop (if (and (< k end)
					    (eqv? #\newline (string-ref str k)))
				       (+ k 1) k))))
			  (else (display (substring str i j) port) (loop j))))))
		 (else
		  (display c port)
		  (loop (+ i 1)))))))))))
