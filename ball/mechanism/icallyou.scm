;; (C) 2000, 2001 Jörg F. Wittenberger see http://www.askemos.org

;;** The Jail

;; Sometimes it could happen that someone doesn't comply with the
;; common code.  To fight them we need a mechanism that goes beyond
;; normal rights.

;; The i-call-you! procedure does exactly that.  It's not too complicated
;; in technical terms, but very important to get that done right.

;; Reviewers, please verify that comments here tell what the code
;; does, and even more important, that you can aggree with the
;; comments, in the sense of the real world.

;; All the rights of the slave are assigned to the chief.  The slave
;; is restricted to the subright of it's own as described by "titles".

(define (i-call-you! chief slave . titles)
  (with-mind-access
   (lambda (titles)
     (with-a-ref
      chief
      (let ((chief (find-frames-by-id chief)))
	(with-a-ref
	 slave
	 (let ((slave (find-frames-by-id slave)))
	   
	   ;; To enslave (or put in jail) someone means that this person
	   ;; gets assigned to a chief, who has all the rights of the
	   ;; slave.  OK depending on actual local juristiction the chief
	   ;; might not be entitled to take away all the rights, but
	   ;; that's policy not mechanism, hence not here.
	   ;;
	   ;; To implement that within the given object system, the chief
	   ;; needs a message, which has the capabilities of the slave.
	   ;; Because the slave can't create such a message (you simply
	   ;; can't sign away all your rights by yourself), nobody can.
	   ;; You see, we need a master of the "universe", i.e., mind.

	   (if (not (dominates? (list (my-oid)) (fget chief 'capabilities)))
	       (for-each
		(lambda (f)
		  (f "i-call-you! tried by chief ~a against ~a\n"
		     (aggregate-entity chief) (aggregate-entity slave)))
		(list logerr error))
	       (logerr "i-call-you! put ~a under ~a\n"
		       (aggregate-entity slave) (aggregate-entity chief)))

	   (respond!
	    chief
	    (lambda (me msg)
	      (for-each (lambda (c) ((me 'get 'writer) capability: c))
			(msg 'capabilities)) #t)
	    respond-replicated #t
	    (make-message (property 'capabilities (fget slave 'capabilities))))

	   ;; Now that the chief got all the rights over the slave, he needs
	   ;; to force the slave to change his capabilities.

	   (respond!
	    slave
	    (lambda (me msg) ((me 'get 'writer) 'capabilities (msg 'permission)))
	    respond-replicated #t
	    (make-message
	     (property 'capabilities (fget chief 'capabilities))
	     (property 'permission titles))))))))
   titles))

;;* Grove administration.  Manipulating access points.

(define mind-xmlns-attribute
  (make-xml-attribute 'core 'xmlns namespace-mind-str))

(define mind-xmlns-attribute-list (list mind-xmlns-attribute))

(define (make-link-form name nl . more)
  (make-xml-element
   'form namespace-mind mind-xmlns-attribute-list
   (let loop ((name name) (nl nl) (more more))
     (%node-list-cons
      (make-xml-element
       'link namespace-mind
       (list (make-xml-attribute 'name #f name))
       (if nl nl (empty-node-list)))
      (if (null? more) (empty-node-list)
	  (or (and-let* (((pair? more))
			 (name (car more))
			 ((string? name))
			 ((pair? (cdr more))))
			(loop name (cadr more) (cddr more)))
	      (error "make-link-form; odd number of arguments" more)))))))

(define (make-grant-form nl capas)
  (make-xml-element
   'form namespace-mind mind-xmlns-attribute-list
   ;; BEWARE KLUDGE: a scheme list is only by accident also a node-list.
   (map (lambda (capa)
          (make-xml-element
           'grant namespace-mind '()
           (node-list (make-xml-element 'to #f '() (children nl))
                      (right->node-list capa))))
        capas)))

(define (make-new-element tree . att)
  (make-xml-element
   'new namespace-mind
   (cons mind-xmlns-attribute
         (map (lambda (a) (make-xml-attribute (car a) #f (cdr a))) att))
   tree))

;; Ancient comment to be irgnored - and digged up anyway:
;;
;; make-entry-point would be make-grove, if it would not need another
;; parameter.  I'm not sure right now what the "nl" of DSSSL's
;; make-grove is good for.  If we could resonably stick the auth info
;; in, this should be renamed.

;; TODO There is overlap with the reaction of the "" dc-identifier
;; upon host-master-request?.  Reconcile!

(: make-local-entry-point! (:quorum: string :xml-element: -> *))
(define (make-local-entry-point! quorum name nl)
  (if (entry-name->oid name)
      (raise-precondition-failed (format "~a already exists" name)))
  (and-let*
   (((if (and (eq? (gi nl) 'id)
	      (not (find-frame-by-id/quorum (string->oid (data nl)) quorum))
	      (quorum? quorum)
	      (pair? (quorum-others quorum)))
	 (find-frame-by-id-on (string->oid (data nl)) quorum)
	 #t))
    (capas (public-capabilities)))

   ;; FIXME: we better check that the place has not yet made
   ;; any transactions.

   (let* ((request
	   (make-message
	    ;; FIXME: it would be better to pass the quorum as a
	    ;; distribution-extension element rather then the usage of
	    ;; the messages 'replicates slot
	    (property 'replicates quorum)
	    (property 'capabilities capas)
	    (property 'destination '())
	    (property 'dc-creator (public-oid))
	    (property 'dc-date
		      (time-utc->date *system-time* (timezone-offset)))
	    (property 'body/parsed-xml
		      (make-xml-element
		       'request #f '()
		       (node-list (make-link-form name nl))))))
	  (result (with-a-ref (my-oid)
			      (respond! (find-frames-by-id (my-oid))
					(lambda (me msg)
					  (interpret-rw
					   me msg (children (dsssl-message-body msg))))
					respond-local #t request))))

     (let ((new-entry
	    (find-frames-by-id
	     (or (entry-name->oid name)
		 (raise (make-condition
			 &service-unavailable
			 'message
			 (format "fatal: new entry point \"~a\" doesn't exist" name)
			 'status 503))))))

       (update-*reverse-of-public-name-space*)
       (let ((id (oid->string (aggregate-entity new-entry))))
	 (set-slot! result 'body/parsed-xml
		    (make-xml-element 'id namespace-mind '() id))
	 (set-slot! result 'location (list id)))
       result))))

(: entry-point-metainfo (:quorum: string -> (or false :message:)))
(define (entry-point-metainfo quorum name)
  (let loop ((others (quorum-others quorum)))
    (and (pair? others)
	 (let ((ans ((metainfo-remote) (host-lookup* (car others)) name)))
	   (if (frame? ans) (message-body ans) (loop (cdr others)))))))

(define (support-entry-point! quorum/host name . rname)
  (let* ((quorum (if (quorum? quorum/host) quorum/host (make-quorum (list quorum/host))))
	 (ans (entry-point-metainfo
	       quorum (if (pair? rname) (car rname) name)))
	 (id (and ans
		  (attribute-string
		   'about (node-list-first
			   ((sxpath '(Description)) ans))))))
    (and id (string->oid id)
	 (make-local-entry-point!
	  quorum name (make-xml-element 'id namespace-mind '() id)))))

(define $delay-user-replication 500)

(define (make-entry-point! host secret name nl . rname)
  (let ((rname (if (pair? rname) (car rname) name))
	(quorum (make-quorum (list (or (and (oid? host) host) (string->oid host) host)))))
    (if (check-administrator-password secret)
	(if (and nl (quorum-local? quorum))
	    (make-local-entry-point! quorum name nl)
	    (!start
	     (let loop ((try 1))
	       (thread-sleep!/ms (* try $delay-user-replication))
	       (or (support-entry-point! quorum name rname)
		   (if (fx>= try 5)
		       (begin (logerr "USER ~a NOT FOUND at ~a\n" name
				      (xml-format (quorum-xml quorum)))
			      (empty-node-list))
		       (loop (add1 try)))))
	     (format "support entry ~a@~a as ~a" rname host name)))
	;; Silently fail, the request applies to another replica.
	(begin
	  (logerr " Ignoring user creation request for ~a at ~a\n"
		  name (quorum-others quorum))
	  (empty-node-list)))))

(define (disable-entry-point! place . secret)
  (let ((encoded
	 (cond
	  ((pair? secret)
	   (let ((val (car secret))
		 (mode (cdr secret)))
	     (cond
	      (else (secret-encode val (md5-digest (literal (current-date))))))))
	  (else 'disabled)))
	(place (if (aggregate? place) place
		   (let* ((whom (or (string->oid place) (entry-name->oid place)))
			  (obj (find-local-frame-by-id whom 'disable)))
		     (or obj (error "can't find" whom))))))
    (fset! place 'secret encoded)
    (commit-frame! #f place (aggregate-meta place) '())))

(define (drop-entry-point! name)
  (and
   (entry-name->oid name)
   (let* ((capas (public-capabilities))
	  (droped (respond!
		   (find-frames-by-id (my-oid))
		   (lambda (me msg)
		     (interpret-rw me msg (children (msg 'body/parsed-xml))))
		   respond-local #t
		   (make-message
		    (property 'capabilities capas)
		    (property 'destination '())
		    (property 'dc-creator (public-oid))
		    (property 'body/parsed-xml
			      (make-xml-element
			       'request #f '()
			       (node-list
				(make-link-form name (empty-node-list)))))))))
     (if droped
	 (begin
	   (update-*reverse-of-public-name-space*)
	   droped) ;(aggregate-entity droped))
	 #f))
   #t))

;; entry-name->oid returns the oid of the entry point (grove)
;; "entry-name" or #f if not found.

(define (entry-name->oid entry-name)
  (frame-resolve-link (aggregate-meta (find-frames-by-id (my-oid))) entry-name #f))

;; Somehow it's convinient to map back from oid's to the entry pont
;; name.  While it would be much better to rely on a well know
;; property defined by the object of oid (the id card equivalent),
;; which could go through all kinds of l10n, I have an ugly
;; fingerprint scheme avail here because this appears to be more
;; reliable.

(define *reverse-of-public-name-space* (make-oid-table))

(define (update-*reverse-of-public-name-space*)
  (let ((from (and (my-oid)
                   (get-slot (aggregate-meta (find-frames-by-id (my-oid)))
                             'mind-links)))
        (exclude (list null-oid one-oid (my-oid) (public-oid))))
    (set! *reverse-of-public-name-space*
          (let ((to (make-oid-table)))
            (if from (links-for-each
                      ;; string-copy here to avoid keeping persistent
                      ;; reference.
                      (lambda (k v) (hash-table-set! to v (string-copy k)))
                      from))
            to))))

(define (entry-name oid)
  (if oid
      (let ((found (hash-table-ref/default *reverse-of-public-name-space* oid #f)))
        (if found found (oid->string oid)))
      #f))
