;; (C) 2000, 2003 Jörg F. Wittenberger see http://www.askemos.org

;;* Protection Mechanism

;; TODO insert edited version of Protection description.

(define say-no (lambda args #f))
(define say-yes (lambda args #t))

(: unprotected (--> :prot:))
(define (unprotected) '())
(define (unprotected? obj) (null? obj))

;;** Universal Rights

;;*** Hirarchical, realms

(: dominates? (:prot: :capaset: --> (or boolean :capa:)))
(define (dominates? protection capas)
  (if (null? capas)
      #f
      (let recurse ((protection protection) (capas capas))
	(let next ((capas capas))
	  (if (and (pair? capas) (null? (car capas)))
	      (error "dominates?: null-capability found.\n"))

	  (let loop ((prot protection) (capa (car capas)))
	    (cond
	     ((null? capa) #t)
	     ((null? prot) (if (null? (cdr capas))
			       #f
			       (recurse protection (cdr capas))))
	     ((not (and (pair? capa) (pair? prot))) #f) ; type check, "default"
	     ((eq? (car capa) (car prot)) (loop (cdr prot) (cdr capa)))
	     ((equal? (car capa) (car prot))
	      (logerr "prot-diff ~a\n" (car capa)) (loop (cdr prot) (cdr capa)))
	     (else (if (null? (cdr capas)) #f (next (cdr capas))))))))))

;; A pile of boredome: we will frequentely need to ask (dominates?
;; (append a b) capas).  The "append" operation might be to slow.
;; Therefore we repeate the whole definition but loop once more, when
;; we reached the end of the first submist.

;; BEWARE: Usually, those lists, which we try to avoid appending here,
;; are only 1-3 elements long.  Hard to predict, whether this
;; optimisation will gain anything or might be even slower.  We
;; provide both versions and a runtime switch.  Profiling will tell,
;; which version to wipe out.

(define (dominates?2 protection1 protection2 capas)
  (if (null? capas)
      #f
      (let recurse ((protection1 protection1) (protection2 protection2) (capas capas))
	(let next ((capas capas))
	  (if (and (pair? capas) (null? (car capas)))
	      (error "dominates?: null-capability found.\n"))

	  (let loop ((prot protection1) (protection2 protection2) (capa (car capas)))
	    (cond
	     ((null? capa) #t)
	     ((null? prot)
	      (if (pair? protection2)
		  (loop protection2 '() capa)
		  (if (null? (cdr capas))
		      #f
		      (recurse protection1 protection2 (cdr capas)))))
	     ((not (and (pair? capa) (pair? prot))) #f) ; type check, "default"
	     ((eq? (car capa) (car prot)) (loop (cdr prot) protection2 (cdr capa)))
	     ((equal? (car capa) (car prot))
	      (logerr "prot-diff ~a\n" (car capa)) (loop (cdr prot) protection2 (cdr capa)))
	     (else (if (null? (cdr capas)) #f (next (cdr capas))))))))))

(: contains-strictly? (:prot: :capaset: --> (or boolean :capa:)))
(define (contains-strictly? right capas)
  (and (pair? capas)
       (or (let loop ((r right) (c (car capas)))
             (or (and (null? c) (not (null? r)))
                 (and (pair? c) (pair? r)
                      (eq? (car r) (car c)) (loop (cdr r) (cdr c)))))
           (contains-strictly? right (cdr capas)))))

;;*** conjunctive, functional rights

(: serves (:prot: :capaset: --> (or boolean :capa:)))
(define (serves function capabilities)
  ;; ... the protection is shortened.  If that's dominated by the
  ;; request we've found the service level.
  (let loop ((function function))
    (cond
     ((null? function) #f)
     ((dominates? function capabilities) function)
     (else (loop (cdr function))))))

;; Again a two parameter version.

(define (serves2 function1 function2 capabilities)
  (let loop ((function1 function1) (function2 function2))
    (cond
     ((null? function1) (if (pair? function2) (loop function2 '()) #f))
     ((dominates?2 function1 function2 capabilities)
      ;; Too bad, we can't yet get rid of the append in the match
      ;; case.  However that's just in match case.
      (append function1 function2))
     (else (loop (cdr function1) function2)))))

;;*** "Service Level" - a handy abstraction for the user level.

;; Old version, uses append, not optimised versions.  Kept for
;; reference.

;;(: make-general-service-level (:prot: :capaset: --> (procedure :prot: (or boolean :capa:))))
(define (make-general-service-level protection capabilities)
  (if (null? protection)
      ;; According to right of the first occupier we
      say-yes
      ;; see also RouseauSocialContract.
      (lambda subs
	(let ((protection (if (null? subs)
			      protection
			      (append protection subs))))
	  (if (dominates? protection capabilities)
	      #t
	      (serves protection capabilities))))))

;;(: make-general-service-level2 (:prot: :capaset: --> (procedure :prot: (or boolean :capa:))))
(define (make-general-service-level2 protection capabilities)
  (if (null? protection)
      ;; According to right of the first occupier we
      ;; see also RouseauSocialContract.
      say-yes
      (lambda subs
	(if (null? subs)
	    (if (dominates? protection capabilities)
		#t
		(serves protection capabilities))
	    (if (dominates?2 protection subs capabilities)
		#t
		(serves2 protection subs capabilities))))))

;;* Simple access control file.

;; A service level is a function, which accepts an action to be
;; performed, which together with the protection and capabilities
;; evaluates the actual right.  If the action has not been used in
;; determining the access, the result will be eq? to the protection
;; argument otherwise it will be "action".

;;(: make-acl-service-level (:prot: :capaset: --> (procedure :prot: (or boolean :capa:))))
(define (make-acl-service-level protection capabilities)
  (if (pair? protection)
      (if (pair? capabilities)
          (lambda action
	    (if (member protection capabilities)
		#t
		(and (pair? action)
		     (let loop ((a1 (car action)) (ca capabilities))
		       (cond
			((null? ca) #f)
			((member a1 (car ca)) action)
			(else (loop a1 (cdr ca))))))))
          say-no)
      say-yes))

;;* Initilization

(define $key-ring-type 'general-2)

;;(: make-service-level (:prot: :capaset: --> (procedure :prot: (or boolean :capa:))))
(define make-service-level
  (lambda (p c) (error "protection not initialized.") (lambda _ #f)))

(: init-protection (symbol -> . *))
(define (init-protection $key-ring-type)
  (set! make-service-level
        (case $key-ring-type
          ((acl) make-acl-service-level)
	  ((general-append) make-general-service-level)
	  ((general-2) make-general-service-level2)
          (else make-general-service-level2))))

(define (full-right? protection capabilities)
  ((make-service-level protection capabilities)))
