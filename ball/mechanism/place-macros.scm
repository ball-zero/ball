;; (C) 2000-2002, 2006, 2008 Jörg F. Wittenberger see http://www.askemos.org

;;* Misc

(define-macro (logagree fmt . rest)
  `(and $agree-verbose (logerr ,fmt . ,rest)))

(define-macro (logagree/effective type who on . rest)
  `(and $agree-verbose (($logagree/effective) ,type ,who ,on . ,rest)))

(define-macro (change-signal value-expr was-there-expr)
  `(let ((value ,value-expr)
         (was-there ,was-there-expr))
     (values was-there
	     value
             (cond
	      ((eq? value was-there) #f)
              ((and value was-there) 'update)
              (value 'insert)
              (was-there 'delete)))))

;;* place.scm

(define-macro (resolve-else slot properties-snapshot)
  (if (pair? slot)
      (list 'get-slot properties-snapshot slot)
      (list '%resolve-else slot properties-snapshot)))

;;** quorum.scm

(define-macro (make-ready-message request version)
  `(cons* 'ready ,request ,version))
(define-macro (make-echo-message request version)
  `(cons* 'echo ,request ,version))

(define-macro (sync-message-data? obj) `(pair? ,obj))

(define-macro (sync-message-phase message) `(car ,message))

(define-macro (sync-message-request-chks message) `(cadr ,message))

(define-macro (sync-message-serial message) `(caddr ,message))

(define-macro (sync-message-container-serial msg)
  `(case (sync-message-phase ,msg)
     ((echo ready) (caddr ,msg))
     ((one-shot) (caddr (cadr ,msg)))
     (else (error (format "Unhandled message ~a" ,msg)))))

(define-macro (sync-message-container-messages msg) `(cdr ,msg))

(define-macro (sync-message-chks message) `(cdddr ,message))

(define-macro (sync-message-version message) `(cddr ,message))

(define-macro (version-serial v) `(car ,v))

(define-macro (version-chks v) `(cdr ,v))

(define-macro (current-echo? version message)
  `(and (eq? (sync-message-phase ,message) 'echo)
	(eqv? (sync-message-serial ,message)
	      (version-serial ,version))))

(define-macro (current-ready? version message)
  `(and (eq? (sync-message-phase ,message) 'ready)
	(eqv? (sync-message-serial ,message)
	      (version-serial ,version))))

(define-macro (next-echo? version message)
  `(and (eq? (sync-message-phase ,message) 'echo)
	(eqv? (sync-message-serial ,message)
	      (add1 (version-serial ,version)))))

(define-macro (confirms-version? version message)
  `(equal? (sync-message-chks ,message) (version-chks ,version)))

(define-macro (matches-request? chksum message)
  `(equal? (sync-message-request-chks ,message) ,chksum))

(define-macro (may-match-request? chksum message)
  `(or (not (sync-message-request-chks ,message))
       (equal? (sync-message-request-chks ,message) ,chksum)))
