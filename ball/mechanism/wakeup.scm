;; ($fsm-precise #f)

;;(set! license-file "")

;;(define patch-in-zero-state 'except-replicates) ;; #f #t 'except-replicates
(define patch-in-zero-state #f) ;; #f #t 'except-replicates

(set-wake-up!
 (let ((with-defaults wake-up))
   (lambda (server)
     (lambda (message)
       ;; (define (with-mind-access-now f m) (f m))
       (logerr "Full setup (scripted)\n")
       ($use-well-known-symbols #f)
       (if (public-oid)
	   (server message)
	   (with-mind-access-now
	    (lambda (message)
	      (let* ((host (make-place! (let ((message (message-clone message)))
					  ;; TBD: Maybe we do not really have to set these here?
					  (set-slot! message 'dc-creator null-oid)
					  (set-slot! message 'mind-action-document null-oid)
					  message)))
		     (who-can-be-found-at
		      (begin (bind-place! null-oid host #t)
			     (and-let* ((secret (fget host 'secret)))
				       (fset! host 'secret (secret-encode secret
									  (md5-digest (literal (current-date))))))
			     (fset! host 'replicates (make-quorum (list (aggregate-entity host))))
			     (set-my-place! host)
			     (commit-frame! #f host (aggregate-meta host) '())))
		     (replies (server message))
		     (public
		      (let* ((names (fget (find-frames-by-id (my-oid)) 'mind-links)))
			(find-frames-by-id
			 (fold-links (lambda (k v i) (or i v)) #f names))))
		     (e (list (aggregate-entity public)
			      (aggregate-entity host)))
		     (q (make-quorum (list (aggregate-entity public)))))
		(logit display "Virgin start set up host and public.\n")
		(fset! host 'protection (cdr e))
		(let ((v0 (fget host 'version))
		      (make-frame-version cons))
		  ;; Rarely it happens that the cache was flushed and
		  ;; then the zero-version of the private does not
		  ;; start.  This is the private space, not to be
		  ;; versioned anyway.
		  (fset! host 'version (make-frame-version (add1 (version-serial v0)) (version-chks v0))))
		(fset! host 'replicates q)
		(fset! host 'capabilities (map list e))

		(if patch-in-zero-state
		    (fset! public 'capabilities (list e))
		    (fset! public 'capabilities (list (list (car e)))))

		(fset! public 'protection (list (car e)))
		(fset! public 'dc-creator (aggregate-entity host))	; ?
		(if (and patch-in-zero-state (not (eq? patch-in-zero-state 'except-replicates)))
		    (fset! public 'replicates q))
		(set-public-place! public)
		(bind-place! one-oid public #t)
		(bind-place! (aggregate-entity public) public #t)
		(commit-frame! #f public (aggregate-meta public) '())
		(commit-frame! #f host (aggregate-meta host) '())
					;		     (frame-commit! (aggregate-entity public))
					;		     (frame-commit! (aggregate-entity host))
					;		     (kludge-commit-to-safe-the-well-know-places)
		(set-wake-up! with-defaults)
		(set-local-quorum! (make-quorum (list (aggregate-entity public))))
		(http-host-add! (aggregate-entity public) (local-id) #f #t)
		(if patch-in-zero-state
		    (assert (match
			     (fget (document (public-oid)) 'capabilities)
			     (((a b)) #t)
			     (_ #f)))
		    (assert (match
			     (fget (document (public-oid)) 'capabilities)
			     (((a)) #t)
			     (_ #f))))
		(assert (match
			 (fget (document (my-oid)) 'capabilities)
			 (((a) (b)) #t)
			 (_ #f)))
		replies))
	    message))))))

(set-start-protection!
 (lambda ()
   (i-call-you! (my-oid) (public-oid) (list (public-oid) (my-oid))) ;; NOT
   ;; one-step setup Oh horror: for this questionable goal of having a
   ;; eternal "public" we need so much additional patching.
   (let ((public (find-frames-by-id (public-oid))))
     (fset! public 'replicates (make-quorum (list (aggregate-entity public))))
     (commit-frame! #f public (aggregate-meta public) '()))
   (use-mandatory-capabilities #t)
					;(kludge-commit-to-safe-the-well-know-places)
   ))
