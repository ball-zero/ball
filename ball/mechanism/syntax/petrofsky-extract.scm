(define-syntax extract
  ;; # Petrofsky Extraction
  ;;
  ;; From: Al Petrofsky
  ;; Subject: How to write seemingly unhygienic macros using syntax-rules
  ;; Date: 2001-11-19 01:23:33 PST
  ;;
  ;; Extended to extract multiple identifier by Oleg Kiselyov.
  ;; Rewritten as single annonymous syntax-rules by Jörg F. Wittenberger.
  ;;
  ;; Extract several colored identifiers from a form
  ;;    SYMB-L BODY CONT
  ;; where
  ;;
  ;; SYMB-L is the list of symbols SYMB to extract
  ;;
  ;; BODY is a form that may contain an occurence of an identifiers
  ;; that refers to the same binding occurrence as SYMB, perhaps with
  ;; a different color.
  ;;
  ;; CONT is a form of the shape (K-HEAD K-IDL . K-ARGS) where K-IDL
  ;; are K-ARGS are S-expressions representing lists or the empty
  ;; list.
  ;;
  ;; The macro expands into
  ;;
  ;;   (K-HEAD (extr-id-l . K-IDL) . K-ARGS)
  ;;
  ;; where extr-id-l is the list of extracted colored identifiers.
  ;;
  ;; License: public domain - reuse as at your discretion.
  (syntax-rules ()
    ((_ symb-l body cont)
     (let-syntax
	 (
	  ;; Extract one colored identifier from a form
	  ;;    extract SYMB BODY CONT
	  ;;
	  ;; The extract macro expands into
	  ;;   (K-HEAD (extr-id . K-IDL) . K-ARGS)
	  ;; where extr-id is the extracted colored identifier. If
	  ;; symbol SYMB does not occur in BODY at all, extr-id is
	  ;; identical to SYMB.
	 (extract
	  (syntax-rules ()
	    ((_ symb body_1 cont_1)
	     (letrec-syntax
		 ((tr
		   (syntax-rules (symb)
		     ((_ x symb tail (cont-head symb-l_1 . cont-args))
		      (cont-head (x . symb-l_1) . cont-args)) ; symb has occurred
		     ((_ d (x . y) tail cont_2)   ; if body is a composite form,
		      (tr x x (y . tail) cont_2)) ; look inside
		     ((_ d1 d2 () (cont-head  symb-l_1 . cont-args))
		      (cont-head (symb . symb-l_1) . cont-args)) ; symb does not occur
		     ((_ d1 d2 (x . y) cont_2)
		      (tr x x y cont_2)))))
	       (tr body_1 body_1 () cont_1))))))
       (let-syntax
	   ((_extract*
	     (syntax-rules ()
	       ((_ (symb) body_1 cont_1) ; only one symbol: use extract to do the job
		(extract symb body_1 cont_1))
	       ((_ _symbs _body _cont_1)
		(letrec-syntax
		    ((ex-aux			; extract symbol-by-symbol
		      (syntax-rules ()
			((_ found-symbs () body_1 cont_2)
			 (reverse () found-symbs cont_2))
			((_ found-symbs (symb . symb-others) body_1 cont_2)
			 (extract symb body_1
				  (ex-aux found-symbs symb-others body_1 cont_2)))
			))
		     (reverse	       ; reverse the list of extracted symbols
		      (syntax-rules ()	; to match the order of SYMB-L
			((_ res () (cont-head () . cont-args))
			 (cont-head res . cont-args))
			((_ res (x . tail) cont_2)
			 (reverse (x . res) tail cont_2)))))
		  (ex-aux () _symbs _body _cont_1))))))
	 (_extract* symb-l body cont))))))
