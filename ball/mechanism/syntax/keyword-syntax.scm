;; (C) 2013 Jörg F. Wittenberger - public domain

;; Thanks to Oleg Kiseljov for the real work.

;; TODO: Add a test to disallow unused keyword/value pairs to catch
;; missuse (typo's).  Currently those are ignored, which prone to
;; errors.

;;; (define-syntax let-keyword-syntax
;;;   ;;  KEYWORD-SYNTAX-NAME (POSITIONAL-OPERATOR ARG-DESCRIPTOR ...)
;;;   ;; expands into
;;;   ;;	POSITIONAL-OPERATOR ARG1 ARG2 ...
;;;   ;; where each ARG1 etc. comes either from KW-VALUE or from the
;;;   ;; default part of ARG-DESCRIPTOR. ARG1 corresponds to the first
;;;   ;; ARG-DESCRIPTOR, ARG2 corresponds to the second descriptor, etc.
;;;   ;; Here ARG-DESCRIPTOR describes one argument of the positional use
;;;   ;; case.  POSITIONAL-OPERATOR may be syntax or a first class value.
;;;   ;;
;;;   ;; ARG-DESCRIPTOR has the form 
;;;   ;;	(ARG-NAME DEFAULT-VALUE)
;;;   ;; or
;;;   ;;	(ARG-NAME)
;;;   ;; In the latter form, the default value is not given, so that the
;;;   ;; invocation of KEYWORD-SYNTAX-NAME must mention the corresponding
;;;   ;; parameter.  ARG-NAME can be anything: an identifier, a string, or
;;;   ;; even a number.
;;;   (syntax-rules ()
;;;     ((_
;;;       ((keyword-syntax-name (positional-operator (arg-name . arg-def) ...)) ...)
;;;        body ...)
;;;      (letrec-syntax
;;; 	 ((keyword-syntax-name
;;; 	   (syntax-rules ()
;;; 	     ((_ . kw-val-pairs)
;;; 	      (letrec-syntax
;;; 		  ((find 
;;; 		    (syntax-rules (arg-name ...)
;;; 		      ((find k-args (arg-name . default) arg-name
;;; 			     val . others)	   ; found arg-name among kw-val-pairs
;;; 		       (next val . k-args)) ...
;;; 		       ((find k-args key arg-no-match-name val . others)
;;; 			(find k-args key . others))
;;; 		       ((find k-args (arg-name default)) ; default must be here
;;; 			(next default . k-args)) ...
;;; 			))
;;; 		   (next			; pack the continuation to find
;;; 		    (syntax-rules ()
;;; 		      ((next val vals key . keys)
;;; 		       (find ((val . vals) . keys) key . kw-val-pairs))
;;; 		      ((next val vals)	; processed all arg-descriptors
;;; 		       (rev-apply (val) vals))))
;;; 		   (rev-apply
;;; 		    (syntax-rules ()
;;; 		      ((rev-apply form (x . xs))
;;; 		       (rev-apply (x . form) xs))
;;; 		      ((rev-apply form ()) form))))
;;; 		(next positional-operator () 
;;; 		      (arg-name . arg-def) ...)))))
;;; 	  ...)
;;;        body ...))))

;; syntactic closures don't like the above.  But they digest the following.
(define-syntax let-keyword-syntax
  (syntax-rules ()
    ((_ () body) body)
    ((_
      ((keyword-syntax-name (positional-operator (arg-name . arg-def) ...)) . more)
       body ...)
     (letrec-syntax
	 ((keyword-syntax-name
	   (syntax-rules ()
	     ((_ . kw-val-pairs)
	      (letrec-syntax
		  ((find 
		    (syntax-rules (arg-name ...)
		      ((find k-args (arg-name . default) arg-name
			     val . others)	   ; found arg-name among kw-val-pairs
		       (next val . k-args)) ...
		       ((find k-args key arg-no-match-name val . others)
			(find k-args key . others))
		       ((find k-args (arg-name default)) ; default must be here
			(next default . k-args)) ...
			))
		   (next			; pack the continuation to find
		    (syntax-rules ()
		      ((next val vals key . keys)
		       (find ((val . vals) . keys) key . kw-val-pairs))
		      ((next val vals)	; processed all arg-descriptors
		       (rev-apply (val) vals))))
		   (rev-apply
		    (syntax-rules ()
		      ((rev-apply form (x . xs))
		       (rev-apply (x . form) xs))
		      ((rev-apply form ()) form))))
		(next positional-operator () 
		      (arg-name . arg-def) ...)))))
	  )
       (let-keyword-syntax more (begin body ...))))))

