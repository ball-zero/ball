;; (C) 2000 - 2003 Jörg F. Wittenberger see http://www.askemos.org

;; The functions here are candidates to be factored out.

;; Keep this file at small size *and* heavily commented.

;; Coding rules
;; Logging
;; Really basic things like "filter"
;; Date
;; protocol implementation helpers
;; system dependant regular expression matchers

;; --

;; Many scheme implementations have a restricted version of call/cc
;; which can only be called "upwards" (as usual with most programing
;; languages).  Within this code we tell them apart (continuations
;; bound using bind-exit (named after bigloo's procedure) which MUST
;; only be called "upwards") and try not to need the full call/cc.

;; (define (bind-exit proc) (call-with-current-continuation proc))

;;** define-inline

;; This is just a hint.  Use any macro facility available to get that
;; done.  If the Scheme implementation can optimize by inlining code,
;; use those features

;; (define-macro (define-inline keyword . body) `(define ,keyword . ,body))

;;** Date
;; Hysterical reasons and some decoupling let us rename time.

(: rfc-822-time-string-format string)
(define rfc-822-time-string-format "~a, ~d ~b ~Y ~H:~M:~S ~z")

(define (rfc-822-timestring t)
  (srfi19:date->string t rfc-822-time-string-format))

(define iso-8601-time-string-format "~Y-~m-~dT~H:~M:~S.~N~z")

(define (iso-8601-timestring t)
  (srfi19:date->string t iso-8601-time-string-format))

;;** General Utility functions.

(define-condition-type &domain-error &message
  domain-error?)

(: domain-error (symbol (or symbol string) * -> *))
(define (domain-error caller message value)
  (raise (make-condition
	  &domain-error
	  'message (format "~a ~a: ~s" caller message value))))

;;*** Logging

(define log-destinations
  (vector (current-output-port) (current-error-port)))

(: log-dest-index (symbol --> fixnum))
(define (log-dest-index kind)
  (case kind
    ((output app) 0)
    ((error) 1)
    (else (error "unhandled output kind" kind))))

(define-inline (log-dest-port kind)
  (vector-ref log-destinations (log-dest-index kind)))

(: set-log-output! (symbol (or output-port string) -> undefined))
(define (set-log-output! kind where)
  (let ((idx (log-dest-index kind))
        (np (cond
             ;; TBD: ((integer? how))
             ((output-port? where) where)
             ((string? where) (open-output-file where append:))
             (else (error "set-log-output!: unhandled log destination" where)))))
    (let ((p (vector-ref log-destinations idx)))
      (vector-set! log-destinations idx np)
      (if (not (or (eq? p (current-output-port)) (eq? p (current-error-port))))
          (close-output-port p)))))

(define (log-output tag fmt args)
  (call-with-output-string
   (lambda (p)
     (display (rfc-822-timestring (current-date (timezone-offset))) p)
     (display #\space p)
     (if tag
         (begin
           (display tag p)
           (display #\space p)))
     (apply format p fmt args))))

(define (logerr fmt . args)
  (let ((p (log-dest-port 'error)))
    (display (log-output #f fmt args) p)
    (flush-output-port p)))

(define (logapperr sym fmt . args)
  (display (log-output sym fmt args) (log-dest-port 'app)))

(define (logcond-full tag title msg args)
   (logerr " ~a ~a ~a ~a\n" tag title msg args))

(define (logcond-short tag title msg args)
  (logerr "~a ~a ~a\n" tag title msg))

(define (logcond tag title msg args)
  ((if (enable-warnings) logcond-full logcond-short) tag title msg args))

(define (log-condition tag object)
  (receive
   (title text stacktrace dummy)
   (condition->fields+ object)
   (logcond tag title text stacktrace)))

(define *log-queue* (make-mailbox 'log-queue))

(define (logit . args)
  (send-message! *log-queue* args))

(define (flush-log-queue!)
  (define port (log-dest-port 'output))
  (display
   (with-output-to-string
     (lambda ()
       (let loop ()
	 (receive (task valid) (receive-message-if-present! *log-queue*)
		  (if valid 
		      (begin
			(apply (car task) (cdr task))
			(loop)))))))
   port)
  (flush-output-port port))

;; For debugging purpose we have a second one:

(define (debug lbl x) (logerr "D ~a: ~s\n" lbl x) x)

(define (default-user-debug creator args)
  (logerr "user ~a ~a:\n" creator args)
  args)

(define *user-debug* default-user-debug)

(define (set-user-debug! proc) (set! *user-debug* proc))

(define (user-debug . args) (apply *user-debug* args))

;; This will be some kind of system monitor.  We keep keyed values
;; here and in tree.scm we build a representation to be exported as
;; TrustedCode.

(define *monitored-values* (make-symbol-table))

(define (monitor-value sym val) (hash-table-set! *monitored-values* sym val))

(define *monitor-labels* (make-symbol-table))

(define (monitor-label sym str) (hash-table-set! *monitor-labels* sym str))
(monitor-label 'pipe-fd "last pipe fd")

;;*** Some realy basic things I need.

;; (define-macro (call-with-input-string str proc)
;;  `(with-input-from-string ,str ,proc))

(define (read-all-expressions . port)
  (let ((port (if (null? port) (current-input-port) (car port))))
    (let loop ((expr (read port)) (result '()))
      (if (eof-object? expr)
          (reverse! result)
          (loop (read port) (cons expr result))))))

(define (count-keys equal? key alist)
  (let loop ((n 0) (alist alist))
    (if (null? alist)
        n
        (loop (if (equal? key (caar alist)) (add1 n) n) (cdr alist)))))

;; to-string* makes a string from whatever is in the args list.  There
;; could be a better implementation...

(cond-expand
 (chicken
  (define (object->string obj)
    (cond
     ((symbol? obj) (symbol->string obj))
     ((srfi19:date? obj) (rfc-822-timestring obj))
     ((integer? obj) (clformat "~D" obj))
     ((condition? obj) (condition->string obj))
     (else (to-string obj)))))
 (rscheme
  (define (object->string obj)
    (cond
     ((symbol? obj) (symbol->string obj))
     ((srfi19:date? obj) (rfc-822-timestring obj))
     ((integer? obj) (clformat "~D" obj))
     ((condition? obj) (condition->string obj))
     (else (call-with-output-string (lambda (port) (display-object obj port))))))))

(define (to-string* args)
  (if (and (pair? args) (null? (cdr args)))
      (let ((x (car args)))
	(cond ((string? x) x)
	      ((symbol? x) (symbol->string x))
	      ((input-port? x) (read-bytes #f x))
	      (else (object->string x))))
      (let loop ((n args) (lst '()) (l 0))
        (if (null? n)
            (if (and (pair? lst) (null? (cdr lst)))
                (car lst)
                (let ((result (make-string/uninit l)))
                  (let loop ((n lst) (i (sub1 (string-length result))))
                    (if (null? n)
                        result
                        (let ((source (car n)))
                          (do ((j i (sub1 j))
                               (k (sub1 (string-length source)) (sub1 k)))
                              ((eqv? k -1) (loop (cdr n) j))
                            (string-set! result j (string-ref source k))))))))
            (let ((x (car n)))
	      (if (string? x)
		  (loop (cdr n)
			(cons x lst)
			(+ l (string-length x)))
		  (let ((d (cond
			    ((input-port? x) (read-bytes #f x))
			    (else (object->string x)))))
		    (loop (cdr n)
			  (cons d lst)
			  (+ l (string-length d))))))))))

(define (hash-table->key-vector table)
  (let ((v (make-vector (hash-table-size table))))
    (hash-table-fold table (lambda (k x i) (vector-set! v i k) (add1 i)) 0)
    v))

;;*** map-matches-alternate

;; Walk the text fragmented by matcher applying mfn and nomfn.

;; TODO now that we have the make-source example from xml/parse.scm,
;; I'd should generalize that and reuse it here for sake of speed.
;; This zillon of sustring operations is siply insane.

(define (map-matches-alternate matcher mfn nomfn text initial)
  (let ((len (string-length text)))
    (let loop ((offset 0))
      (receive*
       (start end match-text) (matcher text offset)
       (cond
        ((eqv? start offset)
         (mfn match-text (if (< end len) (loop end) initial)))
        ((and start (> start offset))
         (nomfn (substring text offset start)
                (mfn match-text (if (< end len) (loop end) initial))))
        (else (nomfn (if (eqv? offset 0)
                         text
                         (substring text offset len))
                     initial)))))))

(define (date->time-literal date)
  (let ((tm (date->time-utc date)))
    (string-append (literal-time-second (time-second tm)) "."
		   (literal-time-second (time-nanosecond tm)))))

;(define oid? number?)
;(define (oid? x) (instance? x <oid>))
(define oid? symbol?)

(define oid=? eq?)

(define make-oid-table make-symbol-table)

;; make-config-parameter
;;
;; Define a parameter intented to be set by the configuration.
;; Purpose: call out what's a config item.  Ease tp grep.
;; (Eventually probably just an alias to some general parameter
;; definition.)
(define (make-config-parameter default)
  (let ((value default))
    (lambda args
      (if (null? args) value
	  (let ((old value))
	    (set! value (car args))
	    old)))))

;;** Utilities for protocol implementations.
;; FIXME move this to mechanism/quorum.scm
(define local-id (make-config-parameter #f))

(define (not-eof-object? obj) (not (eof-object? obj)))

(define *location-format-table* (make-symbol-table))

(define (register-location-format! key fmt)        ; EXPORT
  (hash-table-set! *location-format-table* key fmt)
  key)

(define (location-format? nr)
  (hash-table-ref/default *location-format-table*
			  (or nr (error "location-format missing")) #f))

(define (location-format nr)
  (or (location-format? nr)
      (error "location-format '~a' undefined" nr)))

;;** Helpers, exported to application

;; Maybe these were better named location->read-locator ?  Format the
;; location parameter according to the location-format parameter.  The
;; latter is a pair whose car contains a function for callable
;; (synchronous) requests, the cdr contains a function for
;; asynchronous write requests.
;;
;; NOTE Almost all requests, which use http-post operations SHOULD use
;; the write-locator type.

(define (read-locator location-format-nr location)
  ((car (location-format location-format-nr)) location))

(define (write-locator location-format-nr location)
  ((cdr (location-format location-format-nr)) location))

;;** system dependant regular expression matchers

(define (strip-html-suffix str)
  (receive* (start end matched) (strip-html-suffix-regex str)
            (or (and start matched) str)))

(define (html-only-client user-agent)
  (and
   user-agent
   (or (user-agent-mozilla user-agent) (user-agent-ie user-agent))))

(define (string-sql-quote str)
  (if (sql-special-regexp str)
      (let loop ((off 0) (res '()))
        (receive
         (s . e) (sql-special-regexp str off)
         (if s
             (loop (car e) `(,(case (string-ref str s)
                          ((#\\) "\\\\")
                          ((#\') "''")
                          ;; ((#\")   "\\"" )
                          ((#\x0) "\\0" )
                          (else (error "internal error sql-quote broken")))
                       ,(substring str off s)
                       . ,res))
             (to-string* (reverse!      ; res is a fresh list
                          (cons (substring str off (string-length str))
                                res))))))
      str))

(define (sql-quote obj)
  (string-sql-quote (if (string? obj) obj (object->string obj))))

;;* TrustedCode

;; *trustedcode-map* are hash tables mappings from strings (function
;; names) to TrustedCode descriptors.  The latter are #(oid r/w
;; function) vectors, where r/w is a flag indicating whether the
;; function is idempotent or has side effect. If the latter it's to be
;; executed after a transaction.  Beware: there is no mechanism (yet)
;; to roll back the effect of trusted code if such a function raises
;; an error.  Idempotent functions may return value to the application.

(define *trustedcode-map* #f)

(define (reset-trustedcode!) (set! *trustedcode-map* (make-string-table)))

(define (register-trustedcode! oid name r/w function)
  (hash-table-set! *trustedcode-map* name (vector oid r/w function)))

(define (get-trusted-code name oid is-transaction?)
  (let ((result (hash-table-ref/default (or *trustedcode-map*
					    (error "no trusted code"))
					name #f)))
    (and result
         (eqv? (vector-ref result 0) oid)
         (or  (vector-ref result 1) is-transaction?)
         (vector-ref result 2))))

;;** external processes

(define make-temporary-directory
  (let ((n 0)
        (sem (make-mutex "make-temporary-directory"))
        (prefix (make-absolute-pathname
		 '("tmp")
		 (string-append "ball-"	(number->string (current-process-id)) "-"))))
    (lambda ()
      (with-mutex
       sem
       (set! n (add1 n))
       (call-with-output-string
         (lambda (port)
           (display prefix port)
           (display (quotient n 100) port)
           (let ((n (remainder n 100)))
             (display (quotient n 10) port)
             (display (modulo n 10) port))))))))

(define lout-tmp make-temporary-directory)

;; TODO expand/port to better API than make-process-stub, watch for
;; compatibility with other scheme implementations!  (That's the
;; problem now.)

(define (with-output-through-lout type thunk)
  (let* ((tmp (lout-tmp))
	 (tmpl (list tmp))
         (errors (make-absolute-pathname tmpl "errors"))
         (output (make-absolute-pathname tmpl "output"))
         (cmd (cons
               "lout"
               (let ((rest `("-o" ,(if (equal? type "application/pdf")
                                       "postscript" "output")
                             "-e" "errors"
                             "input")))
                 (cond ((equal? type "text/plain") (cons "-p" rest))
                       (else rest)))))
         (run-lout (lambda () (make-process-stub cmd tmp)))
         (cleanup2 (lambda ()
                     (let* ((no-errors (file-empty? errors))
                            (result (filedata (if no-errors output errors))))
                       (remove-dir-recursive tmp)
                      (if no-errors result (error result)))))
         (cleanup (lambda ()
                    (if (and (file-empty? errors)
                             (equal? type "application/pdf"))
                        (call-with-values
                            (lambda ()
                              (make-process-stub
                               `("ps2pdf" "postscript" "output")
                               tmp))
                          (lambda (p out in)
                            (close-output-port out)
                            (read in)   ; sync with eof
                            (cleanup2)))
                        (cleanup2)))))
    (mkdirs tmp)
    (with-output-to-file (make-absolute-pathname tmpl "input") thunk)
    (call-with-values
        run-lout
      (lambda (p out in)
        (close-output-port out)
        (read in)                       ; to sync only, eof expected
        (if (not (file-empty? errors))
            (call-with-values run-lout
              (lambda (p out in)
                (close-output-port out)
                (read in)         ; sync with eof
                (cleanup)))
            (cleanup))))))

(define (with-output-through-htmldoc proc)
  (call-with-temporary-directory
   (lambda (dir)
     (let ((errors (make-absolute-pathname dir "errors"))
	   (output (make-absolute-pathname dir "output")))
       (call-with-values
	   (lambda ()
	     (make-process-stub
	      `("/bin/sh" "-c"
		,(string-append
		  "htmldoc --quiet -t pdf14 --webpage -f output - 2> errors"))
	      dir))
	 (lambda (p out in)
	   (guard (ex (else #f))
		  (proc out))
	   (close-input-port in)
	   (close-output-port out)
	   (receive (p f s) (process-wait p) f)
	   (if (or (not (file-exists? errors)) (file-empty? errors))
	       (filedata output)
	       (error (filedata errors)))))))))

(define *set-child-uid* (make-config-parameter "asnobody"))

;; FIXME we better use sudo here or something?

(define (run-child-process producer consumer cmd dir . env)
  (if dir (change-file-mode dir (if (*set-child-uid*) #o777 #o700)))
  (receive (pid to-cmd from-cmd)
	   (let ((cmd (if (procedure? cmd) ;; FIXME: This case SHOULD
                                           ;; NOT be taken out again
                                           ;; to avoid fork() on
                                           ;; Windows.
                          cmd
                          (cond-expand
                           (single-binary cmd)
                           (else (if (*set-child-uid*) (cons (*set-child-uid*) cmd) cmd))))))
	     (if (pair? env)
		 (make-process-stub cmd dir (car env))
		 (make-process-stub cmd dir)))
	   (if pid
	       (let ((t (thread-start!
			 (make-thread
			  (lambda ()
			    (dynamic-wind
				(lambda () #f)
				(lambda () (producer to-cmd))
				(lambda () (close-output-port to-cmd)))
			    (receive
			     (pid2 success sig) (process-wait pid)
			     (cond
			      ((not success)
			       (let ((fail (format "command ~a failed with ~a\n" cmd sig)))
				 (logerr fail)
				 (raise fail)))
			      (else #t))))
                          ;; FIXME: DEPRECATED, SHOULD NOT be supported, see `cmd` above
			  (if (procedure? cmd) cmd (car cmd))))))
		 (dynamic-wind
		     (lambda () #f)
		     (lambda () (consumer from-cmd))
		     (lambda () (close-input-port from-cmd) (thread-join! t))))
	       (error "starting ~a failed" cmd))))

;; used in mechanism/protocol/http/webdav.scm and policy/webdav.scm
(define (string-split-last str sep) ;-> (last path)
  (let* ((len (sub1 (string-length str)))
	 (last (and (>= len 0) (eq? (string-ref str len) sep)))
	 (str (if last
		  (substring str 0 len) str))
	 (x (string-index-right str sep)))
    (if (not x)
	(values str "" last)
	(values (substring str (add1 x))
		(substring str 0 x)
		last))))
