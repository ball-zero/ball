;;; BEGIN OF DEBUG CODE

#|
(define reftbl (make-symbol-table))
(define refmux (make-mutex 'reftbl))

(define (del1 x lst)
  (let ((compare (if (pair? =) (car =) equal?)))
    (let loop ((lst lst))
      (if (pair? lst)
          (if (compare (car lst) x)
              (cdr lst)
              (cons (car lst) (loop (cdr lst))))
          lst))))

(define (refp sym t)
  (with-mutex
   refmux
   (hash-table-update!/default reftbl sym (lambda (x) (cons t x)) '())))

(define (refv sym t)
  (with-mutex
   refmux
   (hash-table-update!/remove-0 reftbl sym (lambda (x) (del1 t x)))))

(cond-expand
 (rscheme
  (define-class <hash-table-lookup-error> (<condition>)
    hash-table
    key)

  (define (hash-table-update!/remove-0 hash-table key function)
    (let* ((old (table-lookup hash-table key))
	   (input (if (table-key-present? hash-table key)
		      old
		      (raise (make <hash-table-lookup-error>
			       hash-table: hash-table key: key))))
	   (new (function input)))
      (if (null? new)
	  (table-remove! hash-table key)
	  (table-insert! hash-table key new))))

  (define-macro (with-a-ref ref b)
    `(dynamic-wind
	 (lambda () (refp ,ref (current-thread)))
	 (lambda () ,b)
	 (lambda () (refv ,ref (current-thread)))))

  (define-macro (pass-a-ref ref b name)
    `(let ((t (make-thread
	       (lambda ()
		 (dynamic-wind
		     (lambda () #t)
		     (lambda () ,b)
		     (lambda () (refv ,ref (current-thread)))))
	       ,name)))
       (refp ,ref t)
       (thread-start! t)
       (delay (thread-join! t)))))
 (else

  (define (hash-table-update!/remove-0 hash-table key function)
    (hash-table-update! hash-table key function)
    (if (null? (hash-table-ref hash-table key))
	(hash-table-delete! hash-table key)))

  (define-syntax with-a-ref
    (syntax-rules ()
      ((_ ref b)
       (dynamic-wind
	   (lambda () (refp ref (current-thread)))
	   (lambda () b)
	   (lambda () (refv ref (current-thread)))))))

  (define-syntax pass-a-ref
    (syntax-rules ()
      ((pass-a-ref ref b name)
       (let ((t (make-thread
		 (lambda ()
		   (dynamic-wind
		       (lambda () #t)
		       (lambda () b)
		       (lambda () (refv ref (current-thread)))))
		 name)))
	 (refp ref t)
	 (thread-start! t)
	 (delay (thread-join! t))))))))

(define (has-a-ref? sym)
  (pair? (hash-table-ref/default reftbl sym #f)))

(define (print-a-refs)
  (format (current-output-port) "Reference Table:\n")
  (hash-table-walk reftbl (lambda (k v) (format (current-output-port) "~a ~a\n" k v))))
|#

(cond-expand
 (rscheme
  (define-macro (with-a-ref ref b) b)
  (define-macro (pass-a-ref ref b name) `(!order ,b ,name)))
 (else
  (define-syntax with-a-ref
    (syntax-rules ()
      ((_ ref b) b)))
  (define-syntax pass-a-ref
    (syntax-rules ()
      ((pass-a-ref ref b name) (future b))))))

(define (has-a-ref? sym) #f)
(define (print-a-refs) (values))

;;; END OF DEBUG CODE

;;* Properties

;; Remark to FramerD
;;
;; One of the most interesting features of FramerD is it's ability to
;; handle sets of objects as if they where single objects.  This is:
;; a) powerful
;; b) dangerous some times
;; c) can hide relevant complexity

;;

;; Property set, frame, environment or object (e.g., in Python) --
;; these are all different names for the very same concept.  It goes
;; back to Aristoteles to understand every perception (concept) as a
;; group of parameters (properties).  Objects can be "classified",
;; that is they are understood to be an instance of such a concept, if
;; they have all the properties of that group (conformance, he
;; distingishes "form", as syntactic form within properties, from
;; "material", as values of the properties).  We call the conceptual
;; group(object) the class while the collection of actual values is
;; the object.

;; Don't be fooled by the babylonian multitude of names which
;; historically emerged!  All programming languages _must_ implement
;; that concept of property grouping and a method to identifying an
;; object.  Otherwise they would be hopelessly useless as they drop 3k
;; years of the most valuable history of man kind.

;; The most simple implementation is called "association list".  It is
;; just enough to express the concept and therefore also the most
;; flexible.  But it requires a linear search for properties, which is
;; slow.  Therefore we use SRFI-9 records for most (meta) system
;; defined properties.  Additionally we keep one slot reserved for
;; other properties in an assoc list.  This might be used from the
;; denotational system (often called user code), though it's not
;; recommented, because there is no guaranties about those properties.

;; BTW: Oleg has later on brushed up and commented those environments
;; http://okmij.org/ftp/Scheme/util.html#env .  This is not actually
;; rocket since, but he has some nice ideas how the API could look
;; alike (beware it's not thread safe) .  Close by find a lot of XML
;; stuff to reincorporate.

;;*** USAGE

;; A property is just an association of two entities, one serves as the name
;; the second as value.
;; WARNING: never ever rely on this internal presentation!

; (define-macro (property name value) `(cons ,name ,value))

; (define-macro (property? x) `(pair? ,x))

;; Create an initial property set with some properties preset.
;; Hopefully we never need to add something.
;; All the properties should have been created with the property
;; procedure above.
;; EXAMPLE
;; (make-property-set (property 'a (+ 2 3)) (property 'b (cons 2 3)))

(: properties-get (symbol :propertyset: --> *))
(: properties-set (:propertyset: &rest :property: --> :propertyset:))
(: make-property-set (&rest :property: --> :propertyset:))
(: make-property-set+ (&rest :property: -> :propertyset:))
(cond-expand
 (use-vector+assoc-as-frame
  (define (make-property-set . properties) properties)

  ;; properties-set: create a new set of properties with those bindings
  ;; replaced.

  (: %replace-association (:property: :propertyset: --> :propertyset:))
  (: %remove-association (symbol :propertyset: --> :propertyset:))
  (define (%replace-association pair alist)
    (bind-exit
     (lambda (return)
       (let loop ((key (property-name pair)) (rest alist))
	 (cond
	  ((null? rest) (return (cons pair alist)))
	  ((eq? key (caar rest)) (if (eq? (property-value pair) (cdar rest))
				     (return alist)
				     (cons pair (cdr rest))))
	  (else (cons (car rest) (loop key (cdr rest)))))))))

  (define (%remove-association key alist)
    (bind-exit
     (lambda (return)
       (let loop ((rest alist))
	 (cond
	  ((null? rest) (return alist))
	  ((eq? key (caar rest)) (cdr rest))
	  (else (cons (car rest) (loop (cdr rest)))))))))

  ;; This could be made smarter, see the "set"-handling in "state-env"
  ;; of rdp.scm.  We don't do that for now.  Let's investigate the
  ;; performance issue here.

  (define (properties-set properties . newprop)
    (do ((rest newprop (cdr rest))
	 (result properties (%replace-association (car rest) result)))
	((null? rest) result)))

  ;; linear update variant.  For safety defined as the side effect free
  ;; version.

					;(define (properties-set+ properties . newprop)
					;  (apply properties-set properties newprop))

  (define properties-set+ properties-set)

  ;; The basic lookup operation on properties.  Be very careful with the
  ;; result value!  This is NOT to be exported!

  (define (properties-get key properties) (assq key properties))

  (define property-set-fold fold)

  ) (else


     (define (property name value)
       (make-property #f #f #f name value))

     (cond-expand
      (chicken)

      (else

       (define-record-type <property>
	 (make-property color left right name value)
	 property?
	 (color property-color property-color-set!)
	 (left property-left property-left-set!)
	 (right property-right property-right-set!)
	 (name property-name property-name-set!)
	 (value property-value property-value-set!))

       (define-llrbtree-code
	 (ordered pure)
	 ;;        ((node . args)
	 ;; 	`(let ((node ,node))
	 ;; 	   . ,(let loop ((args args))
	 ;; 		(if (null? args)
	 ;; 		    '(node)
	 ;; 		    (cons
	 ;; 		     (case (car args)
	 ;; 		       ((color:) `(property-color-set! node ,(cadr args)))
	 ;; 		       ((left:) `(property-left-set! node ,(cadr args)))
	 ;; 		       ((right:) `(property-right-set! node ,(cadr args)))
	 ;; 		       (else (error  (format "unbrauchbar ~a" args))))
	 ;; 		     (loop (cddr args)))))))

	 ((node . args)
	  (let ((other-slots '(property-name property-value))
		(tree-slots '((color: property-color)
			      (left: property-left)
			      (right: property-right))))
	    (define (updater e)
	      (let ((c (memq (car e) args)))
		(if c (cadr c)
		    `(,(cadr e) ,node))))
	    (define (copier acc)
	      `(,acc ,node))
	    `(make-property
	      ,@(map updater tree-slots)
	      ,@(map copier other-slots))))
	 property-set-init!	           ;; defined
	 property-lookup			   ;; defined
	 #f				   ;; no min defined
	 property-set-fold		   ;; defined
	 #f				   ;; no for-each defined
	 property-node-insert!		   ;; defined
	 property-delete!			   ;; defined
	 #f				   ;; no delete-min defined
	 property-set-empty?	   ;; defined
	 ((k n) `(eq? ,k (property-name ,n)))
	 ((k n) `(string<? (symbol->string ,k) (symbol->string (property-name ,n))))
	 ((node1 node2) `(string<? (symbol->string (property-name ,node1))
				   (symbol->string (property-name ,node2))))
	 property-left
	 property-left-set!
	 property-right
	 property-right-set!
	 property-color
	 property-color-set!
	 #f)))

  (define (make-property-set . properties)
    (do ((properties properties (cdr properties))
	 (set (property-set-init! (make-property #f #f #f #f #f))
	      (property-node-insert!
	       set (property-name (car properties)) (car properties))))
	((null? properties) set)))

  ;; properties-set: create a new set of properties with those bindings
  ;; replaced.

  (: %replace-association (:property: :propertyset: --> :propertyset:))
  (define (%replace-association property set)
    (property-node-insert! set (property-name property) property))

  (: %remove-association (symbol :propertyset: --> :propertyset:))
  (define (%remove-association key set)
    (property-delete! set key))

  ;; This could be made smarter, see the "set"-handling in "state-env"
  ;; of rdp.scm.  We don't do that for now.  Let's investigate the
  ;; performance issue here.

  (define (properties-set properties . newprop)
    (do ((newprop newprop (cdr newprop))
	 (properties
	  properties
	  (property-node-insert! properties (property-name (car newprop)) (car newprop))))
	((null? newprop) properties)))

  ;; linear update variant.  For safety defined as the side effect free
  ;; version.

  (define properties-set+ properties-set)

  ;; The basic lookup operation on properties.  Be very careful with the
  ;; result value!  This is NOT to be exported!

  (define (properties-get key properties) (property-lookup properties key))

  ))


;;* Aggregate

;; A specialized pair we'll have to talk about.  Most of the time in
;; comments, since I'm using 'cons' via place-macros.scm

;; (define-record-type <aggregate>
;;   (aggregate meta entity)
;;   aggregate?
;;   (meta aggregate-meta
;;         ;; 'set-aggregate-meta!' is only to be used to commit (see
;;         ;; ACID) a ProcessStep.  Porting remark: we rely by definition
;;         ;; on that one beeing atomic.
;;         set-aggregate-meta!
;;         )
;;   (entity aggregate-entity
;;           ;; There is nothing stranger than 'set-aggregate-entity!'.
;;           ;; It modifies the identity of a given object.  The
;;           ;; "identity morphed" vanishs and the postulated new entity
;;           ;; enters reality. Such a construction is only required to
;;           ;; solve the chichen and egg problem.  Therefor BEWARE, only
;;           ;; the wakeup proccess may ever use it!
;;           set-aggregate-entity!         ; private, see above
;;           ))

;;* Frame Pools

;; We use the same data structure for the frame (place
;; representation), the change set a ProcessStep creates and messages
;; flowing around.  The mutation procedures ("set-.*!") are considered
;; private to this module.

;; TODO: we should update this code to use SRFI-57 for the sake of
;; readability and robustness.


(: allocate-frame
   (:frame-version:
    (or false :quorum:)
    (or false :reply-channel:)
    :linkset:
    (or false string) ; content-type
    :body-plain:
    (or false :xml-nodelist:) ;; body/xml
    :prot:
    :capaset:
    :propertyset: ;; other
    --> :message:))

(: frame-version (:message: --> :frame-version:))
(: set-frame-version! (:message: :frame-version: -> undefined))
(: frame-replicates (:message: --> (or false :quorum:)))
(: set-frame-replicates! (:message: :quorum: -> undefined))
(: frame-reply-channel (:message: --> (or false :reply-channel:)))
(: set-frame-reply-channel! (:message: :reply-channel: -> undefined))
(: frame-links (:message: --> :linkset:))
(: set-frame-links! (:message: :linkset: -> undefined))
(: frame-content-type (:message: --> (or false string)))
(: set-frame-content-type! (:message: (or false string) -> undefined))
(: frame-body/plain (:message: --> :body-plain:))
(: set-frame-body/plain! (:message: :body-plain: -> undefined))
(: frame-body/xml (:message: --> (or false :xml-nodelist:)))
(: set-frame-body/xml! (:message: (or false :xml-nodelist:) -> undefined))
(: frame-protection (:message: --> :prot:))
(: set-frame-protection! (:message: :prot: -> undefined))
(: frame-capabilities (:message: --> :capaset:))
(: set-frame-capabilities! (:message: :capaset: -> undefined))
(: frame-other (:message: --> :propertyset:))
(: set-frame-other! (:message: :propertyset: -> undefined))

(: make-frame-changeset (:message: --> :message:))

(: make-frame ((or :message: (list-of :property:)) --> :message:))

(cond-expand

((or use-record+llrbtree-as-frame use-vector+assoc-as-frame)

;; use-vector+assoc-as-frame is used as reference implementation.  It
;; used to be the first one historically.  Basically it's an obvious
;; optimisation to assoc lists if certain keys are known to be in
;; frequent use.
;;
;; Advantage: In the early days, when the pstore facility had many
;; bugs, it was extraordinary important to proove code beeing pure,
;; referencial transparent.
;;
;; Disadvantage: This data structure, especially in combination with
;; pure, referential transparent updates, as implemented, is slow.
;;
;; Experience: In reality it's not that bad: we ran applications on
;; top of that data structure for 3 years and always got good
;; performance reports from outside developers.  But it can be done
;; better: the next alternative will use "environments" however they
;; are defined in the host Scheme system.  Usually those are hash
;; tables.

(define-record-type <frame>
  ;; Our frame pools handle a few slots special for the sake of
  ;; efficiency.
  (allocate-frame                       ; private
   version
   replicates
   reply-channel
   links
   content-type
   body/plain
   body/xml
   protection
   capabilities
   other)
  frame?
  (version frame-version set-frame-version!)
  (replicates frame-replicates set-frame-replicates!)
  (reply-channel frame-reply-channel set-frame-reply-channel!)
  (links frame-links set-frame-links!)
  (content-type frame-content-type set-frame-content-type!)
  (body/plain frame-body/plain set-frame-body/plain!)
  (body/xml frame-body/xml set-frame-body/xml!)
  (protection frame-protection set-frame-protection!)
  (capabilities frame-capabilities set-frame-capabilities!)
  (other frame-other set-frame-other!))

;; We jump one hop to be sure there is a syntax check for the
;; 'new-frame' operator we use from here.  Do NOT use 'allocate-frame'
;; anywhere else!  'make-frame' is the official constructor for
;; frames.

;; (define-macro
;;   (new-frame
;;    version
;;    replicates
;;    proposal
;;    links
;;    content-type
;;    body/plain
;;    body/xml
;;    other)
;;   (list 'allocate-frame
;;         version
;;         replicates
;;         proposal
;;         links
;;         content-type
;;         body/plain
;;         body/xml
;;         other))

) (use-env-as-frame

;; use-env-as-frame compiles frames as environments as provided by the
;; host Scheme system.  This ought to be the fastes way.
;;
;; Disadvantage: we can hardly proof referential transparency.  When
;; in doubt switch to the reference implementation!

;; FIXME: doesn't yet compile under rscheme

;; (define-macro
;;   (new-frame
;;    version
;;    replicates
;;    proposal
;;    links
;;    content-type
;;    body/plain
;;    body/xml
;;    other)
;;   (let ((envt (gensym)))
;;     `(let ((,envt (make-environment)))
;;        (environment-extend! ,envt 'version ,version)
;;        (environment-extend! ,envt 'replicates ,replicates)
;;        (environment-extend! ,envt 'proposal ,proposal)
;;        (environment-extend! ,envt 'links ,links)
;;        (environment-extend! ,envt 'content-type ,content-type)
;;        (environment-extend! ,envt 'body/plain ,body/plain)
;;        (environment-extend! ,envt 'body/xml ,body/xml)
;;        (for-each (lambda (p)
;;                    (environment-extend! ,envt (car p) (cdr p)))
;;                  ,other)
;;        ,envt)))

(define frame? environment?)

;; This is rscheme (TODO: move the code over there) and not the
;; fastest thing possible yet.  However it's going to ease useage, I
;; hope.

(define-macro (%environment-value e k)
  `(and (environment-ref ,e ,k) (value (environment-ref ,e ,k))))

(define (frame-version frame)
  (%environment-value frame 'version))
(define (set-frame-version! frame version)
  (environment-extend! frame 'version version))

(define (frame-replicates frame)
  (%environment-value frame 'replicates))
(define (set-frame-replicates! frame replicates)
  (environment-extend! frame 'replicates replicates))

(define (frame-reply-channel frame)
  (%environment-value frame 'proposal))
(define (set-frame-reply-channel! frame proposal)
  (environment-extend! frame 'proposal proposal))

(define (frame-links frame)
  (%environment-value frame 'links))
(define (set-frame-links! frame links)
  (environment-extend! frame 'links links))

(define (frame-content-type frame)
  (%environment-value frame 'content-type))
(define (set-frame-content-type! frame content-type)
  (environment-extend! frame 'content-type content-type))

(define (frame-body/plain frame)
  (%environment-value frame 'body/plain))
(define (set-frame-body/plain! frame body/plain)
  (environment-extend! frame 'body/plain body/plain))

(define (frame-body/xml frame)
  (%environment-value frame 'body/xml))
(define (set-frame-body/xml! frame body/xml)
  (environment-extend! frame 'body/xml body/xml))

(define (frame-other frame)
  (%environment-value frame 'other))
(define (set-frame-other! frame other)
  (environment-extend! frame 'other other))

(define (make-frame-changeset frame)
  (environment-copy frame))

)(use-hashtable-as-frame

;; use-env-as-frame compiles frames as environments as provided by the
;; host Scheme system.  This ought to be the fastes way.
;;
;; Disadvantage: we can hardly proof referential transparency.  When
;; in doubt switch to the reference implementation!

;; FIXME: coesn't yet compile under rscheme

;; (define-macro
;;   (new-frame
;;    version
;;    replicates
;;    proposal
;;    links
;;    content-type
;;    body/plain
;;    body/xml
;;    other)
;;   (let ((envt (gensym)))
;;     `(let ((,envt (make-symbol-table)))
;;        (hash-table-set! ,envt 'version ,version)
;;        (hash-table-set! ,envt 'replicates ,replicates)
;;        (hash-table-set! ,envt 'proposal ,proposal)
;;        (hash-table-set! ,envt 'links ,links)
;;        (hash-table-set! ,envt 'content-type ,content-type)
;;        (hash-table-set! ,envt 'body/plain ,body/plain)
;;        (hash-table-set! ,envt 'body/xml ,body/xml)
;;        (for-each (lambda (p)
;;                    (hash-table-set! ,envt (car p) (cdr p)))
;;                  ,other)
;;        ,envt)))

(define <frame> <symbol-table>)

(define (frame? obj)
  (instance? obj <symbol-table>))

(define (frame-version frame)
  (hash-table-ref frame 'version))
(define (set-frame-version! frame version)
  (hash-table-set! frame 'version version))

(define (frame-replicates frame)
  (hash-table-ref frame 'replicates))
(define (set-frame-replicates! frame replicates)
  (hash-table-set! frame 'replicates replicates))

(define (frame-reply-channel frame)
  (hash-table-ref frame 'proposal))
(define (set-frame-reply-channel! frame proposal)
  (hash-table-set! frame 'proposal proposal))

(define (frame-links frame)
  (hash-table-ref frame 'links))
(define (set-frame-links! frame links)
  (hash-table-set! frame 'links links))

(define (frame-content-type frame)
  (hash-table-ref frame 'content-type))
(define (set-frame-content-type! frame content-type)
  (hash-table-set! frame 'content-type content-type))

(define (frame-protection frame)
  (hash-table-ref/default frame 'content-type (unprotected)))
(define (set-frame-content-type! frame protection)
  (hash-table-set! frame 'content-type protection))

(define (frame-content-type frame)
  (hash-table-ref frame 'content-type))
(define (set-frame-content-type! frame content-type)
  (hash-table-set! frame 'content-type content-type))

(define (frame-body/plain frame)
  (hash-table-ref frame 'body/plain))
(define (set-frame-body/plain! frame body/plain)
  (hash-table-set! frame 'body/plain body/plain))

(define (frame-body/xml frame)
  (hash-table-ref frame 'body/xml))
(define (set-frame-body/xml! frame body/xml)
  (hash-table-set! frame 'body/xml body/xml))

(define (frame-other frame)
  (hash-table-ref frame 'other))
(define (set-frame-other! frame other)
  (hash-table-set! frame 'other other))

(define (make-frame-changeset frame)
  (let ((result (hash-table-copy frame)))
    (hash-table-delete! result 'proposal)
    result))

)(else (error 'no-frame-representation-selected)))

(cond-expand
 (use-vector+assoc-as-frame

 ;; This seems the moment, where I get tired of the record type syntax.
 ;; It's flexible, yet, and tedious.

 (define (make-frame-changeset frame)
   (new-frame
    (frame-version frame)
    (frame-replicates frame)
    (frame-reply-channel frame)
    (frame-links frame)
    (frame-content-type frame)
    (frame-body/plain frame)
    (frame-body/xml frame)
    (frame-protection frame)
    (frame-capabilities frame)
    (frame-other frame)))

) (use-record+llrbtree-as-frame

 (define (make-frame-changeset frame)
   (new-frame
    (frame-version frame)
    (frame-replicates frame)
    (frame-reply-channel frame)
    (frame-links frame)
    (frame-content-type frame)
    (frame-body/plain frame)
    (frame-body/xml frame)
    (frame-protection frame)
    (frame-capabilities frame)
    (frame-other frame)))

) (else (error 'no-frame-representation-selected)))

;; Aggregate access

(define (%set-slot! frame slot value)
  (case slot
    ((version) (set-frame-version! frame value))
    ((replicates) (set-frame-replicates! frame value))
    ((reply-channel)
     (set-frame-reply-channel! frame value))
    ((mind-links) (set-frame-links! frame value))
    ((content-type) (set-frame-content-type! frame value))
    ((mind-body)
     ;; (if (a:blob? (frame-body/plain frame))
     ;; 	 (unregister-blob! (frame-body/plain frame) frame))
     ;; (if (a:blob? value)
     ;; 	 (register-blob! (frame-body/plain value) frame))
     (set-frame-body/plain! frame value))
    ((body/parsed-xml) (set-frame-body/xml! frame value))
    ((protection) (set-frame-protection! frame value))
    ((capabilities) (set-frame-capabilities! frame value))
    (else (set-frame-other!
           frame
           (if value
               (%replace-association (property slot value) (frame-other frame))
               (%remove-association slot (frame-other frame)))))))

(define (%get-slot frame slot)
  (case slot
   ((version) (frame-version frame))
   ((replicates) (frame-replicates frame))
   ((reply-channel) (frame-reply-channel frame))
   ((mind-links) (frame-links frame))
   ((content-type) (frame-content-type frame))
   ((mind-body) (frame-body/plain frame))
   ((body/parsed-xml) (frame-body/xml frame))
   ((protection) (frame-protection frame))
   ((capabilities) (frame-capabilities frame))
   (else (let ((v (properties-get slot (frame-other frame))))
           (and v (property-value v))))))

;;** Link Table

(cond-expand
 (use-hashtable-for-links

  (define (make-mind-links) (make-string-table))

  (define (frame-resolve-link frame name default)
    (and-let* ((links (frame-links frame)))
	      (hash-table-ref/default links name default)))

  (define (resolve-mind-links frame)
    (let ((links (frame-links frame)))
       (if links (key-sequence links) '())))

  (define (make-link-folder frame)
    (let ((table (frame-links frame)))
      (lambda (combine init)
	(if table
	    (hash-table-fold table (lambda (k v i) (combine k i)) init)
	    init))))

  (define (fold-links f i l) (hash-table-fold l f i))

  (define (fold-links-sorted f init table)
    ;; vector-sort! works on a fresh vector here.
    (let ((links (vector-sort! (hash-table->key-vector table) string<?)))
      (let loop ((init init) (i 0))
	(if (eqv? i (vector-length links))
	    init
	    (loop
	     (let ((n (vector-ref links i)))
	       (f n (hash-table-ref/default table n #f) init))
	     (add1 i))))))

  (define (make-link-folder-ascending frame)
    (let ((table (frame-links frame)))
      (lambda (combine init)
	(if table
	    (fold-links-sorted combine init table)
	    init))))

  (define (links-for-each proc links)
    (hash-table-walk proc links))

  (define link-bind! hash-table-set!)

  (define (link-ref links name) (hash-table-ref/default links name #f))

  (define link-delete! hash-table-delete!)

  (define links-merge! hash-table-merge!)

  )
 (else

  (define (make-mind-links) (make-string-ordered-llrbtree))

  (define (frame-resolve-link frame name default)
    (if (string? name)
	(let ((links (frame-links frame)))
	  (if links
	      (string-ordered-llrbtree-ref/default links name default)
	      default))
	(raise (format "frame-resolve-link ~a\n" name))))

  (define (make-link-folder properties-snapshot)
    (let ((table (frame-links properties-snapshot)))
      (lambda (combine init)
	(if table
	    (let ((c (lambda (k v i) (combine k i))))
	      (string-ordered-llrbtree-fold c init table))
	    init))))


  (define make-link-folder-ascending make-link-folder)

  (define (resolve-mind-links frame)
    (let ((table (frame-links frame)))
      (if table (string-ordered-llrbtree-fold (lambda (k v i) (cons k i)) '() table) '())))

  (define fold-links string-ordered-llrbtree-fold)

  (define fold-links-sorted string-ordered-llrbtree-fold)

  (define links-for-each string-ordered-llrbtree-for-each)

  (define link-bind! string-ordered-llrbtree-set!)

  (define (link-ref links name)
    (if (string? name)
	(string-ordered-llrbtree-ref/default links name #f)
	(raise (format "link-ref ~a\n" name))))

  (define link-delete! string-ordered-llrbtree-delete!)

  (define (links-merge! to from)
    (string-ordered-llrbtree-for-each
     (lambda (k v) (string-ordered-llrbtree-set! to k v))
     from))

  ))


;; frames

(define frame-clone make-frame-changeset)

;; Create a new frame.  This is the official constructor.

(define make-frame
  (let ((null-version '(0 . "indeed")))
    (define (make-frame args)             ; EXPORT
      (cond
       ((null? args)
	(let ((r (new-frame null-version
			    #f		; quorum
			    #f		; reply-channel
			    #f		; links
			    #f		; content-type
			    #f		; body/plain
			    #f		; body/xml
			    (unprotected) ; protection
			    '()		; capabilities
			    (make-property-set))))
	  ;; (cond-expand
	  ;;  (rscheme (register-for-finalization r))
	  ;;  (chicken (set-finalizer! r finalize-frame!)))
	  r))
       ((frame? (car args)) (car args))
       (else
	;; There should be a syntax for sorting assoc lists into
	;; positioned parameters.
	;;
	;; In general, we would pre-allocate the frame and use side effect
	;; to set the slots.  I just don't want to do that here for
	;; didactical reasons.  This is the most basic point where memory
	;; is allocated for a local projection of an abstract Askemos
	;; place and I want to kill the argument, that this can't be done
	;; in pure functional way.
	;;
	;; However I admit, that I lack a consise _syntax_ to in Scheme.
	(let loop ((args args)
		   (version null-version)
		   (replicates #f)
		   (reply-channel #f)
		   (links #f)
		   (content-type #f)
		   (body/plain #f)
		   (body/xml #f)
		   (protection (unprotected))
		   (capabilities '())
		   (value (make-property-set)))
	  (if (null? args)
	      (let ((r (new-frame
			version
			replicates
			reply-channel
			links
			content-type body/plain body/xml
			protection capabilities
			value)))
		;; (if (a:blob? body/plain)
		;; 	(register-blob! body/plain r))
		;; (cond-expand
		;;  (rscheme (register-for-finalization r))
		;;  (chicken (set-finalizer! r finalize-frame!)))
		r)
	      (case (property-name (car args))
		((version)
		 (loop (cdr args) (property-value (car args)) replicates reply-channel
		       links content-type body/plain body/xml protection capabilities value))
		((replicates)
		 (loop (cdr args) version (property-value (car args)) reply-channel
		       links content-type body/plain body/xml protection capabilities value))
		((reply-channel)
		 (loop (cdr args) version replicates (property-value (car args))
		       links content-type body/plain body/xml protection capabilities value))
		((mind-links)
		 (loop (cdr args) version replicates reply-channel
		       (property-value (car args)) content-type body/plain body/xml protection capabilities value))
		((content-type)
		 (loop (cdr args) version replicates reply-channel
		       links (property-value (car args)) body/plain body/xml protection capabilities value))
		;; BEWARE: The next two should have been
		;; checked that they match each other.  They
		;; are supposed to provide the cached result
		;; of parsing/quotienting of the other one.
		((mind-body)
		 (loop (cdr args) version replicates reply-channel
		       links content-type (property-value (car args)) body/xml protection capabilities value))
		((body/parsed-xml)
		 (loop (cdr args) version replicates reply-channel
		       links content-type body/plain (property-value (car args)) protection capabilities value))
		((protection)
		 (loop (cdr args) version replicates reply-channel
		       links content-type body/plain body/xml (property-value (car args)) capabilities value))
		((capabilities)
		 (loop (cdr args) version replicates reply-channel
		       links content-type body/plain body/xml protection (property-value (car args)) value))
		(else
		 (loop (cdr args) version replicates reply-channel
		       links content-type body/plain body/xml protection capabilities
		       (properties-set value (car args))))))))))
    make-frame))
;; Conditions

(define-condition-type &http-effective-condition &message
  http-effective-condition?
  (status http-effective-condition-status))

(define-condition-type &service-unavailable &http-effective-condition
  service-unavailable-condition?)

(define (timeout-condition? obj)
  (or (timeout-object? obj)
      (and-let* (((http-effective-condition? obj))
		 (status (http-effective-condition-status obj)))
		(or (eq? status 408) (eq? status 503)))
      (agreement-timeout? obj)
      (and-let* (((frame? obj))
		 (status (get-slot obj 'http-status)))
		(or (eq? status 408) (eq? status 503)))))

(define (raise-service-unavailable-condition str)
  (raise (make-condition &service-unavailable
			 'message str
			 'status 503)))

(define-condition-type &unauthorised &http-effective-condition
  unauthorised-condition?
  (resource unauthorised-condition-resource)
  (source unauthorised-condition-source))

(define-condition-type &forbidden &http-effective-condition
  forbidden-condition?
  (resource forbidden-condition-resource)
  (source forbidden-condition-source))

(define (raise-precondition-failed message)
  (raise (make-condition &http-effective-condition 'status 412 'message message)))

(define-condition-type &agreement-timeout &condition
  agreement-timeout?
  (ready agreement-ready)
  (result agreement-result)
  (digest agreement-digest)
  (required-echos agreement-required-echos)
  (uninformed agreement-uninformed)
  (required agreement-required)
  (missing agreement-missing))

;;** WebDAV very low level

(define (dav:accept-propertyupdate current-properties propertyupdate)
  (make-xml-element
   'prop namespace-dav
   (xml-element-attributes (node-list-first current-properties))
   (node-list
    ((xml-walk* #f #f 'no-root #f '()'() #f
		(children current-properties)
		(lambda args #f))
     (fold
      (lambda (pattern rest)
	(if (xml-element? pattern)
	    (cons
	     (cons
	      (lambda (node root ns-binding)
		(and (eq? (gi node) (gi pattern))
		     (eq? (ns node) (ns pattern))))
	      xml-drop)
	    rest)
	    rest))
      (list (cons sxp:element? xml-copy))
      ((sxpath '(* * *)) propertyupdate)))
    (children (children (select-elements
			 (children propertyupdate) 'set))))))
