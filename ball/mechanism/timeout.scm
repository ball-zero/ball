;; (C) 2003, 2008 Joerg F. Wittenberger see http://www.askemos.org

;;** bounded execution time

;; Remark: This mailbox+timeout combination has made it into chicken.
;; We should incorporate those
;; http://www.call-with-current-continuation.org/eggs/mailbox2.html
;; changes and unify the API.

;;*** prerequisities

;; Application must run a thread ``watchdog''.  Every second it scans
;; through a queue for expired events and either sends a timeout
;; object to the mailbox or delivers an exception withtin the thread
;; which computes thunk.

;;*** exports

;; User:
;; (with-timeout timeout thunk)
;; (register-timeout-message! time mbox)
;;
;; System
;;
;; thunk is called, when the parent process is missing.
;; (make-watchdog thunk)

;;*** R5RS implementation
;; (define-macro (make-entry time obj)
;;   `(vector '() '() ,time ,obj))
;; (define-macro (entry-successor obj) `(vector-ref ,obj 0))
;; (define-macro (entry-predecessor obj) `(vector-ref ,obj 1))
;; (define-macro (entry-time obj) `(vector-ref ,obj 2))
;; (define-macro (entry-value obj) `(vector-ref ,obj 3))
;; (define-macro (entry-set-succ! obj succ) `(vector-set! ,obj 0 ,succ))
;; (define-macro (entry-set-pred! obj pred) `(vector-set! ,obj 1 ,pred))

;; (define *local-timeout-symbol* '(timeout))

;;*** rscheme implementation with ordered linerar list

(define-macro (within-time tm . body) `(with-timeout ,tm (lambda () . ,body)))
(define-macro (within-time! tm . body) `(with-timeout! ,tm (lambda () . ,body)))

;; KLUDGE: this is a bit risky: if <body> raises an exception, the
;; timout may kick in later.
(define-macro (within-time%1 tm . body)
  (let ((%%val (gensym))
	(%%tmo (gensym)))
    `(let* ((,%%tmo (register-timeout-message! ,tm (current-thread)))
	    (,%%val (begin . ,body)))
       (cancel-timeout-message! ,%%tmo)
       ,%%val)))


(cond-expand
 ;; WARNING: double-linked-list is untested now.
 (double-linked-list
  (define-class <queue-entry> (<object>)
    entry-successor
    entry-predecessor
    entry-time
    entry-value)

  (define-macro (make-entry time obj)
    `(make <queue-entry>
       entry-successor: #f
       entry-predecessor: #f
       entry-time: ,time
       entry-value: ,obj))

  (define-macro (entry-set-succ! obj succ) `(set-entry-successor! ,obj ,succ))
  (define-macro (entry-set-pred! obj pred) `(set-entry-predecessor! ,obj ,pred))

  (define-macro (entry-init! obj)
    `(begin 
       (entry-set-succ! ,obj ,obj)
       (entry-set-pred! ,obj ,obj)))

  (define (%entry-insert! tq pred time task)
    (if (entry-value task)
	(let loop ((lst (entry-predecessor tq)))
	  (cond
	   ((eq? lst tq) (entry-insert-after! lst task))
	   ((pred time (entry-time lst))
	    (entry-insert-after! lst task))
	   (else (loop (entry-predecessor lst)))))
	#f))

  (define (entry-insert-after! lst task)
    (entry-set-pred! task lst)
    (entry-set-succ! task (entry-successor lst))
    (entry-set-pred! (entry-successor lst) task)
    (entry-set-succ! lst task)
    task)

  (define (%entry-remove! lst)
    (if (entry-successor lst)
	(begin
	  (entry-set-succ! (entry-predecessor lst) (entry-successor lst))
	  (entry-set-pred! (entry-successor lst) (entry-predecessor lst))
	  (entry-set-succ! lst #f)
	  (entry-set-pred! lst #f)))
    (and-let* ((v (entry-value lst)))
	      (set-entry-value! lst #f)
	      v))

  (define-macro (timeout-queue-leftmost tree)
    `(entry-successor (tq-queue ,tree)))

  (define-macro (timeout-queue-empty? q) `(eq? lst (tq-queue ,q)))

  )
 (rbtree
  
  (define-class <timeout-queue> (<object>)
    (color getter: timeout-queue-color setter: timeout-queue-color-set!)
    (parent getter: timeout-queue-parent setter: timeout-queue-parent-set!)
    (left getter: timeout-queue-left setter: timeout-queue-left-set!)
    (right getter: timeout-queue-right setter: timeout-queue-right-set!)
    (time getter: timeout-queue-time setter: timeout-queue-time-set!)
    (value getter: timeout-queue-value setter: timeout-queue-value-set!))

  (define timeout-queue-leftmost timeout-queue-right)

  (define timeout-queue-leftmost-set! timeout-queue-right-set!)

  (define (make-timeout-queue-entry color parent left right time value)
    (make <timeout-queue> color: color parent: parent left: left right: right time: time value: value))

  (define (timeout-queue-before? node1 node2) ;; ordering function
    (srfi19:time<? (timeout-queue-time node1) (timeout-queue-time node2)))

  (define-rbtree
    timeout-queue-init!	      ;; defined by define-rbtree
    timeout-queue->rbtree       ;; defined by define-rbtree
    #f			      ;; defined by define-rbtree
    timeout-queue-node-fold   ;; defined by define-rbtree
    #f
    timeout-queue-node-insert! ;; defined by define-rbtree
    timeout-queue-remove!       ;; defined by define-rbtree
    timeout-queue-reposition!   ;; defined by define-rbtree
    timeout-queue-empty?	     ;; defined by define-rbtree
    timeout-queue-singleton?    ;; defined by define-rbtree
    #f
    #f
    timeout-queue-before?
    timeout-queue-color
    timeout-queue-color-set!
    timeout-queue-parent
    timeout-queue-parent-set!
    timeout-queue-left
    timeout-queue-left-set!
    timeout-queue-right
    timeout-queue-right-set!
    timeout-queue-leftmost
    timeout-queue-leftmost-set!
    #f
    #f)

  (define (make-timeout-queue)
    (timeout-queue-init! (make-timeout-queue-entry #f #f #f #f
						   #f #f)))

  (define (timeout-queue-fold proc init tree)
    (timeout-queue-node-fold
     (lambda (node init) (proc (timeout-queue-time node) (timeout-queue-value node) init))
     init tree))

  (define-macro (entry-init! obj) `(timeout-queue-init! ,obj))

  (define-macro (make-entry time task) `(make-timeout-queue-entry #f #f #f #f ,time ,task))
  )
 (else
  
  (define-class <timeout-queue> (<object>)
    (color getter: timeout-queue-color setter: timeout-queue-color-set!)
    (left getter: timeout-queue-left setter: timeout-queue-left-set!)
    (right getter: timeout-queue-right setter: timeout-queue-right-set!)
    (time getter: timeout-queue-time setter: timeout-queue-time-set!)
    (value getter: timeout-queue-value setter: timeout-queue-value-set!))

  (define (make-timeout-queue-entry color left right time value)
    (make <timeout-queue> color: color left: left right: right time: time value: value))

  (define-llrbtree-code
    ()
    ((node . args)
     `(let ((node ,node))
	. ,(let loop ((args args))
	     (if (null? args)
		 '(node)
		 (cons
		  (case (car args)
		    ((color:) `(timeout-queue-color-set! node ,(cadr args)))
		    ((left:) `(timeout-queue-left-set! node ,(cadr args)))
		    ((right:) `(timeout-queue-right-set! node ,(cadr args)))
		    (else (error  (format "unbrauchbar ~a" args))))
		  (loop (cddr args)))))))
    timeout-queue-init!	           ;; defined
    #f				   ;; no lookup defined
    #f				   ;; no min defined
    timeout-queue-node-fold	   ;; defined
    #f				   ;; no for-each defined
    timeout-queue-node-insert!	   ;; defined
    #f			           ;; no delete defined
    timeout-queue-node-delete-min! ;; defined
    timeout-queue-empty?	   ;; defined
    (args #f)
    (args #f)
    ((node1 node2) `(srfi19:time<=? (timeout-queue-time ,node1) (timeout-queue-time ,node2)))
    timeout-queue-left
    timeout-queue-left-set!
    timeout-queue-right
    timeout-queue-right-set!
    timeout-queue-color
    timeout-queue-color-set!
    #f)

  (define (make-timeout-queue)
    (timeout-queue-init! (make-timeout-queue-entry #f #f #f
						   #f #f)))

  (define (timeout-queue-fold proc init tree)
    (timeout-queue-node-fold
     (lambda (node init) (proc (timeout-queue-time node) (timeout-queue-value node) init))
     init tree))

  (define-macro (entry-init! obj) `(timeout-queue-init! ,obj))

  (define-macro (make-entry time task) `(make-timeout-queue-entry #f #f #f ,time ,task))
  ))

(cond-expand
 ((or double-linked-list rbtree)
  (define-record-type <tq>
    (%make-tq mutex queue)
    tq?
    (mutex tq-mutex)
    (queue tq-queue)))
  (define-macro (%%make-tq s r) (%make-tq s r))
 (else
  (define-record-type <tq>
    (%make-tq mutex leftmost queue)
    tq?
    (mutex tq-mutex)
    (leftmost timeout-queue-leftmost set-timeout-queue-leftmost!)
    (queue tq-queue))
  (define-macro (%%make-tq s r) `(%make-tq ,s #f ,r))))

(define-macro (entry-time obj) `(timeout-queue-time ,obj))
(define-macro (entry-value obj) `(timeout-queue-value ,obj))
(define-macro (set-entry-value! obj v) `(timeout-queue-value-set! ,obj ,v))

(define (make-tq name)
  (let ((r (make-entry #f #f)))
    (entry-init! r)
    (%%make-tq (make-semaphore name 1) r)))

(define (tq-name tq)
  (mutex-name (tq-mutex tq)))

(define-class <timeout> (<condition>))

(define *local-timeout-symbol* (make <timeout>))

(define (timeout-object) *local-timeout-symbol*)

(define (timeout-object? obj)
  (eq? *local-timeout-symbol* obj))

(define (thread-signal-timeout! t . tv)
  (handler-case
   (thread-deliver-signal! t (if (pair? tv) (car tv) *local-timeout-symbol*))
   ((<condition>))))

;;*** general code

(define *stop-list* (make-tq 'stopset))

(cond-expand
 (rbtree
  (define-macro (%entry-remove! lst)
    `(begin (timeout-queue-remove! ,lst)
	    (and-let* ((v (entry-value ,lst)))
		      (set-entry-value! ,lst #f)
		      v))))
 (else
  (define-macro (%entry-remove! lst)
    `(and-let* ((top (timeout-queue-leftmost *stop-list*)))
	       (set-timeout-queue-leftmost!
		*stop-list*
		(timeout-queue-node-delete-min! (tq-queue *stop-list*)))
	       (and-let* ((v (entry-value top)))
			 (set-entry-value! top #f)
			 v)))))

(define (entry-remove! obj)
  (with-semaphore (tq-mutex *stop-list*) (%entry-remove! obj)))

(cond-expand
 (double-linked-list
  (define-macro (%entry-insert! tq pred entry)
    `(timeout-queue-node-insert! ,tq ,entry))

  (define-macro (tq-insert! tq pred time task)
    `(let ((entry (make-entry ,time ,task)))
       (with-semaphore (tq-mutex ,tq) (%entry-insert! (tq-queue ,tq) ,pred entry))))
  )

 (rbtree
  (define-macro (entry-insert! task)
    `(with-semaphore
      (tq-mutex *stop-list*)
      (timeout-queue-node-insert! (tq-queue *stop-list*) ,task))))
 (else
  (define-macro (entry-insert! task)
    `(with-semaphore
      (tq-mutex *stop-list*)
      (let ((top (timeout-queue-leftmost *stop-list*)))
	(if (not top)
	    (set-timeout-queue-leftmost! *stop-list* ,task)
	    (if (srfi19:time<? (timeout-queue-time top) (timeout-queue-time ,task))
		(timeout-queue-node-insert! (tq-queue *stop-list*) ,task)
		(begin
		  (timeout-queue-node-insert! (tq-queue *stop-list*) top)
		  (set-timeout-queue-leftmost! *stop-list* ,task))))))))
 )

(cond-expand
 (double-linked-list
  (define (display-stop-list)
    (cons
     'timeoutlist
     (let loop ((lst (entry-successor (tq-queue *stop-list*))))
       (if (not (eq? lst (tq-queue *stop-list*)))
	   (if (not (entry-value lst))
	       (loop (entry-successor lst))
	       `((timeoutlistentry
		  (timeoutdate
		   ,(rfc-822-timestring
		     (time-utc->date (entry-time lst) (timezone-offset))))
		  (timeoutobject ,(format #f "~s" (entry-value lst))))
		 . ,(loop (entry-successor lst))))
	   '())))))
 (rbtree
  (define (display-stop-list)
    (cons
     'timeoutlist
     (timeout-queue-node-fold
      (lambda (e i)
	(if (entry-value e)
	    `((timeoutlistentry
	       (timeoutdate
		,(rfc-822-timestring
		  (time-utc->date (entry-time e) (timezone-offset))))
	       (timeoutobject ,(format #f "~s" (entry-value e))))
	      . ,i)
	    i))
      '()
      (tq-queue *stop-list*)))))
 (else
  (define (display-stop-list)
    (cons
     'timeoutlist
     (let ((r (timeout-queue-node-fold
	       (lambda (e i)
		 (if (entry-value e)
		     `((timeoutlistentry
			(timeoutdate
			 ,(rfc-822-timestring
			   (time-utc->date (entry-time e) (timezone-offset))))
			(timeoutobject ,(format #f "~s" (entry-value e))))
		       . ,i)
		     i))
	       '()
	       (tq-queue *stop-list*))))
       (if (let ((t (timeout-queue-leftmost *stop-list*)))
	     (or (not t) (not (entry-value t))))
	   r
	   (let ((e (timeout-queue-leftmost *stop-list*)))
	     `((timeoutlistentry
		(timeoutdate
		 ,(rfc-822-timestring
		   (time-utc->date (entry-time e) (timezone-offset))))
		(timeoutobject ,(format #f "~s" (entry-value e))))
	       . ,r))))))))

(define *execute-at* (make-mailbox 'execute-at))

(define (execute-at time obj)
  (let ((task (make-entry time obj)))
    (send-message! *execute-at* task)
    task))

(define (queue:timeout-value time)
  (cond ((and (number? time) (> time 0))
	 (add-duration 
	  (srfi19:current-time 'time-utc)
	  (make-time 'time-duration
		     (* (- time (inexact->exact time))
			1000 1000 1000)
		     (inexact->exact time))))
	((srfi19:time? time)
	 (case (time-type time)
	   ((time-duration)
	    (and (or (> (time-second time) 0)
		     (and (eqv? (time-second time) 0)
			  (> (time-nanosecond time) 0)))
		 (add-duration 
		  (srfi19:current-time 'time-utc) time)))
	   ((time-utc)
	    (and (srfi19:time>? 
		  time (srfi19:current-time 'time-utc))
		 time))
	   ((time-tai)
	    (and (srfi19:time>?
		  time (srfi19:current-time 'time-tai))
		 (time-tai->time-utc time)))
	   ((time-monotonic)
	    (and (srfi19:time>?
		  time (srfi19:current-time 'time-monotonic))
		 (time-monotonic->time-utc time)))
	   (else #f)))
	(else #f)))

(define (queue:register-timeout-message! time mbox)
  (and-let* ((time (queue:timeout-value time)))
	    (execute-at time mbox)))

(define (queue:cancel-timeout-message! obj)
  ;; (if (and obj (entry-value obj)) (entry-remove! obj))
  (if obj (set-entry-value! obj #f))
  #f)

(define (threads:timeout-value time)
  (cond ((and (number? time) (> time 0))
	 (inexact->exact (* time 1000)))
	((srfi19:time? time)
	 (case (time-type time)
	   ((time-duration)
	    (let* ((ms (+ (* (time-second time) 1000)
			  (quotient (time-nanosecond time) 1000000))))
	      (and (> ms 0) ms)))
	   ((time-utc time-tai time-monotonic)
	    (let* ((v (time-difference
		       time (srfi19:current-time 
			     (time-type time))))
		   (ms (+ (* (time-second v) 1000)
			  (quotient (time-nanosecond v) 1000000))))
	      (and (> ms 0) ms)))
	   (else #f)))
	(else #f)))

(define (threads:register-timeout-message!/ms ms target)
  (thread-start!
   (make-thread
    (lambda ()
      (thread-sleep!/ms ms)
      ((cond
	((mailbox? target) send-message!)
	((thread? target) thread-signal-timeout!)
	(else (error "NYI")))
       target *local-timeout-symbol*))
    (format #f "~a wait ~a ms" (current-thread) ms))))

(define threads:register-timeout-message! queue:register-timeout-message!)

(define (threads:cancel-timeout-message! obj)
  ;; (if (and obj (entry-value obj)) (entry-remove! obj))
  (if obj (set-entry-value! obj #f))
  #f)

(define register-timeout-message! queue:register-timeout-message!)
(define cancel-timeout-message! queue:cancel-timeout-message!)

(define *time-procedure* #f)
(define *system-time* (srfi19:current-time 'time-utc))
(define *system-date* #f)

(define (timestamp) (or *system-date* (current-date 0)))

(define (register-time-procedure! proc)
  (set! *time-procedure* proc))

(define (run-time-procedure now)
  ;; We should check the timezone just at 2 a.m.?
  (if (eqv? (modulo (time-second now) 3600) 0)
      (timezone-tzset))
  (set! *system-date* (time-utc->date now 0))
  (if (and *time-procedure* *system-time*)
      (future (guard
               (exception
                (else (receive
		       (title msg args rest) (condition->fields exception)
		       (logcond 'system-clock title msg args))))
               (*time-procedure* now))
              "clocktick"))
  (let loop ((lst (timeout-queue-leftmost
		   (cond-expand
		    ((or double-linked-list rbtree) (tq-queue *stop-list*))
		    (else *stop-list*)))))
    (if (and (cond-expand
	      ((or double-linked-list rbtree)
	       (not (eq? lst (tq-queue *stop-list*))))
	      (else (and lst (entry-time lst))))
	     (srfi19:time<=? (entry-time lst) now))
        (let ((success (entry-remove! lst)))
          (cond
           ((not success))
           ((thread? success) (thread-signal-timeout! success))
           ((procedure? success)
            (future (guard (exception
                            (else (logerr "Timeout ran into exception ~a"
                                          exception)))
                           (success))
                    "time-shifted-execution"))
           ((mailbox? success)
            (send-message! success *local-timeout-symbol*))
           (else (logerr "internal error watchdog saw ~s\n" success)))
          (loop (timeout-queue-leftmost
		 (cond-expand
		  ((or double-linked-list rbtree) (tq-queue *stop-list*))
		  (else *stop-list*))))))))

;; Create a procedure, which takes a thunk to be serialized with
;; possible timeout.

(define (make-serial-guard name)
  (let ((sema (make-semaphore name 1)))
    (lambda (thunk)
      (dynamic-wind
          (lambda () (semaphore-wait sema))
          thunk
          (lambda () (semaphore-signal sema))))))

(define-syscall-glue (send-cont-signal (pid <fixnum>))
  properties: ((other-h-files "<signal.h>"))
{
  REG0 = int2fx( kill( fx2int(pid), SIGCONT ) );
  RETURN1();
})

(define parent (getppid))

(define (send-alive-signal) (send-cont-signal parent))

(define c-950-000-000 (inexact->exact (* 950 1000 1000)))

(define c-1000-000-000 (inexact->exact (* 1000 1000 1000)))

(define (update-system-time! now dead-parent)
  (if (not (eqv? (time-second *system-time*) (time-second now)))
      (let ((now (make-time 'time-utc 0 (time-second now))))
	(set! *system-time* now)          ; update system wide cache
	(if (not (eqv? 0 (send-alive-signal)))
	    (begin (logerr "Failed to send alive signal.\n")
		   (dead-parent)))
	(guard
	 (ex (else (logerr "watchdog caught exception ~a\n" ex)))
	 (run-time-procedure now))
	(let loop ()
	  (if (not (mailbox-empty? *execute-at*))
	      (let ((task (receive-message! *execute-at*)))
		(if (entry-value task) (entry-insert! task))
		(loop)))))))

(define *watchdog* #f)

(define (make-watchdog! dead-parent)
  (set!
   *watchdog*
   (thread-start!
    (make-thread
     (lambda ()
       (do ((now (srfi19:current-time 'time-utc) (srfi19:current-time 'time-utc)))
	   (#f)
	 (update-system-time! now dead-parent)
	 (thread-sleep!/ms
	  (if (> (time-nanosecond now) c-950-000-000)
	      (add1 (quotient (- c-1000-000-000 (time-nanosecond now)) 2000000))
	      (fx- 990 (quotient (time-nanosecond now) 1000000))))))
     "watchdog")))
  *watchdog*)

(define respond-timeout-interval (make-config-parameter #f))

(define remote-fetch-timeout-interval (make-config-parameter #f))

(define (timeout time-out method access request)
  (with-timeout time-out (lambda () (method access request))))

(define (queue:with-timeout time-out thunk)
  (let ((time (queue:timeout-value time-out)))
    (if time
	(let ((pending #f))
	  (dynamic-wind
	      (lambda ()
		(set! pending (queue:register-timeout-message! time-out (current-thread))))
	      thunk
	      (lambda () (queue:cancel-timeout-message! pending))))
	(thunk))))

(define (threads:with-timeout time-out thunk . make-signal)
  (if time-out
      (let ((pending #f))
	(dynamic-wind
	    (lambda ()
	      (set! pending (queue:register-timeout-message!
			     time-out
			     (if (null? make-signal)
				 (current-thread)
				 (let ((t (current-thread)))
				   (lambda ()
				     (thread-signal-timeout! t ((car make-signal)))))))))
	    thunk
	    (lambda () (queue:cancel-timeout-message! pending))))
      (thunk)))

(define (threads:with-timeout! time-out thunk)
  (if time-out
      (let ((pending #f))
	(dynamic-wind
	    (lambda ()
	      (set! pending (queue:register-timeout-message! time-out (current-thread))))
	    (lambda ()
	      (thread-join! (thread-start! (make-thread thunk (thread-name (current-thread))))))
	    (lambda () (queue:cancel-timeout-message! pending))))
      (thunk)))

(define (with-timeout!+ time-out thunk)
  (set! time-out (threads:timeout-value time-out))
  (if time-out
      (let ((pending #f)
	    (thread (make-thread thunk (thread-name (current-thread)))))
	(dynamic-wind
	    (lambda ()
	      (set! pending (queue:register-timeout-message! time-out (current-thread))))
	    (lambda ()
	      (guard
	       (ex ((timeout-object? ex)
		    (thread-signal-timeout! thread)
		    (raise ex)))
	       (thread-join! (thread-start! thread))))
	    (lambda () (queue:cancel-timeout-message! pending))))
      (thunk)))

(define (with-timeout+ time-out thunk)
  (if time-out
      (let ((pending #f)
	    (thread (make-thread thunk (thread-name (current-thread)))))
	(dynamic-wind
	    (lambda ()
	      (set! pending (queue:register-timeout-message! time-out (current-thread))))
	    (lambda ()
	      (thread-join! (thread-start! thread)))
	    (lambda ()
	      (thread-terminate! thread)
	      (queue:cancel-timeout-message! pending))))
      (thunk)))

(define with-timeout threads:with-timeout)
(define with-timeout! threads:with-timeout!)

(define *timeout-handling* 'threads)

(define (timeout-handling-policy . how)
  (if (pair? how)
      (begin
        (case (car how)
          ((threads)
           (set! *timeout-handling* (car how))
           (set! with-timeout threads:with-timeout)
           (set! with-timeout! threads:with-timeout!)
           (set! register-timeout-message! queue:register-timeout-message!)
           (set! cancel-timeout-message! queue:cancel-timeout-message!))
          ((queue)
           (set! *timeout-handling* (car how))
           (set! with-timeout queue:with-timeout)
           (set! with-timeout! queue:with-timeout)
           (set! register-timeout-message! queue:register-timeout-message!)
           (set! cancel-timeout-message! queue:cancel-timeout-message!))
          )))
  *timeout-handling*)

(timeout-handling-policy 'treads)

(define (timeout-second arg)
  (cond ((and (srfi19:time? arg)
	      (eq? (time-type arg) 'time-duration))
	 (let ((s (time-second arg))
	    (ns (if (> (time-nanosecond arg) 1000000)
		    (* (time-nanosecond arg) 0.000000001)
		    0)))
	   (if (> s 0) (+ s ns) (if (> ns 0) ns #f))))
	((and (number? arg)
	      (> arg 0))
	 arg)
	(else #f)))
