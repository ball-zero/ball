;; (C) 2000 - 2003, 2005, 2007-2010 Jörg F. Wittenberger see
;; http://www.askemos.org

(: capability-intersection (:capaset: :capaset: :capaset: --> :capaset:))
(define (capability-intersection initial descretionary mandatory)
  (let loop ((local initial) (remote descretionary))
    (cond
     ((null? remote)
      ;; Users are confused by reordered capabilities on remote hosts
      ;; and we just consed up that list anyway.
      ;;
      ;; 2016-03-21: Does this comment still apply?
      (reverse local))
     ((and (dominates? (car remote) mandatory)
	   (not (member (car remote) local)))
      (loop (cons (car remote) local) (cdr remote)))
     (else (loop local (cdr remote))))))

(define *signal-clearance* #t)		; #f for testing

(define (signal-subject! oid type message)
  ;; The SOAPAction header/slot is gone anyway.  It's an internal sign
  ;; of the current implementation that we run the old agreement
  ;; protocol, which ought to be replaced.
  (let ((signal-type (get-slot message 'soapaction)))
    (cond
     ((not signal-type)
      (with-a-ref
       oid
       (let ((obj ((if (eq? type 'write) find-frames-by-id
		       find-frame-by-id/asynchroneous)
		   oid)))
	 (if obj
	     (if (eq? type 'write)
		 ;; Don't replicate write calls to public-equivalent?
		 ;; places.  They raise an exception anyway.  Remark:
		 ;; this breaks the abstration of a place somewhat,
		 ;; because at this mechanical level we can not know
		 ;; that the place will raise an exception.  At the
		 ;; other hand it saves network ressources to know it.
		 (if (public-equivalent? (aggregate-entity obj))
		     (call-local-subject! (replicates-of obj) obj type message)
		     (let ((quorum (replicates-of obj)))
		       (if (and (pair? (quorum-others quorum))
				(or (global-proxy-mode)
				    (and
				     *signal-clearance*
				     (raise-service-unavailable-condition
				      (format "~a lacks client clearance"
					      (local-id)))))
				;; replicate only requests which come
				;; from a web browser, not soap calls.
				(not
				 (soap+xml-content?
				  (or (get-slot message 'content-type) ""))))
			   ;; Go
			   (let ((minimum
				  ((if (quorum-local? quorum)
				       identity (lambda(x) (add1 x)))
				   (quotient (* 2 (quorum-size quorum)) 3))))
			     (logagree
			      "relaying request on ~a to ~a of ~a\n"
			      oid minimum (quorum-others quorum))
			     (if (quorum-local? quorum)
				 (let ((qa ((replicate-call)
					    #f
					    (hosts-lookup-at-least
					     (quorum-others quorum) minimum) minimum
					     obj type message #t)))
				   (guard
				    (ex ((service-unavailable-condition? ex)
					 (or (force qa) (raise ex)))
					((agreement-timeout? ex)
					 (or (force qa) (raise ex)))
					(else (raise ex)))
				    ;; FIXME: the value should be
				    ;; reasonable, not just hard coded
				    (thread-sleep! 0.1)
				    (call-local-subject!
				     (replicates-of obj) obj type message)))
				 ((replicate-call)
				  (or (get-slot message 'reply-channel) #t)
				  (hosts-lookup-at-least
				   (quorum-others quorum) minimum) minimum
				   obj type message)))
			   (call-local-subject!
			    (replicates-of obj) obj type message))))
		 (call-subject! obj *local-quorum* type message))
	     (raise (make-object-not-available-condition
		     oid (get-slot message 'caller) 'signal-subject!))))))
     (else (raise (format "signal-subject! caught ~a\n" signal-type))))))

;; -------------------------------------------------------------------------

;;** Missplaced code.


(define (delete-is-sync-flag!deep oid)
  (delete-is-sync-flag! oid)
  (sql-set-missing-oid! oid 0))

;; This is the toplevel driver for the client and replication interface.

;; Now (dec. 2002), that the byzantine protocol and process step stuff
;; should be refactored into the mechanism section, and that some
;; profiling revealed too much time spent in the askemos-web-user
;; function, we a little mess with the code won't hurt anyway... sorry

;; KLUDGE Somehow we have to understand some basic http stuff until we
;; live in our own SOAP opera.

(define (host-master-request? message)
  (and-let* (((equal? (get-slot message 'web-user) $administrator))
	     (secret (get-slot message 'secret)))
	    (check-administrator-password secret)))

;;* Web Access

;; Here comes a lengthy definition, which is just at the boundary
;; between kernel and client code, i.e., it interprests structures,
;; which are modifies by client code.

;; host-home (with a magic object id 0)
;;    +- links
;;          | -/- indexed by http user name
;;          +- user object  (creator, pass, holds capabilities)
;;              +- links

;; If $skip-referrer-check is set, we ingore the user agents idea
;; about the origin of request it sends.  Often user agents (w3m)
;; don't send referer headers with POST.

(define $skip-referrer-check #f)

(define (client-position-obvious? method request absolute user)
  (cond
   ((or (equal? method "PUT")
;; 	(and-let* (((eq? (get-slot request 'content-type) text/xml))
;; 		   (body-plain (get-slot request 'mind-body))
;; 		   (body-parsed (get-slot request 'body/parsed-xml))
;; 		   (body (or body-parsed (xml-parse body-plain))))
;; 		  (if (not body-parsed)
;; 		      (set-slot! request 'body/parsed-xml body))
;; 		  (let ((nmsp (ns (document-element body))))
;; 		    (or (eq? nmsp namespace-mind)
;; 			(eq? nmsp namespace-mind-v1))))
	)
    absolute)
   (else

    ;; FIXME: Flash 7 and 8 are broken and might be used to forge
    ;; request headers.
    ;; http://ha.ckers.org/blog/20060725/forging-http-request-headers-with-flash/
    ;; http://www.securityfocus.com/archive/1/441014/30/0/threaded

    (and-let*
     ((referrer (get-slot request 'referer))
      (uri (string->uri referrer))
      (pp (parsed-locator (uri-path uri))))
     (eq? (receive
	   (referrer quorum)
	   (oid-parse (if (pair? pp) (car pp) (uri-path uri)))
	   referrer)
	  absolute)))))

(define (handle-web-conditions/default c request)
  (let ((c (let loop ((c c))
	     (if (uncaught-exception? c)
		 (loop (uncaught-exception-reason c))
		 c)))
	(location (append-reverse (or (get-slot request 'destination) '())
				  (or (get-slot request 'location) '()))))
    (cond
     ((message-condition? c)
      (let ((ec (if (http-effective-condition? c)
		    (http-effective-condition-status c)
		    400)))
	(if (eq? (ns (node-list-first (condition-message c)))
		 namespace-soap-envelope)
	    (if (accept-header-matches-application/soap+xml
		 (get-slot request 'accept))
		(make-soap-error ec location (condition-message c))
		(make-html-error ec location
				 (condition-message c)
				 (condition-message c) '()))
	    (let* ((title (format "Error ~a" ec))
		   (txt (condition-message c)))
	      (make-html-error ec location title txt '())))))
     ((or (agreement-timeout? c) (timeout-object? c))
      (if (accept-header-matches-application/soap+xml
	   (get-slot request 'accept))
	  (make-soap-error 503 location (condition-message c))
	  (make-html-error	503 location
				"Timeout"
				(sxml `(p ,(cond
					    ((late-timeout-object? c) "late")
					    ((early-timeout-object? c)"early")
					    (else "general"))
					  " timeout"))
				'())))
     (else (receive
	    (title msg args rest) (condition->fields c)
	    (logcond location title msg args)
	    (if (accept-header-matches-application/soap+xml
		 (get-slot request 'accept))
		(make-soap-error
		 400 location
		 (make-soap-receiver-error
		  location title msg args))
		(make-html-error
		 400 location title msg args)))))))

(define $html-error-handler (make-config-parameter handle-web-conditions/default))

(define (handle-web-conditions c request)
  (($html-error-handler) c request))

;; $virtual-host is to be removed, see Makefile.
(define $virtual-host (make-config-parameter (lambda (host) #f)))

;; This looks too internal to be exportet… but it is now :-/
(: web-handle-authenticated-service
   (:message: :oid: :place: boolean -> :message:))
(define (web-handle-authenticated-service request user user-place is-public)

  (yield-system-load)

  ;; we got a full owner for the request
  (or is-public (set-slot! request 'secret #f))
  (set-slot! request 'dc-creator user)
  (set-slot! request 'dc-date (current-date (timezone-offset)))
  ;; If the first part of the path is an oid, we send direct to
  ;; this place (i.e., don't go over the user place).
  (let* ((name (get-slot request 'dc-identifier))
	 (method (get-slot request 'request-method))
	 (type (if (member method '("PUT" "POST" #t write))
		   'write 'read))
	 ;; Some mangling likely to ease protocol use.
	 (path (parsed-locator name)))
    (receive
     (absolute quorum-hint)
     (if (pair? path)
	 (guard
	  (ex (else (values #f #f)))
	  (oid-parse (car path)))
	 (values #f #f))
     (let ((absolute-place (and absolute
				(find-frame-by-id/quorum
				 absolute
				 (or quorum-hint
				     (and user-place (fget user-place 'replicates))
				     *local-quorum*)))))
       (if absolute-place
	   (begin
	     (set-slot! request 'destination (cdr path))
	     (set-slot! request 'location (list (car path))))
	   (begin
	     (set-slot! request 'destination path)
	     (set-slot! request 'location '())))

       ;; Protect the user from malicious web forms.  Attach users
       ;; rights only, if the form action points back to the agent,
       ;; which produced the form.
       (let* ((origin (if (or $skip-referrer-check
			      (eq? type 'read) (not absolute)
			      (client-position-obvious?
			       method request absolute user))
			  user-place (find-frames-by-id (public-oid))))
	      (capa (fget origin 'capabilities)))
	 (if capa (set-slot! request 'capabilities capa)))

       (let ((target (or absolute
			 (and-let* ((vhost (get-slot request 'host)))
				   (($virtual-host) vhost))
			 (and is-public (null? path) (eq? type 'read)
			      (frame-resolve-link (aggregate-meta user-place) "welcome" #f))
			 user)))
	 (guard
	  (ex (else (handle-web-conditions ex request)))
	  (signal-subject! target type request)))))))

(define (web-handle-service request user-name user is-public user-place)

  (cond
   ;; we want to change the host configuration?
   ((and (equal? (get-slot request 'dc-identifier) "/")
	 (host-master-request? request)
	 (is-meta-form? (make-reader request)))
    (logerr "Administrative request.\n")
    (set-slot! request 'dc-creator (public-oid))
    (set-slot! request 'capabilities (public-capabilities))
    (set-slot! request 'destination '())
    (let ((result
	   (respond!
	    (find-local-frame-by-id (my-oid) 'web-handle-service)
	    (lambda (me msg)
	      (let* ((form (document-element (msg 'body/parsed-xml)))
		     (link (and
			    (not (node-list-empty? form))
			    (select-elements (children form) 'link)))
		     (name (and (not (node-list-empty? link))
				(attribute-string
				 'name (node-list-first link)))))
		(if (and name (entry-name->oid name))
		    (error (format "~a already exists" name))
		    ;; BUG, needs to separate propose and accept stage.
		    (interpret-rw me msg (msg 'body/parsed-xml)))))
	    respond-making-laune #t request)))
      (set-slot! result 'location '(""))
      (delete-is-sync-flag! (my-oid))
      ;; This could need a ticket for the persistant store (if we
      ;; run 'late' access scheme).  That in turn would block in the
      ;; initialisation.
      (!order/thunk update-*reverse-of-public-name-space*
		    'update-*reverse-of-public-name-space*)
      result))
   ;; or can not validate the user/passwd combination?
   ((or (not user)
	(not (or is-public
		 (secret-verify (fget user-place 'secret) (get-slot request 'secret)))))
    ;(make-no-access-error user-name)
    http-require-auth-message)
   (else (web-handle-authenticated-service request user user-place is-public))))

(: askemos-web-user (:message: -> :message:))
(define (askemos-web-user request)
  (let* ((user-name (get-slot request 'web-user))
	 ;; the host has a name space for users...
	 (user (if (my-oid) (entry-name->oid user-name) #f))
	 (is-public (string=? user-name "public")))
    (guard
     (c ((or (message-condition? c)
	     (agreement-timeout? c) (timeout-object? c))
	 (condition->message #f request c))
	(else (raise c)))
     (let ((user-place (and user
			    (or (find-local-frame-by-id user 'askemos-web-user)
				(find-frames-by-id user)
				(raise (make-object-not-available-condition
					(oid->string user) user-name 'web-user))))))
       (set-slot! request 'caller (or user null-oid)) ; FIXME: null-oid here is questionable
       (web-handle-service request user-name user is-public user-place)))))

;; The generated accesor works simillar to askemos-web-user but
;; without host master controls and credentials are passed to the
;; application, while the real user id is still public.

(: make-askemos-public-user (:oid: --> (procedure (:message:) :message:)))
(define (make-askemos-public-user user)
  (lambda (request)
    (let* ((user-name (get-slot request 'web-user))
	   ;; the host has a name space for users...
	   (user-place (if user (find-frames-by-id user) #f)))

      (set-slot! request 'caller user)

      (set-slot! request 'dc-creator user)
      (set-slot! request 'dc-date (current-date (timezone-offset)))
      ;; If the first part of the path is an oid, we send direct to
      ;; this place (i.e., don't go over the user place).
      (let* ((name (get-slot request 'dc-identifier))
	     (method (get-slot request 'request-method))
	     (type (if (member method '("PUT" "POST" #t write))
		       'write 'read))
	     ;; Some mangling likely to ease protocol use.
	     (path (parsed-locator name))
	     (absolute (and (pair? path) (string->oid (car path))))
	     (absolute-place (and absolute (find-frames-by-id absolute))))

	(if absolute-place
	    (begin
	      (set-slot! request 'destination (cdr path))
	      (set-slot! request 'location (list (car path))))
	    (begin
	      (set-slot! request 'destination path)
	      (set-slot! request 'location '())))

	;; Protect the user from malicious web forms.  Attach users
	;; rights only, if the form action points back to the agent,
	;; which produced the form.
	(let ((capa (fget user-place 'capabilities))
	      (pub (public-capabilities)))
	  (set-slot! request 'capabilities (if capa (append capa pub) pub)))

	(let ((target (or absolute user)))
	  (bind-exit
	   (lambda (return)
	     (with-exception-handler
	      (lambda (c)
		(let ((c (let loop ((c c))
			   (if (uncaught-exception? c)
			       (loop (uncaught-exception-reason c))
			       c))))
		  (return
		   (cond
		    ((message-condition? c)
		     (let ((ec (if (http-effective-condition? c)
				   (http-effective-condition-status c)
				   400))
			   (location (reverse (get-slot request 'destination))))
		       (if (eq? (ns (node-list-first (condition-message c)))
				namespace-soap-envelope)
			   (if (accept-header-matches-application/soap+xml
				(get-slot request 'accept))
			       (make-soap-error ec location (condition-message c))
			       (make-html-error	ec location (condition-message c)
						(condition-message c) "Hm"))
			   (let* ((title (format "Error ~a" ec))
				  (txt (condition-message c)))
			     (make-html-error ec location title txt '())))))
		    (else (receive
			   (title msg args rest) (condition->fields c)
			   (logcond 'askemos-web-user title msg args)
			   (let ((location
				  (append-reverse (get-slot request 'destination)
						  (get-slot request 'location))))
			     (if (accept-header-matches-application/soap+xml
				  (get-slot request 'accept))
				 (make-soap-error
				  400 location
				  (make-soap-receiver-error
				   location title msg args))
				 (make-html-error
				  400 location title msg args)))))))))
	      (lambda () (signal-subject! target type request))))))))))


;;** replication

(define default-replicates-of replicates-of)

(define answer-not-found
  (make-message (property 'http-status 404)))

;; The copyright directive from 1991 says acts of "loading,
;; displaying, running, transmision or storage of the computer
;; program" are subject to authorization by the rightholder.

(define public-read-request
  (delay
    (make-message
     (property 'capabilities (public-capabilities))
     (property 'location '())
     (property 'destination '())
     (property 'dc-creator (public-oid))
     (property 'dc-date (current-date (timezone-offset)))
     (property 'location-format 'http-location-format) ; ??? ??? ???
     )))

(define (public-read obj)
  (call-subject! obj *local-quorum* 'read (force public-read-request)))

(define $http-max-direct-string-length 1024)

#|
(define (maybe-resync-later.sleep oid msg quorum)
  (let ((tmo (+ (* ($broadcast-retries)
		   ($broadcast-timeout)
		   (or (respond-timeout-interval) 2)))))
    (thread-sleep! tmo)
    (if (let ((obj (find-frame-by-id/synchronised+quorum oid quorum)))
	  (or (not obj)
	      (message-ahead-of (fget obj 'version) msg)))
	(delete-is-sync-flag!deep oid))))
|#

(define (maybe-resync-later oid msg quorum)
  (register-timeout-message!
   (+ (* ($broadcast-retries)
	 ($broadcast-timeout)
	 (or (respond-timeout-interval) 2)))
   (lambda ()
     (if (let ((obj (find-frame-by-id/synchronised+quorum oid quorum)))
	   (or (not obj)
	       (message-ahead-of (fget obj 'version) msg)))
	 (delete-is-sync-flag!deep oid)))))

(define (maybe-trigger-completion! oid message)
  (guard (ex
	  ((service-unavailable-condition? ex)
	   (delete-is-sync-flag! oid)
	   (logtopo "On ~a: ~a\n" oid (condition->string ex)))
	  (else
	   (log-condition (format "maybe-trigger-completion! ~a" oid) ex)
	   ;; The condition as just filed in the log ought ot be handled.
	   ;; Nevertheless we try to fix the case.
	   (delete-is-sync-flag! oid)))
	 (and-let* ((obj (find-local-frame-by-id oid 'maybe-trigger-completion!))
	     (body (get-slot message 'mind-body))
	     ((is-text/scheme? (get-slot message 'content-type)))
	     (msg (call-with-input-string body read)))
	    (set-slot! message 'mind-body msg)
	    (advance-transactions obj message 'maybe-trigger-completion!)
	    (if (message-ahead-of (fget obj 'version) msg)
		(maybe-resync-later oid msg (default-replicates-of obj)))
	    (thread-yield!))))

;; 0: never wait, 1: wait on state, 2: wait on digest, 3: wait on both
(define $defer-sync-replies-to-stable-state
  (make-shared-parameter 1))

(define (get-state-reply oid request)
  (if (odd? ($defer-sync-replies-to-stable-state))
      (!start (maybe-trigger-completion! oid request)
	      (dbgname (oid->string oid) "get-state-reply ~a"))
      (maybe-trigger-completion! oid request))
  (let ((obj (find-local-frame-by-id oid 'get-state-reply)))
    (if obj
	(make-message
	 http-property-text/scheme
	 (property
	  'mind-body
	  (call-with-output-string
	   (lambda (port) (write (agreement-last-message obj) port)))))
	(make-message
	 (property 'http-status 404)
	 (property 'mind-body (format "object ~a not available" oid))))))

(register-agreement-handler! 'get-state get-state-reply)

(define (check-query! at auth message type has-user user dc-creator caller)
  (if (not (or
	    ;; normal inter-object call
	    (and caller
		 (not (member auth (quorum-others at)))
		 (or (member
		      auth (quorum-others (replicates-of caller)))
		     ;; Instead of this exception, we
		     ;; could put the real caller on all
		     ;; public-meta-info requests.
		     (and (eq? type 'read)
			  (metainfo-request? message))))
	    ;; entrypoint replication
	    (and has-user (eq? user caller))
	    ;; public terminal
	    (and ;; (eq? auth one-oid) -- why should we care
		 (or (not caller) (eq? user caller)) ;; ??? Why check this?
		 (or (public-equivalent? dc-creator)
		     (member
		      auth (quorum-others (replicates-of user)))))
	    ))
      (raise (make-condition
	      &http-effective-condition 'status 406
	      'message (format "Query from ~a rejected. ~a" auth (public-oid))))))

(define (sync-replicate-reply oid message)
  (let ((auth (get-slot message 'authorization))
	(type (if (equal? (get-slot message 'request-method) "GET")
		  'read 'write)))
    (let ((obj (or (and (eq? type 'read)
			(find-local-frame-by-id oid 'sync-replicate-reply))
		   (retain-mutex
		    (respond!-mutex oid)
		    (or (find-local-frame-by-id oid 'sync-replicate-reply)
			(handle-invitation oid message auth)))
		   (raise (make-object-not-available-condition
			   (oid->string oid) (local-id) 'sync-replicate-reply)))))
      (let ((at (and obj (replicates-of obj))))
	(if (and at (quorum-local? at))
	    (let* ((peer (get-slot message 'ssl-peer-certificate))
		   (pq (make-quorum (list auth)))
		   (has-user (and (mesh-certificate? peer) (mesh-cert-o peer)))
		   (dc-creator (if has-user
				   has-user
				   (get-slot message 'dc-creator)))
		   (user (find-frame-by-id/asynchroneous+quorum dc-creator pq))
		   (caller (and-let*
			    ((caller (get-slot message 'caller))
			     ((not (or (eq? caller one-oid) (eq? caller auth)))))
			    (find-frame-by-id/asynchroneous+quorum caller pq))))
	      (check-query! at auth message type has-user user dc-creator caller)
	      (set-slot! message 'capabilities
			 (if user
			     (capability-intersection
			      (public-capabilities)
			      (get-slot message 'capabilities)
			      (if caller
				  (cons
				   (if (eq? (aggregate-entity caller) dc-creator)
				       (list (aggregate-entity caller))
				       (list (aggregate-entity caller) (my-oid)))
				   (fget user 'capabilities))
				  (public-capabilities)))
			     (public-capabilities)))
	      (guard
	       (c ((or (message-condition? c)
		       (agreement-timeout? c) (timeout-object? c))
		   (condition->message #f message c))
		  (else (condition->message 'askemos-sync message c)))
	       (if (eq? type 'write)
		   (write-with-sync-answer
		    (lambda (message)
		      (call-local-subject! at obj type message))
		    message)
		   (call-local-subject! at obj type message)))))))))

(register-agreement-handler! 'replicate sync-replicate-reply)

(define (signal-rb! oid message)
  (let* ((msg (and (is-text/scheme? (get-slot message 'content-type))
		   (call-with-input-string (get-slot message 'mind-body) read)))
	 (auth (get-slot message 'authorization))
	 (obj (or (find-frame-by-id/asynchroneous oid)
		  (raise (make-object-not-available-condition oid auth))))
	 (q (default-replicates-of obj)))
    (if (not (quorum-local? (replicates-of obj)))
	(let ((nobj (begin (delete-is-sync-flag! oid)
			   (or (find-frame-by-id/resynchronised+quorum
				oid (replicates-of obj))
			       (begin
				 (delete-is-sync-flag!deep oid)
				 (find-frame-by-id/resynchronised+quorum
				  oid (replicates-of obj)))))))
	  (if (and nobj (quorum-local? (replicates-of nobj)))
	      (begin
		(set! obj nobj)
		(set! q (default-replicates-of obj)))
	      (raise (make-object-not-available-condition oid auth)))))
    (if (not (member auth (quorum-others q)))
	(raise (make-object-not-available-condition oid auth)))
    (if (sync-message-data? msg)
	(!start
	 (begin
	   (set-slot! message 'mind-body msg)
	   (advance-transactions obj message 'signal-rb!)
	   (let ((obj (find-local-frame-by-id oid 'signal-rb!)))
	     (let ((local-version (version-serial (fget obj 'version)))
		   (msg-version (sync-message-container-serial msg)))
	       (cond
		((> local-version
		    (if (eq? (sync-message-phase msg) 'ready)
			(add1 msg-version) msg-version))
		 (let* ((request (make-reader message))
			(access (make-reader-access obj request)))
		   (logagree "~a: ~a from ~a is late (here ~a) .\n"
			     oid msg (get-slot message 'authorization)
			     local-version)
		   ((send-ready)
		    (list (host-lookup* (get-slot message 'authorization)))
		    access request (cons (sub1 local-version) (cdr (fget obj 'version))) #f)))
		((> msg-version local-version)
		 (delete-is-sync-flag!deep oid)
		 (logagree "~a reports ~a at ~a local ~a\n"
			   (get-slot message 'authorization) oid
			   msg-version local-version)
		 ;; Get on board again.
		 (if (or (eq? (sync-message-phase msg) 'ready)
			 (> (add1 msg-version) local-version)
			 (and-let* ((v (fget obj 'last-modified))
				    (tmo (respond-timeout-interval))
				    ((srfi19:time>=?
				      (add-duration *system-time* (make-time 'time-duration 0 tmo))
				      (date->time-utc v))))))
		     (find-frame-by-id/resynchronised+quorum oid (replicates-of obj))))
		((< msg-version local-version)
		 (logagree "~a ~a from ~a droped.\n"
			   oid msg (get-slot message 'authorization)))
		(else #f)))))
	 (dbgname (oid->string oid) "signal-rb! ~a from ~a" auth)))
    (http-update-alive-time! (host-lookup* auth) #t)
    (make-message
     http-property-text/scheme
     (property
      'mind-body
      (call-with-output-string
       (lambda (port) (write (agreement-last-message obj) port)))))))

(register-agreement-handler! 'ReliableBroadcast signal-rb!)

(define (handle-invitation oid request auth)
  (let* ((frame (find-local-frame-by-id oid 'handle-invitation))
	 (old-quorum (and frame (replicates-of frame))))
    (if (and old-quorum (quorum-local? old-quorum))
	frame
	(let* ((inv-quorum (and-let* ((s auth)) (make-quorum (list s))))
	       (quorum ((quorum-lookup)	oid inv-quorum)))
	  (if (and quorum (quorum-local? quorum))
	      (begin
		(delete-is-sync-flag!deep oid)
		(if (fx>= (quorum-size quorum) 3)
		  (resync-locked-now! oid frame quorum)
		  (begin
		    (logagree "On ~a first join.\n" oid)
		    (resync-locked-now! oid frame inv-quorum))))
	      #f)))))

(define (invitation-reply oid request)
  (retain-mutex
   (respond!-mutex oid)
   (handle-invitation oid request (get-slot request 'authorization)))
  (make-message))

(register-agreement-handler! 'invitation invitation-reply)

(define (reply-notice-reply oid message)
  (let ((dst (get-slot message 'destination)))
    (let ((version (string->number (car dst)))
	  (http-status (string->number (cadr dst)))
	  (caller (string->oid (get-slot message 'dc-identifier)))
	  (auth (get-slot message 'authorization)))
      (let* ((obj (or (and-let* ((obj (find-local-frame-by-id oid 'reply-notice-reply)))
				(find-frame-by-id/asynchroneous+quorum
				 oid (replicates-of obj)))
		      (raise (make-object-not-available-condition oid auth))))
	     (quorum (replicates-of
		      (or (find-frame-by-id/asynchroneous+quorum
			   caller (make-quorum (list auth)))
			  (raise (make-object-not-available-condition
				  caller (local-id)))))))
	(if (member auth (quorum-others quorum))
	    (let ((local-version (or (and-let* ((v (fget obj 'version)))
					       (version-serial v))
				     0)))
	      (cond
	       ((> version local-version)
		(logagree "~a Reply notice for ~a at ~a local ~a\n"
			  (get-slot message 'authorization) oid
			  version local-version)
		(delete-is-sync-flag!deep oid)
		(let ((obj (find-frame-by-id/resynchronised+quorum oid quorum)))
		  (transaction-send-message-version! oid version message)))
	       (else
		(logagree "Reply notice for ~a ~a from ~a\n" oid version auth)))

	      (set-slot! message 'destination (cddr dst))
	      (set-slot! message 'caller caller)
	      (if http-status (set-slot! message 'http-status http-status))
	      ;; (register-transaction! oid version #f #f #f)
	      (transaction-send-message-version! oid version message)
;; 	      (let ((message (message-clone message)))
;; 		(set-slot! message 'soapaction "ReliableBroadcast")
;; 		(set-slot! message 'http-status #f)
;; 		(set-slot! message 'mind-body (make-ready-message #f (cons version "??")))
;; 		(transaction-send-message-version! oid (sub1 version) message))
	      (remove-transaction! oid version #f #f #f #f))
	    (logagree "Droped reply notice on ~a ~a from ~a\n" oid version auth)))))
  (make-message))

(register-agreement-handler! 'reply-notice reply-notice-reply)

(define $get-reply-wait (make-shared-parameter #t))

;; Deal with the legacy API from WebDAV implementation.
(define (%cleaned-transaction-reply oid version request)
  (and-let* ((r (transaction-reply oid version (get-slot request 'authorization)))
	     (r1 (write-with-sync-answer identity (message-clone r))))
	    (set-slot!
	     r1 'destination
	     (cons* (number->string version)
		    (number->string (or (get-slot r1 'http-status) 200))
		    (or (get-slot r1 'location) '())))
	    r1))

(define (get-reply-reply oid request)
  (let ((version (string->number (get-slot request 'mind-body))))
    (or (and version
	     (or (%cleaned-transaction-reply oid version request)
		 (and ($get-reply-wait)
		      (guard
		       (ex ((timeout-object? ex) #f))
		       (wait-for-transaction/version
			oid version transaction-completed? ($complete-timeout))
		       (%cleaned-transaction-reply oid version request)))))
	(and-let* ((obj (find-local-frame-by-id oid 'get-reply-reply))
		   (local-version (version-serial (fget obj 'version)))
		   ((fx< version local-version)))
		  (make-message
		   (property 'http-status 412)))
	(make-message
	 (property 'http-status 404)))))

(register-agreement-handler! 'get-reply get-reply-reply)

(define (certificate-request-reply oid message)
  (send-email $administrator (message-body/plain message)
	      `((From "unknown")
		(Subject "certificate request")))
  (make-message))

(register-agreement-handler! 'certificate-request certificate-request-reply)

(define approve-eval (make-config-parameter (lambda (authorisation) #f)))

(define eval-result-attribute-node-list
  (list (make-xml-attribute 'core 'xmlns namespace-mind-str)))

(define (eval-reply oid request)
  (cond
   ((and-let* ((peer (get-slot request 'ssl-peer-certificate))
	       ((mesh-certificate? peer)))
	      ((approve-eval) (mesh-cert-o peer)))
    =>
    (lambda (eval)
      (let* ((expr (call-with-input-string (get-slot request 'mind-body) read))
	     (results '())
	     (result (with-output-to-string
		       (lambda ()
			 (enter-front-court
			  (lambda (expr)
			    (guard
			     (ex (else
				  (receive
				   (title msg args rest)
				   (condition->fields (if (uncaught-exception? ex)
							  (uncaught-exception-reason ex)
							  ex))
				   (format #t "~a ~a ~a\n" title msg args))
				  (set! results (list ex))))
			     (call-with-values (lambda () (eval expr))
			       (lambda args (set! results args)))))
			  expr)))))
	(make-message
	 (property 'content-type text/plain)
	 (property
	  'mind-body
	  (xml-format
	   (make-xml-element
	    'evaluation-result namespace-mind eval-result-attribute-node-list
	    (node-list
	     (make-xml-element
	      'output namespace-mind '() result)
	     (make-xml-element
	      'results namespace-mind '()
	      (map
	       (lambda (r)
		 (make-xml-element
		  'result namespace-mind '()
		  (call-with-output-string (lambda (p) (write r p)))))
	       results))))))))))
   (else (make-message
	  http-property-soap+xml
	  (property 'mind-body (xml-format (make-soap-sender-error
					    (literal (get-slot request 'authorization))
					    "eval denied"
					    (empty-node-list) #f)))))))

(register-agreement-handler! 'eval eval-reply)

(define (frame->get-place auth obj request)
  (mime-format
   (node-list
    (if auth (frame-metadata obj) (public-meta-info obj))
    (and-let* ((bdy (and auth (message-body/plain (aggregate-meta obj)))
		    #;(guard
		     (ex (else (literal ex)))
		     (message-body/plain
		      (if auth (aggregate-meta obj) (public-read obj)))))
	       ((or (and (string? bdy) 
			 (< (string-length bdy)
			    $http-max-direct-string-length))
		    (and (a:blob? bdy)
			 (let ((size (or (a:blob-size bdy)
					 (blob-notation-size
					  *the-registered-stores*
					  bdy #f))))
			   (and size
				(< size
				   $http-max-direct-string-length)))))))
	      (make-xml-element
	       'output namespace-mind
	       (list
		(make-xml-attribute
		 'method #f "text")
		(make-xml-attribute
		 'media-type #f
		 (or (fget obj 'content-type)
		     "text/plain")))
	       ((if (string? bdy) literal body->string) bdy))))))

(define (frame->get-meta auth obj request)
  (values (xml-format (if auth (frame-metadata obj) (public-meta-info obj)))
	  text/xml))

(define (frame->get-digest auth obj request)
  (values (xml-digest-simple
	   (if auth
	       (let ((oid (aggregate-entity obj)))
		 (!start (maybe-trigger-completion! oid request)
			 (dbgname (oid->string oid) "sync-digest-complete ~a"))
		 (frame-signature obj))
	       (public-meta-info obj)))
	  text/xml))

(define (frame->get-privileges auth obj request)
  (values
   (if (quorum-local? (replicates-of obj))
       (let* ((caller (get-slot request 'dc-creator))
	      (from (or (find-local-frame-by-id
			 caller
			 'frame->get-privileges)
			(raise
			 (make-object-not-available-condition
			  caller
			  (aggregate-entity obj)
			  'frame->get-privileges)))))
	 (set-slot! request 'capabilities
		    (capability-intersection
		     (cons
		      (list (aggregate-entity from))
		      (public-capabilities))
		     (get-slot request 'capabilities)
		     (fget from 'capabilities)))
	 (xml-format (make-capability-statement obj request)))
       (make-condition
	&http-effective-condition 'status 406
	'message
	(format "Query for privileges from ~a rejected. ~a" auth (public-oid))))
   text/xml))

(define (frame->get-blob auth obj request)
  (if auth
      (let* ((path (parsed-locator (get-slot request 'dc-identifier)))
	     (valid (or (let ((b (fget obj 'mind-body)))
			  (if (a:blob? b)
			      (string=? (cadr path) (symbol->string (blob-sha256 b)))
			      (and (string? b)
				   (string=? (cadr path) (sha256-digest b))
				   b)))
			(fx>=
			 (sql-ref
			  (within-time!
			   1 ;; FIXME: almost no wait, should be configurable
			   (spool-db-select/prepared
			    "select count(*) from CoreBLOBs where oid = ?1 and blob = ?2"
			    (oid->string (aggregate-entity obj)) (cadr path)))
			  0 0)
			 1))))
	(values
	 (cond
	  ((eq? valid #t)
	   (let ((target (a:make-blob (string->symbol (cadr path)) #f #f #f #f)))
	     (or (fetch-blob-notation *the-registered-stores* target #f)
		 (begin
		   (!start (fetch-blob-notation *the-remote-stores* target #f) (cadr path))
		   #f))))
	  ((string? valid) valid)
	  (else #f))
	 "application/octet-stream"))
      (values #f #f)))

(define (make-get-*-reply handler request-name)
  (lambda (oid request)
    (guard
     (ex (else (log-condition (format "E: Q ~a while answering ~a" oid request-name) ex)
	       (handle-web-conditions ex request)))
     (let ((obj (or (and (fx< ($defer-sync-replies-to-stable-state) 2)
			 (find-already-loaded-frame-by-id oid))
		    (within-time! (respond-timeout-interval)
				  (find-local-frame-by-id oid request-name))
		    (begin
		      ;; (delete-is-sync-flag! oid)
		      ;; Loops if called like this:
		      ;; (resync-now! oid)
		      #f)))
	   (peer-auth (get-slot request 'authorization)))
       (if obj
	   (let ((auth (or (member peer-auth (quorum-others (default-replicates-of obj)))
			   (and-let* (((public-equivalent?
					(fget obj 'mind-action-document)
					(replicates-of obj)))
				      (p (fget obj 'protection)))
				     ((make-service-level p (public-capabilities))
				      (my-oid))))))

	     (and-let* ((h ((host-lookup) peer-auth)))
		       (http-update-alive-time! h #t))

	     ;; If the transaction, which triggered a replication, is not
	     ;; yet finished, we would just deliver garbage to be
	     ;; resynchronized the next moment.  So we wait.
	     ;; 		    (if (is-in-transaction? oid)
	     ;; 			(or (and auth (eqv? 2 (quorum-size (replicates-of obj))))
	     ;; 			    ;;  muß nicht: (member auth (replicates-of obj))
	     ;; 			    ;; But if we are one of only two voters, and the other one needs
	     ;; 			    ;; meta-infos its likely he lost his invitation.
	     ;; 			    (with-timeout (respond-timeout-interval)
	     ;; 					  (lambda () (with-mutex (respond!-mutex oid) #t)))))

	     ;; BEWARE This could still fail (and be resynched the next
	     ;; moment), if the next transaction is completed before we
	     ;; completed.  But chances are small, so we risk resync.
	     (receive
	      (value type) (handler auth obj request)
	      (if value
		  (make-message
		   (property 'caller (aggregate-entity obj))
		   (property 'dc-identifier (oid->string (aggregate-entity obj)))
		   (property 'dc-creator (fget obj 'dc-creator))
		   (property 'dc-date (fget obj 'dc-date))
		   (property 'content-type type)
		   (property 'mind-body value))
		  (make-message
		   (property 'http-status 404)
		   (property 'mind-body (format "object ~a not available" oid))))))
	   (make-message
	    (property 'http-status 404)
	    (property 'mind-body (format "object ~a not available" oid))))))))

(register-agreement-handler! 'digest (make-get-*-reply frame->get-digest 'digest))
(register-agreement-handler! 'get-place (make-get-*-reply frame->get-place 'get-place))
(register-agreement-handler! 'get-meta (make-get-*-reply frame->get-meta 'get-meta))
(register-agreement-handler! 'get-blob (make-get-*-reply frame->get-blob 'get-blob))
(register-agreement-handler! 'get-privileges (make-get-*-reply frame->get-privileges 'get-privileges))

(: askemos-sync (:message: string -> :message:))
(define (askemos-sync request action)
  (let* ((path (parsed-locator (get-slot request 'dc-identifier)))
         (public (find-frames-by-id (public-oid)))
         (oid (or (string->oid (car path))
		  ((public-context) (car path))
		  (and-let* ((peer (get-slot request 'ssl-peer-certificate)))
			    (mesh-cert-o peer)))))
    (cond
     ((not oid) (make-soap-error 400 path "bad request"))
     ;; silently fail here.
     ((oid=? oid (my-oid)) (make-message))
     (else (call-agreement-handler oid request (string->symbol action))))))
