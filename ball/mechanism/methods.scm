;; (C) 2000, 2001, 2004 Jörg F. Wittenberger see http://www.askemos.org

;;** Method Tables

;; A VMT is a (memoized) evaluated slot value.

;; Look for "method" elements in the parsed xml document "tree".
;; Evaluate the concatenation of the content of contained
;; programlistings as methods.

(define (sweet-read-all-expressions)
  (sweet-read-all (current-input-port)))

(define (nu-eval-method-listing action-type tree)
  (let loop ((methods
	      (let ((backward-compatible (select-elements (%children tree) 'method)))
		(if (node-list-empty? backward-compatible)
		    ((sxpath '(* method)) tree)
		    backward-compatible))))
    (if (null? methods)
        #f
        (let* ((method (node-list-first methods))
               (listing (select-elements (%children method)
                                         'programlisting))
               (type (attribute-string-defaulted 'type method "write"))
               (mename (attribute-string-defaulted  'this method "me"))
               (reqname (attribute-string-defaulted 'request method "msg"))
               (planam (attribute-string-defaulted 'plan method "result")))
          (cond
           ;; Next a sanity check.
           ((not (string=? (symbol->string action-type) type))
            (loop (node-list-rest methods)))
           ((or (equal? mename "") (equal? reqname ""))
            (error
             "Make sure that the action document complies with the DTD."))
           (else
            ;; Construct the procedure.
            (eval-sane
             `(lambda (,(string->symbol mename)
                       ,(string->symbol reqname)
                       . ,(if (eq? action-type 'accept)
                              (list (string->symbol planam))
                              '()))
                . ,(with-input-from-string (data listing) sweet-read-all-expressions)))))))))

;; remove this parameter
(define $cfg-voters (make-parameter '()))

(define (nu-action-init default-read default-write)
  (define public-read
    (delay
      (make-message
       (property
	'capabilities
	(let ((capas
	       (make-a-transient-copy-of-object	(public-capabilities))))
	  ;; Subset of the others public with our private protection.
	  ;; This should really be expressed with set operations, not
	  ;; lists.
	  (apply append capas
		 (map (lambda (public)
			(map (lambda (c) (cons public (cdr c))) capas))
		      (filter oid? ($cfg-voters))))))
       (property 'location '())
       (property 'destination '())
       (property 'dc-creator (public-oid)))))
  (define (default-action action-type)
    ;; Jerry: never mess with this function!
    (cond
     ((eq? action-type 'read) default-read)
     ((eq? action-type 'write)
      ;; maybe we better replicate (i.e., change the #f to second
      ;; position)?
      (cons #f default-write))
     (else (error "can't understand \"~a\" for default action"
                  action-type))))
  (define (pick-from oid action-type requestor context)
    (let ((obj (or (if
                    (eqv? (aggregate-entity requestor)
			  (if (eq? oid one-oid) (public-oid) oid))
                    (aggregate-meta requestor)
                    (let* ((obj (or (find-local-frame-by-id oid 'find-action-document)
				    (resync-now! oid context) ;; FIXME: prefer the next line instead of this.
				    ;; (find-frame-by-id/asynchroneous+quorum oid context)
				    (raise (make-object-not-available-condition oid context 'find-action-document))))
			   (ad (fget obj 'mind-action-document)))
		      (if (and ad (public-equivalent? ad (replicates-of obj)))
			  (call-subject! ; call-local-subject ???
			   obj
			   (replicates-of requestor)
			   'read (force public-read))
			  (raise-service-unavailable-condition
			   (format "ActionDocument ~a not public at ~a" ad oid)))))
                   (raise (format #f "could not load action ~a from ~a" oid requestor)))))
      (let* ((tree (or (document-element (message-body obj))
		       (error (string-append "action document " (symbol->string oid)
					     " unparseable"))))
	     (method (case action-type
		       ((write)
			(let ((propose (nu-eval-method-listing
					'propose tree))
			      (accept (nu-eval-method-listing
				       'accept tree)))
			  (if accept
			      (cons propose accept)
			      (nu-eval-method-listing 'write tree))))
		       ((read) (nu-eval-method-listing action-type tree))
		       (else #f))))
	(if method method (default-action action-type)))))
  (define (nu-action-uncached oid action-type requestor quorum)
    (if oid
        (pick-from oid action-type requestor quorum)
        (default-action action-type)))

  (set! nu-action (memoize nu-action-uncached oid=? eq? (lambda (a b) #t) (lambda (a b) #t))))

;;** full call

(define transaction-mailbox oid-channel-mailbox)

(define close-transaction-mailbox oid-cannel-mailbox-close!)

;; A subject is an autonomous instance, which knows how to answer a
;; message and responds under it's own control.

(define (call-local-subject! context-quorum subject type message)
;;  (logerr "Location (~a) : ~a\nDestination (~a) : ~a\n"
;;          (length (or (get-slot message 'location) '())) (get-slot message 'location)
;;          (length (get-slot message 'destination)) (get-slot message 'destination))
  (let ((action (nu-action (fget subject 'mind-action-document)
                           type
                           subject context-quorum)))
    (cond
     ;; Read actions are idempotent, don't need a
     ;; transaction and their result might be cached
     ;; here (but isn't yet).
     ((eq? type 'read) (respond! subject action respond-local #f message))
     ;; Write action need a transaction and their result
     ;; must not be cached.
     ((eq? type 'write)
      (respond! subject action respond-replicated #t message))
     (else (error "Illegal action ~a\n" action)))))

(define call-subject-remote-answer-timeout (make-shared-parameter #f))

;; Subscribe to the place and wait for the transaction to complete.

(define (call-subject-wait-on-version! oid version quorum tm message)
  (let* ((mbox (transaction-mailbox oid version))
	 (result
	  (let loop ((n 5))
	    (guard
	     (ex ((fx>= n 0)
		  ((request-reply)
		   (quorum-others quorum) oid version
		   (lambda (r)
		     (let ((auth (get-slot r 'authorization)))
		       (if auth
			   (begin
			     (case (get-slot r 'http-status)
			       ((404 408 504))
			       ((412)
				(logagree "~a Reply notice: out of sync for ~a version ~a\n"
					  auth oid version)
				(transaction-send-message-version! oid version r))
			       (else
				(logagree "~a Reply notice recieved for ~a version ~a\n"
					  auth oid version)
				(transaction-send-message-version! oid version r))))
			   (logagree "W: failed to retrieve reply on ~a: reply not authorized cert ~a.\n" oid (get-slot r 'ssl-peer-certificate))))))
		  (loop (sub1 n)))
		 (else (timeout-object)))
	     (within-time
	      tm
	      (let loop ((reply (receive-message! mbox)))
		;; KLUDGE: A crude way to
		;; filter messages.
		(cond
		 ((and-let* (((frame? reply))
			     (b (get-slot reply 'mind-body))
			     ((pair? b))
			     (tag (car b)))
			    (or (eq? tag 'ready) (eq? tag 'echo)))
		  (loop (receive-message! mbox)))
		 ((timeout-condition? reply) (loop (receive-message! mbox)))
		 (else reply))))))))
    (close-transaction-mailbox oid version mbox)
    result))

(define (call-subject-wait-for-remote-reply! subject reply-channel message)
  (let ((oid (aggregate-entity subject))
	(version (or (and-let* ((v (fget subject 'version)))
			       (version-serial v)) 0))
	(quorum (replicates-of subject)))
    (logagree "wait for reply on ~a ~a from ~a\n" oid version (quorum-others quorum))
    (let ((reply (call-subject-wait-on-version!
		  oid version quorum
		  (respond-timeout-interval)
		  message)))
      (logagree "received reply on ~a ~a: ~a\n" oid version reply)
      (mailbox-send! reply-channel
		     (cond
		      ((timeout-object? reply)
		       (make-condition &service-unavailable
				       'message (oid->string oid)
				       'status 503))
		      ((frame? reply) reply)
		      ((condition? reply)
		       (condition->message 'call-subject-wait-for-remote-reply!message
					   message reply))
		      (else
		       (logerr
			"call-subject-wait-for-remote-reply! suspect result ~s \n"
			reply)
		       (make-condition &service-unavailable
				       'message (oid->string oid)
				       'status 503))))
      (or (cond
	   ((timeout-condition? reply)
	    (delete-is-sync-flag! oid)
	    (find-frame-by-id/asynchroneous+quorum oid quorum))
	   ;; We used to resync in this case too.  That proved to be
	   ;; rather slow.  Proposal: include a version info about
	   ;; 'subject' in the reply and adjust the local channel
	   ;; accordingly.
	   ((frame? reply)
	    ;; HACK
	    (and-let* ((v (fget subject 'version))
		       ((eqv? version (version-serial v))))
		      (fset! subject 'version (cons (add1 version) (version-chks v))))
	    subject)
	   (else #f))
	  (raise-service-unavailable-condition
	   (format "Can't reach any host of ~a"
		   (quorum-others quorum)))))))

(define (call-subject!-implementation subject from-quorum type message)
  (let ((quorum (replicates-of subject)))
    (if (quorum-local? quorum)
        (if (and (eq? type 'write) (not (eq? from-quorum quorum)))
	    (let ((others (quorum-members-to-inform
			   from-quorum (fget subject 'version) quorum 0)))
	      (if (pair? others)
		  (begin
		    (logagree "replicating request on ~a to ~a\n"
			      (aggregate-entity subject) others)
		    (let* ((qa ((replicate-call) #f
				others (quotient (add1 (length others)) 2)
				subject type message))
			   (reply (guard
				   (ex ((service-unavailable-condition? ex)
					(or (force qa) (raise ex))))
				   (call-local-subject! quorum subject type message))))
		      (and-let*
		       ((reply-channel (get-slot message 'reply-channel)))
		       (mailbox-send! reply-channel reply))
		      reply))
		  (call-local-subject! quorum subject type message)))
	    (call-local-subject! quorum subject type message))
	;; KLUDGE: The base case is the alternative part here.  The
	;; local short cut is useful for stable system, but not valid
	;; for instable legal systems.  However who needs the latter?
        (if (public-equivalent? (fget subject 'mind-action-document) quorum)
	    (call-local-subject! quorum subject 'read message)
	    (if (eq? type 'write)
		(and-let*
		 ((reply-channel (get-slot message 'reply-channel)))
		 (let ((others (quorum-members-to-inform
				from-quorum (fget subject 'version) quorum 0)))
		   (if (null? others)
		       (call-subject-wait-for-remote-reply! subject reply-channel message)
		       (begin
			 ;; FIXME: we could wait for answers from
			 ;; other members to increase reliability.
			 (logagree "replicating request on ~a to ~a out of ~a\n"
				   (aggregate-entity subject) others (quorum-others quorum))
			 ;; TODO: don't depend on the peer; merge with wait-for-remote...
			 ((replicate-call)
			  reply-channel
			  others (quotient (add1 (length others)) 2)
			  subject type message)
			 ;; END TODO 
			 ))))
		;; TODO: this loop looks as if it should be moved to http/client
		(let ((mbox (make-mailbox (aggregate-entity subject))))
		  (let loop ((seen '())
			     (round 1)
			     (retries (quorum-size quorum)))
		    (if (eqv? retries 0)
			(if (eqv? round 2)
			    (raise-service-unavailable-condition
			     (format "Can't reach any host of ~a"
				     (quorum-others quorum)))
			    (loop '() 2 (quorum-size quorum)))
			(let already ((n (random (quorum-size quorum))))
			  (if (memv n seen)
			      (already (random (quorum-size quorum)))
			      (let ((other (apply
					    (host-lookup)
					    (list-ref (quorum-others quorum) n)
					    (if (eqv? round 1) '(nowait) '()))))
				(if (and other (or (eqv? round 2)
						   ((host-alive?) other)))
				    (let ((tmo
					   (and #f (register-timeout-message!
						    (call-subject-remote-answer-timeout)
						    mbox))))
				      (logagree "forwarding ~a => ~a to ~a\n"
						(get-slot message 'caller)
						(aggregate-entity subject) other)
				      
				      ((forward-call) mbox other subject type message)
				      (let ((result (receive-message! mbox)))
					(if (timeout-condition? result)
					    (loop (cons n seen) round (sub1 retries))
					    (begin
					      (cancel-timeout-message! tmo)
					      (cond
					       ((frame? result)
						(case (get-slot result 'http-status)
						  ((equal? status 408)
						   (loop (cons n seen) round (sub1 retries)))
						  ((equal? status 406)
						   (logagree "Not Accepted ~a => ~a to ~a\n~a\n"
							     (get-slot message 'caller)
							     (aggregate-entity subject)
							     other
							     (message-body/plain message))
						   result)
						  (else result)))
					       (else (raise result)))))))
				    (loop (cons n seen) round (sub1 retries))))))))))))))

(set-call-subject! call-subject!-implementation)

;; If (global-proxy-mode) is #t, all servers forward all messages from
;; browsers among each other.  This brings the single point of failure
;; to the local protocol adaptor instead of the remote end.  Use with
;; CAUTION.

(define global-proxy-mode (make-shared-parameter #f))
