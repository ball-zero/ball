;; (C) 2004 Peter Hochgemuth see http://www.askemos.org
;; This is not yet alpha quality code!
;; see mechanism/protocol/http/webdav.scm for the protocol adapter part

;;** webdav.scm

(cond-expand
 (chicken
  (define-syntax dav:self-reference (syntax-rules  () ((_ self) (self 'get 'id))))
  (define-syntax dav:property
    (syntax-rules () ((_ name value) (user:property name namespace-dav value))))
  )
 (else
  (define-macro (dav:self-reference self) `(,self 'get 'id))
  (define-macro (dav:property name value)
	 `(user:property ,name namespace-dav ,value))
  ))





(define (short-response-message status . args)
  (define slots (make-message
		 (property 'http-status status)
		 ;; (property 'content-type text/xml)
		 ;;
		 ;; FIXME: this property should never have made it
		 ;; into the source at all!
		 (property 'no-action #t)))
  (let loop ((args args))
    (if (null? args)
	(if (and (get-slot slots 'body/parsed-xml)
		 (not (get-slot slots 'content-type)))
	    (begin (set-slot! slots 'content-type text/xml) slots)
	    slots)
	(cond
	 ;; ((property? (car args))
	 ;;  (set-slot! slots (property-name (car args)) (property-value (car args)))
	 ;;  (loop (cdr args)))
	 ;; This should never match!
	 ((pair? (car args))
	  (set-slot! slots (caar args) (cdar args))
	  (loop (cdr args)))
	 (else 
	  (case (car args)
	    ((error error:)
	     (set-slot! slots 'http-status 400)
	     (set-slot! slots 'mind-body (cadr args))
	     (loop (cddr args)))
	    ((body body:)
	     (set-slot! slots 'body/parsed-xml (cadr args))
	     (loop (cddr args)))
	    (else
	     (raise
	      (format "short-response-message: argument error ~a" args)))
	    ))))))

(define (user:property name ns value)
  (and value (make-xml-element name ns '() value)))

(define (dav:property-name name)
  (make-xml-element name namespace-dav '() (empty-node-list)))

;; read access
;; properties known by default - stored somewhere in place slots
;(%early-once-only
 (define *property-handler-namespaces* (make-symbol-table))
 (define *property-handler-default-ns* (make-symbol-table))

;; default reader - select a property from dav:properties by <ns:gi/>
(define (default-get-property-handler me msg prop)
  (let ((props (me 'dav:properties))
	(my-gi (gi prop))
	(my-ns (ns prop)))
    (node-list-filter
     (if my-ns
	 (lambda (e) (and (eq? my-gi (gi e))
			  (eq? my-ns (ns e))))
	 (lambda (e) (eq? my-gi (gi e))))
     (children 
      (or (and (eq? (gi props) 'prop)
	       (eq? (xml-element-ns props) namespace-dav)
	       props)
	  '())))))

(define (default-set-property-handler me msg prop)
  (node-list 
   (make-xml-element (gi prop) (ns prop) '() (children prop))))

(define (register-property-handler prop reader writer)
  (let ((t (if (not (ns prop)) 
	       *property-handler-default-ns*
	       (or (hash-table-ref/default *property-handler-namespaces* (ns prop) #f)
		   (let ((t (make-symbol-table)))
		     (hash-table-set! *property-handler-namespaces* (ns prop) t)
		     t)))))
    (if (hash-table-ref/default t (gi prop) #f)
	(raise
	 (format "Property handler for \"~a\" already registered!" 
		 (if (ns prop)
		     (string->symbol
		      (string-append (symbol->string (ns prop)) ":" (symbol->string (gi prop))))
		     (gi prop))))
	(hash-table-set! t (gi prop) (vector reader writer)))))

(define property-handler-default-result
  (vector default-get-property-handler
	  default-set-property-handler))

 (define (property-handler prop)
   (hash-table-ref/default
    (or (and (ns prop)
	     (hash-table-ref/default *property-handler-namespaces* (ns prop) #f))
	*property-handler-default-ns*)
    (gi prop) property-handler-default-result))

 (define (get-property me msg prop)
   ((vector-ref (property-handler prop) 0) me msg prop))

 (define (set-property! me msg prop)
   ((vector-ref (property-handler prop) 1) me msg prop))

 (define (life-property-handler property-name namespace reader writer)
   (let ((element (make-xml-element property-name namespace '() '())))
     ;;(set! *fife-properties* (cons element *fife-properties*))
     (register-property-handler element reader writer)))

 (life-property-handler
  'creationdate namespace-dav
  (lambda (me msg prop)
    (node-list
     (dav:property 
      'creationdate
      (iso-8601-timestring (me 'dc-date)))))
  (lambda (me msg val) #f)) ;; read only

 (life-property-handler
  'displayname namespace-dav
  (lambda (me msg prop)
    (let ((location (msg 'location)))
      ;;  maybe? (uri-quote (car location))
      (cond ((pair? location)
	     (node-list (dav:property
			'displayname (car location))))
	    (else
	     (default-get-property-handler
	       me msg (dav:property-name 'displayname))))))
  (lambda (me msg val) #f))

;;protect this from trying to be written to!
 (define read-only-dav:properties
   '(creationdate 
     displayname
     getcontentlength
     getcontenttype
     getlastmodified
     resourcetype))

 (define place-dav:property-names
   (map dav:property-name read-only-dav:properties))

;; properties handled special
(define slot-dav:properties
  '(content-language          ;; getcontentlanguage
    etag                      ;; getetag
    resourcetype))            ;; ro resourcetype
;)

;; per definition: if set (me 'dav:properties) is a D:prop element
(define (dav:property-value me . select)
  ;;returns a not empty node-list, its data value or #f 
  (and-let* ((slot  (me 'dav:properties))
	     (      (and (xml-element? slot)
			 (eq? (gi slot)'prop)
	                 (eq? (ns slot) namespace-dav)))
	     (child (children slot)) ;; must be xml-element or nl!
	     (result(if (null? select) child 
			(select-elements child (car select))))
	     (      (not (node-list-empty? result))))
	    (if (and (pair? select) (pair? (cdr select)))
		(cond ((eq? (cadr select) 'data)
		       (data result))
		      ((eq? (cadr select) 'children)
		       (children result))
		      (else result))
		result)))

(define (dav:creationdate me msg)
  (dav:property 'creationdate (iso-8601-timestring (me 'dc-date))))

(define (dav:displayname me msg)
  (let ((location (msg 'location)))
    (cond ((pair? location) ;;  maybe? (uri-quote (car location))
	    (dav:property 'displayname (car location))) 
	  (else
	   (empty-node-list)))))

(define *dav:depth-infinity-forbidden* #t)
;; if requested, send "404 Not Found" for collections
(define *send-contentinfo-for-collections* #f)

(define (dav:getcontentlength me msg)
  (if (and (dav:collection? me msg) 
 	   (not *send-contentinfo-for-collections*))
      (empty-node-list)
      ;; check xml-body!
      (dav:property 'getcontentlength
		    (number->string (or (dsssl-message-body/plain-size me)
					(begin
					  (logerr "~a bogus body ~s\n"
						  (dav:self-reference me)
						  (dsssl-message-body/plain me))
					  0))))))

(define (dav:getcontenttype me msg)
  (dav:property 'getcontenttype (me 'content-type)))

;; must be defined if a response sets the Last-Modified header
;; FIXME protocol adapter must not override the value and usercode
;; should (set-slot! answer 'last-modified (me 'dc-date)) Last-Modified: 
(define (dav:getlastmodified me msg)
  (dav:property 'getlastmodified
		(rfc-822-timestring
		 (time-utc->date
		  (date->time-utc (askemos:dc-date
				   (or (me 'last-modified)
				       (me 'dc-date))))
		  0))))

;;need a slot for this; must be included as HTTP Content-Language:
(define (dav:getcontentlanguage me msg)
  (dav:property 'getcontentlanguage
		(or (me 'dc-language)
		    (dav:property-value me 'content-language 'data))))


; see section 3.11 of [RFC2068]
; set if a GET would respond with an Etag header
(define (dav:getetag me msg)
  (dav:property 'getetag ;; should be a function of request URL
		(dav:property-value me 'etag 'data)))

(define (dav:lockdiscovery me msg prop-filter)
  (let* ((lockd (dav:property-value me 'lockdiscovery))
	 ;; here filter tells if we say yes to lockdiscovery at all
	 (dav2 (and lockd (prop-filter lockd))))
    (cond ((or (not dav2) (node-list-empty? dav2))
	   (empty-node-list))
	  ;; OK we send lockdiscovery - now filter active locks
	  (else
	   (dav:property 'lockdiscovery
			 (node-list-reduce
			  (children lockd)
			  (lambda (i activelock)
			    (node-list (prop-filter activelock) i))
			  (empty-node-list)))))))

(define (make-dav:activelock scope type depth owner? timeout? locktoken?)
  (let ((scope (if (member scope '(exclusive shared)) scope
		   (error "illegal dav:lockscope specified!")))
	(type  (if (member type '(write read)) type
		   (error "illegal dav:locktype specified!")))
	(depth (if (or (string=? depth "0") 
		       (string=? depth "1") 
		       (string=? depth "infinity"))
		   depth
		   (error "illegal dav:depth specified!"))))
    (dav:property 
     'activelock
     (node-list 
      (dav:property 'lockscope (dav:property-name scope))
      (dav:property 'locktype  (dav:property-name type))
      (dav:property 'depth     depth)
      (dav:property 'owner     owner?) ;;should be an entry-point's OID
      (dav:property 'timeout   timeout?)
      (dav:property 'locktoken locktoken?)
      ))))

(define (dav:resourcetype me msg)
  (or (dav:property-value me 'resourcetype)
      (dav:property-name 'resourcetype)))

(define (dav:resourcetype-collection)
  (dav:property 
       'resourcetype
       (dav:property-name 'collection)))

(define (dav:collection? me msg)
  (eq? 'collection 
       (gi (node-list-first (dav:property-value me 'resourcetype 'children)))))

(define (dav:source me msg prop-filter)
  (let ((l (node-list-reduce
	    (or (dav:property-value me 'source 'children) '())
	    (lambda (i link)
	      (node-list (prop-filter link) i))
	    (empty-node-list))))
    (dav:property 'source (if (node-list-empty? l) #f l))))

(define (make-dav:source me msg args)
  (dav:property 
   'source 
   (node-list-map
    (lambda (link) 
      (dav:property 
       'link 
       (node-list 
	(dav:property 'src (car link)) ;; use (uri-quote ) here?
	(dav:property 'dst (cadr link)))))
    args)))

(define dav:all-supportedlock
  (sxml `(D:supportedlock
	  (@ (@ (*NAMESPACES* (,namespace-dav ,namespace-dav-str D))))
	  (D:lockentry (D:lockscope (D:exclusive))
		       (D:locktype (D:write))))))

(define dav:default-supportedlock dav:all-supportedlock)

(define (dav:supportedlock me msg)
  (or (dav:property-value me 'supportedlock)
      dav:default-supportedlock))

; locktype is of
;'((exclusive . read) 
;  (exclusive . write)
;  (shared . read) 
;  (shared . write))
(define (make-dav:supportedlock locktypes)
  (dav:property 
   'supportedlock 
   (node-list
    (map 
     (lambda (locktype)
       (make-dav:lockentry (car locktype) (cdr locktype)))
     locktypes))))

(define (make-dav:lockentry scope type)
  (let ((scope (if (member scope '(exclusive shared)) scope
		   (error "illegal dav:lockscope specified!")))
	(type  (if (member type '(write read)) type
		   (error "illegal dav:locktype specified!"))))
    (dav:property 
     'lockentry 
     (node-list 
      (dav:property 'lockscope (dav:property-name scope))
      (dav:property 'locktype  (dav:property-name type))))))
;;
;;<default-actions>
;; <action type="text/abc" match="pcre">oid or name</action>
;;</default-actions>
(define (default-action-property me msg type body)
  (let* ((type (if (not (string? type)) (to-string type) type))
	 ;;(tree (and (string=? type "text/xml") (xml-parse body)))
	 ;;(stylesheet (and tree (eq? (gi (document-element tree))
	 ;;			    'stylesheet)))
	 (actions
	  (node-list-reduce 
	   (or (dav:property-value me 'default-actions 'children) '())
	   (lambda (i n)
	     (let ((atype (attribute-string 'type n))
		   (match (attribute-string 'match n)))
	       (if (and atype (string=? atype type)
			(or (not match) ((pcre->proc match) body)))
		   (cons (data n) i)
		   i)))
	   '())))
    (and (pair? actions) (car actions))))

(define (make-default-action-property args)
  (dav:property 
   'default-actions
   (map
    (lambda (arg) 
      (cond 
       ((xml-element? arg) arg)
       ((pair? arg)  ;;content-type name/oid [match]
	(make-xml-element 
	 'action namespace-dav 
	 (cons
	  (make-xml-attribute 'type #f (car arg))
	  (if (eq? 3 (length arg))
	      (list (make-xml-attribute 'match #f (caddr arg))) '()))
	 (cadr arg)))
       (else
	(raise (format " make-dav:default-actions wrong argument ~a" arg)))))
    args)))

; posible defaults set in (me 'dav:properties)
;'(contentlanguage "") 
;'(etag "") 
;'collection "collection"
;'(resourcetype collection)
;'(source <xml>) 
;'(supportedlock <xml>)
;<xml>
;'(PropertyName value)
;(make-dav:supportedlock '((exclusive . write)))
(define (dav:property-init! me . args)
  (let loop
      ((args args)
       (lang '()) 
       (etag '()) ;"what is an etag?"
       (type (list (dav:property-name 'resourcetype))) 
       (src  '()) 
       (lock '())
       (user '())) 
    (cond ((null? args)
	   (dav:property 'prop
			 (append lang etag type src lock user)))
	  ((symbol? (car args))
	   (case (car args)
	     ((collection)
	      (loop (cdr args) lang etag  
		    (list (dav:property 'resourcetype 
				  (dav:property-name 'collection)))
		    src lock user))
	     (else (raise
		    (format 
		     "dav:property-init! unknown symbol argument: ~a"
		     (car args))))))
	  ((pair? (car args))
	   (case (caar args)
	     ((content-language)
	      (loop (cdr args) 
		    (list (dav:property (caar args) (cdar args)))
		    etag type src lock user))
	     ((etag)
	      (loop (cdr args) lang (list (dav:property 
				     (caar args) (cdar args))) 
		    type src lock user))
	     ((resourcetype)
	      (loop (cdr args) lang etag 
		    (list (dav:property 'resourcetype 
				  (dav:property-name (cdar args))))
		    src lock user))
	     ((source)
	      (loop (cdr args) lang etag type (list (cdar args)) lock user))
	     ((supportedlock)
	      (loop (cdr args) lang etag type src (list (cdar args)) user))
	     (else
	      (loop (cdr args) lang etag type src lock 
		    (cons (dav:property (caar args) (cdar args))
			  user)))))
	  ((string? (car args))
	   (loop (cons (string->symbol (car args)) (cdr args))
		 lang etag type src lock user))
	  ((xml-element? (car args))
	   (loop (cdr args) lang etag type src lock 
		 (cons (car args) user)))
	  (else 
	   (raise
	    (format
	     "dav:property-init! unknown argument: ~a"
	     (car args)))))))


(define (get-dav:propnames me msg prop-filter)
  (dav:property
   'propstat
   (node-list 
    (dav:property
     'prop
     (node-list-reduce
      (or (dav:property-value me) '())
      (lambda (i n)
	(if (member (gi n) slot-dav:properties) i
	    (node-list (prop-filter (user:property (gi n) (ns n) '())) i)))
      (append
       ;; that are props known by default
       place-dav:property-names 
       ;; and that are default optional props
       (if (or (me 'dc-language)
	       (dav:property-value me 'content-language)) 
	   (dav:property-name 'getcontentlanguage)
	   (empty-node-list))
       (if (or (me 'etag)
	       (dav:property-value me 'etag))
	   (dav:property-name 'getetag)
	   (empty-node-list)))))
    (dav:property 'status "HTTP/1.1 200 OK")
    ;; (dav:property 'responsedescription "some text")
    )))

(define (get-dav:allprop me msg prop-filter collection?)
  (dav:property
   'propstat
   (node-list 
    (dav:property
     'prop
     (node-list-reduce
      (or (dav:property-value me) '())
      (lambda (i n)
	(if (member (gi n) slot-dav:properties) i
	    (node-list (prop-filter n) i)))
      (node-list
       (dav:creationdate me msg)
       (dav:displayname me msg)
       (if collection?
	   (empty-node-list)	   
	   (dav:getcontentlength me msg))
       (dav:getcontenttype me msg)
       (dav:getlastmodified me msg)
       (dav:getcontentlanguage me msg)
       (dav:getetag me msg)
       (dav:lockdiscovery me msg prop-filter)
       (if collection?
	   (dav:resourcetype-collection)
	   (dav:resourcetype me msg))
       ;;(dav:source me msg prop-filter)
       (dav:supportedlock me msg)
       )))
    (dav:property 'status "HTTP/1.1 200 OK")
    ;; (dav:property 'responsedescription "some text")
    )))

(define (get-dav:prop me msg prop-filter prop collection?)
  (let ((allprops ((sxpath '(prop *)) (get-dav:allprop me msg identity collection?))))
    (let loop ((requested (children prop))
	       (found     '())  ;;-> status 200 OK
	       (forbidden '())  ;;-> error  403 Forbidden
	       (missed    '())) ;;-> error  404 Not Found 
      (if (node-list-empty? requested)
	  (node-list 
	   (if (null? found) (empty-node-list)
	       (dav:property 
		'propstat (node-list 
			   (dav:property 'prop found)
			   (dav:property 'status "HTTP/1.1 200 OK")
			   ;; (dav:property 'responsedescription "text")
			   )))
	   (if (null? forbidden) (empty-node-list)
	       (dav:property
		'propstat (node-list 
			   (dav:property 'prop forbidden)
			   (dav:property 'status "HTTP/1.1 403 Forbidden")
			   ;; (dav:property 'responsedescription "text")
			   )))
	   (if (null? missed) (empty-node-list)
	       (dav:property
		'propstat (node-list 
			   (dav:property 'prop missed)
			   (dav:property 'status "HTTP/1.1 404 Not Found")
			   ;; (dav:property 'responsedescription "text")
			   ))))
	  (let ((first (node-list-first requested)))
	    (if (xml-element? first)
		(let* ((exist  (select-elements allprops (gi first)))
		       (access (and (not (node-list-empty? exist))
				    (prop-filter exist))))
		  (cond ((and access (not (node-list-empty? access))) 
			 (loop (node-list-rest requested) (node-list access found) 
			       forbidden missed))
			((not (node-list-empty? exist))
			 (loop (node-list-rest requested) found
			       (%node-list-cons first forbidden) missed))
			(else
			 (loop (node-list-rest requested) found forbidden
			       (%node-list-cons first missed)))))
		(begin
		  ;; Leaving the log message here until it might be annoying
		  (logerr "unexpected content '~a' in WebDAV prop element (did it run through the fallback parser?)\n~a\n" first (xml-format prop))
		  (loop (node-list-rest requested) found forbidden missed))))))))

;; Since some companies still don't know sell good software for good
;; money: http://www.lyra.org/pipermail/dav-dev/2002-July/003776.html
;; no default namespaces for WebDAV, always declare namespace prefix!
;;
;; (define dav-defaulted-xmlns-decl
;;   (list (make-xml-attribute 'xmlns #f namespace-dav-str)))

(define dav-xmlns-decl
  (list (make-xml-attribute 'D 'xmlns namespace-dav-str)))

(define (make-prop-filter me msg)
  (let* ((service (make-service-level (me 'protection) (msg 'capabilities)))
	 (my-service (service (my-oid)))
	 (dav2 (dav-level2? (dav-supported-methods me)))
	 (private 
	  (or (and-let* 
	       ((privates (dav:property-value me 'private-property-names)))
		(map gi privates))
	      '())))
    (lambda(node)
      (case (gi node)
	((lockdiscovery supportedlock)
	 (if (and my-service dav2)
	    node
	    (empty-node-list)))
	((activelock)
	;; use instead somethin like: 
	;;  (service (protection-of (data ((sxpath '(owner)) node))))
	(if my-service node (empty-node-list)))
	((link)
	 (if my-service node (empty-node-list)))
	(else
	 (if (memq (gi node) private)
	     (if my-service node (empty-node-list))
	     node))))))

;; Read requests

(define (propfind-request-element body depth)
  (let ((body (document-element body)))
    (make-xml-element
     (gi body) namespace-mind
     (fold (lambda (a i)
	     (if (memq (xml-attribute-name a) '(api depth)) i (cons a i)))
	   (list
	    (make-xml-attribute 'api 'xmlns namespace-mind-str)
	    (make-xml-attribute 'depth #f (or depth "infinity")))
	   (attributes body))
     (children body))))

(: is-propfind-request?-implementation (:message-accessor: --> boolean))
(define (is-propfind-request?-implementation request) 
  (and (is-meta-form? request) 
       (is-mind-element? 
	(document-element (dsssl-message-body request)) 'propfind)))

(is-propfind-request? is-propfind-request?-implementation)

(define (dav-propfind-element me msg . args)
  (let* ((propfind  (document-element (dsssl-message-body msg)))
	 (depth     (attribute-string 'depth propfind))
	 (recursive (cond ((string=? depth "infinity")
			   (if *dav:depth-infinity-forbidden*
			       (raise (make-condition
				       &http-effective-condition
				       'message "PROPFIND with Depth=\"infinity\" not supported"
				       'status 403))
			       propfind))
			  ((string=? depth "1") 
			   (propfind-request-element propfind "0"))
			  (else #f)))
	 (collection? (dav:collection? me msg))
	 (locks #f)
	 (childs      #f)
	 (location    #f)
	 (adoption (prevent-pishing public-oid me msg))
	 (lookup
	  (lambda (name . raise-unavail)
	    (guard 
	     (ex ((and (pair? raise-unavail) (car raise-unavail)
		       (object-not-available-condition? ex)) (raise ex))
		 (else
		  (logerr "propfind-lookup ~a ~a ~a\n" (dav:self-reference me) name (if (message-condition? ex) (condition-message ex) ex))
		  (dav:property
		   'response
		   (node-list
		    (dav:property
		     'href (read-locator (msg 'location-format)
					 (cons name location)))
		    (dav:property
		     'propstat
		     (node-list
		      (dav:property
		       'prop
		       (case (first-child-gi propfind)
			 ((propname) (dav:property-name 'displayname))
			 ((allprop) (dav:property 'displayname name))
			 ((prop)
			  (if (node-list-empty?
			       (select-elements (children (children propfind)) 'displayname))
			      (empty-node-list)
			      (dav:property 'displayname name)))))
		      (dav:property 'status "HTTP/1.1 200 OK")))
		    (if (eq? (first-child-gi propfind) 'prop)
			(let ((nf (node-list-filter
				   (lambda (n)
				     (not (eq? (gi n) 'displayname)))
				   (children (children propfind)))))
			  (if (node-list-empty? nf)
			      (empty-node-list)
			      (dav:property
			       'propstat
			       (node-list
				(dav:property 'prop nf)
				(dav:property
				 'status "HTTP/1.1 404 Not Found")))))
			(empty-node-list))))))
	     (and-let*
	      ((oid (cond
		     ((oid? name) name)
		     ((string? name)
		      (or (string->oid name)
			  (me name)
			  (raise (make-object-not-available-condition name (dav:self-reference me) 'dav-propfind-element))))
		     (else (error "dav-propfind-info invalid name ~a"
				  name)))) ; found some results?
	       (answer (document-element
			(if adoption
			    (fetch-other me oid ;; adopt: #t
					 type: pishing-mode-compatible-forward-call
					 'location (cons name (msg 'location))
					 'body/parsed-xml recursive)
			    (fetch-other me oid 'location (cons name (msg 'location))
					 'body/parsed-xml recursive)))))
	      (if (and (eq? (ns answer) namespace-dav)
		       (eq? (gi answer) 'multistatus))
		  (if locks
		      (let ((lock (node-list-filter
				   (lambda (x)
				     (equal? (attribute-string 'resource x) name))
				   (children locks))))
			(if (node-list-empty? lock)
			    (children answer)
			    (make-xml-element
			     (gi answer) (ns answer) (xml-element-attributes answer)
			     (node-list-map
			      (lambda (response)
				(make-xml-element
				 (gi response) (ns response) (xml-element-attributes response)
				 (%node-list-cons
				  (make-xml-element
				   'propstat namespace-dav (empty-node-list)
				   (node-list
				    (make-xml-element
				     'prop namespace-dav (empty-node-list)
				     (make-xml-element
				      'lockdiscovery namespace-dav (empty-node-list)
				      (make-xml-element
				       'activelock namespace-dav (empty-node-list)
				       (children lock))))
				    (make-xml-element
				     'status namespace-dav (empty-node-list)
				     (literal "HTTP/1.1 200 OK"))))
				  (children response))))
			      (children answer)))))
		      (children answer))
		  (raise (format "dav-propfind-element ~a\n~a" oid (xml-format answer)))))))))
    (let ((orig-args args))
      (do ((args args (cddr args)))
	  ((null? args)
	   (if (not location)
	       (set! location (msg 'location)))
	   (cond ((string? childs)
		  (set! recursive propfind))
		 ((or (not collection?) (not recursive))
		  (set! childs '()))
		 ((not childs)
		  (set! childs ((me 'fold-links) cons '())))))
	(case (car args)
	  ((collection:) (set! collection? (cadr args)))
	  ((children:) (set! childs (cadr args)))
	  ((location:) (set! location (cadr args)))
	  ((lookup:)
	   (if (procedure? (cadr args))
	       (set! lookup ((cadr args) lookup))
	       (raise (format "dav-propfind: lookup: ~a not a procedure ~a" orig-args (cadr args)))))
	  ((locks:) (set! locks (cadr args)))
	  (else (raise (format "dav-propfind-info ~a unknown keyword ~a" orig-args (car args)))))))

    (if (and (not locks) (equal? (me 'conntent-type) text/xml)
	     (eq? (gi (document-element (dsssl-message-body me))) 'WebDAVDirectory))
	(set! locks ((sxpath '(locks)) (dsssl-message-body me))))

    (let ((prop-filter (make-prop-filter me msg)))
      (make-xml-element
       'multistatus namespace-dav dav-xmlns-decl
       (if (string? childs)
	   (lookup childs #t)
	   (%node-list-cons
	    ;; dav-properties at place
	    (dav:property
	     'response
	     (node-list
	      (dav:property
	       'href (read-locator (msg 'location-format)
				   (if collection? (cons "" location) location)))
	      (case (first-child-gi propfind)
		((propname) (get-dav:propnames me msg prop-filter))
		((allprop)  (get-dav:allprop me msg prop-filter collection?))
		((prop)     (get-dav:prop me msg prop-filter 
					  (children propfind) collection?))
		(else  (raise (format "internal: propfind element unknown"))))))
	    ;; followed by the links, if requested
	    (if (node-list-empty? childs) (empty-node-list)
		(!mapfold lookup (lambda (n v i) (values n (cons v i))) (empty-node-list) childs
			  map-handler: raise))))))))

(define dav-default-output-attributes
  (list (make-xml-attribute 'method #f "xml")
	(make-xml-attribute 'media-type #f text/xml)))

(define (dav-propfind-message-implementation me msg . args)
  (xml-document->message me msg (apply dav-propfind-element me msg args)))

(dav-propfind-message dav-propfind-message-implementation)


;; this is to get the "http-legal-commands" from 
;; mechanism/protocol/http/webdav.scm here!
(define options-allowed-commands '())

(define (set-options-allowed-commands! cmds)
  (set! options-allowed-commands cmds))

(define (dav-supported-methods me)
  (supported-methods-defaulted me options-allowed-commands))

(define (supported-methods-defaulted me default)
  (let ((allow  (or (dav:property-value me 'allowed-http-methods 'children)
		    default))
	(deny   (dav:property-value me 'denied-http-methods 'children)))
    (if (not deny) allow
	(fold (lambda (method init)
		(if (member method deny)
		    init (cons member init)))
	      '() allow))))

(define (dav-level1? allow)
  (or (member "PROPFIND" allow)
      (member "PROPPATCH" allow)
      (member "MKCOL" allow)
      (member "COPY" allow)
      (member "MOVE" allow)))

(define (dav-level2? allow)
  (or (member "LOCK" allow)
      (member "UNLOCK" allow)))

(define (supported-dav-level allow)
  (and (dav-level1? allow)
       (if (dav-level2? allow)
	   "1,2"
	   "1")))

(define options-request-element
  (make-xml-element
   'options namespace-mind
   (list (make-xml-attribute 'api 'xmlns namespace-mind-str))
   (empty-node-list)))

(define (is-options-request? msg) 
  (and (is-meta-form? msg) 
       (is-mind-element? 
	(document-element (dsssl-message-body msg)) 'options)))

(define (dav-options options)
  (make-xml-element
   'options namespace-mind
   (list (make-xml-attribute 'api 'xmlns namespace-mind-str))
   (map (lambda (m) (make-xml-element 'allow #f '() m)) options)))

(define (trace-request-element request slots)
  (sxml
   `(api:trace
     (@ (@ (*NAMESPACES* (,namespace-mind ,namespace-mind-str api))))
     (api:output (@ (media-type (get-slot request 'content-type))
		    (method "text")) 
		 ,(get-slot request 'mind-body))
     . ,(let loop ((slot slots))
	  (cond ((null? slot) '())
		((get-slot request (car slot))
		 (cons (list (car slot) (literal (get-slot request (car slot))))
		       (loop (cdr slot))))
		(else
		 (loop (cdr slot))))))))

(define (is-trace-request? msg)
  (and (is-meta-form? msg)
       (eq? (gi (document-element (dsssl-message-body msg))) 'trace)))

(define (dav-trace me msg)
  (apply
   make-message 
   (property 'caller (dav:self-reference me))
   (property 'location (msg 'location))
   (property 'location-format (msg 'location-format))
   (node-list-reduce
    (children (document-element (dsssl-message-body msg)))
    (lambda (result slot)
      (case (gi slot)
	((output) (property 'body/parsed-xml slot))
	(else (property (gi slot) (data slot)))))
    '())))

; Write requests
(define (put-request-element name content-type body)
  (sxml
   `(api:put
     (@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
			 (,namespace-mind ,namespace-mind-str api)))
	(filename ,(uri-parse name)))
     ; (overwrite "default")
     (api:output
      (@ (media-type ,content-type)
	 (method "text")
	 (xml:space "preserve"))
      ,(cond
	((a:blob? body) (blob->xml body))
	((not body) "")
	(else body))))))

;; (define (put-request-element-XXL name content-type body)
;;   ;;FIX make-put-element to use this
;;   (sxml
;;    `(api:put
;;      (@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
;; 		    (,namespace-mind ,namespace-mind-str api)))
;; 	(filename ,(uri-parse name)))
;;      . ,(let ((tree (if (xml-parseable? content-type)
;; 			(guard (e (else #f))
;; 			       (xml-parse body)))))
;; 	  (or (and (pair? tree) (xml-element? (document-element tree)) tree)
;; 	      `((api:output (@ (media-type ,content-type)
;; 			       (method "text") (xml:space "preserve"))
;; 			    ,body)))))))
    

(define (is-put-request? msg)
  (and (is-meta-form? msg)
       (eq? (gi (document-element (dsssl-message-body msg))) 'put)))
       
;; ist (public-equivalent? oid) kann oid nicht geschrieben werden!
(define *hold-old-version* #f)
(define (make-put-element me msg)
  (let* ((request (document-element (dsssl-message-body msg)))
	 (name    (attribute-string 'filename request))
	 (output  (select-elements (children request) 'output))
	 (media-type (attribute-string 'media-type output))
	 (exist?  (and (me name) *hold-old-version*))
	 (action (or (default-action-property me msg media-type (data output))
		     "public")))
    (sxml 
     `(api:reply 
       (@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
			   (,namespace-mind ,namespace-mind-str api))))
       (api:version ,(literal (or (me 'version) 0)))
       (api:date ,(date->time-literal (msg 'dc-date)))
       ,(let ((b (dsssl-message-body me)))
	  (if (or (pair? b)(null? b))
	      `(continue . ,b)
	      `(continue ,b)))
       ;(continue . ,(me 'body/parsed-xml))
       (api:link
	(@ (name ,name)) 
	(new 
	 (@ (action ,action)
	    (protection ,(right->string (me 'protection))))
	 ,@(if exist?
	       `((link (@ (name "last")) (id , (oid->string (me name))))) '())
	 . ,output))
       (output (@ (media-type ,application/xml))
	       (success (status 201))))))) ;;or 204?

(define (delete-request-element name)
  (sxml 
   `(api:form 
     (@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
			 (,namespace-mind ,namespace-mind-str api))))
     (link (@ (name ,name))))))

(define (is-delete-request? msg)
  (and (is-meta-form? msg)
       (let ((body (document-element (dsssl-message-body msg))))
	 (and (eq? (gi body) 'link)
	      (node-list-empty? (children body))))))

(define (make-delete-element me msg)
  (error "do'nt call this!"))

;; Do some plausibility checks and raise errors.  If not, it must
;; return the element as is.
;proppatch-propose
(define (dav:propertyupdate me msg . maybe-filter+keys)
  (let* ((propertyupdate (document-element (dsssl-message-body msg)))
	 (prop-filter (make-prop-filter me msg))
	 (set-updates
	  (node-list-filter
	   (lambda (n) (and (eq? (gi n) 'set)
			    (not (node-list-empty? (prop-filter n)))))
	   (children propertyupdate)))
	 (remove-updates
	  (node-list-filter
	   (lambda (n) (and (eq? (gi n) 'remove)
			    (not (node-list-empty? (prop-filter n)))))
	   (children propertyupdate))))
    propertyupdate))

(define (dav:prepare-propertyupdate me msg . maybe-filter+keys)
  (let ((request (document-element (dsssl-message-body msg))))
    #f)

;; first loop throu (me 'dav:properties) +
  )

(define (proppatch-request-element body)
  (make-xml-element
   (gi body) namespace-mind
   (cons
    (make-xml-attribute 'api 'xmlns namespace-mind-str)
    (attributes body))
   (children body)))

(define (is-proppatch-request? msg)
  (and (is-meta-form? msg)
       (eq? (gi (document-element (dsssl-message-body msg))) 'propertyupdate)))

(define (make-proppatch-element me msg)
  (make-xml-element
   'reply namespace-mind '()
   (node-list
    (make-xml-element 'version namespace-mind '()
		      (literal (or (me 'version) 0)))
    (make-xml-element
     'date namespace-mind '() (date->time-literal (msg 'dc-date)))
    (make-xml-element 'continue #f '() (dsssl-message-body me))
    ;;das is (node-list
    ;;        (set! dav:properties (d:prop (name val) ...))
    ;;        (set! slot value)...
    (dav:propertyupdate me msg)
    (make-xml-element
     'output namespace-mind '() 
     (make-xml-element ;; NEED! a multistatus response here!
      'success #f '()
      (make-xml-element 'status #f '() (literal "200")))))))

;; Include plausibility checks here.  Remember the 'do-move' part runs
;; in the 'accept' part of a transaction.  Exceptions in that part are
;; hard to notice.

;; FIXME find a better name: these make-*-extension are consitently
;; used for predicate/transformer declarations which implement CoreAPI
;; elements.  This here is somewhat different and I don'T undrstand it
;; well.  /jfw

(define (object-identification me path)
  (receive 
   (name path terminate) (string-split-last path #\/)
   (let* ((p (parsed-locator path))
	  (name (uri-parse name))
	  (info (guard (exception (else #f)) 
		       (me (if (null? p) (dav:self-reference me) p) 'metainfo))))
     (values 
      name
      (let ((src-oid 
	     (attribute-string 
	      'href 
	      ((sxpath `(Description links Bag (li (@ (equal? (resource ,name)))))) info))))
	(and src-oid (string->oid src-oid)))
      path
      (let ((parent-oid (attribute-string 'about ((sxpath '(Description)) info))))
	(and parent-oid (string->oid parent-oid)))))))



(%early-once-only
 (define source-request
   (sxml `(api:form
	   (@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
		    (,namespace-mind ,namespace-mind-str api))))
	   (template "source"))))
)

(define (copy/move-check-parameter me request)
  (define action (data (form-field 'action request)))
  (receive 
   (src-name src-oid src-path src-parent-oid)
   (object-identification me (data (form-field 'source request)))
   (if (string=? src-name "")
       (raise (make-condition
		&http-effective-condition 'status 400
		'message (format "Can't ~a! Source URL is illegal: ~s."
				 action
				 (data (form-field 'source request))))))
   (if (not src-oid)
       (raise (make-object-not-available-condition
	       (data (form-field 'source request)) (dav:self-reference me) 'copy/move-check-parameter)))
   (if (not (find-frame-by-id/quorum src-oid (or (me 'replicates) *local-quorum*)))
       (raise (make-object-not-available-condition
	       src-name
	       (format "Can't ~a! Source object ~a not available."
		       action (data (form-field 'source request)))
	       'copy/move-check-parameter)))
   (receive
    (dst-name dst-oid dst-path dst-parent-oid)
    (object-identification me (data (form-field 'destination request)))
    (if (string=? dst-name "")
	(raise (make-condition
		&http-effective-condition 'status 400
		'message (format "Can't ~a! Destination URL is illegal ~s." 
				 action
				 (data (form-field 'destination request))))))
    (if (not dst-parent-oid)
	(raise (make-condition
		&http-effective-condition 'status 409
		'message (format "Can't ~a! Destination path ~s not found."
				 action dst-path))))
    (if (and dst-oid (node-list-empty? (form-field 'overwrite request)))
	(raise-precondition-failed (format "Collection ~a already exists" dst-name)))
    (if (and (eq? src-parent-oid dst-parent-oid)
	     (string=? src-name dst-name))
	(raise (make-condition
		&http-effective-condition 'status 403
		'message (format "Can't ~a! Operation at the same place: ~a/~a ~a/~a ."
				 action
				 src-path src-name dst-path dst-name))))

    ;; Are we trying to move down in src name space?
    (if (and (string=? action "move")
	     (let ((s (parsed-locator src-path)))
	       (and (pair? s)
		    (let loop ((s s) (d (parsed-locator dst-path)))
		      (cond ((null? d) #f)
			    ((null? s)
			     (and (pair? d) (string=? (car d) src-name)))
			    ((string=? (car s) (car d))
			     (loop (cdr s) (cdr d)))
			    (else #f))))))
	(raise (make-condition
		&http-effective-condition 'status 403
		'message
		(format "Can't ~a! Destination is in the scope of source: ~a/~a -> ~a/~a."
			action
			src-path src-name dst-path dst-name))))

    (values src-name src-oid src-path src-parent-oid
	    dst-name dst-oid dst-path dst-parent-oid
	    (if (string=? "copy" action)
		(let ((body (guard
			     (exception (else #f))
			     (document-element
			      (fetch-other me src-oid
					   'content-type text/xml
					   'body/parsed-xml source-request)))))
		  (if (not (eq? (gi body) 'source))
		      (raise
		       (make-condition
			&http-effective-condition 'status 403
			'message
			(format "Can't ~a! Source entity data of ~a not available."
				action
				(data (form-field 'source request))))))
		  (children body))
		#f)))))

(define (copy/move-request-element request move)
  (let* ((match ((pcre->proc "(?:https?://[^/]*)?(/?.*)")
		 (get-slot request 'dav-destination)))
	 (dst0  (if match (cadr match) (get-slot request 'dav-destination)))
	 ;; FIXME remove this debug code
	 (prefix (if (equal? (get-slot request 'expect) "X-Askemos-no-prefix")
		     0
		     (string-index-right
		      dst0 #\/ 0
		      (string-prefix-length
		       dst0 (get-slot request 'dc-identifier)))))
	 (to    (substring (get-slot request 'dc-identifier) 0 prefix))
	 (dst   (substring dst0 (+ 1 prefix)))
	 (src   (substring (get-slot request 'dc-identifier) (+ 1 prefix)))
	 (over  (string=? "T" (or (get-slot request 'overwrite) "T")))
	 (body  (guard
		 (exception (else #f))
		 (document-element 
		  (xml-parse (or (body->string (get-slot request 'mind-body))
				 "")))))
	 (props (cond
		 ((or(not body)
		     (not (eq? (gi body) 'propertybehavior)))
		  (make-xml-element 'allproperties #f '() '()))
		 ((eq? 'omit (first-child-gi body))
		  (make-xml-element 'relaxproperties #f '() '()))
		 ((eq? 'keepalive (first-child-gi (children body)) )
		  (if (string=? "*" (data (node-list-first (children body))))
		      (make-xml-element 'lifeproperties #f '() '())
		      (node-list-map 
		       (lambda (p) 
			 (if (eq? (gi p) 'href)
			     (make-xml-element 'property #f '()
					       (make-xml-literal (data p)))
			     (empty-node-list)))
		       (children (children body)))))
		 (else
		  (make-xml-element 'allproperties #f '() '())))))
    (values
     to
     (make-specialised-xml-element
      'form namespace-mind
      (list (make-xml-attribute 'mind 'xmlns namespace-mind-str))
      (node-list
       (make-xml-element
	'action #f '() (make-xml-literal move))
       (make-xml-element
	'source #f '() (make-xml-literal src))
       (make-xml-element 'destination #f '() (make-xml-literal dst))
       props
       (if over
	   (make-xml-element 'overwrite #f '() '())
	   (empty-node-list)))))))

(define (is-copy-request? msg)
  (and (is-meta-form? msg)
       (string=? 
	"copy" (data (form-field 'action (dsssl-message-body msg))))))

;; source           ok
;; destination      ok
;; propertybehavior
;; overwrite        ok
;; locks
(define (make-copy-element me msg . request)
  (receive 
   (src-name src-oid src-path src-parent-oid
	     dst-name dst-oid dst-path dst-parent-oid body)
   (copy/move-check-parameter me (if (null? request) (dsssl-message-body msg) (car request)))
   ;; THINK ABOUT! need to copy the properties to
   (let ((self  (eq? dst-parent-oid (dav:self-reference me)))
	 (the-link 
	  `(api:link
	    (@ (name ,(uri-parse dst-name))) 
	    ,(if (is-copy-request? msg)
		 `(new (@ (action ,(oid->string (action-document src-oid)))
			  (protection ,(right->string (me 'protection))))
		       . ,(if (or (pair? body) (null? body)) body (list body)))
		 `(id ,(oid->string src-oid))))))
     (sxml 
      `(api:reply 
	(@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
			    (,namespace-mind ,namespace-mind-str api))))
	(api:version ,(literal (or (me 'version) 0)))
	(api:date ,(date->time-literal (msg 'dc-date)))
	(continue . ,(let ((b (dsssl-message-body me)))
		       (if (or (pair? b) (null? b)) b (list b))))
	,(if self
	     the-link
	     `(api:send
	       (@ (type "write"))
	       (to ,dst-path)
	       (Body (api:form ,the-link))))
	(output (@ (media-type ,application/xml))
		(,(if self 'success 'actor) 
		 (final ,dst-parent-oid) 
		 (status ,(if dst-oid 204 201))
					;(text (html (head) (body (p "some Info"))))
		 )))))))

(define (is-move-request? msg)
  (and (is-meta-form? msg)
       (string=? 
	"move" (data (form-field 'action (dsssl-message-body msg))))))

;; source
;; destination
;; propertybehavior
;; overwrite
;; locks
(define (make-move-element me msg . request)
  (receive 
   (src-name src-oid src-path src-parent-oid
	     dst-name dst-oid dst-path dst-parent-oid body)
   (copy/move-check-parameter
    me (if (null? request) (dsssl-message-body msg) (car request)))
   (let* ((link-it   `(api:link (@ (name ,(uri-parse dst-name)))
				(id ,src-oid)))
	  (unlink-it `(api:link (@ (name ,(uri-parse src-name)))))
	  (rename?    (eq? src-parent-oid dst-parent-oid))
	  (local?     (eq? dst-parent-oid (dav:self-reference me)))
	  (move-element
	   (if rename?
	       (cond 
		(local?
		 `(,link-it ,unlink-it))
		(else
		 `((api:send (@ (type "write")) (to ,dst-path)
			     (Body (api:form ,link-it ,unlink-it))))))
	    ;;; move
	       (cond 
		(local?
		 `(,link-it
		   (api:send (@ (type "write")) (to ,src-path)
			     (Body (api:form ,unlink-it)))))
		(#f ;; new sequence syntax
		 `((api:sequence
		    (api:step (@ (trigger ,(oid->string dst-parent-oid)))
			      (to ,dst-path)
			      (Body (api:form ,link-it)))
		    (api:step ;(@ (trigger ,(oid->string src-parent-oid)))
					;(@ (trigger "is not needed at last step"))
		     (to ,src-path)
		     (Body (api:form ,unlink-it))))))
		(else ;; old sequens syntax
		 `((api:sequence
		    (api:step 
		     (@ (continue-from ,(oid->string dst-parent-oid))
			(destination ,dst-path))
		     (api:form ,link-it))
		    (api:step
		     (@ (destination ,src-path))
		     (api:form ,unlink-it))))))))
	  (output
	   (let ((status (if dst-oid 204 201)))
	     (if (and rename? local?)
		 `(success (status ,status))
		 `(actor   (final ,src-parent-oid)
			   (status ,status))))))
     ;;       (logerr "I: make-move: ~a ~s/~s -> ~s/~s:\n~a\n"
     ;; 		 (if rename? "rename" "move")
     ;; 		 (if (string=? src-path "") "." src-path) src-name
     ;; 		 (if (string=? dst-path "") "." dst-path) dst-name
     ;; 		 (xml-format (sxml `(,@move-element ,output))))
     (sxml `(api:reply
	     (@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
				 (,namespace-mind ,namespace-mind-str api))))
	     (api:version ,(literal (or (me 'version) 0)))
	     (api:date ,(date->time-literal (msg 'dc-date)))
	     (continue . ,(let ((b (dsssl-message-body me)))
			    (if (or (pair? b) (null? b)) b (list b))))
	     ,@move-element
	     (output (@ (media-type ,application/xml)) ,output))))))

(define (mkcol-request-element name)
  (make-xml-element
   'mkcol namespace-mind
   (let ((attributes (list (make-xml-attribute 'api 'xmlns namespace-mind-str)
			   (make-xml-attribute 'name #f name))))
     attributes)
   '()))

(define (is-mkcol-request? msg)
  (and (is-meta-form? msg)
       (eq? (gi (document-element (dsssl-message-body msg))) 'mkcol)))

(define (make-mkcol-element me msg . args)
  (let* ((request (document-element (dsssl-message-body msg)))
	 (name (attribute-string 'name request))
	 (body (dsssl-message-body me))
	 (protection (right->string (me 'protection)))
	 (init #f)
	 (force #f)
	 (action (oid->string (me 'mind-action-document))))
    (do ((args args (cddr args)))
	((null? args))
      (case (car args)
	((name:) (set! name (cadr args)))
	((action:) (set! action (cadr args)))
	((protection:) (set! protection (cadr args)))
	((init:) (set! init (cadr args)))
	((force:) (set! force (cadr args)))
	((body:) (set! body (cadr args)))
	(else (raise (format "unknown keyword ~a in make-mkcol-element" (car args))))))
    
    (if (not name)
	(raise (format " Missing required attribute \"name\" in mkcol")))

    (if (and (me name) (not force))
	(raise (make-condition
		&http-effective-condition
		'status 405 'message (format "Collection ~a already exists" name))))

    ;; FIX for sxml!
    (set! body (if (or (pair? body) (null? body)) body (list body)))

    (sxml
     `(api:reply 
       (@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
			   (,namespace-mind ,namespace-mind-str api))))
       (api:version ,(literal (or (me 'version) 0)))
       (api:date    ,(date->time-literal (msg 'dc-date)))
       (continue . ,(document-element (dsssl-message-body me)))
       (api:link
	(@ (name ,name))
	(api:new 
	 (@ (action ,action)
	    (protection ,protection)
	    ,@(if init `((initialize ,init)) '()))
	 . ,body))
       (output (@ (media-type ,application/xml))
	       (success (status 201)))))))

(define (accept-right-reply me msg)
  (sxml
   `(api:reply 
     (@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
			 (,namespace-mind ,namespace-mind-str api))))
     (api:version ,(literal (or (me 'version) 0)))
     (api:date    ,(date->time-literal (msg 'dc-date)))
     (continue . ,(dsssl-message-body me))
     (output (@ (media-type ,application/xml))
	     (success (status 201)))
     . ,((sxpath  '(right)) (dsssl-message-body msg)))))

(define (is-collection-request? msg)
  (and (is-meta-form? msg)
       (or 
	(let ((action (data (form-field 'action (dsssl-message-body msg)))))
	  (or 
	   (string=? action "copy")	; is-copy-request?
	   (string=? action "move")))	; is-move-request?
	(memq (gi (document-element (dsssl-message-body msg)))
	      '(propertyupdate		; is-proppatch-request?
		put			; is-put-request?
		mkcol			; is-mkcol-request?
		grant)))))

