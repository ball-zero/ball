<?xml version="1.0" encoding="UTF-8" ?>
<!-- (C) 2000, 2001, 2006 Jörg F. Wittenberger see http://www.askemos.org -->
<!-- OBSOLET: This file is recreated as ../app/reflexive-xdslt.xml -->
<!--

 JFW (2006-07-31): The document root element has been renamed to html
 for the sake of literal inclusion of some CSS.  FIXME: we ought to
 have either a pure xml solution or a completely valid html
 representation.

 JFW (2002-11-20): FIXME normaly there should be a doctype declaration
 but right now the parser we use is broken in that respect.

 When fixing fix the comment end marker down there!

<!DOCTYPE action [
  <!ELEMENT action (method | para)* >
  <!ELEMENT method (programlisting | para)* >
  <!ATTLIST method
    type (read|propose|accept) "read"
    this CDATA "me"
    request CDATA "msg"
  >
  <!- -
    See action.dtd for comments.
  - FIXME fix this comment end marker FIXME ->
  <!ELEMENT programlisting (#PCDATA) >
  <!ELEMENT para (#PCDATA)* >
]>
-->
<html>
 <head>
  <title></title>
  <style lang="text/css">
programlisting {
 display: block;
 font-family: "monospace";
 padding: 2em;
 margin: 2em;
 border: 1px solid black;
}
  </style>
 </head>
 <body>
  <h1>Reflexive XSLT based Language</h1>
  <p>Presentation and behavior are both scripted at the place in question.</p>
  <p>Remark: this is the prefered "new style" action document.
The "new style" splits transaction into
two methods.
One for the <code>propose</code>~ and one for the <code>accept</code>
part of the transaction.
The old style, using only a general <code>write</code> method
is deprecated.
(Be warned: due to it's restrictions,
it need create places too early.
Upon transaction retries
conflicts in OID calculation will result.
Those in turn can lock you places forever due to mutual disagreement
on current state.)</p>
 </body>

 <method type="read">
  <programlisting>
   (read-only-xslt-transformation me msg)
  </programlisting>
 </method>

 <method type="propose">
  <programlisting>
   (reflexive-xslt-transformation-propose me msg)
  </programlisting>
 </method>

 <method type="accept">
  <programlisting>
   (xslt-transformation-accept me msg result)
  </programlisting>
 </method>

</html>
