;; (C) 2000, 2001, 2003 Jörg F. Wittenberger see http://www.askemos.org

;; (C) 1997-2000 Joerg F. Wittenberger <Joerg.Wittenberger@pobox.com>

;;* Rights Section, Part II

;; Again some ethico-juristic policy.  I want to grant some rights to
;; someone else.

;; Precondition: the request has the capability to grant.

;; A right is something, you can receive, but you can't sign it away,
;; all you can sign away is a sub-right of yours.

;; Finally you can't grant someone something which those already own.

(define (make-right-node c)
  (cond
   ((or (eq? c (public-oid)) (eq? c (my-oid)))
    (dsssl-make dsssl-element-key
                gi: 'name ns: namespace-mind
                attributes: '()
                (node-list (literal (if (eq? c (public-oid)) "public" "private")))))
   ((oid? c)
    (dsssl-make dsssl-element-key
                gi: 'id ns: namespace-mind
                attributes: '()
                (node-list (literal (oid->string c)))))
   ((string? c)
    (dsssl-make dsssl-element-key
                gi: 'name ns: namespace-mind
                attributes: '()
                (node-list (literal c))))
   (else (error (format #f "illegal right ~a" c)))))

(define (right->node-list capa)
  (make-xml-element
   'right namespace-mind
   '()
   (map make-right-node capa)))

(define (node-list->right node)
  (and
   (eq? (gi node) 'right)
   (eq? (ns node) namespace-mind)
   (let loop ((nl (children node)) (res '()))
     (if (node-list-empty? nl)
         (reverse! res)
         (loop (node-list-rest nl)
               (let ((node (node-list-first nl)))
                 (if (eq? (ns node) namespace-mind)
                     (case (gi node)
                       ((id) (cons (string->oid (data (%children node))) res))
                       ((name) (cons (or ((public-context) (data (%children node)))
					 (and (string=? "private" (data (%children node)))
					      (my-oid))
                                         (error "can't publically resolve ~a"
                                                (data (%children node))))
                                     res))
                       (else res))
                     res)))))))

;; This short note seems to get out of control.  What's the problem:

;; We could consider to map the local identity, "private", to the
;; _same_ "legal meaning" for each node (by symbolic name instead of
;; identifier as with "public").  I hesitate, since I'm not sure that
;; there's no case, when doing so could leak private information.

;; However not doing so seems to incure an interesting consequence,
;; when supporting peers: Public material is only accessiable at the
;; host where the entry point was first created.  When access through
;; a network connection to a "foreign" host, access is denied, since
;; the "public read capability" is not the same.  This however seems
;; to be the right thing in a way.  But I failed so far to get such
;; assumtions working.  Hence reverting back to assume "others have
;; the *same* right of privacy.

(define (oid->right-name id)
  (or (and (public-equivalent? id) "public")
      ;; see above comment, we might want to comment that out.
      (and (eq? (my-oid) id) "private")
      (oid->string id)))

(define (oid->absolute-right-name id)
  (or (oid->string id)
      (error (format #f "right->string: illegal oid ~a" id))))

(define (oid->external-right-name id)
  (or (and (public-equivalent? id) "public")
      ;; see above comment, we might want to comment that out.
      (and (eq? (my-oid) id) "private")
      id))

(define (right->string capa)
  (srfi:string-join (map oid->right-name capa) "/"))

(define (right->absolute-string capa)
  (srfi:string-join (map oid->absolute-right-name capa) "/"))

(define (right-name->oid r)
  (cond
   ((equal? r "public") (public-oid))
   ;; see above comment, we might want to comment that out.
   ((equal? r "private") (my-oid))
   (else (string->oid r))))

(define (string->right str)
  (map right-name->oid (string-split str "/")))

(define *coreapi-attribute-list*
  (list namespace-coreapi-declaration))

(define (make-grant-document capa)
  (make-xml-element
   'grant namespace-mind *coreapi-attribute-list*
   (if (and (xml-element? capa) (eq? 'right (gi capa)))
       capa (right->node-list capa))))

(define $adopt-grant (make-shared-parameter #t))

(define (make-grant me request)
  (define (grant to capa)
    (cond
     ((eqv? to (public-oid))
      (error "grant: can't write to public anyway..."))
     ((and (oid? to) (oid=? (me 'get 'id) to))
      (let ((right (if (and (xml-element? capa)
                            (eq? 'right (gi capa)))
                       (node-list->right capa)
                       capa))
            (settor (me 'get 'writer)))
        (settor capability: right)
        ;; See comment in make-access: the potentialities slot does
        ;; quite the opposite of what you expect.  It's not a feature,
        ;; it's a missnomer.
        (settor 'potentialities right)))
     (else
      (let ((r (if (and (xml-element? capa)
                        (eq? 'right (gi capa)))
                   (node-list->right capa)
                   capa)))
        (if (or ($adopt-grant) (contains-strictly? r (request 'capabilities)))
            ((me 'get 'sender)
             to: to type: 'write adopt: ($adopt-grant)
             ;; We send special message, only the right to be granted,
             ;; the full onw right (even if inactive) to sign the
             ;; request (the place won't accept it anyway - deserves
             ;; cleanup) and the public right in case the target place
             ;; needs some other things to read before.  Looks
             ;; doubtful, doesn't it?
             capabilities: `(,r
                             (,(me 'get 'id)) ; FIXME: add ,(public-oid)
                             . ,(public-capabilities))
             'body/parsed-xml (make-grant-document capa))
            ;; (logerr "grant from ~a to ~a capa ~a not strict in ~a\n"
            ;;        (me 'get 'id) to r (request 'capabilities))
            )))))
  grant)

;; Revocation of rights at kernel level is an enforced operation.  The
;; user must not have a chance to resist.  For that reason we provide
;; a kernel method here, which is independant of the application level
;; code.

;; FIXME There should be a user notification, but is not yet.

(define (make-revoke me request)
  (define (revoke from capa)
    (let ((capa (if (and (xml-element? capa) (eq? 'right (gi capa)))
                    (node-list->right capa) capa)))
      (cond
       ;; Special case for self restricting.  The capabilities are
       ;; disabled and saved as potentialities.
       ((and (oid? from) (oid=? (me 'get 'id) from))
        (let ((capa (if (and (xml-element? capa)
                             (eq? 'right (gi capa)))
                        (node-list->right capa)
                        capa))
              (settor (me 'get 'writer)))
          (settor capabilities: (filter
                                 (lambda (c) (not (equal? capa c)))
                                 (me 'capabilities)))
          (settor potentiality: capa)))
       ;; Must dominate the capability.
       ((not (contains-strictly? capa (request 'capabilities))) #f)
       ((eqv? from (me 'get 'id)) #f)   ; otherwise it might lock up
       ((eqv? from (public-oid)) #f)    ; superflous, 1st cond should fail
       (else
        (let ((frame (find-local-frame-by-id from 'revoke)))
          (respond! frame
                    (lambda (place message) ; kernel method
                      (let ((settor (place 'get 'writer)))
                        (settor
                         capabilities: (filter
                                        (lambda (c) (not (equal? capa c)))
                                        (place 'capabilities)))
                        ;; Again missnomer; see comment make-access.
                        (settor 'potentialities capa)))
                    respond-local #t
                    (make-message
                     (property 'capabilities (fget frame 'capabilities)))))))))
  revoke)

;;* Askemos code

;; This part implements the askemos server as it is in use.

;; It's divided into sections concerning persistent data and
;; environment (abstract handling supporting data structure), data
;; structures, administrative routines (file system mirror, output
;; generation etc.), predicates supporting business logic, messages,
;; method tables, nunu methods

(define (make-privilege-item x)
  (make-xml-element 'right namespace-mind '() (right->string x)))

(define (make-privilege-statement type ns-decls capabilities)
  (make-xml-element
   type namespace-mind ns-decls
   (map make-privilege-item capabilities)))

(define (fold-privilege-items capabilities init items)
  (if (null? items) init
      (let ((item (car items)))
	(if (dominates? item capabilities)
	    (cons (make-privilege-item item)
		  (fold-privilege-items capabilities init (cdr items)))
	    (fold-privilege-items capabilities init (cdr items))))))

(define (make-capability-statement frame request) ; export
  (make-xml-element
   'capabilities namespace-mind signature-att-decl
   (let ((capas (get-slot request 'capabilities)))
     (fold-privilege-items
      capas
      (fold-privilege-items capas (empty-node-list) (fget frame 'capabilities))
      (fget frame 'potentialities)))))

(define (parse-capability-statement nl)
  (node-list-reduce
   (children nl)
   (lambda (init item)
     (let ((right (string->right (data item))))
       (if right (cons right init) init)))
   '()))

(define (creator-element-for-meta creator)
  ;; There are not too many cases where we have no creator.
  ;; For backward BUG compatibility we deliver "#f" instead of (empty-node-list)
  (make-xml-element 'creator namespace-dc '() (if (oid? creator) (oid->string creator) (begin (logerr "creator element required for creator ~a\n" creator) "#f"))))

(define (frame-metadata frame)
  (make-xml-element
   'RDF namespace-rdf signature-att-decl
   (make-xml-element
    'Description namespace-rdf
    (list (make-xml-attribute 'about namespace-rdf (oid->string (aggregate-entity frame))))
    (node-list
     (creator-element-for-meta (fget frame 'dc-creator))
     (make-xml-element 'date namespace-dc '()
		       (literal (rfc-822-timestring (or (fget frame 'dc-date)
							frame-dummy-date))))
     (make-xml-element 'action-document namespace-mind '()
		       (literal (oid->string (or (fget frame 'mind-action-document)
						 (aggregate-entity frame)))))
     (let ((version (or (fget frame 'version)
			(cons 0 (aggregate-entity frame)))))
       (make-xml-element
        'version namespace-mind rdf-parsetype-literal-attribute-list
        (node-list
	 (make-xml-element 'serial namespace-mind '()
			   (literal (version-serial version)))
	 (make-xml-element
	  'checksum namespace-mind '() (literal (version-chks version))))))

     (replicates-of/xml frame)

     (make-xml-element 'protection namespace-mind '()
		       (right->string (or (fget frame 'protection) '())))
     (if (fget frame 'secret)
	 (make-xml-element
	  'secret namespace-mind '() (literal (fget frame 'secret)))
	 (empty-node-list))
     (make-privilege-statement
      'capabilities '() (fget frame 'capabilities))
     (make-privilege-statement
      'potentialities '() (or (fget frame 'potentialities) '()))
     (make-xml-element 'content-type #f '()
		       (literal (fget frame 'content-type)))
     (let ((body (or (fget frame 'mind-body)
		     (xml-format (or (fget frame 'body/parsed-xml)
				     (empty-node-list))))))
       (make-xml-element
	'body namespace-mind '()
	(if (a:blob? body)
	    (blob->xml body)
	    (blob->xml
	     (a:make-blob (string->symbol (sha256-digest body)) (string-length body) (string-length body) 1 #f)))))
     (let ((body (fget frame 'sqlite3)))
       (cond
	((a:blob? body)
	 (make-xml-element 'sqlite3 namespace-mind '() (blob->xml body)))
	(else (empty-node-list))))
     (let ((links (fget frame 'mind-links)))
       (if links
	   (table->rdf-bag links make-rdf-li-for-link)
	   (empty-node-list)))
     ;; -------------------------------------------------------------------
     ;; Below this line we carry optional stuff (sugar).
     ;; -------------------------------------------------------------------
     (make-xml-element 'last-modified namespace-dav '()
		       (rfc-822-timestring (or (fget frame 'last-modified)
					       (fget frame 'dc-date)
					       frame-dummy-date)))
     (let ((lang (fget frame 'dc-language)))
       (if lang
	   (make-xml-element 'language namespace-dc '() lang)
	   (empty-node-list)))
     ;; I don't remember, why we decided that we need yet another
     ;; container around the "prop" element of DAV.  Pit, please
     ;; comment!
     (let ((davprops (fget frame 'dav:properties)))
       (if davprops
	   (make-xml-element 'properties namespace-dav '() davprops)
	   (empty-node-list)))
     ))))

(%early-once-only
 (define empty-blob-xml
   (blob->xml (a:make-blob (string->symbol (sha256-digest "")) 0 0 1 #f))))

(: frame-signature (:place: -> :xml-element:))
(define (frame-signature frame)
  (assert (aggregate-entity frame))
  (make-xml-element
   'RDF namespace-rdf signature-att-decl
   (make-xml-element
    'Description namespace-rdf
    (list (make-xml-attribute 'about namespace-rdf (oid->string (aggregate-entity frame))))
    (node-list
     (creator-element-for-meta (fget frame 'dc-creator))
     (make-xml-element
      'date namespace-dc '()
      (literal (rfc-822-timestring (or (fget frame 'dc-date)
				       (begin
					 (logerr "~a missing dc-date\n" (aggregate-entity frame))
					 frame-dummy-date)))))
     (make-xml-element
      'action-document namespace-mind '()
      (literal (oid->string (or (fget frame 'mind-action-document)
				(begin (logerr "~a missing action document\n" (aggregate-entity frame))
				       (public-oid))))))
     (let ((version (or (fget frame 'version)
			(cons 0 (aggregate-entity frame)))))
       (make-xml-element
        'version namespace-mind rdf-parsetype-literal-attribute-list
        (node-list
	 (make-xml-element 'serial namespace-mind '()
			   (literal (version-serial version)))
	 (make-xml-element
	  'checksum namespace-mind '() (literal (version-chks version))))))
     (replicates-of/xml frame)
     (make-xml-element 'protection namespace-mind '()
                       (right->string (or (fget frame 'protection) '())))
     (make-xml-element 'content-type #f '()
		       (literal (fget frame 'content-type)))
     (let ((body (or (fget frame 'mind-body)
		     (xml-format (or (fget frame 'body/parsed-xml)
				     (empty-node-list))))))
       (make-xml-element
	'body namespace-mind '()
	(cond
	 ((a:blob? body) (blob->xml body))
	 ((string? body)
	  (blob->xml
	   (a:make-blob (string->symbol (sha256-digest body)) (string-length body) (string-length body) 1 #f)))
	 (else empty-blob-xml))))
     (let ((body (fget frame 'sqlite3)))
       (cond
	((a:blob? body)
	 (make-xml-element 'sqlite3 namespace-mind '() (blob->xml body)))
	(else (empty-node-list))))
     (table->rdf-bag-sorted (fget frame 'mind-links))
     (let ((davprops (fget frame 'dav:properties)))
       (if davprops
	   (make-xml-element 'properties namespace-dav '() davprops)
	   (empty-node-list)))))))

;; 'access-signature' SHOULD exactly replicate frame-signature, just
;; the access protocol differ.  But it doesn't: where should the
;; 'protection' and 'replicates' info go?  TODO: this cries for
;; cleaner implementation!

(define access-signature-links-attlist
  (list (make-xml-attribute 'ID namespace-rdf "links")))

(: access-signature (:place-accessor: -> :xml-element:))
(define (access-signature access)
  (make-xml-element
   'RDF namespace-rdf signature-att-decl
   (make-xml-element
    'Description namespace-rdf
    (list (make-xml-attribute 'about namespace-rdf
                              (oid->string (access 'get 'id))))
    (node-list
     (creator-element-for-meta (access 'dc-creator))
     (make-xml-element 'date namespace-dc '()
                       (literal (rfc-822-timestring (or (access 'dc-date)
                                                        (begin
                                                          (logerr "access-signature ~a missing dc-date\n" (access 'get 'id))
                                                          frame-dummy-date)))))
     (let ((oid (access 'mind-action-document)))
       (if oid
	   (make-xml-element 'action-document namespace-mind '()
			     (literal (oid->string oid)))
	   (begin
	     (logerr "access-signature ~a missing action document\n" (access 'get 'id))
	     (empty-node-list))))
     (let ((version (or (access 'version) (cons 0 (access 'get 'id)))))
       (make-xml-element
        'version namespace-mind rdf-parsetype-literal-attribute-list
        (node-list
          (make-xml-element 'serial namespace-mind '()
			    (literal (version-serial version)))
          (make-xml-element
           'checksum namespace-mind '() (literal (version-chks version))))))
;;      (make-xml-comment "
;; Where should the 'protection' and 'replicates' info go?
;; At least 'protection' doesn't really belong to the basic meta data.
;; ")
     (access text/xml 'replicates)
     
     (make-xml-element 'protection namespace-mind '()
                       (right->string (or (access 'protection) '())))

     (make-xml-element 'content-type #f '()
		       (literal (access 'content-type)))
     ;; (make-xml-comment
     ;;  "The  elements 'body' and 'sqlite3' are not conformant to any standard and subject to change.")
     (let ((body (access 'mind-body)))
       (make-xml-element
       'body namespace-mind '()
       (cond
	((a:blob? body) (blob->xml body))
	((string? body)
	 (blob->xml
	  (a:make-blob (string->symbol (sha256-digest body)) (string-length body) (string-length body) 1 #f)))
	(else empty-blob-xml))))
     (let ((body (access 'sqlite3)))
       (cond
	((a:blob? body)
	 (make-xml-element 'sqlite3 namespace-mind '() (blob->xml body)))
	(else (empty-node-list))))

     (let ((links ((access 'fold-links-ascending)
		   (lambda (i init)
		     (%node-list-cons
		      (make-rdf-li-for-link i (access i))
		      init))
		   (empty-node-list))))
       (if (node-list-empty? links) (empty-node-list)
           (make-xml-element
	    'links namespace-mind '()
	    (make-xml-element
	     'Bag namespace-rdf frame-metadata-links-attlist
	     links))))

     (let ((davprops (access 'dav:properties)))
       (if davprops
	   (make-xml-element 'properties namespace-dav '() davprops)
	   (empty-node-list)))))))

(define (resolve-link meta-xml name)
  (let ((li ((sxpath `(Description links Bag (li (@ (equal? (resource ,name))))))
	     meta-xml)))
    (if (node-list-empty? li)
	#f
	(string->oid (attribute-string 'href (node-list-first li))))))

;; version-identifier returns a string value, which uniquely
;; identifies the version of the place 'me'.  But this is an _opaque
;; value.  It might be either the string value of the sequence number
;; version or the sequence running checksum.

(define (version-identifier me)
  (number->string (version-serial (me 'version))))

;; Make up a message, which can be send from frame in response of
;; message.  TODO These functions need some cleanup.  They appear to
;; define an API, which doesn't really fit the purpose.  2000-08-15

;; More cleanup seems to be in order because the caching between body
;; and parsed representation could be of better effect. 2001-11-19

;; Make a message of an xml tree.

(define (message-default-location frame message)
  (let ((md (message 'destination)))
    (if (pair? md)
	(append! (reverse md) (message 'location))
	(message 'location))))

;; A stupid HACK
(define (http-state-for tree)
  (and-let* ((el (document-element tree))
	     ((and (eq? (ns el) namespace-dav)
		   (eq? (gi el) 'multistatus))))
	    207))

(define (xml-document/content-type->message
	 frame message tree content-type date . properties)
  (apply
   make-message			
   (property 'mind-action-document (frame 'mind-action-document))
   (property 'caller (frame 'get 'id))
   (property 'capabilities (message 'capabilities))
   (property 'dc-creator (message 'dc-creator))
   (property 'dc-date date)
   (or (find (lambda (p) (eq? (property-name p) 'location)) properties)
       (property 'location (message-default-location frame message)))
   (property 'body/parsed-xml tree)
   (property 'content-type content-type)
   (property 'mind-body
             (cond
              ((eq? tree (message 'cached:body/parsed-xml))
               (message 'cached:mind-body))
              ((eq? tree (frame 'cached:body/parsed-xml))
               (frame 'cached:mind-body))
              (else #f)))

   (let ((status (http-state-for tree)))
     (if status
	 (list (property 'http-status (http-state-for tree)))
	 '()))

   ))

(define (xml-document->message frame message tree . properties)
  (apply xml-document/content-type->message
	 frame message tree text/xml (message 'dc-date) properties))

;; Make a message of a plain string object.

(define (mime-document->message frame message
                                data content-type location expires date)
  (make-message
   (property 'mind-action-document (frame 'mind-action-document))
   (property 'caller (frame 'get 'id))
   (property 'capabilities (message 'capabilities))
   (property 'dc-creator (message 'dc-creator))
   (property 'dc-date date)
   location
   expires
   (property 'content-type content-type)
   (property 'mind-body (if (or (string? data) (a:blob? data)) data #f))
   (property 'body/parsed-xml
             (cond
              ((eq? data (message 'cached:mind-body))
               (message 'cached:body/parsed-xml))
              ((eq? data (frame 'cached:mind-body))
               (frame 'body/parsed-xml))
              (else #f)))))

(define (mime-document->message* frame message data content-type date
                                 . properties)
  (mime-document->message
   frame message data content-type
   (or (find (lambda (p) (eq? (property-name p) 'location)) properties)
       (property 'location (message-default-location frame message)))
   (or (find (lambda (p) (eq? (property-name p) 'http-expires)) properties)
       (property 'http-expires #f))
   date))

(define (output-element->message frame message output-fragment properties)
  (let ((method (attribute-string 'method (node-list-first output-fragment)))
	(media-type
	 (attribute-string 'media-type (node-list-first output-fragment)))
	(properties (let* ((o (node-list-first output-fragment))
			   (location (attribute-string 'location o))
			   (expires (attribute-string 'expires o)))
		      `(,@(if location (list (property 'location location)) '())
			,@(if expires (list (property 'http-expires location)) '())
			. ,(filter (lambda (p) (not (memq (property-name p) '(expires location))))
				   properties)))))
    (if (or (not media-type) (equal? method "xml") (xml-parseable? media-type))
	(apply xml-document/content-type->message
	       frame message
	       (children output-fragment) (or media-type
					      (node-list-media-type
					       (children output-fragment))
					      text/xml)
	       (askemos:dc-date (message 'dc-date)) properties)
	(receive
	 (value type) (mime-format output-fragment)
	 (apply mime-document->message* frame message
		value type (askemos:dc-date (message 'dc-date))	properties)))))

;;*** Extensions

;; These extensions are for use with the xsl language as well as for
;; the meta data protocol.

(define-syntax define-extension-transformer
  (syntax-rules () ((_ id loc body) (define-transformer id loc body))))

;;**** Send a message to another place.

;; This is the most basic means of communication between objects.

(define is-core-namespace?
  (let ((corenamespaces `(,namespace-mind ,namespace-mind-v0 #f mind http://www.askemos.org/2004/Synchrony/)))
    (define (is-core-namespace? x)
      (and (xml-element? x) (memq (ns x) corenamespaces)))
    is-core-namespace?))

;; The `mind' symbol is here just for backward compatibility; it is
;; bound to go agay.  Don't use it!
(define (is-mind-element? x name)
  (and (eq? (gi x) name) (is-core-namespace? x)))

(define (is-send-extension? x r n) (is-mind-element? x 'send))
;; resolve-next-address makes sure that we don't run into any
;; exceptions from the send operation because of wrong addresses.
;; TODO do something to notify the user.
(define (resolve-next-address place to)
  (let* ((pl (parsed-locator (string-trim-both (data to))))
         (id (if (null? pl) #f (car pl))))
    (and id (let* ((v (place id))
                   (vv (or v (string->oid id))))
              (and vv (cons vv (cdr pl)))))))

;; Usable in xml-walk; send the element as msg in new thread.
(define-extension-transformer do-send-extension "core:send"
  (begin
    ;; TODO add an extension to determine the content type.  This
    ;; should be done so that any element, which is has a name like a
    ;; known slot has it's extension implemented using higher order
    ;; functions.
    (let ((type
	   ;; Until 20140424 we required a type attribute.  This is
	   ;; just insane.  This is only ever executed while a
	   ;; transaction is commited: at that point only type 'write
	   ;; operations are ever useful.
	   ;;
	   ;; (let loop ((type (@? 'type)))
	   ;; 	  (cond
	   ;; 	   ((equal? "write" type) 'write)
	   ;; 	   (else 'read)))
	   'write)
	  (adopt (equal? (@? 'adopt) "yes"))
	  (to-els (select-elements (%children (sosofo)) 'to))
	  (sender ((current-node) 'get 'sender))
	  ;;  Old code, short but predates the soap specification.
	  ;;  (body (%children (node-list-first
	  ;;                    (select-elements (%children nl) 'body))))
	  ;; Now unroled to check for both legacy body and SOAP Body
	  (body (let loop ((nl (%children (sosofo))))
		  (if (node-list-empty? nl)
		      (empty-node-list)
		      (let ((n1 (node-list-first nl)))
			(cond
			 ((match-element? 'Body n1) (document-element (%children n1)))
			 ((match-element? 'body n1)
			  (!start
			   (let ((quorum ((current-place) 'replicates))
				 (self (current-place $$)))
			     (call-subject!
			      (find-frame-by-id/quorum  ((current-place) 'dc-creator) quorum)
			      quorum
			      'write
			      (make-message
			       (property 'capabilities (public-capabilities))
			       (property 'dc-creator self)
			       (property 'dc-date ((current-message) 'dc-date))
			       (property 'content-type text/xml)
			       (property
				'body/parsed-xml
				(sxml
				 `(item
				   (title
				    ,"obsolete all lower case body used in SOAP")
				   (link  "/"
					  ,(oid->string self))
				   (pubDate ,(literal ((current-message) 'dc-date)))))))))
			   'obsolete-body-message)
			  (logerr "obsolete all lower case body used in SOAP\n")
			  (document-element (%children n1)))
			 (else (loop (node-list-rest nl)))))))))
      (let ((content-type (if (match-element? 'output (document-element body))
			      (attribute-string 'media-type body)
			      text/xml)))
	(do ((to-els to-els (cdr to-els)))
	    ((null? to-els))
	  (sender
	   to: (resolve-next-address (current-place) (car to-els))
	   type: type adopt: adopt
	   'content-type content-type 'body/parsed-xml body))))
    (empty-node-list)))

(define send-extension (cons is-send-extension? do-send-extension))

(define (acquire* name contexts)
  (if (null? contexts)
      #f
      (let ((v ((car contexts) name)))
        (if v v (apply acquire name (cdr contexts))))))

(define (acquire name . contexts) (acquire* name contexts))

(define (mind-default-lookup me request name)
  ;; Could use acquire etc. but this should be faster.
  (let ((nn (cond
             ((oid? name) name)
             ((string? name) (string->oid name))
             (else (error (string-append "not a legal locator " name))))))
    (or nn (me name) ((user-context me request) name) ((public-context) name))))

(define (mind-parse-protection str . context)
  (let ((str (string-trim-both str)))
    (if (string=? str "") (unprotected)
	(map (lambda (s)
	       (let ((n (string->oid s)))
		 (cond
		  (n n)
		  ((equal? s "public") (public-oid))
		  ((equal? s "private") (my-oid))
		  (else (acquire* s context)))))
	     (string-split str "/")))))

(%early-once-only

 (define meta-read-request-body
   (make-xml-element
    'form namespace-mind (list (make-xml-attribute 'type #f "read"))
    (empty-node-list)))
)

(define (nunu-new-interp-sql place nl sql)
  (let ((spec (document-element (children nl))))
    (cond
     (sql
      (logapperr (place 'get 'id) "IRGNORED second sql element") sql)
     ((eq? (gi spec) 'blob) spec)
     ((eq? (gi spec) 'id)
      (if (equal? (data spec) (oid->string (place 'get 'id)))
	  (call-with-values
	      (lambda () (block-table-flush!
			  (place/make-block-table
			   (place 'get 'id) (place 'replicates)
			   (place 'sqlite3)
			   ($default-sql-block-size)
			   ($default-sql-bpb))
			  #f))
	    vector)
	  (begin
	    (logapperr (place 'get 'id) "IRGNORED sql ID ~a: NYI, the ID is not here (~a)"
		       (data spec) (place 'get 'id))
	    sql)))
     (else
      (logapperr (place 'get 'id) "IRGNORED STRANGE sql element '~a'" (gi spec))
      sql))))

(define (nunu-make-new-place-of place message environment value)
  (receive
   (links replicates dav-props sqlite3 other)
   (node-list-fold-left
    (%children value)
    (lambda (element links replicates dav-props sql other)
      (cond
       ((is-link-extension? element #f #f)
	(let ((new (prepare-simple-link place message environment element)))
	  (values #t (if (cdr new) (cons new links) links) replicates dav-props sql other)))
       ((and (is-core-namespace? element) (eq? (gi element) 'replicates))
	(values #t links (make-quorum element) dav-props sql other))
       ((and (eq? (ns element) namespace-dav) (eq? (gi element) 'prop))
	(values #t links replicates (cons element dav-props) sql other))
       ((and (is-core-namespace? element) (eq? (gi element) 'sql))
	(values #t links replicates dav-props (nunu-new-interp-sql place element sql) other))
       (else (values #t links replicates dav-props sql (cons element other)))))
    '()
    (or (message 'replicates) (place 'replicates))
    '() #f '())
   (let ((womb (place *dump-message-signal* 'reader))
         (initialize (attribute-string 'initialize value))
         (is-output? (match-element? 'output (document-element other))))
     (receive
      (body type) (if is-output?
                      (let* ((o (document-element other))
			     (b (node-list-first (children o))))
			(cond
			 ((a:blob? b) (values b (attribute-string 'media-type o)))
			 ((and (is-core-namespace? b) (eq? (gi b) 'blob))
			  (values (xml->blob b) (attribute-string 'media-type o)))
			 (else (mime-format other))))
		      ;; others is just consed up in ...fold-left
                      (values (reverse! other) text/xml))
      (let ((baby (womb
                   type: 'write
                   'content-type type
                   (if is-output? 'mind-body 'body/parsed-xml) body
		   'sqlite3 sqlite3
                   'mind-action-document
                   ;; I'm really not sure along which path we should
                   ;; look for the action.  It's a name the user
                   ;; intuitively expects to be found *and* it should be a
                   ;; simple rule.
                   (let* ((att (attribute-string 'action value))
                          (dn (if att (string->oid att) #f)))
                     (cond
		      ((and dn (not (public-oid))) dn) ;; only during setup
                      ((and dn (find-frame-by-id/quorum dn (place 'replicates))) dn)
                      (att
                       (let* ((parsed (parsed-locator att))
			      (p1 (and (pair? parsed) (car parsed))))
			 (receive
			  (oid q) (if p1
				      (let ((soid (string->oid p1)))
					(if soid
					    (values soid #f)
					    (oid-parse p1)))
				      (values #f #f))
			  (let ((obj (or
				      oid
				      (environment-ref/default environment (string->symbol p1) #f)
				      (mind-default-lookup place message p1))))
			    (cond
			     ((and obj (pair? (cdr parsed)))
			      ;;(logerr "Deprecated case: at ~a use of path in action '~a'\n" (place 'get 'id) att)
			      (guard
			       (c (else
				   (receive
				    (title msg args rest) (condition->fields c)
				    (error (format #f
						   "link ~a error reading action ~a: ~a ~a"
						   title att msg args)))))
			       
			       (let ((answer (womb
					      to: (cons obj (cdr parsed))
					      type: 'call
					      'accept application/rdf+xml
					      'body/parsed-xml
					      meta-read-request-body
					      'mind-body #f)))
				 (if
				  (pair? answer)
				  (get-slot (car answer) 'caller)
				  (error (format #f
						 "illegal action ~a ~a" att answer))))))
			     ((not obj) (error (format #f "can't find action ~a" att)))
			     (else obj))))))
                      (else #f)))
		   ;; 2014-08-20: We need (often) to proof the source
		   ;; place, not the creator actually signing the
		   ;; request.  The latter may use un-proven means to
		   ;; create an object, while the source is clearly
		   ;; bound to its contract.
		   ;;
		   ;; We'll need to update the lower level API to
		   ;; support more cases.
		   adopt:
		   (let ((key (attribute-string 'proof value)))
		     (cond
		      ((not key) #f)	; default: the request's signature
		      ((string=? key "creator") #f) ; same as default
		      ((string=? key "origin") #t)  ; proof "parent"
		      ((string=? key "contract")    ; SHOULD proof current-contract
		       (raise "NYI: new with 'proof=\"contract\"'"))
		      (else (raise (format "Undefined proof key ~a" key)))))
		   ;; It is pretty easy to create a new place to log
		   ;; into anyway: just add a secret.
                   'secret (attribute-string 'secret value)
                   ;; ??: 'content-type (attribute-string 'media-type value)
                   'protection
                   (let* ((pa (attribute-string 'protection value)))
                     (if pa
                         (let ((pp (mind-parse-protection pa (public-context))))
                           (if (and pp (memq #f pp))
                               (error (format #f "invalid protection ~a ~a" pa pp))
                               pp))
                         (unprotected)))
		   'replicates replicates
                   'mind-links
                   (and (pair? links)
                        (let ((table (make-mind-links)))
                          (for-each (lambda (l)
                                      (link-bind! table (car l) (cdr l)))
                                    links)
                          table))
		   'dav:properties (and (pair? dav-props) (car dav-props))
		   )))
        (if (and initialize (not (equal? initialize "")))
            ((place 'get 'sender)
             to: baby type: 'write
             'location (message 'location)
             'content-type text/plain 'mind-body initialize))
        baby)))))

(define (link-envt-with-let*-bindings place message environment nl)
  (let ((environment (environment-copy environment)))
    (node-list-reduce
     (children (select-elements (%children nl) 'bindings))
     (lambda (dummy binding)
       (and (eq? (gi binding) 'bind)
	    (let ((result (prepare-simple-link place message environment binding)))
	      (environment-extend!
	       environment (string->symbol (car result)) (cdr result) #t))))
     #f)
    environment))

(define (link-envt-with-let-bindings place message environment nl)
  (let ((local (environment-copy environment)))
    (node-list-reduce
     (children (select-elements (%children nl) 'bindings))
     (lambda (dummy binding)
       (and (eq? (gi binding) 'bind)
	    (let ((result (prepare-simple-link place message environment binding)))
	      (environment-extend!
	       local (string->symbol (car result)) (cdr result) #t))))
     #f)
    local))

(define (link-drop-bindings nl)
  (let loop ((nl (%children nl)))
    (if (node-list-empty? nl) nl
	(let ((x (node-list-first nl)))
	  (if (not (eq? (gi x) 'bindings)) nl (loop (node-list-rest nl)))))))

(define (prepare-simple-link place message environment nl)
  ;; TODO compare with XLink and evaluate XLink syntax here.
  (let ((name (string-trim-both 
	       (or (attribute-string 'name nl)
		   (error "missing \"name\" attribute to link to")))))
    (let loop ((environment environment)
	       (value (document-element (%children nl)))) ; FIXME, need first elem
      (let ((result (cond
		     ;; FIXME rethink whether something like this
		     ;; (compare with XLink) would be the way to go or
		     ;; have oids defined as a fixed syntax (regex?).
		     ;; (let ((pool (select-elements (%children value)
		     ;; 'pool)) (cell (select-elements (%children
		     ;; value) 'cell))) (cond ((node-list-empty? pool)
		     ;; (string->oid (data value))) ((node-list-empty?
		     ;; cell) (error "missing cell to link to")) (else
		     ;; (if (eqv? (string->oid (data pool))
		     ;; (public-oid)) value (error "remote oids not yet
		     ;; implemented")))))
		     ((not value) #f)                  ; removes link
		     ((is-core-namespace? value)
		      (case (gi value)
			((id) (receive
			       (oid q) (oid-parse (data value))
                               (cond
                                ((not oid) (error "invalid oid in link/id ~a" (data value)))
                                (q (find-frame-by-id/quorum oid q)))
                                oid))
#|
			((qid)
			 (let ((oid (string->oid
				     (data (select-elements (%children value) 'id))))
			       (source (select-elements (%children value) 'replicates)))
			   (if (not (node-list-empty? source))
			       (find-frame-by-id/quorum oid (make-quorum source)))
			   oid))
|#
			((new) (nunu-make-new-place-of place message environment value))
			((ref) (environment-ref environment (string->symbol (data value))))
			((let)
			 (cdr (loop (link-envt-with-let-bindings place message environment value)
				    (document-element (link-drop-bindings value)))))
			((letseq)
			 (cdr (loop (link-envt-with-let*-bindings place message environment value)
				    (document-element (link-drop-bindings value)))))
			(else (error "unknown object ~a" (xml-format value)))))
		     (else (error (format "link error: not a valid object description: ~a"
					  (if (node-list? value) (xml-format value) value)))))))
	(cons name result)))))

(define (do-simple-link place message environment nl)
  (let ((link (prepare-simple-link place message environment nl)))
    ((place 'get 'linker) (car link) (cdr link))
    link))

(define (is-link-extension? x r n) (is-mind-element? x 'link))
(define-extension-transformer do-link-extension "core:link"
  (begin
    (do-simple-link (current-place) (current-message) ($< 'environment) (sosofo))
    (empty-node-list)))
(define link-extension (cons is-link-extension? do-link-extension))

(define (is-let-extension? x r n) (is-mind-element? x 'let))
(define-extension-transformer do-let-extension "core:let"
  (begin
    (transform
     variables: (bind-xsl-value
		 (environment) 'environment
		 (link-envt-with-let-bindings
		  (current-place) (current-message) ($< 'environment) (sosofo)))
     sosofos: (link-drop-bindings (sosofo)))
    (empty-node-list)))
(define let-extension (cons is-let-extension? do-let-extension))

(define (is-let*-extension? x r n) (is-mind-element? x 'letseq))
(define-extension-transformer do-let*-extension "core:letseq"
  (begin
    (transform
     variables: (bind-xsl-value
		 (environment) 'environment
		 (link-envt-with-let*-bindings
		  (current-place) (current-message) ($< 'environment) (sosofo)))
     sosofos: (link-drop-bindings (sosofo)))
    (empty-node-list)))
(define let*-extension (cons is-let*-extension? do-let*-extension))

(define (is-grant-extension? x r n) (is-mind-element? x 'grant))
(define-extension-transformer do-grant-extension "core:grant"
  (let ((name (string-trim-both
               (data (document-element (select-elements (%children (sosofo)) 'to)))))
        (right (document-element (select-elements (%children (sosofo)) 'right))))
    ((make-grant (current-place) (current-message))
     (mind-default-lookup (current-place) (current-message) name)
     (or (let ((r (node-list->right right))) (and (pair? r) r))
         (mind-parse-protection (data right))))
    (empty-node-list)))
(define grant-extension (cons is-grant-extension? do-grant-extension))

(define (is-revoke-extension? x r n) (is-mind-element? x 'revoke))
(define-extension-transformer do-revoke-extension "core:revoke"
  (let ((name (string-trim-both
               (data (document-element (select-elements (%children (sosofo))
                                                        'from)))))
        (right (document-element (select-elements (%children (sosofo)) 'right))))
    ((make-revoke (current-place) (current-message))
     (mind-default-lookup (current-place) (current-message) name)
     (or (let ((r (node-list->right right))) (and (pair? r) r))
         (mind-parse-protection (data right))))
    (empty-node-list)))
(define revoke-extension (cons is-revoke-extension? do-revoke-extension))

(define (is-trustedcode-extension? x r n) (is-mind-element? x 'TrustedCode))
(define-extension-transformer do-trustedcode-extension "sys:TustedCode"
  (begin
    (((current-place) (@! 'name) 'TrustedCode) (%children (sosofo)))
    (empty-node-list)))
(define trustedcode-extension (cons is-trustedcode-extension?
                                    do-trustedcode-extension))

(define (is-entrypoint-extension? x r n) (is-mind-element? x 'EntryPoint))
(define-extension-transformer do-entrypoint-extension "sys:EntryPoint"
  (let* ((host (@? 'host))
	 (name (@! 'name))
	 (quorum ((current-place) 'replicates))
	 (host-quorum (make-quorum (list (if host (or (string->oid host) host) (public-oid))))))
    (if (check-administrator-password (@? 'secret))
	(if (and (quorum-local? host-quorum) (quorum-local? quorum))
	    (make-local-entry-point! quorum name (%children (sosofo)))
	    (!start
	     (let loop ((try 1))
	       (thread-sleep!/ms (* try $delay-user-replication))
	       (or (support-entry-point! host-quorum name)
		   (if (fx>= try 5)
		       (begin (logerr "USER ~a NOT FOUND at ~a\n" name
				      (xml-format (quorum-xml quorum)))
			      (empty-node-list))
		       (loop (add1 try))))
	       (let ((new-entry-id (or (entry-name->oid name)
				       (error "fatal: new entry point \"~a\" doesn't exist"
					      name))))
		 (with-a-ref
		  new-entry-id
		  (find-frame-by-id/quorum new-entry-id quorum))))
	     (format "support entry ~a@~a as ~a" name host name)))
	;; Silently fail, the request applies to another replicate.
	(begin
	  (logerr " Ignoring user creation request for ~a at ~a\n"
		  name (quorum-others quorum))))
    ;; No result value.
    (empty-node-list)))
(define create-entrypoint-extension (cons is-entrypoint-extension?
                                          do-entrypoint-extension))

(define (is-sql-extension? x r n) (is-mind-element? x 'update))
(define-extension-transformer do-sql-extension "sys:SQL"
  (let* ((has-params? (let ((att (attribute-string 'parameterized (sosofo))))
			(and att ;; backbard compatible
			     (not (equal? att "no")))))
	 (template (if has-params?
		       (select-elements (%children (sosofo)) 'template)
		       (%children (sosofo))))
	 (params (if has-params?
		     (node-list-map
                      (lambda (param)
                        (let ((spec (document-element (children param))))
                          (if (and (xml-element? spec) (eq? (gi spec) 'ref))
                              (literal (environment-ref ($< 'environment) (string->symbol (data spec))))
                              (data param))))
                      (select-elements (%children (sosofo)) 'with-param))
		     '()))
	 (update (transform sosofos: template)))
    ;; FIXME: Add some error checking.  Enforce just one template an
    ;; params following it for clarity.
    (cond
     ((node-list-empty? update))
     (else
      ((current-place)
       (if (and (pair? update) (symbol? (car update)))
	   (begin
	     (logerr "Does this ever happen in the core:update operation?\n")
	     (sql-write update))
	   (list (data update) params))
       'sql)))
    ;; No result value.
    (empty-node-list)))
(define sql-extension (cons is-sql-extension? do-sql-extension))

(define (is-reference-extension? x r n) (is-mind-element? x 'ref))
(define-extension-transformer do-reference-extension "core:ref"
  (literal
   #\'
   (sql-quote
    (environment-ref
     ($< 'environment)
     (string->symbol (data (sosofo)))))
   #\'))
(define reference-extension (cons is-reference-extension? do-reference-extension))

(define-syntax make-general-extension
  (syntax-rules ()
    ((_ loc slot value-function)
     (let ((predicate (lambda (x r n) (is-mind-element? x slot))))
       (define-extension-transformer transformer loc
	 (begin
	   (((current-place) 'get 'writer) slot (value-function (sosofo)))
	   (empty-node-list)))
       (cons predicate transformer)))))

;; Change the secret.  Only avail to nodes describing users.  This
;; looks a bit complicated but it's a good sample of code pieces,
;; which SHOULD be available in a library for reuse but aren't yet.

(define pass-extension
  (make-general-extension "sys:secret" 'secret (lambda (s) (string-trim-both (data s)))))

;; Change the protection

(define protection-extension
  (make-general-extension
   "sys:protection" 'protection
   (lambda (node)
     (let ((prot (mind-parse-protection (data node))))
       (if (and prot (memq #f prot))
	   (error (format #f "could not parse protection ~a" (data node)))
	   prot)))))

;; There is actually no reason to be as strict as we are now.  The
;; capability could be any symbol and reights any sequence thereof.
;; The old and maybe future way to interpret a right was broader:
;;(define (capability-extension-generator node)
;;  (define (interpret-capability-token token)
;;    (let* ((d (data token)) (n (string->oid d)))
;;      (cond
;;       (n n)                            ; found a document
;;       ((string=? d "") (error "empty capability token"))
;;       (else (string->symbol d)))))
;;  (node-list-map interpret-capability-token (%children node)))

(define capability-extension
  ;; Would be: (make-general-extension 'capability node-list->right)
  ;; but doesn't work since we write via different keyword.
  (let ((predicate (lambda (x r n) (is-mind-element? x 'right))))
    (define-extension-transformer transformer "core:capatility"
      (begin
	(((current-place) 'get 'writer) capability: (node-list->right (sosofo)))
	(empty-node-list)))
    (cons predicate transformer)))

(define distribution-extension
  (make-general-extension
   "core:support" 'replicates
   ;;(lambda (node) (document-element (children node)))
   (lambda (node)
     (let ((c (document-element (children node))))
       ;; Bug compatible: prefer a nested level of 'replicates'.
       (if (eq? (gi c) 'replicates) c node)))))

(define (is-forward-extension? x r n) (is-mind-element? x 'forward))
(define forward-extension (cons is-forward-extension? xml-drop))

(define xml-drop-special
  (cons (lambda (x r n) (or (is-mind-element? x 'version)
			    (is-mind-element? x 'output)
			    (is-mind-element? x 'date)))
        xml-drop))

(define xmlns-drop
  (cons (lambda (x r n) (and (xml-element? x) (eq? (gi x) 'xmlns))) xml-drop))

;; ----------------------------------------------------------------------
;; WebDAV related modifications

;; TODO move helpers elsewhere

(define (is-dav-extension? x r n)
  (is-mind-element? x 'propertyupdate))

(define-extension-transformer do-dav-extension "DAV:properties"
  (let ((place (current-place)))
    ((place 'get 'writer)
     'dav:properties
     (dav:accept-propertyupdate
      (or (place 'dav:properties) 
	  (make-xml-element 'prop namespace-dav '() '()))
      (sosofo)))
    (empty-node-list)))

(define dav:property-extension
  (cons is-dav-extension? do-dav-extension))

(define (is-sequence-extension? x r n) (is-mind-element? x 'sequence))
(define-extension-transformer do-sequence-extension "sys:sequence"
  (let* ((first  (node-list-first (%children (sosofo))))
	 (more   (node-list-rest (%children (sosofo))))
	 (caller ((current-message) 'reply-channel))
	 (self   (current-place $$))
	 ;; <api:sequence>
	 ;;  [<api:step destination="path/oid" continue-from="oid">
	 ;;    {list of extensions}
	 ;;   </api:step>]*
	 ;; </api:sequence>
	 (send (lambda (sender step reply-channel)
		 (let ((to (let ((dst (parsed-locator 
				       (or (attribute-string 'destination step) ""))))
			     (if (null? dst) self dst)))
		       (body (%children step)))
		   (sender type: 'write to: to body: body 'reply-channel reply-channel)
					;? 'content-type "application/soap+xml"
		   (string->oid (or (attribute-string 'continue-from step) "")))))
	 ;; <api:sequence>
	 ;;  [<api:step trigger="oid">
	 ;;    <to>"path/oid"</to>
	 ;;     <Body>
	 ;;      {list of extensions}
	 ;;     </Body>
	 ;;   </api:step>]*
	 ;; </api:sequence>
	 (send2 (lambda (sender step reply-channel)
		  (let ((to (let ((dst (parsed-locator (data ((sxpath '((to 1))) step)))))
			      (if (null? dst) self dst)))
			(body ((sxpath '((Body 1) *)) step)))
		    (sender type: 'write to: to body: body 'reply-channel reply-channel)
					;? 'content-type "application/soap+xml"
		    (string->oid (or (attribute-string 'trigger step) ""))))))

    (cond ((node-list-empty? first)
	   (logapperr self "E: sequence-handler: empty sequence! Is this intended?\n")
	   (empty-node-list))
	  ((node-list-empty? more)
	   (logapperr self "W: sequence-handler: single step sequence! Is this intended?\n")
	   (if (string=? "" (or (attribute-string 'destination first) ""))
	       (transform sosofos: (%children first))
	       (send ((current-place) 'get 'sender) first caller))
	   ;;           new syntax:
	   ;; 	    (if (string=? "" (data ((sxpath '((to 1))) first)))
	   ;; 		(execute-extensions place message environment
	   ;;                         	    ((sxpath '((Body 1) *)) first))
	   ;;                                    (send (place 'get 'sender) first caller))
	   (empty-node-list))
	  (else
	   (let* ((mbox (make-mailbox 'sequence))
		  (trigger (send ((current-place) 'get 'sender) first mbox))
		  (tmo (register-timeout-message! 3 mbox)))
	     ;;(logerr "D: sequence-handler: first step sent, start future and wait for ~a\n" trigger)
	     (!start
	      (let steper ((next (node-list-first more))
			   (more (node-list-rest more))
			   (trigger trigger))
		(let loop ((result (receive-message! mbox)))
		  (cond
		   ((or (timeout-object? result) (message-condition? result))
		    (cancel-timeout-message! tmo)
		    (logapperr
		     self "E: sequence-handler: exception in step before ~a, terminating.\n~a"
		     (if (timeout-object? result) "timeout" (condition-message result))
		     (xml-format next))
		    (mailbox-send! caller result)
		    #f) ;; finish future
		   ((not (frame? result))
		    (logapperr
		     self "E: sequence-handler: ignore result in sequence ~a~a\n next is ~a\n"
		     (cond-expand (rscheme (class-name (object-class result)))
				  (else result))
		     result (xml-format next))
		    (mailbox-send! caller result)
		    (loop (receive-message! mbox)))
		   ((eq? trigger (get-slot result 'caller))
					;(send-message! caller result)
		    (guard 
		     (c (else
			 (logapperr
			  self "E: sequence-handler: terminating! Exception ~a~a, sending ~a\n"
			  (cond-expand (rscheme (class-name (object-class c)))
				       (else c))
				 c (xml-format next))
			 (mailbox-send! caller c)
			 (cancel-timeout-message! tmo)
			 #f)) ;; finish future
		     (cond 
		      ((node-list-empty? more)
		       ;;(logerr "D: sequence-handler: trigger from ~a, send last step and terminate\n" trigger)
		       (send (make-sender #t (find-frames-by-id self) (current-message)
					  (askemos:dc-date (get-slot result 'dc-date)))
			     next caller)
		       (cancel-timeout-message! tmo)
		       #f) ;; finish future
		      (else
		       ;;(logerr "D: sequence-handler: trigger from ~a, send next step\n" trigger)
		       (steper (node-list-first more)
			       (node-list-rest more)
			       (send (make-sender #t (find-frames-by-id self) (current-message)
						  (askemos:dc-date (get-slot result 'dc-date)))
				     next mbox))))))
		   (else
		    ;; (send-message! caller result)
		    ;;(logerr "I: sequence-handler: Ignoring, not a trigger message from ~a"
		    ;;	     (get-slot result 'caller))
		    (loop (receive-message! mbox))))))
	      'sequence))))))
		     
(define sequence-extension 
  (cons is-sequence-extension? do-sequence-extension))

;; END of WebDAV related changes.
;;----------------------------------------------------------------------

(define any-place-extentions
  (list
   forward-extension
   link-extension
   send-extension
   protection-extension
   capability-extension
   distribution-extension
   let-extension
   let*-extension
   reference-extension
   dav:property-extension
   sequence-extension
   trustedcode-extension
   sql-extension
   create-entrypoint-extension
   xmlns-drop
   xml-drop-special))

(define entry-point-extentions
  (list
   pass-extension
   grant-extension
   revoke-extension))

(define entry-point-effective-extentions
  (append entry-point-extentions any-place-extentions))

(define (askemos-core-add-extension! type predicate procedure)
  (cond
   ((eq? type any:)
    (set! any-place-extentions
	  `((,predicate . ,procedure)
	    . ,(remove (lambda (x) (eq? predicate x)) any-place-extentions)))
    (set! entry-point-effective-extentions
	  (append entry-point-extentions any-place-extentions)))
   (else
    (set! entry-point-extentions
	  `((,predicate . ,procedure)
	    . ,(remove (lambda (x) (eq? predicate x)) entry-point-extentions)))
    (set! entry-point-effective-extentions
	  (append entry-point-extentions any-place-extentions)))))

(define (default-execute-extensions frame request environment answer)
  ;; (define own-right (list (frame 'get 'id)))  TODO cleanup
  ;; (define capabilities (request 'capabilities))
  ((xml-walk*
    frame request
    #f					; root-node
    (bind-xsl-value #f 'environment environment)
    '()					; namespaces
    '() frame (%children (document-element answer)) '())
   (if ;; (and (pair? capabilities) (full-right? own-right capabilities))
    (eqv? (frame 'get 'id) (request 'dc-creator))
    entry-point-effective-extentions
    any-place-extentions)))

(define execute-extensions default-execute-extensions)

(define (interpret-rw frame request answer)
  (if (not (xml-element? (document-element answer)))
      (mime-document->message* frame request
                               answer text/plain
                               (askemos:dc-date (request 'dc-date)))
      (xml-document->message
       frame request
       (execute-extensions frame request (make-environment) answer))))

(define (public-meta-info obj)
  (let ((ans (guard
	      (ex (else (default-public-meta-info obj)))
	      (message-body
	       (call-subject! obj *local-quorum* 'read (force public-read-metainfo-request))))))
    ;; We should check that all fields are filled in as required for
    ;; public meta info.  However for we need the only jurisdiction for
    ;; routing leave the rest up to the judges.
    (if (node-list-empty? ((sxpath '(Description replicates)) ans))
	(default-public-meta-info obj)
	ans)))

(: metainfo-implementation
   (:place-accessor: :message-accessor:
		     ;; TODO: check, maybe the string-list is never used.
		     (or :oid: string (list-of string)) ; object or path
		     (list-of *)
		     -> :xml-nodelist:))
(define (metainfo-implementation place request object keys)
  (define (find-frame! n)
    (or (find-frame-by-id/quorum n (place 'replicates))
	(raise (make-object-not-available-condition
		n (place 'get 'id) 'metainfo))))
  (if (string? object) (set! object (or (string->oid object) (parsed-locator object))))
  (if (eq? (place 'get 'id) object)
      (access-signature place)
      (guard
       (ex ((object-not-available-condition? ex) (raise ex))
	   ((pair? object)
	    (receive
	     (p n) (let loop ((s '()) (d object))
		     (if (null? (cdr d))
			 (values (reverse! s) (car d))
			 (loop (cons (car d) s) (cdr d))))
	     (if (null? p)
		 (default-public-meta-info
		   (find-frame! (or (string->oid n)
				    (place n)
				    (raise (make-object-not-available-condition
					    n (place 'get 'id) 'metainfo:1)))))
		 (let* ((ans (apply fetch-other
				    place p
				    'accept application/rdf+xml
				    'content-type text/xml
				    'body/parsed-xml metainfo-request-element
				    keys))
			(which (resolve-link ans n)))
		   (default-public-meta-info
		     (find-frame! (or which
				      (raise (make-object-not-available-condition
					      object (place 'get 'id) 'metainfo:2)))))))))
	   ((oid? object) (default-public-meta-info  (find-frame! object)))
	   (else (raise (format "~a metainfo: illegal request ~a" (place 'get 'id) object))))
       (let ((ans (document-element
		   (apply fetch-other
			  place object
			  'accept application/rdf+xml
			  'content-type text/xml
			  'body/parsed-xml metainfo-request-element
			  keys))))
	 (if (node-list-empty? ((sxpath '(Description replicates)) ans))
	     (raise (format "at ~a no replicates found in\n ~a"  (place 'get 'id) (xml-format ans)))
	     ans)))))

(set-meta-info! metainfo-implementation)

(define (is-metainfo-request? request)
  (and (is-meta-form? request)
       (equal? (request 'accept) application/rdf+xml)))

(define dav-propfind-message
  (make-shared-parameter (lambda args (error "WebDAV not enabled"))))

(define is-propfind-request?
  (make-shared-parameter (lambda args #f)))

(define (default-meta-lookup type)
  ((make-context (public-oid))
   (case type
     ((read) "metaview")
     ((write) "metactrl")
     (else ""))))

;; (define $meta-lookup-method (make-parameter default-meta-lookup))

(define (make-global-parameter default)
  (let ((value default))
    (lambda args
      (if (pair? args)
          (let ((old value))
            (set! value (car args))
            old)
          value))))

(define $meta-lookup-method (make-global-parameter default-meta-lookup))

(define (default-metaview me request)
  (let ((template (or (($meta-lookup-method) 'read)
		      (default-meta-lookup 'read))))
    (cond
     ;; Should we realy do metainfo and dav propfind here as
     ;; fallbacks?  Maybe we not.  At the other hand, users might just
     ;; disable the whole metaview.  After all: "metaview" is supposed
     ;; to do just the right thing.  No matter how.
     (((is-propfind-request?) request)
      (or (and-let* ((d (request 'destination))
		     ((pair? d))
		     (x (car d))
		     ((not (equal? x ""))))
		    ((dav-propfind-message) me request children: x))
	  ((dav-propfind-message) me request)))
     ((equal? (request 'accept) application/rdf+xml)
      (xml-document->message me request (access-signature me)))
     (else
      (if template                       ; should only happen in setup.
	  (let ((result (document-element
			 ((make-xslt-transformer me request)
			  (make-xml-element
			   'request namespace-mind read-attribute-list
			   (request 'body/parsed-xml))            
			  (if (oid? template)
			      (aggregate-body
			       (find-frame-by-id/quorum template (me 'replicates)))
			      template)))))
	    (if (eq? (gi result) 'output)
		(output-element->message
		 me request (document-element result) '())
		(xml-document->message me request result)))
	  (xml-document->message
	   me request metainterface-not-available-response))))))

(define (default-metactrl-reply me request)
  (let ((template (or (($meta-lookup-method) 'write)
		      (default-meta-lookup 'write))))
    (if template			; should only happen in setup.
	((make-xslt-transformer me request)
	 (make-xml-element
	  'request namespace-mind write-attribute-list
	  (request 'body/parsed-xml))
	 (if (oid? template)
	     (aggregate-body (or (find-frame-by-id/quorum template (me 'replicates))
				 (raise (make-object-not-available-condition
					 template (me 'get 'id) 'metactrl-reply))))
	     template))
	metainterface-not-available-response)))

(define (log-deprecated origin interface) ; TODO: move towards logging
  (logapperr origin "W: deprecated interface (DSSSL?) ~a\n" interface))

(define metaview (lambda (place message)
		   (log-deprecated (place 'get 'id) 'metaview)
		   (default-metaview place message)))
(define metactrl
  (lambda (place message)
    (log-deprecated (place 'get 'id) 'metactrl)
    (xml-document->message place message (default-metactrl-reply place message))))

(set-function-metaview! default-metaview)
(set-function-metactrl! default-metactrl-reply)
