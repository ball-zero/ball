;; (C) 2010, 2013 Jörg F. Wittenberger see http://www.askemos.org

(: make-block-table
   (fixnum
    fixnum fixnum
    (procedure (:blob: string) :blob:); store
    (procedure (:blob:) string); fetch
    (procedure (string (procedure (fixnum string) undefined)) undefined);read
    (procedure ((vector-of (or false :blob:))) string); write
    (or false procedure)
    --> :block-table:))
(: block-table? (* -->  boolean : :block-table:))
(: block-table-size (:block-table: -> fixnum))
(: set-block-table-size! (:block-table: number -> *))
(: block-table-bs (:block-table: --> fixnum))
(: block-table-bpb (:block-table: --> fixnum))
(: block-table-blocks (:block-table: -> (or (vector-of (or false :blob:)) false)))
(: set-block-table-blocks! (:block-table: (or false (vector-of (or false :blob:))) -> *))

(: block-table-store (:block-table: --> (procedure (:blob: string) :blob:)))
(: block-table-fetch (:block-table: --> (procedure (:blob:) string)))
(: block-table-read (:block-table: --> (procedure (string (procedure (fixnum string) undefined)) undefined)))
(: block-table-write (:block-table: --> (procedure ((vector-of (or false :blob:))) string)))

(define-record-type <block-table>
  (%make-block-table
   mux
   bs
   bpb
   size
   store
   fetch
   read
   write
   display
   blocks)
  block-table?
  (mux block-table-mux)
  (bs block-table-bs)
  (bpb block-table-bpb)
  (size block-table-size set-block-table-size!)
  (store block-table-store)
  (fetch block-table-fetch)
  (read block-table-read)
  (write block-table-write)
  (display block-table-display)
  (blocks block-table-blocks set-block-table-blocks!)
  )

(define (make-block-table bs bpb size store fetch read write display)
  (%make-block-table
   (make-mutex 'blocks)
   bs
   bpb
   size
   store
   fetch
   read
   write
   display
   #f))

(define-syntax make-bt-node
  (syntax-rules ()
    ((_ table) (make-vector (add1 (block-table-bpb table)) #f))))

(: bt-node-set! (vector fixnum (or false vector :blob:) -> undefined))
(define-inline (bt-node-set! n i v)
  (vector-set! n i v))

(define (next-bucket t v i r)
  (let ((e (vector-ref v i)))
    (if (vector? e) e
	(if e
	    (let ((nv (blob-blocks e)))
	      (if nv
		  (if r (begin (bt-node-set! v i nv) nv) nv)
		  (with-mutex
		   (block-table-mux t)
		   (let ((nv (make-bt-node t)))
		     ((block-table-read t)
		      ((block-table-fetch t) e)
		      (lambda (i v) (bt-node-set! nv i v)))
		     (if r (bt-node-set! v i nv))
		     (set-blob-blocks! e nv)
		     nv))))
	    (let ((nv (make-bt-node t)))
	      (bt-node-set! v i nv)
	      nv)))))

(: block-table-for-each
   (:block-table: (procedure (:blob:) . *) -> boolean))
(define (block-table-for-each t proc)
  (let loop ((depth 0)
	     (v (block-table-blocks t)))
    (do ((i 0 (add1 i)))
	((eqv? i (block-table-bpb t))
	 (and-let* ((b (vector-ref v i)))
		   (proc b)
		   (loop (add1 depth)
			 (next-bucket t v i #f))))
      (and-let* ((b (vector-ref v i)))
		(proc b)
		(or (eqv? depth 0)
		    (loop (sub1 depth)
			  (next-bucket t v i #f)))))))

(: load-block-table!
   (:block-table: :blob: -> :block-table:))
(define (load-block-table! t blob)
  (with-mutex
   (block-table-mux t)
   (if (not (block-table-blocks t))
       (set-block-table-blocks! t (make-bt-node t)))
   ((block-table-read t)
    ((block-table-fetch t) blob)
    (lambda (i v) (bt-node-set! (block-table-blocks t) i v)))
   (set-block-table-size! t (a:blob-size blob)))
  t)

(define (block-path/depth bpb depth blkno)
  (if (eq? depth 0)
      (if (< blkno bpb)
	  (list blkno)
	  (cons bpb
		(block-path/depth
		 bpb
		 (add1 depth)
		 (- blkno bpb))))
      (let* ((size (expt bpb depth))
	     (ibn (quotient blkno size)))
	(if (< ibn bpb)
	    (cons ibn
		  (block-path/depth
		   bpb
		   (sub1 depth)
		   (- blkno (* ibn size))))
	    (cons bpb
		  (block-path/depth
		   bpb
		   (add1 depth)
		   (- blkno size)))))))

;;(: block-table-set! (:block-table: fixnum string #!rest -> *))
(define (block-table-set! t blkno value . cs)
  (if (fx< (block-table-bs t) (string-length value))
      (raise 'block-table-set!-value-too-large))
  (if (not (block-table-blocks t))
      (set-block-table-blocks! t (make-bt-node t)))
  (let ((ns (fx+ (fx* (block-table-bs t) blkno)
		 (string-length value))))
    (if (fx< (block-table-size t) ns)
	(set-block-table-size! t ns)))
  (let loop ((path (block-path/depth (block-table-bpb t) 0 blkno))
	     (v (block-table-blocks t)))
    (if (null? (cdr path))
	(let ((i (car path))
	      (l (string-length value))
	      (cs (if (pair? cs) (car cs) (string->symbol (sha256-digest value)))))
	  (let ((e (vector-ref v i)))
	    (if e
		(if (not (eq? (blob-sha256 e) cs))
		    (let ((bv (a:make-blob cs l l 1 #f)))
		      (bt-node-set! v i bv)
		      ((block-table-store t) bv value)))
		(let ((bv (a:make-blob cs l l 1 #f)))
		  (bt-node-set! v i bv)
		  ((block-table-store t) bv value)))))
	(loop (cdr path)
	      (next-bucket t v (car path) #t)))))

(: block-table-update! (:block-table: number string -> string))
(define (block-table-update! t at value)
  (define bs (block-table-bs t))
  (define blkno (quotient at bs))
  (define offset (modulo at bs))
  (if (not (block-table-blocks t))
      (set-block-table-blocks! t (make-bt-node t)))
  (if (fx< bs (fx+ offset (string-length value)))
      (begin
	;; Well meaning application code will care to write only within blocks.
	(logerr "Warning: block-table-update! value exceeds block boundary\n")
	(block-table-update!
	 t (fx* (add1 blkno) bs)
	 (substring/shared value (fx- bs offset) (string-length value)))
	(set! value (substring/shared value 0 (fx- bs offset)))))
  (let ((ns (fx+ (fx* (block-table-bs t) blkno)
		 (fx+ (string-length value) offset))))
    (if (fx< (block-table-size t) ns)
	(set-block-table-size! t ns)))
  (let loop ((path (block-path/depth (block-table-bpb t) 0 blkno))
	     (v (block-table-blocks t)))
    (if (null? (cdr path))
	(let* ((i (car path))
	       (l (string-length value))
	       (e (vector-ref v i))
	       (old (and e ((block-table-fetch t) e)))
	       (new (cond
		     ((not old)
		      (if (eqv? offset 0) value
			  (let ((new (make-string/uninit
				      (fx+ offset (string-length value)))))
			    (memory-set! new #\x0 0 offset)
			    (move-memory! value new l 0 offset)
			    new)))
		     ((eqv? offset 0)
		      (if (fx>= l (string-length old)) value
			  (let ((new (make-string/uninit (string-length old))))
			    (move-memory! value new l 0 0)
			    (move-memory! old new (fx- (string-length old) l) l l)
			    new)))
		     ((fx< (string-length old) offset)
		      (let ((new (make-string/uninit (fx+ l offset)))
			    (ol (string-length old)))
			(move-memory! old new ol 0 0)
			(memory-set! new #\x0 ol (fx- offset ol)) ; zero fill hole
			(move-memory! value new l 0 offset)
			new))
		     (else
		      (let* ((r0 (fx+ l offset))
			     (ol (string-length old))
			     (new (make-string/uninit (if (fx< r0 ol) ol r0))))
			(move-memory! old new offset 0 0)
			(move-memory! value new l 0 offset)
			(if (fx< r0 ol)
			    (move-memory! old new (fx- ol r0) r0 r0))
			new))))
	       (cs (string->symbol (sha256-digest new))))
	  (if (not (and e (eq? (blob-sha256 e) cs)))
	      (let ((bv (a:make-blob cs l l 1 #f)))
		(bt-node-set! v i bv)
		((block-table-store t) bv new))))
	(loop (cdr path)
	      (next-bucket t v (car path) #t)))))

(: block-table-truncate! (:block-table: number -> number))
(define (block-table-truncate! t size)
  (if (fx< size (block-table-size t))
      (begin
	(let loop ((path (block-path/depth (block-table-bpb t) 0
					   (quotient size (block-table-bs t))))
		   (v (block-table-blocks t)))
	  (if (null? (cdr path))
	      (let ((last (modulo size (block-table-bs t))))
		(do ((i (if (eqv? last 0) (car path) (add1 (car path))) (add1 i)))
		    ((fx< (block-table-bpb t) i))
		  (bt-node-set! v i #f))
		(if (eqv? last 0) v
		    (let* ((old ((block-table-fetch t) (vector-ref v (car path))))
			   (new (substring old 0 last))
			   (cs (string->symbol (sha256-digest new)))
			   (bv (a:make-blob cs last last 1 #f)))
		      ((block-table-store t) bv new)
		      (bt-node-set! v (car path) bv)
		      v)))
	      (let ((i (car path)))
		(bt-node-set! v i (loop (cdr path) (next-bucket t v (car path) #t)))
		(if (and (eqv? i 0) (not (vector-ref v 0)))
		    #f
		    (begin
		      (do ((i (add1 i) (add1 i)))
			  ((fx>= i (vector-length v)) v)
			(bt-node-set! v i #f)))))))
	(set-block-table-size! t size)))
  (block-table-size t))

(: block-table-ref/default
   (:block-table: number (or boolean :blob:) --> (or boolean :blob:)))
(define (block-table-ref/default t key default)
  (let loop ((path (block-path/depth (block-table-bpb t) 0 key))
	     (v (block-table-blocks t)))
    (if v
	(if (null? (cdr path))
	    (or (vector-ref v (car path)) default)
	    (loop (cdr path) (next-bucket t v (car path) #f)))
	default)))

(: block-table-ref
   (:block-table: number (procedure () :blob:) -> :blob:))
(define (block-table-ref t key default)
  (let loop ((path (block-path/depth (block-table-bpb t) 0 key))
	     (v (block-table-blocks t)))
    (if v
	(if (null? (cdr path))
	    (or (vector-ref v (car path)) (default))
	    (loop (cdr path) (next-bucket t v (car path) #f)))
	(default))))

(: block-table-resolve/default
   (:block-table: number (or boolean string) -> (or boolean string)))
(define (block-table-resolve/default t key default)
  (let loop ((path (block-path/depth (block-table-bpb t) 0 key))
	     (v (block-table-blocks t)))
    (if v
	(if (null? (cdr path))
	    (let ((b (vector-ref v (car path))))
	      (if b ((block-table-fetch t) b) default))
	    (loop (cdr path) (next-bucket t v (car path) #f)))
	default)))

(define (add-diff! e e0 delta)
  (if (not (or (eq? e e0)
	       (and (a:blob? e) (a:blob? e0)
		    (eq? (blob-sha256 e) (blob-sha256 e0)))))
      (begin
	(if (a:blob? e) (set-car! delta (cons e (car delta))))
	(if (a:blob? e0) (set-cdr! delta (cons e0 (cdr delta)))))))

(: v->p (:block-table: (vector-of (or false :blob:)) -> string))
(define-inline (v->p t v)
#|
  (or (not v) (vector? v) (error l))
  (srfi:vector-for-each
   (lambda (i v)
     (or (not v) (a:blob? v) (error l)))
   v)
|#
  ((block-table-write t) v))

  (: v->b (:block-table: (vector-of (or false :blob:)) -> :blob:))
  (define-inline (v->b t v)
    (let* ((c (v->p t v))
	   (d (string->symbol (sha256-digest c)))
	   (bv (a:make-blob
		d
		(string-length c)
		(block-table-bs t)
		(block-table-bpb t)
		v)))
      ((block-table-store t) bv c)
      bv))
  ;; Pulling the loop out of the main definition in order to make
  ;; chicken smarter about it.
  (: btf-loop (:block-table:
	       pair
	       fixnum
	       (or (vector-of (or false :blob: vector)) vector false)
	       (or (vector-of (or false :blob: vector)) vector false)
	       ->
	       (or false :blob:)))
  (define (btf-loop t delta depth v v0)
    (if (eq? depth 0)
	(if (vector? v)
	    (let ((nv (vector-ref v (block-table-bpb t))))
	      (if (vector? nv)
		  (bt-node-set! v (block-table-bpb t)
			       (btf-loop t delta 1 nv (and v0 (next-bucket t v0 (block-table-bpb t) #f)))))
	      (let* ((c (v->p t v))
		     (d (string->symbol (sha256-digest c)))
		     (bv (a:make-blob
			  d
			  (block-table-size t)
			  (block-table-bs t)
			  (block-table-bpb t)
			  v)))
		((block-table-store t) bv c)
		;; update diff
		(if (vector? v0)
		    (do ((i (block-table-bpb t) (sub1 i)))
			((eqv? i -1)
			 (let ((i (block-table-bpb t)))
			   (btf-loop t delta 1
				     (next-bucket t v i #f)
				     (next-bucket t v0 i #f))))
		      (add-diff! (vector-ref v i) (vector-ref v0 i) delta))
		    (do ((i (block-table-bpb t) (sub1 i)))
			((eqv? i -1)
			 (let ((i (block-table-bpb t)))
			   (btf-loop t delta 1 (next-bucket t v i #f) #f)))
		      (add-diff! (vector-ref v i) #f delta)))
		bv))
	    (begin
	      ;; update diff
	      (if (vector? v0)
		  (do ((i 0 (add1 i)))
		      ((fx>= i (vector-length v0))
		       (btf-loop t delta 1 #f (next-bucket t v0 (block-table-bpb t) #f)))
		    (add-diff! #f (vector-ref v0 i) delta)))
	      v))
	(cond
	 ((and (vector? v) (vector? v0))
	  (do ((i (sub1 (block-table-bpb t)) (sub1 i)))
	      ((eqv? i -1)
	       (set! i (block-table-bpb t))
	       (let ((e (vector-ref v i))
		     (e0 (vector-ref v0 i)))
		 (if (not (or (eq? e e0)
			      (and (a:blob? e) (a:blob? e0)
				   (eq? (blob-sha256 e) (blob-sha256 e0)))))
		     (let ((e (btf-loop t delta (add1 depth)
					(next-bucket t v i #f)
					(next-bucket t v0 i #f))))
		       (bt-node-set! v i e)
		       (add-diff! e e0 delta)))
		 (v->b t v)))
	    (let ((e (vector-ref v i))
		  (e0 (vector-ref v0 i)))
	      (if (not (or (eq? e e0)
			   (and (a:blob? e) (a:blob? e0)
				(eq? (blob-sha256 e) (blob-sha256 e0)))))
		  (let ((e (btf-loop t delta (sub1 depth)
				     (next-bucket t v i #f)
				     (next-bucket t v0 i #f))))
		    (bt-node-set! v i e)
		    (add-diff! e e0 delta))))))
	 ((vector? v0)
	  (assert (eq? v #f))
	  (do ((i 0 (add1 i)))
	      ((fx>= i (vector-length v0))
	       (set! i (block-table-bpb t))
	       (let ((e0 (vector-ref v0 i)))
		 (if e0
		     (begin
		       (btf-loop t delta (add1 depth) #f (next-bucket t v0 i #f))
		       (add-diff! #f e0 delta))))
	       #f)
	    (let ((e0 (vector-ref v0 i)))
	      (if e0
		  (begin
		    (btf-loop t delta (sub1 depth) #f (next-bucket t v0 i #f))
		    (add-diff! #f e0 delta))))))
	 ((vector? v)
	  (do ((i 0 (add1 i)))
	      ((fx>= i (vector-length v))
	       (set! i (block-table-bpb t))
	       (if (vector-ref v i)
		   (bt-node-set! v i (btf-loop t delta (add1 depth) (next-bucket t v i #f) #f)))
	       (add-diff! (vector-ref v i) #f delta)
	       (v->b t v))
	    (if (vector-ref v i)
		(bt-node-set! v i (btf-loop t delta (sub1 depth) (next-bucket t v i #f) #f)))
	    (add-diff! (vector-ref v i) #f delta)))
	 (else (assert (eq? v #f)) v))))

(: block-table-flush!
   (:block-table: (or false :blob: :block-table:)
    -> (or false :blob:) (list-of :blob:) (list-of :blob:)))
(define (block-table-flush! t t0)
  (let* ((t1 (if (block-table? t0) t0
		 (let ((tb (make-block-table
			    (block-table-bs t)
			    (block-table-bpb t)
			    0
			    (block-table-store t)
			    (block-table-fetch t)
			    (block-table-read t)
			    (block-table-write t)
			    (block-table-display t))))
		   (if (a:blob? t0) (load-block-table! tb t0) tb))))
	 (delta (cons '() '()))
	 (v (btf-loop t delta 0 (block-table-blocks t) (block-table-blocks t1))))
    (values v (car delta) (cdr delta))))
