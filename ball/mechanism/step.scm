;; (C) 2000 - 2003, 2005, 2007-2010 Jörg F. Wittenberger see http://www.askemos.org

;; These are only the most basic process steps, which can be performed
;; at a place.

;; Dispatch to another place.

(define (dispatch-to place message type dst)
  (let* ((next (if (pair? dst) (car dst) dst))
         (oid (place next)))		; resolve name
    ;; TODO: this SHOULD better be the best result.  It's just
    ;; undefined how to obtain it.  As implemented here so far, the
    ;; sender/reader seems to be in charge to figure the best result.
    ;; Hence the API needs some cleanup not to return a list of
    ;; results having the only and best result first, right?  But that
    ;; might not match well with other uses? -> TODO
    (car
     (if oid
	 ((place 'get (if (eq? type 'write) 'sender 'reader))
	  to: (if (pair? dst) (cons oid (cdr dst)) oid) type: type
	  'location (cons (if (pair? dst) (car dst) dst) (message 'location)))
	 ;; Special case for the host's root namespace.
	 (let ((oid (and (eq? (place 'get 'id) (public-oid))
			 ((public-context) next))))
	   (if oid
	       (let ((to (if (pair? dst) (cons oid (cdr dst)) oid))
		     (location (cons (if (pair? dst) (car dst) dst) (message 'location))))
		 (if (eq? type 'write)
		     ((place 'get 'sender) to: to type: type
		      'location location)
		     ((place 'get 'reader) to: to type: type
		      'location location
		      'host (message 'host))))
	       (raise (make-object-not-available-condition
		       (read-locator (message 'location-format) (reverse dst))
		       (place 'get 'id)
		       'dispatch-to))))))))

;;*** Routing and (basic) Access

;; Most places will just dispatch along their "axons", if the
;; destination is not et reached.  (Possibly checking access before.)

(define-macro (with-dispatch place message type body)
  `(let ((dst (,message 'destination)))
     (if (or (null? dst) (and (null? (cdr dst))
			      ((is-propfind-request?) ,message)))
         ,body
	 (dispatch-to ,place ,message ,type dst))))

;; At the most basic level any place is just delivered as it is.

(define $access-denied-reveals-creadentials #f)

(define (raise-access-denied-condition me request)
  (let ((name (me 'get 'id)))
    (raise (make-condition &forbidden
			   'resource (request 'location)
			   'source name
			   'message (if $access-denied-reveals-creadentials
					(format "access denied for ~a Protection ~a Credentials ~a"
						name (me 'protection) (request 'capabilities))
					(format "access denied for ~a" name))
			   'status 403))))

(define (read-only-plain-transformation me request)
  (if ((make-service-level (me 'protection) (request 'capabilities))
       (my-oid))
      (with-dispatch
       me request pishing-mode-compatible-forward-call
       (if (is-meta-form? request)
           (cond
	    (((is-propfind-request?) request)
	     (or (and-let* ((d (request 'destination))
			    ((pair? d))
			    (x (car d))
			    ((not (equal? x ""))))
			   ((dav-propfind-message) me request children: x))
		 ((dav-propfind-message) me request)))
	    ((equal? (request 'accept) application/rdf+xml) ;; (is-metainfo-request? request)
	     ;; TODO we SHOULD  cache that until the next transaction!
	     (xml-document->message me request (access-signature me)))
	    (else (metaview me request)))
           (let ((content-type (me 'content-type)))
	     (if (xml-parseable? content-type)
		 (xml-document/content-type->message
		  me request (me 'body/parsed-xml) content-type (me 'dc-date))
		 (mime-document->message*
		  me request (me 'mind-body) content-type (me 'dc-date))))))
      ;; FIXME: we should distinguish access denied messages for the
      ;; read and write case.  Here, in the read only case, we protect
      ;; data privacy (secrecy).
      (raise-access-denied-condition me request)))

(define public-place
  (let ((cnd (make-condition
	      &forbidden
	      'status 403 'message "This is a public place.
Nobody has the right to take this data away."
	      'resource #f 'source #f)))
    (lambda () (raise cnd))))

(define *transaction-private-plain-slots*
  '(mind-body body/parsed-xml content-type dc-date dc-creator))

;; At first sight the following seems incorrect: the apparent
;; corresponding viewer, read-only-plain-transformation (which is the
;; other default method), produces some forms (via metaview) , which
;; need to be processed by "metactrl" before they are understood here.
;; Due to this asymetry the most simple form of a private place (as it
;; was in force before WebDAV time), which has just
;; "transaction-private-plain-transformation!"  as accept-part of the
;; action document, is rather inconvinient.  Real applications ought
;; to pre-process requests (and perform plausibility checks).

(define (transaction-private-plain-transformation! me request . result)
  (if ((make-service-level (me 'protection) (request 'capabilities)))
      (with-dispatch
       me request 'write
       (within-time
        (respond-timeout-interval)      ; actually protects just
					; agains large data
	(if (is-meta-form? request)
	    ;; FIXME: "become" SHOULD be handled in "execute-extensions".
	    ;; Handling it here just avoids a bigger change for now.
	    (let* ((result (if (and (pair? result) (not (eq? (car result) 'dummy)))
			       (car result) (request 'body/parsed-xml)))
		   (become (select-elements (children result) 'become)))
	      (if (not (node-list-empty? become))
		  (receive
		   (value type) (mime-format (children become))
		   (let ((settor (me 'get 'writer)))
		     (settor 'mind-body value)
		     (settor 'content-type type))))
	      (interpret-rw me request result))
	    (let ((settor (me 'get 'writer)))
	      (settor 'replicates *local-quorum*)
	      (do ((slots *transaction-private-plain-slots* (cdr slots)))
		  ((null? slots) #t)
		(settor (car slots) (request (car slots))))))))
      ;; The naive implementation did say:
      ;; (error "This is a private property, sorry.")
      ;; Please note: we deny a property delect here, since the object
      ;; would otherwise change, thus depriving the regular owner from
      ;; further use of the property.
      (raise-access-denied-condition me request))
  (xml-document->message me request (empty-node-list)))

(define (transaction-public-plain-transformation! me request . result-dummy)
  (public-place))

;;* Process Steps

;; TODO: It might be a good idea to interwave the evaluation of the
;; send-extension with the xslt-transform.  But this would require
;; that the whole transformation was successful before any side
;; effects take place.
;;
;; MUST NOT allow any send's here.  Otherwise a distributed system
;; could give birth to contrary messages.))))

(define know-proxy-protocols '("http:"))

(define (xslt-request-childen request)
  (let ((content-type (request 'content-type)))
    (cond
     ((not content-type) (empty-node-list))
     ((soap+xml-content? content-type)
      (let ((envelope (document-element (request 'body/parsed-xml))))
        (if (and (eq? (gi envelope) 'Envelope)
                 (eq? (ns envelope) namespace-soap-envelope))
            (children (select-elements (children envelope) 'Body))
            envelope)))
     ((xml-parseable? content-type)
      (or (document-element (request 'body/parsed-xml)) (empty-node-list)))
     ((equal? content-type "text/x-bail")
      (request 'mind-body))
     (else (empty-node-list)))))

;; If this is the entry point and we have a referrer, then we may
;; adopt the request, but only if the browsers position is obvious to
;; the user, otherwise we drop all but the public capabilities.
;;
;; The whole idea is experimental and in strong need of discussion.

(define (forward-drop-capabilities me request type)
  (and (not (eq? type 'read))
       (prevent-pishing public-oid me request) ; for compatibility
       (eq? (me 'get 'id) (request 'dc-creator)) ; hm
       (and-let*
	((referrer (request 'referer))
	 (uri (string->uri referrer))
	 (pp (parsed-locator (uri-path uri))))
	(if (or (null? pp)
		(eq? (string->oid (car pp)) (me 'get 'id))
		(equal? pp (request 'destination)))
	    #f
	    (public-capabilities)))))

(define kernel-extent-proxy-forward
  (make-shared-parameter
   (lambda (access request how)
     (error "kernel-extent-proxy-forward not implemented"))))

(define (read-only-xslt-transformation me request code)
  (let ((result ((make-xslt-transformer me request)
		 (make-xml-element
		  'request namespace-mind read-attribute-list
		  (xslt-request-childen request))
		 code))
	(destination (request 'destination)))
    (cond
     ((frame? (node-list-first result)) (node-list-first result))
     ((not (xml-element? (document-element result)))
      (mime-document->message*
       me request
       (if (string? result) result "") "text/plain"
       (askemos:dc-date (request 'dc-date)) ;; FIXME should be now!
       ))
     ((is-forward-extension? (document-element result) #f #f)
      (let ((d0 (request 'destination)))
	(if (and (eq? (me 'get 'id) (request 'dc-creator)) ; user place only
		 (member (car d0) know-proxy-protocols)
		 (not (or (me (cadr d0)) (string->oid (cadr d0)))))
	    ((kernel-extent-proxy-forward) me request 'read)
	    (let ((destination (if (member (car d0) know-proxy-protocols)
				   (cdr d0) d0))
		  (capa (forward-drop-capabilities me request 'read)))
	      (car (if capa
		       ((me 'get 'reader)
			to: destination
			;; type: 'call
			type: pishing-mode-compatible-forward-call
			capabilities: capa
			'location (cons (car destination) (request 'location)))
		       ((me 'get 'reader)
			to: destination
			;; type: 'call
			type: pishing-mode-compatible-forward-call
			'location (cons (car destination) (request 'location)))))))))
     ((eq? (gi (document-element result)) 'output)
      (output-element->message me request (document-element result) '()))
     (else (xml-document->message me request result)))))

(define (reflexive-read-only-xslt-transformation me request)
  (read-only-xslt-transformation me request (me 'body/parsed-xml)))

(define (bail/read me request)
  (reflexive-read-only-xslt-transformation me request))

(define (read-contract me)
  (document-element (fetch-other me (me 'mind-action-document) body: #f)))

(define (bail/confirm me request)
  (read-only-xslt-transformation me request (read-contract me)))

(define default-xslt-reply-attributes
  (list
   (make-xml-attribute 'ac 'xmlns namespace-mind-str)
   (make-xml-attribute 'rdfs 'xmlns namespace-rdf-str)
   ;; (make-xml-attribute 'xslt 'xmlns namespace-xsl-str)
   ))

(define (xslt-transformation-propose me request code)
  (let ((result (document-element
		  ((make-xslt-transformer me request)
		   (make-xml-element
		    'request namespace-mind write-attribute-list
		    (xslt-request-childen request))
		   code))))
    ;;        (if (and xslt-transformation-debuged
    ;;                 (hash-table-ref/default xslt-transformation-debuged (me 'get 'id) #f))
    ;;            (hash-table-set! xslt-transformation-debuged (me 'get 'id) result))
    (cond
     ;; TODO re-remove adding of the version.  This ought to be handled
     ;; one level down.
     ((is-mind-element? result 'reply)
      (make-xml-element
       'reply namespace-mind default-xslt-reply-attributes
       (node-list
	;; TODO remove the version element.  It's here left over
	;; from the experimental stage; now moved into the protocol
	;; layer.
	(make-xml-element 'version namespace-mind '()
			  (literal (or (me 'version) 0)))
	(make-xml-element
	 'date namespace-mind '() (date->time-literal (request 'dc-date)))
	(children result))))
     ((is-mind-element? result 'forward)
      (make-xml-element
       'forward namespace-mind default-xslt-reply-attributes
       (node-list
	(make-xml-element 'version namespace-mind '()
			  (literal (or (me 'version) 0)))
	(make-xml-element
	 'date namespace-mind '() (date->time-literal (request 'dc-date)))
	(children result))))
     (else (error "reflexive-xslt-transformation-propose: unexpected reply ~a"
		  (if (xml-element? result) (xml-format result) result))))))

(define (reflexive-xslt-transformation-propose me request)
  (xslt-transformation-propose me request (me 'body/parsed-xml)))

(define (bail/propose me request)
  (reflexive-xslt-transformation-propose me request))

(define (bail/submit me request)
  (xslt-transformation-propose me request (read-contract me)))

(define (bail/accept me request result)
  (if (is-forward-extension? (document-element result) #f #f)
      (let ((d0 (request 'destination)))
        (let ((destination (if (member (car d0) know-proxy-protocols)
			       (cdr d0) d0))
	      (capa (forward-drop-capabilities me request 'write)))
	  (if capa
	      ((me 'get 'sender) to: destination type: 'write
	       capabilities: capa
	       'location (cons (car destination) (request 'location)))
	      ((me 'get 'sender) to: destination type: 'write
	       'location (cons (car destination) (request 'location))))
	  ;; You know: RPCIsBrokenByDesign.  Contrary to common
	  ;; misbelieve I'm sure that we must not wait for the result
	  ;; of the operation.
	  (xml-document->message me request has-been-forwarded-nl)))
      (let* ((unknown (execute-extensions me request (make-environment) result))
	     (output (select-elements (children result) 'output))
             (continuation (let ((new (select-elements (children result) 'become)))
			     (if (node-list-empty? new)
				 (select-elements (children result) 'continue)
				 new)))
             (body (if (node-list-empty? continuation)
                       #f (children continuation)))
             (location (and (not (node-list-empty? output))
                            (attribute-string 'location
                                              (node-list-first output))))
             (args (or (and location (list (property 'location location)))
                       '()))
             (settor (me 'get 'writer)))
        (if body
	    (begin
	      (settor 'body/parsed-xml body)
	      (settor 'content-type
		      (let ((enc (and (eq? (xml-pi-tag (node-list-first body)) 'xml)
				      (xml-pi-encoding (xml-pi-data (node-list-first body))))))
			(if (member enc '(#f "UTF8" "UTF-8")) 
			    text/xml (string-append "text/xml; charset=" enc)))))
	    (let ((unknown
		   (node-list-filter
		    (lambda (e)
		      (not (or (eq? e continuation)
			       (eq? e output))))
		    unknown)))
	      (if (pair? unknown)
		  (logerr "Unknown reply ~a\n~a\n" (me 'get 'id) (xml-format unknown)))))
        (cond
         ((node-list-empty? output)
          (xml-document->message me request (empty-node-list)))   
         ((and (node-list-empty? (node-list-rest output))
               (xml-parseable? (attribute-string 'media-type
                                                 (node-list-first output))))
          (apply xml-document->message
                 me request (children (node-list-first output)) args))
         ;; I'm not really satisfied with the default.  It
         ;; serialized too many things.
	 ;; If we (re)activate the lower-half behavior in respond!
	 ;; this should be wrapped with (lambda () ...) 
         (else (output-element->message me request output args))))))

(define (xslt-transformation-accept me request result)
  (bail/accept me request result))

;; BEWARE 'transaction-xslt-transformation!' is only for backward
;; compatibility.  There are two kinds of ActionDocument's the older
;; (semi obsolete) style with one method of type 'write' and those
;; with 'propose' and 'accept' methods.  The former has a bug with
;; regard to transaction restart (causes oid clash).  However since
;; the new style is preferable anyway, this might not be worth fixing.

(define (transaction-xslt-transformation! me request)
  (let ((result (reflexive-xslt-transformation-propose me request)))
    (and-let* ((dbg ($broadcast-debug-proposal))
	       ((procedure? dbg)))
	      (dbg (me 'get 'id) me request result))
    (values
     (xslt-transformation-accept me request result)
     (xml-digest-2-simple result))))
