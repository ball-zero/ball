;; (C) 2000, 2001, 2002, 2005, 2013 Jörg F. Wittenberger see http://www.askemos.org

(define (raise-message fmt . args)
  ;;(raise (make-condition &message 'message (apply format fmt args)))
  (error (apply format fmt args)))

;;* prepare some kind of read syntax virtualisation

;; This appears to be an expensive idea of questionable value.
;; DEPRECIATED.  Instead we'd rather pay the cost of another slot in
;; xml elements (which this originally tried to avoid).

(define *make-xml-element-ns-handlers* (make-symbol-table))
(define *make-xml-element-default-ns-handlers* (make-symbol-table))

(define (make-specialised-xml-element gi ns attributes content)
  (or (if
       ns
       (and-let* ((entry (hash-table-ref/default *make-xml-element-ns-handlers* ns #f))
                  (elcons (if (hash-table? entry)
			      (hash-table-ref/default entry gi #f)
			      entry)))
                 (elcons gi ns attributes content))
       (and-let* ((elcons (hash-table-ref/default
                           *make-xml-element-default-ns-handlers* gi #f)))
                 (elcons gi ns attributes content)))
      (make-xml-element gi ns attributes content)))

(define (register-xml-element-constructor! gi ns constructor)
  (cond
   ((not (procedure? constructor))
    (error "register-xml-element-handler! constructor ~a not a procedure"
           constructor))
   ((not ns)
    (hash-table-set! *make-xml-element-default-ns-handlers* gi constructor))
   (else (let ((entry (hash-table-ref/default *make-xml-element-ns-handlers* ns #f)))
           (cond
            ((and (hash-table? entry) gi) (hash-table-set! entry gi constructor))
            ((procedure? entry)
             (error "register-xml-element-handler! namespace ~a already handled globally" ns))
            ((not gi)
             (hash-table-set! *make-xml-element-ns-handlers* ns constructor))
            (else (let ((table (make-symbol-table)))
                    (hash-table-set! *make-xml-element-ns-handlers* ns table)
                    (hash-table-set! table gi constructor)))))))
  #t)

;;* SGML/XML Handling

;; Useful parts from sdc (sdc "SGML Document Compiler" is an old,
;; fast, powerful SGML formater) to parse sgml/xml files.  It
;; demonstrates lazy tree walking, which is missing here and a lot of
;; other small code pieces, which could be handy.  This nsgmls driver
;; is more or less copied from there.  It should at least for XML be
;; replaced without fork(2).
;;
;; See the bm2wb script, which uses sdc as well.

;;** Name Spaces

(: find-namespace-by-prefix
   ((procedure (:xml-namespace:) boolean) (list-of :xml-namespace:)
    --> (or false :xml-namespace:)))
(define (find-namespace pred lst)
  (let loop ((lst lst))
    (and (pair? lst) (or (and (pred (car lst)) (car lst))
			 (loop (cdr lst))))))

(: find-namespace-by-prefix
   ((or false symbol) (list-of :xml-namespace:)
    --> (or false :xml-namespace:)))
(define (find-namespace-by-prefix prefix lst)
  (let loop ((lst lst))
    (and (pair? lst)
	 (or (and (eq? (xml-namespace-local (car lst)) prefix) (car lst))
	     (loop (cdr lst))))))

(: find-namespace-by-uri
   (symbol (list-of :xml-namespace:) --> (or false :xml-namespace:)))
(define (find-namespace-by-uri uri lst)
  (let loop ((lst lst))
    (and (pair? lst)
	 (or (and (eq? (xml-namespace-uri (car lst)) uri) (car lst))
	     (loop (cdr lst))))))

(: xml-split-name (string --> symbol (or false symbol)))
(define (xml-split-name name)
  ;; (assert (string? name))
  (let ((colon (string-index name #\:)))
    (if colon
        (values (string->symbol (substring name (add1 colon)
                                           (string-length name)))
                (string->symbol (substring name 0 colon)))
        (values (string->symbol name) #f))))

(define namespace-xml-str "http://www.w3.org/XML/1998/namespace")
(define namespace-xml (string->symbol namespace-xml-str))
(define namespace-xsl-str "http://www.w3.org/1999/XSL/Transform")
(define namespace-xsl (string->symbol namespace-xsl-str))
(define namespace-dsssl1-str "http://www.askemos.org/2000/NameSpaceDSSSL")
(define namespace-dsssl1 (string->symbol namespace-dsssl1-str))
(define namespace-dsssl2-str "http://www.askemos.org/2005/NameSpaceDSSSL/")
(define namespace-dsssl2 (string->symbol namespace-dsssl2-str))
(define namespace-dsssl3-str "http://www.askemos.org/2013/bail/")
(define namespace-dsssl3 (string->symbol namespace-dsssl3-str))
;; TODO: do the transition!
(define namespace-dsssl-str namespace-dsssl1-str)
(define namespace-dsssl (string->symbol namespace-dsssl-str))
(define namespace-sync-str "http://www.askemos.org/2004/Synchrony/")
(define namespace-sync (string->symbol namespace-sync-str))
(define namespace-rdf-str "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
(define namespace-rdf (string->symbol namespace-rdf-str))
;; This used to be http://purl.org/dc/elements/1.0/ now version 1.1:
(define namespace-dc-str "http://dublincore.org/documents/2004/12/20/dces/")
(define namespace-dc (string->symbol namespace-dc-str))
;; FIXME the old definition is sort of meaningless.
(define namespace-mind-str-v0 "http://www.askemos.org/2000/CoreAPI")
(define namespace-mind-v0 (string->symbol namespace-mind-str-v0))
(define namespace-mind-str-v1 "http://www.askemos.org/2000/CoreAPI#")
(define namespace-mind-v1 (string->symbol namespace-mind-str-v1))
(define namespace-mind-str namespace-mind-str-v1)
(define namespace-mind namespace-mind-v1)
(define namespace-html-str "http://www.w3.org/1999/xhtml")
(define namespace-xhtml (string->symbol namespace-html-str))
(define namespace-html #f)
(define namespace-soap-envelope-str "http://www.w3.org/2003/05/soap-envelope")
(define namespace-soap-envelope (string->symbol namespace-soap-envelope-str))
(define namespace-forms-str "http://www.askemos.org/2000/NameSpaceHTMLForm")
(define namespace-forms (string->symbol namespace-forms-str))
(define namespace-http-str "http://www.w3.org/Protocols/rfc2616/rfc2616.html")
(define namespace-http (string->symbol namespace-http-str))
(define namespace-dav-str "DAV:")
(define namespace-dav (string->symbol namespace-dav-str))
(define namespace-xsql-str "http://www.askemos.org/2006/XSQL/")
(define namespace-xsql (string->symbol namespace-xsql-str))

(define namespace-lout-str "lout")
(define namespace-lout (string->symbol namespace-lout-str))

(define namespace-svg-str "http://www.w3.org/2000/svg")
(define namespace-svg (string->symbol namespace-svg-str))
(define namespace-xlink-str "http://www.w3.org/1999/xlink")
(define namespace-xlink (string->symbol namespace-xlink-str))

(: make-xml-namespace-declaration (symbol string --> :xml-ns-decl:))
(define (make-xml-namespace-declaration prefix url)
  (make-xml-attribute prefix 'xmlns url))

(: xml-namespace-declaration? (* --> boolean : :xml-ns-decl:))
(define (xml-namespace-declaration? obj)
  (and (xml-attribute? obj)
       (eq? (xml-attribute-ns obj) 'xmlns)))

(define namespace-coreapi-declaration
  (make-xml-namespace-declaration 'mind namespace-mind-str))

(define-record-type <xml-document>
  (make-xml-document document-children)
  xml-document?
  (document-children xml-document-children))

(: xml-document? (* --> boolean : :xml-document:))

;; DSSSL 10.1.3

(define named-node-list-names
  (let ((f (lambda (x) (symbol->string (xml-attribute-name x)))))
    (lambda (nnl) (map f nnl))))

;; named-node-list-symbols is for kernel use.  It doesn't convert the
;; symbolic name into a string, as DSSSL would require.  We could save
;; the efford, if we would internalize strings as Scheme does when
;; converting them into a symbol, without the Scheme data type
;; "symbol".  Than we could use eq? to compare those internalized
;; strings (the original decision why we don't want the names to be
;; strings) and still apply string operations (as DSSSL intends).

(define (named-node-list-symbols nnl) (map car nnl))


(: find-first-attribute
   ((procedure (:xml-attribute:) *) :xml-element: --> (or :xml-attribute: false)))
(define (find-first-attribute pred nl)
  (let loop ((atts (attributes nl)))
    (and (pair? atts) (or (and (pred (car atts)) (car atts))
                          (loop (cdr atts))))))

(: attribute-string ((or symbol string) :xml-element: --> (or false string)))
(define (attribute-string aname node)    ; 10.2.4.3
  (let* ((attid (if (string? aname) (string->symbol aname) aname))
         (attribute-node
          (let loop ((atts (attributes node)))
            (and (pair? atts)
                 (or (and (eq? (xml-attribute-name (car atts)) attid)
                          (car atts))
                     (loop (cdr atts)))))))
    (and attribute-node (xml-attribute-value attribute-node))))

(: dsssl-attribute-string
   ((or string symbol) (or :xml-element: list) --> (or string false)))
(define (dsssl-attribute-string name node)
  (if (xml-element? node)
      (attribute-string name node)
      (and (pair? node)
           (and (node-list-empty? (node-list-rest node))
                (let ((node (node-list-first node)))
                  (and (xml-element? node)
                       (attribute-string name node)))))))

(: required-attribute-string
   (symbol :xml-element: * --> string))
(define (required-attribute-string name node error-message)
  (let ((att (attribute-string name node)))
    (or att
        (raise-message "Missing required attribute \"~a\" in ~a"
		       name error-message))))

(: attribute-string-defaulted
   ((or symbol string) :xml-element: string --> string))
(define (attribute-string-defaulted name node dflt)
  (or (attribute-string name node) dflt))

;; The attribute-ns EXTENTION parallels 10.2.4.3 attribute-string for
;; namespaces, but it's sort of broken, as it finds only the first
;; attribute with 'name'.
(: attribute-ns ((or symbol string) :xml-element: --> (or false string)))
(define (attribute-ns name node)    
  (let ((attribute-node
         (find-first-attribute
          (node-typeof? (if (string? name) (string->symbol name) name))
          node)))
    (and attribute-node (xml-attribute-ns attribute-node))))

;(define (gi node) (vector-ref node 1))

;; lt. DSSSL gi has an optional node list argument defaulting to
;; current-node.  This is too expensive for the kernel for now.  To
;; comply we would also need to check that node is actually a
;; singleton node list.

;(define (gi node)
;  (let ((node (node-list-first node)))
;    (and (xml-element? node) (name node))))

;(define (ns node) (vector-ref node 2))
;(define (%children node) (vector-ref node 4))

(define (assert-cadr args)
  (if (null? (cdr args))
      (error "make: key ~a without argument" (car args))
      (cadr args)))
(define (as-symbol v)
  (if (string? v)
      (if (string=? v "")
	  (error "dsssl:make element empty string invalid as symbol")
	  (string->symbol v))
      v))

(define (dsssl-make-element-make-attribute name ns value)
  (if (string-index (symbol->string name) #\:)
      (raise-message "dsssl-make-element: illegal attribute name ~a" name))
  (make-xml-attribute name ns value))

(define (dsssl-make-element args)
  (let loop ((args args) (gi #f) (ns #f) (atts '()) (childs '()))
    (if (null? args)
        (if gi
            (make-specialised-xml-element
             gi ns atts
             ;; If we see just one list as first child we flatten the
             ;; list now otherwise we leave that for whoever cares.
             (if (and (pair? childs)
                      (pair? (car childs))
                      (null? (cdr childs)))
                 (car childs)
                 childs))
            (raise-message "make: no gi found."))
        (case (car args)
          ((gi:)
           (loop (cddr args) (as-symbol (assert-cadr args)) ns atts childs))
          ((ns:)
           (loop (cddr args) gi (as-symbol (assert-cadr args)) atts childs))
          ((attributes:)
           (loop (cddr args)
                 gi ns
                 (map (lambda (a)
                        (if (pair? (cddr a)) ; 3 elems in list -> ns syntax
                            (dsssl-make-element-make-attribute
                             (as-symbol (cadr a))
                             (as-symbol (car a))
                             (caddr a))
                            ;; traditional J. Clark extension syntax
                            (dsssl-make-element-make-attribute
                             (as-symbol (car a)) #f (cadr a))))
                      (assert-cadr args))
                 childs))
          (else (loop '() gi ns atts args))))))

(define dsssl-element-key '(xml-element))
(define dsssl-comment-key '(xml-comment))
(define dsssl-pi-key '(xml-pi))
(define (dsssl-make key . args)
  (cond
   ((eq? key dsssl-element-key) (dsssl-make-element args))
   ((eq? key dsssl-comment-key) (make-xml-comment (apply-string-append args)))
   ((eq? key dsssl-pi-key)
    (make-xml-pi (if (symbol? (car args))
                     (car args) (string->symbol (car args)))
                 (apply-string-append (cdr args))))
   (else (raise-message "make: sosofo ~a unknown." key))))

(define (copy-attributes nd)            ; not dsssl but practical
  (let loop ((atts (or (attributes nd) '())))
    (if (null? atts)
        '()
        (cons (list (xml-attribute-ns (car atts))
                    (xml-attribute-name (car atts))
                    (xml-attribute-value (car atts)))
              (loop (cdr atts))))))

(: match-element? (symbol :xml-element: --> boolean))
(define (match-element? pattern snl)    ; 10.2.5
  (cond
   ((pair? pattern)                     ; TODO
    (error "match-element?: list patterns not yet implemented."))
   ((pair? snl)
    (error "match-element?: not a singleton node list."))
   (else (eq? (gi snl) pattern))))

;(define (make-xml-literal data)
;  (vector 'xml-literal (if (string? data) data (format #f "~a" data))))

; (define-macro (make-xml-literal data) data)

;(define (literal . args)
;  (vector 'xml-literal (to-string* args)))

(define (literal . args) (make-xml-literal (to-string* args)))

;(define (xml-literal? node)
;  (and (vector? node) (eq? (vector-ref node 0) 'xml-literal)))

(define xml-literal? string?)

;(define-macro (xml-literal-value lit) `(vector-ref ,lit 1))
; (define-macro (xml-literal-value lit) lit)


;; xml-output is a special version of literals, which is not escaped
;; upon output.
(define (make-xml-output . args)
  (vector 'xml-output (to-string* args)))
(define (xml-output? node)
  (and (vector? node) (eq? (vector-ref node 0) 'xml-output)))

;; data moved down due to use of node-list-rest

(define (make-xml-pi tag line) (vector 'xml-pi tag line))
(define (xml-pi? node)
  (and (vector? node) (eq? (vector-ref node 0) 'xml-pi)))

(define (xml-pi-tag node)
  (and (xml-pi? node) (vector-ref node 1)))

(define (xml-pi-data node)
  (and (xml-pi? node) (vector-ref node 2)))

;; FIXME this ought raise errors if we find a -- inside the comment.
(define (make-xml-comment line) (vector 'xml-comment line))
(define (xml-comment? node)
  (and (vector? node) (eq? (vector-ref node 0) 'xml-comment)))

(define (xml-comment-data node)
  (and (xml-comment? node) (vector-ref node 1)))

;; TODO Make this more than just a dummy.
(define (make-xml-doctype data)
  (vector 'DOCTYPE data))

;;** Node Lists and Tree Queries

;; I'm a bit unsure whether this node-list? be keept as it is.  DSSSL
;; hides the difference between a node and a singleton node list.  But
;; I don't understand why.

(define node-list?
  (let ((node-types '(xml-pi xml-comment xml-element xml-literal xml-attribute
                             DOCTYPE)))
    (define (node-list? obj)
      (or (null? obj)
          (xml-element? obj)
          (xml-literal? obj) (xml-attribute? obj)
	  (xml-document? obj)
          (and (vector? obj) (memq (vector-ref obj 0) node-types))
          (and (pair? obj) (node-list? (car obj)))))
    node-list?))

;; This predicate stems from sxpath.  It makes certain assumtions of
;; the data model.  Take care and don't use it where it doesn't
;; belong.
(define (nodeset? obj)
  (or (null? obj) (and (pair? obj) (node-list? (car obj)))))

(define (attribute-node-list? obj)
  (or (xml-attribute? obj)
      (and (pair? obj) (xml-attribute? (car obj)))))

; (define-macro (empty-node-list) ''())   ; dsssl 10.2.2

(define (fun:empty-node-list) (empty-node-list))

;; TODO node-list should be reimplemented using rdp's lazy evaluated
;; stream.  At least it should be rewritten in a tail recursive way.
;; It's to late for today.

;(define (node-list . other)
;  (let loop ((other other) (next (empty-node-list)))
;    (if (null? other)
;        next
;        (let ((first (car other))
;              (rest (loop (cdr other) next)))
;          (cond
;           ((pair? first) (loop first (loop (cdr other) next)))
;           ((node-list-empty? rest) other)
;           ((eq? rest (cdr other)) other)
;           (else (cons first rest)))))))

(define (node-list . other) other)

;; %node-list-cons is for optimization only.  Not to be exported!

;; (define -macro (%node-list-cons snl nl) `(cons ,snl ,nl))
;; (define -macro (%node-list-append . nl) `(append . ,nl))

(define (node-list-first nl)
  (let loop ((nl nl))
    (cond
     ((null? nl) (empty-node-list))
     ((pair? nl) (let ((t (loop (car nl))))
		   (if (null? t) (loop (cdr nl)) t)))
     ((promise? nl) (loop (force nl)))
     (else nl))))

(define (node-list-empty? nl)
  (let loop ((nl nl))
    (or (null? nl)
	(and (pair? nl)
	     (loop (car nl)) (loop (cdr nl))))))

(define (node-list-rest nl)
  (let loop ((nl nl) (next (empty-node-list)))
    (cond
     ((pair? nl)
      (loop (car nl)
	    (or (and (node-list-empty? next) (cdr nl))
		(and (null? (cdr nl)) next)
		(cons (cdr nl) next))))
     ((and (null? nl) (pair? next)) (loop next (empty-node-list)))
     ((promise? nl) (loop (force nl) next))
     (else next))))

;; data value of a node-list, belongs to make-literal
;; Originally defined by DSSSL (reference missing).
;; See also sxml:string-value [sxp:string-value].

(define (data nl)                       ; EXPORT
  (let recurse ((nl nl))
    (cond
     ((xml-element? nl) (recurse (%children nl)))
     ((xml-literal? nl) (xml-literal-value nl))
     ((xml-attribute? nl) (recurse (xml-attribute-value nl)))
     ((xml-document? nl) (xml-document-children nl))
     ((and (pair? nl) (null? (cdr nl))) (recurse (car nl)))
     (else
      (let loop ((n nl) (l 0))
	(if (node-list-empty? n)
	    (let ((result (make-string/uninit l)))
	      (let loop ((n nl) (i 0))
		(if (node-list-empty? n)
		    result
		    (let ((x (node-list-first n)))
		      (cond
		       ((xml-literal? x)
			(loop (node-list-rest n)
			      (string-splice!
			       result i (xml-literal-value x) 0
			       (lambda (buffer i str j cont) i))))
		       ((xml-element? (node-list-first n))
			(loop (node-list (%children x) (node-list-rest n))
			      i))
		       ((xml-attribute? (node-list-first n))
			(loop (node-list (xml-attribute-value x) (node-list-rest n))
			      i))
		       ;; ((or (number? x))
		       ;;  (loop (node-list-rest n)
		       ;; 	    (string-splice!
		       ;; 	     result i (literal x) 0
		       ;; 	     (lambda (buffer i str j cont) i))))
		       (else (loop (node-list-rest n) i)))))))
	    (let ((x (node-list-first n)))
	      (cond
	       ((xml-literal? x)
		(loop (node-list-rest n)
		      (fx+ l (string-length (xml-literal-value x)))))
	       ((xml-element? x)
		(loop (node-list (%children x) (node-list-rest n))
		      l))
	       ((xml-attribute? x)
		(loop (node-list-rest n)
		      (fx+ l (string-length (xml-attribute-value x)))))
	       ;; ((or (number? x))
	       ;;  (loop (node-list-rest n)
	       ;; 	    (fx+ l (string-length (literal x)))))
	       (else (loop (node-list-rest n) l))))))))))

(define (first-child-gi osnl)		; 10.2.4.1
  (let loop ((c (children osnl)))
    (let ((c1 (node-list-first c)))
      (cond
       ((null? c1) #f)
       ((xml-element? c1) (gi c1))
       (else (loop (node-list-rest c)))))))

;; see above

;(define (node-list-map proc nl)
;  (cond
;   ((pair? nl) (map proc nl))
;   ((node-list-empty? nl) (empty-node-list))
;   (else (proc nl))))

(define (node-list-map proc nl)       ; 10.2.2
  (or (procedure? proc) (error "node-list-map: procedure not a procedure"))
  (cond
   ((node-list-empty? nl) (empty-node-list))
   ((pair? nl)
    (let* ((orig (node-list-first nl))
           (first (proc orig)))
      (cond
       ((or (node-list-empty? first) (not first))
        (node-list-map proc (node-list-rest nl)))
       ((eqv? orig first)
        (let* ((orig-rest (node-list-rest nl))
               (rest (node-list-map proc orig-rest)))
          (if (eq? orig-rest rest) nl (cons first rest))))
       (else (%node-list-cons first
                              (node-list-map proc (node-list-rest nl)))))))
   (else (proc nl))))

(define (node-list-map-parallel abort-handler exception-handler n tmo f args)
  (or (procedure? f) (error "node-list-map: predicate not a procedure"))
  (cond
   ((node-list-empty? args) (empty-node-list))
   ;; TODO: Add a case to handle singleton node lists without timeout
   ;; without expending a fresh thread and all the overhead.
   (else
    ((!map node-list-map abort-handler exception-handler n tmo f
	   (if (pair? args) args (list args)))
     fold-right: cons (empty-node-list)))))

(define (node-list-filter pred nl)
  (or (procedure? pred) (error "node-list-filter: predicate not a procedure"))
  (let loop ((nl nl) (next (empty-node-list)))
    (if (node-list-empty? nl)
        next
        (let ((f (node-list-first nl)))
          (cond
           ((null? f) (loop (node-list-rest nl) next))
           ((pred f) (%node-list-cons f (loop (node-list-rest nl) next)))
           (else (loop (node-list-rest nl) next)))))))

(define (node-list-reduce nl combine init)
  (or (procedure? combine) (error "node-list-reduce: combinator not a procedure"))
  (let loop ((nl nl) (result init))
    (if (node-list-empty? nl)
        result
        (loop (node-list-rest nl) (combine result (node-list-first nl))))))

;; SRFI 44 ( http://srfi.schemers.org/srfi-44/srfi-44.html ) defines
;; some nameing scheme for collection fold functions and their
;; application.  This code has been written in a simillar style,
;; though one difference: I used to pass a function down to the
;; fold-function, which would continue the iteration.  I feel this is
;; slightly more flexible, while I'd prefer to force the use of
;; keyword style return macros to handle those seed values, which are
;; not intesting anyway (quite a common situation in the xslt
;; implementation).  I did this by intentionally
;; permutating/abstracting the seed value types (knowing that this
;; would come at a slightly performace penealty and iterested to find
;; out how much that would be).  I will probably change towards srfi44
;; style over time.  Expect code in both styles ahead.

(define-syntax define-fold-left*
  (syntax-rules ()
    ((define-fold-left* % empty? head tail seed ...)
     (define (% collection fold-function seed ...)
       (let loop ((collection collection)
		  (seed seed)
		  ...)
	 (if (empty? collection)
	     (values seed ...)
	     (call-with-values
		 (lambda ()
		   (fold-function (head collection) seed ...))
	       (lambda (proceed seed ...)
		 (if proceed
		     (loop (tail collection) seed ...)
		     (values seed ...))))))))))

(define (node-list-fold-left1 nl fold-function seed)
  (let loop ((nl nl) (seed seed))
    (if (node-list-empty? nl)
        seed
        (receive (proceed seed)
                 (fold-function (node-list-first nl) seed)
                 (if proceed
                     (loop (node-list-rest nl) seed)
                     seed)))))

(define (node-list*-fold-left nl fold-function seeds)
  (let loop ((nl nl) (seeds seeds))
    (if (node-list-empty? nl)
        (apply values seeds)
        (receive
         (proceed . seeds)
         (apply fold-function (node-list-first nl) seeds)
         (if proceed
             (loop (node-list-rest nl) seeds)
             (apply values seeds))))))

(define (node-list-fold-left nl fold-function seed . seeds)
  (if (null? seeds)
      (node-list-fold-left1 nl fold-function seed)
      (node-list*-fold-left nl fold-function (cons seed seeds))))

(define (node-list-length nl) (node-list-reduce nl (lambda (n x) (add1 n)) 0))

(define (document-element lst)
  (let loop ((lst (if (xml-document? lst) (xml-document-children lst) lst)))
    (cond
     ((xml-element? (node-list-first lst)) (node-list-first lst))
     ((node-list-empty? lst) #f)
     (else (loop (node-list-rest lst))))))

(define (children nl)                   ; 10.2.3
  (cond
   ((xml-element? nl) (%children nl))
   ((pair? nl)                          ; BEWARE depends on implementation
    (let loop ((nl nl))
      (let ((f (node-list-first nl)))
	(cond
	 ((node-list-empty? f) (empty-node-list))
	 ((and (xml-element? f)
	       (not (node-list-empty? (%children f))))
	  (let ((rest (node-list-rest nl)))
	    (if (node-list-empty? rest)
		(%children f)
		(node-list (%children f) (loop rest)))))
	 ((xml-attribute? f)
	  (node-list (xml-attribute-value f) (loop (node-list-rest nl))))
	 (else (loop (node-list-rest nl)))))))
   ((or
     (node-list-empty? nl)
     (xml-literal? nl)
     (xml-comment? nl)
     (xml-pi? nl)
     (xml-document? nl))
    (empty-node-list))
   (else (error "children: not a node list ~a" nl))))

(define (node-list-tail nl i)
  (let loop ((nl nl) (i i))
    (if (or (eqv? i 0) (node-list-empty? nl))
        nl
        (loop (node-list-rest nl) (sub1 i)))))

(define (node-list-head nl i)
  (let loop ((nl nl) (i i))
    (if (or (eqv? i 0) (node-list-empty? nl))
        (empty-node-list)
        (%node-list-cons (node-list-first nl)
                         (loop (node-list-rest nl) (sub1 i))))))

;; See srfi-43
(define (node-list->vector lst . maybe-start+end)
  (define (nonneg-int? x)
    (and (integer? x)
	 (not (negative? x))))
  (define (check-type pred? value callee)
    (if (pred? value)
	value
	;; Recur: when (or if) the user gets a debugger prompt, he can
	;; proceed where the call to ERROR was with the correct value.
	(check-type pred?
		    (error (format #f "node-list->vector: erroneous value ~a ~a" pred? value))
		    callee)))
  ;; Checking the type of a proper list is expensive, so we do it
  ;; amortizedly, or let %LIST->VECTOR or LIST-TAIL do it.
  (let ((start (or (and (pair? maybe-start+end) (car maybe-start+end)) 0))
	(end (or (and (pair? maybe-start+end)
		      (pair? (cdr maybe-start+end))
		      (car maybe-start+end))
		 (node-list-length lst))))
    (let ((start (check-type nonneg-int? start 'node-list->vector))
	  (end   (check-type nonneg-int? end   'node-list->vector)))
      ((lambda (f)
	 (vector-unfold f (- end start) (node-list-tail lst start)))
       (lambda (index l)
	 (cond ((null? l)
		(error
		 (format #f
			 "node-list->vector: list ~a was too short attempted end was ~a"
			 lst end)))
	       ((pair? l)
		(values (node-list-first l) (node-list-rest l)))
	       (else
		;; Make this look as much like what CHECK-TYPE
		;; would report as possible.
		(error
		 (format #f "node-list->vector: erroneous value not a list ~a" lst)))))))))

(: select-elements (:xml-nodelist: symbol --> :xml-nodelist:))
(define (select-elements nl my-gi)
  (if (pair? nl)
      (node-list-filter
       (lambda (e) (eq? my-gi (gi e))) nl)
      (if (eq? my-gi (gi nl))
	  nl
	  (empty-node-list))))

(: dsssl-select-elements (:xml-nodelist: * --> :xml-nodelist:))
(define (dsssl-select-elements nl nm)
  (select-elements nl (cond
		       ((string? nm) (string->symbol nm))
		       ((symbol? nm) nm)
		       (else (error "select-elements: ~a neither string nor symbol" nm)))))

;; select-elements-ns is not DSSSL.

(: select-elements-ns (:xml-nodelist:
		       (or false string symbol)
		       (or false string symbol)
		       --> :xml-nodelist:))
(define (select-elements-ns nl nm nmsp)
  (let ((my-gi (cond
		((symbol? nm) nm)
		((string? nm) (string->symbol nm))
		((not nm) #f)
		(else (error "select-elements-ns: name ~a neither string, symbol nor #f"
                              nm))))
	(my-ns (cond
		((symbol? nmsp) nmsp)
		((boolean? nmsp) nmsp)
		((string? nmsp) (string->symbol nmsp))
		(else (error "select-elements-ns: namespace ~a neither string nor symbol"
			     nmsp)))))
    (if (and (eq? my-ns #t) (not my-gi))
	nl
	(let ((test (if my-gi
			(if (eq? my-ns #t)
			    (lambda (e) (eq? (gi e) my-gi))
			    (lambda (e) (and (eq? my-ns (ns e)) (eq? my-gi (gi e)))))
			(lambda (e) (eq? my-ns (ns e))))))
	  (if (pair? nl)
	      (node-list-filter test nl)
	      (if (test nl) nl (empty-node-list)))))))

;; I hesitated for over a year to implement this stupid test.  Who can
;; even want such a deep equality test?

(: node-list=? (:xml-nodelist: :xml-nodelist: --> boolean))
(define (node-list=? nl1 nl2)

  (cond-expand
   (chicken (chicken-check-interrupts!))
   (else #f))

  (let loop ((nl1 nl1) (nl2 nl2))
    (if (or (node-list-empty? nl1) (node-list-empty? nl2))
        (and (node-list-empty? nl1) (node-list-empty? nl2))
        (and
         (let ((a (node-list-first nl1))
               (b (node-list-first nl2)))
           (cond
            ((xml-element? a)
             (and (xml-element? b)
                  (eq? (gi a) (gi b)) (eq? (xml-element-ns a) (ns b))
                  (node-list=? (xml-element-attributes a)
                               (xml-element-attributes b))
                  (node-list=? (%children a) (%children b))))
            ((xml-attribute? a)
             (and (xml-attribute? b)
                  (eq? (xml-attribute-name a) (xml-attribute-name b))
                  (eq? (xml-attribute-ns a) (xml-attribute-ns b))
                  (equal? (xml-attribute-value a)
                          (xml-attribute-value b))))
            ((or (xml-literal? a)
                 (xml-pi? a)
                 (xml-comment? a))
             (equal? a b))
            (else #f)))
         (loop (node-list-rest nl1) (node-list-rest nl2))))))

(define (node-list-contains? nl snl)
  (let loop ((nl nl))
    (and (not (node-list-empty? nl))
         (or (node-list=? (node-list-first nl) snl)
             (loop (node-list-rest nl))))))

(define (node-list-difference-with-duplicates . args)
  (cond
   ((null? args) (empty-node-list))
   ((null? (cdr args)) (car args))
   (else
    (let loop ((nls (cdr args)) (remove (empty-node-list)))
      (if (null? nls)
          (node-list-filter
           (lambda (n) (not (node-list-contains? remove n)))
           (car args))
          (let collect ((nl (car nls)) (remove remove))
            (if (node-list-empty? nl)
                (loop (cdr nls) remove)
                (if (node-list-contains? remove (node-list-first nl))
                    (collect (node-list-rest nl) remove)
                    (collect (node-list-rest nl)
                             (%node-list-cons (node-list-first nl)
                                              remove))))))))))


;;** Walking the tree

;; The tree transformation code is probably not as easy to read as the
;; one at
;; http://cvs.sf.net/cgi-bin/viewcvs.cgi/ssax/SSAX/lib/SXML-tree-trans.scm
;; is.  TODO, make it so.  Excuses:-): a) it predates SSAX, b) it was
;; supposed to be more flexibel (we'll maybe) c) it flattens the
;; result list ASAP as DSSSL requires d) it includes Askemos specific
;; parameters.

;;*** Transformers

;; These "transformers" are kind of a monadic abstraction, though I
;; did not hear of monads, before I wrote it; hence the different
;; naming.  The whole tree traversal is also driven by the
;; understanding, that parsing and tree traversal are essentially the
;; same thing.  TODO this code here should be cleaned up.  The idea
;; behind the transformers absolutely identical with the nice paper:
;; http://www.cs.indiana.edu/~jsobel/Parsing/explicit.html though it
;; includes many (not all) of the closures in the expanded code.  We
;; should follow the paper and expand without all the closures.

;; A transformer implements the handler which converts a node-list
;; into a result node-list.  (The equivalent in the pre-post-order
;; function of SSAX (see above) is the last element of a binding.)
;; Transformers are invoked, when a node test was triggered on the
;; current node (the `node-list-first' of the `tree' parameter).

;; All the transformer specs share a common signature.  We call this
;; signature a virtual node because its arguments are mostly the axes
;; XSL defines as parts of a node.  See, a DOM implementation would
;; suggest to build a heap data structure instead.
;;
;; From the user point of view: within a xml-transformer are the names
;; in the list bond to the values accociated with the current node.

;; TODO fix ancestor handling and add handling for other axes.

;; Walk the "tree" in the with context node "self-node".  "chain" is a
;; list of (predicate . transformation) pairs.  The transformation is
;; applied if the predicate is satisfied.  It's result replace the
;; current node of "tree" in the result tree.

(cond-expand

 ;; Copied and manually optimized from the expanded code.
 (rscheme

  (define (xml-walk*1 place
		      _message_50
		      _root-node0_51
		      _variables_52
		      _namespaces0_53
		      _ancestors0_54
		      _self-node_55
		      _nl_56
		      _mode-choice0_57
		      _proc-chain_58)
    
    (let _cl_100 ((_nl_101 _nl_56) (_chain_102 _proc-chain_58))
      (if (null? _nl_101)
	  _nl_101
	  (if (pair? _nl_101)
	      (let ((_first_103 (_cl_100 (car _nl_101) _chain_102)))
		(if (let ((_x_104 (not _first_103)))
		      (if _x_104 _x_104 (null? _first_103)))
		    (_cl_100 (cdr _nl_101) _chain_102)
		    (cons _first_103 (_cl_100 (cdr _nl_101) _chain_102))))
	      (if (null? _chain_102)
		  _nl_101
		  (if ((caar _chain_102) _nl_101 _root-node0_51 _variables_52)
		      ((cdar _chain_102)
		       place
		       _message_50
		       _root-node0_51
		       _variables_52
		       _namespaces0_53
		       _ancestors0_54
		       _self-node_55
		       _nl_101
		       _mode-choice0_57
		       (lambda (_place_50
				_message_51
				_root-node_52
				_variables_53
				_namespaces_54
				_ancestors_55
				_self-node_56
				_nl_57
				_mode-choice_58)
			 (xml-walk*1 _place_50
				     _message_51
				     _root-node_52
				     _variables_53
				     _namespaces_54
				     _ancestors_55
				     _self-node_56
				     _nl_57
				     _mode-choice_58
				     _proc-chain_58)))
		      (_cl_100 _nl_101 (cdr _chain_102)))))))))

 (else

  (define xml-walk*1-loop
    (lambda-process "xml-walk*1.1" CASE1 (x)
		    (apply-transformer use: xml-walk*1 process: x)))

  (define-transformer xml-walk*1 "xml-walk*1"
    (let cl ((nl (sosofo)) (chain (%%proc-chain)))
      (cond
       ((null? nl) nl)
       ((pair? nl)
	(let ((first (cl (car nl) chain)))
	  (cond
	   ;;((pair? first) (append first (cl (cdr nl) chain)))
	   ((or (not first) (null? first)) (cl (cdr nl) chain))
	   (else (cons first (cl (cdr nl) chain))))))
       ((null? chain) nl)
       (else
	(if ((caar chain) nl (root-node) (environment))
	    (apply-transformer
	     use: (cdar chain) sosofos: nl
	     process: (xml-walk*1-loop (%%proc-chain)))
	    (cl nl (cdr chain)))))))))


;; backward compatibility definition

#;(define (xml-walk*
         place message                  ; access control
	 root-node
         variables namespaces ancestor self-node tree ; virtual node
         mode-choice)
  ;; The chain parameter is list of pairs (node-test . transformer),
  ;; when the node-test is true for a 'self-node' the transformer's
  ;; result replaces the node in the result tree.
  (lambda (chain)                       ; process chain
    (xml-walk*1 place message root-node variables namespaces
                ancestor self-node tree mode-choice chain)))

;; The chain parameter is list of pairs (node-test . transformer),
;; when the node-test is true for a 'self-node' the transformer's
;; result replaces the node in the result tree.
(define xml-walk*
  (lambda-process "xml-walk*" () (apply-transformer use: xml-walk*1)))

(define (xml-walk
         place message                  ; access control
	 root-node
         variables namespaces ancestor self-node tree ; virtual node
         )
  (lambda chain
    (xml-walk*1 place message root-node variables namespaces
                ancestor self-node tree
                (lambda (mode)
                  (if mode
                      (error "toplevel transform may not use modes")
                      chain))
                chain)))

(define-fold-left*
  xml-tree-fold-left
  node-list-empty? node-list-first node-list-rest

  place                                 ; only for Askemos
  message                               ; only for Askemos
  variables                             ; xsl variables
  namespaces                            ; namespace bindings
  ancestor                              ; context
  self-node                             ; self
  nl                                    ; style sheet etc.
  mode-choice                           ; 1-ari proc, selects
					; continuation mode, #f
					; selects default mode, symbol
					; selects named mode, string
					; selects named template
  proc-chain                            ; next processing instruction
  )


(define (xml-apply-template-once chain)
  (lambda (nl place message root-node
              variables namespaces ancestor self-node mode-choice proc-chain)
    (let loop ((chain chain))
      (if (null? chain)
          (values proc-chain nl)
          (if ((caar chain) nl root-node namespaces)
              ((cdar chain) nl place message root-node
               variables namespaces ancestor
               self-node mode-choice proc-chain)
              (loop (cdr chain)))))))

(define (xml-walk1 place message root-node variables namespaces ancestor self-node tree)
  (lambda chain
    (let* ((ff (xml-apply-template-once chain))
           (recurse (lambda (mode) ff)))
      (xml-tree-fold-left
       tree ff place message root-node variables namespaces ancestor self-node tree recurse))))

;; Handy stuff

(define-transformer xml-drop "xml:drop-node" (empty-node-list))
(define-transformer xml-copy "xml:copy" (sosofo))

(define (dsssl-read str)
  (cond
   ((input-port? str) (sweet-read str))
   ((string? str) (call-with-input-string str sweet-read))
   (else (call-with-input-string (data str) sweet-read))))

(define (symbol<? a b)
  (string<? (symbol->string a) (symbol->string b)))

#|
(define (monitor-state what)
  (make-xml-element
   'table namespace-html '()
   (map
    (lambda (key)
      (make-xml-element
       'tr namespace-html '()
       (node-list
        (make-xml-element
         'td namespace-html '()
         (literal (or (hash-table-ref *monitor-labels* key) key)))
        (make-xml-element
         'td namespace-html '()
         (literal (hash-table-ref *monitored-values* key))))))
    (or what (sort (hash-table-keys *monitored-values*) symbol<?)))))
|#

;;* xpath evaluators

;; runtime functions to support xpath evaluation.

(define (node-constant constant) (lambda (node root ns-binding) constant))

(define (string-value input)
  (if (string? input) input
   ((if (xml-attribute? input) xml-attribute-value data) input)))

(define (number-value input)
  (if (number? input) input
      (string->number (string-value input))))

(define (boolean-value obj)
  (or (and (boolean? obj) obj)
      (and (number? obj) (and (not (eqv? obj 0))
                              ;; FIXME NaN handling missing
                              ))
      (and (nodeset? obj) (not (node-list-empty? obj)))
      (and (string? obj) (not (eqv? (string-length obj) 0)))))

(define (node-number-op func left-expr right-expr)
  (lambda (node root ns-binding)
    (func (number-value (left-expr node root ns-binding))
	  (number-value (right-expr node root ns-binding)))))

;; --------------------------------------------------------------------------
;                                 SXML

(define sxml-predifined-entities
  `(("nbsp" . ,(literal (integer->char 160)))
    ("gt" . ">")
    ("lt" . "<")
    ("amp" . "&")))

(define (sxml-attributes element)
  (or (and (pair? element) (symbol? (car element))
           (pair? (cdr element)) (pair? (cadr element))
           (eq? (caadr element) '@)
           (cdadr element))
      '()))

(define (sxml-namespaces-annotations e i)
  (if (and (pair? e) (eq? (car e) '*NAMESPACES*))
      (let loop ((decls (cdr e)))
        (if (null? decls)
            i
            (cons
             (if (null? (car decls))
                 (error "sxml namespace declaration invalid in ~a" e)
                 (if (and (pair? (car decls)) (pair? (cdar decls)))
                     (if (pair? (cddar decls))
                         (cons (symbol->string (caddar decls)) (caar decls))
                         (cons (symbol->string (caar decls))
                               (string->symbol (cadar decls))))
                     (error "sxml namespace declaration invalid in ~a" e)))
             (loop (cdr decls)))))
      i))

(define (sxml-find-namespaces-annotations e i)
  (if (and (pair? e) (eq? (car e) '@))
      (fold sxml-namespaces-annotations i (cdr e))
      i))

(define shtml-named-char-id   "additional")
(define shtml-numeric-char-id "additional-char")

(define (sxml body)
  (let ((root (let loop ((nl body))
                   (and (pair? nl)
                        (if (and (pair? (car nl))
                                 (symbol? (caar nl))
                                 (not (memq (caar nl) '(@ *PI* *COMMENT*))))
                            (car nl)
			    (loop (cdr nl)))))))
    (let loop ((nl (if (pair? body)
                       (if (pair? (car body))
                           body
                           (if (eq? (car body) '*TOP*)
                               (cdr body)
                               (list body)))
                       body))
               (nsdecls (fold
			 sxml-find-namespaces-annotations
			 (fold sxml-find-namespaces-annotations '() body)
			 (sxml-attributes root))))
      (if (pair? nl)
           ;; Allow ordinary xml elements too...
	   ;;
	   ;; BEWARE: left here for backward compatibility. 2011-07-05
	  (if (pair? (car nl))
	      (let ((key (caar nl)))
		(cond
		 ((or (eq? key '$$) (eq? key '$))
		  (let* ((args (cdar nl))
			 (arg1 (car args))
			 (v (if (procedure? arg1) (apply arg1 (cdr args)) args)))
		    (if (node-list-empty? v)
			(loop (cdr nl) nsdecls)
			(node-list v (loop (cdr nl) nsdecls)))))
		 ((eq? key '*COMMENT*)
		  (%node-list-cons (apply make-xml-comment (cdar nl))
				   (loop (cdr nl) nsdecls)))
		 ((eq? key '*PI*)
		  (%node-list-cons (apply make-xml-pi (cadar nl) (cddar nl))
				   (loop (cdr nl) nsdecls)))
		 ((eq? key '@) (loop (cdr nl) nsdecls))
		 ((eq? key '*ENTITY*)
		  (%node-list-cons
		   (let ((entity (car nl)))
		     (if (and (pair? entity)
			      (= (length entity) 3))
			 (let ((public-id (list-ref entity 1))
			       (system-id (list-ref entity 2)))
			   (cond
			    ((equal? public-id shtml-named-char-id)
			     (or (and-let* ((predef
					     (assoc system-id
						    sxml-predifined-entities)))
					   (cdr predef))
				 system-id))
			    ((equal? public-id shtml-numeric-char-id)
			     (literal
			      (integer->char (string->number system-id))))
			    (else (error "invalid entity public id ~a" public-id))))
			 (error "shtml-entity-value not an entity ~a" entity)))
		   (loop (cdr nl) nsdecls)))
		 ((eq? key '*DECL*)
		  (%node-list-cons (make-xml-doctype (cdar nl))
				   (loop (cdr nl) nsdecls)))
		 (else
		  (let* ((node (car nl))
			 (gistr (symbol->string (car node)))
			 (colon (string-index gistr #\:))
			 (atts (sxml-attributes node))
			 (nsdecls (fold sxml-find-namespaces-annotations
					nsdecls atts)))
		    (%node-list-cons
		     (make-specialised-xml-element
		      (if colon
			  (string->symbol
			   (substring gistr (add1 colon) (string-length gistr)))
			  (string->symbol gistr))
		      (and colon
			   (let ((nmsp (substring gistr 0 colon)))
			     (or (and-let* ((mapping (assoc nmsp nsdecls)))
					   (cdr mapping))

				 (string->symbol nmsp))))
		      (let loop ((atts atts))
			(if (null? atts)
			    '()
			    (let ((att (car atts)))
			      (if (eq? (car att) '@)
				  (let ((nmsps
					 (if (eq? node root)
					     (append
					      (or
					       (and-let* ((ann (assq '@ (cdr body)))
							  (nmsp (assq '*NAMESPACES*
								      (cdr ann))))
							 (cdr nmsp))
					       '())
					      (or
					       (assq '*NAMESPACES* (cdr att))
					       '()))
					     (assq '*NAMESPACES* (cdr att)))))
				    (if nmsps
					(let l2 ((nmsps (cdr nmsps)))
					  (if (null? nmsps)
					      (loop (cdr atts))
					      (cons
					       (make-xml-attribute
						(or (and (pair? (cddar nmsps))
							 (caddar nmsps))
						    (caar nmsps))
						'xmlns (cadar nmsps))
					       (l2 (cdr nmsps)))))
					(loop (cdr atts))))
				  (let* ((nmstr (symbol->string (car att)))
					 (colon (string-index nmstr #\:)))
				    (cons
				     (make-xml-attribute
				      (if colon
					  (string->symbol
					   (substring nmstr (add1 colon)
						      (string-length nmstr)))
					  (car att))
				      (and colon
					   (let ((nmsp (substring nmstr 0 colon)))
					     (or (and-let* ((mapping
							     (assoc nmsp nsdecls)))
							   (cdr mapping))
						 (string->symbol nmsp))))
				      (if (any pair? (cdr att))
					  (apply-string-append
					   (map
					    (lambda (e)
					      (if (and (pair? e) (or (eq? (car e) '$$) (eq? (car e) '$))) 
						  (let* ((args (cdr e))
							 (arg1 (car args))
							 (v (if (procedure? arg1)
								(apply arg1 (cdr args))
								args)))
						    (if (node-list-empty? v) ""
							(data v)))
						  e))
					    (cdr att)))
					  (apply-string-append (cdr att))))
				     (loop (cdr atts))))))))
		      (loop ((if (pair? atts) cddr cdr) node) nsdecls))
		     (loop (cdr nl) nsdecls))))))
	      (let ((x (car nl)))
		(%node-list-cons 
		 (if (or (string? x) (xml-element? x)
			 (xml-comment? x) (xml-pi? x))
		     x (literal x))
		 (loop (cdr nl) nsdecls))))
	  nl))))

(define (node-list->list nl)		; DEPRECIATED
  (if (pair? nl)
      (if (any pair? nl)
	  (let loop ((nl nl) (r '()))
	    (cond
	     ((null? nl) r)
	     ((pair? nl)
	      (loop (car nl) (loop (cdr nl) r)))
	     (else (cons nl r))))
	  nl)
      (list nl)))

;; (display (xml-format
;;           (sxml
;;            '(*TOP* (*COMMENT* "quatsch") (*PI* haha "gaga")
;;                    (root (@ (att "atva")
;;                             (@ (*NAMESPACES* (ns:url "ns:url" prefix))))
;;                      (html (head (title "testpage"))
;;                            (body (@ (bgcolor "white")))
;;                            (prefix:p "Ein Test")))))))

;; --------------------------------------------------------------------------

;			XML processing in Scheme
;		     SXPath -- SXML Query Language
;
;  http://web.archive.org/web/20070207182039/http://www196.pair.com/lisovsky/query/sxpath/
;
; SXPath is a query language for SXML, an instance of XML Information
; set (Infoset) in the form of s-expressions. See SSAX.scm for the
; definition of SXML and more details. SXPath is also a translation into
; Scheme of an XML Path Language, XPath:
;	http://www.w3.org/TR/xpath
; XPath and SXPath describe means of selecting a set of Infoset's items
; or their properties.
;
; To facilitate queries, XPath maps the XML Infoset into an explicit
; tree, and introduces important notions of a location path and a
; current, context node. A location path denotes a selection of a set of
; nodes relative to a context node. Any XPath tree has a distinguished,
; root node -- which serves as the context node for absolute location
; paths. Location path is recursively defined as a location step joined
; with a location path. A location step is a simple query of the
; database relative to a context node. A step may include expressions
; that further filter the selected set. Each node in the resulting set
; is used as a context node for the adjoining location path. The result
; of the step is a union of the sets returned by the latter location
; paths.
;
; The SXML representation of the XML Infoset (see SSAX.scm) is rather
; suitable for querying as it is. Bowing to the XPath specification,
; we will refer to SXML information items as 'Nodes':
; 	<Node> ::= <Element> | <attributes-coll> | <attrib>
; 		   | "text string" | <PI>
; This production can also be described as
;	<Node> ::= (name . <Nodeset>) | "text string"
; An (ordered) set of nodes is just a list of the constituent nodes:
; 	<Nodeset> ::= (<Node> ...)
; Nodesets, and Nodes other than text strings are both lists. A
; <Nodeset> however is either an empty list, or a list whose head is not
; a symbol.  A symbol at the head of a node is either an XML name (in
; which case it's a tag of an XML element), or an administrative name
; such as '@'.  This uniform list representation makes processing rather
; simple and elegant, while avoiding confusion. The multi-branch tree
; structure formed by the mutually-recursive datatypes <Node> and
; <Nodeset> lends itself well to processing by functional languages.
;
; A location path is in fact a composite query over an XPath tree or
; its branch. A singe step is a combination of a projection, selection
; or a transitive closure. Multiple steps are combined via join and
; union operations. This insight allows us to _elegantly_ implement
; XPath as a sequence of projection and filtering primitives --
; converters -- joined by _combinators_. Each converter takes a node
; and returns a nodeset which is the result of the corresponding query
; relative to that node. A converter can also be called on a set of
; nodes. In that case it returns a union of the corresponding queries over
; each node in the set. The union is easily implemented as a list
; append operation as all nodes in a SXML tree are considered
; distinct, by XPath conventions. We also preserve the order of the
; members in the union. Query combinators are high-order functions:
; they take converter(s) (which is a Node|Nodeset -> Nodeset function)
; and compose or otherwise combine them. We will be concerned with
; only relative location paths [XPath]: an absolute location path is a
; relative path applied to the root node.
;
; Similarly to XPath, SXPath defines full and abbreviated notations
; for location paths. In both cases, the abbreviated notation can be
; mechanically expanded into the full form by simple rewriting
; rules. In case of SXPath the corresponding rules are given as
; comments to a sxpath function, below. The regression test suite at
; the end of this file shows a representative sample of SXPaths in
; both notations, juxtaposed with the corresponding XPath
; expressions. Most of the samples are borrowed literally from the
; XPath specification, while the others are adjusted for our running
; example, tree1.
;
; Id: SXPath.scm,v 3.5 2001/01/12 23:20:35 oleg Exp oleg


	; See http://pobox.com/~oleg/ftp/Scheme/myenv.scm
	; See http://pobox.com/~oleg/ftp/Scheme/myenv-scm.scm
	; See http://pobox.com/~oleg/ftp/Scheme/myenv-bigloo.scm
;(module SXPath
;  (include "myenv-bigloo.scm"))		; For use with Bigloo 2.2b
;(load "myenv-scm.scm")		; For use with SCM v5d2
;(include "myenv.scm")		; For use with Gambit-C 3.0



; $Id: tree.scm,v 1.27 2006/12/28 13:41:00 realjerry Exp $
; 
; Differences from the original sxpath:
;
; 1. Criterion '*' doesn't accept COMMENT, ENTITY and NAMESPACES nodes
;  any more
;
; 2. Criterion *data* introduced in node-typeof? function.
;
; 3. Function node-nameof? introduced. It takes a list of node names, and
; returns a predicate Node -> Boolean
; A node is accepted if its name is in the list of names given.
;
; 4. node-prefix-of? introduced.  It takes a namespace prefix, and
; returns a predicate Node -> Boolean, which is #t for nodes with this
; very namespace prefix. 
;
; 5. sxpath has an additional rewriting rule for 'or@':
; (sxpath1 '(or@ ...))  -> (select-kids (node-nameof?
;                                          (cdr '(or@ ...))))
;
; 6. Function "filter" is renamed as "sxp:filter" for the sake of
; compatibility with SRFI-1
;
; 7. Function pretty-print is replaced by pp
;
; 8. Function "sxp:invert" introduced.
;
; 9. sxpath has an additional rewriting rule for 'not@' (EXPERIMENTAL!):
; (sxpath1 '(not@ ...)) -> (select-kids (sxp:invert 
;                                         (node-nameof?
;                                          (cdr '(not@ ...)))))
;
; 10. sxpath has an additional rewriting rule for 'prefix:*' (EXPERIMENTAL!):
; (sxpath1 '(prefix:* x)) -> (select-kids 
;                                      (node-prefix-of? x))
;
;         Kirill Lisovsky 
;         lisovsky@acm.org
;

;(define (nodeset? x)
;  (or (and (pair? x) (not (symbol? (car x)))) (null? x)))

;-------------------------
; Basic converters and applicators
; A converter is a function
;	type Converter = Node|Nodeset -> Nodeset
; A converter can also play a role of a predicate: in that case, if a
; converter, applied to a node or a nodeset, yields a non-empty
; nodeset, the converter-predicate is deemed satisfied. Throughout
; this file a nil nodeset is equivalent to #f in denoting a failure.

; The following function implements a 'Node test' as defined in
; Sec. 2.3 of XPath document. A node test is one of the components of a
; location step. It is also a converter-predicate in SXPath.
;

; The function node-nameof? takes a list of acceptable node names as a
; criterion and returns a function, which, when applied to a node, 
; will return #t if the node name is present in criterion list and #f
; othervise.
;	node-nameof? :: ListOfNames -> Node -> Boolean
(define (ntype-names? crit)
  (lambda(node root-node ns-binding)
    (and (memq (or (and (xml-element? node) (gi node))
                   (and (xml-attribute? node) (xml-attribute-name node))
		   (and (xml-namespace? node) (xml-namespace-local node)))
               crit) #t)))

(define node-nameof? ntype-names?)

; The function node-typeof? takes a type criterion and returns a function,
; which, when applied to a node, will tell if the node satisfies
; the test.
;	node-typeof? :: Crit -> Node -> Boolean
;
; The criterion 'crit' is a symbol, one of the following:
;	id		- tests if the Node has the right name (id)
;	@		- tests if the Node is an <attributes-coll>
;	*		- tests if the Node is an <Element>
;	*text*		- tests if the Node is a text node
;	*PI*		- tests if the Node is a PI node
;	*any*		- #t for any type of Node

;(define (node-typeof? crit)
;  (lambda (node)
;    (case crit
;      ((*) (and (pair? node) (not (memq (car node) '(@ *PI*)))))
;      ((*any*) #t)
;      ((*text*) (string? node))
;      (else
;       (and (pair? node) (eq? crit (car node))))
;)))

(: sxp:element? :sxpath-pred:)
(define (sxp:element? node root ns-binding) (xml-element? node))
(: sxp:literal? :sxpath-pred:)
(define (sxp:literal? node root ns-binding) (xml-literal? node))
(: sxp:pi? :sxpath-pred:)
(define (sxp:pi? node root ns-binding) (xml-pi? node))
(: sxp:comment? :sxpath-pred:)
(define (sxp:comment? node root ns-binding) (xml-comment? node))
(: sxp:true :sxpath-pred:)
(define (sxp:true node root ns-binding) #t)

(: node-typeof? (symbol --> :sxpath-pred:))
(define (node-typeof? crit)
  (case crit
    ((*) (lambda (n root ns-binding) (or (xml-element? n) (xml-attribute? n))))
    ((@) 
     (lambda (nl root ns-binding) (or (xml-attribute? nl)
				      (and (pair? nl) (xml-attribute? (car nl))))))
    ((*text*) sxp:literal?)
    ((*PI*) sxp:pi?)
    ((*comment*) sxp:comment?)
    ((*any*) sxp:true)
    (else (lambda (node root ns-binding)
	    (eq? crit (or (and (xml-attribute? node)
			       (xml-attribute-name node))
			  (gi node)))))))
; The function ntype? takes a type criterion and returns
; a function, which, when applied to a node, will tell if the node satisfies
; the test.
;	ntype? :: Crit -> Node -> Boolean
;
; The criterion 'crit' is 
;  one of the following symbols:
;	id		- tests if the Node has the right name (id)
;	@		- tests if the Node is an <attributes-list>
;	*		- tests if the Node is an <Element>
;	*text*		- tests if the Node is a text node
;	*data*		- tests if the Node is a data node 
;                         (text, number, boolean, etc., but not pair)
;	*PI*		- tests if the Node is a PI node
;	*COMMENT*	- tests if the Node is a COMMENT node
;	*ENTITY*        - tests if the Node is a ENTITY node
;	*any*		- #t for any type of Node
(: ntype? (symbol --> :sxpath-pred:))
(define (ntype? crit)
  (case crit
    ((*) (lambda (n root ns-binding) (or (xml-element? n) (xml-attribute? n))))
    ((@)
     (lambda (nl root ns-binding) (or (xml-attribute? nl)
				      (and (pair? nl) (xml-attribute? (car nl))))))
    ((*data*) (lambda (x root ns-binding)
		(not (or (pair? x)
			 (xml-element? x) (xml-attribute? x) (xml-pi? x)
			 (xml-comment? x)

			 ))))
    ((*any*) sxp:true)
    ((*text*) sxp:literal?)
    ((*PI*) sxp:pi?)
    ((*comment*) sxp:comment?)
    (else (lambda (node root ns-binding) (eq? crit (or (and (xml-attribute? node)
							    (xml-attribute-name node))
						       (gi node)))))
    ))

; This function takes a namespace-id, and returns a predicate
; Node -> Boolean, which is #t for nodes with this very namespace-id.
; ns-id is a string
; (ntype-namespace-id? #f) will be #t for nodes with non-qualified names.
(: ntype-namespace-id? ((or string symbol) --> :sxpath-pred:))
(define (ntype-namespace-id? ns-id)
  (let ((ns-id (if (symbol? ns-id) ns-id (string->symbol ns-id))))
    (lambda (node root ns-binding)
      (cond
       ((xml-element? node) (eq? ns-id (ns node)))
       ((xml-attribute? node) (eq? ns-id (xml-attribute-ns node)))
       (else #f)))))
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

; This function takes a predicate and returns it inverted 
; That is if the given predicate yields #f or '() the inverted one  
; yields the given node (#t) and vice versa.
(: sxp:invert (:sxpath-pred: --> :sxpath-pred:))
(define (sxp:invert pred)
  (lambda(node root ns-binding)
    (case (pred node root ns-binding)
      ((#f '()) node)
      (else #f))))

; Curried equivalence converter-predicates
(: node-eq? (:sxpath-node: --> :sxpath-pred:))
(define (node-eq? other)
  (lambda (node root ns-binding)
    (eq? other node)))

;(define (node-equal? other)
;  (lambda (node)
;    (equal? other node)))

(: node-equal? (list --> :sxpath-pred:))
(define (node-equal? other)
  (lambda (node root ns-binding)
    (cond
     ((xml-attribute? node)
      (and (pair? other) (eq? (xml-attribute-name node) (car other))
           (or (null? (cdr other))
               (equal? (xml-attribute-value node) (cadr other)))))
     (else ;;(node-list=? node)
	   #f))))

;(if (or (string? a) (number? a))
;      (if (not (or (string? b) (number? b)))
;          (sxp:node=? b a)
;          (equal? a b))
;      (equal?
;       ((if (xml-attribute? a) xml-attribute-value data) a) b))

;; XPath 3.4
(define (sxp:compare-singleton-node compare a b)
  ;; If not both parameters are node-lists, than b is none.
  (cond
   ((or (boolean? a) (boolean? b))
    (compare (boolean-value a) (boolean-value b)))
   ((or (number? a) (number? b))
    (compare (number-value a) (number-value b)))
   (else (compare (string-value a) (string-value b)))))

(define (sxp:compare compare left-expr right-expr)
  (lambda (node root ns-binding)
    (let loop ((a (left-expr node root ns-binding)) (b (right-expr node root ns-binding)))
      (cond
       ((or (node-list-empty? a) (node-list-empty? b))
        (compare (string-value a) (string-value b)))
       ((nodeset? a)
        (or (loop (node-list-first a) b)
            (let ((rest (node-list-rest a)))
              (and (not (node-list-empty? rest))
                   (loop rest b)))))
       ((nodeset? b)
        (or (loop a (node-list-first b))
            (let ((rest (node-list-rest b)))
              (and (not (node-list-empty? rest))
                   (loop a rest)))))
       (else (sxp:compare-singleton-node compare a b))))))

(define (sxp:equal? a b) (sxp:compare equal? a b))
(define (not-equal? a b) (not (equal? a b)))
(define (sxp:not-equal? a b) (sxp:compare not-equal? a b))

(define (sxp:lt? a b)
  (and-let* ((a (number-value a)) (b (number-value b))) (< a b)))
(define (sxp:< a b) (sxp:compare sxp:lt? a b))

(define (sxp:lt-eq? a b)
  (and-let* ((a (number-value a)) (b (number-value b))) (<= a b)))
(define (sxp:<= a b) (sxp:compare sxp:lt-eq? a b))

(define (sxp:gt? a b)
  (and-let* ((a (number-value a)) (b (number-value b))) (> a b)))
(define (sxp:> a b) (sxp:compare sxp:gt? a b))

(define (sxp:gt-eq? a b)
  (and-let* ((a (number-value a)) (b (number-value b))) (>= a b)))
(define (sxp:>= a b) (sxp:compare sxp:gt-eq? a b))

(: sxp:or (:sxpath-pred: :sxpath-pred: --> :sxpath-pred:))
(define (sxp:or a b)
  (lambda (node root ns-binding) (or (boolean-value (a node root ns-binding))
				     (boolean-value (b node root ns-binding)))))

(: sxp:and (:sxpath-pred: :sxpath-pred: --> :sxpath-pred:))
(define (sxp:and a b)
  (lambda (node root ns-binding) (and (boolean-value (a node root ns-binding))
				      (boolean-value (b node root ns-binding)))))

(define (sxp:+ a b)
  (lambda (node root ns-binding) (+ (number-value (a node)) (number-value (b node)))))

(define (sxp:- a b)
  (lambda (node root ns-binding) (+ (number-value (a node)) (number-value (b node)))))

(define (sxp:mul a b)
  (lambda (node root ns-binding) (* (number-value (a node)) (number-value (b node)))))

(define (sxp:div a b)
  (lambda (node root ns-binding) (quotient (number-value (a node)) (number-value (b node)))))

(define (sxp:mod a b)
  (lambda (node root ns-binding) (modulo (number-value (a node)) (number-value (b node)))))

(define (sxp:union . selectors)
  (lambda (node root ns-binding)
    (if (null? selectors) node
        (let ((result ((car selectors) node root ns-binding)))
          (if (node-list-empty? result) ((sxp:union (cdr selectors)) node root ns-binding)
              ((if (nodeset? result) append cons) result
               ((sxp:union (cdr selectors)) node root ns-binding)))))))

; node-pos:: N -> Nodeset -> Nodeset, or
; node-pos:: N -> Converter
; Select the N'th element of a Nodeset and return as a singular Nodeset;
; Return an empty nodeset if the Nth element does not exist.
; ((node-pos 1) Nodeset) selects the node at the head of the Nodeset,
; if exists; ((node-pos 2) Nodeset) selects the Node after that, if
; exists.
; N can also be a negative number: in that case the node is picked from
; the tail of the list.
; ((node-pos -1) Nodeset) selects the last node of a non-empty nodeset;
; ((node-pos -2) Nodeset) selects the last but one node, if exists.

;(define (node-pos n)
;  (lambda (nodeset)
;    (cond
;     ((not (nodeset? nodeset)) '())
;     ((null? nodeset) nodeset)
;     ((eqv? n 1) (list (car nodeset)))
;     ((negative? n) ((node-pos (+ n 1 (length nodeset))) nodeset))
;     (else
;      (assert (positive? n))
;      ((node-pos (-- n)) (cdr nodeset))))))

(define (node-pos n)
  (lambda (nodeset)
    (cond
     ((not (nodeset? nodeset)) (empty-node-list))
     ((node-list-empty? nodeset) nodeset)
     ((negative? n) ((node-pos (+ n 1 (node-list-length nodeset))) nodeset))
     (else (let loop ((n n) (l nodeset))
             (if (eqv? n 1)
                 (node-list (node-list-first l))
                 (loop (sub1 n) (node-list-rest l))))))))

; filter:: Converter -> Converter
; A filter applicator, which introduces a filtering context. The argument
; converter is considered a predicate, with either #f or nil result meaning
; failure.
;(define (sxp:filter pred?)
;  (lambda (lst)	; a nodeset or a node (will be converted to a singleton nset)
;    (let loop ((lst (if (nodeset? lst) lst (list lst))) (res '()))
;      (if (null? lst)
;	  (reverse res)
;	  (let ((pred-result (pred? (car lst))))
;	    (loop (cdr lst)
;		  (if (and pred-result (not (null? pred-result)))
;		      (cons (car lst) res)
;		      res)))))))

;(: sxp:filter (:sxpath-pred: --> :sxpath-step:))
(define (sxp:filter1 pred?)
  (lambda (lst ; a nodeset or a node (will be converted to a singleton nset)
	   root ns-binding)
    (let loop ((lst (if (nodeset? lst) lst (list lst))) (res '()))
      (if (node-list-empty? lst)
	  (reverse! res)                ; fresh cons'd
	  (let ((pred-result (pred? (node-list-first lst) root ns-binding)))
	    (loop (node-list-rest lst)
		  (if (and pred-result (not (node-list-empty? pred-result)))
		      (%node-list-cons (node-list-first lst) res)
		      res)))))))

(define (sxp:filter p/n)
  (if (number? p/n)
      (lambda (node root-node var-binding)
        ((node-pos p/n) node))
      (sxp:filter1 p/n)))

; take-until:: Converter -> Converter, or
; take-until:: Pred -> Node|Nodeset -> Nodeset
; Given a converter-predicate and a nodeset, apply the predicate to
; each element of the nodeset, until the predicate yields anything but #f or
; nil. Return the elements of the input nodeset that have been processed
; till that moment (that is, which fail the predicate).
; take-until is a variation of the sxp:filter above: take-until passes
; elements of an ordered input set till (but not including) the first
; element that satisfies the predicate.
; The nodeset returned by ((take-until (not pred)) nset) is a subset -- 
; to be more precise, a prefix -- of the nodeset returned by
; ((sxp:filter pred) nset)

;(define (take-until pred?)
;  (lambda (lst)	; a nodeset or a node (will be converted to a singleton nset)
;    (let loop ((lst (if (nodeset? lst) lst (list lst))))
;      (if (null? lst) lst
;	  (let ((pred-result (pred? (car lst))))
;	    (if (and pred-result (not (null? pred-result)))
;		'()
;		(cons (car lst) (loop (cdr lst)))))
;	  ))))

(: take-until (:sxpath-pred: --> :sxpath-step:))
(define (take-until pred?)
  (lambda (lst ; a nodeset or a node (will be converted to a singleton nset)
	   root ns-binding)
    (let loop ((lst (if (nodeset? lst) lst (list lst))))
      (if (node-list-empty? lst) lst
	  (let ((pred-result (pred? (node-list-first lst) root ns-binding)))
	    (if (and pred-result (not (node-list-empty? pred-result)))
		(empty-node-list)
		(%node-list-cons (node-list-first lst)
                                 (loop (node-list-rest lst)))))
          ))))

; take-after:: Converter -> Converter, or
; take-after:: Pred -> Node|Nodeset -> Nodeset
; Given a converter-predicate and a nodeset, apply the predicate to
; each element of the nodeset, until the predicate yields anything but #f or
; nil. Return the elements of the input nodeset that have not been processed:
; that is, return the elements of the input nodeset that follow the first
; element that satisfied the predicate.
; take-after along with take-until partition an input nodeset into three
; parts: the first element that satisfies a predicate, all preceding
; elements and all following elements.

;(define (take-after pred?)
;  (lambda (lst)	; a nodeset or a node (will be converted to a singleton nset)
;    (let loop ((lst (if (nodeset? lst) lst (list lst))))
;      (if (null? lst) lst
;	  (let ((pred-result (pred? (car lst))))
;	    (if (and pred-result (not (null? pred-result)))
;		(cdr lst)
;		(loop (cdr lst))))
;	  ))))


(: take-after (:sxpath-pred: --> :sxpath-step:))
(define (take-after pred?)
  (lambda (lst ; a nodeset or a node (will be converted to a singleton nset)
	   root ns-binding)
    (let loop ((lst (if (nodeset? lst) lst (list lst))))
      (if (node-list-empty? lst) lst
	  (let ((pred-result (pred? (node-list-first lst) root ns-binding)))
	    (if (and pred-result (not (node-list-empty? pred-result)))
		(node-list-rest lst)
		(loop (node-list-rest lst))))
	  ))))

; Apply proc to each element of lst and return the list of results.
; if proc returns a nodeset, splice it into the result
;
; From another point of view, map-union is a function Converter->Converter,
; which places an argument-converter in a joining context.

;(define (map-union proc lst)
;  (if (null? lst) lst
;      (let ((proc-res (proc (car lst))))
;	((if (nodeset? proc-res) append cons)
;	 proc-res (map-union proc (cdr lst))))))

(: map-union
   (:sxpath-step: :sxpath-nodelist: :xml-node: :xsl-envt:
    --> :sxpath-nodelist:))
(define (map-union proc lst root bindings)
  (let recurse ((proc proc) (lst lst) (root root) (bindings bindings))
    (if (node-list-empty? lst) lst
	(let ((proc-res (proc (node-list-first lst) root bindings)))
	  ((if (nodeset? proc-res) append cons)
	   proc-res (recurse proc (node-list-rest lst) root bindings))))))

; node-reverse :: Converter, or
; node-reverse:: Node|Nodeset -> Nodeset
; Reverses the order of nodes in the nodeset
; This basic converter is needed to implement a reverse document order
; (see the XPath Recommendation).
(: node-reverse :sxpath-step:)
(define node-reverse 
  (lambda (node-or-nodeset root ns-binding)
    (if (not (nodeset? node-or-nodeset)) (list node-or-nodeset)
	(reverse node-or-nodeset))))

; node-trace:: String -> Converter
; (node-trace title) is an identity converter. In addition it prints out
; a node or nodeset it is applied to, prefixed with the 'title'.
; This converter is very useful for debugging.

(define (node-trace title)
  (lambda (node-or-nodeset root ns-binding)
    (display "\n-->")
    (display title)
    (display " :")
    (pretty-print node-or-nodeset)
    node-or-nodeset))


;-------------------------
; Converter combinators
;
; Combinators are higher-order functions that transmogrify a converter
; or glue a sequence of converters into a single, non-trivial
; converter. The goal is to arrive at converters that correspond to
; XPath location paths.
;
; From a different point of view, a combinator is a fixed, named
; _pattern_ of applying converters. Given below is a complete set of
; such patterns that together implement XPath location path
; specification. As it turns out, all these combinators can be built
; from a small number of basic blocks: regular functional composition,
; map-union and sxp:filter applicators, and the nodeset union.



; select-kids:: Pred -> Node -> Nodeset
; Given a Node, return an (ordered) subset its children that satisfy
; the Pred (a converter, actually)
; select-kids:: Pred -> Nodeset -> Nodeset
; The same as above, but select among children of all the nodes in
; the Nodeset
;
; More succinctly, the signature of this function is
; select-kids:: Converter -> Converter

;(define (select-kids test-pred?)
;  (lambda (node)		; node or node-set
;    (cond 
;     ((null? node) node)
;     ((not (pair? node)) '())   ; No children
;     ((symbol? (car node))
;      ((sxp:filter test-pred?) (cdr node)))	; it's a single node
;     (else (map-union (select-kids test-pred?) node root ns-binding)))))

;; HACK: if the test-pred? is a symbol, we dispatch to the magic
;; form-field handling.  Needs to be changed to use a well know, true
;; predicate. (Avoiding the export for dev time.)
(: select-kids ((or symbol :sxpath-pred:) --> :sxpath-step:))
(define (select-kids test-pred?)
  (lambda (node				; node or node-set
	   root ns-binding)
    (cond
     ((symbol? test-pred?)
      (cond
       ((xml-element? node)
	(if (or (eq? (gi node) 'form)
		(eq? (ns node) namespace-forms))
	    (let ((special (xml-element-special node)))
	      (if special
		  (special test-pred?)
		  (select-elements (%children node) test-pred?)))
	    (select-elements (%children node) test-pred?)))
       ((nodeset? node) (map-union (select-kids test-pred?) node root ns-binding))
       (else (empty-node-list))))
     ((xml-element? node)
      ((sxp:filter test-pred?) (%children node) root ns-binding)) ; it's a single node
     ((xml-attribute? node)
      ((sxp:filter test-pred?) (node-list node (xml-attribute-value node)) root ns-binding))
     ((xml-document? node)
      ((sxp:filter test-pred?) (xml-document-children node) root ns-binding))
     ((nodeset? node) (map-union (select-kids test-pred?) node root ns-binding))
     (else (empty-node-list)))))


;; reduce-kids combines a select-kids test with node-reduce.  Special
;; cased for special XML elements supporting their own selector methods.
(: reduce-kids ((or symbol :sxpath-pred:) &rest :sxpath-step: --> :sxpath-step:))
(define (reduce-kids test-pred? . converters)
  (lambda (node				; node or node-set
	   root binding)
    (cond
     ((symbol? test-pred?)
      (cond
       ((xml-element? node)
	(cond
	 ((and (eq? (ns node) namespace-xsql) (xml-element-special node))
	  =>
	  ;; This will become a one-line dispatch to "special".  Just
	  ;; for debugging now we keep all code here, where it does
	  ;; NOT belong.
	  (lambda (special)
	    (cond
	     ((null? converters)
	      (make-xml-element test-pred? #f (empty-node-list) (special test-pred?)))
	     ;; This test is simply wrong, isn't it?
	     ((eq? (car converters) sxp:literal?)
	      (special test-pred?))
	     (else
	      ((apply node-reduce converters)
	       (make-xml-element test-pred? #f (empty-node-list) (special test-pred?))
	       root binding)))))
	 ((and (or (eq? (gi node) 'form)
		   (eq? (ns node) namespace-forms))
	       (not (or (eq? test-pred? '*) (eq? test-pred? '@) (eq? test-pred? '*any*)
			(eq? test-pred? '*text*) (eq? test-pred? '*comment*) (eq? test-pred? '*PI*) )))
	  (let* ((special (xml-element-special node))
		 (sel (if special
			  (special test-pred?)
			  (select-elements (%children node) test-pred?))))
	    (if (null? converters)
		 sel
		 ((apply node-reduce converters) sel root binding))))
	 (else ((if (null? converters)
		    (select-kids (ntype? test-pred?))
		    (apply node-reduce (select-kids (ntype? test-pred?)) converters))
		node root binding))))
       ((nodeset? node) (map-union (apply reduce-kids test-pred? converters) node root binding))
       (else ((if (null? converters)
		  (select-kids (ntype? test-pred?))
		  (apply node-reduce (select-kids (ntype? test-pred?)) converters))
	      node root binding))))
     ((nodeset? node) (map-union (apply reduce-kids test-pred? converters) node root binding))
     (else ((if (null? converters)
		(select-kids test-pred?)
		(apply node-reduce (select-kids test-pred?) converters))
	    node root binding)))))

; Unified with sxpath-ext and sxml-tools
; Returns the list of attributes for a given SXML node
; Empty list is returned if the given node os not an element,
; or if it has no list of attributes

(: sxml:attr-list
   (:sxpath-node: :xml-node: :xml-ns-env: --> (list-of :xml-attribute:)))
(define (sxml:attr-list obj root ns-binding)
  (if (xml-element? obj)
      (xml-element-attributes obj)
      '()))

; Attribute axis
(: sxml:attribute (:sxpath-pred: --> :sxpath-step:))
(define (sxml:attribute test-pred?)
  (let ((fltr (sxp:filter test-pred?)))
    (lambda (node root ns-binding)
      (fltr (node-attributes node root ns-binding) root ns-binding))))

(: node-attributes :sxpath-step:)
(define (node-attributes nodeset root ns-binding)
  (map-union
   (lambda (node root ns-binding)
     (if (xml-element? node)
	 (xml-element-attributes node)
	 (empty-node-list)))
   nodeset root ns-binding))

;;(define (node-namespaces nodeset root bindings)
 ;; binding)

(define node-namespaces 'node-namespaces)

; Child axis
;  This function is similar to 'select-kids', but it returns an empty
;  child-list for PI, Comment and Entity nodes and has no predicate.
(: sxp:child :sxpath-step:)
(define (sxp:child node root vars)
  (cond
   ((xml-element? node) (%children node))
   ((xml-attribute? node) (xml-attribute-value node))
   ((xml-document? node) (xml-document-children node))
   ((node-list-empty? node) (empty-node-list))
   ((node-list? node) (children node))
   (else (empty-node-list))))

; Parent axis
; Given a predicate, it returns a function 
;  RootNode -> Converter
; which which yields a 
;  node -> parent 
; converter then applied to a rootnode.
; Thus, such a converter may be constructed using
;  ((sxml:parent test-pred) rootnode)
; and returns a parent of a node it is applied to.
; If applied to a nodeset, it returns the 
; list of parents of nodes in the nodeset. The rootnode does not have
; to be the root node of the whole SXML tree -- it may be a root node
; of a branch of interest.
; The parent:: axis can be used with any SXML node.

;; JFW: this is a rather expensive way to compute the parent.  In
;; contrast to the stx engine, our's maintains a reverse parent list
;; when descending the tree.  BEWARE: watch out shortcut the
;; resolution when available.

(: sxml:parent (:sxpath-pred: --> (procedure (:xml-node:) :sxpath-step:)))
(define (sxml:parent test-pred?)
  (lambda (root-node)			; node or nodeset
    (lambda (node			; node or nodeset
	     root ns-binding)
      (if (nodeset? node)
	  (map-union ((sxml:parent test-pred?) root-node) node root ns-binding)
	  (let loop ((roots (if (nodeset? root-node)
				root-node (list root-node))))
	    (let ((first (node-list-first roots)))
	      (if (node-list-empty? first)
		  '()
		  (append
		   (cond
		    ((and (xml-attribute? first) (xml-literal? node)
			  (equal? (xml-attribute-value first) (data node)))
		     (list first))
		    ((and (xml-attribute? node) (xml-element? first))
		     (let ((x (memq node (xml-element-attributes first))))
		       (if (and x ((sxp:filter test-pred?) (car x) root ns-binding))
			   (list first)
			   (loop (children node)))))
		    ((xml-element? node)
		     (let ((x (memq node (%children first))))
		       (if (and x ((sxp:filter test-pred?) (car x) root ns-binding))
			   (list first)
			   (loop (children node)))))
		    (else '()))
		   (loop (node-list-rest roots))))))))))

; node-self:: Pred -> Node -> Nodeset, or
; node-self:: Converter -> Converter
; Similar to select-kids but apply to the Node itself rather
; than to its children. The resulting Nodeset will contain either one
; component, or will be empty (if the Node failed the Pred).
(define node-self sxp:filter)


; node-join:: [LocPath] -> Node|Nodeset -> Nodeset, or
; node-join:: [Converter] -> Converter
; join the sequence of location steps or paths as described
; in the title comments above.
;(define (node-join . selectors)
;  (lambda (nodeset root ns-bindings)		; Nodeset or node
;    (let loop ((nodeset nodeset) (selectors selectors))
;      (if (null? selectors) nodeset
;	  (loop 
;	   (if (nodeset? nodeset)
;	       (map-union (car selectors) nodeset root ns-binding)
;	       ((car selectors) nodeset))
;	   (cdr selectors))))))

(define (node-join . selectors)
  (lambda (nodeset		; Nodeset or node
	   root ns-binding)
    (let loop ((nodeset nodeset) (selectors selectors))
      (if (null? selectors) nodeset
	  (loop 
	   (if (node-list-empty? (node-list-rest nodeset))
	       ((car selectors) nodeset root ns-binding)
	       (map-union (car selectors) nodeset root ns-binding))
	   (cdr selectors))))))

; node-reduce:: [LocPath] -> Node|Nodeset -> Nodeset, or
; node-reduce:: [Converter] -> Converter
; A regular functional composition of converters.
; From a different point of view,
;    ((apply node-reduce converters) nodeset)
; is equivalent to
;    (foldl apply nodeset converters)
; i.e., folding, or reducing, a list of converters with the nodeset
; as a seed.
(define (node-reduce . converters)
  (lambda (nodeset			; Nodeset or node
	   root ns-binding)
    (let loop ((nodeset nodeset) (converters converters))
      (if (node-list-empty? nodeset) nodeset
          (if (null? converters) nodeset
              (loop ((car converters) nodeset root ns-binding)
		    (cdr converters)))))))


; node-or:: [Converter] -> Converter
; This combinator applies all converters to a given node and
; produces the union of their results.
; This combinator corresponds to a union, '|' operation for XPath
; location paths.
; (define (node-or . converters)
;   (lambda (node-or-nodeset)
;     (if (null? converters) node-or-nodeset
; 	(append 
; 	 ((car converters) node-or-nodeset)
; 	 ((apply node-or (cdr converters)) node-or-nodeset)))))
; More optimal implementation follows
;(define (node-or . converters)
;  (lambda (node-or-nodeset)
;    (let loop ((result '()) (converters converters))
;      (if (null? converters) result
;	  (loop (append result (or ((car converters) node-or-nodeset) '()))
;		(cdr converters))))))

(define (node-or . converters)
  (lambda (node-or-nodeset root ns-binding)
    (let loop ((result (empty-node-list)) (converters converters))
      (if (null? converters) result
	  (loop (%node-list-append
                 result (or ((car converters) node-or-nodeset root ns-binding)
                            (empty-node-list)))
		(cdr converters))))))

; node-closure:: Converter -> Converter
; Select all _descendants_ of a node that satisfy a converter-predicate.
; This combinator is similar to select-kids but applies to
; grand... children as well.
; This combinator implements the "descendant::" XPath axis
; Conceptually, this combinator can be expressed as
; (define (node-closure f)
;      (node-or
;        (select-kids f)
;	 (node-reduce (select-kids (node-typeof? '*)) (node-closure f))))
; This definition, as written, looks somewhat like a fixpoint, and it
; will run forever. It is obvious however that sooner or later
; (select-kids (node-typeof? '*)) will return an empty nodeset. At
; this point further iterations will no longer affect the result and
; can be stopped.

(: node-closure (:sxpath-pred: --> :sxpath-step:))
(define (node-closure test-pred?)	    
  (lambda (node				; Nodeset or node
	   root ns-binding)
    (let loop ((parent node) (result (empty-node-list)))
      (if (node-list-empty? parent) result
	  (loop ((select-kids (node-typeof? '*)) parent root ns-binding)
		(%node-list-append result
                                   ((select-kids test-pred?) parent root ns-binding)))
	  ))))

(: node-descendant-or-self (:sxpath-pred: --> :sxpath-step:))
(define (node-descendant-or-self test-pred?)
  (lambda (nodeset root ns-binding)
    ((if (nodeset? nodeset) append cons) nodeset
     ((node-closure test-pred?) nodeset root ns-binding))))

; node-parent:: RootNode -> Converter
; (node-parent rootnode) yields a converter that returns a parent of a
; node it is applied to. If applied to a nodeset, it returns the list
; of parents of nodes in the nodeset. The rootnode does not have
; to be the root node of the whole SXML tree -- it may be a root node
; of a branch of interest.
; Given the notation of Philip Wadler's paper on semantics of XSLT,
;  parent(x) = { y | y=subnode*(root), x=subnode(y) }
; Therefore, node-parent is not the fundamental converter: it can be
; expressed through the existing ones.  Yet node-parent is a rather
; convenient converter. It corresponds to a parent:: axis of SXPath.
; Note that the parent:: axis can be used with an attribute node as well!

;(define (node-parent rootnode)
;  (lambda (node root ns-bindings)		; Nodeset or node
;    (if (nodeset? node) (map-union (node-parent rootnode) node root ns-binding)
;	(let ((pred
;	       (node-or
;		(node-reduce
;		 (node-self (node-typeof? '*))
;		 (select-kids (node-eq? node)))
;		(node-join
;		 (select-kids (node-typeof? '@))
;		 (select-kids (node-eq? node))))))
;	  ((node-or
;	    (node-self pred)
;	    (node-closure pred))
;	   rootnode)))))

;; (define (node-parent rootnode)
;;   (lambda (node)		; Nodeset or node
;;     (if (nodeset? node) (map-union (node-parent rootnode) node ns-binding)
;; 	(let ((pred
;; 	       (node-or
;; 		(node-reduce
;; 		 (node-self (node-typeof? '*))
;; 		 (select-kids (node-eq? node)))
;; 		(node-join
;; 		 (select-kids (node-typeof? '@))
;; 		 (select-kids (node-eq? node))))))
;; 	  ((node-or
;; 	    (node-self pred)
;; 	    (node-closure pred))
;; 	   rootnode)))))

(define node-parent (sxml:parent (ntype? '*any*)))

;-------------------------
; Evaluate an abbreviated SXPath
;	sxpath:: AbbrPath -> Converter, or
;	sxpath:: AbbrPath -> Node|Nodeset -> Nodeset
; AbbrPath is a list. It is translated to the full SXPath according
; to the following rewriting rules
; (sxpath '()) -> (node-join)
; (sxpath '(path-component ...)) ->
;		(node-join (sxpath1 path-component) (sxpath '(...)))
; (sxpath1 '//) -> (node-or 
;		     (node-self (ntype? '*any*))
;		     (node-closure (ntype? '*any*)))
; (sxpath1 '(equal? x)) -> (select-kids (node-equal? x))
; (sxpath1 '(eq? x))    -> (select-kids (node-eq? x))
; (sxpath1 '(or@ ...))  -> (select-kids (ntype-names?
;                                          (cdr '(or@ ...))))
; (sxpath1 '(not@ ...)) -> (select-kids (sxml:invert 
;                                         (ntype-names?
;                                          (cdr '(not@ ...)))))
; (sxpath1 '(ns-id:* x)) -> (select-kids 
;                                      (ntype-namespace-id? x))
; (sxpath1 ?symbol)     -> (select-kids (ntype? ?symbol))
; (sxpath1 ?string)     -> (txpath ?string)
; (sxpath1 procedure)   -> procedure
; (sxpath1 '(?symbol ...)) -> (sxpath1 '((?symbol) ...))
; (sxpath1 '(path reducer ...)) ->
;		(node-reduce (sxpath path) (sxpathr reducer) ...)
; (sxpathr number)      -> (node-pos number)
; (sxpathr path-filter) -> (filter (sxpath path-filter))
;; (define (sxpath path . ns-binding)
;;   (let loop ((converters '())
;;              (root-vars '())
;;              (path (if (string? path) (list path) path)))
;;     (cond
;;       ((null? path)  ; parsing is finished
;;        (lambda (node . root-var-binding)
;;          (let ((root-node
;;                 (if (null? root-var-binding) node (car root-var-binding)))
;;                (var-binding
;;                 (if
;;                  (or (null? root-var-binding) (null? (cdr root-var-binding)))
;;                  '() (cadr root-var-binding))))
;;            (let rpt ((nodeset (if (nodeset? node) node (list node)))
;;                      (conv (reverse converters))
;;                      (r-v (reverse root-vars)))
;;              (if
;;               (null? conv)  ; the path is over
;;               nodeset
;;               (rpt
;;                (if (car r-v)  ; the current converter consumes 3 arguments
;;                    ((car conv) nodeset root-node var-binding)
;;                    ((car conv) nodeset))
;;                (cdr conv)
;;                (cdr r-v)))))))
;;       ; or@ handler
;;       ((and (pair? (car path)) 
;;             (not (null? (car path)))
;;             (eq? 'or@ (caar path)))
;;        (loop (cons (select-kids (ntype-names? (cdar path))) converters)
;;              (cons #f root-vars)
;;              (cdr path)))
;;       ; not@ handler 
;;       ((and (pair? (car path)) 
;;             (not (null? (car path)))
;;             (eq? 'not@ (caar path)))
;;        (loop (cons
;;               (select-kids (sxml:invert (ntype-names? (cdar path))))
;;               converters)
;;              (cons #f root-vars)
;;              (cdr path)))
;;       ((procedure? (car path))
;;        (loop (cons (car path) converters)
;;              (cons #t root-vars)
;;              (cdr path)))
;;       ((eq? '// (car path))
;;        (loop (cons (node-or 
;;                     (node-self (ntype? '*any*))
;;                     (node-closure (ntype? '*any*)))
;;                    converters)
;;              (cons #f root-vars)
;;              (cdr path)))
;;       ((symbol? (car path))
;;        (loop (cons (select-kids (ntype? (car path))) converters)
;;              (cons #f root-vars)
;;              (cdr path)))
;;       ((string? (car path))
;;        (and-let*
;;         ((f (txpath (car path) ns-binding)))
;;         (loop (cons f converters)
;;               (cons #t root-vars)
;;               (cdr path))))
;;       ((and (pair? (car path)) (eq? 'equal? (caar path)))
;;        (loop (cons (select-kids (apply node-equal? (cdar path))) converters)
;;              (cons #f root-vars)
;;              (cdr path)))
;;       ; ns-id:* handler 
;;       ((and (pair? (car path)) (eq? 'ns-id:* (caar path)))
;;        (loop
;;         (cons (select-kids (ntype-namespace-id? (cadar path))) converters)
;;         (cons #f root-vars)
;;         (cdr path)))
;;       ((and (pair? (car path)) (eq? 'eq? (caar path)))
;;        (loop (cons (select-kids (apply node-eq? (cdar path))) converters)
;;              (cons #f root-vars)
;;              (cdr path)))      
;;       ((pair? (car path))
;;        (and-let*
;;         ((select
;;           (if
;;            (symbol? (caar path))
;;            (lambda (node root-node . var-binding)
;;              ((select-kids (ntype? (caar path))) node))
;;            (sxpath (caar path)))))       
;;         (let reducer ((reducing-path (cdar path))
;;                       (filters '()))
;;           (cond
;;             ((null? reducing-path)
;;              (loop
;;               (cons
;;                (lambda (node root-node var-binding)                 
;;                  (map-union
;;                   (lambda (node)
;;                     (let label ((nodeset (select node root-node var-binding))
;;                                 (fs (reverse filters)))
;;                       (if
;;                        (null? fs)
;;                        nodeset
;;                        (label
;;                         ((car fs) nodeset root-node var-binding)
;;                         (cdr fs)))))
;;                   (if (nodeset? node) node (list node))
;;                   ns-binding))
;;                converters)
;;               (cons #t root-vars)
;;               (cdr path)))
;;             ((number? (car reducing-path))
;;              (reducer
;;               (cdr reducing-path)
;;               (cons
;;                (lambda (node root-node var-binding)
;;                  ((node-pos (car reducing-path)) node))
;;                filters)))
;;             (else
;;              (and-let*
;;               ((func (sxpath (car reducing-path))))
;;               (reducer
;;                (cdr reducing-path)
;;                (cons
;;                 (lambda (node root-node var-binding)
;;                   ((sxml:filter
;;                     (lambda (n) (func n root-node var-binding)))
;;                     node))
;;                 filters))))))))
;;       (else
;;        (cerr "Invalid path step: " (car path))
;;        #f))))

(define sxpath-special-symbols
  '(// @ prefix:* eq? equal? * *text* *PI* *any*))

(: sxpath* (* :xml-ns-env: --> procedure))
(define (sxpath* path ns-binding)
  (let recurse ((path path))
    (let loop ((converters '())
	       (path (if (string? path) (list path) path)))

      (cond-expand
       (chicken (chicken-check-interrupts!))
       (else #f))

      (cond
       ((null? path)  ; parsing is finished
	(lambda (node . root-var-binding)
	  (let ((root-node
		 (if (null? root-var-binding) node (car root-var-binding)))
		(var-binding
		 (if
		  (or (null? root-var-binding) (null? (cdr root-var-binding)))
		  '() (cadr root-var-binding))))
	    (let rpt ((nodeset (if (nodeset? node) node (list node)))
		      (conv (reverse converters)))
	      (if
	       (null? conv)  ; the path is over
	       nodeset
	       (rpt
		((car conv) nodeset root-node var-binding)
		(cdr conv)))))))
					; or@ handler
       ((and (pair? (car path)) 
	     (not (null? (car path)))
	     (eq? 'or@ (caar path)))
	(loop (cons (select-kids (ntype-names? (cdar path))) converters)
	      (cdr path)))
					; not@ handler 
       ((and (pair? (car path)) 
	     (not (null? (car path)))
	     (eq? 'not@ (caar path)))
	(loop (cons
	       (select-kids (sxp:invert (ntype-names? (cdar path))))
	       converters)
	      (cdr path)))
       ((procedure? (car path))
	(loop (cons (car path) converters)
	      (cdr path)))
       ((eq? '// (car path))
	(loop (cons (node-or
		     (node-self (ntype? '*any*))
		     (node-closure (ntype? '*any*)))
		    converters)
	      (cdr path)))
       ((eq? '@ (car path))
	(loop (cons node-attributes converters)
	      (cdr path)))
       ((symbol? (car path))
	(loop ;;(cons (select-kids (ntype? (car path))) converters)
 	      (cons (reduce-kids (car path)) converters)
	      (cdr path)))
       ;;       ((string? (car path))
       ;;        (and-let*
       ;;         ((f (txpath (car path) ns-binding)))
       ;;         (loop (cons f converters)
       ;;               (cdr path))))
       ((and (pair? (car path)) (eq? 'equal? (caar path)))
	(loop (cons (select-kids (apply node-equal? (cdar path))) converters)
	      (cdr path)))
					; ns-id:* handler 
       ((and (pair? (car path)) (eq? 'ns-id:* (caar path)))
	(loop
	 (cons (let ((bound (find-namespace-by-prefix (cadar path) ns-binding)))
		 (if bound
		     (select-kids (ntype-namespace-id? (xml-namespace-uri bound)))
		     (error "ns-prefix '~a' not bound" (cadar path))))
	       converters)
	 (cdr path)))
       ((and (pair? (car path)) (eq? 'eq? (caar path)))
	(loop (cons (select-kids (apply node-eq? (cdar path))) converters)
	      (cdr path)))      
       ((pair? (car path))
	(and-let*
	 ((select
	   (let ((key (caar path)))
	     (cond
	      ((eq? key '@) (recurse '(@)))
	      ((symbol? key)
	       (lambda (node root-node var-binding)
		 ((select-kids (ntype? key)) node root-node var-binding)))
	      (else (recurse (caar path)))))))       
	 (let reducer ((reducing-path (cdar path))
		       (filters '()))
	   (cond
            ((null? reducing-path)
             (loop
              (cons
               (lambda (node root-node var-binding)                 
                 (map-union
                  (lambda (node root ns-binding)
                    (let label ((nodeset (select node root-node var-binding))
                                (fs (reverse filters)))
                      (if
                       (null? fs)
                       nodeset
                       (label
                        ((car fs) nodeset root-node var-binding)
                        (cdr fs)))))
                  (if (nodeset? node) node (list node))
		  root-node
		  var-binding))
               converters)
              (cdr path)))
            ((number? (car reducing-path))
             (reducer
              (cdr reducing-path)
              (cons
               (lambda (node root-node var-binding)
                 ((node-pos (car reducing-path)) node))
               filters)))
            (else
             (and-let*
              ((func (recurse (car reducing-path))))
              (reducer
               (cdr reducing-path)
               (cons
                (lambda (node root-node var-binding)
                  ((sxp:filter
                    (lambda (n r b) (func n r b)))
		   node root-node var-binding))
                filters))))))))
       (else
	(error "Invalid path step: ~a" (car path))
	#f)))))

(define (sxpath path . ns-binding)
  (let* ((ns-binding (if (pair? ns-binding) (car ns-binding) '()))
	 (expr (sxpath* path (if (pair? ns-binding) (car ns-binding) '()))))
    (lambda (node . root-var-bindings)
      (expr node
	    (if (pair? root-var-bindings) (car root-var-bindings) '())
	    ns-binding))))


;; W3C compliant extensions to SXPathlib
; $Id: sxpath-ext.scm,v 1.911 2002/12/06 22:10:53 kl Exp kl $:
;
; This software is in Public Domain.
; IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND.
;
; Stying close to the oiginal API the implementation modified by Joerg
; Wittenberger while integrating with the BALL implementation of XML
; transformations.  It consider the extensible data model of BALL
; superior to s-expression SXML.  But I'd like to be able to use sxml
; for both, especially for Scheme source code too.  The intent is
; therefore complete decoupling of the data model, be it
; s-expressions, xml node list classes or what ever, from the
; transformation tools.
;
; Please send bug reports and comments to:
;   lisovsky@acm.org      Kirill Lisovsky
;   lizorkin@hotbox.ru    Dmitry Lizorkin
;   Joerg.Wittenberger@softeyes.net Joerg F. Wittenberger

;=========================================================================
; The counterpart to XPath 'string' function (section 4.2 XPath Rec.)
; Converts a given object to a string
; NOTE:
;  1. When converting a nodeset - a document order is not preserved
;  2. number->string function returns the result in a form which is slightly
; different from XPath Rec. specification
(define (sxml:string-value obj) (data obj))
(define (sxml:string object)
  (cond
    ((string? object) object)
    ((nodeset? object) (if (node-list-empty? object)
			 ""
			 (sxml:string-value (node-list-first object))))
    ((number? object) (number->string object))
    ((boolean? object) (if object "true" "false"))
    (else
     ;; "" ; Unknown type -> empty string. 
     ;; Option: write its value to string port?
     (call-with-output-string (lambda (p) (write object p))))))  

; The counterpart to XPath 'boolean' function (section 4.3 XPath Rec.)
; Converts its argument to a boolean
(: sxml:boolean (* --> boolean))
(define (sxml:boolean object)
  (cond
    ((boolean? object) object)
    ((number? object) (not (eqv? object 0)))
    ((string? object) (> (string-length object) 0))
    ((nodeset? object) (not (node-list-empty? object)))
    (else #f)))  ; Not specified in XPath Rec.

; The counterpart to XPath 'number' function (section 4.4 XPath Rec.)
; Converts its argument to a number
; NOTE: 
;  1. The argument is not optional (yet?)
;  2. string->number conversion is not IEEE 754 round-to-nearest
;  3. NaN is represented as 0
(define (sxml:number obj)
  (cond
    ((number? obj) obj)
    ((string? obj)
     (let ((nmb (call-with-input-string obj read)))
       (if (number? nmb)
	 nmb
	 0))) ; NaN
    ((boolean? obj) (if obj 1 0))
    ((nodeset? obj) (sxml:number (sxml:string obj)))
    (else 0))) ; unknown datatype

;=========================================================================
; Comparators for XPath objects 

; A helper for XPath equality operations: = , !=
;  'bool-op', 'number-op' and 'string-op' are comparison operations for 
; a pair of booleans,  numbers and strings respectively
(define (sxml:equality-cmp bool-op number-op string-op)
  (lambda (obj1 obj2)
    (cond
      ((and (not (nodeset? obj1)) (not (nodeset? obj2)))  
       ; neither object is a nodeset
       (cond
         ((boolean? obj1) (bool-op obj1 (sxml:boolean obj2)))
         ((boolean? obj2) (bool-op (sxml:boolean obj1) obj2))
         ((number? obj1) (number-op obj1 (sxml:number obj2)))
         ((number? obj2) (number-op (sxml:number obj1) obj2))
         (else  ; both objects are strings
          (string-op obj1 obj2))))
      ((and (nodeset? obj1) (nodeset? obj2))  ; both objects are nodesets
       (let first ((str-set1 (map sxml:string-value obj1))
                   (str-set2 (map sxml:string-value obj2)))
         (cond
           ((null? str-set1) #f)
           ((let second ((elem (car str-set1))
                         (set2 str-set2))
              (cond
                ((null? set2) #f)
                ((string-op elem (car set2)) #t)
                (else (second elem (cdr set2))))) #t)
           (else
            (first (cdr str-set1) str-set2)))))
      (else  ; one of the objects is a nodeset, another is not
       (receive
	(nset elem)
	;; Equality operations are commutative
	(if (nodeset? obj1) (values obj1 obj2) (values obj2 obj1))
        (cond
          ((boolean? elem) (bool-op elem (sxml:boolean nset)))
          ((number? elem)
           (let loop ((nset 
                       (map
                        (lambda (node) (sxml:number (sxml:string-value node)))
                        nset)))
             (cond
               ((null? nset) #f)
               ((number-op elem (car nset)) #t)
               (else (loop (cdr nset))))))
          ((string? elem)
           (let loop ((nset (map sxml:string-value nset)))
             (cond
               ((null? nset) #f)
               ((string-op elem (car nset)) #t)
               (else (loop (cdr nset))))))
          (else  ; unknown datatype
           (error "Unknown datatype: ~a" elem)
           #f)))))))
         
(define sxml:equal? (sxml:equality-cmp eq? = string=?))

(define sxml:not-equal?
  (sxml:equality-cmp
   (lambda (bool1 bool2) (not (eq? bool1 bool2)))
   (lambda (num1 num2) (not (= num1 num2)))
   (lambda (str1 str2) (not (string=? str1 str2)))))
         

; Relational operation ( < , > , <= , >= ) for two XPath objects
;  op is comparison procedure: < , > , <= or >=
(define (sxml:relational-cmp op)
  (lambda (obj1 obj2)
    (cond
      ((not (or (nodeset? obj1) (nodeset? obj2)))  ; neither obj is a nodeset
       (op (sxml:number obj1) (sxml:number obj2)))
      ((boolean? obj1)  ; 'obj1' is a boolean, 'obj2' is a nodeset
       (op (sxml:number obj1) (sxml:number (sxml:boolean obj2))))
      ((boolean? obj2)  ; 'obj1' is a nodeset, 'obj2' is a boolean
       (op (sxml:number (sxml:boolean obj1)) (sxml:number obj2)))
      ((or (null? obj1) (null? obj2)) ; one of the objects is an empty nodeset
       #f)
      (else  ; at least one object is a nodeset
       (op
        (cond
          ((nodeset? obj1)  ; 'obj1' is a (non-empty) nodeset
           (let ((nset1 (map
                         (lambda (node) (sxml:number (sxml:string-value node)))
                         obj1)))
             (let first ((num1 (car nset1))
                         (nset1 (cdr nset1)))
               (cond
                 ((null? nset1) num1)
                 ((op num1 (car nset1)) (first num1 (cdr nset1)))
                 (else (first (car nset1) (cdr nset1)))))))
          ((string? obj1) (sxml:number obj1))
          (else  ; 'obj1' is a number
           obj1))
        (cond
          ((nodeset? obj2)  ; 'obj2' is a (non-empty) nodeset
           (let ((nset2 (map
                         (lambda (node) (sxml:number (sxml:string-value node)))
                         obj2)))
             (let second ((num2 (car nset2))
                          (nset2 (cdr nset2)))
               (cond
                 ((null? nset2) num2)
                 ((op num2 (car nset2)) (second (car nset2) (cdr nset2)))
                 (else (second num2 (cdr nset2)))))))
          ((string? obj2) (sxml:number obj2))
          (else  ; 'obj2' is a number
           obj2)))))))

;;
;; SOAP support stuff.

(define soap-error-attributes
  (list
   (make-xml-attribute 'env 'xmlns namespace-soap-envelope-str)
   (make-xml-attribute 'a 'xmlns namespace-mind-str)
   (make-xml-attribute 'dc 'xmlns namespace-dc-str)))

(define soap-sender-error-code
  (make-xml-element
   'Code namespace-soap-envelope '()
   (node-list
    (make-xml-element
     'Value namespace-soap-envelope '()
     (literal "env:Sender"))
    (make-xml-element
     'Subcode namespace-mind '()
     (literal "exception")))))

(define soap-receiver-error-code
  (make-xml-element
   'Code namespace-soap-envelope '()
   (node-list
    (make-xml-element
     'Value namespace-soap-envelope '()
     (literal "env:Receiver"))
    (make-xml-element
     'Subcode namespace-mind '()
     (literal "exception")))))

(define soap-xml-lang-attribute-list
  (list (make-xml-attribute 'lang namespace-xml "en")))

(define (make-soap-para text)
  (if (xml-element? text)
      text
      (make-xml-element
       'Text namespace-soap-envelope '()
       (make-xml-literal (if (string? text) text (format #f "~a" text))))))

(define (make-soap-sender-fault name title message msgtxt)
  (make-xml-element
   'Fault namespace-soap-envelope soap-error-attributes
   (node-list
    soap-sender-error-code
    (make-xml-element
     'Reason namespace-soap-envelope '()
     (make-xml-element
      'Text namespace-soap-envelope soap-xml-lang-attribute-list
      (literal title)))
    (make-xml-element
     'Detail namespace-soap-envelope '()
     (node-list
      (if name
          (make-xml-element
           'Relation namespace-dc '()
           (make-xml-element 'id namespace-mind '() (literal name)))
          (empty-node-list))
      (if (pair? message)
          (map make-soap-para message)
          (make-soap-para message))))
;;     (map (lambda (para)
;;            (make-xml-element
;;             'Detail namespace-soap-envelope '()
;;             (make-soap-para para)))
;;          msgtxt)
    )))

(define (make-soap-receiver-fault name title message msgtxt)
  (make-xml-element
   'Fault namespace-soap-envelope soap-error-attributes
   (node-list
    soap-receiver-error-code
    (make-xml-element
     'Reason namespace-soap-envelope '()
     (make-xml-element
      'Text namespace-soap-envelope soap-xml-lang-attribute-list
      (literal title)))
    (make-xml-element
     'Detail namespace-soap-envelope '()
     (%node-list-cons
      (make-xml-element
       'Relation namespace-dc '()
       (make-xml-element 'id namespace-mind '() (literal name)))
      (if (pair? message)
          (map make-soap-para message)
          (make-soap-para message))))
;;     (map (lambda (para)
;;            (make-xml-element
;;             'Detail namespace-soap-envelope '()
;;             (make-soap-para para)))
;;          msgtxt)
    )))

(define (make-soap-sender-error name title message msgtxt)
  (make-xml-element
   'Envelope namespace-soap-envelope soap-error-attributes
   (make-xml-element
    'Body namespace-soap-envelope '()
    (make-soap-sender-fault name title message msgtxt))))

(define (make-soap-receiver-error name title message msgtxt)
  (make-xml-element
   'Envelope namespace-soap-envelope soap-error-attributes
   (make-xml-element
    'Body namespace-soap-envelope '()
    (make-soap-receiver-fault name title message msgtxt))))
