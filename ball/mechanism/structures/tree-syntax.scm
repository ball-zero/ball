;; (C) 2013 Jörg F. Wittenberger; see askemos.org

;; This need fixing.  Under alexpander direct application yields the
;; to-be-applied lambda, not the application.  Work around for now.

(define-syntax lambda-transformer
  (syntax-rules ()
    ((_ errloc body)
     (begin
       ;; (: name :render-renderer:)
       (lambda (place			; only for Askemos
		message			; only for Askemos
		root-node0
		variables		; xsl binding environment for
					; parameters and variables
		namespaces0
		ancestors0		; ancestor context
		self-node		; current transformer input
		nl			; current sosofo
		mode-choice0		; 1-ari proc, selects
					; continuation mode, #f
					; selects default mode, symbol
					; selects named mode, string
					; selects named template
		proc-chain		; next processing instruction (to
					; be remove)
		)
	 (let-syntax
	     ((rewrite
	       (syntax-rules ()
		 ((_ (*place
		      *message
		      *root-node
		      *environment
		      *namespaces
		      *ancestors
		      *$
		      *$<
		      *@?
		      *@!
		      *transform
		      *lambda-transformer
		      *apply-transformer
		      *current-node
		      *sosofo
		      *mode-choice
		      *%%proc-chain
		      )
		     body_)
		  (letrec-syntax
		      (
		       (*place (syntax-rules ($$)
				 ((_ $$) (place 'get 'id))
				 ((_) place)))
		       (*message (syntax-rules (format-location)
				   ((_) message)
				   ((_ format-location mode)
				    ((if (eq? mode 'get) read-locator write-locator)
				     (message 'location-format)
				     (message 'location)))
				   ((_ format-location mode tail)
				    ((if (eq? mode 'get) read-locator write-locator)
				     (message 'location-format)
				     tail))
				   ))
		       (proc-chlds
			(syntax-rules ()
			  ((_ root-node_ variables_ namespaces_ ancestors_
			      self-node_ nl_ mode-choice_ proc-chain_)
			   (proc-chain_
			    place message root-node_ variables_ namespaces_ ancestors_
			    self-node_ nl_ mode-choice_))))
		       (apply-transformer_
			(syntax-rules ()
			  ((_ transformer_ root-node_ variables_ namespaces_ ancestors_
			      self-node_ nl_ mode-choice_ proc-chain_)
			   (transformer_
			    place message root-node_ variables_ namespaces_ ancestors_
			    self-node_ nl_ mode-choice_ proc-chain_))))
		       (*mode-choice (syntax-rules ()
				       ((_ key) (mode-choice0 key))))
		       (*lambda-transformer
			(syntax-rules ()
			  ((_ errloc_1 body_1)
			   (let ()
			     (define-transformer local errloc_1 body_1)
			     local))
			  ((_ body_1)
			   (let ()
			     (*lambda-transformer "local-transformer" body_1)))))
		       (*$ (syntax-rules ()
			     ((_ name)
			      (fetch-xsl-variable/string name (xsl-environment-variables variables) errloc))))
		       (*$< (syntax-rules ()
			     ((_ name)
			      (fetch-xsl-variable/symbol name (xsl-parameters variables) errloc))))
		       (*@? (syntax-rules ()
			      ((_ name) (attribute-string name nl))))
		       (*@! (syntax-rules ()
			      ((_ name) (required-attribute-string name nl errloc))))
		       
		       (*ancestors (syntax-rules () ((_) ancestors0)))
		       (*namespaces (syntax-rules () ((_) namespaces0)))
		       (*environment (syntax-rules () ((_) variables)))
		       (*root-node (syntax-rules () ((_) root-node0)))
		       (*current-node (syntax-rules () ((_) self-node)))
		       (*sosofo (syntax-rules () ((_) nl)))
		       (*%%proc-chain (syntax-rules () ((_) proc-chain)))
		       )
		    (let-keyword-syntax
		     ((*transform
		       (proc-chlds
			(root-node: root-node0)
			(variables: variables)
			(namespaces: namespaces0)
			(ancestors: ancestors0)
			(current-node: self-node)
			(sosofos: nl)
			(continue: mode-choice0)
			(process: proc-chain)))
		      (*apply-transformer
		       (apply-transformer_
			(use:)
			(root-node: root-node0)
			(variables: variables)
			(namespaces: namespaces0)
			(ancestors: ancestors0)
			(current-node: self-node)
			(sosofos: nl)
			(continue: mode-choice0)
			(process: proc-chain)))
		      )
		     body_))))))
	   (extract
	    (
	     current-place
	     current-message
	     root-node
	     environment
	     namespaces
	     ancestors
	     $				; resolve variable to value
	     $<
	     @?				; optional attribute as string or #f
	     @!				; required attribute as string
	     transform			; process children
	     cc/lambda-transformer
	     apply-transformer
	     current-node
	     sosofo
	     mode-choice
	     %%proc-chain		; current transformation; to be abstracted away
	     )
	    body
	    (rewrite () body))))))))

#;(define-syntax lambda-process
  (syntax-rules ()
    ((_ loc (proc-chain) body ...)
     (lambda
	 ;; Parameters must match lambda-transfomer except for the
	 ;; proc-chain, which comes from the outer lambda list.
	 (place
	  message
	  root-node
	  variables
	  namespaces
	  ancestors
	  self-node
	  nl
	  mode-choice
	  )
       ((lambda-transformer loc (begin body ...))
	place
	message
	root-node
	variables
	namespaces
	ancestors
	self-node
	nl
	mode-choice
	proc-chain
	)
       ))
    ((_ loc () body ...)
     (lambda
	 ;; Parameters must match lambda-transfomer except for the
	 ;; proc-chain, which comes from the outer lambda list.
	 (place
	  message
	  root-node
	  variables
	  namespaces
	  ancestors
	  self-node
	  nl
	  mode-choice
	  )
       (lambda (proc-chain)
	 ((lambda-transformer loc (begin body ...))
	  place
	  message
	  root-node
	  variables
	  namespaces
	  ancestors
	  self-node
	  nl
	  mode-choice
	  proc-chain
	  ))
       ))))

(define-syntax lambda-process
  (syntax-rules (CASE1)
    ((_ loc (captured-proc-chain) body ...)
     (let ((transformer (lambda-transformer loc (begin body ...))))
       (lambda
	   ;; Parameters must match lambda-transfomer except for the
	   ;; proc-chain, which comes from the outer lambda list.
	   (place
	    message
	    root-node
	    variables
	    namespaces
	    ancestors
	    self-node
	    nl
	    mode-choice
	    )
	 (transformer
	  place
	  message
	  root-node
	  variables
	  namespaces
	  ancestors
	  self-node
	  nl
	  mode-choice
	  captured-proc-chain
	  )
	 )))
    ((_ loc () body ...)
     (let ((transformer (lambda-transformer loc (begin body ...))))
       (lambda
	   (place
	    message
	    root-node
	    variables
	    namespaces
	    ancestors
	    self-node
	    nl
	    mode-choice
	    )
	 (lambda (free-proc-chain)
	   (transformer
	    place
	    message
	    root-node
	    variables
	    namespaces
	    ancestors
	    self-node
	    nl
	    mode-choice
	    free-proc-chain
	    )))))
    ((_ loc CASE1 (captured-proc-chain) body ...)
     (lambda (captured-proc-chain)
       (lambda
	   ;; Parameters must match lambda-transfomer except for the
	   ;; proc-chain, which comes from the outer lambda list.
	   (place
	    message
	    root-node
	    variables
	    namespaces
	    ancestors
	    self-node
	    nl
	    mode-choice
	    )
	 (let ((t (lambda-transformer loc (begin body ...))))
	   (t
	    place
	    message
	    root-node
	    variables
	    namespaces
	    ancestors
	    self-node
	    nl
	    mode-choice
	    captured-proc-chain
	    ))
	 )))))

(define-syntax define-transformer
  (syntax-rules ()
    ((_ transformer errloc body)
     (begin
       ;; (: name :render-renderer:)
       (define transformer (lambda-transformer errloc body))))))
