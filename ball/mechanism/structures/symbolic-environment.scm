;; (C) 2011, 2013 Jörg F. Wittenberger, GPL, see http://www.askemos.org

;; all too often needed...

;; SRFI-44 mandates at least both: fold-left and fold-right
;;
;; There is not intention to implement full SRFI-44 here,
;; the code is *supposed* to be used (aka optimisable) as an
;; "environment" in recusive evaluations which themself are
;; implemented in a 'pure' (i.e., side effect free) way.
;;
;; srfi-44 is too contradictory to srfi-1; but it's much more logical;
;; let's try to cater both for a while

(define (rough-error msg . args)
  (error (apply format msg args)))

(cond-expand
 (chicken (begin))
 (else
  (define-record-type <binding-node>
    (make-binding-node color left right name value)
    binding-node?
    (color binding-node-color binding-node-color-set!)
    (left binding-node-left binding-node-left-set!)
    (right binding-node-right binding-node-right-set!)
    (name binding-node-name binding-node-name-set!)
    (value binding-node-value binding-node-value-set!))
  (define %binding-node-name binding-node-name)
  (define %binding-node-value binding-node-value)
  (define-syntax %symbol->string (syntax-rules () ((_ x) (symbol->string x))))
  (define-syntax %string<? (syntax-rules () ((_ a b) (string<? a b))))

 ;;; ) ;;;; moved into it's own file - but not backward compatible

;; (define-syntax binding-set-update
;;   (syntax-rules ()
;;     ((_ n . more)
;;      (letrec-syntax
;; 	 ((doit (syntax-rules (left: right: color:)
;; 		  ((_ n l r c ())
;; 		   (make-binding-node c l r (binding-node-name n) (binding-node-value n)))
;; 		  ((_ n l r c (left: v . more))
;; 		   (doit n v r c more))
;; 		  ((_ n l r c (right: v . more))
;; 		   (doit n l v c more))
;; 		  ((_ n l r c (color: v . more))
;; 		   (doit n l r v more))
;; 		  )))
;;        (doit n (binding-node-left n) (binding-node-right n) (binding-node-color n) more)))))

(define-syntax binding-node-update
  (syntax-rules (left: right: color:)
    ((_ 1 n l r c ())
     (make-binding-node c l r (binding-node-name n) (binding-node-value n)))
    ((_ 1 n l r c (left: v . more))
     (binding-node-update 1 n v r c more))
    ((_ 1 n l r c (right: v . more))
     (binding-node-update 1 n l v c more))
    ((_ 1 n l r c (color: v . more))
     (binding-node-update 1 n l r v more))
    ((_ n . more)
     (binding-node-update
      1 n (binding-node-left n) (binding-node-right n) (binding-node-color n) more))))

(define-syntax binding-k-n-eq?
  (syntax-rules () ((_ k n) (eq? k (binding-node-name n)))))

(define-syntax binding-n-n-eq?
  (syntax-rules () ((_ n1 n2) (eq? (binding-node-name n1) (binding-node-name n2)))))

(define-syntax binding-k-n-lt
  (syntax-rules () ((_ k n) (%string<? k (%binding-node-name n)))))

(define-syntax binding-n-n-lt
  (syntax-rules () ((_ node1 node2) (%string<? (%binding-node-name node1)
					       (%binding-node-name node2)))))

(define-llrbtree/positional
  (ordered pure)
  binding-node-update
  binding-set-init!    ;; defined
  binding-set-lookup   ;; defined
  #f			   ;; no min defined
  %binding-set-fold	   ;; defined
  #f			   ;; no for-each defined
  %binding-set-insert   ;; defined
  binding-set-delete   ;; defined
  #f			   ;; no delete-min defined
  binding-set-empty?   ;; defined
  binding-k-n-eq?
  binding-n-n-eq?
  binding-k-n-lt
  binding-n-n-lt
  binding-node-left
  binding-node-right
  binding-node-color
  )

;; Constructors

;; 0X0

(define (%make-new-binding-node k v)	; internal/unclean
  (make-binding-node #f #f #f k v))

(define %empty-binding-set		; internal
  (binding-set-init! (make-binding-node #f #f #f #f #f)))

(define (empty-binding-set) %empty-binding-set)	; export

;; 0Xpairs

(define (make-binding-set . lst)	; export
  (if (null? lst)
      (empty-binding-set)
      (do ((lst lst (cdr lst))
	   (set %empty-binding-set
		(let ((x (car lst)))
		  (%binding-set-insert
		   set (%make-new-binding-node (%symbol->string (car x)) (cdr x))))))
	  ((null? lst) set))))

(define (%binding-set-ref/thunk envt k thunk) ; export?
  (cond
   ((null? envt) (thunk))
   ((binding-node? envt)
    (let ((entry (binding-set-lookup envt k)))
      (if entry (binding-node-value entry) (thunk))))
   (else (rough-error "binding-set-ref/thunk (key ~a) envt invalid: ~a" k envt))))

(define (%binding-set-ref/default envt k default) ; internal
  (cond
   ((null? envt) default)
   ((binding-node? envt)
    (let ((entry (binding-set-lookup envt k)))
      (if entry (binding-node-value entry) default)))
   (else (rough-error "binding-set-ref/default (key ~a) envt invalid: ~a" k envt))))

(: binding-set-ref/default ((struct <binding-node>) symbol * -> *))
(define (binding-set-ref/default envt k default) ; export
  ;;(or (symbol? k) (rough-error "binding-set-ref/default key invalid: ~a" k))
  (%binding-set-ref/default envt (%symbol->string k) default))

;; This generic case is probably (hopefuly) not very useful.
;;(: binding-set-ref ((struct <binding-node>) symbol #!rest (procedure () . *) -> *))
(define (binding-set-ref envt k . thunk) ; export
;;  (or (symbol? k) (rough-error "binding-set-ref key invalid: ~a" k))
  (%binding-set-ref/thunk
   envt (%symbol->string k)
   (if (pair? thunk) (car thunk)
       (lambda ()
	 (rough-error "binding-set-ref \"~a\" unbound" k)))))

;; setXkeyXvalue

(: binding-set-insert ((struct <binding-node>) symbol * -> (struct <binding-node>)))
(define (binding-set-insert nodeset k v) ; export
;;  (or (symbol? k) (rough-error "binding-set-insert key invalid ~a" k))
  (let ((k (%symbol->string k)))
    (cond
     ((null? nodeset)
      (%binding-set-insert
       %empty-binding-set (%make-new-binding-node k v)))
     ((binding-node? nodeset)
      (%binding-set-insert nodeset (%make-new-binding-node k v)))
     (else (rough-error "binding-set-insert nodeset (key ~a) invalid in ~a" k nodeset)))))

;; srfi-1::alist-cons compatible
(define (binding-set-cons k v nodeset) ; export
  (binding-set-insert nodeset k v))

(: binding-set-fold ((struct <binding-node>) (procedure (* *) . *) * -> *))
(define (binding-set-fold nodeset kvcons nil)
  (or (procedure? kvcons) (rough-error "binding-set-fold kons not a procedure"))
  (if (null? nodeset) nil (%binding-set-fold (lambda (e i) (kvcons (string->symbol (binding-node-name e)) (binding-node-value e) i)) nil nodeset)))

;; setXset

(: binding-set-union
   ((struct <binding-node>) (struct <binding-node>) --> (struct <binding-node>)))
(define (binding-set-union inner outer) ; export
  ;; (wt-tree/union outer inner)
  (cond
   ((null? inner) outer)
   ((null? outer) inner)
   (else
    (or (binding-node? inner) (rough-error "binding-set-union inner invalid ~a" inner))
    (or (binding-node? outer) (rough-error "binding-set-union outer invalid ~a" outer))
    (%binding-set-fold
     (lambda (node init)
       (%binding-set-insert init node))
     outer
     inner))))

(define (binding-set-update nodeset k update dflt) ; export
  (checkbinding-node nodeset 'binding-set-update)
  (ensure symbol? k)
  (ensure procedure? update)
  (ensure procedure? dflt)
  (let ((k (%symbol->string k)))
    (%binding-set-insert
     nodeset k #f
     (lambda (n)
       (let ((v (update (binding-node-value n))))
	 (make-binding-node #f #f #f (%binding-node-name n) v)))
     (lambda () (%make-new-binding-node k (dflt))))))

)) ;;; end of "cond-expand" from near the top


;; namespaced environments (2nd level)

;; WARNING the 2nd level us UNTESTED.  Just here because I'd like to
;; have it.  Thus to be fixed instead to be removed.

(define-record-type <symbolic-environment>
  (%make-symbolic-environment default named)
  %symbolic-environment?
  (default symbolic-environment-default)
  (named symbolic-environment-named))

(define (make-symbolic-environment . rest)
  (%make-symbolic-environment
   (apply make-binding-set rest)
   (make-binding-set)))

(define (symbol-environment-ref/default envt ns nm default)
  ;; (or (symbol? nm) (rough-error "symbol-environment-ref/default key invalid: ~a" nm))
  (cond
   ((%symbolic-environment? envt)
    (if ns
	(let ((nmsp (binding-set-ref/default
		     (symbolic-environment-named envt)
		     ns #f)))
	  (if nmsp
	      (binding-set-ref/default nmsp nm default)
	      default))
	(binding-set-ref/default (symbolic-environment-default envt) nm default)))
   (else ;; (binding-node? envt)
    (binding-set-ref/default envt nm default))
   ;; (else (rough-error "symbol-environment-ref: envt invalid ns ~a nm ~a: ~a" ns nm envt))
   ))

(define (symbol-environment-ref envt ns nm . thunk)
  (define (use-thunk)
    (if (and (pair? thunk) (procedure? (car thunk)))
	((car thunk))
	(rough-error "symbol-environment-ref thunk invalid ~a" thunk)))
  (or (symbol? nm) (rough-error "symbol-environment-ref key invalid ~a nm"))
  (cond
   ((null? envt) (use-thunk))
   ((%symbolic-environment? envt)
    (if ns
	(let ((nmsp (binding-set-ref/default
		     (symbolic-environment-named envt)
		     ns #f)))
	  (if nmsp
	      (apply binding-set-ref nmsp nm thunk)
	      (use-thunk)))
	(if (null? thunk)
	    (binding-set-ref (symbolic-environment-default envt) nm)
	    (binding-set-ref (symbolic-environment-default envt) nm (car thunk)))))
   (else ;; (binding-node? envt)
    (if (null? thunk) (binding-set-ref envt nm) (binding-set-ref envt nm (car thunk))))
   ;;(else (error 'symbol-environment-ref:envt-type))
   ))

(: symbol-environment-insert
   (;; Intented is only: (struct <symbolic-environment>)
    (or (struct <symbolic-environment>) (struct <binding-node>) null)
    (or false symbol) symbol * -> (struct <symbolic-environment>)))
(define (symbol-environment-insert envt ns nm v)
  (cond
     ((%symbolic-environment? envt)
      (if ns
	  (%make-symbolic-environment
	   (symbolic-environment-default envt)
	   (binding-set-update
	    (symbolic-environment-named envt)
	    ns
	    (lambda (set)
	      (binding-set-insert set nm v))
	    empty-binding-set))
	  (%make-symbolic-environment
	   (binding-set-insert
	    (symbolic-environment-default envt) nm v)
	   (symbolic-environment-named envt))))
     (else ;; (or (null? envt) (binding-node? envt))
      (if ns
	  (%make-symbolic-environment
	   envt
	   (binding-set-insert
	    %empty-binding-set
	    ns
	    (make-binding-set (cons nm v))))
	  (binding-set-insert (if (null? envt) %empty-binding-set envt) nm v)))
     ;;(else (error (format "not an symbolic environment ~a" envt)))
     ))
