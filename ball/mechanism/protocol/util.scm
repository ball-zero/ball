;; (C) 2000-2003, 2013 Jörg F. Wittenberger see http://www.askemos.org

;;; Blocktable Accumulator

(: make-blocktable-accumulator
   (:oid: :quorum: fixnum fixnum
    -->
    (procedure (* :block-table:) :block-table:)
    :block-table:
    (procedure (:block-table:) :blob: fixnum (or false string))))
(define make-blocktable-accumulator
  (let ()
    (define (cbwrite t from n at)
      (define bs (block-table-bs t))
      (define blkno (quotient at bs))
      (define offset (modulo at bs))
      (define fb (min (fx- bs offset) n))
      (if (fx< 0 offset)
	  (let ((to (make-string fb #\x0)))
	    (move-memory! from to fb 0 0)
	    (block-table-update! t at to)
	    (set! blkno (add1 blkno))
	    (set! offset fb)
	    (set! at (fx+ at fb))
	    (set! n (fx- n fb))))
      (let loop ((n n) (at at) (foff offset))
	(if (eqv? n 0)
	    'SQLITE_OK
	    (let* ((cbs (min n bs))
		   (to (if (and (eqv? n cbs) (eqv? foff 0))
			   from
			   (let ((to (make-string/uninit cbs)))
			     (move-memory! from to cbs foff 0)
			     to))))
	      (block-table-update! t at to)
	      (loop (fx- n cbs) (fx+ at cbs) (fx+ foff cbs))))))
    (define (make-blocktable-accumulator oid quorum bs bpb)
      (let ((last (xthe (or false :block-table:) #f))
	    (btbl (place/make-block-table oid quorum #f bs bpb))
	    (last-size 0))
	(define (add-chunk chunk checkv)
	  (assert (eq? checkv btbl))
	  (cbwrite btbl chunk (string-length chunk) (block-table-size btbl))
	  (cond-expand
	   (chicken (chicken-check-interrupts!))
	   (else ))
	  (if (>= (block-table-size btbl)
		  (+ (* 9 bs) last-size))
	      (receive
	       (blob ins del) (block-table-flush! btbl last)
	       (set! last (place/make-block-table oid quorum blob bs bpb))
	       (set! last-size (block-table-size last))))
	  btbl)
	(define (reduce-chunks checkv)
	  (assert (eq? checkv btbl))
	  (let ((fb (block-table-resolve/default btbl 0 #f)))
	    (receive
	     (blob ins del) (block-table-flush! btbl last)
	     (values blob (a:blob-size blob) fb))))
	(values add-chunk btbl reduce-chunks)))
    make-blocktable-accumulator))

#|
(define (http-display-blob-notation store blob key ranges display)
  (let* ((offset (if ranges (caar ranges) 0))
	 (limit (if ranges
		    (- (add1 (cdar ranges)) offset)
		    (a:blob-size blob)))
	 (t (place/make-block-table
	     (lambda (s . r)
	       (cond
		((eq? s 'replicates) *local-quorum*)
		(else (public-oid))))
	     blob))
	 (buf (make-string/uninit (min limit #x3ffff))))
    (let loop ((len limit)
	       (offset offset))
      (if (> len 0)
	  (let ((s (http-cbread store t buf (min len (string-length buf)) offset)))
	    (if s
		(begin
		  (display buf)
		  (loop (- len (string-length buf))
			(+ offset (string-length buf))))))))))
|#

(define $immediate-body-size 500)

;; timeout parameter / configurable in bopcntrl and setup
;;
;; The opposite of good is well intented.  Someone has torn apart the
;; logic originally put into those timeout being computed from a few
;; parameters into a busload of independant parameters.  I regret
;; since (and it was not even me).

(define http-read-part-min-timeout 15)

;; timeout for each chunk to be written
; currently respond-timeout-interval
(: write-chunk-timeout (string --> fixnum))
(define (write-chunk-timeout str)
  (fx+ http-read-part-min-timeout
       (quotient (string-length str) ($http-bandwidth))))

;; timeout used to write a plain body
; currently remote-fetch-timeout-interval
(define write-plain-timeout (make-shared-parameter #f))

;; connection keep alive timeout (http-server)
(define http-keep-alive-timeout (make-shared-parameter #f))

;; timeout to get a request line (http-server)
; currently http-keep-alive-timeout
(define (http-read-request-line-timeout . ignored) (http-keep-alive-timeout))

;; timeout to get a status line (http-client)
; currently remote-fetch-timeout-interval
(define http-read-status-line-timeout (make-shared-parameter #f))

;; timeout to get all request header
; currently short-local-timeout
(define (http-read-header-timeout . ignored)
  http-read-part-min-timeout)

;; timeout between two chunks
; currently http-keep-alive-timeout, should be no more than 2 seconds
(define http-read-chunked-header-timeout
  (make-shared-parameter #f))

;; timeout to get all trailing header
; currently http-keep-alive-timeout
(define http-read-trailer-timeout
  (make-shared-parameter #f))

;; timeout to get a request body w/o a content-length header
; currently respond-timeout-interval
(define http-read-to-end-timeout
  (make-shared-parameter #f))

;; timeout to get the next free channel, a channel becomes free after a 
;; http-send gets a response or $http-max-retries times timeouts in rd-response
; currently remote-fetch-timeout-interval
(define http-with-channel-timeout
  (make-shared-parameter 60))

(define short-local-timeout (make-shared-parameter #f))

(define ssl-setup-timeout (make-shared-parameter 20))

(define nameserver (dns-find-nameserver))

(define (ip/name->ip what)
  (if (ip-address/port? what) what
      (dns-get-address nameserver what)))

(define default-silent-actions
  '("replicate" "echo" "ready" "get-state"
    "reply-notice" "get-reply"
    "digest" "get-place" "get-meta" "get-body" "get-blob"
    "Hello" "X509Certificate" "X509CACertificate"))

(define (default-log-access
          host client user date method location protocol state size)
  (if (or $agree-verbose (not (member method default-silent-actions)))
      (let ((port (current-output-port)))
	(display
	 ;; FIXME: the mangling and sorting ougth to go into scheme
	 ;; implemenation specific code sections.
	 (let ((a (cond-expand
		   (rscheme
		    (if (instance? client <inet-socket-addr>)
			(receive (a p) (inet-socket-addr-parts client)
				 (inet-addr->string a))
			(inet-addr->string client)))
		   (else client))))
	   (guard
	    ;; FIXME: the dns-get-name can raise errors for
	    ;; valid clients.  That should not happen.
	    (ex (else a))
	    (or (dns-get-name nameserver a) a)))
	 port)
	(display " - " port)
	(display (if user (entry-name user) #\-) port)
	(display (srfi19:date->string date " [~d/~b/~Y:~H:~M:~S ~z] \"") port)
	(display method port)
	(display #\space port)
	(display (uri-path location) port)
	(if (uri-query location)
	    (begin
	      (display #\? port)
	      (display (uri-query location) port)))
	(display #\space port)
	(display protocol port)
	(display "\" " port)
	(display state port)
	(display #\space port)
	(display size port)
	(newline port)
	(flush-output-port port)
	)))

(define log-access (make-shared-parameter default-log-access))

(define (log-access! . args) (apply logit (log-access) args))

(define (default-log-remote
          host client user date method location protocol state size)
  ;; This is NYI; just the stub
  #f)

(define $log-remote (make-shared-parameter default-log-remote))
(define (log-remote! . args) (apply logit ($log-remote) args))

(define (expect-object timeout escape test reader . args)
  (let ((obj (within-time%1 timeout (apply reader args))))
    (if (test obj) obj (escape obj))))

;; A slot handling record is
;; #( slotname::symbol header:string reader::proc writer::proc )

;; reader ist a procedure of one string argument,which converts this
;; string into the actual value

;; writer is a procedure (lambda (value port)), which writes the value
;; to port in a way which can be read by reader.

;; slotname is the internal slot name, header is the external used
;; string value

(: http-register-handler
   (symbol string (procedure (string) *) (procedure (* output-port) undefined)
    &optional * -> undefined))
(define (http-register-handler slot header reader writer . flags)
  (if (hash-table-ref/default *http-slot-handlers-by-slot* slot #f)
      (error "Handler for ~a already registered as ~a"
             slot (hash-table-ref/default *http-slot-handlers-by-slot* slot #f)))
  (let ((v (vector slot header reader writer (and (pair? flags) (car flags)))))
    (hash-table-set! *http-slot-handlers-by-slot* slot v)
    (hash-table-set! *http-slot-handlers-by-header* (string-downcase header) v)))

(define *agreement-message-handlers* (make-symbol-table))

(: register-agreement-handler! (symbol :aggrement-handler: -> undefined))
(define (register-agreement-handler! id handler)
  (hash-table-set! *agreement-message-handlers* id handler))

(: call-agreement-handler (:oid: :message: symbol -> :message:))
(define (call-agreement-handler oid request action)
  (let ((handler (hash-table-ref/default *agreement-message-handlers* action #f)))
    (if (procedure? handler)
	(handler oid request)
	(make-message
	 (property 'http-status 405)
	 (property 'mind-body action)))))

(: read-from-string (string --> *))
(define (read-from-string str) (call-with-input-string str read))

(define (http-read-date str)
  ;; would be better if we would match by regex as the rscheme time
  ;; reader does.  This is "Python style".  Argh.
  (guard
   (ex (else (guard
	      (ex (else (guard
			 ;; last resort ANSI C' asctime
			 (ex (else
			      (guard
			       ;; KLUDGE: We MUST not fail here to
			       ;; avoid strange failure modes for the
			       ;; user.  However defaulting to #f is dangerous.
			       (ex (else #f))
			       (srfi19:string->date str "~a ~b  ~d ~H:~M:~S ~Y"))))
			 ;; next try RFC 850
			 (srfi19:string->date str "~A, ~d-~b-~Y ~H:~M:~S ~z"))))
	      ;; first try RFC 822
	      (srfi19:string->date str "~a, ~d ~b ~Y ~H:~M:~S ~z"))))
   ;; before anything, try our own
   (srfi19:string->date str "~a, ~d ~b ~Y ~H:~M:~S.~N ~z")))

;; (define (http-write-date date port)
;;   (display (rfc-822-timestring date) port))

(define (http-write-date date port)
  (display (srfi19:date->string date "~a, ~d ~b ~Y ~H:~M:~S.~N ~z") port))

(define (http-read-capabilities str)
  (map string->right (read-from-string str)))

(define (http-write-capabilities capas port)
  (write (map right->string capas) port))

(define (http-format-location-path uri-quote lst port)
  (define (fmt-rest rest)
    (if (pair? (cdr rest)) (fmt-rest (cdr rest)))
    (display #\/ port)
    (display (uri-quote (literal (car rest))) port))
  (cond
   ((null? lst) (display #\/ port))
   ((pair? lst) (fmt-rest lst))
   (else (display #\/ port) (display lst port))))

(define (http-format-destination-path uri-quote lst port)
  (define (fmt-rest rest)
    (display #\/ port)
    (display (uri-quote (literal (car rest))) port)
    (if (pair? (cdr rest)) (fmt-rest (cdr rest))))
  (cond
   ((null? lst) (display #\/ port))
   ((pair? lst) (fmt-rest lst))
   (else (display #\/ port) (display lst port))))

(: write-query-form (* output-port -> undefined))
(define (write-query-form form port)
  (let* ((name (and (gi form) (not (eq? (gi form) 'form))
                    (begin (display "*name*=" port)
                           (display 
			    (form-quote (symbol->string (gi form))) port)
                           #t)))
         (be (or
              (and (ns form)
                   (begin
                     (if name (display #\& port))
                     (display "*xmlns*=" port)
                     (display
		      (form-quote (symbol->string (ns form))) port)
                     #t))
              name)))
    (let loop ((be be) (nl (children form)))
      (if (not (node-list-empty? nl))
          (let ((n (node-list-first nl)))
            (if (xml-element? n)
                (begin
                  (if be (display #\& port))
                  (display (form-quote (symbol->string (gi n))) port)
                  (display #\= port)
                  (display (form-quote (data n)) port)))
            (loop #t (node-list-rest nl)))))))

(define (http-format-location uri-quote lst port)
  (let loop ((lst lst) (body #f))
    (if (and (pair? lst) (eq? (car lst) body:))
        (if (null? (cdr lst))
            (error "format-location: \"body:\" requires argument")
            (loop (cddr lst) (cadr lst)))
        (begin
          (http-format-location-path uri-quote lst port)
          (if body
	      (cond
	       ((string? body)
		(if (not (eqv? (string-length body) 0))
		    (begin
		      (display #\? port)
		      (display body port))))
	       ((not (node-list-empty? (children body)))
		(display #\? port)
		(write-query-form body port))))))))

(define (http-write-location location port)
  (if (string? location)
      (display location port)
      (http-format-location uri-quote-all location port)))

(define (http-read-location str)
  (if (string-index str #\?) str (reverse! (parsed-locator str))))

(define (http-write-destination destination port)
  (http-format-location uri-quote-all (reverse destination) port))

(define (http-read-destination str)
  (parsed-locator str))

(%early-once-only

 (define *http-slot-handlers-by-slot* (make-symbol-table))
 (define *http-slot-handlers-by-header* (make-string-table))

 (http-register-handler 'dc-creator "X-DC-Creator" read-from-string write)
 (http-register-handler 'dc-date    "Date" http-read-date http-write-date)
 (http-register-handler
  'mind-action-document "X-Askemos-Action" read-from-string write)
 (http-register-handler
  'capabilities "X-Askemos-Capabilities"
  http-read-capabilities http-write-capabilities)
 ;; (http-register-handler
 ;;  'content-type "Content-Type" identity display)
 ;; (http-register-handler
 ;;  'accept "Accept" identity display)
;; (http-register-handler
;;  'content-length "Content-Length" string->number display)
 (http-register-handler
  'destination "X-Askemos-Destination"
  http-read-destination http-write-destination)
  ;;parsed-locator http-format-destination-path)
 (http-register-handler
  'location "X-Askemos-Location" http-read-location http-write-location)
  ;;(lambda (path) (reverse (parsed-locator)))  http-format-location-path)
 (http-register-handler
  'location-format "X-Askemos-Location-Format" read-from-string write)
 (http-register-handler
  'caller "X-Askemos-Caller" read-from-string write)

 (http-register-handler
  'version "X-Askemos-version" read-from-string write)
 (http-register-handler
 'replicates "X-Askemos-replicates" read-from-string write)

) ; %early-once-only

;; http-read-header reads all RFC822-Headers of a request into a
;; frame.

;; (define (rfc822-update-header head body tluser)
;;   (let ((old (assq head tluser)))              
;;     (if old
;;         (let loop ((keep tluser))
;;           (if (eq? (car keep) old)
;;               (cons (cons head (string-append (cdr old) ", " body))
;;                     (cdr keep))
;;               (cons (car keep) (loop (cdr keep)))))
;;         (cons (cons head body) tluser))))          

(define (rfc822-update-header! head body tluser)
  (let loop ((keep tluser))
    (cond
     ((null? keep)
      (cons (cons head body) tluser))
     ((eq? head (caar keep))
      (set-cdr! (car keep) (string-append (cdar keep) ", " body))
      tluser)
     (else (loop (cdr keep))))))

(define rfc822-update-header+ rfc822-update-header!)

(define (rfc822-read-header-from-port port)
  (let loop ((line (read-line port)) (tluser '()))
    (receive (next head body) (read-one-header line port)
             (if next
                 (loop next (rfc822-update-header+ head body tluser))
                 ;; just consed up here.
                 (if head
                     (reverse! (rfc822-update-header+ head body tluser))
                     '())))))

(define (read-one-header line input)
  (cond
   ((eof-object? line) (values #f #f #f))
   ((or (eqv? (string-length line) 0)
        (and (eqv? (string-length line) 1)
             (eqv? (string-ref line 0) (integer->char 13))))
    (values #f #f #f))
   ((string-match colon-split-regex line) =>
    (lambda (match)
      (let ((tag (string-downcase! (cadr match))))
        (let loop ((line (read-line input))
                   (senil (cddr match)))
          (cond
           ((eof-object? line)
	    (raise (make-condition
		    &message 'message "premature end of message header")))
           ((or (eqv? (string-length line) 0)
                (and (eqv? (string-length line) 1)
                     (eqv? (string-ref line 0) (integer->char 13))))
            (values #f tag (apply-string-append (reverse! senil))))
           ((memv (string-ref line 0) '(#\space #\tab))
            (loop (read-line input) (cons line senil)))
           (else
            (values line tag (apply-string-append (reverse! senil)))))))))
   (else (raise
	  (make-condition
	   &message 'message (string-append "illegal header line: " line))))))

;; TODO: the unmarshalling could really save a few conses.

(define (unmarshall-header0 hdr env)
  (let ((r (hash-table-ref/default *http-slot-handlers-by-header* (car hdr) #f)))
    (if r
	(case (vector-ref r 4)
	  ((#f) (set-slot! env (vector-ref r 0) ((vector-ref r 2) (cdr hdr))))
	  ((list)
	   ;; FIXME: this should be coded better.
	   (let ((val (get-slot env (vector-ref r 0))))
	     (set-slot! env (vector-ref r 0)
			(cons
			 ((vector-ref r 2) (cdr hdr))
			 (or val '())))))
	  (else (error "unmarshall-header unhandled header type")))
	(set-slot! env (string->symbol (car hdr)) (cdr hdr)))
    env))

(define unmarshall-header unmarshall-header0)

(define (unmarshall-header-loged hdr env)
  (logerr "~a: ~a\n" (car hdr) (cdr hdr))
  (unmarshall-header0 hdr env))

(define (http-read-header port)
  (fold
   unmarshall-header
   (make-message)
   (rfc822-read-header-from-port port)))

(: http-read-trailers! (:message: input-port -> :message:))
(define (http-read-trailers! request port)
  (fold unmarshall-header
	request
	(rfc822-read-header-from-port port)))

(define (http-write-header00 tag wr value port)
  (display tag port)
  (display ": " port)
  (wr value port)
  ;; (newline port) ; RFC2616 19.3 would allow to improve to
  ;; use terminate lines with just LF, but M$ is, as too
  ;; often, stupid enough not to see the light.
  (display "\r\n" port))

(define (http-write-header0 slot value port)
  (let ((r (hash-table-ref/default *http-slot-handlers-by-slot* slot #f)))
    (if (vector? r)
        (let ((tag (vector-ref r 1)) (wr (vector-ref r 3)))
	  (case (vector-ref r 4)
	    ((#f) (http-write-header00 tag wr value port))
	    ((list) (for-each
		     (lambda (value)
		       (http-write-header00 tag wr value port))
		     value))
	    (else (error "unhandled header type")))))))

(: http-write-header (symbol * output-port -> undefined))
(define http-write-header http-write-header0)

(define (http-write-header-loged slot value port)
  (let ((x (call-with-output-string
            (lambda (port) (http-write-header0 slot value port)))))
    (display x (current-error-port))
    (display x port)))

(define (http-log-header . flag)
  (let ((flag (or (null? flag) (car flag))))
    (if flag
        (begin
	  (set! http-write-header http-write-header-loged)
	  (set! unmarshall-header unmarshall-header-loged))
        (begin
	  (set! http-write-header http-write-header0)
	  (set! unmarshall-header unmarshall-header0)))
    flag))

(define (http-write-headers headers response port . defaults) 
  (let ((defaults (or (and (pair? defaults) (car defaults)) (lambda (x) #f))))
    (for-each
     (lambda (header)
       (let ((v (or (get-slot response header)
                    (defaults header))))
         (if v (http-write-header header v port))))
     headers)))

;; http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
;; The "chunked" transfer-coding is always acceptable.

(define (large-message-size) (fx- $buffer-size 100))

(define (use-chunked-encoding? protocol msg msg-size)
  (and msg-size (fx>= msg-size (large-message-size))
       protocol (not (string=? protocol "HTTP/1.0"))))

(define $http-max-write-length (make-shared-parameter 4096))

(define (http-make-display port)
  (cond-expand
   (chicken (define-inline (display str port) (http-make-display-display str port)))
   (else ))
  (define (display-step s)
    (within-time%1
     (write-chunk-timeout s)
     (display
      (call-with-output-string
       (lambda (port)
	 (display "\r\n" port)
	 (display (number->string (string-length s) 16) port)
	 (display "\r\n" port)))
      port)
     (display s port)
     (flush-output-port port)
     #f)
    (thread-yield!))
  (if ($http-max-write-length)
      (letrec ((buffer (make-string/uninit ($http-max-write-length)))
	       (flush (lambda (buf i str j continue)
			(display-step buffer)
			(continue buf 0 str j flush))))
	(lambda (s)
	  (let ((r (string-splice! buffer 0 s 0 flush)))
	    (if (fx>= r 1)
		(display-step
		 (substring s (fx- (string-length s) r) (string-length s)))))))
      display-step))

(: http-write-blob-chunked (:blob: output-port (or false :thunk:) -> undefined))
(define (http-write-blob-chunked body port stop)
  (display-blob-notation *the-registered-stores* body #f (http-make-display port))
  (if stop (stop))
  (display "\r\n0\r\n\r\n" port)
	     (flush-output-port port))

(: http-write-body-chunked
   ((or :blob: string (or (pair number (list-of string))
			  (pair number (pair :blob: null))))
    output-port (or false :thunk:) -> undefined))
(define (http-write-body-chunked body port stop)
  (cond
   ((a:blob? body) (http-write-blob-chunked body port stop))
   ((and (pair? body) (pair? (cdr body)) (a:blob? (cadr body)))
    (http-write-blob-chunked (cadr body) port stop))
   (else
    (let ((ds (http-make-display port)))
      (do ((bdy (if (string? body) (list body) (cdr body))
		(cdr bdy)))
	  ((null? bdy)
	   (if stop (stop))
	   (display "\r\n0\r\n\r\n" port)
	   (flush-output-port port))
	(ds (car bdy)))))))

(: http-write-body-plain
   ((or :blob: string (or (pair number (list-of string))
			  (pair number (pair :blob: null))))
    output-port -> undefined))
(define (http-write-body-plain body port)
  (within-time%1
   ;; TODO use a request size and bandwidth dependant timeout.
   (write-plain-timeout)
   (http-write-body-plain-no-timeout body port)))

(: http-write-body-plain-no-timeout
   ((or false string :blob: (or (pair number (list-of string))
				(pair number (pair :blob: null))))
    output-port -> *))
(define (http-write-body-plain-no-timeout body port)
  (cond ((string? body) (display body port))
	((a:blob? body)
	 (display-blob-notation
	  *the-registered-stores*
	  body #f (lambda (part) (display part port))))
	((not body))
	((and (pair? body) (a:blob? (cadr body)))
	 (display-blob-notation
	  *the-registered-stores*
	  (cadr body) #f (lambda (part) (display part port))))
	(else (for-each (lambda (body) (display body port))
			(cdr body))))
  (flush-output-port port)
  #f)

(define empty-line? (pcre->proc "^\\r?$"))

(define chunk-size-regex (pcre->proc "([[:xdigit:]]+)(?:[[:space:]];.*)?\r?$"))

(define maximum-message-size-condition
  (make-condition &http-effective-condition 'status 413
		  'message "maximum message size exceeded"))

(define $http-bandwidth (make-shared-parameter 50))

;; body-read-timeout is badly named.  Better change to body-transfer-timeout
(define (body-read-timeout size)
  (and-let* ((base (timeout-second (respond-timeout-interval))))
	    (+ http-read-part-min-timeout
	       base
	       (quotient size ($http-bandwidth)))))

(define (http-read-content-length/rest
	 port request large
	 add-chunk initial-chunks reduce-chunks
	 total content-length
	 ignored-large-handler
	 )
  (define chunk-timeout (body-read-timeout $buffer-size))
  (let loop ((total total)
	     (sum initial-chunks))
    (if (>= total content-length)
	(reduce-chunks sum)
	(let* ((n (min (- content-length total) $buffer-size))
	       (buf (expect-object
		     chunk-timeout ;; or, but too detailed: (body-read-timeout n)
		     raise not-eof-object? read-bytes n port)))
	  (loop (+ total (string-length buf)) (add-chunk buf sum))))))

(define (http-read-chunked-encoding/rest
	 port request large
	 add-chunk initial-chunks reduce-chunks
	 total token
	 large-handler
	 )
  (let loop ((chunk-header (or token
			       (expect-object
				(http-read-chunked-header-timeout)
				raise not-eof-object? read-line port)))
	     (total total)
	     (tluser initial-chunks))
    (cond
     ((chunk-size-regex chunk-header) =>
      (lambda (match)
	(let ((size (string->number (cadr match) 16)))
	  (cond
	   ((eqv? size 0) (reduce-chunks tluser))
	   ((and large (fx>= (fx+ total size) large))
	    (large-handler
	     port request large
	     #f (receive (sofar sz fb) (reduce-chunks tluser) sofar) #f
	     total chunk-header
	     http-read-chunked-encoding/rest))
	   (else
	    (let ((chunk (expect-object 
			  (body-read-timeout size)
			  raise not-eof-object? read-bytes size port)))
	      (loop (expect-object
		     (http-read-chunked-header-timeout)
		     raise not-eof-object? read-line port)
		    (fx+ total (string-length chunk))
		    (add-chunk chunk tluser))))))))
     ((empty-line? chunk-header)
      (loop (expect-object 
	     (http-read-chunked-header-timeout)
	     raise not-eof-object? read-line port)
	    total tluser))
     (else 
      (raise (make-condition
	      &http-effective-condition 'status 400
	      'message "chunked encoding read garbage line"))))))

;; The initial implementation is: what did you expect?  Naive and
;; aimed at small requests.

(define (default-large-request-handler
	  port request large
	  add-chunk initial-chunks reduce-chunks
	  total token
	  recurse)
  (raise maximum-message-size-condition))

(define $large-request-handler
  (make-shared-parameter default-large-request-handler))

;; Request size to switch to bhob handling
(define $large-request-size
  (make-shared-parameter 131072))

;; Request size when to fail when no bhob handling available.
(define $small-request-limit
  (make-shared-parameter 2500000))

(define dispatching-request-handler
  (let ((collect (lambda (x)
		   (let ((all (apply-string-append (reverse! x))))
		     (values all (string-length all) #f)))))
    (lambda (port request large token
		  recurse large-handler)
      (receive
       (res sz fb)
       (if (and large (number? token) (> token large))
	   (large-handler
	    port request large
	    #f #f #f
	    0 token
	    recurse)
	   (recurse
	    port request large
	    cons '() collect
	    0 token
	    large-handler))
       (set-slot! request 'mind-body res)
       (if fb (set-slot! request 'body-first-block fb))
       (set-slot! request 'content-length sz)
       request))))

(define chunked-encoding? (pcre->proc "(?i:chunked)"))

;; TODO: this code does not work for replies to HEAD requests.  It
;; would try to read the request body according to the content-length
;; header.

(: http-read-content
   (input-port boolean (or false fixnum) :large-request-handler: -> :message:))
(define (http-read-content port read-to-end large-size large-handler)
  (let* ((request (within-time%1 (http-read-header-timeout) 
				 (http-read-header port)))
         (content-length (get-slot request 'content-length)))
    (cond
     ((and-let* ((encoding (get-slot request 'transfer-encoding)))
		(chunked-encoding? encoding))
      (dispatching-request-handler
       port request large-size #f
       http-read-chunked-encoding/rest large-handler)
      ;; FIXME we MUST only remove "chunked", not the whole header
      (set-slot! request 'transfer-encoding #f)
      (within-time%1 
       (http-read-trailer-timeout)
       (http-read-trailers! request port)))
     ;; rfc2616: 4.4: If the message does include a nonidentity
     ;; transfer-coding, the Content-Length MUST be ignored. Hence the
     ;; case after tranfer-encoding check.
     (content-length
      (dispatching-request-handler
       port request large-size content-length
       http-read-content-length/rest large-handler))
     (read-to-end
      (let ((buf (within-time%1
		  (http-read-to-end-timeout)
		  (read-bytes #f port))))
        (if (and (not (eof-object? buf)) (not (eqv? 0 (string-length buf))))
            (begin
              (set-slot! request 'mind-body buf)
              (set-slot! request 'content-length (string-length buf)))))))
    request))

(define whithespace-pcre
  (pcre/a->proc-uncached "[[:space:]]*([^[:space:]]+)"))

(define (splitwhite str start)
  (let loop ((m (whithespace-pcre str start)))
    (if m
	(cons (cadr m) (loop (whithespace-pcre str (cdar m))))
	'())))

(define mime-types-file (make-shared-parameter "/etc/mime.types"))

(: *mime.types* *) ;; actually (or false :string-table:) but this requires changes to chicken/typedefs.scm
(define *mime.types* #f)

(define (load-mime.types! file)
  (set! *mime.types* (make-string-table))
  (call-with-input-file file
    (lambda (port)
      (do ((line (read-line port) (read-line port)))
          ((eof-object? line) #t)
        (if (and (> (string-length line) 1)
                 (not (eqv? (string-ref line 0) #\#)))
            (let ((p (splitwhite line 0)))
              (for-each (lambda (ext)
                          (hash-table-set! *mime.types* ext (car p)))
                        (cdr p))))))))

(define file-name->content-type		; export; maybe users want to overwrite it
  (lambda (file)
    (if (not *mime.types*)
	(load-mime.types! (mime-types-file)))
    (let ((dot (string-index-right file #\.)))
      (and dot (hash-table-ref/default
		*mime.types*
		(substring file (add1 dot) (string-length file))
		#f)))))

(define $log-content-type-source
  (make-shared-parameter #f))
(define $invalid-xml-content-type
  (make-shared-parameter "application/octet-stream"))

(define xmlctregex
  ;; (pcre/a->proc-uncached "\\n?[[:space:]]*<\\?xml ")
  xml-pi-regex)

(define htmlctregex
  ;; (pcre/a->proc-uncached "[[:space:]\\n]*<(?:(?:html)|(?:HTML)|(?:!doctype.*(?:(?:HTML)|(?:html))))")
  html-regex)

;; BEWARE: while we try to detemine the request syntax we either
;; reject illegal XML or we MUST not store it as XML otherwise we will
;; a) break all applications and b) defeat attempts on optimization.
;;
;; The side effect of rejecting certain unprocessable entities might
;; not always be the perfect solution, even though it leads to fixes
;; in cause of the problem instead of suffering it.  Therefore TODO:
;; parameterize and switch to an alternative, NYI version, which fixes
;; the content type into application/octed-stream or maybe text/plain.

(define-condition-type &illegal-request-body &http-effective-condition
  illegal-request-body?)

(define cleanup-libmagic-mistakes
  (let ((x-c? (pcre->proc "^text/x-c.*"))
	(plain? (pcre->proc "^text/plain"))
	(zip? (lambda (x) (string=? x "application/x-zip")))
	(html? (pcre->proc "^text/html")))
    (define (cleanup-libmagic-mistakes cmd url request m-ct)
      (cond
       ((html? m-ct) (or (file-name->content-type url) m-ct))
       ((or (zip? m-ct) (x-c? m-ct) (plain? m-ct))
	(or (file-name->content-type url) m-ct))
       (else m-ct)))
    cleanup-libmagic-mistakes))

(define (guess-request-content-type cmd url request)
  (define b0 (message-body/plain request))
  (define body
    (cond
     ((string? b0) b0)
     ((not b0) b0)
     ((a:blob? b0) (get-slot request 'body-first-block))
     (else
      (raise (format "guess-request-content-type strange body ~s" b0)))))
  (cond 
   ((or (not body) (eqv? 0 (string-length body)))
    (if ($log-content-type-source)
	(logerr "Content-Type application/x-empty from empty body\n"))
    "application/x-empty")
   ((xmlctregex body)
    (guard (ex (else
		(let ((dflt (if (a:blob? b0)	; KLUDGE: just believe it.
				text/xml
				($invalid-xml-content-type))))
		  (if ($log-content-type-source)
		      (logerr
		       "Content-Type defaulted from illegal xml body: ~a\n"
		       dflt))
		  (or dflt
		      (raise (make-condition
			      &illegal-request-body
			      'status 406 'message (literal ex)))))))
	   (let* ((parsed (document-element (xml-parse body)))
		  (ct (node-list-media-type parsed)))
	     (if ($log-content-type-source)
		 (logerr "Content-Type ~a from (xml)body\n" ct))
	     (set-slot! request 'body/parsed-xml parsed)
	     ct)))
   ((htmlctregex body)
    (if ($log-content-type-source)
	(logerr "Content-Type text/html from (html)body\n"))
    (guard (ex (else (guard (ex (else "text/html"))
			    (content-type-for-html
			     (document-element (xml-parse body mode: 'html))))))
	   (content-type-for-html (document-element (message-body request)))))
   ((and-let* ((m-ct (mime-type-of-string body)))
	      (if (string=? m-ct "")
		  (begin
		    (if ($log-content-type-source)
			(logerr
			 "CHECK mime-db unrecognized content type in ~a ~a\n"
			 cmd url))
		    #f)
		  m-ct))
    => (lambda (m-ct)
	 (if ($log-content-type-source)
	     (logerr "Content type ~a from (mime)body\n" m-ct))
	 (cleanup-libmagic-mistakes cmd url request m-ct)))
   ((and (string=? cmd "PUT")
	 (file-name->content-type url))
    => (lambda (n-ct)
	 (if ($log-content-type-source)
	     (logerr "Content-Type ~a from file name ~a\n" n-ct url))
	 n-ct))
   (else 
    (if ($log-content-type-source)
	(logerr "Content-Type Unknown use application/octet-stream\n"))
    "application/octet-stream"
    )))

(define (ensure-request-content-type! cmd url request)
  (let ((content-type
	 (cond
	  ((and-let* ((content-type (get-slot request 'content-type))
		      ((not (equal? content-type ""))))
		     content-type)
	   => (lambda (content-type)
		(if ($log-content-type-source)
		    (logerr "Content type ~a supplied by ~a\n" content-type
			    (get-slot request 'user-agent)))
		content-type))
	  (else  (let ((content-type (guess-request-content-type cmd url request)))
		   (set-slot! request 'content-type content-type)
		   content-type)))))
    (set-slot! request 'content-type content-type)
    (if (a:blob? (get-slot request 'mind-body))
	(set-slot! request 'body-first-block #f))
    content-type))

(: http-form-encode (string --> string))
(define (http-form-encode str)
  (call-with-output-string
    (lambda (port)
      (let loop ((i 0))
        (if (not (eqv? i (string-length str)))
            (let ((c (string-ref str i)))
              (cond
               ((eqv? c #\space) (display #\+ port))
               ((< (char->integer c) 32)
                (let ((n (char->integer c)))
                  (display #\% port)
                  (display (quotient n 32) port)
                  (display (remainder n 32) port)))
               ;; Just to remember rscheme can do it like this:
               ;; (format #t "%~02x" (char->integer c))
               (else (display c port)))
              (loop (add1 i))))))))

(define n-parallel-requests 15)

(define parallel-secured-requests-semaphore
  (make-semaphore 'parallel-requests (fx* 10 n-parallel-requests)))

(define parallel-requests-semaphore
  (make-semaphore 'parallel-requests n-parallel-requests))

(define (parallel-requests . n)
  (if (pair? n)
      (if (car n)
	  (let ((n (car n)))
	    (set! n-parallel-requests n)
	    (set! parallel-requests-semaphore
		  (make-semaphore 'parallel-public-requests n))
	    (set! parallel-secured-requests-semaphore
		  (make-semaphore 'parallel-secure-requests (fx* n 10))))
	  (begin
	    (set! n-parallel-requests #f)
	    (set! parallel-requests-semaphore #f)
	    (set! parallel-secured-requests-semaphore #f))))
  n-parallel-requests)
