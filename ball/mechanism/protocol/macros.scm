;; (C) 2006 Jörg F. Wittenberger see http://www.askemos.org

(define-macro (loghttps fmt . rest)
  `(and ($https-client-verbose) (logerr ,fmt . ,rest)))

(define-macro (logtopo fmt . rest)
  `(and ($topology-verbose) (logerr ,fmt . ,rest)))
