;; (C) 2005 Christian Hermann, GPL see http://www.askemos.org

;; lmtp (rfc2033) works fine; but not resistant against DoS-attacks
;; TODO: base64 decode; plain->xmtp without request-build;
;; TODO:  full lmtp-support

;; timeout set to 200 seconds
(define lmtp-local-timeout (make-shared-parameter 200))

;; regex for mail-address processing
(define lmtp-user-regex (pcre->proc "^([^@]+)@.*$"))
(define lmtp-email-regex (pcre->proc "^([^@]+@.*)$"))
(define lmtp-cleartext-email-regex (pcre->proc "^[^<]*<([^@]+@.*)>.*$"))
(define lmtp-mail-from-regex (pcre->proc "^(?i)MAIL FROM:[ \t]*(.*)$"))
(define lmtp-rcpt-to-regex (pcre->proc "^(?i)RCPT TO:[ \t]*(.*)$"))

(define lmtp-boundary-regex (pcre->proc "^([^;]+);\\sboundary=\"([^\"]+)\""))
(define lmtp-header-regex (pcre->proc "^([^:]+): ([^;]+).*$"))
(define lmtp-headerarg-regex
  (pcre->proc "^[^;]+;\\s([^=]+)=[\"]?([^\"]+)[\"]?"))
(define lmtp-sheaderarg-regex (pcre->aproc "([^=]+)=[\"]?([^\"]+)[\"]?"))
(define (lmtp-body-regex cut) (pcre->proc (string-append "^(.*)\r" cut ".*")))
(define lmtp-line-regex (pcre->proc "^([^\r]*)\r"))
(define lmtp-ctype-regex
  (pcre->proc"^([^;]*);\\s([^=]*)=[\"]?([^\"]*)[\"]?[\\s]?$"))

;; @PARAM in-port - communication channel in-port
;; @PARAM out - communication channel out-port
;; @PARAM answer-function - function to send/receive askemos message

;; TODO: add namespace definition to mechanism/tree.scm ?!
(define namespace-mime-str "http://www.grovelogic.com/xmtp")
(define namespace-mime (string->symbol namespace-mime-str))

(define (lmtp-read-data port)
  (call-with-output-string
   (lambda (out)
     (do ((line  (read-line port) (read-line port)))
	 ((or (eof-object? line)
	      (string=? line ".")
	      (string=? line ".\r")) #t)
       (display line out)))))

(define (lmtp-request-server in-port out answer-function)

  (define (lmtp-out line)
    (display line out)
    (flush-output-port out) #f)

  ;; converts plain email into xmtp format
  ;; @PARAM frame request - holds the mail-headers
  ;; @PARAM string mbody - complete mail-body, including attachments
  ;; RETURN list - list ready for sxml parser starting with mime:Parts
  (define (plain->xmtp request mbody)
    (define (unbound-body request mbody)
      (let*
	  ((ctreg (lmtp-boundary-regex (get-slot request 'content-type)))
	   (separator (if ctreg (string-append "--" (caddr ctreg)))))
	(if ctreg
	    (cons 'mime:Parts
		  (let loop ((bodyrest mbody)
			     (retlist '())
			     (partlist '())
			     (partbody "")
			     (is-reading-header #f)
			     (is-base64 #f))
		    (let* ((partreg (lmtp-line-regex bodyrest))
			   (line (cadr partreg))
			   (spos (cdar partreg))
			   (slen (string-length bodyrest))
			   (newbrest (substring bodyrest spos slen)))


		      (cond
		       ;; if last mime part
		       ((string=? line (string-append separator "--"))
			;; All the lists where freshly build here and
			;; are not going to be used again.  We reverse
			;; by side effect.
			(reverse! (cons
				   (cons 'mime:MIME
					 (reverse!
					  (cons `(mime:BODY ,partbody)
						partlist)))
				   retlist)))

		       ;; if begin/end of mime-part
		       ((string=? line separator)
			(if (equal? partlist '())
			    (loop newbrest retlist '() "" #t #f)
			    (begin
			      (if (not is-base64)
				  (let ((slen (string-length partbody)))
				    (set! partbody (substring
						    partbody 0
						    (- slen 2)))))
			      (loop newbrest
				    (cons
				     (cons 'mime:MIME
					   (reverse!
					    (cons `(mime:BODY ,partbody)
						  partlist)))
				     retlist)
				    '() "" #t #f))))

		       ;; if reading-header
		       ;; TODO switch on base64
		       (is-reading-header
			(if (<= spos 1)
			    (loop newbrest retlist partlist "" #f is-base64)
			    (let ((headreg (lmtp-header-regex line))
				  (hargreg (lmtp-headerarg-regex line))
				  (shargreg (lmtp-sheaderarg-regex line)))

			      (cond
			       (hargreg
				(loop newbrest retlist
				      (cons `(,(string->symbol (cadr headreg)) (@ (,(string->symbol (cadr hargreg)) ,(caddr hargreg))) ,(caddr headreg)) partlist)
				      partbody #t is-base64))
			       (headreg
				(if (string-ci=? (caddr headreg) "base64")
				    (set! is-base64 #t))
				(loop newbrest retlist
				      (cons `(,(string->symbol (cadr headreg)) ,(caddr headreg)) partlist)
				      partbody #t is-base64))
			       (shargreg 
			        (loop newbrest retlist
				      `((,(caar partlist) (@ (,(string->symbol (cadr shargreg)) ,(caddr shargreg))) ,(data (cdar partlist))) . ,(cdr partlist))
				      partbody #t is-base64))))))



		       ;; if reading-body
		       (else
			(if is-base64
			    (if (<= spos 1)
				(loop newbrest retlist partlist
				      partbody #f is-base64)
				(loop newbrest retlist partlist
				      (string-append partbody line)
				      ;; (pem-decode line))
				      #f is-base64))
			    (loop newbrest retlist partlist
				  (string-append partbody line "\r")
				  #f is-base64)))
		       ))))
	    ;;if no boundary return unprocessed body
	    mbody)))

    ;;begin of unbounding
    (let ((toheader (get-slot request 'to)))
      (sxml
       `(MIME
	 (@ (@ (*NAMESPACES* (,namespace-mime ,namespace-mime-str mime))))
	 (To ,(get-slot request 'to))
	 (From ,(get-slot request 'from))
	 (Subject ,(get-slot request 'subject))
	 (MIME-Version ,(get-slot request 'mime-version))
	 ,(let ((ctarg (lmtp-ctype-regex
			(get-slot request 'content-type))))
	    (if ctarg
		`(Content-Type (@ (,(string->symbol (caddr ctarg))
				   ,(cadddr ctarg))) ,(cadr ctarg))
		`(Content-Type ,(get-slot request 'content-type))))
 	 (mime:BODY ,(unbound-body request mbody))))))


  ;; sends "POST" message to user-entry place returns #t if
  ;; answered with <reply><ack>.*</ack></reply>
  ;; @PARAM string to - recipients mailaddress
  ;; @PARAM frame request - a prepared request
  ;; @PARAM string data - mailbody including headers
  ;; RETURN boolean - #t if mail successfully delivered, else #f
  (define (deliver-mail to request xmtpmail)
    (set-slot! request 'mind-body (xml-format xmtpmail))
    (set-slot! request 'body/parsed-xml xmtpmail)
    (set-slot! request 'dc-identifier
	       (oid->string (entry-name->oid
			     (cadr (lmtp-user-regex to)))))

    (set-slot! request 'web-user "public")
    (set-slot! request 'request-method "POST")
    (set-slot! request 'location-format 'lmtp-location-format)
    (set-slot! request 'content-type "text/xml")
    (let* ((answer (guard (exception (else (logerr " MSGERR: ~a\n" exception)
					   #f))
			  (answer-function request)))
	   (xmlbody (if answer
			(message-body answer)
			#f)))
      (if (and (frame? answer)
	       (equal? "ACK" (data xmlbody)))
	  #t
	  #f)))


  ;; definitions finished, here comes the protocolpart
  (within-time%1
   (lmtp-local-timeout)
   (bind-exit
    (lambda (return)
      (begin
	(lmtp-out "220 askemos.dls LMTP server ready\n")
	(let loop ((from #f) (to '()))
	  (let ((line (read-line in-port)))
	    (if (string? line)
		(let ((line-length (string-length line))
		      (command (if (< (string-length line) 5)
				   "noco"
				   (substring line 0 4))))
		  (cond

		   ;; QUIT command
		   ((string-ci=? command "QUIT")
		    (lmtp-out "221 askemos.dls closing connection\n"))

		   ;; NOOP command
		   ((string-ci=? command "NOOP")
		    (begin
		      (lmtp-out "250 Ok\n")
		      (loop from to)))

		   ;; LHLO command
		   ((string-ci=? command "LHLO")
		    (begin
		      (lmtp-out "250 askemos.dls\n")
		      (loop from to)))

		   ;; RSET command
		   ((string-ci=? command "RSET")
		    (begin
		      (lmtp-out "250 Ok: Reset! Try Again?\n")
		      (loop #f '())))

		   ;; MAIL command
		   ((string-ci=? command "MAIL")
		    (let* ((regs (lmtp-mail-from-regex line))
			   (regmail (if regs
					(or (lmtp-cleartext-email-regex (cadr regs))
					    (lmtp-email-regex (cadr regs)))
					#f)))
		      (if regmail
			  (begin
			    (lmtp-out "250 OK\n")
			    (loop (cadr regmail) to))
			  (begin
			    (lmtp-out "501 Syntax: MAIL FROM:<user@domain>\n")
			    (loop from to)))))

		   ;; RCPT command
		   ((string-ci=? command "RCPT")
		    (let* ((regto (lmtp-rcpt-to-regex line))
			   (regmail (if regto
					(or (lmtp-cleartext-email-regex (cadr regto))
					    (lmtp-email-regex (cadr regto)))
					#f))
			   (reguser (if regmail
					(lmtp-user-regex (cadr regmail))
					#f)))
		      (if reguser
			  (if (entry-name->oid (cadr reguser))
			      (begin
				(lmtp-out "250 OK\n")
				(loop from (cons (cadr regmail) to)))
			      (begin
				(lmtp-out "550 No such user here\n") ;;danger brute-force could scan for askemos-users
				(loop from to)))
			  (begin
			    (lmtp-out "501 Syntax: RCPT TO:<user@domain>\n")
			    (loop from to)))))
		   
		   ;; DATA command
		   ((string-ci=? command "DATA")
		    (begin
		      (if (or (equal? to '())
			      (not from))
			  (begin
			    (lmtp-out "503 Error: need MAIL and RCPT command first\n")
			    (loop from to))
			  (begin
			    (lmtp-out "354 End data with <CR><LF>.<CR><LF>\n")
			    (let ((request (expect-object
					    (http-keep-alive-timeout)
					    return not-eof-object?
					    http-read-header in-port))
				  (mbody (lmtp-read-data in-port)))
			      (begin
				(let ((xmtpmail (plain->xmtp request mbody))
				      (to (reverse to)))
				  (for-each
				   (lambda (rcpt)
				     (if (not (deliver-mail rcpt request xmtpmail))
					 (lmtp-out (string-append "450 " rcpt " - delivery failed\n"))
					 (lmtp-out (string-append "250 " rcpt " - mail delivered\n"))))
				   to))
				(loop #f '())))))))

		   ;; unknown command
		   (else
		    (begin
		      (lmtp-out "502 Error: command not implemented\n")
		      (loop from to)))))))))))))


;; server run by main
(define (lmtp-server host port answer-function)
  (askemos:run-tcp-server
   host port
   (lambda (in-port out-port peer)
     (guard
      (c (else (receive
		(title msg args rest) (condition->fields c)
		(logerr " LMTP EXCEPTION ~a ~a ~a\n" title msg args))))
      (lmtp-request-server in-port out-port answer-function)))
   parallel-requests-semaphore #f "smtp-request"))
