;; (C) 2000-2002 Jörg F. Wittenberger see http://www.askemos.org

;; Beware: The SMTP implementation is a bit simple.  Due to missing
;; protocol error handling it might even hang.  It's here this way,
;; because it was simpler to cut&paste the code from FramerD than
;; decide and link something better.  Just keep the interface for
;; portability and replace it with something better as soon as
;; required.

(define $smtp-host (make-shared-parameter "localhost"))

(define (send-email to body header)
  (define (smtp ip expect op str . rest)
    (apply format op str rest)
    (flush-output-port op)
    (let ((result (read-line ip))
          (el (string-length expect)))
      (if (and (not (eof-object? result))
               (>= (string-length result) el)
               (equal? (substring result 0 el) expect))
          #t
          (error (string-append "SMTP: expect " expect " got " result )))))
  (define (rcpt-to in-port out-port email)
    (smtp in-port "250" out-port "RCPT TO: ~a\n" email))
  (let* ((to (if (hash-table? to) (hash-table-values to) to))
	 (all-hosts (if ($smtp-host)
			(list ($smtp-host))
			(dns-get-mail-exchangers (dns-find-nameserver)
						 (if (pair? to) (car to) to))))
         (from-header (or (assq 'from header) (assq 'From header)))
         (from (if from-header
                   (if (pair? (cdr from-header))
                       (cadr from-header)
                       (cdr from-header))
                   "askemos@localhost")))
    (let loop ((hosts all-hosts))
      (if (null? hosts)
	  (raise-service-unavailable-condition
	   (call-with-output-string (lambda (p) (display all-hosts p))))
	  (guard
	   (ex (else (loop (cdr hosts))))
	   (askemos:run-tcp-client
	    (car hosts) 25
	    (lambda (in-port out-port)
	      (read-line in-port)
	      (smtp in-port "250" out-port "HELO ~a\n" (local-id))
	      (smtp in-port "250" out-port "MAIL FROM: ~a\n" from)
	      (if (pair? to)
		  (for-each (lambda (e) (rcpt-to in-port out-port e)) to)
		  (rcpt-to in-port out-port to))
	      (smtp in-port "354" out-port "DATA\n")
	      (if (not (assq 'To header))
		  (if (pair? to)
		      (begin
			(format out-port "To: ~a" (car to))
			(for-each (lambda (t) (format out-port ",\n\t~a" t)) (cdr to))
			(newline out-port))
		      (format out-port "To: ~a\n" to)))
	      (for-each
	       (lambda (hdr)
		 (format out-port "~a: ~a\n"
			 (symbol->string (car hdr))
			 (if (pair? (cdr hdr)) (cadr hdr) (cdr hdr))))
	       header)
	      (newline out-port)
	      (display body out-port)
	      (smtp in-port "250" out-port "\n.\n")
	      (smtp in-port "221" out-port "QUIT\n")
	      (logerr "Mail sent.\n"))))))))

(define (convert-headers nl)
  (let ((node (node-list-first nl)))
    (cond
     ((node-list-empty? nl) (empty-node-list))
     ((and (xml-element? node) (not (member (gi node) '(BODY MIME Parts To))))
      `((,(gi node) . ,(call-with-output-string
			(lambda (port)
			  (display (data node) port)
			  (xmtp-print-attributes node port))))
        . ,(convert-headers (node-list-rest nl))))
     (else (convert-headers (node-list-rest nl))))))

;;
;; Diese Funktion haengt Attributwerte an Headereinträge an.
;;

(define (xmtp-print-attributes al port)
  (if (node-list-empty? al)
      (empty-node-list)
      (let* ((attributes (xml-element-attributes (node-list-first al)))
             (name (if (pair? attributes) (xml-attribute-name (car attributes)) #f)))
 	(if (pair? attributes)
	    (format port (if (eq? name 'boundary) "; ~a=\"~a\"" "; ~a=~a") name
 		    (normalise-data (xml-attribute-value (car attributes)) #f #f " "))))))

;;
;; Hier wird der Header generiert.
;; Einmal für die Funktion send-email (Standard-Header)
;; Und auch für die MIME-Parts (MIME-Header)
;;
;; TODO this could reuse the handler registration mechanism.  See
;; http.
(define (xmtp-print-headers nl port)
  (let ((node (node-list-first nl)))
    (cond
     ((node-list-empty? nl) (empty-node-list))
     ((and (xml-element? node) (not (member (gi node) '(BODY MIME Parts To))))
      (format port "~a: ~a" (gi node) (data node))
      (xmtp-print-attributes node port)
      (display "\n" port)
      (xmtp-print-headers (node-list-rest nl) port))
     (else (xmtp-print-headers (node-list-rest nl) port)))))

;; Konvertiert XMTP in EMailquelltext Ruft entweder sich selbst, oder
;; die Funktion convert-mime auf.  Hier wird ggf. die beginnende
;; Boundary gesetzt. (sofern vorhanden)
;;

(define (convert-mail port nl)
  (cond 
   ((node-list-empty? nl) (string))
   ((equal? (gi (node-list-first nl)) 'MIME)
    (convert-mail port (children (node-list-first nl))))
   ((not (node-list-empty? (select-elements nl 'Content-Type))) 
    (let* ((ct-text "text/plain")
	   (ctype (node-list-first (select-elements nl 'Content-Type)))
           (is-plain-email (equal? (data ctype) ct-text))
	   (boundary (if (node-list-empty? ctype)
			 (make-boundary-for nl)
			 (if is-plain-email ""
			     (or (attribute-string-defaulted 'boundary ctype #f)
				  (make-boundary-for nl))))) )
      ;; FIXME the // selector is grossly inefficient
      (if (node-list-empty? ((sxpath '(// Parts)) nl)) 
	  (convert-mime port (select-elements nl 'BODY) boundary)
	  (convert-mime port
			(children (node-list-first (select-elements nl 'BODY)))
			boundary))))))

;;
;; Fuer den Bau eines Mime-Body's wichtig.
;; Mithilfe von mime-format wird der Inhalt des BODY formatiert.
;;

(define (convert-body port nl . rest)
  (let ((node (node-list-first nl))
	(rest (node-list-rest nl))
	(boundary (if (pair? rest) (car rest) '()))
	(type (if (pair? rest) (cdr rest) '())))
    (cond 
     ((node-list-empty? nl))
     ((not (xml-element? node)) (convert-body port rest boundary type))
     ((equal? (gi node) 'Parts) (convert-mime port (children node) boundary))
     ((equal? (gi node) 'BODY)
      (if (node-list-empty? ((sxpath '(Parts)) node))
	  (begin
	    (newline port)
	    (display
	     (mime-format
	      (sxml `(output (@ (method "text") (media-type ,(car type)))
			     ,(data node))))
	     port))
	  (convert-mime port (children node) boundary)))
     (else (convert-body port (children node) boundary type)
	   (convert-body port rest boundary type)))))

;;
;; Parst MIME-Parts und wandelt sie in EMailquelltext um.
;; Setzt am Ende eines MIME-Parts die Boundary (sofern vorhanden)
;;
(define (convert-mime port nl . values)
  (let loop ((nl nl) (boundary (and (pair? values) (car values))))
    (let* ((node (node-list-first nl))
	   ;; FIXME using copy-attributes is grossly inefficient.
	   ;; Depreciated for kernel level use.
	   (content-type (copy-attributes
			  (node-list-first (select-elements node 'Content-Type))))
	   ;; FIXME ist die boundary wirklich korrekt und sinnvoll?  in
	   ;; notation/mime.scm war doch schon Code um boundaries zu
	   ;; generieren.
	   (boundary (cond (boundary boundary)
			   ((pair? content-type) (car content-type))
			   (else "")))
	   (type (data (node-list-first (select-elements node 'Content-Type)))))
      (cond
       ((node-list-empty? nl) (string))
       ((null? node) (string))
       ((or (equal? (gi node) 'MIME) (equal? (gi node) 'Parts)) 
	(if (eq? (gi node) 'MIME) (format port "\n--~a\n" (data boundary)) (string))
	(convert-mime port (children node) boundary))
       (else (xmtp-print-headers node port)
	     (convert-body port (select-elements node 'BODY) boundary type)))
      (if (not (node-list-empty? (node-list-rest nl)))
	  (loop (node-list-rest nl) boundary)))))

(define (sendmail mime)
    (let* ((nl (children mime))
	(to (node-list-map data (select-elements nl 'To))))
	    (send-email to
			(call-with-output-string
			 (lambda (port) (convert-mail port nl)))
			(convert-headers nl))))
    

;(define (sendmail nl)
;  (let ((to (node-list-map data (select-elements nl 'to))))
;    (receive
;     (body type) (mime-format (select-elements nl 'output))
;     (send-email to body (if (equal? type "text/plain")
;                             (convert-headers nl)
;                             `((MIME-Version "1.0")
;                               (Content-Type ,type)
;                               . ,(convert-headers nl)))))))
