;; (C) 2000, 2001, 2003 Jörg F. Wittenberger see http://www.askemos.org

;;*** "direct" aka WebDAV

;; WebDAV is an ill conceived protocol (see the helloween documents,
;; where some background on the question "why WebDAV" was leaked).  To
;; avoid bloat, we don't even try to support full WebDAV, instead we
;; do the minumum to make favorite client software work.  No more!

(define webdav-enabled? (the boolean #f)) ; Yes! we don't want that as
					; default.
(define debug-webdav? (the boolean #f))

(define webdav-commands (the (list-of string) '()))
(define webdav-commands-with-body '("PROPPATCH" "LOCK"))

(define webdav-command-expecting-102
  '("replicate" "PUT" "PROPPATCH" "DELETE" "LOCK" "COPY" "MOVE" "MKCOL" "UNLOCK"
    "LINK"				; non standard extension
    ))

(define webdav-commands-without-body 
  '("COPY" "PROPFIND" "MOVE" "MKCOL" "UNLOCK" "LINK"))
;; "COPY" and "MOVE" may have a request body containing D:propertybehavior
(define webdav-commands-without-reply-body '("MKCOL" "UNLOCK"))

(define webdav-extended-html-commands '("DELETE" "OPTIONS"))

(define (init-webdav! init-http!)
  (if (not (member "PROPFIND" http-legal-commands))
      (begin
	(set! http-commands-with-body
              (append http-commands-with-body
		      webdav-commands-with-body))
	(set! http-commands-without-body
              (append http-commands-without-body
		      webdav-commands-without-body))
	(set! webdav-commands
	      (append webdav-commands-with-body
		      webdav-commands-without-body))
	(set! http-legal-commands '())
	(init-http!)
	(set-options-allowed-commands! http-legal-commands)
	(set! webdav-enabled? #t)))
  #t)

(%early-once-only
 
(define (request-type-not-implemented cmd)
  (short-response-message 
   501 error: (format "~a not implemented!" cmd)))

(define (request-malformed msg . args)
  (short-response-message
   400 error: (if (pair? args) (apply format msg args) msg)))

(define (request-not-allowed text allow)
  (short-response-message
   405
   (property 'allow allow)
   body: (sxml `(html
		 (head)
		 (body (h1 "Requested Method not allowed") 
		       (p ,(literal text)))))))

(define (request-conflict text)
  (short-response-message
   409
   body: (sxml `(html
		 (head)
		 (body (h1 "Unable to comlete requested Operation") 
		       (p ,(literal text)))))))

(define (request-media-type-unsupported text)
  (short-response-message
   415
   body: (sxml `(html
		 (head)
		 (body (h1 "The request type of the body is unsupport.") 
		       (p ,(literal text)))))))

) ;; %early-once-only

(define (meta-info-of answer request path)
  (and-let* ((info (answer
		    (make-message
		     (property 'request-method 'read)
		     (property 'content-type text/xml)
		     (property 'body/parsed-xml metainfo-request-element)
		     (property 'accept application/rdf+xml)
		     (property 'web-user (get-slot request 'web-user))
		     (property 'secret   (get-slot request 'secret))
		     (property 'location-format 
			       (get-slot request 'location-format))
		     (property 'dc-identifier path))))
	     (      (frame? info))
	     (meta  (document-element (message-body info)))
	     (      (eq? 'RDF (gi meta))))
	    meta))

;; dav-supported-dir-methods the http-legal-commands w/o MKCOL

(define dav-supported-dir-methods
  '("OPTIONS" "GET" "HEAD" "POST" 
	      "PROPFIND" "PROPPATCH" "COPY"
	      "LOCK" "UNLOCK"
	      ))

(define dav-supported-methods-list
  (cons "MKCOL" dav-supported-dir-methods))

(define (dav-lock-request name isdir request)
  (set-slot! request 'body/parsed-xml
	     (sxml
	      `(core:lock
		(@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
				    (,namespace-mind ,namespace-mind-str core)
				    (,namespace-dav ,namespace-dav-str D)))
		   (depth ,(or (get-slot request 'depth) "infinity"))
		   (resource ,name))
		,@(if isdir '((D:resourcetype (D:collection))) '())
		(D:timeout ,(or (get-slot request 'timeout) "Infinite"))
		. ,(or (list (document-element (message-body request))) '()))))
  (set-slot! request 'content-type text/xml)
  request)

(define (dav-unlock-request name isdir request)
  (set-slot! request 'body/parsed-xml
	     (sxml
	      `(core:unlock
		(@ (@ (*NAMESPACES* (,namespace-xml ,namespace-xml-str xml)
				    (,namespace-mind ,namespace-mind-str core)
				    (,namespace-dav ,namespace-dav-str D)))
		   (depth ,(or (get-slot request 'depth) "infinity"))
		   (resource ,name))
		(D:locktoken (D:href ,(or (get-slot request 'lock-token) "lock token not found")))
		. ,(cond ((document-element (message-body request)) => list) (else '())))))
  (set-slot! request 'content-type text/xml)
  request)

(define $http-is-synchronous (make-shared-parameter #f))

(define (webdav-process cmd answer-function request)
  (cond
   ((not webdav-enabled?)
    (if (string=? cmd "OPTIONS")
	(request-type-not-implemented cmd)
	(answer-function request)))
   (else
    (case (string->symbol cmd)
      ;; rfc2616 HTTP/1.1
      ((OPTIONS)
;;     (set-slot! request 'request-method 'read)
;;     (set-slot! request 'content-type text/xml)
;;     (set-slot! request 'mind-body #f)
;;     (set-slot! request 'body/parsed-xml options-request-element)
       (let* ((answer request);;(answer (answer-function request))
	      (allow (let ((a (map data ((sxpath '(allow)) (message-body answer)))))
		       (if (pair? a) a 
			   '("OPTIONS" "GET" "HEAD" "POST" "DELETE" 
			     "PROPFIND" "MOVE" "LOCK" "UNLOCK" "LINK")))))
	 (set-slot! answer 'allow allow)
	 (let ((level (supported-dav-level allow)))
	   (if level (set-slot! answer 'dav level)))
	 answer))
      ((GET)
       (answer-function request))
      ((POST)
       (if (or ($http-is-synchronous)
	       (equal? (get-slot request 'expect) "X-Askemos-synchronous"))
	   (write-with-sync-answer
	    answer-function request
	    timeout: (body-read-timeout (message-body/plain-size request)))
	   (answer-function request)))
      ((PUT)
       (receive 
	(name path last)
	(string-split-last (get-slot request 'dc-identifier) #\/)
	;; if path does not extist answer with 409 "Confict"!
	(write-with-sync-answer 
	 answer-function request
	 to: path
	 body: (put-request-element 
		name
		(get-slot request 'content-type)
		(get-slot request 'mind-body))
	 timeout: (body-read-timeout (message-body/plain-size request)))))
      ((DELETE)
       (receive 
	(name path last)
	(string-split-last (get-slot request 'dc-identifier) #\/)
;; This optimisation assumes too much of the inner structure of applications.
;;
;; 	(let* ((meta (meta-info-of answer-function request path))
;; 	       (name (uri-parse name)))
;; 	  (cond
;; 	   ((and meta ;; we don't need to unlink a not existing resource 
;; 		 (node-list-empty? 
;; 		  ((sxpath 
;; 		    `(Description links Bag (li (@ (equal? (resource ,name))))))
;; 		   meta)))
;; 	    (obj-not-found (format "~a" (get-slot request 'dc-identifier))))
;; 	   (else
;; 	    (write-with-sync-answer 
;; 	     answer-function request
;; 	     to: path
;; 	     body: (sxml 
;; 		    `(api:form 
;; 		      (@ (@ (*NAMESPACES*
;; 			     (,namespace-mind ,namespace-mind-str api))))
;; 		      (link (@ (name ,name)))))
;; 	     actor: (if meta 
;; 			(string->oid 
;; 			 (attribute-string 
;; 			  'about ((sxpath '(Description)) meta)))
;; 			#f)
;; 	     status: 204))))
	(write-with-sync-answer 
	 answer-function request
	 to: path
;;FIXME! include If-Header
;;       use somthing like dav-delete-request
;;       check for locks in "path" and for name!
	 body: (sxml 
		`(api:form 
		  (@ (@ (*NAMESPACES*
			 (,namespace-mind ,namespace-mind-str api))))
		  (link (@ (name ,(uri-parse name))))))
	 status: 204)
	))
      ((TRACE)
       (request-type-not-implemented cmd))
      ((CONNECT)
       (request-type-not-implemented cmd))
      ((HEAD)
       (set-slot! request 'location-format 'webdav-location-format)
       (set-slot! request 'request-method 'read)
       (set-slot! request 'content-type text/xml)
       (set-slot! request 'mind-body #f)
       (set-slot! request 'body/parsed-xml
		  (propfind-request-element
		   (make-xml-element
		    'propfind namespace-dav
		    (list (make-xml-attribute
			   'xmlns #f namespace-dav-str ))
		    (node-list (make-xml-element
				'allprop namespace-dav '()
				(empty-node-list))))
		   "0"))
       (let* ((r (answer-function request))
	      (b (document-element (message-body r))))
	 (if (and b (eq? (ns b) namespace-dav))
	     (let ((prop ((sxpath '(response propstat prop *)) b)))
	       (set-slot! r 'http-status 200)
	       (set-slot! r 'mind-body #f)
	       (set-slot! r 'body/parsed-xml #f)
	       (let ((l (select-elements prop 'getcontentlength)))
		 (set-slot! r 'content-length (and (pair? l) (data l))))
	       (let ((t (select-elements prop 'getcontenttype)))
		 (set-slot! r 'content-type (and (pair? t) (data t))))
	       (let ((l (select-elements prop 'getlastmodified)))
		 (set-slot! r 'last-modified
			    (and (pair? l)
				 (srfi19:string->date
				  (data l)
				  rfc-822-time-string-format))))))
	 r))
      ;; rfc2518 WebDAV
      ((PROPFIND)
       (set-slot! request 'location-format 'webdav-location-format)
       (let* ((req-body (guard
			 (exception 
			  (else 
			   (logerr " ~a can't parse request body\n~a\n" 
				   cmd (body->string (get-slot request 'mind-body)))
			   #f))
			 (or (document-element (message-body request))
			     (make-xml-element
			      'propfind namespace-dav
			      (list (make-xml-attribute
				     'xmlns #f namespace-dav-str ))
			      (node-list (make-xml-element
					  'allprop namespace-dav '()
					  (empty-node-list)))))))
	      (valid?   (and req-body 
			     (eq? (gi req-body) 'propfind)
			     (member (first-child-gi req-body)
				     '(allprop prop propname)))))
	 (if (not valid?)
	     (request-malformed (format #f "Invalid ~a Request" cmd))
	     (begin
	       (set-slot! request 'request-method 'read)
	       (set-slot! request 'content-type text/xml)
	       (set-slot! request 'mind-body #f)
	       (set-slot! request 'body/parsed-xml
			  (propfind-request-element
			   req-body (get-slot request 'depth)))
;;FIXME        ;;POSTPROCESS muss 207 liefern: Fehler behandeln!
	       (webdav-postprocess (answer-function request))))))

      ((PROPPATCH)
;;FIXME! include If-Header
       (let* ((req-body (guard
			 (exception 
			  (else
			   (logerr " ~a can't parse request body\n~a\n" 
				   cmd (body->string
					(get-slot request 'mind-body)))
			   #f))
			 (document-element 
			  (xml-parse 
			   (body->string (get-slot request 'mind-body))))))
	      (valid?   (and req-body
			     (eq? (gi req-body) 'propertyupdate)
			     (member (first-child-gi req-body)
				     '(set remove)))))
	 (if (not valid?)
	     (request-malformed (format #f "Invalid ~a Request" cmd))
	     (webdav-postprocess 
	      (write-with-sync-answer 
	       answer-function request
	       body: (proppatch-request-element req-body))))))

      ((MKCOL)
       (if (equal? (get-slot request 'dc-identifier) "/")
	   ;; We could use this to create an entry point
	   (request-not-allowed "You can't use MKCOL to create \"/\""
	    dav-supported-dir-methods)
	   (receive 
	    (name path last)
	    (string-split-last (get-slot request 'dc-identifier) #\/)
	    (let* ((meta (meta-info-of answer-function request path))
		   (name (uri-parse name)))
	      ;; if meta is not set, there is no place at "path", we can not
	      ;; not read until there or some place on the way to path dosn't
	      ;; understand the meta-info-request
	      (cond 
	       ((not meta) ;; no meta -> error 409 ancestor dosn't exist
		(request-conflict
		 (string-append
		  "Unable to MKCOL "
		  (get-slot request 'dc-identifier)
		  ". Can't find Information about parent collection \""
		  path
		  "\". Either don't have the neccessary rights "
		  "or one component of that URL does not existist.")))
	       ((not (node-list-empty? ;; error 405 already exist 
		      ((sxpath 
			`(Description links Bag (li (@ (equal? (resource ,name))))))
		       meta)))
		(request-not-allowed
		 "MKCOL can not be executed on a already existent resource."
		 dav-supported-methods-list))
	       ((and (get-slot request 'mind-body)
		     ;; clients might send a content-length header in MKCOL
		     (> (body-size (get-slot request 'mind-body)) 0))
		;; For now we don't know a valid request body format
		;; to describe the creation of members and properties.
		;; So we reject any request with a message body as
		;; 415 Unsupported Media Type
		(request-media-type-unsupported
		 "The Server does not support MKCOL requests with a request entity"))
	       (else
		(write-with-sync-answer 
		 answer-function request
		 body: (mkcol-request-element name)
		 to: path
		 actor: (string->oid 
			 (attribute-string 
			  'about ((sxpath '(Description)) meta)))
		 status: 201)))))))

      ((MOVE COPY LINK)
       (if (not (get-slot request 'dav-destination))
	   (request-malformed "~a needs Destination Header" cmd)
	   ;; test also if Depth != "" or "infinity"
	   (receive
	    (to body)
	    (copy/move-request-element request (string-downcase cmd))
	    (write-with-sync-answer 
	     answer-function request
	     to: to ;; use prefix here
	     body: body))))
      ((LOCK UNLOCK)
       (receive
	(name path last)
	(string-split-last (get-slot request 'dc-identifier) #\/)
	;; if path does not extist answer with 409 "Confict"!
	(write-with-sync-answer
	 answer-function ((if (equal? cmd (symbol->string 'LOCK))
			      dav-lock-request dav-unlock-request)
			  name last request)
	 to: path)))
      (else
       (answer-function request))))))

;      (and debug-webdav? (frame-slots request))
;      ;(if (string=? "MOVE" (get-slot request 'request-method))
;      ;	 (set-slot! request 'reply-channel (make-mailbox "reply-cannel")))


(define (webdav-postprocess answer)
  (if (frame? answer)
      (let ((ans (document-element (get-slot answer 'body/parsed-xml))))
					;    (and debug-webdav? (frame-slots answer))
	(cond
	 ((and ans (or (eq? (gi ans) 'success)
		       (eq? (gi ans) 'error)))
	  (let ((text ((sxpath '(text *)) ans))
		(status (string->number (data ((sxpath '((status 1) *text*)) ans)))))
	    (set-slot! answer 'mind-body #f)
	    (set-slot! answer 'http-status (or status 200))
	    (set-slot! answer 'location #f)
	    (if (node-list-empty? text)
		(begin
		  (set-slot! answer 'content-type #f)
		  (set-slot! answer 'body/parsed-xml #f))
		(begin
		  (set-slot! answer 'content-type text/xml)
		  (set-slot! answer 'body/parsed-xml text)))))
	 ((and (eq? (ns ans) namespace-dav)
	       (eq? (gi ans) 'Envelope))
	  (set-slot! answer 'mind-body #f)
	  (set-slot! answer 'body/parsed-xml ((sxpath '(Body *)) ans))
	  (for-each
	   (lambda (header)
	     (and-let* ((header-tag
			 (case (gi header)
			   ((Lock-Token lock-token) 'lock-token)
			   (else #f))))
		       (set-slot! answer header-tag (data header))))
	   ((sxpath '(Header *)) ans))))	  
	;; for MOVE??
	;;(set-slot! answer 'location #f)
	;; Doesn't help anyway:
	;; (set-slot! answer 'dav "1,2")
	answer)
      answer))



;_____________________________________________________________________
; debug code
;_____________________________________________________________________

(define (frame-slots frame)
  (display (format "---- frame slots - default ----\n"))
  (display (format "version:\t ~s\n" (get-slot frame 'version )))
  (display (format "replicates:\t ~s\n" (get-slot frame 'replicates )))
  (display (format "proposal:\t ~s\n" (get-slot frame 'proposal )))
  (display (format "mind-links:\t ~s\n" (get-slot frame 'mind-links )))
  (display (format "content-type:\t ~s\n" (get-slot frame 'content-type )))
  (display (format "mind-boby:\t ~s\n" (get-slot frame 'mind-body )))
  (display (format "boby/parsed-xml:\t ~s\n" (get-slot frame 'body/parsed-xml )))
  (display (format "---- frame slots - other ------\n"))
  (let loop((slots (frame-other frame)))
    (cond
     ((pair? slots)
      (display (format "~s:\t ~s\n" (caar slots) (cdar slots)))
      (loop (cdr slots)))
     (else #t)))
  (display (format "-- %<--- %<--- %<--- %<--- %<--\n")))

;; (soap-call
;;  (sxml
;;   `(env:Envelope 
;;     (@ (@ (*NAMESPACES* 
;; 	      (,namespace-soap-envelope ,namespace-soap-envelope-str env)
;; 	      (,namespace-http ,namespace-http-str http)
;; 	      (,namespace-dav  ,namespace-dav-str dav))))
;;     (env:Header
;;      (dav:method      ,(get-slot request 'request-method))
;;      (dc-identifier   ,(get-slot request 'dc-identifier))
;;      (content-type    ,(get-slot request 'content-type))
;;      (dav:destination ,(or (get-slot request 'dav-destination) ""))
;;      (dav:depth       ,(or (get-slot request 'depth) "infinity"))
;;      (dav:overwrite   ,(or (get-slot request 'overwrite) "T"))
;;      (dav:if          ,(or (get-slot request 'dav-if) ""))
;;      (dav:lock-token  ,(or (get-slot request 'lock-token) ""))
;;      )
;;     ;; test is it xml?
;;     ;; for< DELETE: parse locator 
;;     ;; <form xmlns=mind><link name="destination"/></form> 
;;     (env:Body         ,body))))


	   ;; gemeinsamer prefix von (get-slot request 'dc-identifier) und
	   ;;   (or (get-slot request 'dav-destination) "")

	   ;; (let* ((s1 "/ab/ycde/ax") 
	   ;;        (s2 "/ab/cde/af")
	   ;;        (prefix (substring s1 0 (string-prefix-length s1 s2)))
	   ;;        (match ((pcre->proc "/[^/]*$") prefix))
	   ;;       ) 
	   ;;   (if match (substring s1 0 (caar match)) #f))

