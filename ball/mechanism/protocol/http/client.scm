;; (C) 2000-2003, 2005 Jörg F. Wittenberger see http://www.askemos.org

;; TODO this quick HACK lived far too long already.  Needs rewrite to
;; support redirection etc. without bad hacks using 'orig-msg', which
;; was intentionally shadowed.

(define serializable-transient-meta-slots-not-in-http-headers
  (filter (lambda (slot) (not (or (memq slot http-general-headers)
				  (memq slot http-request-headers)
				  (memq slot http-entity-headers))))
          serializable-transient-meta-slots))

;; FIXME: the timeouts SHOULD depend on body size.

(define $http-max-retries (make-shared-parameter 1))

;; *** httpX-send-message!

;; Sends a message and recieves the answer.  It should not touch the
;; message as is was (with the exception of "fixing it up" with
;; cacheable values, e.g. content-length).

;; Testing existance of all headers is excessive.  We should make sure
;; our passed-in message contain them in the desired order and not
;; re-sort them in the client code.  To mitigate the effect, we define
;; a few to-be-written header sets and default them to "all".

;; Note however that using parameters is pretty bad.  Try to get rid
;; of it again.

(define *http-transfered-headers*
  (make-parameter
   (append
    http-general-headers
    http-request-headers
    http-entity-headers
    dav-request-header
    serializable-transient-meta-slots-not-in-http-headers)))

(define http-status-pcre
  (pcre/a->proc-uncached
   "HTTP/1\\.[01][[:blank:]]+([[:digit:]]{3})[[:blank:]]+([^\\r\\n]*)"))

(define (httpX-send-message! client-open stop respond request auth)
  (define host (or (get-slot request 'host)
		   (error "http-send-message!: missing host")))
  (define body (or (get-slot request 'mind-body)
		   (xml-format/list (get-slot request 'body/parsed-xml))))
  
  (define bsize (if (pair? body) (car body) (body-size body)))

  (define transfer-encoding (get-slot request 'transfer-encoding))

  (define command (let ((type (get-slot request 'type)))
		    (cond
		     ((member type '(post "POST"))    "POST")
		     ((member type '(write "PUT" #t)) "PUT")
		     ((eq? type 'CONNECT) "CONNECT")
		     (else "GET"))))

  ;; We buffer the otherwise unbuffered output, thus reducing the
  ;; number of packets on the network.  Surprisingly this buffering
  ;; obviously happens, but seems not to have much influence on the
  ;; performance of the agreement protocol.
  (define first-block
    (call-with-output-string
     (lambda (port)
       (let ((path (get-slot request 'dc-identifier)))
	 (format port
		 (if (and (fx>= (string-length path) 1)
			  (eqv? (string-ref path 0) #\/))
		     "~a ~a~a HTTP/1.1\n"
		     "~a ~a/~a HTTP/1.1\n")
		 command
		 "";;(if proxy-request (format #t "~a://~a" scheme host) "")
		 path))
       (if (not (get-slot request 'connection))
	   (display "Connection: keep-alive\r\n" port))
       (if (not (or transfer-encoding (get-slot request 'content-length)))
	   (begin
	     ;; (logerr "http client content-length not set for ~a bytes\n" bsize)
	     (format port "Content-Length: ~a\r\n" bsize)))
       (if (pair? auth)
	   (begin
	     (display "Authorization: Basic " port)
	     (display (pem-encode-string
;;FIXME the server ignores an empty passwd - so the (or "") should not be done here 
		       (string-append (car auth) ":" (or (cdr auth) "")))
		      port)
	     (display "\r\n" port)))
       (http-write-headers (*http-transfered-headers*) request port)
       (and-let* ((action (get-slot request 'SOAPAction)))
		 (format port "SOAPAction: ~a\r\n" action))
       (if (not transfer-encoding) (display "\r\n" port))
       (if (and (< bsize $immediate-body-size) (not stop))
	   (http-write-body-plain-no-timeout body port)))))

  (define (rd-response connection)
    (define port (connection-input-port connection))
    (let loop ()
      (let* ((read (within-time%1 (http-read-status-line-timeout)
				  (read-line port)))
	     (line (if (eof-object? read)
		       (raise read)
		       (and (string? read) (http-status-pcre read)))))
	(if line
	    (let ((response (http-read-content
			     port #f
			     ($small-request-limit) ($large-request-handler)))
		  (status (string->number (cadr line))))
	      (if (eqv? status 102)
		  (loop)
		  (let ((peer-cert (connection-certificate connection)))
		    (set-slot! response 'http-status status)
		    (if (eqv? status 406)
			(logerr "Failed on ~a: ~a\n"
				host ;; (get-slot response 'mind-body)
				line))
		    (if peer-cert
			(begin
			  (set-slot! response 'ssl-peer-certificate peer-cert)
			  (set-slot! response 'authorization
				     (http-affirm-authorization peer-cert)))
			(loghttps "HTTP(S) client no peer cert on ~a\n" host))
		    response)))
	    (raise (format #f "Host ~a illegal status line ~s\n" host read))))))

  (guard
   (c ;;; Do not raise exceptions here the way we did before.
	  ;;; This could be risky.  But if we do, the can be delivered
	  ;;; into the client, even though the transaction did work
	  ;;; out.  We should try if "no exception" leaves things
	  ;;; hanging elsewhere.
    ;;; ((timeout-object? c) (raise-service-unavailable-condition host) )
    ;;; ((timeout-object? c) c) ;; logging the timeout is annoying
    ((timeout-object? c)
     (respond
      (make-message
       (property 'http-status 503)
       (property 'mind-body (format "timeout on ~a\n" host)))))
    (else
     (receive
      (title msg args rest) (condition->fields c)
      (loghttps "HTTP(S) client ~a ~a ~a\n" host (if (message-condition? c) (condition-message c) msg) args)
      (respond (condition->message #f request c)))))
   (http-with-channel
    host (http-with-channel-timeout)
    (lambda (channel)
      (let loop ((retries ($http-max-retries)) (errors '()))
	(if (eqv? retries 0)
	    (let ((x (make-message
		      (property 'http-status 504)
		      (property 'mind-body (format "timeout on ~a: ~a\n" host errors)))))
	      ;; (http-close-channel! host)
	      (respond x)
	      ;; (logerr "RAISE RETURNED ~a\n" (raise (timeout-object)))
	      )
	    (receive
	     (connection reused)
	     (http-client-allocate-connection
	      channel client-open host (not (string=? command "CONNECT")))
	     (if (not connection) (loop 0 (cons (if reused reused 0) errors))
		 (let ((port (connection-output-port connection)))
		   (and
		    (guard
		     (exception
		      (else
		       (set! connection
			     (http-client-dispose-connection channel connection #f))
		       (let ((left (if reused retries (sub1 retries))))
			 (loghttps "~a retries on 1st block left on ~a ~a\n"
				   left host
				   (cond ((message-condition? exception)
					  (condition-message exception))
					 ((eof-condition? exception)
					  (condition->string (eof-condition-reason exception)))
					 (else (condition->string exception))))
			 (loop left (cons 1 errors)))))
		     (within-time%1
		      (short-local-timeout)
		      (display first-block port)
		      (flush-output-port port)
		      connection))
		    (if (or stop (fx>= bsize $immediate-body-size))
			(guard
			 (exception
			  (else
			   (set! connection
				 (http-client-dispose-connection channel connection #f))
			   (let ((left (sub1 retries)))
			     (loghttps "~a retries left on ~a ~a\n"
				       left host
				       (cond ((message-condition? exception)
					      (condition-message exception))
					     ((eof-condition? exception)
					      (eof-condition-reason exception))
					     (else (condition->string exception))))
			     (loop left (cons 2 errors)))))
			 (if transfer-encoding
			     (http-write-body-chunked body port stop)
			     (http-write-body-plain body port))
			 connection)
			connection)
		    connection
		    (guard
		     (exception ((eof-object? exception)
				 (http-client-dispose-connection
				  channel connection #f)
				 (let ((left (if reused retries (sub1 retries))))
				   (loghttps "EOF on STATUS line ~a retries left on ~a\n"
					     retries host)
				   (loop left (cons 3 errors))))
				(else
				 (http-client-dispose-connection
				  channel connection #f)
				 (loghttps "on ~a while reading reply: ~a\n" host
					   (if (message-condition? exception)
					       (condition-message exception)
					       (condition->string exception)))
				 (respond
				  (condition->message #f request exception))))
		     (let ((response (rd-response connection)))
		       (or (http-host-maybe-alive? host)
			   (cache-invalid! *http-alive-cache* host))
		       (if (string=? command "CONNECT")
			   (if (eqv? (quotient (get-slot response 'http-status) 100) 2)
			       (respond connection)
			       (http-client-dispose-connection channel connection #f))
			   (begin
			     (http-client-dispose-connection
			      channel connection
			      (not (equal? (get-slot response 'connection) http-close)))
			     (respond response))))))
		   #f)))))))))

;; The follow-redirection code ist completely broken.  We need to lift
;; it to the proper place.
;;
;; (and-let*
;;  ((follow-redirection)
;;   (l (get-slot response 'http-location))
;;   (s (get-slot response 'http-status))
;;   (r (or (eq? 303 s)
;; 	 ;; 304 is not realy redirect
;; 	 ;; 305 is proxy ignored for now
;; 	 ;; 306 is reserved
;; 	 (and (memq s '(300 301 302 307))
;; 	      (member (get-slot request 'type)
;; 		      ;; add other write methods here
;; 		      '(post "POST"
;; 			     write "PUT" #t)))))
;;   (m ((pcre->proc "(?:([^:]+)://)([^/]+)(/.*)") l))
;;   ;;TODO: need a http/https parameter here
;;   ;;(p (string=? (cadr m protocol)))
;;   ((or (not (member l locations))
;;        (error "loop in redirection")))
;;   ((or (not (> (length locations) 5)) ;;like old RFC
;;        (error "too many indirections"))))
;;  (set-slot! request 'host (caddr m))
;;  (set-slot! request 'dc-identifier (cadddr m))
;;  (if (eq? 303 s)
;;      (begin
;;        (set-slot! request 'type 'get)
;;        (set-slot! request 'mind-body #f)
;;        (set-slot! request 'content-length #f)
;;        (set-slot! request 'content-type #f)
;;        (set-slot! request 'transfer-encoding #f)))
;;  (return (redirection (cons l locations))))

(define (with-host-resolved msg proc)
  (let ((host (get-slot msg 'host)))
    (cond
     ((not host) (raise "host missing"))
     ((not (string? host)) (error "host is not a string" host))
     ((($https-use-socks4a) host) (proc msg host))
     ((ip-address/port? host) (proc msg host))
     (else
      (logerr "W client resolving ~a\n" host)
      (raise host)
      (let* ((msg (message-clone msg))
	     (colon (string-index host #\:))
	     (name (if colon (substring host 0 colon) host))
	     (ns (dns-find-nameserver))
	     (ip (and ns (dns-get-address ns name)))
	     (nv (if colon
		     (string-append ip ":" (substring host (add1 colon) (string-length host)))
		     ip)))
	(assert (string? ip))
	(set-slot! msg 'host nv)
	(proc msg nv))))))

(define (http-send-message! stop respond msg auth follow-redirection)
  (with-host-resolved
   msg
   (lambda (msg label)
     (!start (httpX-send-message! http-client-open stop respond msg auth)
	     label))))

(define (https-send-message! stop respond msg auth follow-redirection)
  (with-host-resolved
   msg
   (lambda (msg label)
     (!start (httpX-send-message! https-client-open-sslmgr stop respond msg auth)
	     label))))

(define (http-cc-send-message! open stop respond msg auth follow-redirection)
  (!start (httpX-send-message! open stop respond msg auth)
	  (get-slot msg 'host)))


(define (http-response msg . args)
  (let ((auth (or (and (pair? args) (null? (cdr args)) (car args))
		  (and-let* ((kw (memq authentication: args)))
			    (cadr kw))))
	(follow-redirection (and-let* ((kw (memq follow-redirection: args)))
				      (cadr kw))))
    (with-host-resolved
     msg
     (lambda (msg label)
       (let ((result (the (or false :message:) #f)))
	 (!apply
	  (lambda (respond)
	    (httpX-send-message! http-client-open #f respond msg auth))
	  (list (lambda (value) (set! result value))))
	 result)))))
(define (https-response-auth msg auth follow-redirection)
  (with-host-resolved
   msg
   (lambda (msg label)
     (let ((result (the (or false :message:) #f)))
       (!apply
	(lambda (respond)
	  (httpX-send-message! https-client-open-sslmgr #f respond msg auth))
	(list (lambda (value) (set! result value))))
       result))))
#|
(define (https-response-auth msg auth follow-redirection)
  (!apply
   (lambda ()
     (let* ((result (the (or false :message:) #f))
	    (respond (lambda (value) (set! result value))))
       (httpX-send-message! https-client-open-sslmgr #f respond msg auth)
       result))
   '()))
|#
(define (https-response msg . args)
  (let ((auth (or (and (pair? args) (null? (cdr args)) (car args))
		  (and-let* ((kw (memq authentication: args)))
			    (cadr kw))))
	(follow-redirection (and-let* ((kw (memq follow-redirection: args)))
				      (cadr kw))))
    (let ((result (the (or false :message:) #f)))
      (!apply
       (lambda (respond)
	 (httpX-send-message! https-client-open-sslmgr #f respond msg auth))
       (list (lambda (value) (set! result value))))
      result)))

(define (http-for-each escape . args)
  (guard
   (c (else (format #t "~a" c) (escape c)))
   (let* ((follow-redirection #f)
	  (stepper (make-mailbox 'http-for-each))
          (respond (lambda (result) (send-message! stepper result)))
          (await (lambda ()
                   (let ((result (receive-message! stepper)))
                     (if (and result (frame? result)
                              (memq (get-slot result 'http-status)
				    '(200 201 303)))
                         result
                         (begin
                           (logerr "failed. status:\n~a\n~a"
				   (get-slot result 'http-status)
				   (message-body/plain result))
                           (escape result))))))
          (http-response
           (lambda args
             (apply (lambda (arg . rest)
		      (http-send-message! #f respond arg
					  (and (pair? rest)
					       (cons (car rest) (cadr rest)))
					  follow-redirection))
		    args)
             (await)))
          (force-and-response
           (lambda (e)
             (let ((request (force e)))
               (and request (apply http-response request))))))
     (apply for-each force-and-response args)
     (map force args))))
