;; (C) 2004 Peter Hochgemuth, GPL see http://www.askemos.org
;;
;; this file collects most of the definitions from the DAV protocol


(define dav-request-header
  '(depth           ; 0|1|infinity
    dav-destination ; URI for MOVE or COPY
    dav-if          ; tokenlist -> match to the places before continue
    lock-token      ; a lock token
    overwrite       ; T | F defaults to T -> like If-Match but If-Match applies not to Destination
    timeout         ; only for LOCK! Second-\d | Infinite | Extend field-value
    ))

;; dav-if: If Header  
;;                           schema:     //host     /path    ?param       #fragment
(define url-match-str "^(?:[^:/?#]+:)?(?://[^/?#]*)?([^?#]*)(?:\\?[^#]*)?(?:#.*)?$")
(define dav-if-header-parse
  (pcre-lalr-parser
   (("[[:space:]]+")
    ("\\(" lb)
    ("\\)" rb)
    ("[nN][oO][tT]" not)
    ("<[[:alpha:]]+://[^/]*([^>]+)>" coded-url)
    ("<([^>]+)>" coded-url)
    ("\\[([^]]+)]" entity-tag))

   (expect: 12)

   (lb rb not entity-tag coded-url)

   (if (no-tag-list+) : $1 ;(cons 'locks $1)
       (tagged-list+) : $1) ;(cons 'locks $1))

   (no-tag-list+ (list+ no-tag-lists) : `((lock . ,$1) . ,$2))

   (no-tag-lists () : '()
		 (list no-tag-lists) : `((lock . ,$1) . ,$2))

   (tagged-list+ (tagged-list tagged-lists) : (cons $1 $2))

   (tagged-lists () : '()
		 (tagged-list tagged-lists) : (cons $1 $2))

   (tagged-list (coded-url list+) :
		(let ((dst (guard
			    (e (else $1)) ;; the guard in not nessesary
			    (parsed-locator 
			     (cadr ((pcre->proc url-match-str) (car $1)))))))
		  (if (null? dst) 
		      `(lock (@ (resource "")) . ,$2)
		      (let loop ((dst dst))
			(if (or (null? (cdr dst))
				(string=? "" (cadr dst)))
			    `(lock (@ (resource ,(car dst))) . ,$2)
			    `(lock (@ (resource ,(car dst)))
				   ,(loop (cdr dst))))))))

   (list+ (list lists) : `( ,$1 . ,$2))
   (lists () : '() (list lists) : `( ,$1 . ,$2))

   (list (lb listelement+ rb) : `(and . ,$2))

   (listelement+ (listelement listelements) : (cons $1 $2))
   (listelements () : '() (listelement listelements) : (cons $1 $2))

   (listelement (not statetoken) : `(not ,$2)
		(statetoken) : $1)

   (statetoken (coded-url) : (list 'Lock-Token (car $1))
	       (entity-tag) : (list 'Entity-Tag (car $1)))

   ))

(define (dav-parse-if-header str)
  (sxml `(locks (@ (requested ,str)) . ,(dav-if-header-parse str))))

(define (dav-write-if-header locks port)
  (if (and (eq? (gi locks) 'locks)
	   (attribute-string 'requested locks))
      (display (attribute-string 'requested locks) port)
      (error "dav-write-if-header can't understand value ~a\n" locks)))


;; lock-token: Lock-Token Header
(define pcre-lock-token
  (pcre/a->proc-uncached "[[:space:]]*<([^>]+)>"))

(define (dav-parse-lock-token str)
  (and-let* ((m (pcre-lock-token str))) (cadr m)))

(define (dav-write-lock-token token port)
  (display "<" port) (display token port) (display ">" port))

(%early-once-only
 (http-register-handler 
  'depth           "Depth"       identity display)
 (http-register-handler 
  'dav-destination "Destination" identity display)
 (http-register-handler 
  'dav-if          "If"          dav-parse-if-header dav-write-if-header)
 (http-register-handler 
  'lock-token      "Lock-Token"  dav-parse-lock-token dav-write-lock-token)
 (http-register-handler 
  'overwrite       "Overwrite"   identity display)
 (http-register-handler 
  'timeout         "TimeOut"     identity display)
)

(define dav-response-headers
  '(dav ; DAV: 1,2 response only for OPTIONS-requests
    lock-token
    status-uri
    ))

(%early-once-only
 (http-register-handler 
  'dav             "DAV" identity display)
 (http-register-handler 
  'status-uri      "Status-URI" identity display)
) 

(define (dav-write-entity-headers response port defaults) 
  (http-write-headers dav-response-headers response port defaults))
