;; (C) 2004 Peter Hochgemuth see http://www.askemos.org
;;
;; this file collects most of the definitions from the http protocol

(define http-legal-commands '())
;; HTTP-Methods as defined in RFC2616:
;; OPTIONS
;; GET
;; HEAD
;; POST
;; PUT
;; DELETE
;; TRACE
;; CONNECT
;; respond with 405 (Method not Allowed) on a per URL basis
;; or 501 (Not Implemented) in general
(define http-commands-with-body '("POST" "PUT"))
(define http-commands-without-body '("HEAD" "GET" "OPTIONS" "DELETE"))
(define http-commands-without-reply-body '("OPTIONS")) ;; "HEAD" not here, special cased

(define http-close "close")

(define http-close-match (pcre/a->proc-uncached "[[:space:]]*[cC][lL][oO][sS][eE][[:space:]]*"))

(%early-once-only
 (define http-status-table (make-fixnum-table))
 (for-each 
  (lambda (x) 
    (hash-table-set! http-status-table (car x) (cdr x)))
  '((100 . "Continue")                        ;; Section 10.1.1
    (101 . "Switching Protocols")             ;; Section 10.1.2
    (102 . "Processing"         )             ;; WebDAV 10.1
    (200 . "OK")                              ;; Section 10.2.1
    (201 . "Created")                         ;; Section 10.2.2
    (202 . "Accepted")                        ;; Section 10.2.3
    (203 . "Non-Authoritative Information")   ;; Section 10.2.4
    (204 . "No Content")                      ;; Section 10.2.5
    (205 . "Reset Content")                   ;; Section 10.2.6
    (206 . "Partial Content")                 ;; Section 10.2.7
    (207 . "Multi-Status")                    ;; WebDAV 10.2
    (300 . "Multiple Choices")                ;; Section 10.3.1
    (301 . "Moved Permanently")               ;; Section 10.3.2
    (302 . "Found")                           ;; Section 10.3.3
    (303 . "See Other")                       ;; Section 10.3.4
    (304 . "Not Modified")                    ;; Section 10.3.5
    (305 . "Use Proxy")                       ;; Section 10.3.6
    (307 . "Temporary Redirect")              ;; Section 10.3.8
    (400 . "Bad Request")                     ;; Section 10.4.1
    (401 . "Unauthorized")                    ;; Section 10.4.2
    (402 . "Payment Required")                ;; Section 10.4.3
    (403 . "Forbidden")                       ;; Section 10.4.4
    (404 . "Not Found")                       ;; Section 10.4.5
    (405 . "Method Not Allowed")              ;; Section 10.4.6
    (406 . "Not Acceptable")                  ;; Section 10.4.7
    (407 . "Proxy Authentication Required")   ;; Section 10.4.8
    (408 . "Request Time-out")                ;; Section 10.4.9
    (409 . "Conflict")                        ;; Section 10.4.10
    (410 . "Gone")                            ;; Section 10.4.11
    (411 . "Length Required")                 ;; Section 10.4.12
    (412 . "Precondition Failed")             ;; Section 10.4.13
    (413 . "Request Entity Too Large")        ;; Section 10.4.14
    (414 . "Request-URI Too Large")           ;; Section 10.4.15
    (415 . "Unsupported Media Type")          ;; Section 10.4.16
    (416 . "Requested range not satisfiable") ;; Section 10.4.17
    (417 . "Expectation Failed")              ;; Section 10.4.18
    (422 . "Unprocessable Entity")            ;; WebDAV 10.3
    (423 . "Locked")                          ;; WebDAV 10.4
    (424 . "Failed Dependency")               ;; WebDAV 10.5
    (451 . "Unavailable For Leagal Reason")   ;; proposed
    (500 . "Internal Server Error")           ;; Section 10.5.1
    (501 . "Not Implemented")                 ;; Section 10.5.2
    (502 . "Bad Gateway")                     ;; Section 10.5.3
    (503 . "Service Unavailable")             ;; Section 10.5.4
    (504 . "Gateway Time-out")                ;; Section 10.5.5
    (505 . "HTTP Version not supported")      ;; Section 10.5.6
    (507 . "Insufficient Storage")            ;; WebDAV 10.6
    ))
 )

(define (http-status-string status)
  (or (hash-table-ref/default http-status-table status #f)
      (hash-table-ref/default http-status-table (* 100 (quotient status 100)) #f)
      "Coding Error: status string not found"))

;; if Content-Length or Transfer-Encoding header are present
;; Content-Type must be present
;;;;; (define http-methods
;;;;;   ;; method    type    requestbody     cachable reply 
;;;;;   '(("OPTIONS" 'read  'optional-unused #f 'empty-S200-Allow-Content-Length0) ; Section 9.2
;;;;;     ("GET"     'read  'empty           #? 'body)  ; Section 9.3
;;;;;     ("HEAD"    'read  'empty           #? 'empty)  ; Section 9.4
;;;;;     ("POST"    'write 'body            #f 'optional-200/201/204-LocationHeaderfor201)  ; Section 9.5
;;;;;     ("PUT"     'write 'body            #f 'optional-200/201/204-501ifcontent-Header-are-unknown--apply-request-entity-headers-to-new-place)  ; Section 9.6
;;;;;     ("DELETE"  'write 'empty           #f 'optional-200/202/204); Section 9.7
;;;;;     ("TRACE"   'read  'empty           #f 'S200-body-is-the request contenttype=message/http) ; Section 9.8
;;;;;     ("CONNECT" 'not-implemented!       #f 'unknown)       ; Section 9.9
;;;;;     ))

;;;;; (define webdav-methods
;;;;;   ;; method      type    requestbody      cachable reply 
;;;;;   '(("PROPFIND" 'read    'optional-xml #f 'body-S207-application/xml-or-text/xml)
;;;;;     ("PROPPATCH" 'write   'body-xml     #f 'body-S207)
;;;;;     ("MKCOL"    'write   'optional-props-and-member-w-props-whatistheMedia-Type? #f 'empty-201/403/405ifURIexists/409if-no-parent/415ifrequest-body-MediaType-is-not-suppuorted/507)
;;;;;     ;("GET")
;;;;;     ;("HEAD")
;;;;;     ;("POST")
;;;;;     ;("DELETE"   'write   'empty        #f 'optional-S207-if-errorin-collections)
;;;;;     ;("PUT" if-no-parent-or-exists-as-collection 409)
;;;;;     ("COPY"     'write   'optional-xml #f 'optional-201/204/destination-overwritten/403src==dest/409no-dst-parent/412props-error-or-dest-exist/423/502/507/S207if-error-in-children )
;;;;;     ("MOVE"     'write   'optional-xml #f 'optional-201/204/destination-overwritten/403src==dest/409no-dst-parent/412props-error-or-dest-exist/423/502/507/S207if-error-in-children)
;;;;;     ("LOCK"     'write   'optional-xml #f 'body-xml-S200/207/412/423)
;;;;;     ("UNLOCK"   'write   'empty        #f 'empty/204/207..)
;;;;; ))

(define http-general-headers
  '(cache-control
    connection
    dc-date       ; Date 
    http-pragma   ; Pragma . for HTTP/1.0
    trailer
    transfer-encoding
    upgrade
    via
    warning))

(define (http-write-general-headers response port defaults) 
  (http-write-headers http-general-headers response port defaults))

(%early-once-only
 (http-register-handler 
  'cache-control       "Cache-Control" identity display)
 (http-register-handler 
  'connection          "Connection" identity display)
;; this is done in protocol/util.scm
;;(http-register-handler
;; 'dc-date             "Date"
;; http-read-date
;; (lambda (obj port)
;;   (display (srfi19:date->string obj "~a, ~d ~b ~Y ~H:~M:~S ~z") port)))
 (http-register-handler 
  'http-pragma         "Pragma" identity display)
 (http-register-handler 
  'trailer             "Trailer" identity display)
 (http-register-handler 
  'transfer-encoding   "Transfer-Encoding" identity display)
 (http-register-handler 
  'upgrade             "Upgrade" identity display)
 (http-register-handler 
  'via                 "Via" identity display)
 (http-register-handler 
  'warning             "Warning" identity display)
)

(define http-request-headers
  '(accept
    accept-charset
    accept-encoding
    accept-language
    authorization
    secret ;; proprietary extension to support anon access codes
    expect
    from
    host
    if-match
    if-modified-since
    if-none-match
    if-range
    if-unmodified-since
    max-forwards
    proxy-authorization
    range
    referer
    te
    user-agent))

(define (http-write-request-headers response port defaults) 
  (http-write-headers http-request-headers response port defaults))

(%early-once-only
; Accept: type/subtype [;token="value"]* , ...
 (http-register-handler 
  'accept              "Accept" identity display)
 (http-register-handler 
; Accept-Charset: charset; [;token="value"]* , ...
  'accept-charset      "Accept-Charset" identity display)
 (http-register-handler 
; Accept-Encoding: encoding; [;token="value"]* , ...
  'accept-encoding     "Accept-Encoding" identity display)
 (http-register-handler 
; Accept-Language: lang; [;token="value"]* , ...
  'accept-language     "Accept-Language" identity display)
 (http-register-handler 
; Authorization: credentials
  'authorization       "Authorization" identity display)
 (http-register-handler 
; Annon access code
  'secret       "X-Secret" identity display)
 (http-register-handler 
  'expect              "Expect" identity display)
 (http-register-handler 
  'from                "From" identity display)
 (http-register-handler 
  'host                "Host" identity display)
 (http-register-handler 
  'if-match            "If-Match" identity display)
 (http-register-handler 
  'if-modified-since   "If-Modified-Since" identity display)
 (http-register-handler 
  'if-none-match       "If-None-Match" identity display)
 (http-register-handler 
  'if-range            "If-Range" identity display)
 (http-register-handler 
  'if-unmodified-since "If-Unmodified-Since" identity display)
 (http-register-handler 
  'max-forwards        "Max-Forwards" identity display)
 (http-register-handler 
  'proxy-authorization "Proxy-Authorization" identity display)
 (http-register-handler 
  'range               "Range" identity display)
 (http-register-handler 
  'referer             "Referer" identity display)
 (http-register-handler 
  'te                  "TE" identity display)
 (http-register-handler 
  'user-agent          "User-Agent" identity display)
)

(define http-response-headers
  '(accept-ranges
    age
    etag
    destination				;; KLUDGE Askemos only
    http-location
    proxy-authenticate
    retry-after
    server
    vary
    www-authenticate))

(define (http-write-response-headers response port defaults) 
  (http-write-headers http-response-headers response port defaults))

(%early-once-only
; Accept-Ranges: none|byte  we use none if we got arequest for range
 (http-register-handler 
  'accept-ranges       "Accept-Ranges" identity display)
; Age: seconds
 (http-register-handler 
  'age                 "Age" identity display)
 (http-register-handler 
  'etag                "ETag" identity display)
;;  (http-register-handler 
;;   'http-location     "Location" parsed-locator http-format-location-path)
 (http-register-handler 
  'http-location     "Location" identity display)
 (http-register-handler 
  'proxy-authenticate  "Proxy-Authenticate" identity display)
 (http-register-handler 
  'retry-after         "Retry-After" identity display)
 (http-register-handler 
  'server              "Server" identity display)
 (http-register-handler 
  'vary                "Vary" identity display)
 (http-register-handler 
  'www-authenticate    "WWW-Authenticate" identity display)

 (define http-entity-headers
   '(allow
     content-encoding
     content-language
     content-length
     content-location
     content-md5
     content-range
     content-type
     expires
     last-modified))

 (define http-entity-headers-maybe-chunked
   (filter (lambda (x) (not (eq? x 'content-length)))
           http-entity-headers))
 ) ;; %early-once-only

(define (http-write-entity-headers response port defaults)
  (http-write-headers http-entity-headers response port defaults))

(define (http-write-entity-headers-maybe-chunked response port defaults)
  (http-write-headers http-entity-headers-maybe-chunked response port defaults))

(%early-once-only
; Allow: HTTP-methods must be present in 405 a Response 
; and should be present in the response to PUT
 (http-register-handler 
  'allow               "Allow" 
  (lambda (obj) (string-split obj ","))
  (lambda (obj port)
    (if (not (null? obj))
        (let loop ((obj obj))
          (display (car obj) port)
          (if (not (null? (cdr obj)))
              (begin (display "," port) (loop (cdr obj))))))))
 (http-register-handler 
  'content-encoding    "Content-Encoding" identity display)
 (http-register-handler 
  'content-language    "Content-Language" identity display)
 (http-register-handler 
  'content-length    "Content-Length" string->number display)
 (http-register-handler 
  'content-location    "Content-Location" identity display)
 (http-register-handler 
  'content-md5         "Content-MD5" identity display)
 (http-register-handler 
  'content-range       "Content-Range" identity display)
 (http-register-handler 
  'content-type        "Content-Type" identity display)
 (http-register-handler 
  'expires             "Expires"
  (lambda (str) (or (string->number str) (http-read-date str)))
  (lambda (obj port) (display (if (number? obj) obj (rfc-822-timestring obj)) port)))
 (http-register-handler 
  'last-modified       "Last-Modified" http-read-date
  (lambda (obj port) (display (rfc-822-timestring obj) port)))
) ; %early-once-only

;; (define (http-403->condition response)
;;   (let ((fault ((sxpath '(Body Fault))
;; 		(xml-parse (get-slot response 'mind-body)))))
;;     (raise (make-condition
;; 	    &forbidden
;; 	    'resource (call-with-input-string
;; 		       (data ((sxpath '(Detail Relation id)) fault))
;; 		       read)
;; 	    'source (call-with-input-string
;; 		     (data ((sxpath '(Detail Relation id)) fault))
;; 		     read)
;; 	    'message (data ((sxpath '(Detail Text)) fault))
;; 	    'status 403))))
