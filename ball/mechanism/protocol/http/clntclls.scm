(define http-property-get (property 'type 'get))
(define http-property-post (property 'type 'post))
(define http-property-soap+xml (property 'content-type application/soap+xml))
(define http-property-text/scheme-str "x-text/scheme")
(define http-property-text/scheme
  (property 'content-type http-property-text/scheme-str))
(define (is-text/scheme? str) (equal? str http-property-text/scheme-str))
(define http-property-replicate (property 'SOAPAction "replicate"))
(define http-property-replicate-reply (property 'SOAPAction "reply-notice"))
(define http-property-get-ca (property 'SOAPAction "X509CACertificate"))
(define http-property-eval (property 'SOAPAction "eval"))
;; Note: SOAPAction is gone in SPAM 1.2.  We'll need to follow...
(define http-property-reliable-broadcast
  (property 'SOAPAction "ReliableBroadcast"))

(define http-property-expect-synchronous
  (property 'expect "X-Askemos-synchronous"))

(define http-property-get-digest (property 'SOAPAction "digest"))
(define http-property-get-place (property 'SOAPAction "get-place"))
(define http-property-get-meta (property 'SOAPAction "get-meta"))
(define http-property-get-blob (property 'SOAPAction "get-blob"))
(define http-property-get-privileges (property 'SOAPAction "get-privileges"))
(define http-property-get-state (property 'SOAPAction "get-state"))
(define http-property-get-reply (property 'SOAPAction "get-reply"))
(define http-property-get-invite (property 'SOAPAction "invitation"))

(define http-property-accept
  (property
   'accept
   (srfi:string-join 
    (list application/soap+xml  application/rdf+xml text/xml
          application/xml text/html)
    ", ")))

(define (http-slot->property request)
  (lambda (slot) (property slot (request slot))))

(define (http-log-error-response result)
  (if (or ($https-client-verbose) (agree-verbose))
      (cond
       ((frame? result)
	(and-let* ((status (get-slot result 'status))
		   ((not (eqv? status 200))))
		  (logerr "HTTP CLIENT RESPONSE ~a\n~a\n"
			  status (get-slot result 'mind-body))))
       ((message-condition? result)
	(logerr "HTTP/S CLIENT RESPONSE  ~a\n" (condition-message result)))
       ((timeout-object? result)
	(logerr "HTTP/S CLIENT RESPONSE connection closed (timeout)\n"))
       ((eof-condition? result) 
	(logerr "HTTP/S CLIENT RESPONSE connection closed (~a)\n"
		(eof-condition-reason result)))
       (else (logerr "HTTP CLIENT RESPONSE ~a\n" result))))
  #f)

;; Sanity check on headers.

(define (http-log-missing-header! caller header . optional)
  (if (not (memq header (if (pair? optional) (car optional) optional-transient-meta-slots)))
      (logerr "~a missing ~a\n" caller header)))

(define (http-copy-header-properties callsite-id including from optionals . additionals)
  (let loop ((which serializable-transient-meta-slots))
	   (if (null? which)
	       additionals
	       (let ((x (get-slot from (car which))))
		 (if x
		     (cons (property (car which) x)
			   (loop (cdr which)))
		     (begin
		       (http-log-missing-header! callsite-id (car which) optionals)
		       (loop (cdr which))))))))

;; FIXME we better don't rely on this function, it's a rough
;; estimation and too often too wrong.

(define (estimated-node-list-body-length>= here max)
  (let loop ((total 0) (here here) (more '()))
    (or (fx>= total max)
        (if (node-list-empty? here)
            (if (null? more) #f (loop total (car more) (cdr more)))
            (let ((first (node-list-first here)))
              (if (xml-element? first)
                  (loop (+ total 15)
                        (children first)
                        (cons (node-list-rest here) more))
                  (if (xml-literal? first)
                      (loop (+ total (string-length (xml-literal-value first)))
                            (node-list-rest here) more)
                      (loop (+ total 30) (node-list-rest here) more))))))))

(define (http-always-use-sync request) #t)
(define (http-maybe-use-sync request)
  (or (and (get-slot request 'mind-body)
	   (fx>= (body-size
		  (get-slot request 'mind-body))
		 (large-message-size)))
      (and (node-list? (get-slot request 'body/parsed-xml))
	   (estimated-node-list-body-length>=
	    (get-slot request 'body/parsed-xml)
	    (large-message-size)))))

(define *http-use-sync* http-always-use-sync)

(define (http-sync-point here total await)
  (let ((mutex (make-mutex here))
	(enough (make-condition-variable here))
	(got 0)
	(lost 0)
	(ts (the (or false :time:) #f)))
    (lambda args
      (let loop ((entry #t))
	(mutex-lock! mutex)
	(if entry
	    (if (null? args)
		(begin
		  (set! got (add1 got))
		  (if (eqv? (add1 got) await) (set! ts *system-time*)))
		(set! lost (add1 lost))))
	(cond
	 ((and (pair? args) (fx>= (fx+ lost got) total))
	  (mutex-unlock! mutex)
	  (logerr ;; loghttps
	   "enable ~a hosts for large message at ~a expecting failure\n" got here)
	  (condition-variable-broadcast! enough))
	 ((fx>= got total)
	  (mutex-unlock! mutex)
	  (loghttps "enable ~a hosts for large message at ~a\n" got here)
	  (condition-variable-broadcast! enough))
	 ((eqv? got await)
	  (if ts
	      (let ((tmo (time-second (time-difference *system-time* ts))))
		(if (<= tmo 0)
		    (begin
		      (set! ts #f)
		      (mutex-unlock! mutex)
		      (loghttps "enable ~a hosts for large message at ~a\n" got here)
		      (condition-variable-broadcast! enough))
		    (begin
		      (guard (ex (else
				  (set! ts #f)
				  (loghttps "enable ~a hosts for large message at ~a\n" got here)
				  (condition-variable-broadcast! enough)))
			     (within-time%1 (* (- total got) tmo)
					    (mutex-unlock! mutex enough)))
		      (loop #f))))
	      (mutex-unlock! mutex)))
	 ((fx>= got await)
	  (if ts
	      (begin (mutex-unlock! mutex enough) (loop #f))
	      (mutex-unlock! mutex)))
	 (else (mutex-unlock! mutex enough) (loop #f)))))))

(define (stop-runaway-replicate! t)
  (and-let* ((tm (respond-timeout-interval)))
	    (thread-sleep! tm))
  (thread-signal-timeout! t))

;; TBD move this and the identical definition in http/server into protocol/common
(define http-102-response
  (make-message
   (property 'http-status 102)))

(define (http-replicate-call result voters await place type request . dont-wait-anyway)
  (let* ((here (aggregate-entity place))
	 (total (length voters))
	 (done (and (*http-use-sync* request)
		    (let ((done (make-mutex here)))
		      (mutex-lock! done #f #f) done)))
         (stop (and done
		    (let ((sp (http-sync-point here total await)))
		      (lambda args (sp) (mutex-unlock! done)))))
         (slots (http-copy-header-properties
		 'http-replicate-call serializable-transient-meta-slots request '()
		 http-property-post
		 (property 'transfer-encoding (and stop "chunked"))
		 http-property-expect-synchronous
		 (property 'dc-identifier (oid->string here))
		 http-property-replicate
		 (property 'secret (and (eq? (get-slot request 'dc-creator) (public-oid))
					(get-slot request 'secret)))
		 (if (get-slot request 'mind-body)
		     (property 'mind-body (get-slot request 'mind-body))
		     (property 'body/parsed-xml
			       (get-slot request 'body/parsed-xml)))) )
	 (keep-up (and (mailbox? result)
		       (make-thread
			(lambda ()
			  (do () (#f)
			    (thread-sleep! (or (respond-timeout-interval) 30))
			    (mailbox-send! result http-102-response)))
			"keep-up")))
	 (abrt! (lambda ()
		  (if done (mutex-unlock! done))
		  (if keep-up
		      (let ((t keep-up))
			(set! keep-up #f)
			(guard (ex (else #f)) (thread-terminate! t))))))
	 (run-threads (lambda ()
			(!mapfold
			 (lambda (voter)
			   (let ((result (the (or false :message:) #f)))
			     (httpX-send-message!
			      https-client-open-sslmgr
			      stop (lambda (r) (set! result r))
			      (apply make-message (property 'host voter) slots)
			      (voter-auth voter))
			     result))
			 (lambda (n result i)
			   (values
			    n
			    (or (and-let* (((frame? result))
					   (status (get-slot result 'http-status))
					   ((fx>= status 200)))
					  (if (fx>= status 400)
					      (cond
					       ((let ((is (get-slot i 'http-status)))
						  (or (eqv? is 503) (eqv? is 412)))
						(if stop (stop 'lost))
						(if (or (fx>= status 500) (fx= status 408)) i result))
					       (else (if (or (fx>= status 500) (fx= status 408)) i result)))
					      result))
				i)))
			 ;; init
			 (condition->message
			  #f request
			  (make-condition &service-unavailable
					  'message (format "~a" voters)
					  'status 503))
			 voters		; data
			 ;; fold-handler could be #t or thread-signal-timeout! - but don't just terminate!
			 fold-handler: stop-runaway-replicate!
			 count: await)))
	 (run (lambda ()
		(parameterize
		 ((*http-transfered-headers* (cons 'secret (*http-transfered-headers*))))
		 (dynamic-wind
		     (lambda () (if keep-up (thread-start! keep-up)))
		     run-threads
		     abrt!)))))
    (cond
     ((mailbox? result)
      (if (and (pair? dont-wait-anyway) (car dont-wait-anyway))
	  (begin
	    (!start (mailbox-send! result (guard (ex (else ex)) (run)))
		    (oid->string here))
	    (if done (mutex-lock! done)))
	  (let ((best (guard (ex (else (mailbox-send! result ex) (raise ex))) (run))))
	    (mailbox-send! result best) best)))
     (else (let ((result (!order/thunk run (oid->string here))))
	     (if done (mutex-lock! done))
	     result)))))

(define (http-replicate-reply voters oid version message)
  (let ((slots
	 (http-copy-header-properties
	  'http-replicate-reply serializable-transient-meta-slots message
	  optional-transient-reply-meta-slots
	  http-property-post
	  (property
	   'transfer-encoding
	   (and (or (and (get-slot message 'mind-body)
			 (fx>= (body-size
				(get-slot message 'mind-body))
			       (large-message-size)))
		    (and (node-list? (get-slot message 'body/parsed-xml))
			 (estimated-node-list-body-length>=
			  (get-slot message 'body/parsed-xml)
			  (large-message-size))))
		"chunked"))
	  (property 'dc-identifier (oid->string oid))
	  (property 'destination
		     (cons* (number->string (version-serial version))
			    (literal (get-slot message 'http-status))
			    (or (get-slot message 'destination) '())))
	  http-property-replicate-reply
	  (property 'mind-body (message-body/plain message))) ))
    (parameterize
     (#;(http-read-status-line-timeout (respond-timeout-interval))
      (*http-transfered-headers* (cons 'destination (*http-transfered-headers*))))
     (for-each
      (lambda (voter)
	(if ((host-alive?) voter)
	    (https-send-message!
	     #f http-log-error-response
	     (apply make-message (property 'host voter) slots)
	     (voter-auth voter)
	     #f)
	    (if (agree-verbose)	(logerr "Q no route to host ~a\n" voter))))
      voters))))

(define (http-forward-call result host place type request)
  (let ((secret (and (eq? (get-slot request 'dc-creator) (public-oid))
		     (get-slot request 'secret)))
	(slots (http-copy-header-properties
		'http-forward-call serializable-transient-meta-slots request '()
		(if (eq? type 'read) http-property-get http-property-post)
		(property 'dc-identifier
			  (oid->string (aggregate-entity place)))
		http-property-replicate
		(if (get-slot request 'mind-body)
		    (property 'mind-body (get-slot request 'mind-body))
		    (property 'body/parsed-xml
			      (get-slot request 'body/parsed-xml))))))
    (parameterize
     (#;(http-read-status-line-timeout (http-read-header-timeout))
      (*http-transfered-headers* (cons 'secret (*http-transfered-headers*))))
     (let ((slots (if secret (cons (property 'secret secret) slots) slots)))
       (https-send-message!
	#f
	(lambda (res) (and result (mailbox-send! result res)))
	(apply make-message (property 'host host) slots)
	(voter-auth host)
	#f)))))

(define http-agreement-headers
  '(;; connection hard coded default to "Keep-Alive"
    transfer-encoding
    dc-creator
    dc-date
    content-length
    content-type			; maybe we can get rid of that one again?
    ))

(define (http-echo-call voters place request request-digest state-digest)
  (let ((slots (append!
                (map (http-slot->property request) transient-meta-slots)
                (list
                 http-property-post
		 http-property-text/scheme
                 (property 'dc-identifier (oid->string (place 'get 'id)))
                 (property 'mind-body
                           (call-with-output-string
                            (lambda (port)
                              (write (make-echo-message
				      request-digest state-digest)
				     port))))
                 http-property-reliable-broadcast))))
    (parameterize
     ((*http-transfered-headers* http-agreement-headers)
      #;(http-read-status-line-timeout (respond-timeout-interval)))
     (map-parallel
      #t #f 0 #f
      (lambda (voter)
	(if (and voter ((host-alive?) voter))
	    (httpX-send-message!
	     https-client-open-sslmgr
	     #f
	     http-log-error-response
	     (apply make-message (property 'host voter) slots)
	     (voter-auth voter))))
      voters))))

(define (http-send-ready voters place request value request-digest)
  (let ((slots (append!
                (map (http-slot->property request) transient-meta-slots)
                (list
                 http-property-post
		 http-property-text/scheme
                 (property 'dc-identifier (oid->string (place 'get 'id)))
                 (property 'mind-body 
                           (call-with-output-string
                            (lambda (port)
			      (write (make-ready-message request-digest value)
				     port))))
                 http-property-reliable-broadcast))))
    (parameterize
     ((*http-transfered-headers* http-agreement-headers)
      #;(http-read-status-line-timeout (respond-timeout-interval)))
     (map-parallel
      #t #f 0 #f
      (lambda (voter)
	(if (and voter ((host-alive?) voter))
	    (httpX-send-message!
	     https-client-open-sslmgr
	     #f
	     http-log-error-response
	     (apply make-message (property 'host voter) slots)
	     (voter-auth voter))))
      voters))))

(define (http-send-one-shot voters place request request-digest result-digest)
  (let ((slots (append!
                (map (http-slot->property request) transient-meta-slots)
                (list
                 http-property-post
		 http-property-text/scheme
                 (property 'dc-identifier (oid->string (place 'get 'id)))
                 (property 'mind-body
                           (call-with-output-string
                            (lambda (port)
                              (write (let ((d (xml-digest-simple (request 'mind-body))))
				       (list
					'one-shot
					(make-echo-message d request-digest)
					(make-ready-message d result-digest)))
                                     port))))
                 http-property-reliable-broadcast))))
    (parameterize
     ((*http-transfered-headers* http-agreement-headers)
      #;(http-read-status-line-timeout (respond-timeout-interval)))
     (map-parallel
      #t #f 0 #f
      (lambda (voter)
	(if (and voter ((host-alive?) voter))
	    (httpX-send-message!
	     https-client-open-sslmgr
	     #f
	     http-log-error-response
	     (apply make-message (property 'host voter) slots)
	     (voter-auth voter))))
      voters))))

(define (http-invite-new-supporters! oid old-quorum new-quorum)
  (and old-quorum new-quorum
       (parameterize
	((*http-transfered-headers* http-agreement-headers)
	 #;(http-read-status-line-timeout (respond-timeout-interval)))
	(for-each
	 (lambda (h)
	   (if (not (member h (quorum-others old-quorum)))
	       (!start
		(let ((host (host-lookup* h)))
		  (https-send-message!
		   #f
		   (lambda (result) #f)            ; ignore
		   (make-message
		    (property 'host host)
		    http-property-post
		    http-property-get-invite
		    (property 'dc-identifier (oid->string oid)))
		   (voter-auth host)
		   #f))
		h)))
	 (quorum-others new-quorum)))))

(define (as-get-for-local-sync-state oid version)
  (list
   http-property-get
   http-property-text/scheme
   (property
    'mind-body
    (call-with-output-string
     (lambda (port) (write version port))))
   (property 'dc-identifier (oid->string oid))))

(define (http-get-state voters oid here note)
  (let ((slots (cons http-property-get-state
		     (as-get-for-local-sync-state oid here))))
    (parameterize
     ((*http-transfered-headers* http-agreement-headers)
      #;(http-read-status-line-timeout (respond-timeout-interval)))
     (yield-mutex-set!
      (map-parallel
       #t #f 0 #f
       (lambda (voter)
	 (and-let* ((host ((host-lookup) voter))
		    (((host-alive?) host)))
		   (https-send-message!
		    #f
		    (lambda (result)
		      (if (frame? result)
			  (if (eq? 200 (get-slot result 'http-status))
			      (let ((msg (call-with-input-string
					  (or (get-slot result 'mind-body) "") read)))
				(set-slot! result 'host host)
				(set-slot! result 'mind-body msg)
				(note result))
			      (loghttps "Q ~a ~a answers: ~a\n"
					oid voter (get-slot result 'http-status)))
			  (note (make-message (property 'error result)
					      (property 'host host)))))
		    (apply make-message (property 'host host) slots)
		    (voter-auth voter) #t)))
       voters)))))

(define (http-get-reply voters oid version note)
  (let ((slots (list
                http-property-get
                http-property-get-reply
		http-property-text/scheme
		(property 'mind-body (number->string version))
                (property 'dc-identifier (oid->string oid)))))
    (parameterize
     ((*http-transfered-headers* http-agreement-headers)
      #;(http-read-status-line-timeout (respond-timeout-interval)))
     (map-parallel
      #t #f 0 #f
      (lambda (voter)
	(and-let* ((host ((host-lookup) voter))
		   (((host-alive?) host)))
	  (https-send-message!
	   #f (lambda (r)
		(and-let*
		 (((and (frame? r)
			(not (memv (get-slot r 'http-status) '(404 412 503 504)))))
		  (dst (get-slot r 'destination))
		  ((eqv? (string->number (car dst)) version)))
		 (set-slot! r 'host host)
		 (set-slot! r 'http-status (string->number (cadr dst)))
		 (set-slot! r 'location (reverse (cddr dst)))
		 (set-slot! r 'destination (cddr dst))
		 (note r)))
	   (apply make-message (property 'host host) slots)
	   (voter-auth voter) #t)))
      voters))))

(define (http-get-digest voter oid version)
  (parameterize
   ((*http-transfered-headers* http-agreement-headers)
    #;(http-read-status-line-timeout (respond-timeout-interval)))
   (let ((result (https-response-auth
		  (apply
		   make-message
		   (cons*
		    (property 'host voter)
		    http-property-get-digest
		    (as-get-for-local-sync-state oid version)))
		  (voter-auth voter)
		  #f)))
     (or (and-let* (((frame? result))
		    (status (get-slot result 'http-status))
		    ((eqv? (quotient status 100) 2)))
		   (set-slot! result 'host voter) result)
	 (begin
	   (loghttps "Q ~a host ~a ~a\n" oid voter
		     (if (frame? result) (get-slot result 'http-status) result))
	   (raise #f))))))

(define (http-get-place oid voter)
  (parameterize
   ((*http-transfered-headers* http-agreement-headers))
   (https-response-auth (make-message
			 http-property-get
			 (property 'dc-identifier (oid->string oid))
			 (property 'host voter)
			 http-property-get-place)
			(voter-auth voter) #t)))

(define *get-meta-cache*
  (let ((cache-duration (make-time 'time-duration 0 2)))
    (make-cache "*get-meta-cache*"
		string=?
		#f ;; state
		;; miss:
		(lambda (cs k) (cons k #f))
		;; hit
		(lambda (c es) (logerr "cache hit on *get-meta-cache* ~a\n" (car es)))
		;; fulfil
		(lambda (c es results)
		  (set-cdr! es (add-duration *system-time* cache-duration))
		  (values))
		;; valid?
		(lambda (es)
		  (or (not (cdr es))
		      (srfi19:time>=? (cdr es) *system-time*)))
		;; delete
		#f
		)))

(define (http-get-meta0 oid voter)
;; If we where to try to avoid calles to dead machines, we provoke a
;; chicken and egg problem when connecting to new machines.
;;
;;   (if (not ((host-alive?) voter))
;;       (raise-service-unavailable-condition (literal voter)))
  (parameterize
   ((*http-transfered-headers* http-agreement-headers)
    #;(http-read-status-line-timeout (respond-timeout-interval)))
   (https-response-auth (make-message
			 http-property-get
			 (property 'dc-identifier (if (oid? oid) (oid->string oid) oid))
			 (property 'host voter)
			 http-property-get-meta)
			(voter-auth voter) #t)))

(define (http-get-meta/cached oid voter)
  (cache-ref *get-meta-cache*
   (format "~a-~a" oid voter)
   (lambda () (http-get-meta0 oid voter))))

(begin
  (define (cleanup)
    (cache-cleanup! *get-meta-cache*)
    (register-timeout-message! 10 cleanup))
  (register-timeout-message! 10 cleanup))

(define http-get-meta http-get-meta0)

(define (http-get-blob oid cs voter)
  (logagree "Q ~a request blob ~a from ~a\n" oid cs voter)
  (parameterize
   ((*http-transfered-headers* http-agreement-headers))
   (https-response-auth (make-message
			 http-property-get
			 (property 'dc-identifier (format "~a/~a" oid cs))
			 (property 'host voter)
			 http-property-get-blob)
			(voter-auth voter) #t)))

(define (http-get-privileges from voter capabilities oid)
  (logagree "Q ~a request privileges ~a from ~a\n" from oid voter)
  (parameterize
   ((*http-transfered-headers* (cons 'capabilities http-agreement-headers)))
   (https-response-auth
    (make-message
     http-property-get
     (property 'dc-identifier (oid->string oid))
     (property 'dc-creator (oid->string from))
     (property 'capabilities capabilities)
     (property 'host voter)
     http-property-get-privileges)
    (voter-auth voter) #t)))

(define (http-get-ca host)
  (parameterize
   ((*http-transfered-headers* http-agreement-headers))
   (http-response (make-message
		   http-property-post
		   http-property-soap+xml
		   http-property-get-ca
		   (property 'dc-identifier (oid->string one-oid))
		   (property 'host host)))))

(define (http-send-certificate-request host req)
  (parameterize
   ((*http-transfered-headers* http-agreement-headers))
   (http-response (make-message
		   http-property-post
		   http-property-soap+xml
		   http-property-get-ca
		   (property 'dc-identifier (oid->string host))
		   (property 'host ((host-lookup) host))
		   (property 'dc-date (timestamp))
		   (property 'mind-body req)))))

(define (http-eval wait-for-result quorum expression)
  (let* ((body (call-with-output-string
				(lambda (port) (write expression port))))
	 (total (if (quorum-local? quorum) (sub1 (quorum-size quorum)) (quorum-size quorum)))
	 (result (cond
		  ((not wait-for-result) #f)
		  ((mailbox? wait-for-result) wait-for-result)
		  (else (make-mailbox 'http-eval))))
         (stop (and (fx>= (string-length body) (large-message-size))
		    (http-sync-point 'http-eval total total)))
         (slots `(,http-property-post
		  ,http-property-text/scheme
                  ,(property 'transfer-encoding (and stop "chunked"))
		  ,http-property-expect-synchronous
                  ,(property 'dc-identifier (oid->string (public-oid)))
                  ,http-property-eval
                  ,(property 'mind-body body))))
    (parameterize
     ((*http-transfered-headers* (cons 'expect http-agreement-headers)))
     (map
      (lambda (voter)
	(let ((host (host-lookup* voter)))
	  (https-send-message!
	   stop
	   (lambda (res) (and result (mailbox-send! result res)))
	   (apply make-message (property 'host host) slots)
	   (voter-auth host)
	   #f)))
      (quorum-others quorum)))
    (and wait-for-result (receive-message! result))))
