; (C) 2000-2002, 2005, 2013 Jörg F. Wittenberger see http://www.askemos.org

;; Beware: The HTTP implementation could use some cleanup.  I'd love
;; to use some foreign code, but we need fine control over the
;; headers.  HTTP allows much better user interfaces than todays toolkits.

;;; Authentication

;; Magic logout help: just enter a blank password.

(define (default-authenticate-www-user! request peer-certificate)
  (define public "public")
  (define public-passwd '())
  (define (authenticated! result request user-name pass)
    (set-slot! request 'web-user user-name)
    (set-slot! request 'secret pass)
    result)
  (let ((user (and (mesh-certificate? peer-certificate)
		   (mesh-cert-o peer-certificate))))
    (cond
     (user
      (or (and-let* ((user-name (entry-name user)))
		    (set-slot! request 'dc-creator user)
;; 		      (set-slot! request 'capabilities
;; 				 (fget (find-frames-by-id oid) 'capabilities))
		    (authenticated! #t request user-name user))
	  (authenticated! #f request public public-passwd)))
     ((or (get-slot request 'authorization)
	  (get-slot request 'proxy-authorization))
      =>
      (lambda (a)
	(receive
	 (start end encoded-string) (parse-basic-authorization a)
	 (let ((info (if start
			 (string-split (pem-decode-string encoded-string) ":")
			 '())))
	   (if (eq? (length info) 2)
	       (let ((user-name (car info))
		     (pass (cadr info)))
		 (if (string=? pass "")
		     (authenticated! #f request public public-passwd)
		     (authenticated! #t request user-name pass)))
	       (authenticated! #f request public public-passwd))))))
     ((and (mesh-certificate? peer-certificate)
	   (mesh-cert-l peer-certificate))
      (set-slot! request 'web-user public)
      #t)
     (else (authenticated! #f request public public-passwd)))))

(define $authenticate-www-user (make-parameter default-authenticate-www-user!))

;; Some black magic to get the login/logout mechanism work.

(define http-require-auth-help
;; <meta http-equiv=\"refresh\" content=\"10;URL=/\"/> -- not really nice
  "<html>
<head><title>access denied</title>
</head>
 <body bgcolor=\"black\" text=\"white\" alink=\"grey\" vlink=\"grey\"
style=\"border: 2px solid grey; padding: 1em;\">
 <h1>access denied</h1>
 <p style=\"border: 2px solid grey; padding: 1em;\"><a href=\"/\">log in</a></p>
 </body>
</html>")

(define http-require-auth-help-length (string-length http-require-auth-help))

(define (http-require-auth http-port protocol http-base-url location realm)
  (display
   (call-with-output-string
    (lambda (http-port)
      (let ((is-proxy (is-proxy-request? location)))
	(display (if is-proxy
		     "HTTP/1.0 407 Proxy Authentication Required\r\n"
		     "HTTP/1.0 401 Unauthorized\r\n")
		 http-port)
	(display "Location: " http-port)
	(display http-base-url http-port)
	(display location http-port)
	(display (if is-proxy
		     "\r\nProxy-Authenticate: Basic realm="
		     "\r\nWWW-Authenticate: Basic realm=")
		 http-port)
	(display realm http-port)
	(display "\r\nContent-Type: text/html\r\nConnection: Keep-Alive\r\nContent-Length: " http-port)
	(display http-require-auth-help-length http-port)
	(display "\r\n\r\n" http-port)
	(display http-require-auth-help http-port))))
   http-port)
  (flush-output-port http-port)
  #f)

(%early-once-only
 (define http-require-auth-message
   (make-message
    (property 'http-status 401)
    (property 'content-type text/html)
    (property 'body/parsed-xml
	      (sxml '(html
		      (head (title "access denied")
			    (meta (@ (http-equiv "refresh")
				     (content  "10;URL=/"))))
		      (body (@ (bgcolor "black") (text "white")
			       (alink "grey") (vlink "grey"))
			    (h1 "access denied")
			    (p (@ (style "border: 2px solid grey; padding: 1em;"))
			       (a (@ (href "/")) "log in")))))))
   ))
;;; Query Parsing

;; Parse "str" as x-urlform-encoded (?).  Deliver a xml element node
;; containing a hild for each assignment with a gi as the variable
;; name and a content as the variable value.  If there is an asignment
;; to "xmlfields" use this as a space separated value list of fields,
;; which contain literal xml data.  Those get their value replaced
;; with the parsed representation.

(define drop-method-spec
  (cons
   (lambda (x r n) (and (xml-element? x)
			(memq (gi x) '(*xmlns*
				       *name*
				       xmlns ; deprecated, will go away.
				       ))))
   xml-drop))

(define form-element-attributes
  (node-list (make-xml-attribute 'f 'xmlns namespace-forms-str)
             (make-xml-attribute 'space namespace-xml "preserve")))

(define core-api-element-attributes
  (node-list (make-xml-attribute 'core 'xmlns namespace-mind-str)
	     (make-xml-attribute 'f 'xmlns namespace-forms-str)))

(define (http-form-element fields)
  (let* ((xmlns0 (select-elements fields '*xmlns*))
         (xmlns (if (node-list-empty? xmlns0)
                    (select-elements fields 'xmlns)
                    xmlns0)))
    (receive
     (nmsp atts)
     (if (node-list-empty? xmlns)
	 (values namespace-forms form-element-attributes)
	 (let* ((d (data xmlns))
		(s (string->symbol d)))
	   (if (or (eq? s 'a) (eq? s 'Askemos) (eq? s 'mind))
	       (values namespace-mind core-api-element-attributes)
	       (values
		s
		(node-list (make-xml-attribute 'form 'xmlns (data xmlns)))))))
     (make-specialised-xml-element
      'form nmsp atts
      ((xml-walk #f #f 'no-root '() '() '() #f fields)
       drop-method-spec)))))

(%early-once-only
 (define http-magic-namespaces '("Askemos" "a" "mind"))
 (define http-magic-fieldnames-str '("" "*xmlns*" "*name*" "xmlns"))
 (define http-magic-fieldnames (map string->symbol http-magic-fieldnames-str))
 )

(define http-parse-query
  (let ((foldf (lambda (spec i)
		  (if (eq? 0 (string-length spec)) i
		      (let ((equal (string-index spec #\=)))
			(cons
			 (cons (if (eq? 0 (string-length spec)) #f
				   (string->symbol
				    (if equal (substring spec 0 equal) spec)))
			       (and equal (make-xml-literal
					   (form-parse
					    (substring spec (add1 equal))))))
			 i))))))
    (define (http-parse-query str)
      (let* ((lst (fold foldf '() (string-split str "&")))
	     (name (cond
		    ((assq '*name* lst) => (lambda (m) (string->symbol (cdr m))))
		    (else 'form))))
	(receive
	 (nmsp atts)
	 (cond
	  ((or (assq '*xmlns* lst)
	       (assq 'xmlns lst))
	   => (lambda (m)
		(if (member (cdr m) http-magic-namespaces)
		    (values namespace-mind core-api-element-attributes)
		    (values
		     (string->symbol (cdr m))
		     (node-list (make-xml-attribute 'form 'xmlns (cdr m)))))))
	  (else (values namespace-forms form-element-attributes)))
	 (make-specialised-xml-element
	  name nmsp atts
	  (fold
	   (lambda (e i)
	     (if (memq (car e) http-magic-fieldnames) i
		 (cons (make-specialised-xml-element
			(car e) nmsp '() ; no attributes
			(if (cdr e) (cdr e) '())) i)))
	   '()
	   lst)))))
    http-parse-query))

(define make-output-form-element
  (let ((atts (list (make-xml-attribute 'space 'xml "preserve")
		    (make-xml-attribute 'method #f "text"))))
    (lambda (name filename content-type data)
      (make-specialised-xml-element
       (string->symbol name)
       namespace-forms
       (if filename
           (cons (make-xml-attribute 'filename #f filename)
                 form-element-attributes)
           form-element-attributes)
       (node-list
        (make-specialised-xml-element
         'output namespace-mind
         (cons*  namespace-coreapi-declaration
                (make-xml-attribute 'media-type #f content-type)
		atts)
         (node-list (make-xml-literal data))))))))

;; mime-split is sort of wild guess work.  Should assert rfc compliance.

(define (rfc2046->node-list body boundary)
  (fold
   (lambda (part init)
     (let* ((msg (call-with-input-string part
                   (lambda (port)
		     (http-read-content
		      port #t ($small-request-limit) ($large-request-handler)))))
            (content-type (or (get-slot msg 'content-type) text/plain)))
       (receive
        (name filename) (content-disposition->name
                         (or (get-slot msg 'content-disposition) ""))
	(let ((body
	       (let ((b (get-slot msg 'mind-body)))
		 ;; Safety check: If there's no mind-body we had
		 ;; no content-length in http-read-content.
		 (if b b
		     (let ((b (substring
			       part (message-body-offset part)
			       (string-length part))))
		       (set-slot! msg 'mind-body b)
		       b)))))
	  (if (and name
		   (or (not filename)
		       (and (not (string-null? filename)))))
	      (cons (make-output-form-element
		     name filename content-type
		     (get-slot msg 'mind-body)) init)
	      init)))))
   '()
   (reverse! (rfc2046-split body boundary))))

(define (mime-split request str)        ; RFC 2046?
  (let ((boundary (multipart-data-boundary
                   (or (get-slot request 'content-type) ""))))
   (if boundary
       (http-form-element
        (rfc2046->node-list (get-slot request 'mind-body) boundary))
       (http-parse-query str))))

(define (http-form->xml! request str)
  (set-slot! request 'body/parsed-xml (mime-split request str))
  (set-slot! request 'content-type text/xml)
  (set-slot! request 'mind-body #f))

;;; Write answer frame as HTTP.

(define (http-format-write-location lst)
  (if (string? lst)
      lst
      (call-with-output-string
       (lambda (port)
	 (clformat port "/POST=~9,48D" (random 268435456))
	 (http-format-location uri-quote-all lst port)))))

(define (http-format-read-location lst)
  (if (string? lst)
      lst
      (call-with-output-string
       (lambda (port) (http-format-location uri-quote-all lst port)))))

(define (webdav-format-write-location lst)
  (if (string? lst)
      lst
      (call-with-output-string
       (lambda (port) (display "/POST" port) (http-format-location uri-quote lst port)))))

(define (webdav-format-read-location lst)
  (if (string? lst)
      lst
      (call-with-output-string
       (lambda (port) (http-format-location uri-quote lst port)))))

(define (http-format-check-location lst)
  (call-with-output-string
   (lambda (port) (display "/CHECK" port) (http-format-location uri-quote lst port))))

(define http-redirect-off #f)           ; only for debugging
(define $user-agent-needs-html
  (make-shared-parameter
   (let ((isgnomevfs (pcre->proc "g.*vfs"))
	 (iswget (pcre->proc "Wget")))
     (lambda (user-agent)
       (and user-agent
	    ;; (not (user-agent-mozilla user-agent))
	    (not (isgnomevfs user-agent))
	    (not (iswget user-agent))
	    (not (user-agent-amaya user-agent)))))))

(%early-once-only

(define soap-actions '("replicate" "echo" "ready" "get-state"
		       "reply-notice" "get-reply"
                       "digest" "get-place" "get-meta" "get-body" "get-blob"
		       "Hello" "X509Certificate" "X509CACertificate"))

(define public-soap-actions '("Hello" "X509Certificate" "X509CACertificate" "get-meta"))

(define http-102-response
  (make-message
   (property 'http-status 102)
   ;; TBD: remove useless content again (only here to aid debugging)
   (property 'content-type text/plain)
   (property 'mind-body "hold on\n")
   ))
 )

(define (http-user-resynchronise! url)
  (let ((location (parsed-locator (substring url 6 (string-length url)))))
    (receive
     (oid quorum) (if (pair? location) (oid-parse (car location)) (values #f #f))
     (if oid
	 (make-message
	  (property 'http-status 200)
	  (property 'location url)
	  (property 'content-type text/html)
	  (property
	   'body/parsed-xml
	   (let* ((test (guard
			 (ex (else #f))
			 (find-frame-by-id/resynchronised+quorum
			  oid (or quorum (replicates-of (find-frames-by-id oid)))))))
	     (sxml `(html (head (title "Check Result"))
			  (body (@ (style
				       ,(if test "background-color:green"
					    "background-color:red")))
				(h1 (a (@ (href "/" ,(car location)))
				       ,(literal oid)))
				(p ,(if test "Succeeded" "Failed"))))))))
	 (make-message
	  (property 'http-status 400)
	  (property 'location url)
	  (property 'content-type text/html)
	  (property
	   'body/parsed-xml
	   (sxml `(html (head (title "Check Error"))
			(body (h1 "Check Error")
			      (p "\"" ,url "\" not exactly an oid!"))))))))))

;; In the middle of a rewrite of this ages old code I never liked
;; cause it was pushed out so fast.  Trying to get server push to work.

;;FIXME: do we need that?
(define *always-send-last-modified* #f)

;; Accoding to rfc 2616 14.21 we should never send an expires header
;; more than one year in the future.  Hence a little less than one
;; year.
(define one-year-duration (make-time 'time-duration 0 (* 360 24 60 60)))
(define one-day-duration (make-time 'time-duration 0      (* 24 60 60)))
(define one-second-duration (make-time 'time-duration 0             1))
(define ten-seconds-duration (make-time 'time-duration 0             10))


(define public-expire-duration (make-shared-parameter one-day-duration))
;;set this to #f if you want "no-cache"
(define user-expire-duration (make-shared-parameter #f)) ;; or one-second-duration 'now

;; (define none-cacheable-response? 
;;   (let ((none-cacheable (list text/xml application/xml)))
;;     (lambda (content-type)
;;       (member content-type none-cacheable))))

(define no-cache-str
  "no-store, no-cache, must-revalidate, post-check=0, pre-check=0")

(define (http-expires answer now default tree)
  (let ((e (or (get-slot answer 'http-expires)
	       (and-let* ((a (get-slot answer 'mind-action-document))
			  ((public-equivalent? a)))
			 one-year-duration)
	       (and-let* ((b (document-element tree)))
			 (cond ((eq? (gi b) 'output) (attribute-string 'expires b))
			       ((eq? (gi b) 'html)
				(attribute-string
				 'content
				 ((sxpath '(head (meta (@ (equal? (http-equiv "Expires")))))) b)))
			       (else #f)))
	       (and-let* ((l (get-slot answer 'last-modified)))
			 (guard
			  (ex (else #f))
			  (* 0.1 (time-second (time-difference now (date->time-utc l))))))
	       default)))
    (cond ((srfi19:date? e) => identity)
	  ((not e) #f)
	  ((eq? e 'now) #f)
	  ((number? e)
	   (and (> e 1) (time-utc->date
			 (add-duration now (make-time 'time-duration 0 (inexact->exact (floor e))))
			 (timezone-offset))))
	  ((string? e)
	   (cond ((string->number e) =>
		  (lambda (n)
		    (and (> n 1)
			 (time-utc->date
			  (add-duration now (make-time 'time-duration 0 n))
			  (timezone-offset)))))
		 ((http-read-date e) => identity)
		 (else #f)))
	  ((srfi19:time? e)
	   (time-utc->date
	    (add-duration
	     now
	     (if (eq? (time-type e) 'time-duration) e
		 (make-time 'time-duration (time-nanosecond e) (time-second e))))
	    (timezone-offset)))
	  (else #f))))

(%early-once-only

(define http-location-header-required?
  (let ((table (make-fixnum-table)))
    (for-each (lambda (state) (hash-table-set! table state #t))
              '(201 300 301 302 303 305 307))
    (lambda (state) (hash-table-ref/default table state #f))))

(define http-state-without-body?
  (let ((table (make-fixnum-table)))
    (for-each (lambda (state) (hash-table-set! table state #t))
	      ;; see RFC2616 4.3: 1xx, 204 304 and HEAD responses
	      ;; must not include a message body. All other responsen
	      ;; do include a message body and it may be of zero length
              '(100 101 204 205 302 303 304 305 307))
    (lambda (state) (hash-table-ref/default table state #f))))

) ; %early-once-only

(define http-property-server (make-shared-parameter #f))

(define the-http-domain (make-shared-parameter "Askemos"))

(define (http-realm id)
  (literal "Basic realm=" (the-http-domain) #\@ (local-id)))

;; TODO http-format-answer is (probably due to the policy of sending
;; headers in predefined order and due to the accidental fact that
;; get-slot for non-existing slots is the least effective thing to do)
;; THE candidate for optimisation.  It takes at 2005-03-17) on average
;; 0.037'' while forward-call (which a complete http get request to
;; another server) takes 0.113''.  The amounts to 0.076'' for WAN
;; connection + read access.  Given that read-only-xslt-method, which
;; was at the remote side took 0.053'', we find approximately 0.023
;; wire time.  Since the xslt transformation is a user code
;; interpreter, it's a completely different thing to optimise there.
;; But http-format-answer slows down the system considerably.  Update
;; 2005-03-30 after some optimisation like collecting larger,
;; logically connected chunks of network packets we saved about 40-50%
;; of the numbers.  Now with the srfi 34 compliant exception handling
;; we appar to save even more.  http-format-answer: 0.012...0.024
;; vs. forward-call 0.12 (questions my conclusion about the reasons of
;; the bottleneck) since read-only-xslt-method, which is now at
;; 0.027-0.044, did not profit from any optimisation here, theres
;; indication to the advantage of the srfi 34 exception handler.

;; Some refactorisation is due.  I can't fix that oversized procedure.

;; (http-format-answer-prepare-body xml string content-type
;;  protocol user-agent)
;; => (<body polymorphic> <content-length int>
;;     <content-type string> <transfer-encoding string[?]> )

(define empty-string-buffer-list '(0 ""))

(: http-format-answer-prepare-body
   ((or false string :blob:)		; plain input
    (or false :xml-nodelist:)		; parsed input
    string				; content-type
    string				; protocol
    (or false string)			; user-agent
    (or false (list-of (pair number number))) ; requeste-range
    ->
    (pair number (list-of string))
    (or boolean :xml-nodelist:)
    number				; length
    (or false string)			; content-type
    (or boolean string)			; "chunked" or #f
    (or false string)			; range header
    ))
(define (http-format-answer-prepare-body str src-parsed content-type
					 protocol user-agent requested-range)
  ;;prepare response entity
  (cond 
   ((not (or src-parsed str))  (values empty-string-buffer-list #f 0 #f #f #f))
   (else
    (let* ((xml      (xml-parseable? content-type))
	   (ct-html  (html-parseable? content-type)) ;; only text/html
	   ;; This heuristic is like any heuristic: mostly broken.
	   (backport-p (($user-agent-needs-html) user-agent))
	   (tree     (or src-parsed
			 (and backport-p
			      (or xml ct-html)
			      (let ((str (body->string str)))
				(guard
				 (ex (else (and ct-html (xml-parse str mode: 'html))))
				 (xml-parse str))))))
	   (html     (or ct-html
			 (and tree
			      (eq? 'html
				   (gi (document-element tree))))))
	   (backport (and backport-p tree html))
	   (content-type (or (and html (content-type-for-html tree))
			     content-type
			     text/plain))
	   (bdy (or (and (or (not backport) ct-html) str)
		    (let ()
		      (cond
		       ((node-list? tree)
			((if backport html-format/list xml-format/list)
			 tree))
		       (else (or str ""))))))
	   (length0 (cond
		     ((string? bdy) (string-length bdy))
		     ((a:blob? bdy)
		      (or (a:blob-size bdy)
			  ;; FIXME should not happen
			  (and
			   (not (bhob? bdy))
			   (blob-notation-size *the-registered-stores* bdy #f))
			  ))
		     (else (car bdy))))
	   (length (if requested-range
		       (min length0 (add1 (- (min (cdar requested-range) (sub1 length0))
					     (min (caar requested-range) length0))))
		       length0))
	   (content-range (and requested-range
			       (format "bytes ~a-~a/~a"
				       (min (caar requested-range) length0)
				       (min (cdar requested-range) (sub1 length0))
				       length0)))
	   (chunked? (use-chunked-encoding? protocol 'dummy-answer length))
	   (transfer-data
	    (cond
	     ((string? bdy)
	      (cons length
		    (list (if content-range
			      (substring bdy
					 (min (caar requested-range) length0)
					 (min (add1 (cdar requested-range)) length0))
			      bdy))))
	     ((bhob? bdy)
	      (list length
		    (if content-range
			(bhob->string/anyway bdy (public-oid) *local-quorum*
					     (min (caar requested-range) length0)
					     (min (add1 (cdar requested-range)) length0))
			(bhob->string/anyway bdy (public-oid) *local-quorum* 0 (a:blob-size bdy)))))
	     ;; FIXME that should go as a range info to display-notation
	     ((a:blob? bdy)
	      (list length (if content-range
			       (let ((str (body->string bdy)))
				 (substring str
					    (min (caar requested-range) length0)
					    (min (add1 (cdar requested-range))
						 length0)))
			       bdy)))
	     ((and content-range (pair? bdy))
	      (cons length
		    (let loop ((bdy (cdr bdy))
			       (skip (caar requested-range)))
		      (if (fx>= skip (string-length (car bdy)))
			  (loop (cdr bdy)
				(- skip (string-length (car bdy))))
			  (let loop ((bdy (if (> skip 0)
					      (cons (substring
						     (car bdy) skip
						     (string-length (car bdy)))
						    (cdr bdy))
					      bdy))
				     (length length))
			    (if (fx>= length 0)
				(if (fx>= (string-length (car bdy)) length)
				    (if (eqv? (string-length (car bdy)) length)
					bdy
					(list (substring (car bdy) 0 length)))
				    (cons (car bdy)
					  (loop (cdr bdy)
						(- length (string-length (car bdy))))))
				'()))))))
	     (else bdy))))
      (cond
       (chunked?
	(values transfer-data
		tree
		length (and (fx>= length 1) content-type) "chunked" content-range))
       (else
	(values transfer-data
		tree
		length (and (fx>= length 1) content-type) #f content-range)))))))

;; TODO This is simplified, we'll ignore complicated cases for now.
(define parse-range-header
  (let ((m (pcre/a->proc-uncached "bytes=([0-9]+)-([0-9]+)")))
    (define (range-header-parse str)
      (and-let* ((x (m str)))
		(list (cons (string->number (cadr x)) (string->number (caddr x))))))
    range-header-parse))

(define http-redir-kludge-time 200) ;; 200

(define (http-legal-response? obj)
  (or (frame? obj) (condition? obj)))

(define (http-answer-handle-location! host scheme answer state location)
  (if (http-location-header-required? state)
      (set-slot! answer 'http-location
		 (if (is-proxy-request? location) location
		     (if (eqv? (string-ref location 0) #\/)
			 (string-append scheme host location)
			 (begin
			   (logerr "coding error wrong location ~a\n" location)
			   (string-append scheme host "/" location)))))))

(define (http-format-answer ipaddr host port
			    protocol 
			    connection
                            scheme
                            method 
			    requested
			    requested-range
                            user-agent
                            answer expires)
  (let* ((now *system-time*)
	 (date (timestamp))
	 (answer 
	  (cond ((message-condition? answer)
		 (condition->message #f (lambda (x) #f) answer))
		((not (frame? answer)) ; error
		 (condition->message
		  "http-format-answer: not a frame" (lambda (x) #f) answer))
		((get-slot answer 'error) ;; DEPRECIATED old style usercode error
		 (logerr "http-format-answer: DEPRECIATED slot 'error' used ~a
this code will break in the future\n"
			 (get-slot answer 'error))
		 (if (and (eq? (ns (get-slot answer 'body/parsed-xml))
			       namespace-soap-envelope)
			  (not user-agent))
		     answer
		     (let ((new (make-html-error
				 (or (get-slot answer 'http-status) 400)
				 (get-slot answer 'location)
				 "Error" (get-slot answer 'error)
				 (message-body answer))))
		       (set-slot! new 'dc-creator  (get-slot answer 'dc-creator))
		       new)))
		(else answer)))
	 (orig-body-parsed (get-slot answer 'body/parsed-xml))
	 (orig-content-type (get-slot answer 'content-type))
	 (is-head-request? (string=? method "HEAD")))
    (receive
     (bdy body-parsed content-length content-type transfer-encoding content-range)
     ;; suppress user code message body
     ;; HINT for some responses a short message, describing for 
     ;; instance the redirection, could be included
     (cond
      ((or (member method http-commands-without-reply-body)
	   (eq? orig-body-parsed has-been-forwarded-nl))
       (values empty-string-buffer-list #f 0 #f #f #f))
      ((and (not requested-range) (member method soap-actions))
       (let* ((bdy (message-body/plain answer))
	      (length (message-body/plain-size answer)))
	 (values (list length (or bdy ""))
		 #f length orig-content-type
		 (and (use-chunked-encoding? protocol 'dummy-answer length) "chunked")
		 #f)))
      (else (http-format-answer-prepare-body
	     (get-slot answer 'mind-body) ;; Was wrongly (message-body/plain answer)
	     orig-body-parsed orig-content-type
	     protocol user-agent requested-range)))

     (if is-head-request? (set! transfer-encoding #f)) ; igitt

     ;; Need to keep the old value, since the answer might be cached internaly.
     ;; (set-slot! answer 'body/parsed-xml #f) ; don't use that one any longer
     (set-slot! answer 'mind-body (if is-head-request? empty-string-buffer-list bdy))
     (if (not (equal? orig-content-type content-type))
	 (set-slot! answer 'content-type content-type))
     (if transfer-encoding (set-slot! answer 'transfer-encoding transfer-encoding))
     (if content-range (set-slot! answer 'content-range content-range))

     (let* ((state0 (or (get-slot answer 'http-status)
			(if content-range 206 200)))
	    (location (let ((location (get-slot answer 'location)))
			(if location
			    ((if (eqv? state0 207)
				 webdav-format-read-location http-format-read-location)
			     location)
			    (uri-path requested))))
	    ;; redirection
	    (state  (cond
		     ;; The presence of a Range header in a
		     ;; conditional GET (using both of If-* header)
		     ;; modifies what is returned if the GET is
		     ;; otherwise successful and the condition is
		     ;; true. It does not affect the 304 (Not
		     ;; Modified) response returned if the conditional
		     ;; is false.
		     ((and (fx>= state0 200) (< state0 300) requested-range)
		      206)
		     ((and (eqv? state0 408)
			   (or (string=? method "POST")
			       (string=? method "PUT")
			       (and (not (string=? method "PROPFIND"))
				    (member method webdav-commands))))
		      ;; Let's see whether clients will re-post on 503
		      ;; as well as they do on 408.
		      503)
		     ;; FIXME: these specials ought to go out completely!
		     ((or (equal? location (uri-path requested))
			  (not (eqv? state0 200))
			  (member method soap-actions)
			  http-redirect-off)
;; Dunno: this seemed to be a sensible mesure before.  Chances are it's not.
;; 		      (if (or (eqv? state0 503) (eqv? state0 412))
;; 			  (thread-sleep! (or (respond-timeout-interval) 2)))
		      ;; FIXME: BUG work around; how is it possible
		      ;; that a PUT fast followed by a PROPFIND to the
		      ;; same file yields a 404?
		      (if (and (eqv? state0 201) http-redir-kludge-time)
			  (thread-sleep!/ms http-redir-kludge-time))
		      state0)
		     ((or (equal? protocol "HTTP/1.0")
			  ;; broken clients may not understand '303
			  (broken-303-client (or user-agent "")))
		      (and http-redir-kludge-time
			   (thread-sleep!/ms http-redir-kludge-time)) ;; delay redirection 
		      302)
		     (else
		      ;; (logerr "Redir ~a => ~a\n" (uri-path requested) location)
		      (and http-redir-kludge-time
			   (thread-sleep!/ms http-redir-kludge-time)) ;; delay redirection 
		      303))))

       (if (and (eqv? state 400) ($http-server-verbose))
	   (logerr "~a ~a\n~a\n" method location bdy))

       (if (not (eqv? state0 302))
	   (http-answer-handle-location! host scheme answer state location))

       ;; set defaulted entity header
       (if (and (or (eqv? state 405)
		    (member method '("PUT" "OPTIONS")))
		(not (get-slot answer 'allow)))
	   ;; if not set from user code set a default Allow Header
	   (set-slot! answer 'allow '("GET" "HEAD" "POST" "PUT" "OPTIONS")))

       ;; Sanity check logged.
       (if (and (fx>= content-length 1) (http-state-without-body? state))
	   (begin
	     (set! content-length 0)
	     (set! transfer-encoding #f)
	     (set-slot! answer 'body/parsed-xml #f)
	     (set-slot! answer 'mind-body empty-string-buffer-list)
	     (set-slot! answer 'transfer-encoding #f)
	     (if (and ($http-server-verbose)
		      (not (or (eqv? state 302) (eqv? state 303))))
		 (logerr "warning ~a ~a status ~a suppressed strange body ~a\n"
			 method location state bdy))))

       (if is-head-request?
	   (if (fx>= content-length 1)
	       (set-slot! answer 'content-length content-length))
	   (set-slot! answer 'content-length (and (not transfer-encoding)
						  content-length)))

       ;; We are supposed to deliver the current moment in the Date
       ;; header.
       (set-slot! answer 'dc-date date)

       (let* ((body (get-slot answer 'mind-body))
	      (expire (and (string=? method "GET")
			   (eqv? 2 (quotient state 100))
			   (http-expires answer now expires body-parsed)))
	      (headers (call-with-output-string
			(lambda (port)
			  ;; write the response message
			  ;; FIXME: "Vary: Accept" needs to be moved
			  ;; to a appropriate place neea cache control.
			  (format port "~a ~a ~a\r\nVary: Accept\r\n" protocol state
				  (http-status-string state))
;;; FIXME Das ist unschoen, hier werden Werte im Heap
;;; zusammengestellt, die sich leicht einspaaren und direkt ausgeben
;;; lassen.  Das geschieht jedoch mit dem Hintergrund, dass auf diese
;;; Weise lediglich ein Paket Daten zum sslmgr geschrieben wird.  So
;;; dass ein erheblicher Geschwindigkeitsgewinn erzielt wird.
			  (http-write-general-headers
			   answer port
			   (let ((cc (and (not expire) (string=? method "GET") ))
				 (v1 (string=? protocol "HTTP/1.0")))
			     (lambda (h)
			       (case h
				 ((connection) connection)
				 ((http-pragma) (and cc v1 "no-cache"))
				 ((cache-control) (and cc (not v1)
						       ;; no-cache-str
						       "no-cache"))
				 (else #f)))))
			  (http-write-response-headers 
			   answer port
			   (lambda (h)
			     (case h
			       ((www-authenticate)
				(and (eqv? state 401) (http-realm 'id)))
			       ((proxy-authenticate)
				(and (eqv? state 407) (http-realm 'id)))
			       ((server) (property-value (http-property-server)))
			       (else #f))))
			  (http-write-entity-headers
			   answer port
			   (lambda (h)
			     (case h
			       ((expires) expire)
			       ((last-modified)
				(and *always-send-last-modified* date))
			       (else #f))))
			  (dav-write-entity-headers answer port (lambda (x) #f))

			  ;; BEWARE test to Fix M$ Webfolder mounts
			  (if ((pcre->proc
				"^Microsoft Data Access Internet Publishing Provider")
			       (or user-agent ""))
			      (display "MS-Author-Via: DAV\r\n" port))
			  (if (equal? method "replicate")
			      (http-write-header 'location (get-slot answer 'location) port))
			  ;; Only few and seldom used messages in the
			  ;; synchronisation protocol have a response body.  Anyway
			  ;; those are short (beyond $immediate-body-size, except
			  ;; for replication) and do not cause an extra packet.  I
			  ;; guess this improvement is not worth the jumps the code
			  ;; has to go through.
			  (if (not transfer-encoding) (display "\r\n" port))
			  (if body
			      (if (not (fx>= (car body) $immediate-body-size))
				  (http-write-body-plain-no-timeout body port)))))))
	  (guard
	   (ex (else
		(if ($http-server-verbose)
		    (if (eof-condition? ex)
			(logerr "client ~a closed connection ~a\n"  ipaddr (condition->string (eof-condition-reason ex)))
			(log-condition "format-answer close" ex)))
		(set! connection http-close)))
	   (display headers port)
	   (if (and body (fx>= (car body) $immediate-body-size))
	       (if transfer-encoding
		   (http-write-body-chunked body port #f)
		   (http-write-body-plain body port)))	  
	   (flush-output-port port))
	  (log-access!
	   host ipaddr (get-slot answer 'dc-creator) date
	   method requested protocol state (car body))
	  (not (equal? connection http-close)))))))


;;; Serve a single HTTP request

;; There is again some black magic here.  All requests, with magic
;; prefix "/POST.*" are redirected without that prefix.  This tricks
;; the client into friendly cleaning up the direct answer from it's
;; cache.  This avoids the stupid "result from post operation is no
;; longer available, reload"-question.

;; The rest is pretty straight: the request is converted into a frame,
;; which is given to the "answer-function", which gives a result frame
;; to reply with.

;; FIXME: This might better work with raw input instead of ports?!
;; Expect most of the traffic to be the body, which is read 1by1 here
;; for sake of slightly simpler header parsing.

;; TODO clean this code up a lot!  We need to store a continuation
;; somewhere, which http-format-answer at the connection.  That way
;; it'll be easy to implement updates at the browser window.  If you
;; dunno what I'm talking about see the explanation at xmlblaster.

(register-location-format! 'http-location-format
                           (cons http-format-read-location
                                 http-format-write-location))

(register-location-format! 'webdav-location-format
                           (cons webdav-format-read-location
                                 webdav-format-write-location))

(define (init-http!)
  (if (not (member "GET" http-legal-commands))
      (begin
        (set! http-legal-commands (append http-commands-without-body
                                          http-commands-with-body))))
  ;; FIXME: move this to bopcntrl.scm
  (http-keep-alive-timeout 300)
  (http-read-chunked-header-timeout (http-keep-alive-timeout))
  (http-read-trailer-timeout (http-keep-alive-timeout))

  (short-local-timeout 0.200)		; send all request headers
  (http-read-header-timeout 5)		; read all request headers

  ;; (ssl-setup-timeout 5)
  #t)

(init-webdav! init-http!)

(define http-request-line
  ;; BEWARE: if a https connection is made, expect funny errors like
  ;; "<pcre-error> code -10: PCRE_ERROR_BADUTF8" from request line parsing.
  ;; We should do it different to keep the log file clean.
  (pcre->proc "^([[:upper:]]+) +([^ ]+) +(HTTP/\\S+)\\r?$"))

(define http-form-encoded?
  (pcre->proc "^(multipart/form-data)|(application/x-www-form-urlencoded)"))

(define http-is-logout-url?
  (pcre->proc "^/LOGOUT(?:/([^/]+))?(.*)?"))

(define $http-magic-post-get (make-shared-parameter #f))

(define (http-get-request-line timeout peer port)
  (guard
   (ex ((eof-condition? ex)
	(if ($http-server-verbose)
	    (logerr "client ~a closed connection in keep alive state\n" peer)) #f)
       ((timeout-object? ex)
	(if ($http-server-verbose)
	    (logerr "keep alive timeout for ~a\n" peer)) #f)
       (else (raise ex)))
   (expect-object timeout raise string? read-line port)))

(define bhob-large-request-handler
  (let ((bhob-to-large ($large-request-handler))
	(unreached-large-handler (lambda args
				   (raise 'unreachable-bhob-handler-called))))
    (define (bhob-large-request-handler
	     port request large
	     add-chunk initial-chunks reduce-chunks
	     total token
	     recurse)
      (receive
       (bt-add bt bt-unit)
       (make-blocktable-accumulator
	(public-oid) *local-quorum*
	($default-sql-block-size) ($default-sql-bpb))
       (recurse
	port request
	#f			; large => ought to check some limit (disk)
	bt-add
	(if initial-chunks (bt-add initial-chunks bt) bt)
	bt-unit
	total token
	unreached-large-handler)))
    bhob-large-request-handler))

(define (http-get-request peer peer-certificate in-port cmd uri http-version)
  (let ((request (if (string=? cmd "PUT")
		     (http-read-content
		      in-port #f ($large-request-size)
		      bhob-large-request-handler)
		     (http-read-content
		      in-port #f ($small-request-limit)
		      ($large-request-handler)))))
    (let ((host (or (uri-authority uri) ;; strip userinfo! (uri-host-port uri) 
		    (get-slot request 'host)
		    (local-id)))
	  (url (or (uri-path uri) "/"))
	  (connection (cond ((and-let* ((h (get-slot request 'connection)))
				       (ormap http-close-match (string-split h ",")))
			     http-close)
			    ((string=? "HTTP/1.0" http-version) http-close)
			    (else "keep-alive")))
	  (authenticated? (($authenticate-www-user) request peer-certificate)))
      
      (set-slot! request 'request-method cmd) ; try to get rid of that one
      (set-slot! request 'http-peer peer)
      (set-slot! request 'ssl-peer-certificate peer-certificate)
      (set-slot! request 'host host)

      (values request host connection url authenticated?))))

(define (http-dispatch-request
	 answer-function peer-certificate location-format auth
	 user action cmd uri url request authenticated?)
  (cond
   (action
    (set-slot! request 'dc-identifier (substring url 1)) ;; see how to remove this substring
    (let ((a (and peer-certificate 
		  (http-affirm-authorization peer-certificate))))
      (cond 
       (a
	(set-slot! request 'authorization a))
       ((member action public-soap-actions)
	(set-slot! request 'authorization (public-oid)))
       (else
	(raise (make-condition
		&http-effective-condition 'status 502
		;; (We used to return status 401, but this causes the
		;; client (browser) to ask the user for a
		;; name/password - no solution for the client's lack
		;; of a X509 certificate.)
		'message (format "Query from ~a rejected."
				 peer-certificate)))))
      (webdav-process cmd answer-function request)))

   ((not (member cmd http-legal-commands))
    (if ($http-server-verbose)
	(logerr "server command ~a not enabled on ~a\n" cmd url))
    (make-html-error
     501 url "Not Implemented"
     (string-append cmd " not implemented!") '()))

   ((string=? "*" url)
    (make-message (property 'allow http-legal-commands)
		  (property 'dc-creator
			    (and (my-oid) (entry-name->oid user)))))

   ((http-is-check-url? url) (http-user-resynchronise! url))

   ((http-is-logout-url? url) =>
    (lambda (match)
      (let ((last-user (and-let* ((r (cadr match)) ((not (equal? r "")))) r)))
	(make-message 
	 (property 'dc-creator (and (my-oid) (entry-name->oid user)))
	 (property 'location (cond ((not last-user)
				    (string-append "/LOGOUT/" user))
				   ((equal? (caddr match) "") "/")
				   (else (caddr match))))
	 (property 'http-status (if (and last-user (string=? user last-user))
				    (if (absolute-uri? uri) 407 401)
				    303))
	 (property 'content-type text/html)
	 (property 'mind-body http-require-auth-help)))))

   ((and auth (not authenticated?)) http-require-auth-message)

   (else
    (let ((content-type (ensure-request-content-type! cmd url request)))
      (if (uri-query uri)
	  (http-form->xml! request (uri-query uri))
	  (if (and (string=? cmd "POST")
		   (http-form-encoded?
		    (or content-type
			(raise
			 (make-condition &http-effective-condition
					 'status 415
					 'message "content-type missing")))))
	      (http-form->xml! request (get-slot request 'mind-body)))))

    (let ((is-post (http-is-post-url? url)))
      (if (and is-post (string=? cmd "GET")
	       ($http-magic-post-get))
	  (begin (set-slot! request 'request-method "POST")
		 (set-slot! request 'expect "X-Askemos-synchronous")))
      (set-slot! request 'dc-identifier (or is-post url)))
    (set-slot! request 'location-format location-format)
    (let ((response (webdav-process cmd answer-function request)))
      (if (eq? response *not-authorised*)
	  http-require-auth-message
	  response)))))

(define *public-read-cache*
  (make-cache "*public-read-cache*"
	      string=?
	      #f ;; state
	      ;; miss:
	      (lambda (cs k) (cons k #f))
	      ;; hit
	      #f ;; (lambda (c es) #t)
	      ;; fulfil
	      (lambda (c es results)
		(set-cdr! es (add-duration *system-time* ten-seconds-duration))
		(values))
	      ;; valid?
	      (lambda (es)
		(or (not (cdr es))
		    (srfi19:time>=? (cdr es) *system-time*)))
	      ;; delete
	      #f
	      ))

(begin
  (define (cleanup-public-read-cache)
    (cache-cleanup! *public-read-cache*)
    (register-timeout-message! 10 cleanup-public-read-cache))
  (register-timeout-message! 10 cleanup-public-read-cache))

(define (http-dispatch-request/cache
	 cache-key
	 answer-function peer-certificate location-format auth
	 user action cmd uri url request authenticated?)
  (if (and (not (or authenticated? auth)) (string=? cmd "GET"))
      (cache-ref
       *public-read-cache*
       (string-append (or (get-slot request 'host) "")":" cache-key)
       (lambda ()
	 (http-dispatch-request
	  answer-function peer-certificate location-format auth
	  user action cmd uri url
	  (message-clone request)
	  authenticated?)))
      (http-dispatch-request
       answer-function peer-certificate location-format auth
       user action cmd uri url request authenticated?)))

(define $http-requests-per-connection (make-shared-parameter 5))
(define $https-requests-per-connection (make-shared-parameter #t))

(define $http-server-verbose (make-shared-parameter #t))
(define $http-log-access-local-id (make-shared-parameter #f))

(define *not-authorised* '(*not-authorised*))

;; If you set this, you will see exceptions: "<os-error> error in
;; system call shutdown(33,0) (Transport endpoint is not connected)"

(define *http-request-server-dont-care-on-close* #t)

(define (http-request-server peer peer-certificate in-port http-port
                             auth scheme location-format
                             answer-function)
  (define (receive-error ex cmd uri http-version)
    (let ((answer (cond ((eq? ex 'connection-captured) (raise ex))
			((message-condition? ex)
			 (if ($http-server-verbose)
			     (log-condition "http-get-request close" ex))
			 ex)
			((eof-condition? ex) ;; eof/partial-read
			 ;; we don't respond here since we probably would get an EPIPE
			 (if ($http-server-verbose)
			     (log-condition "http-get-request close" ex))
			 #f)
			((timeout-object? ex)
			 (if ($http-server-verbose)
			     (logerr "http-get-request close: Timeout\n"))
			 (make-html-error
			  408 (uri-path uri) "Timeout"
			  "Timeout while reading request entity" '()))
			(else
			 (receive  ;; system and coding error
			  (title msg args rest) (condition->fields ex)
			  (logcond "http-get-request close: " title msg args)
			  (make-html-error
			   500 (uri-path uri) title msg args))))))
      (and answer
	   (not *http-request-server-dont-care-on-close*)
	   (http-format-answer
	    peer (local-id) http-port http-version http-close scheme cmd
	    uri #f #f answer #f))))

  (let loop ((n (if peer-certificate
		    ($https-requests-per-connection)
		    ($http-requests-per-connection)))
	     (timeout (http-keep-alive-timeout)))

    ;; We accept one more connection than as the parameter would
    ;; suggest to avoid stupid noop's for n==0.
    (let ((request-line (and (or (and (number? n) (fx>= n 0)) n)
			     (http-get-request-line timeout peer in-port))))
      (if (string? request-line)
	  ;; That's pretty strange.  I'd like to just call "loop" from
	  ;; within the guard clause.  But if then some exception stack
	  ;; is printed by rscheme, it it HUGE.  So I resort to
	  ;; call-with-values, to explicitely unwind the exception
	  ;; stack.  There shoud be a better solution, though.
	  (let ((m (http-request-line request-line)))
	    (if m
		(let ((cmd (cadr m))
		      (uri (uri (caddr m)))
		      (http-version (cadddr m)))

		  (update-alive-time-from-server! peer-certificate)

		  (yield-system-load)

		  (call-with-values
		      (lambda ()
			(guard
			 (ex (else (receive-error ex cmd uri http-version)
				   (values #f #f)))
			 (receive
			  (request host connection url authenticated?)
			  (http-get-request peer peer-certificate in-port cmd uri http-version)
			  (if
			   (and (string=? cmd "CONNECT") (http-connect-back-magic? uri)
				peer-certificate)
			   (let ((c (http-find-channel (host-lookup* (mesh-cert-l peer-certificate)))))
			     (http-format-answer
			      peer host http-port http-version "keep-alive" scheme cmd uri #f #f
			      (make-message (property 'http-status 204))
			      #f)
			     (if (< ($http-client-connections-maximum)
				    (bidirectional-connection-cache-number-of-items
				     (http-channel-connection-cache c)))
				 (http-close-one-old-connection! c))
			     (begin
			       (raise 'connection-captured)))
			   (let ((user (get-slot request 'web-user))
				 (action (get-slot request 'soapaction))
				 (range (and-let* ((h (get-slot request 'range))) 
						  (parse-range-header h)))
				 (agent (get-slot request 'user-agent))		  
				 (from (or (and-let* ((($http-log-access-local-id))
						      ((get-slot request 'soapaction))
						      (a (get-slot request 'authorization))
						      ((string? a)))
						     a) 
					   peer))
				 (keep-time (string->number (or (get-slot request 'keep-alive) "")))
				 (accept (get-slot request 'accept))
				 (reply (make-checked-mailbox "reply-cannel" http-legal-response?)))
			     (set-slot! request 'reply-channel reply)
			     (set-slot! request 'uri uri)
			     (let* ((cmd (or action cmd))
				    (send-102 (and (not (string=? http-version "HTTP/1.0"))
						   (member cmd webdav-command-expecting-102))))
			       (if send-102
				   (!start
				    (mailbox-send!
				     reply
				     (guard
				      (c (else (condition->message
						#f (lambda (s)
						     (cond ((eq? s 'accept) accept)
							   (else #f))) c)))
				      (http-dispatch-request
				       answer-function peer-certificate location-format
				       auth user action cmd uri url request authenticated?)))
				    cmd))
			       (values
				(and
				 (if send-102
				     (let* ((to102 (* (or (respond-timeout-interval) 15) 2))
					    (sig (lambda ()
						   (mailbox-send! reply http-102-response))))
				       (let loop ((te102
						   (register-timeout-message! to102 sig)))
					 (let ((response (receive-message! reply)))
					   (if (or (eq? response http-102-response)
						   (and (frame? response) ;; TBD: use assert instead.
							;; TBD: doe we need this test?
							(eqv? (get-slot response 'http-status) 102)))
					       (begin
						 (http-format-answer
						  from host http-port http-version
						  "keep-alive" scheme cmd uri range agent
						  ;; FIXME: mutating the reply message in the protocol adaptor
						  ;; never was a good idea
						  (make-message (property 'http-status 102)) #f)
						 (loop
						  (register-timeout-message! to102 sig)))
					       (begin
						 (cancel-timeout-message! te102)
						 (http-format-answer
						  from host http-port http-version
						  (or (and (number? n) (fx>= n 1) connection)
						      (and (eq? n #t) connection)
						      http-close)
						  scheme (or action cmd) uri range agent
						  response
						  ;; It seems as if search engines need some more timeout.
						  (if (string=? user "public")
						      (public-expire-duration)
						      (user-expire-duration))))))))
				     (http-format-answer
				      from host http-port http-version
				      (or (and (number? n) (fx>= n 1) connection)
					  (and (eq? n #t) connection)
					  http-close)
				      scheme cmd uri range agent
				      (guard
				       (c (else (condition->message
						 #f (lambda (s)
						      (cond ((eq? s 'accept) accept)
							    (else #f))) c)))
				       (http-dispatch-request/cache
					(caddr m)
					answer-function peer-certificate location-format
					auth user action cmd uri url request authenticated?))
				      ;; It seems as if search engines need some more timeout.
				      (if (and (not auth) (string=? user "public"))
					  (public-expire-duration)
					  (user-expire-duration))))
				 (if (boolean? n) n (sub1 n)))
				(let ((default (timeout-second
						(http-keep-alive-timeout))))
				  (if keep-time
				      (if default
					  (if (< keep-time default) keep-time default)
					  keep-time)
				      default)))))))))
		    loop))
		(receive-error 
		 (make-condition
		  &message 'message
		  (string-append 
		   "malformed request line from " (to-string peer) ": "
		   (if (condition? request-line) (condition->string request-line) request-line)))
		 "???" (uri "/") "HTTP/1.0")))))))

(define (hostspec->ip host)
  (if (or (not host) (char-numeric? (string-ref host 0)))
      host
      (and-let* ((ns (dns-find-nameserver))) (dns-get-address ns host))))

(define (http-server host port auth location-format answer-function)
  (askemos:run-tcp-server
   (hostspec->ip host) port
   (lambda (in-port out-port peer)
     (guard
      (c ((or (timeout-object? c) (eof-object? c)) c)
	 (else 
	  (receive
	   (title msg args rest) (condition->fields c)
	   (logcond "HTTP EXCEPTION" title
		    (if (message-condition? c) (condition-message c) msg) args))))
      (http-request-server peer #f in-port out-port
			   (auth) "http://" location-format
			   answer-function)))
   parallel-requests-semaphore
   #f
   "webrequest"))

(define $https-server-require-cert (make-shared-parameter #f))

(define (https-server host port acert auth location-format answer-function)
  (define certificate (and-let* (((pair? acert))
				 (k (assq 'cert acert)))
				(cdr k)))
  (define key (and-let* (((pair? acert))
			 (k (assq 'key acert)))
			(cdr k)))
  (define require-cert (and-let* (((pair? acert))
				  (k (assq 'require acert)))
				 (cdr k)))
  (define ca (and-let* (((pair? acert))
			(k (assq 'ca acert)))
		       (cdr k)))
  
  (askemos:run-ssl-server
   (hostspec->ip host) port
   `(
     ,(if require-cert "-E" "-e")
     "-c" ,certificate			; "self.cert"
     "-k" ,key				; "self.private"
     "-A" ,ca				; "root.cert"
     "-T" ,ca				; "root.cert"
     )
   (lambda (in-port out-port peer peer-certificate)
     (http-request-server peer peer-certificate
			  in-port out-port
			  auth "https://" location-format
			  answer-function))
   (lambda (c)
     (if (not (or (timeout-object? c) (eof-object? c)))
	 (receive
	  (title msg args rest) (condition->fields c)
	  (logcond 'HTTPS-EXCEPTION
		   title (if (message-condition? c) (condition-message c) c) args)))
     c)
   parallel-secured-requests-semaphore
   (lambda (cert from to alive? close)
     (assert (mesh-certificate? cert))
     (let* ((name (mesh-cert-l cert))
	    (host (host-lookup* name))
	    (channel (http-find-channel host)))
       ;; (http-close-channel! host) that's too much, we should not close callback connections
       (http-set-channel-callback! channel)
       (http-client-dispose-connection
	channel
	(%allocate-bidirectional-connection
	 host cert alive? close from to)
	'collect)
       (or (http-host-alive? host)
	   (cache-invalid! *http-alive-cache* host))))
   "sslrequest"))
