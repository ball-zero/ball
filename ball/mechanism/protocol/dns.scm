(define-condition-type &dns-error &message
  dns-error?)

(define (raise-dns-error what)
  (raise (make-condition &dns-error 'message what)))

(define types
  '((a 1)
    (ns 2)
    (md 3)
    (mf 4)
    (cname 5)
    (soa 6)
    (mb 7)
    (mg 8)
    (mr 9)
    (null 10)
    (wks 11)
    (ptr 12)
    (hinfo 13)
    (minfo 14)
    (mx 15)
    (txt 16)
    (rp 17)
    (aaaa 28)
    (dname 39)
    (* 255)))

(define classes
  '((in 1)
    (cs 2)
    (ch 3)
    (hs 4)
    (none 254)
    (* 255)))

(define (cossa i l)
  (cond
   ((null? l) #f)
   ((equal? (cadar l) i)
    (car l))
   (else (cossa i (cdr l)))))

(define-macro (type->number type)
  (let ((tmp (gensym)))
    `(let ((,tmp (assoc ,type types)))
       (if ,tmp (cadr ,tmp) (raise-dns-error ,type)))))

(define-macro (class->number class)
  (let ((tmp (gensym)))
    `(let ((,tmp (assoc ,class classes)))
       (if ,tmp (cadr ,tmp) (raise-dns-error ,class)))))

(define (dns-rcode->string rcode)
  (case rcode
    ((1) "format error")
    ((2) "server failure")
    ((3) "name error")
    ((4) "not implemented")
    ((5) "refused")
    ((9) "not authoritative")))

(define (number->octet-pair n)
  (list (integer->char (arithmetic-shift-right n 8))
        (integer->char (modulo n 256))))

(define (octet-pair->number a b)
  (+ (arithmetic-shift-left (char->integer a) 8)
     (char->integer b)))

(define (octet-quad->number a b c d)
  (+ (arithmetic-shift-left (char->integer a) 24)
     (arithmetic-shift-left (char->integer b) 16)
     (arithmetic-shift-left (char->integer c) 8)
     (char->integer d)))

(define dotted-match
  (let ((match dotted-match-regex))
    (define (dotted-match x)
      (string-match match x))
    dotted-match))

(define (name->octets s)
  (let ((do-one (lambda (s)
                  (cons
                   (integer->char (string-length s))
                   (string->list s)))))
    (let loop ((s s))
      (let ((m (dotted-match s)))
        (if m
            (append
             (do-one (cadr m))
             (loop (caddr m)))
            (append
             (do-one s)
             (list #\nul)))))))

(define (make-std-query-header id question-count)
  (append
   (number->octet-pair id)
   (list #\x1 #\nul) ; Opcode & flags (recusive flag set)
   (number->octet-pair question-count)
   (number->octet-pair 0)
   (number->octet-pair 0)
   (number->octet-pair 0)))

;; http://www.rfc-archive.org/getrfc.php?rfc=2136

(define (make-update-query-header update-count)
  (append
   (number->octet-pair (random 256))
   (list #\( ; (integer->char (arithmetic-shift-left 5 4))
	 #\nul) ; Opcode & flags
   (number->octet-pair 1)
   (number->octet-pair 0) ; prerequisite section
   (number->octet-pair update-count)
   (number->octet-pair 0) ; additional data section
   ))

(define (make-query id name type class)
  (append
   (make-std-query-header id 1)
   (name->octets name)
   (number->octet-pair (type->number type))
   (number->octet-pair (class->number class))))

(define (make-rr name type class ttl rdata)
  (append
   (name->octets name)
   (number->octet-pair (type->number type))
   (number->octet-pair (class->number class))
   (number->octet-pair (arithmetic-shift-right ttl 16))
   (number->octet-pair (bitwise-and ttl #xffff))
   (number->octet-pair ((if (pair? rdata) length string-length) rdata))
   (if (pair? rdata) rdata (string->list rdata))
   ))

(define (make-add-update class name type ttl rdata)
  (make-rr name type class ttl (ip->dns-ip rdata)))

(define (make-delete-rrset name type)
  (make-rr name type '* 0 ""))

(define (make-delete-name name)
  (make-rr name '* '* 0 ""))

(define (make-delete-rr class name type rdata)
  (make-rr name type class 0 rdata))

(define (make-update-section class command . rest)
  (case command
    ((add) (apply make-add-update class rest))
    ((delete-rrset) (apply make-delete-rrset rest))
    ((delete-name) (apply make-delete-name rest))
    ((delete-rr) (apply make-delete-rr class rest))
    (else (raise-dns-error command))))

(define (make-update-call name class rest)
  (append
   (make-update-query-header (length rest))
   (name->octets name)
   (number->octet-pair (type->number 'soa))
   (number->octet-pair (class->number class))
   (fold-right
    (lambda (x i) (append (apply make-update-section class x) i))
    '()
    rest)
   ))

(define (add-size-tag m)
  (append (number->octet-pair (length m)) m))

(define (make-query-string query)
  (list->string (add-size-tag query)))

(define (rr-data rr)
  (cadddr (cdr rr)))

(define (rr-ttl rr)
  (cadddr rr))

(define (rr-class rr)
  (caddr rr))

(define (rr-type rr)
  (cadr rr))

(define (rr-name rr)
  (car rr))

(define (parse-name start reply)
  (let ((v (char->integer (car start))))
    (cond
     ((zero? v)
      ;; End of name
      (values #f (cdr start)))
     ((zero? (bitwise-and #xc0 v))
      ;; Normal label
      (let loop ((len v)(start (cdr start))(accum '()))
        (cond
         ((zero? len) 
          (receive (s start) (parse-name start reply)
                   (let ((s0 (list->string (reverse! accum))))
                     (values (if s
                                 (string-append s0 "." s)
                                 s0)
                             start))))
         (else (loop (sub1 len) (cdr start) (cons (car start) accum))))))
     (else
      ;; Compression offset
      (let ((offset (+ (arithmetic-shift-left (bitwise-and #x3f v) 8)
                       (char->integer (cadr start)))))
        (receive (s ignore-start)  (parse-name (list-tail reply offset) reply)
                 (values s (cddr start))))))))

(define (parse-rr start reply)
  (receive
   (name start) (parse-name start reply)
   (let ((type (car (cossa (octet-pair->number (car start) (cadr start)) types)))
         (start (cddr start)))
     (let ((class (car (cossa (octet-pair->number (car start) (cadr start)) classes)))
           (start (cddr start)))
       (let ((ttl (octet-quad->number (car start) (cadr start)
                                      (caddr start) (cadddr start)))
             (start (cddddr start)))
         (let ((len (octet-pair->number (car start) (cadr start)))
               (start (cddr start)))
					; Extract next len bytes for data:
           (let loop ((len len)(start start)(accum '()))
             (if (zero? len)
                 (values (list name type class ttl (reverse! accum))
                         start)
                 (loop (sub1 len) (cdr start) (cons (car start) accum))))))))))

(define (parse-ques start reply)
  (receive (name start) (parse-name start reply)
           (let ((type (car (cossa (octet-pair->number (car start) (cadr start)) types)))
                 (start (cddr start)))
             (let ((class (car (cossa (octet-pair->number (car start) (cadr start)) classes)))
                   (start (cddr start)))
               (values (list name type class) start)))))

(define (parse-n parse start reply n)
  (let loop ((n n)(start start)(accum '()))
    (if (zero? n)
        (values (reverse! accum) start)
        (receive (rr start) (parse start reply)
                 (loop (sub1 n) start (cons rr accum))))))

(define (dns-parse-reply auth? reply)
  (apply
   (lambda (x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 . start)
     (let ((qd-count (octet-pair->number x4 x5))
	   (an-count (octet-pair->number x6 x7))
	   (ns-count (octet-pair->number x8 x9))
	   (ar-count (octet-pair->number x10 x11)))
       
       (receive
	(qds start) (parse-n parse-ques start reply qd-count)
	(receive
	 (ans start) (parse-n parse-rr start reply an-count)
	 (receive
	  (nss start) (parse-n parse-rr start reply ns-count)
	  (receive
	   (ars start) (parse-n parse-rr start reply ar-count)
	   (if (not (null? start))
	       (raise-dns-error "error parsing server reply"))
	   (values auth? qds ans nss ars reply)))))))
   reply))

(define (update-prescan zname zclass rrs)
  (for-each
   (lambda (rr)
     (define rr.class (rr-class rr))
     (define rr.type (rr-type rr))
     (and (eq? zclass rr.class) (memq rr.type '(* axfr maila mailb))
	  (raise-dns-error "formerr"))
     (and (eq? rr.class '*)
	  (or (not (eqv? (rr-ttl rr) 0))
	      ;; FIXME: test rdlength too!
	      (memq rr.type '(axfr maila mailb)))
	  (raise-dns-error "formerr"))
     (and (eq? rr.class 'none)
	  (or (not (eqv? (rr-ttl rr) 0))
	      (memq rr.type '(* axfr maila mailb)))
	  (raise-dns-error "formerr")))
   rrs)
  rrs)

(define (dns-query nameserver query)

  (let ((reply
          (receive (r w) (tcp-connect
			  (or nameserver
			      ;; TODO we should
			      ;; raise-service-unavailable-condition
			      ;; but the dns resolutio is currently
			      ;; compiled within "util" which is
			      ;; wrong; therefore we can not raise
			      ;; this yet undefined condition.
			      (error "DNS nameserver not configured"))
			  53)
                   (dynamic-wind
                       (lambda () #f)
                       
                       (lambda ()
                         (display (make-query-string query) w)
                         (flush-output-port w)
                         
                         (let ((a (read-char r))
                               (b (read-char r)))
                           (if (eof-object? b)
                               (raise-dns-error "unexpected EOF from DNS server"))
                           (let ((len (octet-pair->number a b)))
                             (let ((s (read-bytes len r)))
                               (if (not (eqv? len (string-length s)))
                                   (raise-dns-error "unexpected EOF from DNS server"))
                               (string->list s)))))
                       
                       (lambda ()
                         (close-input-port r)
                         (close-output-port w))))))

    ;; First two bytes must match sent message id:
    (if (not (and (char=? (car reply) (car query))
                  (char=? (cadr reply) (cadr query))))
        (raise-dns-error "bad reply id from server"))

    (let ((v0 (caddr reply))
          (v1 (cadddr reply)))
 					; Check for error code:
      (let ((rcode (bitwise-and #xf (char->integer v1))))
        (if (not (zero? rcode))
            (raise-dns-error
	     (string-append
	      "error from server: " (dns-rcode->string rcode)))))

      (dns-parse-reply (positive? (bitwise-and #x4 (char->integer v0))) reply))))

(define cache #f)

(define *nameserver* (make-mutex 'nameserver))

(define (set-dns-nameserver! str)
  (set! *nameserver* str))

(define (dns-flush-cache)
  (set! cache (make-symbol-table))
  (set! *nameserver* (make-mutex 'nameserver)))

(dns-flush-cache)

(define (dns-query/cache nameserver addr type class)
  (let ((key (string->symbol (format "~a;~a;~a;~a" nameserver addr type class))))
    (let ((v (hash-table-ref/default cache key #f)))
      (if v
          (list->values v)
          (receive
           (auth? qds ans nss ars reply)
	   (dns-query nameserver (make-query (random 256) addr type class))
           (hash-table-set! cache key (list auth? qds ans nss ars reply))
           (values auth? qds ans nss ars reply))))))

(define (ip->string s)
  (format "~a.~a.~a.~a" 
          (char->integer (list-ref s 0))
          (char->integer (list-ref s 1))
          (char->integer (list-ref s 2))
          (char->integer (list-ref s 3))))

; (define (ormap f lst)
;   (and (pair? lst) (let ((x (f (car lst)))) (or x (ormap f (cdr lst))))))

(define (try-forwarding k nameserver)
  (let loop ((nameserver nameserver)(tried (list nameserver)))
    ;; Normally the recusion is done for us, but it's technically optional
    (receive(v ars auth?) (k nameserver)
            (or v
                (and (not auth?)
                     (let* ((ns (ormap
                                 (lambda (ar)
                                   (and (eq? (rr-type ar) 'a)
                                        (ip->string (rr-data ar))))
                                 ars)))
                       (and ns
                            (not (member ns tried))
                            (loop ns (cons ns tried)))))))))

(define ip-match
  ;; We accept (and ignore) a port number too.
  (pcre/a->proc-uncached
   "^([[:digit:]]+)\\.([[:digit:]]+)\\.([[:digit:]]+)\\.([[:digit:]]+)(?::[[:digit:]]+)?$"))

(define (ip->in-addr.arpa ip)
  (let ((result (ip-match ip)))
    (and result (apply format "~a.~a.~a.~a.in-addr.arpa"
		       (reverse! (cdr result))))))

(define (ip->dns-ip ip)
  (let ((result (ip-match ip)))
    (and result (map (lambda (i) (integer->char (string->number i))) (cdr result)))))

(define get-ptr-list-from-ans
  (lambda (ans)
    (filter (lambda (ans-entry)
              (eq? (list-ref ans-entry 1) 'ptr))
            ans)))

(define dns-get-name 
  (lambda (nameserver ip)
    (or (and (string=? ip "127.0.0.1") "localhost") ; save work and worries
	(try-forwarding
         (lambda (nameserver)
           (receive
            (auth? qds ans nss ars reply)
            (dns-query/cache nameserver (ip->in-addr.arpa ip) 'ptr 'in)
            (values (and (positive? (length (get-ptr-list-from-ans ans)))
                         (let ((s (rr-data (car (get-ptr-list-from-ans ans)))))
                           (receive (name rest) (parse-name s reply) name)))
                    ars auth?)))
         nameserver)
        (raise-dns-error "dns-get-name: bad ip address"))))

(define get-a-list-from-ans
  (lambda (ans)
    (filter (lambda (ans-entry)
              (eq? (list-ref ans-entry 1) 'a))
            ans)))

(define (dns-get-address nameserver addr)
  (or (try-forwarding
       (lambda (nameserver)
         (receive
          (auth? qds ans nss ars reply)
          (dns-query/cache nameserver addr 'a 'in)
          (values (let ((a-list (get-a-list-from-ans ans)))
		    (and (positive? (length a-list))
			 (let ((s (rr-data (car a-list))))
			   (ip->string s))))
                  ars auth?)))
       nameserver)
      (raise-dns-error (string-append "dns-get-address: bad address" addr))))

(define (dns-get-addresses nameserver addr)
  (or (try-forwarding
       (lambda (nameserver)
         (receive
          (auth? qds ans nss ars reply)
          (dns-query/cache nameserver addr 'a 'in)
          (values (map (lambda (e) (ip->string (rr-data e))) (get-a-list-from-ans ans))
                  ars auth?)))
       nameserver)
      (raise-dns-error (string-append "dns-get-address: bad address" addr))))

(define (dns-set-address! nameserver name addr)
  (let ((zone (string-index name #\.)))
    (dns-flush-cache)
    (and zone
	 (dns-query
	  nameserver
	  (make-update-call
	   (if (string-index name #\. (add1 zone))
	       (substring name (add1 zone) (string-length name))
	       name)
	   'in `((delete-rrset ,name a)
		 (add ,name a 3600 ,addr)))))))

(define (dns-delete-address! nameserver name old)
  (dns-flush-cache)
  (let ((zone (string-index name #\.)))
    (if zone
	(dns-query
	 nameserver
	 (make-update-call
	  (if (string-index name #\. (add1 zone))
	      (substring name (add1 zone) (string-length name))
	      name)
	  'in `((delete-rr ,name a ,old))))
	(raise-dns-error (format "dns:delete-address no zone in ~a" name)))))

(define (dns-add-address! nameserver name addr)
  (dns-flush-cache)
  (let ((zone (string-index name #\.)))
    (if zone
	(dns-query
	 nameserver
	 (make-update-call
	  (if (string-index name #\. (add1 zone))
	      (substring name (add1 zone) (string-length name))
	      name) 'in `((add ,name a 3600 ,addr))))
	(raise-dns-error (format "dns:add-address no zone in ~a" name)))))

(define plus-infinite 70000)
(define (dns-get-mail-exchanger nameserver addr)
  (or (try-forwarding
       (lambda (nameserver)
         (receive
          (auth? qds ans nss ars reply)
          (dns-query/cache nameserver addr 'mx 'in)
          (values (let loop ((ans ans)(best-pref plus-infinite)(exchanger #f))
                    (cond
                     ((null? ans)
                      (or exchanger
                          ;; Does 'soa mean that the input address is fine?
                          (and (ormap
                                (lambda (ns) (eq? (rr-type ns) 'soa))
                                nss)
                               addr)))
                     (else
                      (let ((d (rr-data (car ans))))
                        (let ((pref (octet-pair->number (car d) (cadr d))))
                          (if (< pref best-pref)
                              (receive(name start) (parse-name (cddr d) reply)
                                      (loop (cdr ans) pref name))
                              (loop (cdr ans) best-pref exchanger)))))))
                  ars auth?)))
       nameserver)
      (raise-dns-error "dns-get-mail-exchanger: bad address")))

(define (dns-get-mail-exchangers nameserver addr)
  (or (try-forwarding
       (lambda (nameserver)
         (receive
          (auth? qds ans nss ars reply)
          (dns-query/cache nameserver addr 'mx 'in)
          (values (map cdr
		       (sort
			(map
			 (lambda (ans)
			   (let ((d (rr-data ans)))
			     (receive(name start) (parse-name (cddr d) reply)
				     (cons (octet-pair->number (car d) (cadr d))
					   name))))
			 ans)
			(lambda (a b) (< (car a) (car b)))))
                  ars auth?)))
       nameserver)
      (raise-dns-error "dns-get-mail-exchanger: bad address")))

(define (dns-find-nameserver*)
  (guard
   (ex (else #f))
   (call-with-input-file "/etc/resolv.conf"
     (lambda (port)
       (let loop ()
         (let ((l (read-line port)))
           (or (and (string? l)
                    (let ((m (string-match
                              etc-resolve.conf-nameserver-line-regex l)))
                      (and m (cadr m))))
               (and (not (eof-object? l))
                    (loop)))))))))

(define (dns-find-nameserver)
  (if (mutex? *nameserver*)
      (let ((m *nameserver*))
	(with-mutex
	 m
	 (if (mutex? *nameserver*)
	     (begin
	       (set! *nameserver* (dns-find-nameserver*))
	       *nameserver*)
	     *nameserver*)))
      *nameserver*))
