;; (C) 2000-2003, 2005, 2015 Jörg F. Wittenberger see http://www.askemos.org

;; 2015: Beginning clean up….

(define $https-client-verbose (make-shared-parameter #f))

(define $http-client-connections-maximum (make-shared-parameter 4))

(define $topology-verbose (make-shared-parameter #f))

(define (split-target name)
  (let ((colon (string-index name #\:)))
    (if colon
	(values (substring name 0 colon)
		(string->number
		 (substring name (add1 colon) (string-length name))))
	(values name 80))))

(define-record-type <bidirectional-connection>
  (%allocate-bidirectional-connection
   name
   certificate
   alive
   close
   input-port
   output-port)
  bidirectional-connection?
  (name connection-name)
  (certificate connection-certificate)
  (alive connection-alive?)
  (close connection-close)
  (input-port connection-input-port)
  (output-port connection-output-port))

(: connection-close! (* (struct <bidirectional-connection>) -> *))
(define (connection-close! caller connection)
  ;; Close the connection logging any errors.
  (guard
   (condition (else (loghttps "W ~a closing connection ~a error ~a\n"
			      caller (connection-name connection) (condition->string condition))))
   ((connection-close connection) connection)))

;; This is a very, very simple persistent connection pool.  For each
;; "host:port" there is an entry in the *http-channels* table.  The
;; entry contains a list (mutex total . free connections list) which
;; are currently unused connections with the client and a semaphore
;; "total" holding the maximum of connections still available.

(define *http-channels*
  (make-cache "*http-channels*"
	      string=?
	      #f ;; state
	      ;; miss:
	      (lambda (cs k) (list #f))
	      ;; hit
	      #f ;; (lambda (c es) #t)
	      ;; fulfil
	      (lambda (cs es r)
		(if (pair? r) (set-car! es (car r))))
	      ;; valid?
	      (lambda (es) #t)		; should test host alive?
	      ;; delete
	      (lambda (cs es)
		(if (car es) (!start (http-finalize-channel! (car es)) 'close-channel)))
	      ))

;; KLUDGE: we ought not to need the indirection via <bidirectional-connection-cache>
;; but we do need it for debugging.

(cond-expand
 (never

  (cond-expand
   ((or chicken) (define-type :mesh-connection-cache: (struct <bidirectional-connection-cache>)))
   (else ))

  (define-record-type <bidirectional-connection-cache>
    (%make-bidirectional-connection-cache box)
    bidirectional-connection-cache?
    (box bidirectional-connection-cache-mailbox))

  (define (make-bidirectional-connection-cache name)
    (%make-bidirectional-connection-cache (make-mailbox name)))

  (define (bidirectional-connection-cache-empty? cache)
    (assert (bidirectional-connection-cache? cache))
    (mailbox-empty? (bidirectional-connection-cache-mailbox cache)))

  (define (bidirectional-connection-cache-number-of-items cache)
    (assert (bidirectional-connection-cache? cache))
    (mailbox-number-of-items (bidirectional-connection-cache-mailbox cache)))

  (define (bidirectional-connection-cache-get! cache)
    (assert (bidirectional-connection-cache? cache))
    (receive-message! (bidirectional-connection-cache-mailbox cache)))

  (define (bidirectional-connection-cache-store! cache c)
    (assert (bidirectional-connection-cache? cache))
    (assert (bidirectional-connection? c))
    (send-message! (bidirectional-connection-cache-mailbox cache) c))

  (define (b-c-cache-mailbox-get-connection-if-present! cache)
    (assert (bidirectional-connection-cache? cache))
    (receive
     (c v) (receive-message-if-present! (bidirectional-connection-cache-mailbox cache))
     (if v
	 (if (bidirectional-connection? c)
	     (values c v)
	     (begin
	       (logerr "b-c-cache-mailbox-get-connection-if-present! ~a found ~a\n"
		       cache v)
	       (b-c-cache-mailbox-get-connection-if-present! cache)))
	 (values c v))))

  (define (bidirectional-connection-cache-has-connection? cache c eq?)
    (assert (bidirectional-connection-cache? cache))
    (mailbox-has-message? (bidirectional-connection-cache-mailbox cache) c eq?)))

 (else

  (cond-expand
   ((or chicken) (define-type :mesh-connection-cache: (struct <mailbox>)))
   (else ))

  (define (make-bidirectional-connection-cache name) (make-mailbox name))
  (define bidirectional-connection-cache-empty? mailbox-empty?)
  (define (bidirectional-connection-cache-number-of-items x) (mailbox-number-of-items x))
  (define bidirectional-connection-cache-get! receive-message!)
  (define bidirectional-connection-cache-store! send-message!)
  (define b-c-cache-mailbox-get-connection-if-present! receive-message-if-present!)
  (define bidirectional-connection-cache-has-connection? mailbox-has-message?)

  ))

(: http-channel? (* --> boolean : :mesh-channel:))
(: make-http-channel
   ((struct <semaphore>)
    :mesh-connection-cache:
    (or false vector)			; statistics
    symbol				; state
    :time:				; last-time activ
    --> :mesh-channel:))
(: http-channel-maximum (:mesh-channel: --> (or boolean (struct <semaphore>))))
(: http-channel-connection-cache (:mesh-channel: --> :mesh-connection-cache:))
(: %set-http-channel-maximum! (:mesh-channel: boolean -> undefined)) ; deactivate
(: http-channel-state (:mesh-channel: --> symbol))
(: %set-http-channel-state! (:mesh-channel: symbol -> undefined))
(: http-channel-last-time (:mesh-channel: --> :time:))
(: %set-http-channel-last-time! (:mesh-channel: :time: -> undefined))

(define-record-type <http-channel>
  (make-http-channel maximum connection-cache statistics state last-time)
  http-channel?
  (maximum http-channel-maximum %set-http-channel-maximum!)
  (connection-cache http-channel-connection-cache)
  (statistics http-channel-statistics set-http-channel-statistics!)
  ;; states are: new, setup, established, collect, unreachable
  (state http-channel-state %set-http-channel-state!)
  (last-time http-channel-last-time %set-http-channel-last-time!))

(: set-http-channel-state! (:mesh-channel: (or boolean symbol) -> undefined))
(define (set-http-channel-state! c s)
  (case (http-channel-state c)
    ((new unreachable)
     (case s
       ((#t)
	(%set-http-channel-last-time! c *system-time*))
       ((setup established collect)
	(%set-http-channel-last-time! c *system-time*)
	(%set-http-channel-state! c s))
       ((unreachable) (%set-http-channel-state! c s))
       (else (domain-error 'set-http-channel-state! (http-channel-state c) s))))
    ((established setup)
     (case s
       ((#t established)
	(%set-http-channel-last-time! c *system-time*)
	(%set-http-channel-state! c 'established))
       ((collect)
	(%set-http-channel-last-time! c *system-time*)
	(%set-http-channel-state! c s))
       ((setup))
       ((unreachable) (%set-http-channel-state! c s))
       (else (domain-error 'set-http-channel-state! (http-channel-state c) s))))
    ((collect)
     (case s
       ((#t established collect)
	(%set-http-channel-last-time! c *system-time*))
       ((setup unreachable))
       (else (domain-error 'set-http-channel-state! (http-channel-state c) s))))))

(define (display-http-channels)
  (define now (time-second (srfi19:current-time 'time-utc)))
  (cons*
   'table '(@(id "http-channels"))
   '(tr (th "host") (th "left") (th "active")
	(th (@ (colspan "2")) "speed") (th "min") (th "max") (th "s") (th "age"))
   (cache-fold
    *http-channels*
    (lambda (key channel init)
      `((tr
	 (td ,key)
	 (td ,(literal (semaphore-value (http-channel-maximum channel))))
	 (td ,(bidirectional-connection-cache-number-of-items
	       (http-channel-connection-cache channel)))
	 (td ,(or (and-let* ((stats (http-channel-statistics channel)))
			    (literal (/ (vector-ref stats 1)
					(vector-ref stats 0))))
		  "n/a"))
	 (td ,(or (and-let* ((stats (http-channel-statistics channel)))
			    (literal (vector-ref stats 2) ))
		  "n/a"))
	 (td ,(or (and-let* ((stats (http-channel-statistics channel)))
			    (vector-ref stats 3))
		  "n/a"))
	 (td ,(or (and-let* ((stats (http-channel-statistics channel)))
			    (vector-ref stats 4))
		  "n/a"))
	 (td ,(case (http-channel-state channel)
		((new setup) "#")
		((established) ">")
		((collect) "<")
		(else "-")))
	 (td ,(literal (- now (time-second (http-channel-last-time channel))))))
	. ,init)) 
    '())))

(: http-find-channel (:ip-addr: -> :mesh-channel:))
(define (http-find-channel name)
  (cache-ref
   *http-channels*
   name
   (lambda ()
     (make-http-channel
      (make-semaphore
       (dbgname name "~a-http-channel")
       ($http-client-connections-maximum))
      (make-bidirectional-connection-cache name)
      #f				; statistics
      'new
      *system-time*))))

(: http-search-channel (:ip-addr: -> (or false :mesh-channel:)))
(define (http-search-channel name)
  (cache-ref/default *http-channels* name #f #f))

(: http-finalize-channel! (:mesh-channel: -> undefined))
(define (http-finalize-channel! entry)
  (let ((sema (http-channel-maximum entry)))
    (%set-http-channel-maximum! entry #f)
    (semaphore-delete! sema))
  (let ((cache (http-channel-connection-cache entry)))
    (do ()
	((bidirectional-connection-cache-empty? cache))
      ;; (receive
      ;;  (c valid)
      ;;  (b-c-cache-mailbox-get-connection-if-present! cache)
      ;;  (if valid (guard (condition (else 'ignore))
      ;; 		      ((connection-close c) c))))
      (connection-close! 'http-finalize-channel! (bidirectional-connection-cache-get! cache)))))

(: http-close-channel! (:ip-addr: * -> undefined))
(define (http-close-channel! name reason)
  (logtopo "T: closing channel ~a ~a\n" name reason)
  (cache-set! *http-channels* name))

;; Experimental: somewhere this code is loosing connections.  That is
;; it misses disconnects.  Here we attempt to sort this out by calling
;; up from the ssl layer when we see too many connections.  FIXME: get
;; rid of this bug-woraround!
(: http-check-connections! (string :mesh-channel: -> undefined))
(define (http-check-connections! peer channel)
  (define cache (http-channel-connection-cache channel))
  (define logtopo logerr)		;; while debugging
  (do ((n (bidirectional-connection-cache-number-of-items cache) (sub1 n)))
      ((eqv? n 0))
    (receive
     (c e) (b-c-cache-mailbox-get-connection-if-present! cache)
     (if (and e ((connection-alive? c)))
	 (begin
	   (logtopo "T: checking connection ~a found alive state ~a age ~a.\n" peer
		    (http-channel-state channel)
		    (- (time-second *system-time*) (time-second (http-channel-last-time channel))))
	   (bidirectional-connection-cache-store! cache c))
	 (begin
	   (logtopo "T: checking connection ~a found dead state ~a age ~a. Closing now.\n" peer
		    (http-channel-state channel)
		    (- (time-second *system-time*) (time-second (http-channel-last-time channel))))
	   (connection-close! 'http-check-connections! c))))))

(set! ;;dbg-connlimitsignal-set!
 ##dbg#connlimitsignal
 (lambda (peer)
   (cache-fold
    *http-channels*
    (lambda (k channel i)
      (if (string-prefix? peer k) (http-check-connections! k channel)))
    #f)))

(: http-with-channel
   (string				; name
    (or false number)			; timeout
    (procedure (:mesh-channel:) . *)	; proc
    -> . *))

(cond-expand
 (rscheme
  (define (http-with-channel name timeout proc)
    (let ((channel (http-find-channel name)))
      (loghttps "HTTP client allocate connection ~s ~a\n" 
		(http-channel-maximum channel)
		(bidirectional-connection-cache-number-of-items
		 (http-channel-connection-cache channel)))
      (guard
       (ex (else (and-let* ((s (http-channel-maximum channel)))
			   (semaphore-signal s))
		 (raise ex)))
       (within-time timeout
		    (let ((s (http-channel-maximum channel)))
		      (if s
			  (semaphore-wait s)
			  (raise 'channel-is-closed))))
       (proc channel)
       (and-let* ((s (http-channel-maximum channel))) (semaphore-signal s)))
      )))
 (chicken
  (define (http-with-channel name timeout proc)
    (let* ((channel (http-find-channel name))
	   (s (or (http-channel-maximum channel)
		  (raise 'channel-is-closed))))
      (loghttps "HTTP client allocate connection ~s ~a\n" 
		(list (semaphore-name s) (semaphore-value s))
		(bidirectional-connection-cache-number-of-items
		 (http-channel-connection-cache channel)))
      (dynamic-wind
	  (lambda () (within-time%1 timeout (semaphore-wait-by! s 1)))
	  (lambda () (proc channel))
	  (lambda () (semaphore-signal-by! s 1)))
      ))))

(: update-stat-value! (:mesh-channel: :time: -> undefined))
(define (update-stat-value! channel duration)
  (define seconds
    (+ (time-second duration) (* (time-nanosecond duration) 0.000000001)))
  (define record (or (http-channel-statistics channel)
		     (let ((n (vector 0.0 0.0 seconds #f #f)))
		       (set-http-channel-statistics! channel n) n)))
  (vector-set! record 1 (+ (vector-ref record 1) seconds))
  (vector-set! record 0 (+ 1.0 (vector-ref record 0)))
  (let ((x (vector-ref record 2)) (w 0.9))
    (vector-set! record 2 (+ (* x w) (* seconds (- 1 w)))))
  (let ((x (vector-ref record 3)))
    (vector-set! record 3 (if x (min x seconds) seconds)))
  (let ((x (vector-ref record 4)))
    (vector-set! record 4 (if x (max x seconds) seconds)))
  (%set-http-channel-last-time! channel *system-time*))

(define (http-update-statistics oid auth duration)
  (and-let* ((auth)
	     (c (http-search-channel (host-lookup* auth))))
	    (update-stat-value! c duration)))

($broadcast-update-statistics http-update-statistics)

(: http-update-alive-time! (string (or boolean symbol) -> undefined))
(define (http-update-alive-time! name alive)
  (case alive
    ((#f)
     (and-let* ((c (http-search-channel name)))
	       (if (srfi19:time<=?
		    (add-duration
		     (http-channel-last-time c)
		     host-seems-dead-duration)
		    *system-time*)
		   (set-http-channel-state! c 'unreachable))))
    ((close) (http-close-channel! name "explicit close"))
    ((check)
     (and-let* ((c (http-search-channel name)))
	       (if (srfi19:time<=?
		    (add-duration
		     (http-channel-last-time c)
		     host-seems-dead-duration)
		    *system-time*)
		   (http-close-channel! name (cons (http-channel-state c) "requested check: too old")))))
    (else (let ((c (http-find-channel name)))
	    (set-http-channel-state! c alive)))))

(: http-set-channel-callback! (:mesh-channel: -> undefined))
(define (http-set-channel-callback! channel)
  (set-http-channel-state! channel 'collect))

(define (update-alive-time-from-server! peer-certificate)
  (and-let* (((mesh-certificate? peer-certificate))
	     (sig (mesh-cert-l peer-certificate))
	     (location ((host-lookup) sig)) ; don't use host-lookup* - expect no exceptions.
	     (c (http-search-channel location))
	     ((eq? (http-channel-state c) 'collect)))
	    (set-http-channel-state! c 'collect)))

(: update-alive-time-from-callback-server! ((or boolean string) -> undefined))
(define (update-alive-time-from-callback-server! host)
  (if host
      (let ((addr (host-lookup* host)))
	(set-http-channel-state! (http-find-channel addr) 'established)
	(cache-invalid! *http-alive-cache* addr))))

(define *http-alive-cache*
  (make-cache "*http-alive-cache*"
	      string=?
	      #f ;; state
	      ;; miss:
	      (lambda (cs k) (cons k #f))
	      ;; hit
	      #f ;; (lambda (c es) #t)
	      ;; fulfil
	      (lambda (c es results)
		(define success (and (pair? results) (car results)))
		(http-update-alive-time! (car es) (and success 'established))
		(and-let* ((success) (f (http-collect-call-function)))
			  (http-connect-back (car es) f))
		(set-cdr! es (add-duration
			      *system-time*
			      (if success
				  host-seems-alive-duration
				  host-seems-dead-duration)))
		(values))
	      ;; valid?
	      (lambda (es)
		(or (not (cdr es))
		    (srfi19:time>=? (cdr es) *system-time*)))
	      ;; delete
	      (lambda (cs es)
		;; (if (or (not (cdr es))
		;; 	(srfi19:time<=? (cdr es) *system-time*))
		;;     (http-update-alive-time! (car es) 'check))
		(http-update-alive-time! (car es) 'check))
	      ))

(begin
  (define (cleanup-alive-cache)
    (cache-cleanup! *http-alive-cache*)
    (register-timeout-message! 60 cleanup-alive-cache))
  (register-timeout-message! 60 cleanup-alive-cache))

;; KLUDGE: make-host-alive! ought to do it's best.  should become an
;; upcall to bopctrl

(: channel-alive? (:mesh-channel: -> (or false :time:)))
(define (channel-alive? channel)
  (or (and (let ((s (http-channel-state channel)))
	     (or (eq? s 'established) (eq? s 'collect)))
	   (http-channel-last-time channel))
      (and (bidirectional-connection-cache-has-connection?
	    (http-channel-connection-cache channel)
	    'dummy
	    (lambda (c dummy)
	      (and (bidirectional-connection? c)
		   ((connection-alive? c)))))
	   *system-time*)))

(: make-host-alive! (:mesh-channel: -> (procedure () (or false :time:))))
(define (make-host-alive! channel)
  (lambda () (channel-alive? channel)))

(: http-host-alive? (:ip-addr: -> (or false :time:)))
(define (http-host-alive? name)
  (or (and (equal? name (local-id)) *system-time*)
      (cache-ref
       *http-alive-cache* name
       (lambda ()
	 (channel-alive? (http-find-channel name))))))

(: http-host-already-alive? (:ip-addr: -> boolean))
(define (http-host-already-alive? name)
  (or (and (equal? name (local-id)) *system-time*)
      (and-let* ((c (http-search-channel name)))
		(or
		 (eq? (http-channel-state c) 'new)
		 (eq? (http-channel-state c) 'setup)
		 ;; (cache-ref *http-alive-cache* name (make-host-alive! c))
		 (channel-alive? c)))))

(: http-host-maybe-alive? (:ip-addr: -> boolean))
(define (http-host-maybe-alive? name)
  (or (and (equal? name (local-id)) *system-time*)
      (cache-ref/default
       *http-alive-cache* name
       (lambda ()
	 (let ((c (http-find-channel name)))
	   (channel-alive? c)))
       *system-time*)))

(define host-seems-dead-duration (make-time 'time-duration 0 600))

(define host-seems-alive-duration (make-time 'time-duration 0 300))

(: host-seems-dead? (:ip-addr: -> boolean))
(define (host-seems-dead? name)
  (not (or (cache-ref/default *http-alive-cache* name #f #f)
	   (and-let* ((addr (http-host-lookup name))
		      (c (http-search-channel addr)))
		     (if (eq? (http-channel-state c) 'unreachable)
			 #f
			 (srfi19:time<=?
			  (subtract-duration *system-time* host-seems-dead-duration)
			  (http-channel-last-time c)))))))

(: http-channel-size (:ip-addr: -> fixnum))
(define (http-channel-size name)
  (let ((channel (http-search-channel name)))
    (if channel
	(bidirectional-connection-cache-number-of-items (http-channel-connection-cache channel))
	0)))

(: http-close-one-old-connection! (:mesh-channel: -> undefined))
(define (http-close-one-old-connection! channel)
  (receive
   (c valid)
   (b-c-cache-mailbox-get-connection-if-present! (http-channel-connection-cache channel))
   (if valid
       (guard (condition (else (loghttps "close old connection to ~a error ~a\n"
					 (connection-name c)
					 (condition->string condition))))
	      (let ((r ((connection-close c) c)))
		(loghttps "closed old connection to ~a (~a).\n" (connection-name c) r)
		r)))))

(: http-client-wait-for-connection (:mesh-channel: :ip-addr: -> :mesh-connection:))
(define (http-client-wait-for-connection channel host)
  (guard
   (ex (else (or (let ((s (http-channel-state channel)))
		   (or (eq? s 'established) (eq? s 'collect)))
		 (http-close-channel! host ex))
	     (raise ex)))
   (within-time%1
    (http-with-channel-timeout)
    (bidirectional-connection-cache-get!
     (http-channel-connection-cache channel)))))

(: http-client-allocate-connection
   (:mesh-channel:
    procedure :ip-addr: boolean
    -> * boolean))
(define (http-client-allocate-connection channel open host wait)
  (receive
   (connection reused)
   (b-c-cache-mailbox-get-connection-if-present!
    (http-channel-connection-cache channel))
   (if reused
       (if ((connection-alive? connection))
	   (values connection reused)
	   (begin
	     (connection-close! 'http-client-allocate-connection connection)
	     (http-client-allocate-connection channel open host wait)))
       (if (eq? (http-channel-state channel) 'collect)
	   (if wait
	       (values (http-client-wait-for-connection channel host) #t)
	       (values #f #f))
	   (guard
	    (ex (else (loghttps "open to ~a failed with ~a\n" host (condition->string ex))
		      (if (and wait
			       (let ((s (http-channel-state channel)))
				 (or (eq? s 'established) (eq? s 'collect))))
			  (values (http-client-wait-for-connection channel host) #t)
			  (begin
			    (set-http-channel-state! channel 'unreachable)
			    (values #f ex)))))
	    (values
	     (receive (from to cert alive close) (open host)
		      (set-http-channel-state! channel 'setup)
		      (%allocate-bidirectional-connection
		       host cert alive close from to))
	     #f))
	   ))))

(: http-client-dispose-connection (:mesh-channel: :mesh-connection: (or boolean symbol) -> boolean))
(define (http-client-dispose-connection channel c keep)
  (if c
      (begin
	(assert (bidirectional-connection? c))
        (if (and keep (http-channel-maximum channel))
            (begin
	      (set-http-channel-state! channel keep)
	      (bidirectional-connection-cache-store! (http-channel-connection-cache channel) c))
	    (connection-close! 'http-client-dispose-connection c))
	(loghttps "HTTP client dispose connection ~s ~a ~a\n"
                  (cond-expand
		   (rscheme (http-channel-maximum channel))
		   (else (and (http-channel-maximum channel)
			      (semaphore-name (http-channel-maximum channel)))))
		  (bidirectional-connection-cache-number-of-items
		   (http-channel-connection-cache channel))
		  (if keep "keep" "close"))))
  #f)

(define $https-socks4a-server (make-shared-parameter #f))

(define $https-use-socks4a
  (let ((yes (lambda (name) #t))
	(no (lambda (name) #f))
	(maybe (pcre/a->proc-uncached ".*\\.(?:(?:onion)|(?:i2p))(?::[1-9][0-9]*)?$")))
    (make-shared-parameter
     'maybe
     (lambda (v)
       (let ((v (if (string? v) (call-with-input-string v read) v)))
	 (case v
	   ((#f no) no)
	   ((#t yes) yes)
	   ((maybe) maybe)
	   (else (domain-error '$https-use-socks4a "illegal socks4a mode" v))))))))

(define https-certification-for
  (make-shared-parameter
   (lambda args (domain-error 'https-certification-for "https-client-certificate-for: not connected" #f))))

(define (make-sslmgr-command-line name)
  (receive (ca cert key) ((https-certification-for) name)
	   `("sslmgr" ,(string-append
			(if ($https-client-verbose)
			    "-pconnect:" "-qpconnect:") name)
	     "-c" ,(or cert (error "No certificate to connect to ~a." name))
	     "-k" ,key
	     ,@(if (and ($https-socks4a-server)
			(($https-use-socks4a) name))
		   `("-s" ,($https-socks4a-server))
		   '())
	     . ,(if ca
		    (list "-E" "-A" ca "-T" ca)
		    '()))))

;; (define (make-stunnel-command-line name)
;;   (receive (ca cert key) ((https-certification-for) name)
;; 	   `("stunnel" "-c" "-p" ,cert "-r" ,name
;; 	     . ,(if ca
;; 		    (list "-v" "2" "-A" ca)
;; 		    '()))))

;; (define (make-s_client-command-line name)
;;   (receive (ca cert key) ((https-certification-for) name)
;; 	   `("openssl" "s_client" "-ign_eof" "-connect" ,name
;; 	     "-cert" ,cert
;; 	     . ,(if ca
;; 		    (list "-CAfile" ca)
;; 		    '()))))

;(define make-command-line make-stunnel-command-line)
(define make-command-line make-sslmgr-command-line)

(: https-client-open-sslmgr
   (:ip-addr:
    ->
    input-port output-port
    (or false string)			; cert
    (procedure () boolean)		; alive test
    (procedure (*) *)			; close
    ))
(define (https-client-open-sslmgr name)
  (receive
   (ca cert key) ((https-certification-for) name)
   (within-time
    (ssl-setup-timeout)
    (askemos:open-ssl-client
     name ca cert key
     (and-let* ((s ($https-socks4a-server)) ((($https-use-socks4a) name))) s)
     ($https-client-verbose)))))

(: https-client-open
   (:ip-addr:
    ->
    input-port output-port
    boolean				; no cert
    (procedure () boolean)		; alive test
    (procedure (*) *)			; close
    ))

(define (https-client-open name)
  (call-with-values
      (lambda () (make-process-stub (make-command-line name) #f))
    (lambda (p out in)
      (loghttps "HTTPS client ~a connected as ~s.\n" name (process-id p))
      (values in out #f
	      (lambda () (cond-expand
			  (rscheme (not (the-exit-status p)))
			  (else #t)))
              (lambda (dummy)
		;; Work around what appears to be an rscheme bug.
		;; TODO get rid of access to file descriptors of the
		;; underlying implementation.
		;; (define ifd (fd (underlying-input-port in)))
                ;; Should be implied by close-output-port
                ;; (guard (ex (else 'ignore)) (flush-output-port out))
;                 (guard (ex
;                         ((timeout-object? ex)
;                          (guard (ex (else 'ignore))
;                                 (process-kill p)
;                                 (close-output-port out)))
;                         (else 'ignore))
;                        ;; FIXME watchdog caught exception runtime
;                        ;; error: Thread #[<thread>
;                        ;; 80.243.45.75:7443-998] blocked on
;                        ;; #[<process> pid 2282] ??? in: %apply-on-thread
;                        (with-timeout
;                         https-client-close-timeout
;                         (lambda ()
;                           (close-output-port out))))
                                        ; checks for exit status
                (process-kill p)
                (guard (ex (else 'ignore)) (close-output-port out))
		;; (file-close ofd)
                (guard
                 (ex (else (loghttps "HTTPS client ~a error ~a.\n" name
				     (if (message-condition? ex)
					 (condition-message ex)
					 ex))))
                 (close-input-port in))
		;; (file-close ifd)
                (loghttps "HTTPS client ~a disconnected (~s).\n"
                          name (process-id p)))))))

(: http-client-open
   (:ip-addr:
    ->
    input-port output-port
    boolean				; no cert
    (procedure () boolean)		; alive test
    (procedure (*) *)			; close
    ))
(define (http-client-open name)
  (receive
   (in out) (call-with-values (lambda () (split-target name)) tcp-connect)
   ;; (logerr "HTTP client connected to ~a.\n" name)
   (values in out #f
	   (lambda () #t)
           (lambda (dummy)
             (close-input-port in)
             (close-output-port out)
             ;; (logerr "HTTP client connection to ~a terminated.\n" name)
             ))))

(define http-connect-back-magic "/")

(define (http-connect-back-magic? x)
  (and (uri? x) (string=? (uri-path x) http-connect-back-magic)))

(define http-do-connect-back-f #f)
(define (http-do-connect-back he answer-function open)
  (http-do-connect-back-f he answer-function open))
(define (set-http-do-connect-back! f)
  (set! http-do-connect-back-f f))

(define $connect-back (make-shared-parameter #f))

(: http-connect-back/routeable-host
   (:mesh-host: procedure &optional procedure -> undefined))
(define (http-connect-back/routeable-host he answer-function . open)
  (and-let* ((cc ($connect-back))
	     ((not (eq? (host-collect-call he) #t))) ; closing already
	     (addr (host-address he))
	     (channel (http-search-channel addr))
	     ((eq? (http-channel-state channel) 'established)))
	    (if (not (host-collect-call he))
		(set-host-collect-call!
		 he (make-semaphore (dbgname (host-id he) "~a-cb") (if (number? cc) cc 2))))
	    (if (fx>= (semaphore-value (host-collect-call he)) 1)
		(begin
		  (semaphore-wait (host-collect-call he))
		  (http-do-connect-back
		   he answer-function
		   (if (pair? open) (car open) https-client-open-sslmgr))))))

(define (http-connect-back host answer-function . open)
  (and-let* ((he (cache-ref/default
		  *http-routeable-hosts*
		  (if (string? host) host (oid->string host))
		  #f #f)))
	    (apply http-connect-back/routeable-host he answer-function open)))

(define-record-type <ball-host>
  (make-host-entry id cert protocol address cc f)
  ball-host?
  (id host-id)
  (cert host-cert)
  (protocol host-protocol)
  (address host-address)
  (cc host-collect-call set-host-collect-call!)
  (f host-collect-call-function))

(define *http-routeable-hosts* #f)

(define (http-host-lookupV2 host)
  (and-let* ((x (cache-ref/default
		 *http-routeable-hosts*
		 (if (string? host) host (oid->string host))
		 #f #f)))
	    (and-let* ((f (or (host-collect-call-function x) (http-collect-call-function))))
		      (http-connect-back/routeable-host x f))
	    x))

(define (http-host-lookup host)
  (or
   (and-let* ((x (cache-ref/default
		  *http-routeable-hosts*
		  (if (string? host) host (oid->string host))
		  #f #f)))
	     (and-let* ((f (or (host-collect-call-function x) (http-collect-call-function))))
		       (http-connect-back/routeable-host x f))
	     (host-address x))
   (and-let* (((string? host))
	      (new-host (receive (s e protocol new-host r)
				 (http-location-regex host)
				 new-host))
	      (x (cache-ref/default *http-routeable-hosts* new-host #f #f)))
	     (and-let* ((f (or (host-collect-call-function x) (http-collect-call-function))))
		       (http-connect-back/routeable-host x f))
             (host-address x))))

(define (host-cert-lookup host)
  (and-let* ((entry (cache-ref/default *http-routeable-hosts* host #f #f)))
	    (host-cert entry)))

(define (http-all-hosts)
  (make-quorum (cache-fold
                *http-routeable-hosts*
                (lambda (k x i)
                  (if (equal? (host-address x) k) i (cons (or (string->oid k) k) i)))
                '())))

(define (http-display-hosts)
  ;; KLUDGE: this is only correct, if $connect-back doen't change
  ;; after host allocation.
  (define mx (let ((cc ($connect-back))) (if (number? cc) cc 2)))
  (make-xml-element
   'table #f '()
   (cache-fold
    *http-routeable-hosts*
    (lambda (k entry i)
      (sxml `((tr (td ,(literal (host-id entry)))
		  (td ,(literal (host-address entry)))
		  (td . ,(let ((cb (host-collect-call entry)))
			   (if cb (list (literal (fx- mx (semaphore-value cb)))) '()))))
	      . ,i)))
    (empty-node-list))))

(define *http-voters* #f)
(define *http-auths* #f)

(define (voter-auth voter)
  (hash-table-ref/default
   *http-voters* (if (string? voter) voter (oid->string voter)) #f))

;; FIXME: the type declaration reveals that we can get rid of this
;; definition alltogether.  However we better check fist that the type
;; declaration of mesh-cert-host is really correct!
;;
(: http-affirm-authorization (:mesh-cert: --> boolean))
(define (http-affirm-authorization auth)
  (or (mesh-cert-l auth)
      (let ((subject-string (mesh-cert-subject->string auth)))
	(logerr "fall back auth on ~a" subject-string)
	(hash-table-ref/default *http-auths* subject-string #f))))

(: http-affirm-user-authorization ((or false :mesh-cert:) :oid: --> (or boolean :oid:)))
(define (http-affirm-user-authorization auth oid)
  (and auth (eq? (mesh-cert-o auth) oid) oid))

(define http-collect-call-function (make-shared-parameter #f))

(: http-host-add! ((or string :oid:)
		   (or false :ip-addr:)
		   (or false :mesh-cert:)
		   boolean
		   -> (or string :oid:)))
(define (http-host-add! host address cert auth)
  (let* ((index (if (string? host) host (oid->string host)))
	 (old   (cache-ref/default *http-routeable-hosts* index #f #f))
	 (uri   (uri address))
	 (a2 (or (uri-authority uri) address)) ; why that?
	 (protocol (let ((s (memq (uri-scheme uri) '(https http))))
		     (or (and s (car s)) 'https))))
    (if (and old (not (equal? (host-address old) a2))
	     (or auth (not (http-host-already-alive? (host-address old)))))
	(begin
	  (cache-set!
	   *http-routeable-hosts* index values
	   (let ((cc ($connect-back)))
	     (http-close-channel! (host-address old) "new address")
	     (cache-set! *http-alive-cache* (host-address old))
	     (make-host-entry host (or (host-cert old) cert) protocol a2
			      (and cc (make-semaphore (dbgname host "~a-cb") (if (number? cc) cc 2)))
			      (and cc (http-collect-call-function)))))
	  #;(cache-invalid/abort! *http-alive-cache* (host-address old)))
	(if address
	    (cache-ref
	     *http-routeable-hosts* index
	     (lambda ()
	       (let ((cc ($connect-back)))
		 (make-host-entry host (or (and old (host-cert old)) cert) protocol a2
				  (and cc (make-semaphore (dbgname host "~a-cb") (if (number? cc) cc 2)))
				  (and cc (http-collect-call-function)))))))))
  host)

(: http-host-remove! ((or string :oid:) -> undefined))
(define (http-host-remove! host)
  (let ((host (if (string? host) host (symbol->string host))))
    (and-let* ((h (cache-ref/default *http-routeable-hosts* host #f #f)))
	      (and-let* ((c (host-cert h)))
			(hash-table-delete! *http-auths* (mesh-cert-subject->string c)))
	      (cache-set! *http-routeable-hosts* host)
	      (http-close-channel! (host-address h) "host removed"))))

(define (init-host-cache!)
  (set! *http-auths* (make-string-table))
  (set! *http-routeable-hosts*
	(make-cache "*http-routeable-hosts*"
		    string=?
		    #f ;; state
		    ;; miss:
		    (lambda (cs k) (cons k #t))
		    ;; hit (lambda (c es) #t)
		    #f
		    ;; fulfil
		    (lambda (c es v)
		      (if v
			  (begin
			    (set-cdr! es (car v))
			    (if (host-cert (car v))
				(hash-table-set! *http-auths*
						 (mesh-cert-subject->string (host-cert (car v)))
						 (or (string->oid (car es)) (car es)))))
			  ;; (set-cdr! es #f)
			  )
		      (values))
		    ;; valid?
		    (lambda (es) (cdr es))		; should test host alive?
		    ;; delete
		    (lambda (cs es)
		      (and-let* ((h (cdr es))
				 ((not (boolean? h))))
				(and-let* ((auth (host-cert h)))
					  (hash-table-delete! *http-auths*
							      (mesh-cert-subject->string auth)))
				(and-let* ((occ (host-collect-call h)))
					  (set-host-collect-call! h #t)
					  (semaphore-delete! occ))))
		    )))

(define (https-register-voter! target address its-cert
			       here-id here-address here-cert)
  (http-host-add! target address its-cert #f)
  (logit "Voter ~a ~a cert: ~a\n"
	 target address (and its-cert (mesh-cert-subject->string its-cert))))

(define (http-register-voter! target address its-id its-pw
			      here-id here-address my-id my-pw)
  (hash-table-set! *http-voters*
		   address ;; or:? (if (string? target) target (oid->string target))
		   (cons my-id my-pw))
  (if its-pw
      (hash-table-set! *http-auths*
		       (string-append "Basic "
				      (pem-encode-string
				       (string-append its-id ":" its-pw)))
		       target))
  (http-host-add! target address #f #f)
  (logit "Voter ~a ~a\n" target address))

(define (init-http-voters! voters)
  (or *http-auths* (init-host-cache!))
  (set! *http-voters* (make-string-table))
  (for-each (lambda (v)
	      (case (car v)
		((http) (apply http-register-voter! (cdr v)))
		((https) (apply https-register-voter! (cdr v)))
		))
            voters))
