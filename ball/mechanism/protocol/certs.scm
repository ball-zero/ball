;; (C) 2016

;; The cert handling/structure is to be changed soon.
;; For now we just wrap those plain strings used to far.
(: make-mesh-certificate (string (or :oid: false) (or :oid: false) --> :mesh-cert:))
(: mesh-certificate? (* --> boolean : :mesh-cert:))
(define-record-type <mesh-certificate>
  (make-mesh-certificate raw-subject l o)
  mesh-certificate?
  (raw-subject mesh-cert-raw-subject)
  (l mesh-cert-l)
  (o mesh-cert-o)
  )

(: subject-string->mesh-certificate (string --> :mesh-cert:))
(define subject-string->mesh-certificate
  ;; FIXME: this is a horrible hack to avoid "string->oid", which
  ;; would be the right thing to use.  But this is defined way up in
  ;; the module hierarchy.
  (let ((ml (irregex ".*L[[:space:]]*=[[:space:]]*([^/,]+).*"))
	(mo (irregex ".*O[[:space:]]*=[[:space:]]*(A[0-9a-f]{32}).*")))
    (define (subject-string->mesh-certificate raw-subject)
      (let ((l (irregex-match ml raw-subject))
	    (o (irregex-match mo raw-subject)))
	(make-mesh-certificate
	 raw-subject
	 (and l (let ((ls (irregex-match-substring l 1)))
		  (if (eqv? (string-ref ls 0) #\A) (string->symbol ls) ls)))
	 (and o (string->symbol (irregex-match-substring o 1))))))
    subject-string->mesh-certificate))

(: make-mesh-cert ((or :oid: false) (or :oid: false) --> :mesh-cert:))
(define (make-mesh-cert host user)
  (make-mesh-certificate
   (if user
       (string-append "L=" (symbol->string host) ",O=" (symbol->string user))
       (string-append "L=" (symbol->string host)))
   host user
   ))

(: mesh-cert-subject->string (:mesh-cert: --> string))
(define (mesh-cert-subject->string cert) (mesh-cert-raw-subject cert))

(: mesh-cert=? (:mesh-cert: :mesh-cert: --> boolean))
(define (mesh-cert=? a b)
  ;; (string=? (mesh-cert-raw-subject a) (mesh-cert-raw-subject b))
  (and (equal? (mesh-cert-l a) (mesh-cert-l b)) (equal? (mesh-cert-o a) (mesh-cert-o b))))
