(define save-config-flag #f)

(define (config-save-reqested?)
  save-config-flag)

(define (ball-config-saved!)
  (set! save-config-flag #f))

(define (ball-trigger-save-config!)
  (set! save-config-flag #t))

;; Host mapping

(define $host-map-size (make-shared-parameter 10))

(define (host-entry-not-here pred)
  (define here (if (equal? (http-host-lookup (public-oid)) (local-id))
		   (public-oid) (local-id)))
  (lambda (k entry)
    (and (not (equal? (host-id entry) here))
	 (pred k entry))))

(define (host-entry-alive? k entry)
  (and-let* ((x (host-id entry))
	     (h (http-host-lookup (if (symbol? x) (symbol->string x) x))))
	    (http-host-maybe-alive? h)))

(define (not-host-entry-alive? k e) (not (host-entry-alive? k e)))

(define (host-entry-true k entry) #t)

(define (ball-host-map-fold-entry->xml k entry i)
  (let* ((x (host-id entry))
	 (about (if (symbol? x) (symbol->string x) x)))
    (sxml `((Host (@ (about ,about))
		  (protocol ,(literal (host-protocol entry)))
		  (address ,(host-address entry))
		  . ,(if (host-cert entry)
			 `((ssl-certificate ,(mesh-cert-subject->string (host-cert entry))))
			 '()))
	    . ,i))))

(define (ball-host-map->xml table pivot max)

  (make-xml-element
   'HostsDB #f '()
   (let ((x (fold-next-strings
	     table
	     pivot
	     (host-entry-not-here host-entry-alive?)
	     max
	     ball-host-map-fold-entry->xml
	     (empty-node-list))))
     (if (< (node-list-length x) ($host-map-size))
	 (append
	  x
	  (fold-next-strings
	   table
	   pivot
	   (host-entry-not-here not-host-entry-alive?)
	   max
	   ball-host-map-fold-entry->xml
	   (empty-node-list)))
	 x))))

(: ball-add-host! (string :ip-addr: (or false :mesh-cert:) boolean -> boolean))
(define (ball-add-host! id address cert auth)
  (and-let* ((public (public-oid))
	     ;; sanity check
	     ((or (ip-address/port? address)
		  (and-let* ((socks ($https-use-socks4a))) (socks address))))
	     (here (if (eq? public one-oid)
		       (let ((l (local-id))) (and (not (string=? address l)) l))
		       (let ((l (oid->string public))) (and (not (string=? id l)) l))))
	     ((not (let ((hc (host-cert-lookup here)))
		     (and hc cert (mesh-cert=? cert hc))))))
	    (if auth
		(http-host-add! id address cert #t)
		(let ((old (cache-ref/default *http-routeable-hosts* id #f #f)))
		  (if old
		      (let ((oc (host-cert old))
			    (oid (string->oid id)))
			(if (or (not oc) (mesh-cert=? oc cert))
			    (http-host-add! (or oid id) address (or oc cert) #f)))
		      (http-host-add! (or (string->oid id) id) address cert #f))))))

(: ball-read-host-map! (:xml-nodelist: &optional boolean -> *))
(define (ball-read-host-map! node . connect)
  (!start
   (node-list-map
    (lambda (host)
      (and
       (eq? (gi host) 'Host)
       (let ((cert (and-let* ((subject (data ((sxpath '(ssl-certificate)) host)))
			      ((not (string-null? subject))))
			     (subject-string->mesh-certificate subject))))
	 (let ((id (if cert (literal (mesh-cert-l cert)) (attribute-string 'about host)))
	       (address (data ((sxpath '(address)) host)))
	       (protocol ((sxpath '(protocol)) host)))
	   (ball-add-host! id address cert #f)
	   (and (pair? connect) (car connect) ;(not (equal? (local-id) id)) 
		(askemos-reconnect-if-required
		 id
		 (if (node-list-empty? protocol)
		     (if cert address (string-append "https://" address))
		     (string-append (data protocol) "://" address))
		 #f))))))
    (children node))
   "read-host-map"))

(define write-host-map-as-zone-file
  (let ((last-soa #f)
	(last-content #f)
	(last-time (xthe (or false :time:) #f)))
    (define (write-host-map-as-zone-file table port)
      ;; At most one update per minute.
      (if (and last-time
	       (>= (+ 60 (time-second last-time)) (time-second *system-time*)))
	  (begin
	    (display last-soa port)
	    (display last-content port))
	  (let ((here (local-id))
		(serial (quotient (time-second *system-time*) 600))
		(refresh 86400) (retry 7200) (expire 3600000)
		;; Yes, we want to set the minimum ttl to only 15 minutes.
		(ttl 900))
	    (let ((now
		   (call-with-output-string
		    (lambda (port)
		      (display
		       (call-with-output-string
			(lambda (p2)
			  (cache-fold
			   *http-routeable-hosts*
			   (lambda (k v i)
			     (let ((h (http-host-lookup k)))
			       (if (and-let*
				    ((h)
				     (t (http-host-alive? h)))
				    (< (- (time-second *system-time*)
					  (time-second t)) 900))
				   (let ((k (or (and-let*
						 ((m ((pcre->proc "([^.]+)") k)))
						 (cadr m))
						k))
					 (addr (or (and-let*
						    ((m (ip-match (host-address v))))
						    (string-append
						     (cadr m) "." (caddr m) "."
						     (cadddr m) "." (list-ref m 4)))
						   (host-address v))))
				     (format port "@\tIN\tNS\t~a\n" k addr)
				     (if (not (equal? h here))
					 (format p2 "~a\tIN\tA\t~a\n" k addr))))))
			   #f))) port)))))
	      (if (not (equal? now last-content))
		  (begin
		    (set! last-content now)
		    (set! last-soa
			  (let ((admin (or (and-let*
					    (((string? $administrator))
					     (m ((pcre->proc "^([^@]+)@(.*)$")
						 $administrator)))
					    (string-append (cadr m) "." (caddr m)))
					   (string-append "root." here))))
			    (format #f "@\tIN\tSOA\t~a. ~a. (~a ~a ~a ~a ~a)\n"
				    here admin serial refresh retry expire ttl)))))
	      (display last-soa port)
	      (display last-content port))
	    (set! last-time *system-time*))))
    write-host-map-as-zone-file))

(define (askemos-connect-back-function request)
  (let ((action (get-slot request 'soapaction)))
    (if action
	(askemos-sync request action)
	(error "NYI"))))

(http-collect-call-function askemos-connect-back-function)

(define (askemos-connect-back host)
  (and-let* ((n ($connect-back))
	     (to (host-lookup* host))
	     (((host-alive?) to)))
	    (do ((i (if (number? n) n 2) (sub1 i)))
		((fx>= 0 i))
	      (http-connect-back host askemos-connect-back-function))))

(define $external-port (make-shared-parameter 7443))
(define $external-address (make-shared-parameter #f))

(define (askemos-connect to . from+request)
  (define here (if (equal? (http-host-lookup (public-oid)) (local-id))
		   (oid->string (public-oid)) (local-id)))
  (receive
   (from client request) (if (pair? from+request)
			     (apply values from+request)
			     (values here #f #f))
   (let* ((public (public-oid))
	  (body
	   (sxml
	    `(env:Envelope
	      (@ (@ (*NAMESPACES*
		     (env "http://www.w3.org/2003/05/soap-envelope")
		     (s "http://www.softeyes.net/2005/DynDNS/"))))
	      ,@(if (or (not client) (equal? from here)) '()
		    `((env:Header
		       (s:client (@ (env:mustUnderstand "true")
				    (s:href ,client))
				 ,from))))
	      (env:Body
	       (s:Registration
		(s:Host ,(public-meta-info (document public)))
		(s:Protocol "https")
		,@(let ((route (let ((r ($external-address)))
                                 (cond
                                  ((not r) r)
                                  ((ipv6-address? r)
                                   (let ((p ($external-port)))
                                     (if (number? p)
                                         (string-append "[" r "]:" (number->string p))
                                         r)))
                                  (else r)))))
		    (if route `((s:Route ,route)) '()))
		(s:Port ,(or (and-let* (((ipv4-address/port? from))
                                        (m ((pcre->proc "[^:]+:([[:digit:]]+)")
					    from)))
				       (cadr m))
			     ($external-port))))
	       . ,(if request
		      `((s:Lookup ,request))
		      '())))))
	  (http ((pcre->proc "^http://(.*+)") to))
	  (https ((pcre->proc "^https://(.*+)") to))
	  (host (cond (https (cadr https)) (http (cadr http)) (else to)))
	  (msg (make-message
		(property 'dc-identifier (oid->string one-oid))
		(property 'host host)
		http-property-soap+xml
		http-property-post
		(property 'dc-date (timestamp))
		(property 'SOAPAction "Hello")
		(property 'body/parsed-xml body)))
	  (ans
	   (let* ((tmo (max (or (respond-timeout-interval) 6) 60))
		  (handle (register-timeout-message! tmo (current-thread))))
	     ;; (logagree "C trying to connect to ~a\n" to)
	     (guard
	      (ex ((timeout-object? ex)
		   (logtopo "Hello ~a failed with: ~a\n" to ex))
		  (else
		   (cancel-timeout-message! handle)
		   (logerr "Hello ~a failed with: ~a\n" to ex) ex))
	      (if http
		  (http-response msg
				 ;; authentication: (cons "public" "anonymous")
				 )
		  (https-response-auth msg #f #f))))))
     (and (frame? ans)
	  (let* ((b (document-element (message-body ans))) (hostlist ((sxpath '(HostsDB)) b)))
	    (if (node-list-empty? hostlist)
		(begin
		  (logtopo "Error registering IP ~a status ~a: ~a\n"
			   to (get-slot ans 'http-status)
			   (if ($https-client-verbose) (message-body/plain ans) ""))
		  #f)
		(let ((peer (attribute-string 'about b))
		      (auth (get-slot ans 'authorization)))
		  (if (equal? (if (symbol? auth) (symbol->string auth) auth) peer)
		      (begin
			(ball-add-host! peer host (get-slot ans 'ssl-peer-certificate) #f)
			(askemos-connect-back peer))
		      (logerr "connect to ~a got ~a @ ~a\n" peer (get-slot ans 'authorization)
			      (mesh-cert-subject->string (get-slot ans 'ssl-peer-certificate))))
		  (ball-read-host-map! hostlist #t)
		  (ball-trigger-save-config!)
		  (if (not client)
		      (askemos-reconnect-all))
		  #t)))))))

(define (forward-lookup host from client)
  (and
   (not (equal? host from))
   (let () ; ((cmp (if (string<? host from) string<? string>?)))
     (fold-next-strings
      *http-routeable-hosts*
      host host-entry-alive? 1
      (lambda (k entry i)
	(if (and
	     ;; (cmp k host)
	     (not (let ((id (literal (host-id entry))))
		    (or (string=? id from) (and client (string=? id client))))))
	    (begin
	      (!start
	       (askemos-connect
		(literal (host-protocol entry) "://" (host-address entry))
		from client host)
	       "aconn")
	      (add1 i))
	    (begin host i)))
      0))))

(define askemos-host-lookup
  (let ((cache (make-cache "askemos-host-lookup"
			   string=?
			   #f ;; state
			   ;; miss:
			   (lambda (cs k) (cons k #f))
			   ;; hit
			   #f ;; (lambda (c es) #t)
			   ;; fulfil
			   (lambda (c es results)
			     (set-cdr! es (add-duration
					   *system-time*
					   (make-time
					    'time-duration 0
					    (if (and (pair? results) (car results)) 600
						(or (respond-timeout-interval) 60)))))
			     (values))
			   ;; valid?
			   (lambda (es)
			     (or (not (cdr es))
				 (srfi19:time<=? *system-time* (cdr es))))
			   ;; delete
			   #f ;; (lambda (cs es) #f)
			   )))
    (define (askemos-host-lookup host . nowait)
      (set! host (if (symbol? host) (symbol->string host) host))
      (or (http-host-lookup host)
	  (and (string? host) (ip-address/port? host) host)
	  (let ((lookup (lambda ()
			  (let ((n (forward-lookup
				    host
				    (if (equal? (http-host-lookup (public-oid)) (local-id))
					(oid->string (public-oid)) (local-id))
				    #f)))
			    (let ((h (http-host-lookup host)))
			      (or h
				  (and
				   n (> n 0)
				   (let loop ((n 2))
				     (and (< n 8)
					  (or (http-host-lookup host)
					      (begin
						(thread-sleep! n)
						(loop (add1 n)))))))))))))
	    (if (null? nowait)
		(cache-ref cache host lookup)
		(cache-ref/default cache host lookup #f)))))
    (define (cleanup-host-lookup)
      (cache-cleanup! cache)
      (register-timeout-message! 600 cleanup-host-lookup))
    (register-timeout-message! 600 cleanup-host-lookup)
    askemos-host-lookup))

;; Code transition: we'll need additional code in the update, possibly
;; from the config, to maintain vhost settings.

;; TODO: phase out $dns-zone in favor of a parameterized update
;; procedure.

(define $dns-zone (make-parameter #f))

(define (simple-update-dns! location peer port)
  (if (and peer ($dns-zone) (not (string-index location #\.))
	   (not (string=? location (oid->string (public-oid)))))
      (guard
       (ex (else (log-condition (format "update-dns! \"~a\"" location) ex)))
       (let ((ns (dns-find-nameserver))
	     (location (string-append location "." ($dns-zone))))
	 (guard
	  (ex (else (dns-set-address! ns location peer)))
	  (if (not (equal? (dns-get-address ns location) peer))
	      (dns-set-address! ns location peer)))))))

(define $dns-update-hook (make-shared-parameter simple-update-dns!))
(define (update-dns! location peer port) (($dns-update-hook) location peer port))

(define ascending-number-wt-tree number-wt-type)
(define descending-number-wt-tree (make-wt-tree-type >))

(define next-strings-modus #x3ffffff)

(define next-strings-median (inexact->exact (floor (/ next-strings-modus 2))))

(define next-strings-rough-len (string-length (number->string next-strings-modus 16)))

(define (next-strings-hash str)
  (-
   (modulo
    (string->number (if (eqv? (string-ref str 0) #\A)
			(substring str 1 next-strings-rough-len)
			(substring (md5-digest str) 0 next-strings-rough-len))
		    16)
    next-strings-modus)
   next-strings-median))

(define (fold-next-strings cache pivot test range combine init)
  (let ((pivot (next-strings-hash pivot))
	(r (vector (make-wt-tree ascending-number-wt-tree)
		   (make-wt-tree descending-number-wt-tree))))
    (cache-fold
     cache
     (lambda (k0 v i)
       (if (test k0 v)
	   (let* ((k (next-strings-hash k0))
		  (i (if (< k pivot) 0 1))
		  (side (vector-ref r i))
		  (other (vector-ref r (abs (fx- i 1)))))
	     (wt-tree/add! side k v)
	     (if (< (wt-tree/size side) range)
		 (begin
		   (if (< (wt-tree/size other) range)
		       (let ((p (wt-tree/min-pair side)))
			 (wt-tree/add! other (car p) (cdr p))))
		   (wt-tree/delete-min! side))))))
     #f)
    (wt-tree/fold
     combine
     (wt-tree/fold combine init (vector-ref r 1))
     (vector-ref r 0))))

(define *reconnect-cache*
  (let ((cache (make-cache
		"*reconnect-cache*"
		string=?
		#f ;; state
		;; miss:
		(lambda (cs k) (vector k #f #f))
		;; hit (lambda (c es)  #t)
		#f
		;; fulfil
		(lambda (c es results)
		  (define success (and (pair? results) (car results)))
		  (vector-set! es 1 (add-duration
				     *system-time*
				     (if success
					 host-seems-alive-duration
					 host-seems-dead-duration)))
		  (if success
		      (http-connect-back (vector-ref es 0) askemos-connect-back-function))
		  (values))
		;; valid?
		(lambda (es)
		  (or (not (vector-ref es 1))
		      (srfi19:time<=? *system-time* (vector-ref es 1))))
		;; delete
		#f ;; (lambda (cs es) #f)
		)))
    (define (cleanup-reconnect-cache)
      (cache-cleanup! cache)
      (register-timeout-message! 600 cleanup-reconnect-cache))
    (register-timeout-message! 600 cleanup-reconnect-cache)
    cache))

(define (askemos-reconnect-if-required location addr wait)
  (define here (if (equal? (http-host-lookup (public-oid)) (local-id))
		   (oid->string (public-oid)) (local-id)))
  (define (reconnect)
    (and (let loop ((n 4))
	   (or (askemos-connect addr)
	       (and (< n 16)
		    (begin (thread-sleep! n) (loop (fx* n 2))))))
	 (begin
	   (logtopo "refreshed connection to ~a\n" location)
	   (http-rerequest-digests!
	    (or (string->oid location) location)
	    (http-host-lookup location))
	   #t)))
  (and
   (not (equal? here location))
   (if wait
       (cache-ref *reconnect-cache* location reconnect)
       (cache-ref/default *reconnect-cache* location reconnect #f))))

(define (askemos-reconnect-all)
  (fold-next-strings
   *http-routeable-hosts*
   (local-id)
   host-entry-true
   ($host-map-size)
   (lambda (k v i)
     (!start
      (guard
       (ex ;;(else (log-condition (format "connect to ~a failed" (host-address v)) ex))
	   (else (logtopo "connect to ~a failed: ~a\n" (host-address v) ex)))
       (let ((id (let ((id (host-id v))) (if (symbol? id) (symbol->string id) id))))
	 (askemos-reconnect-if-required
	  id
	  (string-append (symbol->string (host-protocol v)) "://"
			 (host-address v))
	  #t)
	 (let ((addr (let ((colon (string-index (host-address v) #\:)))
		       (if colon (substring (host-address v) 0 colon) (host-address v))))
	       (id (let ((dot (string-index id #\.))) (if dot (substring id 0 dot) id))))
	   (update-dns! id addr #f))))
      (host-address v)))
   #f))


(: make-new-peer (:oid: --> :place:))
(define (make-new-peer id)
  (or
   (let ((frame ((resynchronize) (make-quorum (list id)) id)))
     (fset! frame 'protection (car (public-capabilities)))
     (fset! frame 'capatilities (public-capabilities))
     (commit-frame! #f frame (aggregate-meta frame) '())
     frame)
   (make-place-for-id!
    id
    (property 'protection (car (public-capabilities)))
    (property 'capatilities (public-capabilities))
    (property 'replicates (make-quorum (list id)))))
  )

(: askemos-hello-reply :aggrement-handler:)
(define (askemos-hello-reply oid request)
  (define client (node-list-first ((sxpath '(Header client))
				   (message-body request))))
  (define lookup ((sxpath '(Body Lookup *text*))
		  (message-body request)))
  (and-let*
   ((reg (message-body request))
    (host (node-list-first
	   ((sxpath '(Body Registration Host RDF Description))
	    reg)))
    (peer (let ((route (node-list-first
			((sxpath '(Body Registration Route))
			 reg))))
	    (cond
	     ((and (not (node-list-empty? route))
		   (let* ((ri (data route)))
		     (and (not (string-null? ri)) ri)))
	      => (lambda (x) x))
	     (else
	      (let* ((s (literal (or (get-slot request 'http-peer)
				     (error "http-peer unknown in hello request"))))
		     (colon (and (not (ipv6-address? s)) (string-index-right s #\:))))
		(if colon (substring s 0 colon) s))))))
    (port (data (node-list-first
		 ((sxpath '(Body Registration Port))
		  reg))))
    (route (node-list-first
	    ((sxpath '(Body Registration Route)) reg)))
    (about (attribute-string 'about host)))
   (!start
    (let ((id (string->oid about))
	  (addr (if (node-list-empty? route)
                    (cond
                     ((ipv6-address? peer) (string-append "[" peer "]:" port))
                     ((ipv6-address/port? peer) peer)
                     (else (string-append peer ":" port)))
		    (data route)))
	  (peercert (get-slot request 'ssl-peer-certificate)))
      (let* ((location (if peercert (literal (mesh-cert-l peercert)) about))
	     (entry (http-host-lookupV2 location))
	     (oldcert (and entry (host-cert entry)))
	     (oldaddr (and entry (host-address entry))))
	(if (or (not oldcert)
		(mesh-cert=? peercert oldcert))
	    (update-dns! location (if (node-list-empty? route) peer (data route)) port)
	    (logtopo "T: NO DNS update peer ~a old ~a\n" peercert oldcert))
	(cond
	 ((equal? addr oldaddr)
	  (logtopo "T: unchanged ~a at ~a\n" location addr))
	 (else
	  (if (and oldcert (not (mesh-cert=? peercert oldcert)))
	      (logtopo "T: cert change ~a at ~a old ~a now ~a\n"
		       id addr oldcert peercert))
	  (if (and oldaddr
		   (not (equal? addr oldaddr)))
	      (begin
		;(http-close-channel! oldaddr "hello: new address")
		(cache-set! *reconnect-cache* location)))
	  (ball-add-host! location addr peercert #t)))
	(or (find-local-frame-by-id id 'askemos-hello-reply)
	    (make-new-peer id))
	(let ((addr (if peercert (string-append "https://" addr) addr)))
	  (if peercert
	      (askemos-connect-back location))
	  (and-let*
	   (((pair? lookup))
	    ((not (node-list-empty? client)))
	    (href (attribute-string 'href client)))
	   (if (and (not (equal? location href))
		    (http-host-lookup (data lookup)))
	       (askemos-reconnect-if-required href (data client) #f)
	       (forward-lookup (data lookup) href (data client))))))
      (ball-trigger-save-config!))
    "hello"))
  (make-message
   http-property-soap+xml
   (property
    'body/parsed-xml
    (let ((here ((host-lookup) (public-oid))))
      (sxml
       `(Here (@ (about ,(if (equal? here (local-id))
			     (oid->string (public-oid)) (local-id))))
       ,(ball-host-map->xml
	 *http-routeable-hosts*
	 (if (null? lookup)
	     (if (equal? here (local-id))
		 (oid->string (public-oid)) (local-id))
	     (data lookup))
	 ($host-map-size))))))))

(register-agreement-handler! 'Hello askemos-hello-reply)

(define (http-do-connect-back-implementation he answer-function open)
  (define auth #t)
  (parameterize
   ((*http-transfered-headers* http-agreement-headers))
   (http-cc-send-message!
    open #f
    (lambda (connection)
      (if (bidirectional-connection? connection)
	  (if (connection-certificate connection)
	      (thread-start!
	       (make-thread
		(lambda ()
		  (define sig (mesh-cert-l (connection-certificate connection)))
		  (define hen (cache-ref
			       *http-routeable-hosts* (literal sig)
			       (lambda ()
				 (let ((cc ($connect-back)))
				   (make-host-entry
				    sig (connection-certificate connection) 'https (host-address he)
				    (and cc (make-semaphore (dbgname (host-address he) "~a-cb")
							    (if (number? cc) cc 2)))
				    (and cc (http-collect-call-function)))))))
		  (update-alive-time-from-callback-server! sig)
		  (logtopo "CONNECT / ~a listening\n" sig)
		  (parameterize
		   (($https-requests-per-connection #t))
		   (guard
		    (ex ((eq? ex 'connection-captured)
			 (http-client-dispose-connection (http-find-channel (host-lookup* sig)) connection #t)
			 (set! connection #f)
			 ;;(raise ex)
			 #f)
			(else (log-condition (host-address hen) ex)))
		    (http-request-server
		     (string->inet-socket-addr (host-address hen)) (connection-certificate connection)
		     (connection-input-port connection)
		     (connection-output-port connection)
		     auth "https://" 'http-location-format
		     answer-function)))
		  (let ((s (host-collect-call hen)))
		    (if s
			(semaphore-signal s)
			(begin
			  (logtopo "CONNECT / ~a channel is closed\n" sig)
			  (raise 'channel-is-closed))))
		  (logtopo "CONNECT / ~a connection closed\n" sig)
		  (connection-close! 'http-do-connect-back-implementation connection)
		  (http-connect-back sig answer-function open))
		(string-append (host-address he) "-back")))
	      (begin
		(and-let* ((s (host-collect-call he))) (semaphore-signal s))
		(connection-close! 'http-do-connect-back-implementation connection)
		(logtopo "CONNECT no certificate on ~a\n" (connection-name connection))))
	  (begin
	    (and-let* ((s (host-collect-call he))) (semaphore-signal s))
	    (logtopo "CONNECT / ~a fail status ~a\n" (host-address he)
		     (if (frame? connection) (get-slot connection 'http-status) connection)))))
    (make-message
     (property 'type 'CONNECT)
     (property 'host (host-address he))
     (property 'dc-identifier http-connect-back-magic))
    (voter-auth (host-address he))
    #t)))

(set-http-do-connect-back! http-do-connect-back-implementation)
