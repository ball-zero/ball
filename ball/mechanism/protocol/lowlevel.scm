;;*** Evaluation server

;; The following eval-server is only for debugging purpose.  It give
;; you possibly restricted access to manipulate the whole system.
;; This can be very dangerous: if you accidentaly keep a reference to
;; some object to insert later somewhere, the rstore will break giving
;; messages like "access protection at address..."

;; PARAMETER
;;   alist - (symbol . funtion) - "function" to apply if "symbol" is
;;   in operator position.  Special case: if the symbol read at
;;   operator position is not found in alist, we look for 'eval and
;;   apply it's cdr to the whole expression.  This gives essentially
;;   an repl, and, if 'eval is not found, some administrative

;;   operations.

;currently respond-timeout-interval
(define http-eval-respond-timeout
  (make-shared-parameter #f))

(define (eval-request-server in-port out-port peer . alist)
  (let* ((quorum *local-quorum*)
	 (last-values #f)
	 (eval (and-let* ((x (assq 'eval alist))) (cdr x)))
	 (cmd-list
	  `((q . ,(lambda arg
		    (if (pair? arg)
			(let ((q (eval (cons 'list arg))))
			  (set! quorum (cond
					((quorum? (car q)) (car q))
					(else (make-quorum q)))))
			(print quorum))))
	    . ,alist)))
    (bind-exit
     (lambda (return)
       (let loop ()
	 (display ";;; " out-port)
	 (if (quorum-local? quorum) (format out-port "~a " (public-oid)))
	 (if (pair? (quorum-others quorum))
	     (begin
	       (if (quorum-local? quorum) (display "and " out-port))
	       (do ((x (quorum-others quorum) (cdr x)))
		   ((null? x))
		 (format out-port "~a " (car x)))))
	 (display "Nu\n" out-port)
	 (flush-output-port out-port)
	 (guard
	  (c ((message-condition? c)
	      (format out-port "Message: ~a ~a\n"
		      (if (http-effective-condition? c)
			  (http-effective-condition-status c)
			  "[no status]")
		      (condition-message c)))
	     (else (receive
		    (title msg args rest) (condition->fields c)
		    (format out-port "Exception: ~a ~a ~a\n" title msg args)
		    c)))
	  (let ((expr (sweet-read in-port)))
	    (if (eof-object? expr)
		(return expr)
		(let ((proc (and-let* ((x (assq (cond
						 ((pair? expr) (car expr))
						 ((symbol? expr) expr)
						 (else #f))
						cmd-list)))
				      (cdr x))))
		  (with-output-to-port out-port
		    (lambda ()
		      (if proc
			  (format #t "~s\n"
				  (apply proc (if (pair? expr) (cdr expr) '())))
			  (if eval
			      (begin
				(if (null? (quorum-others quorum))
				    (format #t "~s\n" (eval expr))
				    (if (quorum-local? quorum)
					(begin
					  (http-eval #f quorum expr)
					  (format #t "~s\n" (eval expr)))
					(let ((answer (within-time
						       (http-eval-respond-timeout)
						       (http-eval #t quorum expr))))
					  (if (frame? answer)
					      (let ((res (xml-parse
							  (get-slot answer 'mind-body))))
						(if (eq? (gi (document-element res))
							 'evaluation-result)
						    (begin
						      (display (data ((sxpath '(output)) res)))
						      (let ((vals (map
								   (lambda (r)
								     (let ((d (data r)))
								       (format #t "~a\n" d)
								       (guard
									(ex (else ex))
									(call-with-input-string
									 d read))))
								   ((sxpath '(results *)) res))))
							(apply values vals)))
						    (begin
						      ;; (display (xml-format ((sxpath '(Body Fault Reason Text *any*)) res)))
						      (display (xml-format res))
						      (values))))
					      (raise answer))))))
			      "Sorry, secure mode.\n"))))))))
	 (loop))))))

;; I leave the server intentionaly single threaded.  Because I don't
;; expect more that one person do dangerous actions at the same time,
;; so why confuse the poor developer.

(define (eval-server port . alist)
  (askemos:run-tcp-server
   #f port
   (lambda (in out peer)
     (logerr "EVAL CONNECT FROM ~a\n" peer)
     (apply eval-request-server in out peer alist))
   (make-semaphore 'eval-server 1) #f "debug-access"))



(cond-expand
 (chicken
  (define (copy-port-bytewise in out)
    (copy-port in out read-char (lambda (x out) (write-char x out) (flush-output out)))))
 (else
  ;; FIXME, chicken's version looks better.
  (define (copy-port-bytewise in out)
    (guard
     (c ((or (eof-object? c) (timeout-object? c)) (close #f))
	(else (receive
	       (title msg args rest) (condition->fields c)
	       (logcond 'SSL-TRANSPORT
			title (if (message-condition? c) (condition-message c) c) args)
	       (close #f))))
     (let loop ()
       (let ((line (expect-object
		    #f ; (http-read-status-line-timeout)
		    raise not-eof-object? read-line in)))
	 (display line out)
	 (newline out)
	 (flush-output-port out)
	 (loop)))))))

(define (make-tunnel ssl host local remote)
  (define (hostspec->ip host)
    (if (or (not host) (char-numeric? (string-ref host 0)))
	host
	(and-let* ((ns (dns-find-nameserver))) (dns-get-address ns host))))
  (define (resolved host)
    (cond
     ((ip-address/port? host) host)
     (else (let* ((colon (string-index host #\:))
		  (name (if colon (substring host 0 colon) host))
		  (ns (dns-find-nameserver))
		  (ip (and ns (dns-get-address ns name))))
	     (if colon
		     (string-append ip ":" (substring host (add1 colon) (string-length host)))
		     ip)))))
  (thread-start!
   (make-thread
    (lambda ()
      (guard
       (ex (else (logerr "~a TUNNEL ~a\n" (if ssl "SSL " "") ex)))
       (let ((remote (resolved remote)))
	 (askemos:run-tcp-server
	  (hostspec->ip host) local
	  (lambda (sin sout client)
	    (receive
	     (cin cout cert alive? close)
	     ((if ssl https-client-open http-client-open) remote)
	     (let* ((pump
		     (lambda (in out)
		       (lambda ()
			 (copy-port-bytewise in out))))
		    (t1 (make-thread
			 (pump sin cout)
			 (string-append (if ssl "SSL " "") "tunnel client to " remote)))
		    (t2 (make-thread
			 (pump cin sout)
			 (string-append (if ssl "SSL " "") "tunnel server to " remote))))
	       (guard (c (else (receive
				(title msg args rest) (condition->fields c)
				(logcond 'TUNNEL
					 title (if (message-condition? c) (condition-message c) c) args)) (close #f)
					 (guard (ex (else #f)) (thread-terminate! t1))
					 (guard (ex (else #f)) (thread-terminate! t2))
					 (if (not (eof-object? c)) (raise c))))
		      (thread-start! t1)
		      (thread-start! t2)
		      (thread-join! t2)))))
	  #f #f (string-append (if ssl "SSL " "") "tunnel to " remote)))))
    (string-append (if ssl "SSL " "") "tunnel to " remote))))

;; For API compatibility at this time.
(define (ssl-tunnel host local remote certinfo)
  (make-tunnel #t host local remote))
