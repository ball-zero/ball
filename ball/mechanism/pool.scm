;; (C) 1997-2003 Jörg F. Wittenberger see http://www.askemos.org

(: set-resync-completion! (:resync!: -> undefined))


;;** Upcalls.  It would be nice to reduce the number of these.  It's a
;; rather ugly technique to break up the heavy in-module dependencies
;; of the original, huge "place" module.

(: nu-action (symbol (or :oid: false) :place: :quorum: -> *))
(define nu-action
  (lambda args (error "(internal) nu-action not initilized")))

(define call-subject!-f (lambda (subject from-quorum type message) (raise 'no-call-subject!)))

(: call-subject! :call-subject:)
(define (call-subject! subject from-quorum type message)
  (call-subject!-f subject from-quorum type message))

(: set-call-subject! (:call-subject: -> undefined))
(define (set-call-subject! f)
  (if (procedure? f)
      (set! call-subject!-f f)))

(define (no-commit-frame! deleted-slot frame changes new-links)
  (raise 'no-commit-frame!))

(: commit-frame! :commit-frame!:)
(define commit-frame! no-commit-frame!)

(: set!-commit-frame! (:commit-frame!: -> :commit-frame!:))
(define (set!-commit-frame! new)
  (let ((old commit-frame!))
    (set! commit-frame! new)
    old))

(define (no-resync-now! id) (raise 'no-resync-now!))

(define resync-now! no-resync-now!)

(define (set!-resync-now! f) (set! resync-now! f))

(define-condition-type &object-not-available &http-effective-condition
  object-not-available-condition?
  (name missing-object-resource)
  (source missing-object-source))

(define (make-object-not-available-condition name source . location)
  (make-condition &object-not-available 'name name 'source source
		  'message (format "~a in ~a object ~a not found" location
				   (if (quorum? source)
				       (cons (quorum-local? source) (quorum-others source))
				       source)
				   name)
		  'status 404))

;;*** Persistent Data Structures, Constants and Configuration

;; CONFIG: (define mind-file-name "/tmp/me.mm")

;; CAUTION
;;
;; Never write any data structure, which could possible be reached
;; from one of the following global variables without obtaining a
;; proper access permit by using the "with-mind-access" procedure!
;; (Early pstore versions could be broken in the process.)

(define (create-pool . args)
  (make-cache
   "*pool*"
   oid=?
   #f ;; state
   ;; miss:
   (lambda (cs k) (vector #t *system-time* k))
   ;; hit
   (lambda (c es) (vector-set! es 1 *system-time*) (values))
   ;; fulfil
   (lambda (c es results)
     (vector-set! es 0 (and (pair? results) (aggregate? (car results))))
     (vector-set! es 1 *system-time*)
     ;; This signal is only sometimes required.  We should send it
     ;; only, if there's a change above the sync state.  However the
     ;; cache API does not have enough information to know.
     ;; (cache-invalid! *resync-cache* (vector-ref es 2))
     (values))
   ;; valid?
   (lambda (es) (vector-ref es 0))
   ;; delete
   #f ;; (lambda (cs es) (close-oid-channel! (vector-ref es 2)))
   ))

(define (initial-mind)
  `((use-pool . ,(create-pool))
    (waiting-readers)
;;    (use-index . ,(create-index)) ; Might be overwritten
    ))
(define mind-pool (the * #f))
(define mind-waiting-readers (the * #f))	; obsolete already?

(define (pool-init!)
  (let ((initial-mind (initial-mind)))
    (set! mind-pool (cdr (assq 'use-pool initial-mind)))
    (set! mind-waiting-readers (assq 'waiting-readers initial-mind))))

(pool-init!)

(define (mind-pool-cleanup!)
  (cache-cleanup!
   mind-pool
   (let ((old (subtract-duration
	       *system-time*
	       (make-time 'time-duration 0
			  (or (respond-timeout-interval) 10)))))
     (lambda (es)
       (and (vector-ref es 0)
	    (srfi19:time<? (vector-ref es 1) old))))
   (lambda (e)
     (and (aggregate? e)
	  (let ((k (aggregate-entity e)))
	    (and (oid-channel-empty? k) (not (has-a-ref? k))))))))

(define (bind-place! oid obj new)
  (if new (cache-invalid! *resync-cache* oid))
  (cond
   ((aggregate? obj) (cache-set! mind-pool oid values obj) obj)
   ((not obj) (cache-invalid! mind-pool oid) #f)
   (else (cache-set! mind-pool oid raise obj) #f)))

;; USAGE RESTRICTION set-working-mind! must never been called without
;; control over the pstore gatekeeper thread.  We need a priority lock
;; when changing the global variables, which is only guaranteed, when
;; gatekeeper holds back all low priority threads.

;; TODO find a way to factor the update of public/private-oid/context
;; out.

(define (set-working-mind!/lolevel ps default-read default-write)
  (set! mind-waiting-readers (assq 'waiting-readers ps))
  (set! mind-pool (cdr (assq 'use-pool ps))))

;;** The "is (assumed to be) synchrone" attribute.

(define (make-resync-cache-slow)
  (make-cache "*resync-cache*"
	      eq?
	      #f ;; state
	      ;; miss:
	      (lambda (cs k) (cons k #f))
	      ;; hit
	      #f ;; (lambda (c es) #t)
	      ;; fulfil
	      (lambda (c es results)
		(set-cdr! es (if (and (pair? results) (aggregate? (car results))) 
				 #f
				 (add-duration
				  *system-time*
				  (make-time
				   'time-duration 0
				   (or (respond-timeout-interval) 20)))))
		(values))
	      ;; valid?
	      (lambda (es)
		(or (not (cdr es))
		    (srfi19:time<=? *system-time* (cdr es))))
	      ;; delete
	      ;; #f ;; (lambda (cs es) #f)
	      (lambda (cs es) (logagree "Q SyncDel ~a\n" es))
	      ))

(define (make-resync-cache-nervous)
  (make-cache "*resync-cache*"
	      eq?
	      #f ;; state
	      ;; miss:
	      (lambda (cs k) (cons k #t))
	      ;; hit
	      #f ;; (lambda (c es) #t)
	      ;; fulfil
	      (lambda (c es results)
		(set-cdr! es (and (pair? results) (aggregate? (car results))))
		(values))
	      ;; valid?
	      (lambda (es) (cdr es))
	      ;; delete
	      #f ;; (lambda (cs es) #f)
	      ))

(define *resync-cache* (make-resync-cache-nervous))

(begin
  (define (cleanup)
    (cache-cleanup! *resync-cache*)
    (register-timeout-message! 600 cleanup)))

;; (define (set-missing-oid! oid)
;;   (cache-set! *resync-cache* oid raise (make-object-not-available-condition oid #f)))

(: set-missing-oid! (:oid: -> false))
(define (set-missing-oid! oid)
  ;; (cache-set! *resync-cache* oid values #f)
  #f)

(: is-synced? (:oid: --> boolean : :oid:))
(define (is-synced? oid)
  (cache-ref/default *resync-cache* oid #f #f))


;; KLUDGE: this piece forwards "seen" messages eventually to the
;; persistent data.  This SHOULD be changed into an inteface
;; abstraction over such bookkeeping.  It just doesn't belong here and
;; accounts for obvious code bloat'.

(define (default-delete-is-sync-flag! oid)
  (cache-invalid!
   *resync-cache*
   oid
   (let ((frame (find-frame-by-id/asynchroneous oid)))
     (lambda ()
       (%find-frame-sync!
	oid frame
	(if (aggregate? frame) (replicates-of frame) *local-quorum*))))))

(define *delete-is-sync-hook* default-delete-is-sync-flag!)

(define (delete-is-sync-flag! oid) (delete-is-sync-hook oid))

(define (delete-is-sync-hook . new)
  (let ((old *delete-is-sync-hook*))
    (if (pair? new) (set! *delete-is-sync-hook* (car new)))
    old))

(define (pool-reset!)
  (logagree "Q Sync cache reset.\n")
  (cache-fold
   *resync-cache*
   (lambda (k v i) (cache-invalid! *resync-cache* k))
   '()))

(define (pool-fold! lvl kons init)
  (cache-fold
   (case lvl
     ((1) mind-pool)
     ((2) *resync-cache*)
     (else (error (format "pool-fold! illegal pool argument ~a" lvl))))
   kons init))

;;* Self verifying / content defined addresses

(define make-well-known-symbol
  (let ((n -1)
	(mux (make-mutex 'well-known-symbol)))
    (lambda args
      (define (make-well-known-symbol run)
	(let ((r (with-mutex mux (set! n (add1 n)) n)))
	  (let ((s (number->string n 16)))
	    (string-append (make-string (fx- 32 (string-length s)) #\0)  s))))
      make-well-known-symbol)))

(define (frame-deed-data frame)
  (call-with-output-string
   (lambda (port)
     (for-each (lambda (slot)
		 (case slot
		   ((mind-body)
		    (display
		     (let ((b (get-slot frame 'mind-body)))
		       (if (a:blob? b)
			   (blob-sha256 b)
			   (and-let* ((b (message-body/plain frame))) (sha256-digest b))))
		     port))
		   ((mind-links)
		    (display
		     (xml-format (table->rdf-bag-sorted
				  (get-slot frame 'mind-links)))
		     port))
		   ((dc-date)
		    (display (rfc-822-timestring (get-slot frame slot)) port))
		   ((replicates)
		    (and-let*
		     ((replicates (get-slot frame slot))
		      ((fx> (quorum-size replicates) 0)))
		     (display (xml-format (quorum-xml replicates)) port)))
		   (else (display (get-slot frame slot) port))))
	       deed-slots))))

(define pool-hash-function md5-digest)

(define (make-self-verifying-symbol-suggestion frame)
  (let ((pre (frame-deed-data frame)))
    (define (make-self-verifying-symbol-suggestion run)
      (pool-hash-function
       (if (eqv? run 0)
	   pre
	   (string-append pre (number->string run)))))
    make-self-verifying-symbol-suggestion))

(define $use-well-known-symbols (make-shared-parameter #t))

(: make-symbol-suggestion (:message: --> (procedure (number) string)))
(define (make-symbol-suggestion frame)
  (if ($use-well-known-symbols)
      (make-well-known-symbol)
      (make-self-verifying-symbol-suggestion frame)))

;; DFN place - an aggregate with a unique entity identifier

;(define-macro (%make-oid str) `(make <oid> string-data: ,str))
(cond-expand
 (chicken
  (define-inline (%make-oid str) (string->symbol (string-append "A" str))))
 (else
  (define-macro (%make-oid str) `(string->symbol (string-append "A" ,str)))))


(define (make-oid find-frame suggest)
  (let loop ((n 0))
    (let ((p (%make-oid (suggest n))))
      (if (find-frame p 'make-oid)
          (begin
            (logerr "Make-oid: clash ~a\n" p)
            (loop (add1 n)))
          p))))

(: frame-oid-match? (:message: :oid: -> *))
(define (frame-oid-match? frame given)
  (let ((suggest (make-symbol-suggestion frame)))
    (let loop ((n 0))
      (let ((p (%make-oid (suggest n))))
	(cond
	 ((equal? p given) n)
	 ((fx< n 100) (loop (add1 n)))
	 (else #f))))))

(define $enforce-frame-oid-match (make-shared-parameter #t)) ;; still #f for 'stable' branch

(: frame-oid-must-match? (:message: :oid: -> *))
(define (frame-oid-must-match? frame given) ;; badly named
  (if (and ($enforce-frame-oid-match)
	   (not (eq? given one-oid)) (not (eq? given null-oid)))
      (let ((version (get-slot frame 'version)))
	(or (fx> (version-serial version) 0)
	    (and-let*
	     ((look-alike (aggregate frame given))
	      (c (get-slot frame 'mind-action-document))
	      (q (replicates-of look-alike)))
	     (not (public-equivalent? c q)))
	    (frame-oid-match? frame given)))
      #t))

(define (make-place! . args)
  (let* ((frame (make-frame args))
	 (id (make-oid find-local-frame-by-id (make-symbol-suggestion frame)))
	 (p (aggregate frame id)))
    (bind-place! id p #f)))

(define (make-place-for-id! id . args)
  (let* ((frame (make-frame args))
	 (p (aggregate frame id)))
    (bind-place! id p #f)))

(define (find-cached-frame/default id dflt)
  (cache-ref/default mind-pool id #f dflt))

(: find-already-loaded-frame-by-id (:oid: -> :place-or-not:))
(define (find-already-loaded-frame-by-id id) (find-cached-frame/default id #f))

;; DFN message - a frame without identifier, typically conceptually in
;; read only mode.
;;
;; We say conceptually because implementations might modify it sometimes
;; to cache computations, but if this is not transparent (except for
;; log messages or delays), than it's a bug.

(: load-frame/local (:oid: symbol -> :place-or-not:))
(define (load-frame/local id source)
  (guard
   (ex (else (log-condition (cons source id) ex) #f))
   (let ((frame (load-frame *the-registered-stores* id)))
     (if (or (not frame) (frame-oid-must-match? (aggregate-meta frame) id))
	 frame
	 (error "loading local frame: mismatch between frame and oid")))))

;; (define (load-frame/remote-default id)
;;   (load-frame *the-remote-stores* id))

;; (define (load-frame/local+remote-default id source)
;;   (or (guard
;;        (ex (else (log-condition (cons source id) ex) #f))
;;        (load-frame/local id))
;;       (load-frame/remote-default id)))

(define sync-on-load? #f)

(define (sync-on-load . how)
  (if (and (pair? how)) (set! sync-on-load? (car how)))
  sync-on-load?)

(: find-local-frame-by-id (:oid: symbol -> :place-or-not:))
(define (find-local-frame-by-id id call-source)
  (cache-ref
   mind-pool id
   (lambda ()
     (load-frame/local id call-source))))

(: %%find-frame-sync! :%%find-frame-sync!:)
(define %%find-frame-sync!
  (lambda (oid local-frame context) #f))

(: set-resync-completion! (:%%find-frame-sync!: -> undefined))
(define (set-resync-completion! proc)
  (set! %%find-frame-sync! proc))

(: %find-frame-sync! :%%find-frame-sync!:)
(define (%find-frame-sync! oid local-frame context)
  (if (aggregate? local-frame)
      (let ((action (fget local-frame 'mind-action-document)))
	(cond
	 ((not action) (find-local-frame-by-id oid '%find-frame-sync!))
	 ((public-equivalent? action context) local-frame)
	 (else (or (%%find-frame-sync! oid local-frame context)
		   (resync-now! oid (replicates-of local-frame))
		   (resync-now! oid ((quorum-lookup) oid context))))))
      (or (%%find-frame-sync! oid local-frame context)
	  (resync-now! oid ((quorum-lookup) oid context)))))

(define (find-frame-by-id/asynchroneous+quorum oid context)
  (let ((local (find-local-frame-by-id oid 'find-frame-by-id/asynchroneous+quorum)))
    (if local
	(or (cache-ref/default
	     *resync-cache* oid
	     (lambda () (%find-frame-sync! oid local context)) local)
	    local)
	(cache-ref *resync-cache* oid
		   (lambda () (%find-frame-sync! oid #f context))))))

(define (find-frame-by-id/asynchroneous oid)
  (find-frame-by-id/asynchroneous+quorum oid *local-quorum*))

(: find-frame-by-id/synchronised+quorum
   (symbol (struct <quorum>) -> :place-or-not:))
(define (find-frame-by-id/synchronised+quorum oid context)
  (cache-ref
   *resync-cache* oid
   (lambda ()
     (%find-frame-sync! oid (find-local-frame-by-id oid 'find-frame-by-id/synchronised+quorum) context))))

(: find-frame-by-id/resynchronised+quorum (:oid: :quorum: -> :place-or-not:))
(define (find-frame-by-id/resynchronised+quorum oid context)
  (cache-reref
   *resync-cache* oid
   (lambda ()
     (%find-frame-sync! oid (find-local-frame-by-id oid 'find-frame-by-id/resynchronised+quorum) context))))

(define find-frame-by-id-on find-frame-by-id/synchronised+quorum)

(: find-frame-by-id/quorum (:oid: :quorum: -> :place-or-not:))
(define (find-frame-by-id/quorum oid context)
  ((cond
    ((eq? sync-on-load? #f) find-frame-by-id/asynchroneous+quorum)
    (else find-frame-by-id/synchronised+quorum))
   oid context))

(: find-frames-by-id (:oid: -> :place-or-not:))
(define (find-frames-by-id id)
  (define frame (begin (assert (oid? id))
		       (find-frame-by-id/asynchroneous id)))
  (define (synced)
    (%find-frame-sync! id frame
		       (if (aggregate? frame) (replicates-of frame) *local-quorum*)))

  (or (if sync-on-load?
	  (cache-ref *resync-cache* id synced)
	  (cache-ref/default *resync-cache* id synced frame))
      frame))

;;* Reload from remote sites.
;; Watching the agreement process can be useful in understanding
;; network setups.  Hence we can selectively swith it on.

(: hosts-lookup-at-least ((list-of (or string :oid:)) fixnum -> list))
(define (hosts-lookup-at-least hosts n)
  (let loop ((hosts hosts) (n n) (missing '()))
    (if (null? hosts)
	(let loop ((hosts missing) (n n) (missing '()))
	  (if (null? hosts)
	      (if (fx>= n 1)
		  (raise-service-unavailable-condition
		   (call-with-output-string
		    (lambda (p) (for-each (lambda (h)
					    (display h p)
					    (display #\space p))
					  missing))))
		  '())
	      (let ((h ((host-lookup) (car hosts))))
		(if h
		    (cons h (loop (cdr hosts) (sub1 n) missing))
		    (loop (cdr hosts) n (cons (car hosts) missing))))))
	(let ((h ((host-lookup) (car hosts) 'nowait)))
	  (if (and h ((host-alive?) h))
	      (cons h (loop (cdr hosts) (sub1 n) missing))
	      (loop (cdr hosts) n (cons (car hosts) missing)))))))

(define host-alive? (make-shared-parameter (lambda (name) #t)))

(define host-maybe-alive? (make-shared-parameter (lambda (name) #t)))

(define quorum-lookup
  (make-shared-parameter
   (lambda (oid . initial-quorum)
     (error "quorum-lookup: protocol not connected"))))

(define metainfo-remote
  (make-shared-parameter
   (lambda (quorum id)
     (error "metainfo-remote: protocol not connected"))))

(define privilege-info-remote
  (make-shared-parameter
   (lambda (source quorum local target)
     (error "privilege-info-remote: protocol not connected"))))

(define resynchronize
  (make-shared-parameter
   (lambda (quorum id)
     (error "resynchronize: protocol not connected"))))

(define (quorum-get! initial-quorum oid)
  ;; precondition
  (hang-on-mutex! 'quorum-get! (list (respond!-mutex oid)))
  (logagree "Q quorum-get! ~a\n" oid)
;;  (retain-mutex (respond!-mutex oid)
;;		((resynchronize) ((quorum-lookup) oid initial-quorum) oid))

  (resync-now! oid initial-quorum))

(define invite-new-supporters!
  (make-shared-parameter
   (lambda (oid old-quorum new-quorum)
     (error "invite-new-supporters!: protocol not connected"))))

;; FIXME: see XPath whether the prototype of "document" is correct.
(define (document nm . context)
  (if (null? context) (find-frames-by-id nm)
      (find-frame-by-id/quorum nm (if (quorum? (car context)) (car context)
				       (make-quorum context)))))

;;***** BLOB

(define message-body-fetch-blob-function
  (make-shared-parameter
   (lambda (i b f) (error "message-body-fetch-blob-function: protocol not connected"))))

(: want-links-local? (:place: --> boolean))
(define (want-links-local? frame)
  (let ((quorum (replicates-of frame)))
    (or (quorum-local? quorum)
	(and-let* ((action (fget frame 'mind-action-document)))
		  (public-equivalent? action quorum)))))

(define (want-body-local? frame)
  (want-links-local? frame))

(define (raise-blob-missing-condition oid target quorum)
  (raise (make-condition
	  &blob-missing-condition
	  'status 404
	  'message
	  (if #f
	      (format "blob ~a missing in ~a from ~a"
		      (blob-sha256 target) oid (quorum-others quorum))
	      (call-with-output-string
	       (lambda (port)
		 (format port "blob ~a missing in ~a from "
			 (blob-sha256 target) oid)
		 (for-each
		  (let ((alive? (host-alive?)))
		    (lambda (host)
		      (let ((peer (host-lookup* host)))
			(display host port)
			(if (not (alive? peer)) (display " (dead)" port))
			(display #\space port))))
		   (quorum-others quorum)))))
	  'source oid)))

(define (aggregate-fetch-blob-in-quorum1 quorum oid action target)
  ;; Maybe we better ordered the 'ol' list by peers speed.
  (let loop ((ol (quorum-others quorum))
	     (n (if (quorum-local? quorum)
		    (sub1 (quorum-size quorum))
		    (quorum-size quorum))))
    (if (eqv? n 0)
	(raise-blob-missing-condition oid target quorum)
	(let ((nx (list-ref ol (random n))))
	  (guard
	   (ex (else
		(let loop ((ex (if (uncaught-exception? ex)
				   (uncaught-exception-reason ex)
				   ex)))
		  (cond
		   ((timeout-condition? ex)
		    (logagree "Q ~a fetch-blob timeout on ~a for ~a\n"
			      oid nx (blob-sha256 target)))
		   ((missing-blob-condition? ex)
		    (logagree "Q ~a blob missing on ~a for ~a\n"
			      oid nx (blob-sha256 target)))
		   (else (log-condition 'aggregate-fetch-blob ex))))
		(loop (remove (lambda (x) (eq? x nx)) ol) (sub1 n))))
	   (or (and-let*
		((peer (host-lookup* nx))
		 (((host-alive?) peer)))
		(!apply
		 (message-body-fetch-blob-function)
		 (list oid target peer)))
	       (loop (remove (lambda (x) (eq? x nx)) ol) (sub1 n))))))))

(define (make-get-blob-cache name cache-duration)
  (make-cache name
	      eq?
	      #f ;; state
	      ;; miss:
	      (lambda (cs k) (cons k #f))
	      ;; hit
	      #f ;; (lambda (c es) (logerr "cache hit on *get-blob-cache* ~a\n" (car es)))
	      ;; fulfil (optional)
	      (lambda (c es results)
		(set-cdr! es (add-duration *system-time* cache-duration))
		(values))
	      ;; valid?
	      (lambda (es)
		(or (not (cdr es))
		    (srfi19:time>=? (cdr es) *system-time*)))
	      ;; delete
	      #f
	      ))

(define *get-blob-cache*
  (make-get-blob-cache "*get-blob-cache*" (make-time 'time-duration 0 10)))

(begin
  (define (cleanup)
    (cache-cleanup! *get-blob-cache*)
    (register-timeout-message! 10 cleanup))
  (register-timeout-message! 10 cleanup))

(: aggregate-fetch-blob-in-quorum (:quorum: :oid: :oid: :blob: -> boolean))
(define (aggregate-fetch-blob-in-quorum quorum oid action target)
  (if (and (a:blob? target)
	   (or (quorum-local? quorum)
	       (eq? action oid)
	       (public-equivalent? action quorum))
	   (not (blob-notation-size *the-registered-stores* target #f)))
      (cond
       ((null? (quorum-others quorum))
	(raise (make-object-not-available-condition
		(blob-sha256 target) oid 'aggregate-fetch-blob-in-quorum)))
       (else
	(cache-ref
	 (cache-ref
	  *get-blob-cache*
	  oid
	  (lambda () (make-get-blob-cache
		      (format "*get-blob-cache* ~a" (blob-sha256 target))
		      (make-time 'time-duration 0 10))))
	 (blob-sha256 target)
	 (lambda () (aggregate-fetch-blob-in-quorum1 quorum oid action target)))))
      target))

(set-aggregate-fetch-blob! aggregate-fetch-blob-in-quorum)

(define *public-equivalent*
  (make-cache "*public-equivalent*"
	      eq?
	      #f ;; state
	      ;; miss:
	      (lambda (cs k) #t)
	      ;; hit
	      #f ;; (lambda (c es) #t)
	      ;; fulfil
	      #f
	      ;; valid?
	      (lambda (es) #t)
	      ;; delete
	      #f ;; (lambda (cs es) #f)
	      ))

(define (strict-public-equivalent? oid)
  ;; BEWARE: We used to use only local frames here now, because this
  ;; function is called from some optimization hook to bypass
  ;; resynchronisation overhead for static document.  However this
  ;; might broke too much.
  (let ((other (find-local-frame-by-id oid 'strict-public-equivalent?)))
    (if other
	(node-list=?
	 (guard
	  (ex ;; ((enable-warnings) (log-condition oid ex) #f)
	      (else (logagree "Q ~a public equiv test ~a\n" oid (condition->string ex)) #f))
	  (aggregate-body other))
	 (aggregate-body (or (find-local-frame-by-id (public-oid) 'strict-public-equivalent?)
			     (raise (make-object-not-available-condition
				     (public-oid) (public-oid) 'strict-public-equivalent?)))))
	'unknown)))

(define rough-public-equivalent?
  (let ((select (sxpath '(method programlisting))))
    (lambda (oid)
      (let ((other (find-local-frame-by-id oid 'rough-public-equivalent?)))
	(and other
	     (equal? (guard
		      (ex ;; ((enable-warnings) (log-condition oid ex) #f)
			  (else (logagree "Q ~a public equiv test ~a\n" oid (condition->string ex)) #f))
		      (call-with-input-string
		       (data (select (aggregate-body other)))
		       read))
		     (call-with-input-string
		      (data (select
			     (aggregate-body
			      (or (find-local-frame-by-id (public-oid) 'strict-public-equivalent?)
				  (raise (make-object-not-available-condition
					  (public-oid) (public-oid) 'strict-public-equivalent?))))))
		      read)))))))

(define internal-public-equivalent? rough-public-equivalent?)

;; On the purpose of comparision: find equivalents, which are "equal
;; enough" for the purpose.
;; http://www.schneier.com/blog/archives/2005/06/torah_security.html

(define (public-equivalent? oid . context)
  (and (public-oid)
       (or (eq? oid (public-oid))
	   (eq? oid one-oid)
	   (let ((x (cache-ref/default *public-equivalent* oid #f 'NotFound)))
	     (if (eq? x 'NotFound)
		 (and
		  (or (find-local-frame-by-id oid 'rough-public-equivalent?)
		      (and-let* (((pair? context))
				 ;; (seen ((host-lookup) oid)) ;; Don't, it waits.
				 (context (car context)))
				(resync-now! oid context)))
		  (cache-ref *public-equivalent* oid (lambda () (internal-public-equivalent? oid))))
		 x)))))

;; Change the name space.

(define (make-context id)
  (let ((user-frame (if id (find-frame-by-id/asynchroneous id) #f)))
    (lambda (name)
      (if user-frame
	  (frame-resolve-link (aggregate-meta user-frame) name #f)
          #f))))

;; Look here, this is one of the most interesting things.

(define (public-context) (make-context (my-oid)))

;; CAUTION
;; Never mimic what this function does!  The implementation looks too
;; strange and will probably change.  The accessor here is official.
(define (user-context frame request)
  (let ((dc-creator (request 'dc-creator))
	(id (if (aggregate? frame) (aggregate-entity frame) (frame 'get 'id))))
    (if (eq? id dc-creator)
	(if (aggregate? frame)
	    (let ((snapshot (aggregate-meta frame)))
	      (lambda (name) (frame-resolve-link snapshot name #f)))
	    (lambda (name) (and (string? name) (frame name))))
	(make-context dc-creator))))

(: public-capabilities (-> (list-of (list-of :oid:))))
(define (public-capabilities)
  (if (public-oid)
      (get-slot (aggregate-meta (find-local-frame-by-id (public-oid) 'public-capabilities)) 'capabilities)
      '()))

(: quorum-members-to-inform
   (:quorum: (or false (pair number *)) :quorum: fixnum
    -> (list-of :ip-addr:)))
(define (quorum-members-to-inform from-quorum seed to-quorum rc)
  (let ((here (if (equal? ((host-lookup) (public-oid)) (local-id))
		  (public-oid) (local-id)))
	(sources (map
		  (lambda (li)
		    (let ((x (attribute-string 'resource li)))
		      (or (string->oid x) x)))
		  ((sxpath '(Bag li)) (quorum-xml from-quorum)))))
;;     (if (null? sources)
;; 	(raise (make-condition
;; 		&http-effective-condition
;; 		'message
;; 		(format "Can't locate quorum ~a" (quorum-others from-quorum))
;; 		'status 417)))
    (let loop ((result '())
	       (to (quorum-others to-quorum))
	       (from (drop sources (modulo
				    (or (and seed (version-serial seed))
					0)
				    (quorum-size from-quorum))))
	       (r rc))
      (if (null? to)
	  result
	  (if (member (car to) (quorum-others from-quorum))
	      (loop result (cdr to) from rc)
	      (let ((rest (let ((rest (cdr from))) (if (null? rest) sources rest))))
		(if (equal? (car from) here)
		    (let ((h ((host-lookup) (car to) 'nowait)))
		      (if h
			  (loop (cons h result) (cdr to) rest rc)
			  (loop result (cdr to) rest rc)))
		    (let ((f ((host-lookup) (car from) 'nowait)))
		      (if (and f ((host-maybe-alive?) f))
			(if (fx>= r 1)
			    (loop result to rest (sub1 rc))
			    (loop result (cdr to) rest rc))
			(loop result to rest rc))))))))))

;; we need some sort of an administrator, a principal having write access
;; on (my-oid)s links to create new entry points.
;; can we store this data in the repository?
(define $administrator "root@localhost")

(define (administrator) $administrator)

(define (set-administrator-email! v)
  (if (string? v) (set! $administrator v)))

(define administrator-password-hash (the * #f))

(define (check-administrator-password word)
  (secret-verify administrator-password-hash word))

(define (set-administrator-password! word digest)
  (set! administrator-password-hash
	(and (not (string=? word ""))
	     (if digest (secret-encode word (md5-digest (literal (current-date)))) word))))

;; Dump a message (at a new, fresh) place and return it's describing
;; entity.

;; A rather dangerous option turning everything into an evil world.
;; You really don't want to run that in an production environment,
;; except for electronic suizid.
(define $insecure-mode #f)

(: dump-message! ((or false :oid:) :message: -> :oid:))
(define (dump-message! from message)

  (define pubp (public-oid))
  (define quorum (or (get-slot message 'replicates)
		     *local-quorum*))
  (define (find-frame oid)
    (if (eq? oid from)
	(find-already-loaded-frame-by-id oid)
	(find-frame-by-id/quorum oid quorum)))


  ;; You never know who will interact with the place.  But this is
  ;; dangerous for the other party, if they don't know what the place
  ;; will do.  Hence they need read access to the action-document,
  ;; otherwise they simply can't trust enough to attempt interaction.
  ;; We don't need more dangerous garbage, we are already armed,
  ;; thankyouverymuch.

  (let ((what-it-would-do (get-slot message 'mind-action-document)))
    ;; During setup - when there is no (public-oid) we skip the check.
    (if (and what-it-would-do pubp)
        (let ((what-it-would-do-place
	       (or (find-frame what-it-would-do)
		   (make-object-not-available-condition
		    what-it-would-do quorum "new code"))))
          (if (not (public-equivalent?
                    (fget what-it-would-do-place 'mind-action-document)
		    quorum))
	      (raise
	       (format "Behavioral documents ~a need a public place as root.
Otherwise system security would break.
The specified place ~a is not equivalent."
		       what-it-would-do (fget what-it-would-do-place 'mind-action-document)))))
        (if (not what-it-would-do)
	    (set-slot! message 'mind-action-document
		       (if $insecure-mode
			   (or (my-oid) null-oid)
			   (or pubp one-oid))))))
                                        ; BTW this could be #f see below.

  ;; Ouch, now you might ask if I'm sure that (public-oid) should be
  ;; the secure mode.  It's looks weird, but Applications should
  ;; handle private data, here we are concerned with interaction.
  ;; Look at yourself: do you open a secret with "don't tell anybody
  ;; what I'm going to say" or could you imagine to open the
  ;; conversation with with the person who block an entrance with
  ;; these words?  No.  To build trustworthy system we need to default
  ;; to the public place.

  ;; Within an embryonal system, we would still not have something in
  ;; the mind-action-document slot.  We need to handle that case
  ;; together with the fundamental rights of any thing.

  (let ((nobj (apply make-place!
		     (map (lambda (slot)
			    (property slot (get-slot message slot)))
			  stored-slots))))

    ;; If we've got a secret, we'll create an entry point.

    (if (fget nobj 'secret)
        (let ((own-right (list (aggregate-entity nobj))))
	  ;; Now we do something strange: it is actually not correct,
	  ;; to select any kind of protection now.  It appears to me
	  ;; that new members must choose their protection themself.
	  ;; Until they have understood how to do so, they depend on
	  ;; the mercy of their parents.
	  ;;
	  ;; Nevertheless, defaulting to "self possession" seems ok.
          (if (unprotected? (fget nobj 'protection))
              (fset! nobj 'protection own-right))
          (fset! nobj 'capabilities
                 (if pubp
                     (cons own-right
                           (fget (find-frames-by-id pubp) 'capabilities))
                     (list own-right)))

          ;; Ok, we are embryonal.  We accept any part, which can act
          ;; out of itself.  We express that by filling in the
          ;; mind-action-document with the value of it's own
          ;; descriptive entity.

          (if (not (fget nobj 'mind-action-document))
              (fset! nobj 'mind-action-document (aggregate-entity nobj)))
          (logit display "Created Laune.\n"))
        (begin	  
	  ;; Capabilities on newly created objects

	  ;; The case of discretionary capabilies (only).

	  ;; (Discretionary: the capabilities are attached to the
	  ;; message.  In contrast to mandatory, when
	  ;; capabilities are a property attached to the (direct)
	  ;; sender(place).  While the former express the just
	  ;; intention of the creator of the message, the latter
	  ;; assure that there's no man-in-the-middle, who can fork of
	  ;; apparently resulting messages from messages they got.)

          ;; Until version 0.6.12 there where only discretionary
          ;; capabilities *and* those where just stored when a message
          ;; was dumped into a new place. But considering any
          ;; malicious place dumping an arbitrary message.  The action
          ;; of the resulting place could adopt messages in the name
          ;; and with the capabilities of the sender of the arbitrary
          ;; message dumped in the first place.  To mitigate the
          ;; effect, we had no capabilities by default.  They had to
          ;; be granted to the already existing object.
	  
          ;; (fset! nobj 'capabilities #f)

	  ;; During the long phase of incremental improvements until
	  ;; version 0.8.2, we started to experiment with
	  ;; mandatory capabilities.  They are horribly
	  ;; expensive, since the force us to filter with the
	  ;; capabilities attached to the sending place.  Nevertheless,
	  ;; the provide for pishing protection without prior code
	  ;; analysis of newly contacted objects.

	  ;; However descritionary capabilities must be filtered
	  ;; before messages are dumped.  Therefore the removal is
	  ;; left as a comment here.  See 'make-sender' for related
	  ;; actions.

	  (let ((max (or (and-let* ((creator (get-slot message 'dc-creator))
				    (frame (find-frame creator)))
				   (fget frame 'capabilities))
			 '())))
	    (fset! nobj 'capabilities (filter (lambda (c) (contains-strictly? c max))
					      (get-slot message 'capabilities))))

	  ))
    
    (commit-frame! #f nobj (aggregate-meta nobj) '())

    (aggregate-entity nobj)))

(define signature-att-decl
  (list (make-xml-attribute 'rdf 'xmlns namespace-rdf-str)
        (make-xml-attribute 'dc  'xmlns namespace-dc-str)
        (make-xml-attribute 'a   'xmlns namespace-mind-str)
        (make-xml-attribute 's   'xmlns namespace-sync-str)
        (make-xml-attribute 'dav   'xmlns namespace-dav-str) ; ??? better extra
        (make-xml-attribute 'space namespace-xml "default")))

(define rdf-parsetype-literal-attribute
  (make-xml-attribute 'parseType namespace-rdf "Literal"))

(define rdf-parsetype-literal-attribute-list
  (list rdf-parsetype-literal-attribute))

(define frame-metadata-links-attlist
  (list (make-xml-attribute 'id #f "links")))

(define (make-rdf-li-for-link n v)
  (if (oid? v)
      (make-xml-element
       'li namespace-rdf
       (list (make-xml-attribute 'resource namespace-rdf n)
	     (make-xml-attribute
	      'href namespace-mind (oid->string v)))
       (empty-node-list))
      (make-xml-element
       'li namespace-rdf
       (cons (make-xml-attribute 'resource namespace-rdf n)
	     rdf-parsetype-literal-attribute-list)
       v)))

(define (table->rdf-bag table mangle)
  (make-xml-element
   'links namespace-mind '()
   (make-xml-element
    'Bag namespace-rdf frame-metadata-links-attlist
    (fold-links
     (lambda (c v init)
       (%node-list-cons (mangle c v) init))
     (empty-node-list)
     table))))

(define (table->rdf-bag-sorted table)
  (if table
      (make-xml-element
       'links namespace-mind '()
       (make-xml-element
	'Bag namespace-rdf frame-metadata-links-attlist
	(fold-links-sorted
	 (lambda (k v i) (%node-list-cons (make-rdf-li-for-link k v) i))
	 (empty-node-list)
	 table)))
      (empty-node-list)))
