;;* Recursion on Places

;; somehow we can do it...

(define (fetch-other* place result-accessor dst args)
  (let* ((name (if (pair? dst) (car dst) dst))
         (n (cond ((oid? name) name)
		  ((string? name) (string->oid name))
		  ((eq? (gi name) 'id) (string->oid (data (children name))))
		  (else "error invalid destination in fetch-other ~a" dst)))
         (oid0 (or n (place name)))
         (oid (or oid0 ((public-context) name))))
    (if oid
        (let loop ((n 0)
		   (dst (if (pair? dst) (cdr dst) '())))
	  (let ((d (apply
		    (place 'get 'reader)
		    to: (cons oid dst)
		    type: 'call
		    'location (list name)
		    args)))
	    (if (and (pair? d) (car d)) ; found some results?
		(let ((d0 (car d)))
		  (let ((ansloc (get-slot d0 'location)))
		    (if #t ;; (equal? (debug 'dst dst) (debug 'ansloc ansloc))
			(result-accessor d0)
			(if (eqv? n 0)
			    (loop 1 ansloc)
			    (raise (make-object-not-available-condition (format "too many re-directs to ~a" name) (place 'get 'id)))))))
		(raise (make-object-not-available-condition name (place 'get 'id))))))
        (raise (make-object-not-available-condition name (place 'get 'id))))))

(define (fetch-other place arg1 . args)
  (if (procedure? arg1)
      (fetch-other* place arg1 (car args) (cdr args))
      (fetch-other* place message-body arg1 args)))

;;**** Basic behavior

;; These are only the most basic process steps, which can be performed
;; at a place.  TODO They should go into "step.scm" but are needed as
;; default actions.  Hence we must have them in module [place].

;; We have a special xml document syntax: if we read this as write
;; input, we interpret contained extension elements.  The following
;; predicate tests for this input syntax.

(define is-meta-form-namespaces `(,namespace-mind ,namespace-mind-v0 mind))
(define (is-meta-form? request)
  ;; We could get 'nl' directly via message-body, which appears to be
  ;; there for this reason.  However we don't want to cast binaries to
  ;; weird xml containers, just to find ignore them again.
  (and-let* ((nl (or (request 'cached:body/parsed-xml)
		     (and (xml-parseable? (request 'content-type))
			  (request 'body/parsed-xml))))
	     (n (document-element nl)))
	    (and (memq (ns n) is-meta-form-namespaces)
		 (not (eq? (gi n) 'output)))))

(define read-attribute-list
  (list (make-xml-attribute 'type namespace-mind "read")))
(define write-attribute-list
  (list (make-xml-attribute 'type namespace-mind "write")))

;;***** meta data retrieval (and debug level control)

;; The "action-document" the constituting contract (in computer
;; science the objects class) which founds and governs the existence
;; of the object.  This information MUST NOT be denied, since the
;; (adult) user is responsible to abstain from interaction, if
;; it would incure unintented consequences.

;; On the contrary: to my understanding there is NO way to create a
;; legal system where the inhabitants don't understand what a legal
;; system is or can't figure which rules it follows (like children,
;; mental disabilities, contracts relating to third parties etc.).

(define (action-document oid)
  (fget (or (find-frames-by-id oid)
	    (raise (make-object-not-available-condition
		    oid #f 'action-document)))
	'mind-action-document))

;; this is to be exported to user space
(define (secure-action-document oid)
  (if $insecure-mode
      (error "insecure mode, type identity hidden")
      (action-document oid)))

(define (message-protection obj)
  (or (cond
       ((procedure? obj) (obj 'protection))
       ((frame? obj) (get-slot obj 'protection)))
      '()))

(define (message-capabilities obj)
  (or (cond
       ((procedure? obj) (obj 'capabilities))
       ((frame? obj) (get-slot obj 'capabilities)))
      '()))

(define (message-content-type obj)
  (cond
   ((procedure? obj) (obj 'content-type))
   ((frame? obj) (get-slot obj 'content-type))
   (else (error "message-content-type invalid parameter ~a" obj))))

;; The setup process is the only situation, when we need to give the
;; following reply.  If we would never give a reply to requests, which
;; ar "meta" and "write", we could spare this oddity.  Is this desirable?

(define metainterface-not-available-response
  (make-xml-element
   'info namespace-mind
   (empty-node-list)
   (node-list (make-xml-literal "meta interface not found"))))

(define metainfo-request-element
  (make-xml-element
   'get namespace-mind
   (list (make-xml-attribute 'api 'xmlns namespace-mind-str))
   (empty-node-list)))

(define public-read-metainfo-request
  (delay
    (make-message
     (property 'capabilities
	       (get-slot (aggregate-meta (find-local-frame-by-id (public-oid) 'public-read-metainfo-request))
			 'capabilities))
     (property 'accept application/rdf+xml)
     (property 'content-type text/xml)
     (property 'body/parsed-xml metainfo-request-element)
     (property 'location '())
     (property 'destination '())
     (property 'location-format 'http-location-format)
     (property 'caller (public-oid))
     (property 'dc-creator (public-oid))
     (property 'dc-date (timestamp)))))

(: metainfo-request? (:message: --> boolean))
(define (metainfo-request? frame)
  (and
   (equal? (get-slot frame 'accept) application/rdf+xml)
   (equal? (get-slot frame 'content-type) text/xml)
   (let ((x (document-element (message-body frame))))
     (and (eq? (gi x) 'get)
	  (memq (ns x) is-meta-form-namespaces)))))

;; TODO: metainfo-hook should be removed, if possible.
(: metainfo-hook
   (:place-accessor: :message-accessor: (or :oid: string) (list-of *)
		     -> :xml-nodelist:))
(define metainfo-hook
  (lambda (place request object keys) (raise 'metainfo-hook:unbound)))
(: set-meta-info!
   ((procedure (:place-accessor: :message-accessor: (or :oid: string) (list-of *))
	       :xml-nodelist:)
    -> undefined))
(define (set-meta-info! proc)
  (set! metainfo-hook proc))

(define (metainfo reader-access message slot . args)
  (metainfo-hook reader-access message slot args))

;; For a legal system to work we can't deny this minimum on
;; information.  (Note that it's still possible to utter anonymous
;; messsages using "public" as creator.)

(define frame-dummy-date (make-date 0 0 0 0 1 1 1 0))

(: default-public-meta-info (:place: -> :xml-element:))
(define (default-public-meta-info obj)
  (make-xml-element
   'RDF namespace-rdf signature-att-decl
   (make-xml-element
    'Description namespace-rdf
    (list (make-xml-attribute 'about namespace-rdf
			      (oid->string (aggregate-entity obj))))
    (node-list

     ;; constituting contract (type)
     (make-xml-element
      'action-document namespace-mind '()
      (literal (oid->string (or (fget obj 'mind-action-document)
				(aggregate-entity obj)))))

     ;; jurisdiction
     (replicates-of/xml obj)

     ;; The (legal) creator of the object.
     ;;
     ;; Note that we might also need to record the place, where the
     ;; object was created: if it was created unintentional to proof
     ;; how it came about.  (The information is available as the
     ;; "caller" slot when the place is created.
     (make-xml-element
      'creator namespace-dc '()
      (literal (oid->string (fget obj 'dc-creator))))

     ;; Date of creation.
     (make-xml-element
      'date namespace-dc '()
      (literal (rfc-822-timestring (or (fget obj 'dc-date)
				       frame-dummy-date))))
     ;; This might reveal too much.  But at least the 'resourcetype'
     ;; property is essential.
     (let ((davprops (fget obj 'dav:properties)))
       (if davprops
	   (make-xml-element 'properties namespace-dav '() davprops)
	   (empty-node-list)))))))

(: intrinsic-sync-locked (:oid: -> boolean))
(define (intrinsic-sync-locked identifier)
  (and-let* ((obj (guard (exception (else #f))
			 (find-local-frame-by-id identifier 'intrinsic-sync-locked))))
	    (let ((q (replicates-of obj)))
	      (and-let* (((or (quorum-local? q) (not (null? (quorum-others q)))))
			 (action (fget obj 'mind-action-document))
			 ((public-equivalent? action q)))
			obj))))
