;; (C) 2005, 2009, 2010 Jörg F. Wittenberger, GPL see http://www.askemos.org

(: message-digest :message-digest:)
(define (message-digest request)
  (let ((b (message-body/plain request)))
	(if b
	    (string-append
	     (srfi19:date->string (get-slot request 'dc-date) "~s.~N:")
	     (xml-digest-2-simple b))
	    (srfi19:date->string (get-slot request 'dc-date) "~s.~N:"))))

;;* ByzantineAgreement

;; Often enough we will receive duplicate messages.  We did filter them
;; out before they take up space and processing power if the compare
;; accoring to:

(define (oid-channel-message-compare a b)
  ;; Note: we know the "a" message has passed this test before, no
  ;; type check.
  (or (not (frame? b))			; so it get's droped as "seen"
      (and (frame? a) ;; or should we trust and skip this check?
	   (equal? (get-slot a 'authorization) (get-slot b 'authorization))
	   (equal? (get-slot a 'content-type) (get-slot b 'content-type))
	   (equal? (get-slot a 'mind-body) (get-slot b 'mind-body)))))

(define (is-sync-message? msg)
  (and (sync-message-data? msg)
       (or (eq? (sync-message-phase msg) 'echo)
	   (eq? (sync-message-phase msg) 'ready))))

(define (legal-channel-message? obj)
  (or (frame? obj) (condition? obj) (timeout-object? obj)))

(set-respond-mutex-guard! legal-channel-message?)

(define (transaction-send-message-version! oid version-number message)
  (if (not (legal-channel-message? message))
      (logerr "WAS JETZT ~s\n" (if (xml-element? message) (xml-format message) message)))
  (assert (legal-channel-message? message))
  (if version-number
      (if (and (frame? message) (is-sync-message? (message-body/plain message)))
	  (advance-transactions (find-local-frame-by-id oid 'transaction-send-message-version!) message 'transaction-send-message-version!)
	  (oid-channel-send-message-version! oid version-number message
					     oid-channel-message-compare)))
  message)

(define (transaction-send-message! oid version-number message)
  (assert (legal-channel-message? message))	;XXX
  (if (string=? (get-slot message 'soapaction) "ReliableBroadcast")
      (guard (ex (else (log-condition 'tsm ex) (raise ex)))
	     (advance-transactions (find-local-frame-by-id oid 'transaction-send-message!) message 'transaction-send-message!))
      (transaction-send-message-version! oid version-number message))
  message)

;;*** Locking

;; Locking granularity is a place.  In absence of further mechanics
;; this is also what a transaction can modify.

(define (remove-transaction! oid version request-id result completed quorum)
  (if (frame? result)
      (set-slot! result 'destination
		 (list (number->string version)
		       (literal (get-slot result 'http-status)))))
  (oid-channel-remove-version!
   oid-channel-entry-ready? transaction-completed?
   oid version request-id result completed quorum))

(define (make-early-timeout on result digest echos uninformed required missing)
  (make-condition
   &agreement-timeout 'ready #f 'result result 'digest digest
   'required-echos echos 'uninformed uninformed
   'required required 'missing missing))

(define (early-timeout-object? obj)
  (and (agreement-timeout? obj) (not (agreement-ready obj))))

(define (make-late-timeout on result digest echos uninformed required missing)
  (make-condition
   &agreement-timeout 'ready #t 'result result 'digest digest
   'required-echos echos 'uninformed uninformed
   'required required 'missing missing))

(define (late-timeout-object? obj)
  (and (agreement-timeout? obj) (agreement-ready obj)))

(define-atomic-type <transaction>
  (%make-transaction oid work ready
		     missing-echos uninformed required missing)
  define-transaction-update transaction?
  (oid transaction-oid)
  (work transaction-work)
  (ready transaction-ready :mutable)
  (missing-echos transaction-missing-echos :mutable)
  (uninformed transaction-uninformed :mutable)
  (required transaction-required :mutable)
  (missing transaction-missing :mutable))

(define (transaction-effects t)
  (receive (e r d) (force (transaction-work t)) e))

(define (transaction-result t)
  (receive (e r d) (force (transaction-work t)) r))

(define (transaction-digest t)
  (receive (e r d) (force (transaction-work t)) d))

(define (%transaction-work-force work)
  (force work (lambda (e) (values e #f #f))))

(define transaction-failed '(failed))

(define transaction-raised-exception '(user-failed))

(define transaction-done '(done))

(define (transaction-failed? t)
  (eq? (transaction-ready t) transaction-failed))

(define (transaction-done? t)
  (eq? (transaction-ready t) transaction-done))

(define (transaction-completed? t)
  (or (transaction-failed? t) (transaction-done? t)))

(define (transaction-ready? t)
  (eq? (transaction-ready t) #t))

(define oid-channel-entry< (make-oid-channel-entry< transaction-ready? message-digest))

(define oid-channel-entry-ready? (oid-channel-entry-pred transaction-ready?))

(define-transaction-update transaction-set-ready
  () (ready) (value) () value)

(define (compute-ready-second1 request-date)
  (let* ((t0 (date->time-utc request-date))
         (t1 (time-second (add-duration
              t0
              (make-time 'time-duration 150000 0) ; ns 2xping time
              )))
         (sys (time-second *system-time*)))
    (if (> sys (+ 1 (time-second t0))) (sub1 sys) sys)))

(define (sync-message? message)
  (and (frame? message) (pair? (get-slot message 'mind-body))))

;; (define-macro (make-ready-message request version)
;;   `(cons* 'ready ,request ,version))
;; (define-macro (make-echo-message request version)
;;   `(cons* 'echo ,request ,version))

;; (define-macro (sync-message-data? obj) `(pair? ,obj))

;; (define-macro (sync-message-phase message) `(car ,message))

;; (define-macro (sync-message-request-chks message) `(cadr ,message))

;; (define-macro (sync-message-serial message) `(caddr ,message))

;; (define-macro (sync-message-container-serial msg)
;;   `(case (sync-message-phase ,msg)
;;      ((echo ready) (caddr ,msg))
;;      ((one-shot) (caddr (cadr ,msg)))
;;      (else (error (format "Unhandled message ~a" ,msg)))))

;; (define-macro (sync-message-container-messages msg) `(cdr ,msg))

;; (define-macro (sync-message-chks message) `(cdddr ,message))

;; (define-macro (sync-message-version message) `(cddr ,message))

;; (define-macro (version-serial v) `(car ,v))

;; (define-macro (version-chks v) `(cdr ,v))

;; (define-macro (current-ready? version message)
;;   `(and (eq? (sync-message-phase ,message) 'ready)
;; 	(eqv? (sync-message-serial ,message)
;; 	      (version-serial ,version))))

;; (define-macro (next-echo? version message)
;;   `(and (eq? (sync-message-phase ,message) 'echo)
;; 	(eqv? (sync-message-serial ,message)
;; 	      (add1 (version-serial ,version)))))

;; (define-macro (confirms-version? version message)
;;   `(equal? (sync-message-chks ,message) (version-chks ,version)))

;; (define-macro (matches-request? chksum message)
;;   `(equal? (sync-message-request-chks ,message) ,chksum))

;; (define-macro (may-match-request? chksum message)
;;   `(or (not (sync-message-request-chks ,message))
;;        (equal? (sync-message-request-chks ,message) ,chksum)))

(define (fold-ready r v i) (cons (make-ready-message r v) i))

(define (fold-echo r v i) (cons (make-echo-message r v) i))

(define (agreement-last-message frame)
  (let ((l (channel-last-messages
	    (aggregate-entity frame)
	    transaction-digest message-digest oid-channel-entry-ready? oid-channel-entry<
	    fold-echo fold-ready '())))
    (cond
     ((null? l) (make-echo-message #f (fget frame 'version)))
     ((pair? (cdr l)) (cons 'one-shot l))
     (else (car l)))))

;; TODO change the module structure so that these few imports from
;; place into protocoll are moved out of place.  Than move protocoll
;; under place.  This will remove the following two "upcalls".  This
;; is also needed for storage/fsm.scm.

(define forward-call
  (make-shared-parameter
   (lambda (result peer frame type message)
     (error "forward-call: protocol not connected"))))

(define replicate-call
  (make-shared-parameter
   (lambda (quorum await frame type message)
     (error "replicate-call: protocol not connected"))))

(define replicate-reply
  (make-shared-parameter
   (lambda (quorum-list oid version message)
     (error "replicate-reply: protocol not connected"))))

(define echo-call
  (make-shared-parameter
   (lambda (voters me request request-digest digest)
     (error "in echo-call: protocol not connected"))))

(define send-ready
  (make-shared-parameter
   (lambda (others me request result request-digest)
     (error "in send-ready: protocol not connected"))))

(define send-one-shot
  (make-shared-parameter
   (lambda (others me request digest result)
     (error "in send-one-shot: protocol not connected"))))

(define rerequest-state
  (make-shared-parameter
   (lambda (voters oid version note)
     (error "in rerequest-state: protocol not connected"))))

(define request-reply
  (make-shared-parameter
   (lambda (voters oid version note)
     (error "request-reply: protocol not connected"))))

;; (define resynchronize
;;   (lambda (qorum oid)
;;     (error "in resynchronize: protocol not connected")))

;; resync-frame-0! the inner working of actually resynchronising.

(define (keep-transaction-on-resync? x)
  (and (transaction? (oid-channel-entry-transaction x))
       (not (transaction-completed? (oid-channel-entry-transaction x)))
       (or (oid-channel-entry-ready? x)
	   (not (oid-channel-entry-overdue? x)))))

(define (resync-frame-0! quorum oid)
  (let* ((old-frame (find-local-frame-by-id oid 'resync-frame!))
	 (old (and old-frame (get-slot (aggregate-meta old-frame) 'version)))
	 (frame ((resynchronize) quorum oid))
	 (version (and-let* (((aggregate? frame))
			     (version
			      (get-slot (aggregate-meta frame)
					'version))
			     ;; sanity check
			     ((pair? version)))
			    (version-serial version))))
    (and-let*
     ((channel (existing-oid-channel oid)))
     (if version
	 (begin
	   (set-oid-channel-version! channel version)
	   (if (or (not old)
		   (not (equal? (get-slot (aggregate-meta frame) 'version) old)))
	       ;; TODO: This could be made more efficient.
	       (remove-transaction! oid (sub1 version) #f #f #t #f)
	       (begin
		 (remove-transaction! oid (sub1 version) #f #f #t #f)
		 (channel-filter-transactions! channel keep-transaction-on-resync?))))
	 (channel-filter-transactions! channel keep-transaction-on-resync?)))
    (and (aggregate? frame)			; sanitize just in case (been there)
	 frame)))

;; Answer with cached results or do the resynchronisation work and
;; cache the result.  Lock on "oid" is required.

(define (resync-frame! oid . quorum)
  (if (or (eq? oid (my-oid)) (eq? oid (public-oid)))
      (find-local-frame-by-id oid 'default-resync-frame!)
      (guard
       (exception
	((timeout-object? exception) #f)
	(else
	 (log-condition 'resync-frame! exception)
	 (raise (make-object-not-available-condition oid quorum 'resync-frame!))))
       (and-let*
	((quorum (if (pair? quorum) (car quorum)
		     (and-let* ((frame (find-local-frame-by-id oid 'default-resync-frame!)))
			       (replicates-of frame)))))
	(resync-frame-0! quorum oid)))))

(define (resync-locked-now! oid frame quorum)
  (cache-reref *resync-cache*
	     oid
	     (lambda ()
	       (let* ((of (or frame (find-local-frame-by-id oid 'resync-locked-now!)))
		      (old (and of (get-slot (aggregate-meta of) 'version)))
		      (frame (resync-frame! oid quorum)))
		 (if (and frame (not (equal? (get-slot (aggregate-meta frame) 'version) old)))
		     (commit-frame! #f frame (aggregate-meta frame) '()))
		 frame))))

(define (default-resync-now! oid . quorum)
  ;; precondition
  (hang-on-mutex! 'default-resync-now! (list (respond!-mutex oid)))
  (with-mind-modification
   (lambda (oid)
     (with-a-ref
      oid
      (retain-mutex
       (respond!-mutex oid)
       (let ((frame (find-local-frame-by-id oid 'default-resync-now!)))
	 (let ((has-old (and frame #t))
	       (old (and frame (get-slot (aggregate-meta frame) 'version))))
	   (and-let* ((frame (if (pair? quorum)
				 (or (and frame (resync-frame! oid (replicates-of frame)))
				     (resync-frame! oid (car quorum)))
				 (and old (resync-frame! oid (replicates-of frame))))))
		     (if (or (not has-old)
			     (not (equal? (get-slot (aggregate-meta frame) 'version) old)))
			 (commit-frame! #f frame (aggregate-meta frame) '()))
		     frame))))))
   oid))

(define (quorum-frame-sync! oid local-frame context)
  (oid-channel-wait-for-oid!
   find-local-frame-by-id transaction-completed?
   channel-request-state oid))


(define (init-place-upcalls!)
  (set!-commit-frame! default-commit-frame!)
  (set!-resync-now! default-resync-now!)
  (set-resync-completion! quorum-frame-sync!)
  )


;; *broadcast-method* switch between agreement protocol variants
;; defined (symbolic) values:
;;
;; eager-propose: First compute proposal then echo call.  Advantage:
;; if the user code, which computes the proposal, raises an exception,
;; network traffic is saved.
;;
;; interleave: First echo call then compute proposal.  Advantage:
;; shorten the total agreement time.
;;
;; one-shot: Combine echo-call and send-ready into one message.  This
;; is experimental.  It just occured to me this morning (2005-03-17)
;; that I can't see a reason to send two messages.  But I doubt me
;; beeing right at that.  After all the echo/ready algorithm is
;; straight from well known literature, while that oprimisation would
;; be a major advantage not yet documented anywhere.  As explained in
;; A23d71f139fb12a5b79e802bb2f1ac419 is was not safe.  However that
;; seemed to be easy to fix, simplifying the code.

(define *broadcast-method* 'eager-propose)

(define (quorum-broadcast-method . method) ; EXPORT
  (if (pair? method)
      (set! *broadcast-method*
            (case (car method)
              ((eager-propose) 'eager-propose)
              ((interleave) 'interleave)
              ((one-shot) 'one-shot)
              (else (error "broadcast method ~a undefined" (car method))))))
  *broadcast-method*)

;; I'm not sure that it is actually helpful to have retries here at
;; all.  However we have observed some unhandled messages despite they
;; where sent via TCP.  Maybe we can combat the loss that way.  After
;; all: it will be beneficial for a future UDP based mechanism.

(define $broadcast-debug-proposal (make-shared-parameter #f))

(define $broadcast-update-statistics (make-shared-parameter #f))


;; TODO: make this "number theory" a.k.a. arbitrary experimentation a
;; module of it's own.  It appears that the paper whos algorithm has
;; been used originally is the source of confusion.

;; (define (reliable-broadcast-required-echos quorum treshold)
;;   (if (fx>= (quorum-size quorum) 5)
;;       (quotient (add1 (fx+ (quorum-size quorum) treshold)) 2)
;;       (case (quorum-size quorum)
;; 	((4) 2)
;; 	((3) 1)
;; 	((2) 1)
;; 	((1) 0))))

(define (reliable-broadcast-required-echos quorum treshold)
  (quotient (fx+ (quorum-size quorum) treshold) 2))

;; (define (reliable-broadcast-required-readys quorum treshold)
;;   (min
;;    ;; again not 2t+1
;;    (add1 (fx* treshold 2))
;;    ;; but only never more than n-t-1 (fixes cases for 3 and 4 quorum)
;;    (sub1 (fx- (quorum-size quorum) treshold))
;;   ))

;; Other sources recommend t+1, but that has seen to hang in.

(define (reliable-broadcast-required-readys quorum treshold)
  (quotient (quorum-size quorum) 2))

(define (simple-majority quorum treshold)
  (fx+ (quotient (quorum-size quorum) 2) treshold))

;; We may want to use several more ready's than needed in theory for
;; reliable broadcast, because there is a need to deal with sudden
;; network partitioning.  We've seen even the first version to hang,
;; when a 4-size quorum ran for a while on three hosts and then one
;; site commited a transaction while the remaining two did not see the
;; ready.  (However it seems that one host had a wrong wall clock at
;; the same time.  So me might not have a diagnosis yet.
;;
;; (define (quorum-required-readys q t)
;;   (simple-majority q (if (fx>= t 1) (sub1 t) 0)))
;;
;; for quite a while I used:
;; (define (quorum-required-readys q t)
;;   (simple-majority q t))

;; Then:
;; (define quorum-required-readys reliable-broadcast-required-echos)

;; Even that hung up.  Looks as if we will need to experiment with
;; non-monotone updates.  $relaxed-replication in the upper layer will
;; now allow to go one version back under certain conditions.  In
;; reward we can revert to tougher vote counting again.

(define quorum-required-readys reliable-broadcast-required-readys)

#|
(define (quorum-send-echo-message! on frame e)
  (let* ((quorum (replicates-of frame))
	 (others (hosts-lookup-at-least
		  (quorum-others quorum)
		  (- (quorum-size quorum) (respond-treshold quorum) 1)))
	 (request (make-reader (oid-channel-entry-request e)))
	 (access (make-reader-access frame request))
	 (state-digest (or (fget frame 'version) '(cons 0 (oid->string on))))
	 (md (oid-channel-entry-request-digest e message-digest)))
    ((echo-call) others access request md state-digest)
    (logagree "On ~a echo ~a ~a sent to ~a\n" on md state-digest others)))
|#

(define (quorum-send-ready-message! on frame me request proposal chksum)
  (let ((result-digest (transaction-digest proposal)))
    (transaction-effects proposal)	; just to be sure
    ((rerequest-state)
     (quorum-others (replicates-of frame)) on
     (make-ready-message chksum result-digest)
     (lambda (value)
       (and-let* ((obj (find-local-frame-by-id on 'quorum-send-ready-message!)))
		 (advance-transactions obj value 'quorum-send-ready-message!))))))

(define (quorum-send-result! frame version request result)
  (define oid (aggregate-entity frame))
  (and-let*
   ;; Currently the existing code base (especially the old wiki and
   ;; the caching code) expects no restrictions on the result type and
   ;; freely passes other values too.  For regular frames however we
   ;; can enforce more protocol.
   (((frame? result))
    (reply-channel (get-slot request 'reply-channel)))
   ;; TODO: try to get rid of the 'caller' slot and carry the
   ;; replicates at the message.  This is half done, but the
   ;; protocol adaptor writen them out the wrong way.
   (set-slot! result 'caller oid)
   (mailbox-send! reply-channel result)
   (let ((caller (get-slot request 'caller)))
     (!start
      (let ((remote-sources
	     (let ((ff (find-frame-by-id/asynchroneous+quorum
			caller (replicates-of frame))))
	       ;; If we don't find the "fromframe"
	       ;; should we act upon the request at all?
	       (if (not ff) '()
		   (quorum-members-to-inform
		    (replicates-of frame) version
		    (replicates-of ff) 1)))))
	(if (pair? remote-sources)
	    (let ((result (if (frame? result) result (condition->message #f request result))))
	      (logagree "On ~a forwarding reply ~s to ~a\n"
			oid (get-slot result 'http-status) remote-sources)
	      ((replicate-reply) remote-sources oid version result))))
      (dbgname (oid->string oid) "~a send result ~a to ~a" result caller))))

  result)

;; @PRECONDITION: locks taken for frame and transaction
(define (quorum-accept-transaction! oid frame entry me racc)
  (define request (oid-channel-entry-request entry))
  (define proposal (oid-channel-entry-transaction entry))
  (define state (oid-channel-entry-state-digest entry))
  (define version (fget frame 'version))
  (define digest (transaction-digest proposal))

  ;; precondition
  (hang-on-mutex! 'quorum-accept-transaction! (list (respond!-mutex oid)))

  ;; (accept result)
  (and-let*
   (((equal? version state))
    (method (nu-action (fget frame 'mind-action-document)
		       'write frame (replicates-of frame))))
   (receive
    (result v)
    (with-mind-modification
     (lambda (oid)
       (retain-mutex
	(respond!-mutex oid)
	(if (eq? version
		 ;; Used to compare on 'frame' - which SHOULD be ok;
		 ;; but we've seen things taking a wrong turn.  Debugging now.
		 (fget (find-local-frame-by-id oid 'quorum-accept-transaction!) 'version))
	    (receive
	     (result . fail)
	     (begin
	       (me proposal 'restore-proposal-from-cache)
	       (if (and (pair? method) (procedure? (cdr method)))
		   (guard
		    (ex (else (me 'do 'rollback)
			      (log-condition
			       (format "~a exception in acceptor ~a\n~a\n"
				       oid (condition->string ex)
				       (and (node-list? (transaction-result proposal))
					    (xml-format (transaction-result proposal))))
			       ex)
			      (values (condition->message #f request ex) #t)))
		    (let ((result-code (transaction-result proposal)))
		      (within-time
		       (respond-timeout-interval)
		       ((cdr method) me racc result-code))))
		   (transaction-result proposal)))

	     (let ((rv (if (null? fail)
			   (guard
			    (ex (else (values (condition->message oid request ex) #f)))
			    (me
			     (vector (askemos:dc-date (get-slot request 'dc-date)) (version-chks digest))
			     'commit-version)
			    result)
			   result)))

	       (remove-transaction!
		oid (version-serial digest) request rv #t
		(let* ((caller (get-slot request 'caller))
		       (f (if (eq? caller oid) frame
			      (find-local-frame-by-id caller 'quorum-accept-transaction!))))
		  (if f (quorum-others (replicates-of f)) '())))

	       (values rv (and (null? fail) (eq? rv result)))))

	    (values (transaction-result proposal) #f))))
     oid)

    (if (legal-channel-message? result)
	(begin
	  (transaction-send-message-version! oid (version-serial digest) result)
	  (if v (quorum-send-result! frame version request result))))

   result)))

(define (quorum-accept-transaction oid frame entry me racc)
  (pass-a-ref oid (quorum-accept-transaction! oid frame entry me racc) oid))

(cond-expand
 ((or rscheme trust-local-machine)
  (define quorum-accept-transaction+ quorum-accept-transaction))
 (else
  (define quorum-accept-transaction+ quorum-accept-transaction!)))

(define (channel-request-state on frame channel)
  (define msg (agreement-last-message frame))
  (define others (quorum-others (replicates-of frame)))
  (define start (and $agree-verbose (srfi19:current-time 'time-utc)))
  (logagree "On ~a channel ~a to ~a\n" on msg others)
  ((rerequest-state) others on msg
   (lambda (value)
     (define took (and $agree-verbose (time-difference (srfi19:current-time 'time-utc) start)))
     (logagree "On ~a ~a host ~a ~a.~a'' ~a\n" on msg
	       (get-slot value 'authorization)
	       (literal-time-second (time-second took))
	       (literal-time-second (time-nanosecond took))
	       (get-slot value 'mind-body))
     (and-let* ((obj (find-local-frame-by-id on 'channel-request-state)))
	       (advance-transactions obj value 'channel-request-state)))))

(define (length< lst n)
  (if (eqv? n 0)
      #f
      (let loop ((lst lst) (n n))
	(or (null? lst) 
	    (and (fx>= n 2) (loop (cdr lst) (sub1 n)) )))))

(cond-expand
 ((or rscheme)
  (define-macro (log-ignored-message fmt . args)
    `(if (number? $agree-verbose) (logerr ,fmt . ,args))))
 (else
  (define-syntax log-ignored-message
    (syntax-rules ($agree-verbose)
      ((log-ignored-message fmt args ...)
       (if (number? $agree-verbose) (logerr fmt args ...)))))))

(define-condition-type &old-message &condition old-message?)

(define *old-message* (make-condition &old-message))

(define-transaction-update transaction-complete
  ;; input fields
  (work ready missing-echos uninformed required missing)
  ;; updated fields
  (ready missing-echos uninformed required missing)
  (on message frame version chksum use-echos start-time)	; addition arguments
  (completed)				; result values
  (let ((auth (get-slot message 'authorization))
	(msg (get-slot message 'mind-body)))
    (cond
     ((and (current-echo? version msg)
	   (member auth uninformed))
      (if (matches-request? chksum msg)
	  (let ((echos (sub1 missing-echos))
		(rest (delete auth uninformed)))
	    (logagree/effective
	     'echo auth on version missing-echos required msg rest missing)
	    (values #f (or ready (fx>= 0 echos)) echos rest required missing))
	  (begin
	    (log-ignored-message
	     "On ~a ~a (~a/~a) ~a ignored ~a ~a uninformed ~a\n"
	     on version missing-echos required chksum auth msg uninformed)
	    ;; this exceptions should have a different name now
	    (raise *old-message*))))
     ((and (or (current-ready? version msg)
	       (next-echo? version msg))
	   (member auth missing))
      ;; TODO/FIXME: we ought to avoid referencing "work" since this
      ;; could block for ever.
      (receive (effects result digest) (%transaction-work-force work)
	(cond
	 ((not (pair? digest))
	  (if $agree-verbose
	      (log-condition 'transaction-complete effects))
	  (values transaction-raised-exception
		  transaction-failed missing-echos uninformed required missing))
	 ((or (and (current-ready? digest msg) (matches-request? chksum msg))
	      (next-echo? digest msg))
	  (and-let* ((f ($broadcast-update-statistics)))
		    (f on auth (time-difference
				(srfi19:current-time 'time-utc)
				start-time)))
	  (if (confirms-version? digest msg)
	      (let ((missing (delete auth missing)))
		(if (fx>= 1 required)
		    (begin
		      (logagree/effective 'ready-completed auth on digest msg)
		      (values #t transaction-done 0 '() 0 missing))
		    (let ((doublecount (member auth uninformed)))
		      (logagree/effective 'ready auth on digest msg (sub1 required) missing)
		      (let ((echos (if doublecount (sub1 missing-echos) missing-echos)))
			(values (if (null? missing) transaction-failed #f)
				(if (null? missing) transaction-failed
				    (or ready
					(fx>= 0 echos)
					(fx< required (respond-treshold (replicates-of frame)))))
				echos
				(if doublecount (delete auth uninformed) uninformed)
				(sub1 required) missing)))))
	      (let ((left (delete auth missing)))
		(logagree/effective 'contra auth on version digest msg left)
		(if (length< left required)
		    (values transaction-failed
			    transaction-failed missing-echos uninformed required left)
		    (values #f ready missing-echos uninformed required left)))))
	 (else
 	  (log-ignored-message "On ~a ~a ~a ignored ~a wrong message ~a uniformed ~a missing ~a\n"
			       on version digest auth msg uninformed missing)
	  (raise *old-message*)))))
     (else
      (log-ignored-message "On ~a ~a ~a ignored ~a old message ~a uninformed ~a missing ~a\n"
			   on version chksum auth msg uninformed missing)
      (raise *old-message*)
      ;; (values #f ready missing-echos uninformed required missing)
      ))))

;; TODO: These should be a syntax-expanded thing passing additional agruments...
(define (sync-message-for-each proc message)
  (let loop ((body (get-slot message 'mind-body)))
    (let ((tag (and (sync-message-data? body) (sync-message-phase body))))
      (cond
       ((eq? tag 'one-shot)
	(for-each loop (sync-message-container-messages message)))
       ((or (eq? tag 'echo) (eq? tag 'ready)) (proc body))))))

(define (sync-message-fold kons init message)
  (let loop ((body (get-slot message 'mind-body))
	     (init init))
    (let ((tag (and (sync-message-data? body) (sync-message-phase body))))
      (cond
       ((eq? tag 'one-shot)
	(fold loop init (sync-message-container-messages body)))
       ((or (eq? tag 'echo) (eq? tag 'ready)) (kons body init))))))

(define (transaction-precheck-and-force! proposal version message)
  (and
   (member (get-slot message 'authorization) (transaction-missing proposal))
   (sync-message-for-each
    (lambda (message)
      ;; TODO: We could REALLY SAVE this predicate, if we COULD check
      ;; the promise "(transaction-work proposal)" having beeing
      ;; forced already - instead of checking the need-of-force'ing
      ;; before possibly re-forcing it anyway.
      (if (and (or (current-ready? version message)
		   (next-echo? version message)))
	  (force (transaction-work proposal) identity)))
    message)))

(define (complete-transaction-entry! on frame queue version maybe entry message)
  (let ((request (oid-channel-entry-request entry)))

    (cond
     ((not request)
      (logerr "Emtpy request on ~a ~a\n"
	      (aggregate-entity frame)
	      (transaction-digest (oid-channel-entry-transaction entry)))
      #t)
     ((transaction-completed? (oid-channel-entry-transaction entry)) #t)
     (else
      (let ((ready (transaction-ready? (oid-channel-entry-transaction entry)))
	    (chksum (oid-channel-entry-request-digest entry message-digest)))
	(guard
	 (ex ((old-message? ex) (transaction-completed? (oid-channel-entry-transaction entry)))
	     ;; KLUDGE: to my understanding unhandled exceptions should be
	     ;; reraised in the guard context, not the inner one.
	     ;; Apparently this does not happen in chicken, which is
	     ;; strange.
	     (else (raise ex)))
	 (receive
	  (complete proposal)
	  (transaction-complete!
	   entry
	   oid-channel-entry-transaction
	   test-and-set-oid-channel-entry-transaction!
	   on message frame version
	   chksum
	   maybe
	   (oid-channel-entry-start-time entry))
	  (cond
	   ((and (not ready) (or (transaction-ready? proposal)
				 (transaction-done? proposal)))
	    (let* ((racc (make-reader request))
		   (acc (make-access frame racc)))
	      (guard
	       (ex (else (transaction-set-ready!
			  entry
			  oid-channel-entry-transaction
			  test-and-set-oid-channel-entry-transaction!
			  transaction-failed)
			 ;; This is not nice.  Think about a different API.
			 (let* ((result0 (force (transaction-work proposal) identity))
				(result (if (frame? result0) result0 (condition->message #f request result0))))
			   (if (procedure? ($broadcast-debug-proposal))
			       (($broadcast-debug-proposal) on acc racc result0))
			   (transaction-send-message-version! on (version-serial version) result)
			   (remove-transaction! on (version-serial version) request result
						(eq? result result0)
						(let* ((caller (get-slot request 'caller))
						       (f (if (eq? caller on) frame
							      (find-local-frame-by-id caller 'channel-request-state))))
						  (if f (quorum-others (replicates-of f)) '())))
			   (quorum-send-result! frame version request result))))
	       (let ((result (transaction-result proposal)))
		 (and-let* ((dbg ($broadcast-debug-proposal)) ((procedure? dbg)))
			   (dbg on acc racc result)))
	       (quorum-send-ready-message! on frame acc racc proposal chksum)
	       (if (transaction-done? proposal)
		   (begin
		     (quorum-accept-transaction+ on frame entry acc racc)
		     ;; (queue-request-state on queue)
		     )))
	      (transaction-completed? proposal)))
	   ((eq? complete transaction-raised-exception)
	    (let ((result (force (transaction-work proposal)
				 (lambda (e) (condition->message #f request e)))))
	      ;;(transaction-send-message-version! on (version-serial version) result)
	      (remove-transaction!
	       ;; KLUDGE: 2012 change: pass "request" as "request-id";
	       ;; however this should either not be done for
	       ;; exceptions.  Time to clean up the clean up of the
	       ;; code... But passing #f instead of result hides it
	       ;; from the user.
	       on (version-serial version) request result #f
	       (let* ((caller (get-slot request 'caller))
		      (f (if (eq? caller on) frame
			     (find-local-frame-by-id caller 'channel-request-state))))
		 (if f (quorum-others (replicates-of f)) '())))
	      (quorum-send-result! frame version request result)
	      #t))
	   ((transaction-failed? proposal)
	    (logagree
	     "On ~a complete-transaction droped ~a ~a.\n"
	     on (if ready "promise" "proposal") (transaction-digest proposal))
	    #t)
	   ((transaction-done? proposal)
	    (let* ((racc (make-reader request))
		   (acc (make-access frame racc)))
	      (quorum-accept-transaction+ on frame entry acc racc)) #t)
	   (else #f)))))))))

(define (advance-transactions/1 on frame channel message call-indicator)
  (and-let*
   ((body (get-slot message 'mind-body))
    (version (sync-message-version body)))
   (channel-advance-transactions
    transaction-completed? transaction-done? complete-transaction-entry!
    oid-channel-entry-ready? transaction-precheck-and-force!
    on frame channel message (version-serial version)
    call-indicator)))

(define (advance-transactions obj message call-indicator)
  (let ((oid (aggregate-entity obj)))
    (let ((auth (get-slot message 'authorization))
	  (channel (existing-oid-channel oid)))
      (if channel
	  (sync-message-fold
	   (lambda (msg init)
	     (or (advance-transactions/1
		  oid obj channel
		  (make-message (property 'mind-body msg)
				(property 'authorization auth))
		  call-indicator) 
		 init))
	   #f
	   message)
	  (and-let* ((obj (resync-now! oid)))
		    (advance-transactions obj message call-indicator))))))

(define (message-ahead-of v msg)
  (let ((local-version (if v (version-serial v) 0)))
    (let loop ((msg msg))
      (and (sync-message-data? msg)
	   (let ((tag (sync-message-phase msg)))
	     (case tag
	       ((ready)
		(and (sync-message-version msg)
		     (fx< local-version (sync-message-serial msg))))
	       ((echo)
		(and (sync-message-version msg)
		     (fx< local-version (sync-message-serial msg))))
	       ((one-shot)
		(any loop (sync-message-container-messages msg)))
	       (else #f)))))))
