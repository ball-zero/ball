;; (C) 2002 Jörg F. Wittenberger see http://www.askemos.org

;;*** Evaluation server

;; Start the evaluation server from util.scm.

(thread-start!
 (make-thread
  (lambda ()
    (define eval-server-port 7070)
    (define (exit) (mind-exit))
    (define (ev expr)
      (enter-front-court (lambda (expr) (eval expr)) expr))
    (logerr " Debug access open at port ~a.\n" eval-server-port)
    (eval-server eval-server-port 
                 `(exit . ,exit) `(eval . ,ev)))
  "repl"))
