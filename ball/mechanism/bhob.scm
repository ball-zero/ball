;; (C) 2011, 2013 Jörg F. Wittenberger, GPL; see www.askemos.org

;; A BHOB is a huge, huge BLOB.

(define (bhob? obj)
  (and (a:blob? obj) (fx>= (blob-blocks-per-blob obj) 2)))

(define $bhob-size (make-config-parameter 10000000))

(define $bhob-surrogate
  (make-config-parameter
   (lambda (store blob key)
     (raise (format "BLOB too large to load in memory: ~a" (blob-sha256 blob))))))

;;** Blobs

(: a:make-blob
   (:blob-hash:
    (or false number) (or false fixnum) (or false fixnum) (or false vector)
    -> :blob:))
(: a:blob? (* --> boolean : :blob:))
(: bhob? (* -> boolean))
(: blob-sha256 (:blob: --> :blob-hash:))
(: a:blob-size (:blob: --> (or false number)))
(: set-blob-size! (:blob: number -> undefined))
(: blob-blocks-per-blob (:blob: --> (or false fixnum)))
(: blob-blocks (:blob: --> (or false vector)))
(: set-blob-blocks! (:blob: vector -> undefined))

(define-record-type <blob>
  ;; Our frame pools handle a few slots special for the sake of
  ;; efficiency.
  ;;
  ;; TODO use http://www.metalinker.org/ suggestions how to encode the
  ;; bt/infohash
  (%make-blob
   sha256
   size
   block-size
   blocks-per-blob
   blocks)
  a:blob?
  (sha256 blob-sha256)
  (size a:blob-size set-blob-size!)
  (block-size blob-block-size)
  (blocks-per-blob blob-blocks-per-blob)
  (blocks blob-blocks set-blob-blocks!))

(cond-expand
 (rscheme
  (define (a:make-blob
	   sha256
	   size
	   block-size
	   blocks-per-blob
	   blocks)
    (let ((v (%make-blob
	      sha256
	      size
	      block-size
	      blocks-per-blob
	      blocks)))
#|
      (register-for-finalization v)
      (register-blob-ref! sha256 v)
|#
      v))
  (define-method finalize ((self <blob>))
    (unregister-blob-ref! (blob-sha256 self) self)))
 (chicken
  (define (a:make-blob
	   sha256
	   size
	   block-size
	   blocks-per-blob
	   blocks)
    (let ((v (%make-blob
	      sha256
	      size
	      block-size
	      blocks-per-blob
	      blocks)))
#|
      (set-finalizer! v finalize-blob!)
      (register-blob-ref! sha256 v)
|#
      v))
  (define (finalize-blob! blob)
    (unregister-blob-ref! (blob-sha256 blob) blob))))

;;** BLOBs low level

(cond-expand
 (never

(define *transient-blobs* (make-symbol-table))

(define *transient-blobs-mutex* (make-mutex '*transient-blobs*))

(define (traced-transient-blobs-register! key obj)
  (hash-table-update!
   *transient-blobs* key
   (lambda (old)
     (vector (add1 (vector-ref old 0)) (cons obj (vector-ref old 1))))
   (lambda () '#(0 ()))))

(define (transient-blobs-register! key obj)
  (hash-table-update!
   *transient-blobs* key
   (lambda (x) (add1 x)) (lambda () 0)))

(define (register-blob-ref! sha256 obj)
  (with-mutex *transient-blobs-mutex* (transient-blobs-register! sha256 obj)))

(define (traced-transient-blobs-free! key obj)
  (let ((old (hash-table-ref/default *transient-blobs* key #f)))
    (if (eqv? (debug 'DEL (vector-ref old 0)) 1)
	(hash-table-delete! *transient-blobs* key)
	(hash-table-set!
	 *transient-blobs* key
	 (vector (sub1 (vector-ref old 0))
		 (delete obj (vector-ref old 1)))))))

(define (transient-blobs-free! key obj)
  (let ((old (hash-table-ref/default *transient-blobs* key #f)))
    (if (eqv? old 1)
	(hash-table-delete! *transient-blobs* key)
	(hash-table-set! *transient-blobs* key (sub1 old)))))

(define (unregister-blob-ref! sha256 obj)
  (with-mutex *transient-blobs-mutex* (transient-blobs-free! sha256 obj)))

(define (print-blob-usage)
  (with-mutex
   *transient-blobs-mutex*
   (hash-table-fold
    *transient-blobs*
    (lambda (k v port) (format port "~a: ~s\n" k v) port)
    (current-output-port))))

(define (non-transient-blob? sha256)
  (not (hash-table-ref/default *transient-blobs* sha256 #f)))
)
(else
 (define a:make-blob %make-blob)
 (define (register-blob-ref! sha256 obj) #f)
 (define (unregister-blob-ref! sha256 obj) #f)
 (define (non-transient-blob? sha256) #t)
 (define (print-blob-usage) (display "not implemented"))
))