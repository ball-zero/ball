;; (C) 2000-2003, 2006, 2009 Jörg F. Wittenberger see http://www.askemos.org

;;** File System Mirror

;; A backup, exploration etc. example how to save everything into the
;; file system.  Could have all sorts of features, but hasn't any.

(define $fsm-precise (make-shared-parameter #f))

(define $fsm-blob-paranoia (make-shared-parameter #f))

(define (make-fsm-component nufsm-base identifier component version . name)
  ;; version is ignored so far.
  (define mkp make-pathname)
  (let* ((idstr (cond
		 ((string? identifier) identifier)
		 ((symbol? identifier) (symbol->string identifier))
		 ((oid? identifier)  (oid->string identifier))
		 (else (error "illegal identifier class ~a" identifier))))
         (version (case version
		    ((current) ",c")
		    ((next) ",n")
		    ((old) ",o")
		    (else (raise (format "make-fsm-component: unknown version id ~a" version)))))
         (d1 (substring idstr 0 2))
         (d2 (substring idstr 2 4)))
    (case component
      ((blob) (mkp (list nufsm-base "blob" d1 d2) idstr
		   (and (and (pair? name) (car name)) (car name))))
      ((rdf) (mkp (list nufsm-base d1 d2) idstr (string-append ".rdf" version)))
      ((body) (mkp (list nufsm-base d1 d2) (string-append idstr version)))
      ((link) (mkp (list nufsm-base d1 d2 (string-append idstr ".dir"))
		   (string-append "X" (car name) version)))
      ((linkdir) (mkp (list nufsm-base d1 d2)
		      (string-append idstr ".dir" (case version ((old) ",o") (else "")))))
      ((sqlite-user)
       (mkp (list nufsm-base d1 d2) idstr "sqlite"))
      (else (error (format #f "unknown component ~a" component))))))

(define (zip-system-core base fn)
  (define (body-file rel oid)
    (let ((b (make-fsm-component base oid 'body 'current)))
      (if (file-exists? b)
	  (make-fsm-component rel oid 'body 'current)
	  (let ((b (fget (document oid) 'mind-body)))
	    (make-fsm-component rel (blob-sha256 b) 'blob 'current)))))
  (define (include-links rel links recursive?)
    (fold-links
     (lambda (k v i)
       (define d (document v))
       (define t (get-slot (aggregate-meta d) 'mind-action-document))
       (cons*
	(make-fsm-component rel v 'rdf 'current)
	(body-file rel v)
	(make-fsm-component rel t 'rdf 'current)
	(body-file rel t)
	(let ((tl (let ((tl (fget (document t) 'mind-links)))
		    (and #f tl (include-links rel tl #t))))
	      (rl (if recursive?
		      (let ((l (fget d 'mind-links)))
			(if l (append (include-links rel l #t) i) i))  
		      i)))
	  (if tl (append tl rl) rl))))
     '()
     links))
  ;; (if (file-exists? (make-pathname base fn)) (error fn))
  (receive (p t f)
	   (make-process-stub
	    (let ((base "")
		  (ob base))
	      `("zip" ,fn
		,@(fold (lambda (f i)
			  (if (member f '("." "..")) i (cons (make-pathname '("ssl") f) i)))
			'()
			(scandir (make-pathname ob "ssl")))
		,(make-fsm-component base null-oid 'rdf 'current)
		,(make-fsm-component base one-oid 'rdf 'current)
		,(body-file base one-oid)
		,(make-fsm-component base (my-oid) 'rdf 'current)
		,(make-fsm-component base (public-oid) 'rdf 'current)
		,(body-file base (public-oid))
		,@(include-links base (fget (document (my-oid)) 'mind-links) #f)
		,@(include-links base (fget (document (public-oid)) 'mind-links) #t)
		,@(let ((cfgscm (make-pathname base "config.db")))
		    (if (file-exists? cfgscm) (list cfgscm) '()))
		;; undocumented and to be undocumented feature
		,@(let ((cfgscm (make-pathname base "config.scm")))
		    (if (file-exists? cfgscm) (list cfgscm) '()))
		,@(let ((cfgxml (make-pathname base "config.xml")))
		    (if (file-exists? cfgxml) (list cfgxml) '()))))
	    (make-pathname base #f))
	   (dynamic-wind
	       (lambda () #f)
	       (lambda ()
		 (close-output-port t)
		 (process-wait p))
	       (lambda () (close-input-port f)))))

;;*** BLOB support

(define (fsm-user-sqlite-file base identifier)
  (make-fsm-component base identifier 'sqlite-user 'current))

(define (fsm-store-blob-notation store blob key value)
  #f)

(define (fsm-store-blob-value store blob octet-stream)
  (let ((file (make-fsm-component store (blob-sha256 blob) 'blob 'current #f)))
    (if (file-exists? file)
	(begin
	  ;; avoid garbage collection:
	  (set-file-modification-time! file (time-second *system-time*))
	  blob)
	(let ((nf (string-append file ",n")))
	  (mkdirs (dirname nf))
	  (call-with-output-file nf
	    (lambda (port) (display octet-stream port)))
	  (rename-file nf file)
	  blob))))

(define (fsm-copy-blob-value store blob to n foff toff)
  (let ((file (make-fsm-component store (blob-sha256 blob) 'blob 'current #f)))
    (and (file-exists? file)
	 (copy-file-data! file to n foff toff))))

(define (fsm-fetch-blob-notation store blob key)
  (and (not key)
       (let ((file (make-fsm-component store (blob-sha256 blob) 'blob 'current key)))
	 (and (file-exists? file)
	      (if (fx>= (file-size file) ($bhob-size))
		  (($bhob-surrogate) store blob key)
		  (let ((data (file->string file)))
		    (if ($fsm-blob-paranoia)
			(if (string=? (sha256-digest data) (symbol->string (blob-sha256 blob)))
			    data
			    (error (format "checksum mismatch ~a" (blob-sha256 blob))))
			data)))
	      #;(file->string file))))) ;; FIXME: This is Unsinn.

(define (fsm-blob-notation-size store blob key)
  (let ((f (make-fsm-component store (blob-sha256 blob) 'blob 'current key)))
    (and (file-exists? f) (file-size f))))

(define (fsm-display-blob-notation store blob key display)
  (let ((file (make-fsm-component store (blob-sha256 blob) 'blob 'current key)))
    (and (file-exists? file)
	 ;; KLUDGE: instead of read-bytes allocating a fresh string
	 ;; each round, we ought to read into a static string buffer.
	 ;; Unfortunately this is too system dependant.
	 (call-with-input-file file
	   (lambda (port)
	     (let loop ((len (file-size file)))
	       (if (> len 0)
		   (let ((s (read-bytes (min len #x3ffff) port)))
		     (if (not (eof-object? s))
			 (begin
			   (display s)
			   (loop (- len #x3ffff))))))))))))

;;*** Administrative routines

;; Clean up the file system.

;; (define (nufsm-cleanup name)
;;   (define (prune-directory name)
;;     (let ((path (string->file name))          (stat-data (stat name)))
;;       (if (and stat-data (stat-directory? stat-data)
;;                (< (length (scandir name)) 3))
;;           (begin
;;             (remove-dir name)
;;             (prune-directory (pathname->string (file-directory? path))))
;;           name)))
;;   (if (file-empty? name)
;;       (let ((mf (make-fsm-component name 'rdf 'current)))
;;         (remove-file name)
;;         (if (file-exists? mf) (remove-file mf))
;;         (prune-directory (dirname name)))
;;       name))

(define (format-right port prepos postpos right)
  (display prepos port)
  ;; (display (right->string right) port)
  (display (right->absolute-string right) port)
  (display postpos port))

;; Write the meta data of a document as RDF file.

;; FIXME: This is currently only used for reference purposes.  It
;; should be used when reading the data back in.

;; FIXME: See the begin ;;*** Data Structures: once we changed the
;; data model to use RDF, this should become a simple "xml-format"
;; into the .rdf file.

(define rdf-special-slots
  '(version replicates
    capabilities potentialities protection
    dc-creator dc-date last-modified mind-action-document))

(define other-slots
  (let ((result (filter (lambda (x) (not (memq x rdf-special-slots)))
                        (known-slots))))
    (lambda () result)))

(define (write-link base-directory identifier name value update)
  (display "   <rdf:li rdf:resource=\"")
  (xml-quote-display-at-port name (current-output-port))
  (display "\"")
  (if (oid? value)
      (begin
	(display " href=\"")
	(if (number? ($fsm-verbose)) (logerr "LINK ~a => ~a\n" identifier value))
	(display value)
	(display "\"/>\n"))
      (begin
	(display " rdf:parseType=\"Literal\">")
	(cond
	 ((node-list? value)
	  (if update
	      (let* ((fs-name (uri-quote name))
		     (fn (make-fsm-component base-directory
					     identifier 'link 'next
					     fs-name))
		     (on (make-fsm-component base-directory
					     identifier 'link 'current
					     fs-name)))
		(mkdirs (dirname fn))
		(call-with-output-file fn
		  (lambda (port) (xml-format-at-output-port value port)))
		(rename-file fn on))))
	 ;;((aggregate? value) (format #t "~x" (aggregate-entity value)))
	 ((number? value) (format #t "~x" value))
	 ((pair? value)
	  (for-each (lambda (p) (format #t "~a" p)) value))
	 (else (format #t "~a" value)))
	(display "</rdf:li>\n"))))

(define (write-links base-directory identifier frame new-links)
  (receive
   (last links changed) (frame 'mind-links)
   (if (or last (pair? new-links))
       (let ((ff (and last (eq? changed 'update) (eq? last links))))
         (display "  <nu:links><rdf:Bag id=\"links\">\n")
         (if last
             (links-for-each
              (lambda (key v)
                ;; FIXME I don't know anymore why/when new-links
                ;; is not a pair.  The test should be simpler.
                (if (not (and (pair? new-links) (assoc key new-links)))
                    (receive
                     (o value change) (frame key)
                     (write-link base-directory identifier key value (or change ff)))))
              last))
         (if (pair? new-links)
             (for-each
              (lambda (link)
                (if (cdr link)
                    (write-link base-directory
                                identifier (car link) (cdr link) #t)
                    (let ((fn (make-fsm-component base-directory
                                                  identifier 'link 'current
                                                  (uri-quote (car link)))))
                      (if (file-exists? fn) (remove-file fn)))))
              new-links))
         (display "  </rdf:Bag></nu:links>\n")))))

;; TODO FIXME wrt. RDF validation.  I guess I've taken that too
;; lightly.  This code become a mess it is about time to use the RDF
;; stuff from nunu.scm to write/read the fsm data.

(define fsm-namespaces-rev-declaration
  (fold
   (lambda (x i) (binding-set-insert i (cdr x) (car x)))
   (empty-binding-set)
   `((a . ,namespace-mind)
     (nu . ,(string->symbol "http://www.askemos.org/Askemos/PlaceSlots05"))
     (dc . ,namespace-dc)
     (rdf . ,namespace-rdf)
     (dav . ,namespace-dav)
     (xml . ,namespace-xml))))

(define (write-rdf base-directory identifier frame new-links)
  (define ns "nu")
  (display "<?xml version=\"1.0\" ?>
<rdf:RDF
 xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
 xmlns:dc=\"http://dublincore.org/documents/2004/12/20/dces/\"
 xmlns:nu=\"http://www.askemos.org/Askemos/PlaceSlots05\"
 xmlns:a=\"http://www.askemos.org/2000/CoreAPI#\"
 xmlns:dav=\"DAV:\"
>
 <rdf:Description rdf:about=\"")

  (xml-quote-display-at-port identifier (current-output-port))
  (display "\">\n")
  (receive
   (o creator c-c) (frame 'dc-creator)
   (display "  <dc:creator>")
   (display creator)
   (display "</dc:creator>\n"))
  (receive
   (o date date-changed) (frame 'dc-date)
   ;; Dublin Core used to be more restrictive on "date" beeing the
   ;; creation date.  In that light we might have to change back to
   ;; use version 1.0?
   (if date
       (begin
         (display "  <dc:date>")
         (display (rfc-822-timestring date))
         (display "</dc:date>\n"))))
  (receive
   (last date date-changed) (frame 'last-modified)
   ;; Dublin Core used to be more restrictive on "date" beeing the
   ;; creation date.  In that light we might have to change back to
   ;; use version 1.0?
   (if date
       (begin
         (display "  <nu:last-modified>")
         (display (rfc-822-timestring date))
         (display "</nu:last-modified>\n"))))
  (receive
   (o ad c-c) (frame 'mind-action-document)
   (display "  <nu:action-document>")
   (display ad)
   (display "</nu:action-document>\n"))
  (receive
   (last version c-c) (frame 'version)
   (display "  <nu:version>")
   (write (or version (cons 0 (if (string? identifier)
                                  identifier
                                  (symbol->string identifier)))))
   (display "</nu:version>\n"))
  (receive
   (last lang lang-changed) (frame 'dc-language)
   (if lang
       (begin
         (display "  <dc:language>")
         (display lang)
         (display "</dc:language>\n"))))
  (receive
   (last date date-changed) (frame 'last-modified)
   (if date
       (begin
         (display "  <dav:last-modified>")
         (display (rfc-822-timestring date))
         (display "</dav:last-modified>\n"))))
  ;; Would we better use dc:publisher instead of nu:replicates?
  ;; That's a hard question, similar to whether or not use
  ;; dc:identifier to tag the OID.  Outside of the context of Askemos
  ;; ISBN numbers etc. often take the role of of OID's and the
  ;; publisher property might have been used to name a company.
  ;; However publisher would be the correct property to encode
  ;; replicates.
  (receive
   (last replicates c-c) (frame 'replicates)
   (display "  <nu:replicates>")
   (write (if (quorum? replicates)
              (map (lambda (x)
                     (let ((str (attribute-string 'resource x)))
                       (or (string->oid str) str)))
                   ((sxpath '(Bag li))
                    (quorum-xml replicates)))
              replicates))
   (display "</nu:replicates>\n"))
  (receive
   (last protection c-c) (frame 'protection)
   (if protection
       (format-right (current-output-port)
                     "  <nu:protection>" "</nu:protection>\n" protection)))
  (receive
   (last capabilities c-c) (frame 'capabilities)
   (if
    capabilities
    (begin
      (display "  <nu:capabilities>\n")
      (do ((capabilities capabilities (cdr capabilities)))
          ((null? capabilities))
        (format-right (current-output-port)
                      "   <nu:right>" "</nu:right>\n" (car capabilities)))
      (display "  </nu:capabilities>\n"))))
  (receive
   (last potentialities c-c) (frame 'potentialities)
   (if
    potentialities
    (begin
      (display "  <nu:potentialities>\n")
      (do ((potentialities potentialities (cdr potentialities)))
          ((null? potentialities))
        (format-right (current-output-port)
                      "   <nu:right>" "</nu:right>\n" (car potentialities)))
      (display "  </nu:potentialities>\n"))))
  (receive
   (last body c-c) (frame 'mind-body)
   (if (vector? body) (set! body (vector-ref body 0)))
   (if (a:blob? body)
       (begin
	 (display "  <nu:body>\n")
	 (display (xml-format-exc-c14n (blob->xml body) fsm-namespaces-rev-declaration))
	 (display "  </nu:body>\n"))))
  (receive
   (last body c-c) (frame 'sqlite3)
   (if (vector? body) (set! body (vector-ref body 0)))
   (if (a:blob? body)
       (begin
	 (display "  <nu:sqlite3>\n")
	 (display (xml-format-exc-c14n (blob->xml body) fsm-namespaces-rev-declaration))
	 (display "  </nu:sqlite3>\n"))))
  ;; Content-Type SHOULD be moved into dc:format
  (do ((s (other-slots) (cdr s)))
      ((null? s))
    (let ((gi (car s)))
      (receive
       (last v v-c) (frame gi)
       (if v
           (begin
             (display "  <") (display ns)
             (display ":") (display gi)
             (display ">")
             (display (if (number? v) (number->string v 16) v))
             (display "</") (display ns)
             (display ":") (display gi)
             (display ">\n"))))))
  (receive
   (last davprop davprop-changes) (frame 'dav:properties)
   (if davprop
       (begin
         (display "  <dav:properties>\n")
         (xml-format-fragment-at-output-port davprop (current-output-port))
         (display "\n </dav:properties>\n"))))
  (write-links base-directory identifier frame new-links)
  (display " </rdf:Description>\n</rdf:RDF>\n"))

(define (make-defaulted-quorum lst)
  (cond
   ((xml-element? lst) (make-quorum lst))
   ((pair? lst) (make-quorum lst))
   (else *local-quorum*)))

(define those-undecided-namespaces
  (list
   namespace-mind
   namespace-mind-v0
   namespace-sync
   #f
   ))

(define (fsm-xml->blob store element)
  (let ((blob (xml->blob (document-element (children element)))))
    (if (not (a:blob-size blob))
	(let ((s (fsm-blob-notation-size store blob #f)))
	  (if s (set-blob-size! blob s))))
    blob))

(define (read-rdf! base-directory frame desc)
  (node-list-map
   (lambda (element)
     (case (gi element)
       ((version)
        (cond
          ((memq (ns element) those-undecided-namespaces)
           (fset!
            frame 'version
            (cons (string->number
                   (data (select-elements (children element) 'serial)))
                  (data (select-elements (children element) 'checksum)))))
          ((eq? (ns element) 'http://www.askemos.org/Askemos/PlaceSlots05)
           ;; KLUDGE backward compatibility.  The 'cond' ought to go.
           (let ((found (call-with-input-string (data element) read)))
             (fset! frame 'version
                    (cond ((pair? found) found)
                          ((number? found)
                           (cons found (aggregate-entity frame)))
                          (else (cons 0 (aggregate-entity frame)))))))
          (else (error "read-rdf!: ~a unknown namespace \"~a\" on version"
		       (aggregate-entity frame) (ns element)))))
       ((replicates)
        (cond
          ((memq (ns element) those-undecided-namespaces)
           (fset! frame 'replicates (make-quorum element)))
          ((eq? (ns element) 'http://www.askemos.org/Askemos/PlaceSlots05)
           (fset! frame 'replicates
                  (make-defaulted-quorum
                   (with-input-from-string (data element) read))))
          (else (error "read-rdf!: namespace on replicates ~a" (ns element)))))
       ((creator)
        (fset! frame 'dc-creator (string->oid (data element))))
       ((date)
        (fset! frame 'dc-date (guard
                               ;; crude lie to keep running
                               (c (else
                                   (logfsm "Invalid date in ~a\n" (data element))
                                   (current-date (timezone-offset))))
                               (srfi19:string->date
                                (data element)
                                rfc-822-time-string-format))))
       ((last-modified)
        (fset! frame 'last-modified (srfi19:string->date
				     (data element)
				     rfc-822-time-string-format)))
       ((action-document)
        (fset! frame 'mind-action-document (string->oid (data element))))
       ((last-modified)
        (fset! frame 'last-modified (srfi19:string->date
				     (data element)
				     rfc-822-time-string-format)))
       ((protection)
        (fset! frame 'protection (mind-parse-protection (data element))))
       ((capabilities potentialities)
        (fset! frame (gi element)
               (reverse! ;; just consed
		(node-list-reduce
		 (children element)
		 (lambda (init element)
		   (if (match-element? 'right element)
		       (cons (mind-parse-protection (data element)) init)
		       init))
		 '()))))
       ((content-type secret language)
        (fset! frame (gi element)
               (symbol->string (string->symbol (data (children element))))))
       ((body)
	(fset! frame 'mind-body (fsm-xml->blob base-directory element)))
       ((sqlite3)
	(fset! frame 'sqlite3 (fsm-xml->blob base-directory element)))
       ((properties)
        (fset! frame 'dav:properties (document-element (children element))))
       ((links)
        (fset!
         frame 'mind-links
         (let ((base-name (attribute-string 'about desc)))
           (node-list-reduce
            (select-elements
             (children (select-elements (children element) 'Bag))
             'li)
            (lambda (links link)
              (let ((resource-name (attribute-string 'resource link))
		    (href (attribute-string 'href link)))
		(link-bind!
		 links resource-name
		 (cond
		  (href (string->oid href))
		  ((node-list-empty? (children link))
		   ;; get it from file
		   ;; KLUDGE using document element here may be logically wrong.
		   ;; However synchronisation issues arose whithout it.
		   (document-element
		    (xml-parse
		     (file->string (make-fsm-component
				    base-directory
				    base-name
				    'link 'current
				    (uri-quote resource-name))))))
		  ((not (ormap xml-element? (children link)))
		   (data link))
		  (else (children link)))))
              links)
            (make-mind-links)))))
       (else
        (cond
         ((and (eq? (gi element) 'Bag)
               (equal? (attribute-string 'id element) "links"))
	  ;; (logerr "WARNING supposedly unused link bag format found ~a\n" (aggregate-entity frame))
          (fset!
           frame 'mind-links
           (let ((base-name (attribute-string 'about desc)))
             (node-list-reduce
              (children element)
              (lambda (links link)
                (if (gi link)
                    (let ((resource-name (attribute-string 'resource link)))
                      (link-bind!
                       links resource-name
                       (cond
                        ((node-list-empty? (children link))
                         ;; get is from file
                         (xml-parse
                          (file->string (make-fsm-component
                                         base-directory
                                         base-name
                                         'link 'current
                                         (uri-quote resource-name)))))
                        ((node-list-empty? (node-list-rest (children link)))
                         (if (xml-literal? (node-list-first (children link)))
                             (string->oid (data link))
                             (node-list-first (children link))))
                        (else (children link))))))
                links)
              (make-mind-links)))))
         ((gi element) (fset! frame (gi element) (children element)))
         (else #f)))))
   (children desc))
  frame)

;;*** Example Commit

;; There's an API when commiting places into the backing store systems
;; (which are intended to be distributed storage systems like freenet,
;; publius etc.).

;; This example takes the frame and writes it into a file system mirror
;; of the repository.  Here we decide how the repository looks alike.
;; We could save that alltogether, just keeping everything in the
;; persistent store.  A backup into the filesystem is the basic case.
;; Future extensions will stick encryption and distributed file
;; sharing systems in here.

(define (nufsm-write-meta base-directory identifier frame)
  (if ($fsm-precise) (future-write-sql identifier frame '()))
  (with-output-to-file
      (make-fsm-component base-directory identifier 'rdf 'next)
    (lambda () (write-rdf base-directory identifier frame #t))))

(define (nufsm-change-meta base-directory identifier frame new-links)
  (if ($fsm-precise) (future-write-sql identifier frame new-links))
  (with-output-to-file
      (make-fsm-component base-directory identifier 'rdf 'next)
    (lambda () (write-rdf base-directory identifier frame new-links))))

(define link-or-copy
  (let ((style 'hard))
    (define (doit src dst)
      (case style
	((hard)
	 (if (fx< (link src dst) 0)
	     (begin (set! style 'symbolic) (doit src dst))))
	((symbolic)
	 (guard
	  (ex (else (set! style 'copy) (doit src dst)))
	  (create-symbolic-link src dst)
	  (if (not (file-exists? dst))
	      (begin
		(set! style 'copy) (doit src dst)))))
	((copy)
	 (let ((data (filedata src)))
	   ;; FIXME Check result!
	   (call-with-output-file dst (lambda (p) (display data p)))))))
    doit))

(define (nufsm-link-basis base-directory identifier actual)
  (if (not (eq? identifier actual))
      (for-each
       (lambda (component)
         (let ((cf (make-fsm-component base-directory
                                       actual component 'current))
               (nf (make-fsm-component base-directory
                                       identifier component 'current))
               (of (make-fsm-component base-directory
                                       identifier component 'old)))
           (mkdirs (dirname nf))
           (bind-exit
            (lambda (return)
              (guard
               (ex (else (logerr "return ~a ~a\n" ex ex)(return #f)))
               (if (file-exists? nf)
                   (guard
                    (ex (else
                         (logerr "failed to rename ~a to ~a.  Abort.\n")
                         (return #f)))
                    (rename-file nf of)))
               (if (file-exists? cf) (link-or-copy cf nf))
	       (let ((frame (aggregate-meta (find-local-frame-by-id actual 'nufsm-link-basis))))
		 (force (future-write-sql
			 identifier
			 (lambda (key)
			   (let ((v (if (symbol? key)
					(get-slot frame key)
					(frame-resolve-link frame key #f))))
			     (values v v 'update)))
			 #t)))
               (if (file-exists? of) (remove-file of)))))))
       '(rdf body))))

(define (well-know-of identifier)
  (cond
   ((eq? identifier (my-oid)) null-oid)
   ((eq? identifier (public-oid)) one-oid)
   (else identifier)))

(define (nufsm-commit-changes base-directory
                              identifier frame new-links)  ; EXPORT
  (guard
   (c (else (call-with-values
             (lambda () (condition->fields c))
             (lambda (title msg args rest)
               (logcond 
		(string-append 
		 " nufsm-commit " 
		 (if (string? identifier) identifier (oid->string identifier)))
                        title msg args)
               (error c)))))
   (receive
    (last body c-c) (frame 'sqlite3)
    (if (vector? body) (set! body (vector-ref body 0)))
    (if (a:blob? body)
        (aggregate-check-blob2!
         (frame 'replicates) identifier (frame 'mind-action-document) body)))
   (receive
    (last data data-change) (frame 'mind-body)
    (let ((file (make-fsm-component base-directory
                                    identifier 'body 'next))
	  (rdf-next (make-fsm-component base-directory
					identifier 'rdf 'next))
          (l (and (string? data) (string-length data))))
      (mkdirs (dirname file))
      (make-sure-string-is-there data l)
      (if (and data-change data)
          (if (a:blob? data)
	      (aggregate-check-blob2!
               (frame 'replicates) identifier (frame 'mind-action-document) data)
	      (call-with-output-file file (lambda (port) (display data port)))))
      (nufsm-change-meta base-directory identifier frame new-links)
      (if (a:blob? data)
	  (let ((current (make-fsm-component base-directory
					     identifier 'body 'current)))
	    (if (file-exists? current) (remove-file current)))
	  (if (and data-change data)
	      (rename-file file (make-fsm-component base-directory
						    identifier 'body 'current))))
      (rename-file rdf-next
                   (make-fsm-component base-directory
                                       identifier 'rdf 'current))
      (nufsm-link-basis base-directory
                        (well-know-of identifier) identifier)))))

(define (nufsm-write base-directory identifier frame)  ; EXPORT
  (guard
   (c (else (receive (title msg args rest) (condition->fields c)
                     (logcond (string-append
                               " nufsm-write "
                               (if (string? identifier) identifier
                                   (symbol->string identifier)))
                              title msg args)
                     (error c))))
   (let ((data (frame 'mind-body))
	 (file (make-fsm-component base-directory identifier 'body 'next)))
     (mkdirs (dirname file))
     (cond
      ((string? data)
       (make-sure-string-is-there data (string-length data))
       (call-with-output-file file  (lambda (port) (display data port))))
      ((a:blob? data)
       (aggregate-check-blob2!
	(frame 'replicates) identifier (frame 'mind-action-document) data)))
     (nufsm-write-meta base-directory identifier frame)
     (if (string? data)
	 (rename-file file (make-fsm-component base-directory
                                               identifier 'body 'current)))
     (rename-file (make-fsm-component base-directory
                                      identifier 'rdf 'next)
                  (make-fsm-component base-directory
                                      identifier 'rdf 'current))
     (nufsm-link-basis base-directory
                       (well-know-of identifier) identifier))))

(define (nufsm-read base-directory identifier) ; EXPORT
  (let loop ((file-id (oid->string identifier)))
    (let ((metafile (make-fsm-component base-directory
                                        file-id 'rdf 'current)))
      (if (file-exists? metafile)
          (let* ((metadata (xml-parse (file->string metafile)))
                 (desc (document-element
                        (select-elements (children metadata) 'Description)))
                 (oidstr (attribute-string 'about desc)))
            (if (string=? oidstr file-id)
                (let* ((datafile
			(make-fsm-component base-directory
					    file-id 'body 'current))
		       (oid (string->oid oidstr))
		       (frame (aggregate (make-message) oid))) ; should be make-empty-frame
                  (read-rdf! base-directory frame desc)
                  (if (file-exists? datafile)
                      (fset! frame 'mind-body (file->string datafile)))
		  (let ((data (fget frame 'mind-body)))
		    (if (a:blob? data)
			(guard (ex (else #f))
			       (aggregate-check-blob! frame data))))
                  frame)
                (loop oidstr)))
          #f))))

;; dump and restore
;;
;; It's not sure that these should live here at all.

(define (nufsm-commit-all pool base-directory)
  (guard
   (c (else (log-condition "nufsm-dump-i exception" c)))
   (pool-fold!
    pool
    (lambda (identifier frame init)
      (if (not (or (oid=? identifier null-oid)
                   (oid=? identifier one-oid)))
          (nufsm-write base-directory
                       identifier
                       (make-look-ahead-reader
                        #f (aggregate-meta frame) (aggregate-meta frame) '()))
          (begin
            (nufsm-write base-directory
                         (aggregate-entity frame)
                         (make-look-ahead-reader
                          #f (aggregate-meta frame) (aggregate-meta frame) '()))
            (nufsm-link-basis base-directory
                              identifier (aggregate-entity frame))))
      (add1 init))
    0)
   (logerr "nufsm-commit-all done.\n")
   base-directory))

(define (nufsm-dump nufsm-base)
  (!apply nufsm-commit-all (list 1 nufsm-base)))
