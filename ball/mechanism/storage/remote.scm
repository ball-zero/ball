;; (C) 2000-2003, 2006, 2009, 2010, 2011 Jörg F. Wittenberger see http://www.askemos.org

(define fsm-remote-timeout (make-shared-parameter #f))

(define (make-http-error frame)
  (format "~a" frame))

;; There are currently two types of replies possible.  The old ad hoc
;; replication, which is a multipart message with the meta data in the
;; first part and the body in the second, otherwise the metainfo is
;; the only element.

;;* Single Peer Correspondence

;;** Replicate

;;*** BLOB's
;(: http-store-response-blob! (:stores: :oid: :blob: :ip-addr: :message: -> undefined))
(: http-store-response-blob! (* :oid: :blob: :ip-addr: :message: -> undefined))
(define (http-store-response-blob! stores identifier blob from answer)
  (if (frame? answer)
      (let ((status (get-slot answer 'http-status)))
	(case status
	  ((200)
	   (let ((body (get-slot answer 'mind-body)))
	     (if (and (string? body)
		      (let ((checksum (sha256-digest body)))
			(or (equal? checksum (symbol->string (blob-sha256 blob)))
			    (begin
			      (logerr
			       "replication of body of ~a from ~a failed exptected checksum ~a received ~a ~a"
			       identifier from (blob-sha256 blob)
			       checksum
			       (substring body 0 (min (string-length body) 50)))
			      #f))))
		 (begin
		   (if (not (a:blob-size blob)) (set-blob-size! blob (string-length body)))
		   (store-blob-value stores blob body)
		   ;; TODO: rethink: this appears to lead into an endless loop.  Needed at all?
		   ;;
		   ;; (if (and-let* ((bpb (blob-blocks-per-blob blob))) (fx>= bpb 2))
		   ;;     (!order
		   ;; 	(and-let* ((frame (find-local-frame-by-id identifier 'http-store-response-blob!)))
		   ;; 		  (restore-blob-info! (spool-directory) frame 'any blob))
		   ;; 	(blob-sha256 blob)))
		   ))))
	  ((404 503 504)
	   (logagree "Q host ~a replication status ~a for ~a body ~a\n"
		     from status identifier (blob-sha256 blob)))
	  (else
	   (logerr "host ~a replication status ~a for ~a body ~a\n"
		   from status identifier (blob-sha256 blob))
	   (if (eqv? status 400)
	       (logerr "~a Body:\n~a\n" from (message-body/plain answer))))))
      (logerr "host ~a replication ~a for ~a body ~a\n"
	      from answer identifier (blob-sha256 blob))))

(: http-replicate-blob! (:oid: :blob: :ip-addr: -> *))
(define (http-replicate-blob! identifier blob from)
  (or (blob-notation-size *the-registered-stores* blob #f)
      (begin
	(http-store-response-blob!
	 *the-registered-stores* identifier blob from
	 (http-get-blob identifier (blob-sha256 blob) from))
	(blob-notation-size *the-registered-stores* blob #f))))

;;* Quorum Correspondence

;;** See (Idempotent Operations)

;;*** Aggreed State

(define (http-rerequest-digests! host addr)
  ;; A dummy at this time.
  #t)

;; Moni Naor and Udi Wieder in their 2003 paper "A Simple Fault
;; Tolerant Distributed Hash Table" gave the lookup scheme a name:
;; "spam resistant lookup".

(define (http-find0 quorum identifier required local-result version)
  (let* ((others (hosts-lookup-at-least (quorum-others quorum) required))
	 (local-result (force local-result))
	 (state
	  (or
	   (and (fx< (length others) required)
		(raise-service-unavailable-condition
		 (format "http-find ~a ~a" identifier others)))
	   (!mapfold
	    ;; mapf
	    (lambda (voter)
	      (http-get-digest voter identifier version))
	    ;; foldf
	    (lambda (n raw state)
	      (if (frame? raw)
		  (let ((who (get-slot raw 'host))
			(answers (vector-ref state 1))
			(dig (get-slot raw 'mind-body)))
		    (let ((count (add1 (count-keys equal? dig answers))))
		      (logagree "Q ~a host ~a ~a\n" identifier who dig)
		      (vector-set! state 1 `((,dig . ,who) . ,answers))
		      (if (fx>= count required)
			  (begin
			    (vector-set! state 0 dig)
			    (values 0 state))
			  (values required state))))
		  (begin
		    (logerr "http-find: fold function received ~a\n" raw)
		    (values required state))))
	    ;; init
	    (vector
	     local-result
	     (if local-result (list (cons local-result (local-id))) '()))
	    ;; arguments
	    others
	    count: required
	    timeout: (respond-timeout-interval)
	    fold-handler: thread-signal-timeout!
	    compound-condition:
	    (lambda conditions
	      (make-condition
	       &service-unavailable
	       'message (format "~a ~a"
				identifier
				(srfi:string-join
				 (map condition->string conditions)
				 ", "))
	       'status 503))))))
    (values (vector-ref state 0)
	    (filter (lambda (x) (equal? (car x) (vector-ref state 0))) (vector-ref state 1)))))

(: http-find
   (:quorum:
    :oid: fixnum * (or false :frame-version-spec:) fixnum
    -> :hash: (list-of :place-state-on-host:)))
(define (http-find quorum identifier required local-result version retry)

  ;; Strange things have been seen in the logfile.  Try to dig it out.
  (let ((ought (simple-majority quorum (if (quorum-local? quorum) 0 1))))
    (if (and required (< required ought))
	(logerr "Q Find ~a on only ~a instead of ~a\n" identifier required ought))
    (set! required (or required ought)))

  (if (>= 0 required) (values (force local-result) '())
      (if (and retry (> retry 0))
	  (let loop ((retry retry))
	    (guard
	     (ex ((service-unavailable-condition? ex)
		  (if (positive? retry)
		      (begin (thread-sleep! (floor (/ (or (respond-timeout-interval) 2) retry)))
			     (loop (sub1 retry)))
		      (raise ex)))
		 (else (raise ex)))
	     (http-find0 quorum identifier required local-result version)))
	  (http-find0 quorum identifier required local-result version))))


;;** Replicate ("follow", "reverse 'POST')

;; TODO, make this a parameter or something other regular and integrated.
;; (I'm pretty sure this functionality will stay as a debugging aid.)

(define log-replication-failure
  (let ((level (the boolean #t)))
    (lambda v (if (pair? v) (set! level (car v))) level)))

(define (default-check-replicate-reply checksum frame rdf)
  (let* ((expected
	  (cond
	   ((quorum-local? (replicates-of frame))
	    (frame-signature frame))
	   ((and-let* ((ad (fget frame 'mind-action-document))
		       ((or (eq? (aggregate-entity frame) ad)
			    (and (memq ad (quorum-others (replicates-of frame)))
				 (public-equivalent? ad (replicates-of frame)))))
		       (p (fget frame 'protection)))
		      ((make-service-level p (public-capabilities))
		       (my-oid)))
	    (frame-signature frame))
	   (else rdf)))
	 (received (xml-digest-simple expected)))
    (values expected received)))

(define $check-replicate-reply (make-shared-parameter default-check-replicate-reply))

(define (replicate-place/aux! identifier frame)
  (if (want-body-local? frame)
      (begin
	(and-let* ((blob (fget frame 'sqlite3))
		   ((a:blob? blob)))
		  (aggregate-check-blob! frame blob))
	(and-let* ((body (fget frame 'mind-body))
		   ((a:blob? body)))
		  (aggregate-check-blob! frame body))))
  (let ((links (fget frame 'mind-links)))
    (if (and links
	     (or (want-links-local? frame)
		 (eq? identifier null-oid) (eq? identifier (my-oid))))
	(!mapfold
	 resync-now! (lambda (n v i) (values n (cons v i))) '()
	 links
	 map: (lambda (f table)
		(fold-links (lambda (k v init)
			      (if (oid? v) (cons (f v) init) init))
			    '() table))
	 fold-handler: #t
	 count: 0))
    frame))

;; Under strict replication (which SHOULD be the default), never go
;; back.  Non-strict rules will allow to go back one version in case
;; of a tie.

(define $relaxed-replication (make-shared-parameter 0))

(: http-replication-acceptable?
   (:oid: :place: (or false :frame-version-spec:) --> boolean))
(define (http-replication-acceptable? identifier frame version)
  (or (not version)
      (let ((fv (fget frame 'version)))
	(or
	 (not fv)
	 (let ((local (find-local-frame-by-id identifier 'http-check-again)))
	   (or (not local)
	       (not (quorum-local? (replicates-of local)))
	       (let ((version (let ((current-version (fget local 'version)))
				(if (fx< (version-serial version) (version-serial current-version))
				    (begin
				      (logerr "Q stale replication requested already at ~a requested ~a \n"
					      identifier (version-serial current-version) (version-serial version))
				      current-version)
				    version))))
		 (or
		  (and (fx>= (version-serial fv) (version-serial version))
		       (not (equal? fv version)))
		  ;; Relaxed rule: allow to go one version back.
		  (and-let*
		   ((relax ($relaxed-replication))
		    ((fx< 0 relax))
		    (v (fget local 'last-modified))
		    (tmo ($broadcast-timeout))
		    (rty ($broadcast-retries))
		    (tmz (fsm-remote-timeout))
		    (limit (add-duration *system-time* (make-time 'time-duration 0 (+ tmz (* tmo rty)))))
		    (stale (srfi19:time>=? limit (date->time-utc v)))
		    (version (fget local 'version))
		    ((fx>= (version-serial fv) (fx- (version-serial version) relax))))
		   (logerr "Q relaxed replication ~a from ~a to ~a after ~a seconds past limit\n" identifier version fv (time-second (time-difference limit (date->time-utc v))))
		   #t)))))))))

(define (http-replicate-place!/verify retry required identifier from checksum version rdf frame)
  (receive
   (expected received)
   (($check-replicate-reply) checksum frame rdf)
   ;; Verify
   (if (and (or (not checksum) (string=? checksum received))
	    (frame-oid-must-match? (aggregate-meta frame) identifier))
       (begin
	 (commit-frame! #f frame (aggregate-meta frame) '())
	 (replicate-place/aux! identifier frame)
	 (logagree "received ~a version ~a\n" identifier (fget frame 'version))
	 frame)
       (let loop ((retry retry))
	 (if (enable-warnings)
	     (logagree "replication of ~a from ~a failed. cs ~a got ~a:\n~a\n"
		       identifier from checksum received (xml-format expected))
	     (logagree "replication of ~a from ~a failed. cs ~a got ~a.\n"
		       identifier from checksum received))
	 (receive
	  (result answers)
	  (http-find (replicates-of frame) identifier required checksum version retry)
	  (if result
	      (if (string=? received result)
		  (begin
		    (replicate-place/aux! identifier frame)
		    (logagree "receive ~a version ~a\n"
			      identifier (fget frame 'version))
		    frame)
		  (cond
		   ((and ($relaxed-replication) (eqv? retry 1))
		    (parameterize
		     (($relaxed-replication 1))
		     (http-replicate-place-from-answers!
		      0 required answers identifier result version)))
		   ((positive? retry)
		    (http-replicate-place-from-answers!
		     (sub1 retry)
		     required answers identifier result version))
		   (else
		    (raise
		     (make-object-not-available-condition
		      (oid->string identifier)
		      (format "replication of ~a from ~a failed. cs ~a got ~a"
			      identifier from checksum received))))))
	      (raise 'http-replicate-place:illegal-result)))))))

(define (rfc2046->list body boundary)
  (map
   (lambda (part)
     (let* ((msg (call-with-input-string
		  part
		  (lambda (port)
		    (http-read-content
		     port #t ($small-request-limit) ($large-request-handler))))))
       (let ((b (get-slot msg 'mind-body)))
	 ;; Safety check: If there's no mind-body we had
	 ;; no content-length in http-read-content.
	 (if b b
	     (let ((b (substring
		       part (message-body-offset part)
		       (string-length part))))
	       b)))))
   (rfc2046-split body boundary)))


(define-condition-type &rejected-replica &condition rejected-replica-condition?)

(define *rejected-replica* (make-condition &rejected-replica))

(define (http-replicate-place!/to-frame identifier from version rdf)
  (let ((meta ((sxpath '(Description)) rdf)))
    (always-assert (eq? (gi rdf) 'RDF)
		   "http-replicate-place! from ~a illegal meta ~a"
		   from (gi meta))
    (let ((frame (read-rdf! (spool-directory) (aggregate (make-message) identifier) meta)))
      (if (http-replication-acceptable? identifier frame version)
	  frame
	  (begin
	    (logagree "Q http-replicate-place! ~a local version ~a reject version ~a from ~a"
		      identifier version (fget frame 'version) from)
	    (raise *rejected-replica*)))
      frame)))

(define (http-replicate-place!/multipart
	 retry required identifier from checksum version replic boundary)
  (let* ((reply (rfc2046->list (get-slot replic 'mind-body) boundary))
	(rdf (document-element (xml-parse (car reply)))))
    (let ((frame (http-replicate-place!/to-frame identifier from version rdf)))
      (assert (pair? (cdr reply)))
      (fset! frame 'mind-body (cadr reply))
      (http-replicate-place!/verify
       retry required identifier from checksum version rdf frame))))

(: http-replicate-place!
   (fixnum
    fixnum
    :oid:
    :ip-addr:
    :hash:
    (or false :frame-version-spec:)
    -> :place:))
(define (http-replicate-place! retry required identifier from checksum version)
  (let* ((replic (http-get-place identifier from))
         (status (let ((status (get-slot replic 'http-status)))
		   (always-assert
		    (eqv? status 200)
		    "~a replication error status ~a" identifier status)
		   status))
         (boundary (multipart-data-boundary (get-slot replic 'content-type))))
    (if boundary
	(http-replicate-place!/multipart
	 retry required identifier from checksum version replic boundary)
	(let ((rdf (document-element (message-body replic))))
	  (http-replicate-place!/verify
	   retry required identifier from checksum version rdf
	   (http-replicate-place!/to-frame identifier from version rdf))))))

(: http-replicate-place-from-answers!
   (fixnum
    fixnum
    (list-of :place-state-on-host:)
    :oid:
    :hash:
    (or false :frame-version-spec:)
    -> :place-or-not:))
(define (http-replicate-place-from-answers! retry required
					    answers identifier checksum version)
  (ormap
   (lambda (answer)
     (guard
      (exception
       ((rejected-replica-condition? exception) (raise exception))
       (else
	(log-condition (format "While replicating ~a version ~a"  identifier version)
		       exception)
	#f))
      (if (equal? (cdr answer) (local-id))
	  (find-local-frame-by-id identifier 'http-replicate-place-from-answers!)
	  (http-replicate-place!
	   retry required identifier (cdr answer) checksum version))))
   answers))

(define $max-replication-retries (make-shared-parameter 2))

(: http-shall-try? (:oid: --> boolean))
(define (http-shall-try? identifier)
  (let ((missing (sql-oid-missing identifier)))
    (and
     (not (negative? missing))
     (> (time-second *system-time*) missing))))

(: http-read! (:quorum: :oid: &optional fixnum -> :place-or-not:))
(define (http-read! quorum identifier . required)
  (and
   (quorum? quorum)
   (pair? (quorum-others quorum))
   ;; (http-shall-try? identifier)
   (guard
    (x ((service-unavailable-condition? x)
	(sql-set-missing-oid! identifier 1)
	(set-missing-oid! identifier)
	(logagree "Q ~a service unavailable ~a\n" identifier (quorum-others quorum)))
       ((object-not-available-condition? x)
	(sql-set-missing-oid! identifier 2)
	(set-missing-oid! identifier)
	(logagree "Q ~a read failed on ~a\n" identifier (quorum-others quorum))
	#f)
       (else (receive (title msg args rest) (condition->fields x)
                      (logcond 
		       (string-append 
			" http-read! " 
			(oid->string identifier))
		       title msg args))
             #f))
    (let ((local (and (quorum-local? quorum)
		      (delay
			(let ((obj (find-local-frame-by-id identifier 'http-read!)))
			  (and obj (xml-digest-simple (frame-signature obj)))))))
	  (required (or (and (pair? required) (car required))
			(simple-majority quorum  (if (quorum-local? quorum) 0 1)))))
      (receive
       (result answers)		    ; vvvvvvvv handle-invitation here?
       (http-find quorum identifier required local #f ($max-replication-retries))
       (logagree "Q Found ~a ~a (local ~a) ~a\n" identifier result (force local) answers)
       (or (and (equal? result (force local))
		(find-local-frame-by-id identifier 'http-read!))
	   (http-replicate-place-from-answers! ($max-replication-retries) required
					       answers identifier result #f)
	   (raise (make-object-not-available-condition
		   (oid->string identifier) (format "replication of ~a failed" identifier)))))))))

(: http-find-quorum-on-others (:quorum: :oid: -> (or :quorum: false)))
(define (http-find-quorum-on-others initial-quorum identifier)
  ;; FIXME this needs to become a mini http-check! where *only* the
  ;; quorum is beeing checked for consitency.  But the check MUST be
  ;; safe against byzantine failures.  As of now one (remote) site can
  ;; hijack a remote quorum.
  (let loop ((from (quorum-others initial-quorum))
	     (turn '(nowait)))
    (cond
     ((pair? from)
      (or (and-let* ((answer (guard
			      (ex (else #f))
			      (and-let*
			       ((c (or (apply (host-lookup) (car from) turn)
				       (and (not (oid? (car from)))
					    (car from))))
				(((host-alive?) c)))
			       (within-time
				(fsm-remote-timeout)
				(http-get-meta identifier c)))))
		     ((frame? answer))
		     (status (get-slot answer 'http-status))
		     ((or (eqv? status 200)
			  (eqv? status 400) ; FIXME bug compatibility
			  (eqv? status 404) ; not found
			  (eqv? status 408) ; timeout
			  (eqv? status 503) ; service not available
			  (logagree "replication error host ~a status ~a\n" (car from) status)))
		     ((eqv? status 200))
		     (rdf (document-element (message-body answer)))
		     (q ((sxpath '(Description replicates)) rdf))
		     ((not (node-list-empty? q))))
		    (make-quorum q))
	  (loop (cdr from) turn)))
     ((pair? turn) (loop (quorum-others initial-quorum) '()))
     (else
      ;; (sql-set-missing-oid! identifier 1)
      #f))))

(: http-find-quorum (:quorum: (:oid: -> :place-or-not:) :oid: -> :quorum:))
(define (http-find-quorum initial-quorum find-frame oid)
  ;; sollten wir besser den Slot direkt prüfen und den localen Default
  ;; durch ein http-find-quorum-on-others verifizieren?
  (or (and-let* ((frame (find-frame oid)))
		(let ((quorum (replicates-of frame)))
		  (and (quorum-local? quorum) quorum)))
      (http-find-quorum-on-others initial-quorum oid)
      (error (format "http-find-quorum ~a no quorum found in ~a" oid (quorum-others initial-quorum)))))

(: http-check! (:quorum: :oid: &optional fixnum -> :place-or-not:))
(define (http-check! quorum identifier . required)
  (and
   (public-oid)
   (quorum? quorum)
   (or (http-shall-try? identifier) ;; maybe or maybe not that good
       (logerr #;logagree "Q NOT checking ~a, known miss. (Remove from 'missing' table in spool.db or remove the db).\n" identifier)
       #f)
   (guard
    (x ((object-not-available-condition? x)
	(set-missing-oid! identifier)
	(sql-set-missing-oid! identifier 2)
	(logagree "Q ~a missing on ~a\n" identifier (quorum-others quorum)) #f)
       ((service-unavailable-condition? x)
	(sql-set-missing-oid! identifier 1)
	(logagree "Q ~a service unavailable ~a\n" identifier (quorum-others quorum)) #f)
       ((timeout-condition? x)
	(sql-set-missing-oid! identifier 1)
	(set-missing-oid! identifier)
	(logagree "Q ~a timeout\n" identifier #f))
       ((message-condition? x)
	(logerr "Q check ~a\n" (condition-message x)) #f)
       (else (receive (title msg args rest) (condition->fields x)
		      (set-missing-oid! identifier)
		      (logcond 
		       (string-append "http-check! " (oid->string identifier))
		       title msg args))
	     #f))
    (let loop ((quorum quorum))
      (let ((local (and (quorum-local? quorum)
			(delay
			  (let ((obj (find-local-frame-by-id identifier 'http-check!)))
			    (and obj (xml-digest-simple (frame-signature obj)))))))
	    (required (or (and (pair? required) (car required))
			  (simple-majority quorum (if (quorum-local? quorum) 0 1)))))
	(receive
	 (result answers)
	 (let* ((obj (find-local-frame-by-id identifier 'http-check!)))
	   (http-find quorum identifier required local
		      (and obj (cddr (agreement-last-message obj)))
		      ($max-replication-retries)))
	 (cond
	  ((and-let*
	    (((string? result))
	     (obj (guard (exception (else #f))
			 (find-local-frame-by-id identifier 'http-check!))))
	    (string=? result
		      (xml-digest-simple (frame-signature obj))))
	   (find-local-frame-by-id identifier 'http-check!))
	  (result
	   (let* ((obj (find-local-frame-by-id identifier 'http-check!))
		  (version (and obj (fget obj 'version))))
	     (logagree "Q sync ~a/~a local ~a from ~a.\n" identifier version (force local) answers)
	     (or (http-replicate-place-from-answers! ($max-replication-retries) required
						     answers identifier result version)
		 (begin
		   (set-missing-oid! identifier)
		   (raise
		    (make-object-not-available-condition
		     (oid->string identifier)
		     (format "replication of ~a failed" identifier)))))))
	  ((and (not (quorum-local? quorum))
		(and-let* ((q ((quorum-lookup) identifier quorum))
			   ((not (equal? (quorum-others quorum) (quorum-others q)))))
			  q))
	   => (lambda (quorum)
		(logagree "Q retry ~a on new remote quorum ~a\n"
			  identifier (quorum-others quorum))
		(loop quorum)))
	  (else (logerr "http-check! took wrong turn on ~a\n" identifier) #f))))))))

(: http-sync! (:quorum: :oid: -> :place-or-not:))
(define (http-sync! quorum identifier)
  (or (intrinsic-sync-locked identifier)
      (http-check! quorum identifier)))

(define http-storage-adaptor
  (make-storage-adaptor
   load: (lambda (q i)
	   (let ((frame (http-read!
			 (http-find-quorum *local-quorum* ;; (http-all-hosts)
					   find-already-loaded-frame-by-id
					   i)
			 i)))
	     (if frame (commit-frame! #f frame (aggregate-meta frame) '()))
	     frame))))
