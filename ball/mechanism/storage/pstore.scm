;; (C) 2000, 2001, 2003, 2006
;;
;; J�rg F. Wittenberger see http://www.askemos.org

(define-record-type <pstore-data>
  (make-pstore-data
   underlying-pstore
   blob-table)
  pstore-data?
  (underlying-pstore underlying-pstore)
  (blob-table blob-table set-blob-table!))

(define (volatile? obj)
  (allocation-area->store (object->allocation-area obj)))

(define *registered-classes*
  (vector <srfi:date>
	  <long-int>
	  <xml-document> <xml-element> <xml-attribute> <blob> <frame>
	  <quorum> <transaction>))

(define (pstore-register-class! . classes) ; EXPORT
  (let* ((l (vector-length *registered-classes*))
         (new (make-vector (+ l (length classes)))))
    (do ((i 0 (+ 1 i)))
        ((eqv? i l) #t)
      (vector-set! new i (vector-ref *registered-classes* i)))
    (do ((i l (+ 1 i))
         (classes classes (cdr classes)))
        ((null? classes) #t)
      (vector-set! new i (car classes)))
    (set! *registered-classes* new)))

; (pstore-register-class! <oid> oid->hash oid=?)

(define (database-files-of target-name)
  (values (string-append target-name ".v0") (string-append target-name ".v1")))

(define (open-persistent-store-on-lss base-name create-flag)
  (let* ((v (call-with-values (lambda () (database-files-of base-name))
              vector))
         (lss (if create-flag
                  (lss-create v)
                  (and (stat (vector-ref v 0)) (lss-open v))))
         (ps (and lss (open-pstore-on-lss lss))))
    (if ps
        (begin
          (if (not (eqv? (vector-length *registered-classes*) 0))
              (register-indirect-page ps 1 *registered-classes*))
          (start-online-compacter ps)))
    ps))

(define (create-persistent-store-on-lss base-name)
  (open-persistent-store-on-lss base-name #t))

(define (pstore-open-mind mind-file-name . create) ; EXPORT
  (handler-case
   (let ((ps (or (open-persistent-store-on-lss mind-file-name #f)
		 (and
		  (not (null? create))
		  (let ((ps (create-persistent-store-on-lss mind-file-name)))
		    (commit ps `((blob-table . ,(make-symbol-table))
				 . ,((car create))))
		    ps)))))
     (make-pstore-data ps (cdrassq 'blob-table (root-object ps))))
   ((<condition> condition: c)
    (logerr "pstore-open-mind ~a: ~a" mind-file-name c)
    (process-exit 1))))

;; Caution: This is not thread safe.  It must be run from the
;; gatekeeper suspending the other threads.

(define (pstore-commit-into mind-file-name mind)
  (let* ((mind-directory (pathname->os-path
                          (file-directory (string->file mind-file-name))))
         (cs (/ (stat-size (stat mind-directory)) 1024))
         (fs (filesystem-free mind-directory)))
    (if (>= cs fs)
        (error "pstore: Not enough space left on device.
Free: ~a current data base size: ~a.\n" fs cs))
    (let ((target (create-persistent-store-on-lss mind-file-name)))
      (commit target (root-object (underlying-pstore mind)))
      (let ((r (root-object target)))
        (for-each (lambda (f) (if (not (f r))
                                  (error "pstore: bad new root ~a\n"
                                         r)))
                  (list (lambda (r) (eq? (caar r) 'use-pool))
                        (lambda (r) (table? (cdar r)))))
        (make-pstore-data
	 target (cdr (assq 'blob-table (root-object target))))))))

(define (pstore-close mind)
  ;; avoid gc crawling the persitant area, which is to be
  ;; invalidated now.
  (logerr ".")
  (let loop ()
    (if (handler-case
         (begin
           (synchronize-with-finalization)      ; contains required (gc-now)
           (logerr ".")
           (close-persistent-store (underlying-pstore mind))
           #f)
         ((<condition> condition: c)
          (logerr "pstore-close ~a" c)
          (process-exit 1)
          #t))
        (loop)))
  (logerr "repository closed, going down anyway\n")
  (process-exit 1))

(define (logged-commit store)
  ;; FIXME KLUDGE we increase the priority value from the default to
  ;; give other threads a better chance.  The pstore commit operation
  ;; is too slow and eats more than a time slice per run.
  (set-thread-priority! (current-thread) 7)
  (logerr "pstore commit\n")
  (let ((result (commit (underlying-pstore store))))
    (logerr "pstore commit no ~a\n" result)
    result))

(define $pstore-max-blob-size #f)

(define (pstore-store-blob-notation store blob key value)
  (let ((v (or (table-lookup (blob-table store) (blob-sha256 blob))
	       (let ((v (list
			 (cons key 
			       (if (and $pstore-max-blob-size (not key) (string? value)
					(fx>= (string-length value) $pstore-max-blob-size))
				   #f value)))))
		 (table-insert! (blob-table store) (blob-sha256 blob) v) v))))
    (cond
     ((not key) (cdar v))
     ((assoc key (cdr v)) => cdr)
     (else (let ((v (cons* (car v) (cons key value) (cdr v))))
	     (table-insert! (blob-table store) (blob-sha256 blob) v) v)))))

(define (pstore-store-blob-value store blob (octet-stream <string>))
  (let* ((key (blob-sha256 blob))
	 (v (table-lookup (blob-table store) key)))
    (if v
	(caar v)
	(pstore-store-blob-notation store key #f octet-stream))))

(define (pstore-fetch-blob-notation store blob key)
  (cond
   ((table-lookup (blob-table store) (blob-sha256 blob)) =>
    (lambda (a) (cdrassq key a)))
   (else #f)))

;; TODO: since we require the underlying scheme implementation to
;; provide some weak pointer abstraction, the blob garbage collection
;; is dulicate effort and should be removed.

(define (pstore-gc ps)
  (let ((garbage (make-symbol-table))
	(pool (cdrassq 'use-pool (root-object (underlying-pstore ps)))))
    (table-fold (lambda (k i)
		  (table-insert! garbage k #t) i)
		#f (blob-table ps))
    (table-fold
     (lambda (k i)
       (and-let* ((v (table-lookup pool k))
		  (b (get-slot (aggregate-meta v) 'mind-body))
		  ((a:blob? b)))
		 (table-remove! garbage (blob-sha256 b))))
     #f
     pool)
    (let ((bt (blob-table ps)))
      (table-fold
       (lambda (k i) (if (non-transient-blob? k) (table-remove! bt k)))
       #f
       garbage))
    pool))

(define pstore-storage-adaptor
  (make-storage-adaptor open:        pstore-open-mind
                        commit:      logged-commit
                        commit-into: pstore-commit-into
                        close:       pstore-close
			store-blob:  pstore-store-blob-value
			store-blob-notation: pstore-store-blob-notation
			fetch-blob-notation: pstore-fetch-blob-notation
			gc: pstore-gc))

;; TODO move nice into (r)scheme

(define-safe-glue (nice (i <raw-int>))
{
 extern int nice(int); /* #include <unistd.h> */
 if( nice((int) i) ) os_error("nice", 0);
 RETURN0();
})

;; This more or less dances around the API as it developed under the
;; assumtion that a commit might copy the root-object into a different
;; memory region.  Need to review whether we still need that.

(define (compact-database set-working-mind! mind)
  (set-working-mind! (root-object (underlying-pstore mind)))
  (values mind (root-object (underlying-pstore mind))))
