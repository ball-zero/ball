(cond-expand
 (chicken
  (define-syntax logfsm
    (syntax-rules ()
      ((_ fmt ...) (if ($fsm-verbose) (logerr fmt ...)))))
  (define-inline (make-sure-string-is-there data l) #f))
 (else
  (define-macro (logfsm fmt . rest)
  `(and ($fsm-verbose) (logerr ,fmt . ,rest)))

  (define-macro (make-sure-string-is-there data l)
    ;; rscheme/pstore special KLUDGE: if that's a long string, which
    ;; comes from pstore, it might be not present.  Than write fails
    ;; with a bad address exception.  We prevent that for the reload by
    ;; touching every page.
    ;;
    ;; As far as I recall, this has been moved into rscheme.
    ;; 
    ;;   `(if ,l (do ((i 0 (+ i 4096)))
    ;;               ((>= i ,l) (and (> ,l 0) (string-ref ,data (sub1 ,l))))
    ;;            (string-ref ,data i)))
    '(begin))))
