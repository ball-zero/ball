(define use-sql-store (make-shared-parameter #t))

(define-record-type <lockable-db>
  (%make-lockable-db file mutex connection)
  lockable-db?
  (file lockable-db-file %set-lockable-db-file!)
  (mutex lockable-db-mutex)
  (connection lockable-db-connection %set-lockable-db-connection!))

(define *spool-db-file* (xthe (or false :pathname:) #f))
(define *spool-db* (%make-lockable-db #f (make-mutex "*spool-db-file*") #f))

(define (init-spool-db!)
  (set! *spool-db-file* (make-pathname (or (spool-directory) '()) "spool" "db"))
  ;; FIXME: unsure whether we ought to remove the journal
  ;;  (let ((db-journal (make-pathname (or (spool-directory) '()) "spool" "db-journal")))
  ;;    (if (file-exists? db-journal) (remove-file db-journal)))
  (let ((exists (file-exists? *spool-db-file*)))
    (with-mutex
     (lockable-db-mutex *spool-db*)
     (if (not (lockable-db-connection *spool-db*))
	 (let ((db (sqlite3-open *spool-db-file*)))
	   (%set-lockable-db-connection! *spool-db* db)
	   (sqlite3-exec db "PRAGMA locking_mode = EXCLUSIVE--;PRAGMA journal_mode = WAL")
	   (%set-lockable-db-file! *spool-db* *spool-db-file*)
	   (if exists (!start (vacuum-spool-db!) *spool-db-file*)))))
    (if (not exists) (init-sql-core!))))

(define (close-spool-db!)
  (with-mutex
   (lockable-db-mutex *spool-db*)
   ;; do something to get rid of the journal
   (sqlite3-close (lockable-db-connection *spool-db*))
   (set! *spool-db-file* #f)
   (%set-lockable-db-connection! *spool-db* #f)))

(define (vacuum-spool-db!)
  (with-mutex
   (lockable-db-mutex *spool-db*)
   (logerr "Running SQLite 'VACUUM' on ~a - may block system for some time.\n" *spool-db-file*)
   (sqlite3-exec (lockable-db-connection *spool-db*) "VACUUM"))
  (logerr "SQLite 'VACUUM' on ~a completed.\n" *spool-db-file*))

(define (check-spool-db!)
  (if (not (lockable-db-file *spool-db*))
      (let loop ((n 20))
	(guard
	 (ex ((sqlite3-error-db-locked? ex)
	      (if (positive? n)
		  (loop (begin
			  (logerr "W: DB is locked ~a\n" *spool-db-file*)
			  (guard
			   (ex (else (log-condition 'close-spool-db ex)))
			   (sqlite3-close (lockable-db-connection *spool-db*)))
			  (%set-lockable-db-connection! *spool-db* #f)
			  (thread-sleep! 1)
			  (sub1 n)))
		  (begin (log-condition "EXIT: init-spool-db!" ex)
			 (exit 1))))
	     (else (raise ex)))
	 (init-spool-db!)))))

(define (db-set! db test set fail rest)
  (sqlite3-call-test/set
   (lockable-db-connection db)
   test set fail rest))

(define (with-db-set* db proc)
  (guard
    (ex ((abandoned-mutex-exception? ex)
	 (log-condition (format "with-db-set* ~a in ~a" (current-thread) proc) ex)
	 (raise ex))
	(else (raise ex)))
    (sqlite3-call-with-transaction
     (lockable-db-connection db)
     (lambda (sql) (proc sql)))))

(define (with-spool-db proc)
  (with-db-set* *spool-db* (lambda (sql) (proc sql))))

(define (spool-db-exec cmd . args)
  (with-db-set* *spool-db* (lambda (sql) (sql cmd))))

(define (spool-db-exec/prepared query . args)
  (with-db-set* *spool-db* (lambda (sql) (apply sql query args))))

(define (spool-db-select cmd . args)
  (let ((query (if (null? args) cmd (apply format cmd args))))
    (sqlite3-exec (lockable-db-connection *spool-db*) query)))

(define (spool-db-select/prepared cmd . args)
  (let ((query (sqlite3-prepare (lockable-db-connection *spool-db*) cmd)))
    (apply sqlite3-exec (lockable-db-connection *spool-db*) query args)))

(define (spool-db-set! test set . rest)
  (if (null? rest)
      (db-set! *spool-db* test set (lambda () #t) '())
      (db-set! *spool-db* test set (car rest) (cdr rest))))

#;(define (empty-select? query . rest)
  (lambda (sql) (eqv? (sql-ref (apply sql query rest) #f #f) 0)))

(define (empty-count-select? sql query . rest)
  (eqv? (sql-ref (apply sql query rest) 0 0) 0))

;; CoreLinks: s: source d: drain t: type (1: type 2: support 3: channel)

(define (init-sql-core!)
  (guard
   (ex (else (log-condition 'init-sql-core! ex)))
   (spool-db-set!
    (lambda (sql) #t)
    (lambda (sql)
      (sql "CREATE TABLE CoreLinks (s char(33) NOT NULL, d char(33) NOT NULL, t small)")
      (sql "CREATE INDEX CoreLinks_s ON CoreLinks(s)")
      (sql "CREATE INDEX CoreLinks_d ON CoreLinks(d)")
      ;;
      (sql "CREATE TABLE CoreBLOBs (oid char(33) NOT NULL, blob text NOT NULL)")
      (sql "CREATE INDEX CoreBLOBs_oid ON CoreBLOBs(oid)")
      (sql "CREATE INDEX CoreBLOBs_blob ON CoreBLOBs(blob)")
      (sql "CREATE TABLE REACHED (oid char(33) PRIMARY KEY NOT NULL)")
      (sql "create table missing (oid char(33) primary key not null, confirmed boolean default false)")
      (sql "CREATE TABLE FsmState (parameter small, value char(33))")
      )
    (lambda () #f)
    '())))


(define (sql-set-missing-oid! oid confirmed)
  (let ((confirmed (cond
		    ((eq? confirmed #f) 1)
		    ((number? confirmed)
		     (case confirmed
		       ((2) (+ (time-second *system-time*)
			       600))
		       ((1) (+ (* (or (respond-timeout-interval) 10) 4)
			       (time-second *system-time*)))
		       (else confirmed)))
		    (else (time-second *system-time*)))))
    (!start
     (with-spool-db
      (lambda (sql)
	(let ((val (oid->string oid)))
	  (sql "insert or ignore into missing(oid) values(?1)" val)
	  (sql "update missing set confirmed=?1 where oid=?2 and confirmed >= 0"
	       confirmed val))))
     'missing)))

(delete-is-sync-hook
 (let ((old (delete-is-sync-hook)))
   (lambda (oid)
     (old oid) (sql-set-missing-oid! oid 0))))

(define (sql-oid-missing oid)
  (let ((r (spool-db-select/prepared "select confirmed from missing where oid = ?1" (oid->string oid))))
    (if (eqv? (sql-ref r #f #f) 0)
	0
	(let ((v (sql-ref r 0 0)))
	  (cond
	   ((not v) 0)
	   ((number? v) v)
	   ((string? v) (if (string=? v "false") 0 1))
	   (else 0))))))

(define (write-blob-to-sql2 sql identifier last blob ins del c-c)
  (define literal-identifier (oid->string identifier))
  (if (or (pair? ins) (pair? del))
      (begin
	(for-each
	 (lambda (blob)
	   (let ((r (sql "select _rowid_ from CoreBLOBs where oid = ?1 and blob = ?2 limit 1" literal-identifier (symbol->string (blob-sha256 blob)))))
	     (if (fx>= (sql-ref r #f #f) 1)
		 (sql "delete from CoreBLOBs where oid = ?1 and blob = ?2"
		      literal-identifier (literal (blob-sha256 blob))))))
	 del)
	(for-each
	 (lambda (blob)
	   (sql "insert into CoreBLOBs (oid, blob) values (?1, ?2)" literal-identifier (literal (blob-sha256 blob))))
	 ins)))
  (cond
   ((and (eq? c-c 'update) (a:blob? last) (a:blob? blob))
    ;; TODO check first and don't uncoditionally overwrite
    (let ((r (sql "select _rowid_ from CoreBLOBs where oid = ?1 and blob = ?2 limit 1" literal-identifier (literal (blob-sha256 last)))))
      (if (fx>= (sql-ref r #f #f) 1)
	  (sql "DELETE FROM CoreBLOBs where _rowid_ = ?1" (sql-ref r 0 0)))
      (sql "insert into CoreBLOBs (oid, blob) values (?1, ?2)" literal-identifier (literal (blob-sha256 blob)))))
   ((and (or (eq? c-c 'delete) (eq? c-c 'update))
	 (a:blob? last))
    (sql "delete from CoreBLOBs where oid = ?1 and blob = ?1" literal-identifier (literal (blob-sha256 last))))
   ((and (or (eq? c-c 'update) (eq? c-c 'insert)) (a:blob? blob))
    (sql "insert into CoreBLOBs (oid, blob) values (?1, ?2)" literal-identifier (literal (blob-sha256 blob))))))

(define (wbt-sql/body-local? frame)
  (receive (l q c) (frame 'replicates)
	   (or (quorum-local? q)
	       (receive (l a c) (frame 'mind-action-document)
			(and a (public-equivalent? a))))))

(define (write-blob-to-sql sql identifier frame last blob c-c)
  (cond
   ((vector? blob)
    (write-blob-to-sql2 sql identifier last (vector-ref blob 0) (vector-ref blob 1) (vector-ref blob 2) c-c))
   ((not c-c))
   (else (write-blob-to-sql2 sql identifier last blob '() '() c-c)
	 (if (and (bhob? blob) (wbt-sql/body-local? frame))
	     (let ((last (and (not (and (eq? last blob) (eq? c-c 'update))) last)))
	       (!start
		(receive
		 (blob ins del)
		 (receive (last-replicates replicates c-c/r) (frame 'replicates)
			  (block-table-flush!
			   (place/make-block-table
			    identifier replicates blob)
			   (and last (place/make-block-table
				      identifier last-replicates last))))
		 (with-spool-db
		  (lambda (sql)
		    (write-blob-to-sql2 sql identifier last blob ins del 'update))))
		(blob-sha256 blob)))))))

(define (write-sql sql identifier frame new-links blobs)
  (define literal-identifier (literal identifier))
  (sql "INSERT OR IGNORE INTO REACHED (oid) VALUES(?1)" literal-identifier)
  (sql "DELETE FROM MISSING WHERE oid=?1" literal-identifier)
  (receive
   (o ad c-c) (frame 'mind-action-document)
   (if c-c
       (if (empty-count-select? sql "select count(*) from CoreLinks
where s = ?1 and d = ?2 and t=1" literal-identifier (literal ad))
	   (sql "insert into CoreLinks (s, d, t) values (?1, ?2, 1)" literal-identifier (literal ad)))))
  (receive
   (last replicates c-c) (frame 'replicates)
   (if c-c
       (begin
	 (if (quorum-local? replicates)
	     (sql
	      "DELETE FROM MISSING WHERE oid in (select d from CoreLinks where s=?1)" ;; and confirmed=-2 ?
	      literal-identifier))
	 (if (eq? c-c 'update)
	     (sql "delete from CoreLinks where s = ?1 and t=2" literal-identifier))
	 (if replicates
	     (for-each
	      (lambda (id)
		(sql "insert into CoreLinks (s, d, t) values (?1, ?2, 2)" literal-identifier (literal id)))
	      (quorum-others replicates))))))
  (for-each
   (lambda (b)
     (receive
      (last body c-c) (apply values (cdr b))
      (write-blob-to-sql sql identifier frame last body c-c)))
   blobs)
  (receive
   (last links changed) (frame 'mind-links)
   (if (and last (eq? changed 'update) (eq? last links))
       (links-for-each
	(lambda (key v)
	  (if (not (and (pair? new-links) (assoc key new-links)))
	      (receive
	       (last value change) (frame key)
	       (if (and (oid? value)
			(empty-count-select? sql "select count(*) from CoreLinks
where s = ?1 and d = ?2 and t=3" literal-identifier (literal value)))
		   (sql "insert into CoreLinks (s, d, t) values (?1, ?2, 3)" literal-identifier (literal value))))))
	last))
   (if (pair? new-links)
       (for-each
	(lambda (link)
	  (if (cdr link)
	      (sql "insert into CoreLinks (s, d, t) values (?1, ?2, 3)" literal-identifier (literal (cdr link)))
	      (and-let*
	       ((last)
		(ll (link-ref last (car link))))
	       (sql "delete from CoreLinks where s = ?1 and d = ?2 and t=3" literal-identifier (literal ll)))))
	new-links))))

(define $synchron-sql #f)

(define (future-write-sql identifier frame new-links)
  (let ((blobs (map (lambda (key)
		      (receive
		       (last body c-c) (frame key)
		       (list key last body c-c)))
		    '(sqlite3 mind-body))))
    ((if $synchron-sql force identity)
     (!start
      (begin
	(check-spool-db!)
	(with-spool-db (lambda (sql) (write-sql sql identifier frame new-links blobs))))
      identifier))))
