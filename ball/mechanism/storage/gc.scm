;; (C) 2000-2003, 2006, 2009 Jörg F. Wittenberger see http://www.askemos.org

;;*** garbage collector for file space

(define fsm-yield! (make-shared-parameter thread-yield!))

(define (fsm-yield-lower-priority!)
  (thread-sleep!/ms 100))

(define (fsm-get-meta-data base-directory from base)
  ;; return #f or `frame` - no exception
  (or (find-already-loaded-frame-by-id base)
      (let ((metafile (make-fsm-component base-directory base 'rdf 'current)))
        (and (file-exists? metafile)
             (let ((frame (aggregate (make-message) base)))
	       ;; File i/o might be slow anyway.  Lower our "priority":
	       ((fsm-yield!))
	       (guard
		(ex (else (log-condition metafile ex) #f))
		(read-rdf! base-directory frame
			   (document-element
			    (select-elements
			     (children (xml-parse (file->string metafile)))
			     'Description)))))))
      (and-let* (((oid? from))
		 (f (fsm-get-meta-data base-directory #f from))
		 ((eqv? (sql-oid-missing base) 0))
		 (x (!apply quorum-get! (list (replicates-of f) base)))
		 (q (replicates-of x))
		 ((quorum-local? q)))
		x)
      (and (pair? from) (ormap
			 (lambda (from)
			   (and (oid? from)
				(fsm-get-meta-data base-directory from base)))
			 from))))

;; This small helper will prevent error messages if there are plain
;; files in the fsm area.  Maybe it's not a good idea to dischard
;; these messages silently, but otherwise questions are raised about
;; error log entries which are safely ignored.

(define (fsm-scandir-if-dir base name . dirs)
  (let ((name (make-pathname (cons base dirs) name)))
    (if (file-directory? name)
	(scandir name)
	'())))

(define fsm-oid-directory-name?
  (pcre/a->proc-uncached "A[0-9a-f]$"))

(define fsm-blob-directory-name?
  (pcre/a->proc-uncached "[0-9a-f][0-9a-f]$"))

(define fsm-blob-basename?
  (pcre->proc "^[^.]*$"))

(cond-expand
 (rscheme
  (define (directory-list-empty? entries)
    (and (pair? entries) (pair? (cdr entries)) (null? (cddr entries)))))
 (else
  (define (directory-list-empty? entries)
    (null? entries))))

(define (restore-blob-info! base-directory frame tag obj)
  (define identifier (aggregate-entity frame))
  (guard
   (ex (else (log-condition
	      (format "nufsm-restore ~a ~a ~a" identifier tag (blob-sha256 obj))
	      ex)))
   (let ((stmts (call-with-list-extending1
		 (lambda (add)
		   (block-table-for-each
		    (place/make-block-table identifier (replicates-of frame) obj)
		    (lambda (obj)
		      (add (cons (literal identifier) (literal (blob-sha256 obj))))))))))
     (with-spool-db
      (lambda (sql)
	(for-each (lambda (p) (sql "insert into CoreBLOBs (oid, blob) values (?1, ?2)" (car p) (cdr p))) stmts))))))

(define (nufsm-restore-frame base-directory f)
  (define identifier (literal (aggregate-entity f)))
  (define bodies '(sqlite3 mind-body))
  (guard
   (ex (else (log-condition (format "nufsm-restore ~a" identifier) ex)))
   ((fsm-yield!))
   (with-spool-db
    (lambda (sql)
      (sql "delete from CoreLinks where s = ?1" identifier)	
      (and-let*
       ((type (fget f 'mind-action-document)))
       (sql "insert into CoreLinks (s, d, t) values (?1, ?2, 1)" identifier (literal type)))
      (and-let*
       ((links (fget f 'mind-links)))
       (fold-links
	(lambda (e value i)
	  (if (oid? value)
	      (sql "insert into CoreLinks (s, d, t) values (?1, ?2, 3)" identifier (literal value))
	      ))
	'()
	links))
      (sql "DELETE FROM CoreBLOBs WHERE oid = ?1" identifier)
      (for-each
       (lambda (b)
	 (and-let* ((obj (fget f b))
		    ((a:blob? obj)))
		   (sql "insert into CoreBLOBs (oid, blob) values (?1, ?2)"	identifier (literal (blob-sha256 obj)))))
       bodies))))
  (for-each
   (lambda (b)
     (and-let* ((obj (fget f b))
		((bhob? obj))
		((want-body-local? f)))
	       (restore-blob-info! base-directory f b obj)))
   bodies))

(define (nufsm-restore base-directory)
  (spool-db-exec "DELETE FROM REACHED")
  (fsm-walk-places
   (lambda (f) (nufsm-restore-frame base-directory f))
   base-directory))

(define (fsm-walk-places proc base-directory)
  (let ((todo (spool-db-select "SELECT value FROM FsmState WHERE parameter = 0")))
    (if (eqv? (sql-ref todo #f #f) 0)
	(begin
	  (spool-db-exec "INSERT INTO FsmState (parameter, value) VALUES(0, \"p1\");DELETE FROM FsmState WHERE parameter > 0")
	  (fsm-walk-places proc base-directory))
	(let ((todo (sql-ref todo 0 0)))
	  (if (not (or (equal? todo "p4") (equal? todo "p5")))
	      (begin
		(cond
		 ((equal? todo "p1")
		  (fsm-walk-places1 base-directory))
		 ((equal? todo "p2")
		  (fsm-walk-places2 base-directory))
		 ((equal? todo "p3")
		  (fsm-walk-places3 proc base-directory))
		 (else (error "fsm-walk-places: unknown operation")))
		(fsm-walk-places proc base-directory)))))))

(define (fsm-walk-places1 base-directory)
  (for-each
   (lambda (lvl1)
     (if (fsm-oid-directory-name? lvl1)
	 (let ((entries (fsm-scandir-if-dir base-directory lvl1)))
	   (if
	    (directory-list-empty? entries)
	    (remove-dir (make-pathname base-directory lvl1))
	    (spool-db-set!
	     (lambda (sql) #t)
	     (lambda (sql)
	       (sql "INSERT INTO FsmState(parameter, value) VALUES(1, ?1)" lvl1)))))))
   (scandir base-directory))
  (spool-db-exec "UPDATE FsmState SET value = 'p2' where parameter=0"))

(define (fsm-walk-places2 base-directory)
  (let ((todo (spool-db-select "SELECT value FROM FsmState WHERE parameter = 1 ORDER BY value ASC LIMIT 1")))
    (if (eqv? (sql-ref todo #f #f) 0)
	(spool-db-exec "UPDATE FsmState SET value = 'p4' where parameter=0")
	(begin
	  (spool-db-set!
	   (lambda (sql) #t)
	   (lambda (sql)
	     (let* ((lvl1 (sql-ref todo 0 0))
		    (entries (fsm-scandir-if-dir base-directory lvl1)))
	       (if (directory-list-empty? entries)
		   (begin
		     (remove-dir (make-pathname base-directory lvl1))
		     (sql "DELETE FROM FsmState WHERE parameter=1 and value=?1" lvl1))
		   (for-each
		    (lambda (lvl2)
		      (if (not (member lvl2 '("." "..")))
			  (sql "INSERT INTO FsmState (parameter, value) VALUES (2, ?1)" lvl2)))
		    entries)))
	     (sql "UPDATE FsmState SET value = 'p3' where parameter=0")))))))

(define (fsm-walk-places3 proc base-directory)
  (define sql spool-db-select)
  (let ((l1 (sql "SELECT value FROM FsmState WHERE parameter = 1 ORDER BY value ASC LIMIT 1"))
	(l2 (sql "SELECT value FROM FsmState WHERE parameter = 2 ORDER BY value ASC LIMIT 1")))
    (if (eqv? (sql-ref l2 #f #f) 0)
	(begin
	  (spool-db-exec/prepared "DELETE FROM FsmState WHERE parameter=1 AND value=?1" (sql-ref l1 0 0))
	  (spool-db-exec/prepared "UPDATE FsmState SET value='p2' WHERE parameter=0"))
	(let ((lvl1 (sql-ref l1 0 0))
	      (lvl2 (sql-ref l2 0 0)))
	  (let ((entries (fsm-scandir-if-dir base-directory lvl2 lvl1)))
	    (if
	     (directory-list-empty? entries)
	     (remove-dir (make-pathname (list base-directory lvl1) lvl2))
	     (for-each
	      (lambda (fn)
		;; KLUDGE has-suffix? use here breaks abstraction
		 (if (has-suffix? ".rdf,c" fn)
		     (guard
		      (c (else
			  (logerr "fsm-fold-store (1) : ~a ~a\n" fn c)))
		      (let* ((n (string->oid (substring fn 0 33)))
			     (v (fsm-get-meta-data base-directory #f n)))
			(proc v)))))
	      entries))
	    (spool-db-exec/prepared "DELETE FROM FsmState WHERE parameter=2 AND value=?1" lvl2))))))

(define (fsm-walk-blob-store base-directory proc)
  (for-each
   (lambda (lvl1)
     (if (fsm-blob-directory-name? lvl1)
	 (let ((entries (fsm-scandir-if-dir base-directory lvl1)))
	   (if
	    (directory-list-empty? entries)
	    (remove-dir (make-pathname base-directory lvl1))
	    (for-each
	     (lambda (lvl2)
	       (if (not (member lvl2 '("." "..")))
		   (let ((entries (fsm-scandir-if-dir
				   base-directory lvl2 lvl1)))
		     (if (directory-list-empty? entries)
			 (remove-dir (make-pathname
				      (list base-directory lvl1) lvl2))
			 (for-each proc entries)))))
	     entries)))))
   (scandir base-directory)))

;; Handle missing objects.

(define (fsm-gc-begin-missing) #f)

(define (fsm-gc-handle-missing base-directory)
  (define (cons-as-oid x i)
    (or (and-let*
	 ((oid (string->oid (x 0)))
	  (src (fsm-get-meta-data base-directory #f oid))
	  ((want-links-local? src)))
	 (cons oid i))
	i))
  (spool-db-exec
   "insert or ignore into missing(oid)
select oid from reached where not exists (select * from CoreLinks where s=reached.oid)")
  (let loop ()
    (sql-fold
     (spool-db-select "select oid from missing where not confirmed limit 10")
     (lambda (col i)
       (let* ((val (col 0))
	      (oid (string->oid val)))
	 (if oid
	     (let ((srcs (sql-fold
			   (spool-db-select/prepared "select s from CoreLinks where d=?1" val)
			   cons-as-oid
			   '())))
	       (if (null? srcs)
		   (begin
		     (spool-db-exec/prepared "update missing set confirmed=-2 where oid=?1" val)
		     (add1 i))
		   (if (or
			(fsm-get-meta-data base-directory srcs oid)
			(begin
			  (logfsm "fsm ~a missing ~a ref from ~a\n" i oid srcs)
			  #f))
		       (begin
			 (spool-db-exec/prepared "delete from missing where oid=?1" val)
			 (add1 i))
		       (begin
			 (spool-db-exec/prepared "update missing set confirmed=?1 where oid=?2"
					(+ (time-second *system-time*) 90) val)
			 i))))
	     (begin
	       (spool-db-exec/prepared "delete from missing where oid=?1" val)
	       (add1 i)))))
     0)
    (if (>
	 (sql-ref
	  (spool-db-select/prepared "select count(*) from missing where not confirmed")
	  0 0)
	 0)
	(loop)))
  (logfsm
   "fsm ~a missings ~a confirmed\n"
   (sql-ref (spool-db-select/prepared "select count(*) from missing where not confirmed") 0 0)
   (sql-ref (spool-db-select/prepared "select count(*) from missing where confirmed>0") 0 0)))

(define (fsm-gc-end-missing)
  (spool-db-exec "update missing set confirmed=0 where confirmed > 0"))


(define (fsm-gc-compute-tcl base-directory)
  (logfsm "fsm computing transitive closure of reacheable places\n")
  (spool-db-exec/prepared "INSERT OR IGNORE INTO REACHED (oid) VALUES(?1)" (literal (my-oid)))
  (fsm-gc-begin-missing)
  (let loop ((n (sql-ref (spool-db-select "SELECT COUNT(*) FROM REACHED") 0 0)))
    ((fsm-yield!))
    (let ((nn (begin
		(spool-db-exec "INSERT OR IGNORE INTO REACHED (oid) SELECT d FROM CoreLinks JOIN REACHED on REACHED.oid=CoreLinks.s WHERE CoreLinks.d NOT IN (SELECT oid FROM REACHED)")
		(sql-ref (spool-db-select "SELECT COUNT(*) FROM REACHED") 0 0))))
      (if (< n nn)
	  (begin
	    (fsm-gc-handle-missing base-directory)
	    (loop nn))
	  (if (< nn 2) (raise 'suspect-result)
	      (logfsm "fsm ~a reachable\n" nn)))))
  (fsm-gc-end-missing)
  (spool-db-exec "CREATE TABLE IF NOT EXISTS GARBAGE (oid char(33) PRIMARY KEY);
DELETE FROM GARBAGE;
INSERT INTO GARBAGE (oid) SELECT DISTINCT s FROM CoreLinks WHERE s NOT IN (SELECT oid FROM REACHED)"))

(define (nufsm-remove-backup base id part)
  (let ((f (make-fsm-component base id part 'current)))
    (if (file-exists? f)
        (rename-file f (make-fsm-component base id part 'old)))))

(define (nufsm-remove-actually base id part . name)
  (let ((f (make-fsm-component base id part 'current)))
    (if (file-exists? f) (remove-file f))))

(define (make-cleanup-procedure/place base)
  (let ((well-known (list (oid->string null-oid) (oid->string one-oid)))
	(limit (string->number
		(sql-ref (spool-db-select "SELECT value FROM FsmState WHERE parameter=-1") 0 0))))
    (lambda (k i)
      (if (member k well-known)
	  1
	  (let ((remove (if #t nufsm-remove-actually nufsm-remove-backup)))
	    ((fsm-yield!))
	    (let ((rdf (make-fsm-component base k 'rdf 'current)))
	      (if (file-exists? rdf)
		  (let ((t (file-modification-time rdf)))
		    (if (or (not limit) (< t limit))
			(begin
			  (logfsm "gc file ~a\n" k)
			  (remove base k 'rdf)
			  (remove base k 'body)
			  (remove base k 'sqlite-user)
			  (let ((d (make-fsm-component base k 'linkdir 'current)))
			    (if (file-exists? d)
				(if (eq? remove nufsm-remove-actually)
				    (remove-dir-recursive d)
				    (nufsm-remove-backup base k 'linkdir)))))
			(logfsm "gc file ~a kept\n" k)))))
	    0)))))

(define (make-cleanup-procedure/place2 sql)
  (let ((well-known (list (oid->string null-oid) (oid->string one-oid))))
    (lambda (k i)
      (if (member k well-known)
	  1
	  (let ((k (literal k)))
	    (sql "DELETE FROM CoreLinks WHERE s = ?1" k)
	    (sql "DELETE FROM GARBAGE WHERE oid = ?1" k)
	    (sql "DELETE FROM CoreBlobs WHERE oid=?1" k)
	    0)))))

(define (make-cleanup-procedure/blob base)
  (let ((limit (string->number
		(sql-ref (spool-db-select "SELECT value FROM FsmState WHERE parameter=-1") 0 0))))
    (lambda (k i)
      (let* ((l2 (substring k 2 4))
	     (b (list base "blob" (substring k 0 2)))
	     (b2 (append b (list l2))))
	(guard
	 (ex (else #f))
	 (for-each
	  (lambda (e)
	    (if (string-prefix? k e)
		(let* ((fn (make-pathname b2 e))
		       (t (file-modification-time fn))
		       (f (and limit (< t limit))))
		  (logfsm "fsm ~aremove blob ~a\n" (if f "" "NOT ") e)
		  (if f (remove-file fn)))))
	  (scandir (make-pathname b l2))))
	((fsm-yield!))
	(spool-db-exec/prepared "DELETE FROM CoreBLOBS where blob = ?1" (literal k))))))

(define (nufsm-remove-garbage base-directory)
  (logfsm "fsm removing place garbage\n")
  (let ((clean (make-cleanup-procedure/place base-directory)))
    (let loop ((n 0))
      (let ((r (spool-db-select	"SELECT oid from GARBAGE LIMIT 100")))
	(if (> (sql-ref r #f #f) n)
	    (begin
	      (do ((i 0 (add1 i))
		   (n n (+ n (clean (sql-ref r i 0) #f))))
		  ((eqv? i (sql-ref r #f #f))
		   (with-spool-db
		    (lambda (sql)
		      (let ((clean (make-cleanup-procedure/place2 sql)))
			(do ((i 0 (add1 i))
			     (n n (+ n (clean (sql-ref r i 0) #f))))
			    ((eqv? i (sql-ref r #f #f)))))))	   
		   (loop n))))))))
  (spool-db-exec "DELETE FROM GARBAGE")
  (logfsm "fsm removing blob garbage\n")
  (let ((clean (make-cleanup-procedure/blob base-directory)))
    (fsm-walk-blob-store
     (make-pathname base-directory "blob")
     (lambda (fn)
       (guard
	(c (else (logerr "fsm-gc-loop (2) : ~a ~a\n" fn c)))
	(cond
	 ((not (fsm-blob-basename? fn))) ; ignore
;;	 ((not (non-transient-blob? (string->symbol fn))))
	 ((eqv? (sql-ref
		 (spool-db-select/prepared
		  "SELECT count(*) FROM CoreBLOBs JOIN REACHED on CoreBLOBs.oid=REACHED.oid WHERE blob=?1"
		  fn)
		 0 0)
		0)
	  (clean fn #f))
	 (else #t))))))
  ;; (spool-db-exec "DELETE FROM REACHED")
  ;; FIXME: for some unfigured reason VACUUM raises an error.
  ;; (spool-db-exec "VACUUM")
  (and-let* ((d (respond-timeout-interval))) (thread-sleep! d))
  (spool-db-exec "DELETE FROM REACHED")
  (sqlite3-exec (lockable-db-connection *spool-db*) "VACUUM")
  )

(define (fsm-run-now store)
  (guard
   (c (else (receive
	     (title msg args rest)
	     (condition->fields
	      (cond
	       ((uncaught-exception? c)
		(uncaught-exception-reason c))
	       (else c)))
	     (logcond
	      'nufsm-gc title
	      (if (message-condition? c) (condition-message c) msg)
	      args)
	     (raise c))))
   (check-spool-db!)
   (spool-db-exec "CREATE TABLE IF NOT EXISTS
FsmState(parameter small, value char(33))")
   (let* ((r (spool-db-select "SELECT value FROM FsmState WHERE parameter=-1"))
	  (cs (time-second *system-time*))
	  (d (let ((d (or (fsm-delay) 1)))
	       (if (eqv? (sql-ref r #f #f) 0) d
		   (- d (- cs (string->number (sql-ref r 0 0))))) ))
	  (todo0 (spool-db-select "SELECT value FROM FsmState WHERE parameter = 0"))
	  (todo (and (fx>= (sql-ref todo0 #f #f) 1)
		     (sql-ref todo0 0 0))))
     (if (eqv? (sql-ref r #f #f) 0)
	 (spool-db-exec
	  (format "INSERT INTO FsmState (parameter, value) VALUES(-1, ~a)" (- cs d))))
     (cond
      ((equal? todo "p4")
       (fsm-gc-compute-tcl store)
       (nufsm-remove-garbage store))
      ((equal? todo "p5") #f)
      (else (logfsm "fsm restore spool data\n")
	    (nufsm-restore store)
	    (fsm-gc-compute-tcl store)
	    (nufsm-remove-garbage store)))
     (spool-db-exec "UPDATE FsmState SET value = 'p5' where parameter=0")
     (logfsm "fsmgc sleep ~a\n" d)
     (if (not (negative? d)) (thread-sleep! d))
     (spool-db-exec (format "DELETE FROM FsmState;
INSERT INTO FsmState (parameter, value) values(0, ~a);
INSERT INTO FsmState (parameter, value)
VALUES(-1, ~a)" (if ($fsm-precise) "'p4'" "'p1'") (time-second *system-time*))))))

(define fsm-delay (make-shared-parameter 6000))

(define (fsm-enforce-restore! store)
  (check-spool-db!)
  (spool-db-exec "CREATE TABLE IF NOT EXISTS
FsmState(parameter small, value char(33));DELETE FROM FsmState WHERE parameter = 0;INSERT INTO FsmState (parameter, value) values(0, 'p1')")
  (parameterize ((fsm-delay 0)) (fsm-run-now store)))

(define nufsm-gc
  (let ((sig (xthe (or false (struct <mailbox>)) #f)))
    (lambda (store)
      (if (not sig)
	  (begin
	    (set! sig (make-mailbox "fsm-gc"))
	    (thread-start!
	     (make-thread
	      (lambda ()
		(let loop ()
		  (let ((store (receive-message! sig)))
		    (parameterize
		     ((fsm-yield! fsm-yield-lower-priority!))
		     (fsm-run-now store))
		    (mailbox-drop-matching! sig (lambda (x) (equal? x store)))
		    (loop))))
	      "fsm-gc"))))
      (send-message! sig store)
      #f)))

;; FIXME: This implements the horrible HACK "bhob->string:re-check-bhob"

(: bhob->string:re-check-bhob-implementation (:blob: -> boolean))
(define (bhob->string:re-check-bhob-implementation bhob)
  (sql-fold
   (spool-db-select/prepared
     "select distinct oid from CoreBLOBs where blob = ?1"
     (literal (blob-sha256 bhob)))
   (lambda (col success)
     (or success
	 (let* ((oid (string->oid (col 0)))
		(frame (find-local-frame-by-id oid 're-check-bhob)))
	   (and frame
		(guard
		 (ex ((missing-blob-condition? ex) #f)
		     (else (log-condition 'check-bhob ex) #f))
		 (let ((t (place/make-block-table oid (replicates-of frame) bhob)))
		  (block-table-for-each
		   t
		   (lambda (obj)
		     (spool-db-exec/prepared
		      "insert into CoreBLOBs (oid, blob) values (?1, ?2)"
		      (oid->string oid) (literal (blob-sha256 obj)))))))))))
   #f))

(set-bhob->string:re-check-bhob! bhob->string:re-check-bhob-implementation)

;; storage adaptor

(define fsm-storage-adaptor
  (make-storage-adaptor load: nufsm-read
                        commit-frame: nufsm-commit-changes
                        commit-into: nufsm-commit-all
			store-blob: fsm-store-blob-value
			store-blob-notation: fsm-store-blob-notation
			blob-notation-size: fsm-blob-notation-size
			fetch-blob-notation: fsm-fetch-blob-notation
			display-blob-notation: fsm-display-blob-notation
			copy-blob: fsm-copy-blob-value
                        gc: nufsm-gc
                        ))
