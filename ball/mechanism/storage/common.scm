(define spool-directory (make-shared-parameter #f))

(define $fsm-verbose (make-shared-parameter #f))

;; Private to fsm:

(define (aggregate-check-blob! frame target)
  (and (not (blob-notation-size *the-registered-stores* target #f))
       (let ((quorum (replicates-of frame)))
	 (and (pair? (quorum-others quorum))
	      (!start
	       (aggregate-fetch-blob quorum (aggregate-entity frame)
				     (get-slot (aggregate-meta frame) 'mind-action-document)
				     target)
	       (blob-sha256 target))))))

(define (aggregate-check-blob2! quorum oid action target)
  (and-let* (((not (blob-notation-size *the-registered-stores* target #f)))
	     ((quorum? quorum))
	     ((pair? (quorum-others quorum))))
	    (!start (aggregate-fetch-blob quorum oid action target)
		    (blob-sha256 target))))
