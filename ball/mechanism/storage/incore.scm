;; (C) 2006 Jörg F. Wittenberger see http://www.askemos.org

;; BEWARE: $in-core-max-blob-size should IN THEORY NOT be 'fx>=' the
;; normal sqlite3 bhob block size.  Sqlite does it's own caching and
;; thus we would waste memory.  -- That's the rationale.  But it turns
;; out sqlite3 re-requests the same blob (particulary view
;; definitions) over and over again.

;;(define $in-core-max-blob-size 16384)
(define $in-core-max-blob-size 65536)

(define *blob-table*
  (make-cache "*blob-table*"
	      eq?
	      #f ;; state
	      ;; miss:
	      (lambda (cs k) (list 'pending))
	      ;; hit
	      (lambda (c es)
		(set-car! es *system-time*))
	      ;; fulfil
	      (lambda (c es results)
		(set-car! es (and results *system-time*))
		(values))
	      ;; valid?
	      (lambda (es)
		(let ((v (car es)))
		  (or (eq? v 'pending) (srfi19:time? v))))
	      ;; delete
	      #f ;; (lambda (cs es) #f)
	      ))

(define incore-cache-duration (make-time 'time-duration 0 90))

(define (in-core-gc dummy)
  (let ((deadline (subtract-duration *system-time* incore-cache-duration)))
    (cache-cleanup!
     *blob-table*
     (lambda (es)
       (let ((v (car es)))
	 (or (eq? v 'pending) (and v (srfi19:time>=? v deadline))))))))

(define (display-to-string value)
  (call-with-output-string (lambda (port) (display value port))))

(define (incore-cached-text/plain value)
  (if (and $in-core-max-blob-size
	   (string? value)
	   (fx>= (string-length value) $in-core-max-blob-size))
      #f
      value))

(define (incore-to-text/plain blob key value)
  (or
   (fetch-blob-notation
    (let ((sas *the-registered-stores*))
      (if (and (pair? sas) (eq? (caar sas) in-core-storage-adaptor))
	  (cdr sas) sas))
    blob #f)
   ;; (raise
   ;;  (make-condition &blob-missing-condition
   ;; 		    'status 404
   ;; 		    'message (format "blob ~a missing" (blob-sha256 blob))
   ;; 		    'source blob))
   ;; ;; This case will hang!
   ;; (let ((value (force value)))
   ;;   ((or (mime-converter text/plain key)
   ;; 	  (and (string? value) identity)
   ;; 	  (and (node-list? value)
   ;; 	       mime-format)
   ;; 	  display-to-string)
   ;;    value))
   ))

(define (in-core-store-blob-notation store blob key value)
  (let ((v (cache-ref
	    *blob-table* (blob-sha256 blob)
	    (lambda ()
	      (list
	       (cons #f
		     (delay*
		      (incore-cached-text/plain
		       (if (eq? key #f)
			   value
			   (incore-to-text/plain blob key value)))
		      (blob-sha256 blob))))))))
    (cond
     ((not key) (cdar v))		; plain text
     ((assoc key (cdr v)) => cdr)	; found cached value
     ((not value) #f)			; forget it
     ((eq? key text/xml)		; special case; avoid cyclic references
      (let ((oe (document-element (force value))))
	(if oe
	    (cond
	     ((eq? (gi oe) 'output)) ; should check namespace too
	     (else
	      (set-cdr! v (cons (cons key oe) (cdr v)))))))
      value)
     (else (set-cdr! v (cons (cons key value) (cdr v)))
	   value))))

(define (in-core-store-blob-value store blob octet-stream)
  (if (cache-ref/default *blob-table* (blob-sha256 blob) #f #f)
      blob
      (in-core-store-blob-notation store blob #f octet-stream)))

(define (cdrassq k lst)
  (and-let* ((v (assq k lst)))
	    (cdr v)))

(define (in-core-fetch-blob-notation store blob key)
  (cond
   ((not key)
    (if (and $in-core-max-blob-size
	     (number? (a:blob-size blob))
	     (fx>= (a:blob-size blob) $in-core-max-blob-size))
	#f
	(cdrassq
	 key
	 (cache-ref *blob-table* (blob-sha256 blob)
		    (lambda ()
		      (list
		       (cons #f
			     (fetch-blob-notation
			      (let ((sas *the-registered-stores*))
				(if (and (pair? sas) (eq? (caar sas) in-core-storage-adaptor))
				    (cdr sas) sas))
			      blob #f))))))))
   ((cache-ref/default *blob-table* (blob-sha256 blob) #f #f) =>
    (lambda (a) (cdrassq key a)))
   (else #f)))

(define (in-core-copy-blob-value store blob to n foff toff)
  (and-let* ((val (force (in-core-fetch-blob-notation store blob #f))))
	    (move-memory! val to n foff toff)
	    n))

(define in-core-storage-adaptor
  (make-storage-adaptor open:        
                        commit:      
                        commit-into: 
                        close:       
			store-blob:  in-core-store-blob-value
			store-blob-notation: in-core-store-blob-notation
			fetch-blob-notation: in-core-fetch-blob-notation
			copy-blob: in-core-copy-blob-value
			gc: in-core-gc))
