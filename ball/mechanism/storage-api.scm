;; (C) 2000, 2001, 2004, 2013 J�rg F. Wittenberger see http://www.askemos.org

(define $agree-verbose #t)

(define (agree-verbose . flag)
  (if (pair? flag)
      (let ((old $agree-verbose))
	(set! $agree-verbose (car flag))
	old)
      $agree-verbose))

(define (default-logagree/effective type who on . args)
  (apply
   logerr
   (case type
     ((echo) "Q Host ~a on ~a version ~a (~a/~a) ~a uninformed ~a missing ~a.\n")
     ((ready) "Q Host ~a on ~a ~a==~a need ~a more from ~a.\n")
     ((ready-completed) "Q Host ~a on ~a completed by ~a.\n")
     ((contra) "Q Host ~a on ~a version ~a ~a CONTRA ~a missing ~a\n."))
   who on
   args))

(define $logagree/effective
  (let ((handler default-logagree/effective))
    (lambda _
      (let ((x handler))
	(if (pair? _) (set! handler (car _)))
	x))))

;;**** Storage Adaptor API

(: storage-adaptor? (* --> boolean : (struct <storage-adaptor>)))

(define-record-type <storage-adaptor>
  (internal-make-storage-adaptor
   load commit-frame commit-into commit open close
   store-blob store-blob-notation blob-notation-size
   fetch-blob-notation display-blob-notation copy-blob-value
   gc)
  storage-adaptor?
  (load storage-adaptor-load)
  (commit-frame storage-adaptor-commit-frame)
  (commit-into storage-adaptor-commit-into)
  (commit storage-adaptor-commit)
  (open storage-adaptor-open)
  (close storage-adaptor-close)

  (store-blob storage-adaptor-store-blob-value)
  (store-blob-notation storage-adaptor-store-blob-notation)
  (fetch-blob-notation storage-adaptor-fetch-blob-notation)
  (blob-notation-size storage-adaptor-blob-notation-size)
  (display-blob-notation storage-adaptor-display-blob-notation)
  (copy-blob-value storage-adaptor-copy-blob-value)

  (gc storage-adaptor-gc))

(: make-storage-adaptor (&rest --> :storage-adaptor:))
(define (make-storage-adaptor . args)   ; EXPORT
  (let ((load #f)
        (commit-frame #f)
        (commit-into #f)
        (commit #f)
        (open #f)
        (close #f)
	(store-blob #f)
	(store-blob-notation #f)
	(fetch-blob-notation #f)
	(blob-notation-size #f)
	(display-blob-notation #f)
	(copy-blob-value #f)
        (gc #f))
    (do ((args args (cddr args)))
        ((null? args)(internal-make-storage-adaptor
                      load commit-frame commit-into commit open close
		      store-blob store-blob-notation blob-notation-size
		      fetch-blob-notation display-blob-notation
		      copy-blob-value
		      gc))
      (case (car args)
        ((load:)         (set! load (cadr args)))
        ((commit-frame:) (set! commit-frame (cadr args)))
        ((commit-into:)  (set! commit-into (cadr args)))
        ((commit:)       (set! commit (cadr args)))
        ((open:)         (set! open (cadr args)))
        ((close:)        (set! close (cadr args)))
        ((gc:)           (set! gc (cadr args)))
	((store-blob:)   (set! store-blob (cadr args)))
	((store-blob-notation:) (set! store-blob-notation (cadr args)))
	((fetch-blob-notation:) (set! fetch-blob-notation (cadr args)))
	((blob-notation-size:) (set! blob-notation-size (cadr args)))
	((display-blob-notation:) (set! display-blob-notation (cadr args)))
	((copy-blob:) (set! copy-blob-value (cadr args)))
        (else (error "make-storage-adaptor: unknown slot ~a"
                     (car args)))))))

;; Not sure that this api suits best.  But is does for pstore.
(define (commit-mind-into stores target)
  (do ((stores stores (cdr stores)))
      ((null? stores) #t)
    (if (storage-adaptor-commit-into (caar stores))
        (set-cdr! (car stores)
                  ((storage-adaptor-commit-into (caar stores))
                   target (cdar stores))))))

(: commit-mind (:stores: -> boolean))
(define (commit-mind stores)
  (do ((stores stores (cdr stores)))
      ((null? stores) #t)
    (if (storage-adaptor-commit (caar stores))
        ((storage-adaptor-commit (caar stores)) (cdar stores)))))

(: commit-frame-into
   (:stores:
    :oid:
    (* -> * * boolean);; :place:
    (list-of (pair string *)) -> boolean))
(define (commit-frame-into stores entity frame new-links)
  (do ((stores stores (cdr stores)))
      ((null? stores) #t)
    (if (storage-adaptor-commit-frame (caar stores))
        ((storage-adaptor-commit-frame (caar stores))
         (cdar stores) entity frame new-links))))

(: load-frame (:stores: :oid: -> :place-or-not:))
(define (load-frame stores entity)

  (let loop ((stores stores))
    (if (null? stores)
        #f
        (or (and (storage-adaptor-load (caar stores))
                 ((storage-adaptor-load (caar stores))
                  (cdar stores) entity))
            (loop (cdr stores))))))
;; BLOB

(: blob->xml ((struct <blob>) --> (struct <xml-element>)))
(define (blob->xml obj)
  (if (fx< 1 (blob-blocks-per-blob obj))
      (make-xml-element
       'blob namespace-mind
       (list (make-xml-attribute 'size #f (number->string (a:blob-size obj)))
	     (make-xml-attribute 'bs #f (number->string (blob-block-size obj)))
	     (make-xml-attribute 'bpb #f (number->string (blob-blocks-per-blob obj))))
       (make-xml-element
	'sha256 namespace-mind '()
	(literal (blob-sha256 obj))))
      (make-xml-element
       'blob namespace-mind
       (if (a:blob-size obj)
	   (list (make-xml-attribute 'size #f (number->string (a:blob-size obj))))
	   '())
       (make-xml-element
	'sha256 namespace-mind '()
	(literal (blob-sha256 obj))))))

(: xml->blob ((struct <xml-element>) --> (struct <blob>)))
(define (xml->blob obj)
  ;; FIXME: assert that the xml-lement is not made-up, but properly
  ;; received from the system kernel.
  (let ((bpba (attribute-string 'bpb obj)))
    (if bpba
	(let ((bpb (string->number bpba))
	      (bs (string->number (attribute-string 'bs obj)))
	      (size (string->number (attribute-string 'size obj))))
	  (let ((body (data ((sxpath '(sha256)) obj))))
	    (a:make-blob (string->symbol body) size bs bpb #f)))
	(let ((body (string->symbol (data ((sxpath '(sha256)) obj))))
	      (sa (and-let* ((sa (attribute-string 'size obj))) (string->number sa))))
	  (if sa
	      (a:make-blob body sa sa 1 #f)
	      (a:make-blob body #f #f 1 #f))))))

;; Store an octet stream containing a well-formed XML document
;; RETURN <blob>

(: store-blob-value (:stores: (struct <blob>) string -> (struct <blob>)))
(define (store-blob-value stores blob octet-stream)
  (do ((stores stores (cdr stores)))
      ((null? stores) blob)
    (if (storage-adaptor-store-blob-value (caar stores))
	((storage-adaptor-store-blob-value (caar stores))
	 (cdar stores) blob octet-stream))))

;; (Late addition 2012 - to improve SQL speed) copy blob value into
;; target memory just once.

(: copy-blob-value
   (:stores: (struct <blob>)
	     (or string pointer)	; to: address
	     number			; n bytes
	     number			; from offset
	     number			; to offset
	     -> (or boolean number)))
(define (copy-blob-value stores blob to n foff toff)
  (ormap (lambda (sa)
	   (and-let* ((proc (storage-adaptor-copy-blob-value (car sa))))
		     (proc (cdr sa) blob to n foff toff)))
	 stores))

;; Cache alternate representation ('value') of 'blob' under 'key'.
;; Key #f is default notation.  Returns no useful values.

(: store-blob-notation
   (:stores: :blob: string * -> *))
(define (store-blob-notation stores blob key value)
  (do ((stores stores (cdr stores)))
      ((null? stores) (force value))
    (if (storage-adaptor-store-blob-notation (caar stores))
	((storage-adaptor-store-blob-notation (caar stores))
	 (cdar stores) blob key value))))

(: fetch-blob-notation
   (:stores: (struct <blob>) (or false string) -> *))
(define (fetch-blob-notation stores blob key)
  (ormap (lambda (sa)
	   (and (storage-adaptor-fetch-blob-notation (car sa))
		(force
		 ((storage-adaptor-fetch-blob-notation (car sa))
		  (cdr sa) blob key))))
	 stores))

(: blob-notation-size
   (:stores: (struct <blob>) (or false string) -> (or false number)))
(define (blob-notation-size stores blob key)
  (ormap (lambda (sa)
	   (and (storage-adaptor-blob-notation-size (car sa))
		((storage-adaptor-blob-notation-size (car sa))
		 (cdr sa) blob key)))
	 stores))

(: display-blob-notation
   (:stores: (struct <blob>)
	     (or false string)	; key
	     (* -> undefined)		; display
	     -> (or false number)))
(define (display-blob-notation stores blob key display)
  (ormap (lambda (sa)
	   (and (storage-adaptor-display-blob-notation (car sa))
		((storage-adaptor-display-blob-notation (car sa))
		 (cdr sa) blob key display)))
	 stores))

;; The gc-mind protocol is really not yet elaborated.

;; Just a map, but we know the eval sequence.  It's used because we
;; know that the gc process could invalidate information, which it
;; could use for lower storage adaptors.  Hence we eval the last one
;; first.
(define (map-from-end f l)
  (if (null? l)
      '()
      (let ((r (map-from-end f (cdr l))))
        (cons (f (car l)) r))))

(: gc-mind (:stores: -> :stores:))
(define (gc-mind stores)
  (map-from-end
   (lambda (store)
     (let* ((f (storage-adaptor-gc (car store)))
            (o (and f (f (cdr store)))))
       (if o (cons (car store) o) store)))
   stores))

;; A list of (storage-adaptor opaque-data-of-storage-adaptor) elements
;; for each active local store.

(: *the-registered-stores* :stores:)
(define *the-registered-stores* '())

;; Where we replicate byzantine places, which already have several
;; replicates.

(: *the-remote-stores* :stores:)
(define *the-remote-stores* '())

(: register-storage-adaptor! (symbol (struct <storage-adaptor>) * -> undefined))

(define (register-storage-adaptor! type handler param)
  (case type
    ((local:)
     (set! *the-registered-stores*
	   (cons (cons handler param)
	    *the-registered-stores*)))
    ((remote:)
     (set! *the-remote-stores*
	   (cons (cons handler param)
	    *the-remote-stores*)))))
