;; (C) 2000-2008 Jörg F. Wittenberger see http://www.askemos.org

(define current-place (make-parameter (lambda x (error "current-place not set"))))

(define current-message (make-parameter (lambda x (error "current-message not set"))))

;;** Data Structures

;; A place is something, where data can rest.  You could understand
;; them as variables. But there's a little more than just the body of
;; the data.

;; Besides (possibly structured) data and it's type (content type)
;; some meta data slots are maintained respectively some invariants of
;; them are enforced to build trust.

;; It helps to understand that a place is essentially a stream as
;; described in SICP (Abel, Sussmann).  The head of the stream
;; compares to the data currently stored in the slots of the place,
;; while a transaction stores the result of the tail operation at the
;; same place.  But those "head" and "tail" operations are not
;; directly accessible.  Instead there are two kind of messages
;; (which are much like places, except that there is no storage
;; entity, hence they are not persistent) with appropriate actions, a
;; read and a write type.  The read operation delivers the data at the
;; place, possibly transformed by a function (side effect free!).  A
;; write operation changes all data slots at the place in one
;; transaction with the result of another function.  If the objects
;; function code raises an exception, the transaction is aborted.

;; An operation can send out messages of all types to all places it
;; can address.  There's difference between read and write type
;; messages: read operations can be performed as calls, that is, the
;; sender can wait (at any time) for the operation to complete and use
;; the result.  Write operations are always asynchronous (read
;; operations can be as well) at the end of a successful
;; transaction. [Never change that, even if it's in your way!  This
;; restriction allows to implement hierarchical resource locking.]
;; Besides being a technical requirement, this is an important design
;; decision.

;; The second important part is the name space.  Each idea (object) is
;; connected to it's meta system(s) via it's own name space (held in
;; the slot mind-links).  Those used to traditional file/directory
;; distinctions could think of those links as of a directory.  But that
;; point of view holds only for a part of the truth because a) the
;; links can but don't have to be part of a tree b) there is no unique
;; (global) root.  Places are interconnected by links into a net.

;; Places are implemented using frames.  It would probably desirable
;; to change the implementation to use the RDF data model.  That would
;; bring in some generalism, while only slightly sacrifying
;; performance.

;; These slots comprise a deed.

(define deed-slots
  '(dc-creator                          ; creator (see Dublin Core)
    dc-date                             ; creation date
    mind-action-document                ; behavior
    mind-body                           ; serialized content
    ;; `replicates` is a late (2018) additonal restriction we MAY or may not want.
    replicates				; commissioned notaries (can not work, can it?)
    mind-links
    ;; This is either #f or a hash table holding xml nodes under a
    ;; certain name (string).  Note: there is a special document node,
    ;; which can be used to refer to other documents.
    ))

(define interaction-slots
  '(secret                              ; FIXME store that as md5 hash
    ;; a magic phrase to access a Laune
    protection
    ;; Sequence of documents, which express the objects protection.
    capabilities
    ;; Set of <isomorph to protection>, which define the capabilities
    ;; associated to a message floating through the space.
    potentialities
    ;; Only defiened for entry points; isomorph to capabilities; saved
    ;; capabilities, which can be rectivated.
    content-type
    ;; MIME content type of mind-body.
    version
    ;; number of legal changes since creation
    ))

(define internal-slots
  '(body/parsed-xml
    ;; If mind-body is XML, caches the parsed representation.
    ;; In case of difference between both, this slot wins, but this
    ;; has only internal effect, cause any transport will sync them.
    sqlite3
    ;; The SQL table space of the internal sqlite3 database.
    dav:properties
    ;; webdav properties storage space
    replicates
    ))

(define message-slots
  '(destination
    ;; a path of object names; initially the http XPath.
    location
    ;; a read XPath to the result.  Internally held as a reverse list,
    ;; that is, the first element of the list is the last part of the
    ;; path.  When returned as http a slash and the reverse of the
    ;; list is joined with slashes to form the WebDAV Location header.
    location-format
    ;; A pair of function to format the location for read and write access.
    ;; I'd love a better idea.
    caller
    ;; The sender of the message.
    reply-channel
    ;; mailbox where a response is expected
    accept
    ;; What's the HTTP Accept header.
    ;;
    ;; 2006-09-20 KLUDGE
    referer
    ;; Hm.  We'd better remove "referer", but I need to replicate it
    ;; for now.  Anyway: the current use is bad anyway.
    ))

;; This is what the client should be able to handle.

(define (known-metadata)
  (filter (lambda (x) (not (or (eq? 'mind-body x) (eq? 'mind-links x)))) deed-slots))

;; These slots go into the RDF file representing the place.

(define known-slots
  (let ((result (append interaction-slots (known-metadata))))
    (lambda () result)))

(define stored-slots (append deed-slots interaction-slots internal-slots))

;; FIXME `transient-slots' and `message-slots' are somewhat missnomed.
;; The first should be named message-slots and the latter
;; message-special-slots.

;; These slots *define* a message, which flows around and must always
;; be transfered.  The internal representation might contain other
;; slots (cached representations etc.), which are transfered at
;; discretion of implementator.

(define transient-slots
  (append
   ;; I don't see why we would need to transfer the location-format,
   ;; and did not do so until forwarding was introduced.
   ;;
   ;; (filter (lambda (x) (not (eq? 'location-format x))) message-slots)
   message-slots
   (filter (lambda (x) (not (memq x '(secret protection potentialities))))
           interaction-slots)
   (filter (lambda (x) (not (or (eq? 'mind-action-document x)
				(eq? 'mind-links x)))) deed-slots)))

(define transient-meta-slots
  (filter (lambda (x) (not (eq? 'mind-body x))) transient-slots))

(define serializable-transient-meta-slots
  (filter (lambda (x) (not (eq? 'reply-channel x))) transient-meta-slots))

(define optional-transient-meta-slots
  '(content-type accept referer))

(define optional-transient-reply-meta-slots
  `(destination location location-format
		. ,(filter (lambda (s)
			     ;; Unsure: maybe we should require some of these?
			     (not (memq s '(caller capabilities dc-creator dc-date))))
			   optional-transient-meta-slots)))

;;*** Insulation: definitions to be used "locally"

;; DFN frame - an aggregate with or without any identifier.  In the
;; latter case the data field is filled in with a dummy symbol (called
;; 'slotmap), which reminds a bit to the data structure we have there.

;(define (intern-oid x)
;  (let* ((form (if (string? x) (%make-oid x) x))
;         (exists (find-frames-by-id form)))
;    (if exists (aggregate-entity exists) form)))

;; BEWARE: When eventually changing to a better OID representation:
;; protocol/certs currently duplicates this code due to dependency
;; issued.

(define intern-oid string->symbol)

;;** Self certified, persistant frames

(define (oid->string oid) (symbol->string oid))

(define (string->oid id)
  (let ((l (string-length id)))
    (and (eqv? l 33)
         (and (char=? (string-ref id 0) #\A)
	      (let loop ((i 1))
		(and (or (eqv? i 33)
			 (let ((c (string-ref id i)))
			   (and (or (and (char>=? c #\0) (char<=? c #\9))
				    (and (char>=? c #\a) (char<=? c #\f)))
				(loop (add1 i)))))
		     (intern-oid id)))))))

;; Do we really, really want to accept/ignore so much whitespace or
;; would a more restrictive syntax be a better fit?

(define oid-parse*
  (pcre-lalr-parser
   (("(A[[:xdigit:]]{32})" oid)
    ("[[:space:]]*,[[:space:]]*" comma)
    ("@{[[:space:]]*" qb)
    ("[[:space:]]*}" qe)
    ("[[:space:]]+" ws))

   ;; TODO: fix the grammer instead of expecting shift/reduce conflicts
   (expect: 2)

   (qb qe oid comma ws)
   (qid1 (qid) : $1 (ws qid2) : $2)
   (qid2 (qid) : $1 (qid ws) : $1)
   (qid (oid) : (vector (string->symbol (car $1)) #f)
	(oid quorum0) : (vector (string->symbol (car $1)) (make-quorum $2))
	)
   (sep (ws) : #f (comma) : #f)
   (quorum0 (quorum) : $1 (ws quorum) : $2)
   (quorum (qb oid qr) : (cons (car $2) $3))
   (qr (qe) : '()
       (sep oid qr) : (cons (car $2) $3))
   ))

(: oid-parse (string --> (or false symbol) (or false :quorum:)))
(define (oid-parse in)
  ;; FIXME: better remove this exception handler!  What's going to break?
  (guard
   (ex (else (values #f ex)))
   (let ((v (oid-parse* in))) (values (vector-ref v 0) (vector-ref v 1)))))

;; A thin wrapper, since it might be useful some day to advance the
;; current time by the timespan needed for processing.

(define (askemos:dc-date request-date)
  ;; FIXME the dc-date SHOULD be kept as GMT.  But this incures more
  ;; fixing...
  ;;
  ;; KLUDGE FIXME BUG
  ;;
  ;; We urgently need to supply time monotonous dates.  But those need
  ;; to be negotiated from within the aggreement protocol.  In the
  ;; worst case, this might put us back to the open ended, standard,
  ;; multiple rounds protocol, since our binary value is no longer
  ;; 100% determined by input data.  For the time beeing we resort to
  ;; a modified requests date.
  ;;
  ;;(time-utc->date *system-time* (timezone-offset))
  ;;
  ;;(time-utc->date (make-time 'time-utc 0 (compute-ready-second request-date)))
  (or request-date (time-utc->date *system-time* (timezone-offset)))
  )

;;** Aggregate (like document in  WrapBit)

;; This comment is actually misleading.  I just keep to memoize that
;; there's something to edit:
;; Aggregate is the same as a "document" in WrapBit or any attribute
;; object.  (I don't call it object, cause it is none in the OO sense.)

; (define-macro (aggregate? x) `(pair? ,x))

;(define-macro (aggregate meta data)
;  `(cons ,meta ,data))

;(define-macro (aggregate-meta aggregate) `(car ,aggregate))
;(define-macro (aggregate-entity aggregate) `(cdr ,aggregate))

(define (slotmap? x) (and (aggregate? x) (eq? (aggregate-entity x) 'slotmap)))

(define (make-message . args) (make-frame args))
(define message-clone frame-clone)

(define null-oid (intern-oid "A00000000000000000000000000000000"))
(define one-oid (intern-oid "A00000000000000000000000000000001"))

(: *my-oid* (or false :oid:))
(define *my-oid* #f)

(define (set-my-place! f)
  (if (aggregate? f) (set! *my-oid* (aggregate-entity f))) *my-oid*)

(define (my-oid)
  (or *my-oid*
      (let ((mux (make-mutex null-oid)))
	(retain-mutex
	 mux
	 (let ((f (load-frame *the-registered-stores* null-oid)))
	   (and f
		(let ((oid (aggregate-entity f)))
		  (set! *my-oid* oid)
		  oid)))))))

(: *public-oid* (or false :oid:))
(define *public-oid* #f)

(define (set-public-place! . arg)
  (and-let* ((f (if (pair? arg) (car arg) (load-frame *the-registered-stores* one-oid)))
	     (oid (aggregate-entity f)))
	    (set! *public-oid* oid)
	    (if (not (eq? oid one-oid)) (set! *local-quorum* (make-quorum (list oid))))
	    oid))

(define (public-oid) (or *public-oid* (set-public-place!)))

(define (public-not-initialised) (not *public-oid*))
;;*** Credentials Storage

(: secret-encode (procedure (string string) string))
(: secret-verify (procedure (* *) boolean))
(define-values
  (secret-encode secret-verify)
  (let ((salt-len 6) (md5-len 32))
    (let ((encode (lambda (plain salt)
		    (string-append
		     (substring salt 0 salt-len)
		     (md5-digest (string-append (substring salt 0 salt-len) plain)))))
	  ;; Sure I had to make that mistake and not notice.  Using
	  ;; unsalted hash was once a choice to only obfuscate the
	  ;; secret, but still be able to break it for the sake of
	  ;; development.  Hashing the salt after the plain code was a
	  ;; honest mistake.
	  (encode-wrong
	   (lambda (plain salt)
	     (string-append
	      (substring salt 0 salt-len)
	      (md5-digest (string-append plain (substring salt 0 salt-len)))))))
      (values
       encode
       (lambda (encoded plain)
	 (and (string? plain) (string? encoded)
	      (if (eqv? (string-length encoded) md5-len)
		  (string=? encoded (md5-digest plain)) ;; unsalted
		  ;; Bug compatibility.  We must for now allow the wrong encoding.
		  (or (string=? (encode plain encoded) encoded)
		      (string=? (encode-wrong plain encoded) encoded)))))))))

;;*** Basic host lookup.

(define host-lookup
  (make-shared-parameter
   (lambda (host) (error "host-lookup: protocol not connected"))))

;; 2013-02-26 this seems to be incorrect
;; (define (host-lookup* host)
;;   (or ((host-lookup) host)
;;       (and (oid? host)
;; 	   (raise-service-unavailable-condition (literal host)))
;;       host))

(: host-lookup* ((or :oid: string) -> (or false :ip-addr:)))
(define (host-lookup* host)
  (or ((host-lookup) host)
      (raise-service-unavailable-condition (literal "lookup failed for host " host))))

;;*** Quorum
(: allocate-quorum
   (fixnum boolean (list-of (or :oid: string)) :xml-element: --> :quorum:))
(: quorum-size (:quorum: --> fixnum))
(: quorum-local? (:quorum: --> boolean))
(: quorum-others (:quorum: --> (list-of (or :oid: string))))
(: quorum-xml (:quorum: --> :xml-element:))
(define-record-type <quorum>
  (allocate-quorum
   size
   local
   others
   xml)
  quorum?
  (size quorum-size)
  (local quorum-local?)
  (others quorum-others)
  (xml quorum-xml))

(: make-quorum-li ((or :oid: string) --> :xml-element:))
(define (make-quorum-li obj)
  (make-xml-element
   'li namespace-rdf
   ;; This is not only lying, it's outright broken sometimes.  Why the
   ;; hell does a URL have to include a method?  I have no idea, why
   ;; this prefix is sometime added twice.
   (list (make-xml-attribute 'resource namespace-rdf
                             (literal obj)))
   (empty-node-list)))

(: make-quorum-element ((list-of (or :oid: string)) --> :xml-element:))
(define (make-quorum-element quorum-list)
  (make-xml-element
   'replicates 'http://www.askemos.org/2004/Synchrony/ '()
   (make-xml-element
    'Bag namespace-rdf '()
    (map make-quorum-li quorum-list))))

(: quorum-member-local? ((or :oid: string) --> boolean))
(define (quorum-member-local? e)
  ;; (or (public-not-initialised)
  ;;     (if (eq? (public-oid) one-oid)
  ;; 	  (let ((here (local-id))) (or (equal? e here)
  ;; 				       (equal? ((host-lookup) e 'nowait) here)))
  ;; 	  (eq? e (public-oid))))
  (let ((p *public-oid*))
    (or (not p)
	(if (eq? p one-oid)		;; TBD: get rid of that branch.
	    (let ((here (local-id))) (or (equal? e here) (equal? ((host-lookup) e 'nowait) here)))
	    (eq? e p)))))

(: make-quorum ((or :xml-nodelist: (list-of :oid:)) --> :quorum:))
(define (make-quorum obj)
  (receive
   (quorum-l xml)
   (let ((xml (document-element obj)))
     (if xml
	 (values (node-list-reduce
		  ;; ((sxpath '(Bag li)) obj)  ;; preferable if optimised
		  (select-elements
		   (children (select-elements (children obj) 'Bag))
		   'li)
		  (lambda (init element)
		    (cons (let ((r (attribute-string 'resource element)))
			    (or (string->oid r) (error "illegal host identifier" r)))
			  init))
		  '())
		 xml)
	 (values obj (make-quorum-element obj))))
   (receive
    (local others)
    (partition quorum-member-local? quorum-l)
    (allocate-quorum (if (pair? local) (add1 (length others)) (length others))
                     (pair? local) others xml))))

(define (respond-treshold quorum)
  (quotient (sub1 (quorum-size quorum)) 3))

(define (oid->string/literal-oid obj)
  (if (symbol? obj) (symbol->string obj) (data obj)))

(define (quorum->oid:fold-quorum2string x i)
  (if (oid? x) (cons (oid->string x) i) i))

(define (oid+quorum->string oid . quorum)
  (if (null? quorum)
      (oid->string/literal-oid oid)
      (string-append
       (oid->string/literal-oid oid)
       "@{"
       (let ((quorum (let ((quorum (car quorum)))
		       (if (quorum? quorum) quorum (make-quorum quorum)))))
	 (srfi:string-join
	  (fold quorum->oid:fold-quorum2string
		(if (quorum-local? quorum)
		    (list (symbol->string (public-oid)))
		    '())
		(quorum-others quorum))
	  ","))
       "}")))

;;***** BLOB

(define-condition-type &blob-missing-condition &http-effective-condition
  missing-blob-condition?
  (source missing-blob-source))

(define (aggregate-fetch-blob-default quorum oid action target)
  (and (a:blob? target)
       (or (blob-notation-size *the-registered-stores* target #f)
	   (raise (make-condition
		   &blob-missing-condition
		   'status 404
		   'message (format "blob ~a missing in ~a"
				    (blob-sha256 target)
				    oid)
		   'source oid)))))

(: aggregate-fetch-blob (:quorum: :oid: :oid: :blob: -> boolean))
(define aggregate-fetch-blob aggregate-fetch-blob-default)

(: set-aggregate-fetch-blob! ((procedure (:quorum: :oid: :oid: :blob:) boolean) -> undefined))
(define (set-aggregate-fetch-blob! proc)
  (set! aggregate-fetch-blob proc))

(: body->string deprecated)
(define (body->string b)
  (cond
   ((bhob? b) #f)
   ((a:blob? b)
    (or (fetch-blob-notation *the-registered-stores* b #f)
	(raise (make-condition &blob-missing-condition
			       'status 404
			       'message (format "blob ~a missing" (blob-sha256 b))
			       'source b))))
   ((pair? b) (apply-string-append (map body->string (if (number? (car b)) (cdr b) b))))
   (else b)))

(define (xml-digest-2-simple x)
  (if (a:blob? x) (symbol->string (blob-sha256 x)) (xml-digest-simple x)))

(define (body-size b)
  (cond
   ((a:blob? b)
    (or (a:blob-size b)
	(blob-notation-size *the-registered-stores* b #f)
	(raise (make-condition &blob-missing-condition
			       'status 404
			       'message (format "blob ~a missing" (blob-sha256 b))
			       'source b))))
   ((string? b) (string-length b))
   (else 0)))

(define (%message-xml-parse-as b ct)
  (if ct
      (cond
       ((xml-parseable? ct) (xml-parse b mode: 'permissive))
       ((html-parseable? ct) (xml-parse b mode: 'html))
       ((multipart-data-boundary ct)
	=> (lambda (boundary)
	     (rfc2046-split b boundary)))
       ((mime-converter
	 text/xml (let ((semicolon (string-index ct #\;)))
		    (if semicolon (substring ct 0 semicolon) ct)))
	=> (lambda (convert) (convert b)))
       (else (make-xml-element
	      'output namespace-mind
	      (list  namespace-coreapi-declaration
		     (make-xml-attribute 'media-type #f ct)
		     (make-xml-attribute 'method #f "text"))
	      ;; old: (node-list (literal b))
	      b)))
      (make-xml-element
       'output namespace-mind
       (list  namespace-coreapi-declaration
	      (make-xml-attribute 'media-type #f "text/plain")
	      (make-xml-attribute 'method #f "text"))
       ;; old: (node-list (literal b))
       b)))

(: place/make-block-table (:oid: :quorum: (or false :blob:) &rest -> :block-table:))
(define (place/make-block-table oid quorum obj . dflt)
  (define (fetch obj)
    (aggregate-fetch-blob quorum oid oid obj))

  (define (fetch-notation blob)
    (or (fetch-blob-notation *the-registered-stores* blob #f)
	(and-let* (((pair? (quorum-others quorum))))
		  (fetch blob)
		  (fetch-blob-notation *the-registered-stores* blob #f))
	(raise (make-condition
	 	&blob-missing-condition
	 	'status 404
	 	'message (format "in ~a bhob-blob ~a missing" oid (blob-sha256 blob))
	 	'source oid))))
  (define (size blob)
    (or (blob-notation-size *the-registered-stores* blob #f)
	(and-let* (((pair? (quorum-others quorum))))
		  (fetch blob)
		  (blob-notation-size *the-registered-stores* blob #f))))
  (define (store blob value)
    (store-blob-value *the-registered-stores* blob value))
  (define (btread str set-i-v)
    (call-with-input-string
     str
     (lambda (port)
       (let loop ((i 0))
	 (let ((ln (read-line port)))
	   (cond
	    ((eof-object? ln))
	    ((fx>= (string-length ln) 3)
	     (let* ((b (a:make-blob (string->symbol ln) #f #f #f #f))
		    (s (size b)))
	       (set-blob-size! b s)
	       (set-i-v i b)
	       (loop (add1 i))))
	    (else (loop (add1 i)))))))))
  (define (btwrite obj)
    (call-with-output-string
     (lambda (port)
       (srfi:vector-for-each
	(lambda (i v)
	  (cond
	   ((a:blob? v) (display (blob-sha256 v) port))
	   (v (let ((msg (format "IMPLEMENTATION ERROR block table directory contains illegal value ~a" v)))
		(logerr "~a\n" msg)
		(error msg)))
	   (else #f))
	  (newline port))
	obj))))
  (if (a:blob? obj)
      (load-block-table!
       (make-block-table
	(blob-block-size obj)
	(blob-blocks-per-blob obj)
	0 store fetch-notation btread btwrite #f)
       obj)
      (let ((bs (car dflt))
	    (bpb (cadr dflt)))
	(make-block-table bs bpb 0 store fetch-notation btread btwrite #f))))

(: bhob->string
   (:blob: number number :block-table:
	   (procedure (:blob:) boolean)	; fetch
	   -> string))
(define bhob->string/logic
  (begin
    (: copy-block ((procedure (:blob:) boolean) ; fetch
		   :blob:			; blob
		   string			; to
		   fixnum			; block size
		   fixnum fixnum
		   -> number))
    (define (copy-block fetch blk to blksize foff toff)
      (or (copy-blob-value *the-registered-stores* blk to blksize foff toff)
	  (begin
	    (fetch blk)
	    (or (copy-blob-value *the-registered-stores* blk to blksize foff toff)
		(raise (make-condition
			&blob-missing-condition
			'status 404
			'message (format "blob ~a missing" (blob-sha256 blk))
			'source 'copy-block))))))
    (define (bhob->string/logic b offset limit t fetch)
      (let ((len (- limit offset))
	    (toff 0))
	(let ((to (make-string/uninit len))
	      (blksize (blob-block-size b))
	      (blkno (quotient offset (block-table-bs t)))
	      (fboff (modulo offset (block-table-bs t))))
	  (if (> fboff 0)
	      (let ((blk (block-table-ref/default t blkno #f))
		    (blksize (min len (- (block-table-bs t) fboff))))
		(copy-block fetch blk to blksize fboff 0)
		(set! blkno (add1 blkno))
		(set! toff blksize)
		(set! len (- len blksize))))
	  (let loop
	      ((blkno blkno)
	       (toff toff)
	       (len len))
	    (cond
	     ((> len blksize)
	      (let ((blk (block-table-ref/default t blkno #f)))
		(copy-block fetch blk to blksize 0 toff)
		(loop (add1 blkno) (+ toff blksize) (- len blksize))))
	     ((eqv? len 0) to)
	     (else
	      (copy-block fetch (block-table-ref/default t blkno #f) to len 0 toff)
	      to))))))
    bhob->string/logic))

(: bhob->string (:blob: :oid: :quorum: number number -> string))
(define (bhob->string b oid quorum offset len)
  (define (fetch obj)
    (aggregate-fetch-blob quorum oid oid obj))
  (bhob->string/logic
   b offset len
   (place/make-block-table oid quorum b)
   fetch))

;; FIXME: This really bad HACK will trigger a re-fetch of missing
;; blobs.  This is to be called ONLY under the precondition that
;; access is already checked!

(: bhob->string:re-check-bhob (:blob: -> boolean))
(define bhob->string:re-check-bhob (lambda (blob) #f))
(: set-bhob->string:re-check-bhob! ((procedure (:blob:) boolean) -> undefined))
(define (set-bhob->string:re-check-bhob! f)
  (set! bhob->string:re-check-bhob f))

(: bhob->string/anyway (:blob: :oid: :quorum: number number -> string))
(define (bhob->string/anyway b oid quorum offset len)
  (define (fetch obj)
    (bhob->string:re-check-bhob b))
  (bhob->string/logic
   b offset len
   (place/make-block-table oid quorum b)
   fetch))

(: %message-xml-parse (:message: (or false :oid:) -> (or boolean :xml-nodelist:)))
(define (%message-xml-parse msg oid)
  (let ((b (get-slot msg 'mind-body))
        (ct (get-slot msg 'content-type)))
    (cond
     ((string? b) (%message-xml-parse-as b ct))
     ((bhob? b)
      (if (and oid
	       (xml-parseable? ct)
	       (html-parseable? ct)
	       )
	  (or (fetch-blob-notation *the-registered-stores* b text/xml)
	      (store-blob-notation
	       *the-registered-stores* b text/xml
	       (delay* (and-let* ((str (bhob->string
					b oid (get-slot msg 'replicates)
					0 (a:blob-size b)))
				  (tree (%message-xml-parse-as str ct))
				  ((node-list? tree)))
				 tree)
		       (blob-sha256 b))))
	  (blob->xml b)))
     ((a:blob? b)
      (or (fetch-blob-notation *the-registered-stores* b text/xml)
	  (store-blob-notation
	   *the-registered-stores* b text/xml
	   (delay* (%message-xml-parse-as
		    (or (fetch-blob-notation *the-registered-stores* b #f)
			(raise (make-condition
				&blob-missing-condition
				'status 404
				'message (format "blob ~a missing" (blob-sha256 b))
				'source '%message-xml-parse)))
		    ct)
		   (blob-sha256 b)))))
     ((and (pair? b) (number? (car b)))
      (%message-xml-parse-as (apply-string-append (cdr b)) ct))
     (else (empty-node-list)))))


(define *local-quorum* (make-quorum (list one-oid)))

(: set-local-quorum! (:quorum: -> undefined))
(define (set-local-quorum! quorum)
  (set! *local-quorum* quorum))

(: replicates-of (:place: --> :quorum:))
(define (replicates-of frame)
  (let ((x (fget frame 'replicates)))
    (cond
     ((quorum? x)
      (if (quorum-anywhere? x)
	  (make-quorum (list (aggregate-entity frame)))
	  x))
     ;; While we translate our stores, make quorums from lists
     ;; ((pair? x) (make-quorum x)) 	; backward compatibility
     (else *local-quorum*))))

(: quorum-anywhere? (:quorum: --> boolean))
(define (quorum-anywhere? quorum)
  (cond-expand
   (annon-maps-to-self
    (and (eqv? (quorum-size quorum) 1)
	 (memq one-oid (quorum-others quorum))))
   (else #f)))

(: replicates-of/xml (:place: --> :xml-element:))
(define (replicates-of/xml frame) ;; true version
  (let ((x (fget frame 'replicates)))
    (cond
     ((quorum? x) (quorum-xml x))
     (else (format "replicates-of*: no quorum on ~a" (aggregate-entity frame))))))

;; message-body
;;
;; return: internal representation (parsed tree) of the body

(: message-body* (:message: :oid: -> :xml-nodelist:))
(define (message-body* msg oid)
  (or (get-slot msg 'body/parsed-xml)
      (let ((tree (guard
		   (ex ((missing-blob-condition? ex)
			(aggregate-fetch-blob
			 (get-slot msg 'replicates) oid
			 (get-slot msg 'mind-action-document)
			 (get-slot msg 'mind-body))
			(%message-xml-parse msg oid))
		       (else (raise ex)))
		   (%message-xml-parse msg oid))))
	(set-slot! msg 'body/parsed-xml tree) tree)))

(: message-body (:message: -> :xml-nodelist:))
(define (message-body msg)		; export
  ;; The caching here is good for about 10:1 speed up.
  (cond
   ((frame? msg)
    (or (get-slot msg 'body/parsed-xml)
	(let ((tree (%message-xml-parse msg #f)))
	  (set-slot! msg 'body/parsed-xml tree) tree)))
   (else (error (format "message-body: not a frame ~a" msg)))))

(: aggregate-body (:place: -> :xml-nodelist:))
(define (aggregate-body frame)
;;   (or (fget frame 'body/parsed-xml)
;;       (respond! frame parsed-xml respond-local #t empty-message))
  (message-body* (aggregate-meta frame) (aggregate-entity frame)))

;; message-body/plain
;;
;; return: a serialised (external) representation of body

(: message-body/plain (:message: -> :body-plain:))
(define (message-body/plain msg)
  ;; The caching here is good for about 10:1 speed up.
  (cond
   ((frame? msg)
    (or (get-slot msg 'mind-body)
	(and-let* ((tree (get-slot msg 'body/parsed-xml))
		   (plain (xml-format tree)))
		  (set-slot! msg 'mind-body plain) plain)))
   (else (error (format "message-body/plain: not a frame ~a" msg)))))

(: aggregate-body/plain (:place: -> :xml-nodelist:))
(define (aggregate-body/plain frame)
  (message-body/plain (aggregate-meta frame)))

(define (message-body/plain-size msg)
  (let ((body (message-body/plain msg)))
    (cond
     ((not body) 0)
     ((a:blob? body)
      (or (a:blob-size body)
	  (blob-notation-size *the-registered-stores* body #f)))
     ((pair? body) (car body))
     (else (string-length body)))))
;;

(: dsssl-message-body (* --> *))
(define (dsssl-message-body msg)
  (cond
   ((procedure? msg) (msg 'body/parsed-xml))
   ((frame? msg) (message-body msg))
   (else (domain-error 'message-body "illegal argument" msg))))
(: dsssl-message-body/plain (* --> string))
(define (dsssl-message-body/plain msg)
  (cond
   ((procedure? msg) (msg 'mind-body))
   ((frame? msg) (message-body/plain msg))
   (else (domain-error 'message-body/plain "illegal argument" msg))))
(: dsssl-message-body/plain-size (* --> number))
(define (dsssl-message-body/plain-size msg)
  (cond
   ((procedure? msg) (body-size (msg 'mind-body)))
   ((frame? msg) (message-body/plain-size msg))
   (else (domain-error 'message-body/plain-size "illegal argument" msg))))

;;*** Some error messages

(define (accept-header-matches-application/soap+xml hdr)
  (equal? hdr application/soap+xml))

(define (make-html-error status location title message msgtxt)
  ;; HACK BEGIN
  ;; TODO: rewite or better ommit this HACK.  Make sure the exception
  ;; handler will return something useful in any case, but still
  ;; annotate the exception with the "secondary" exception case.
  ;; (Right now related to the values of 'location', which, if
  ;; starting with a pair, would have to be a list of strings
  ;; exclusively.
  (if (and (pair? location) (any (lambda (x) (not (string? x))) location))
      (begin
	(logerr "Error handler fixing location ~a\n" location)
	(set! location (map literal location))))
  ;; END HACK
  (let ((name (if (pair? location) (srfi:string-join location "/") location)))
    (make-message
     (property 'content-type text/html)
     ;(property 'dc-identifier name)
     (property 'location location)
     (property 'http-status status)
     (property 
      'body/parsed-xml
      (sxml
       `(html
	 (head (title ,(literal title)))
	 (body (@ (bgcolor "white"))
	       (h1 ,(literal status ": " title))
	       ,@(cond
		  ((string? message) `((p ,message)))
		  ((node-list? message)
		   (node-list-map (lambda (m) `(p ,m)) message))
		  ((pair? message)
		   (map (lambda (m) `(p ,(literal m))) message))
		  (else `((p ,(literal message)))))
	       ;; FIXME this is just to work around things as they where
	       ;; don't rely on this embedding, it will be removed
	       ,@(guard
		  (ex (else (list (literal msgtxt))))
		  (cond
		   ((node-list? msgtxt) msgtxt)
		   ((xml-element? msgtxt) (list msgtxt))
		   (else (sxml msgtxt))))
	       (pre ,(literal 
		      (xml-format
		       (make-soap-receiver-error
			name title message msgtxt)))))))))))

(define (make-soap-error status location nl)
  (make-message
   (property 'content-type application/soap+xml)
   (property 'location (if (pair? location) (srfi:string-join location "/") location))
   (property 'http-status status)
   (property 'body/parsed-xml nl)))

(define (make-error-answer-message request status title msg args)
  (let ((ref (if (frame? request)
		 (lambda (key) (get-slot request key))
		 request)))
    (let ((accept (ref 'accept))
	  (location
	   (append-reverse (or (ref 'destination) '())
			   (or (ref 'location) '()))))
      (if (accept-header-matches-application/soap+xml (ref 'accept))
	  (make-soap-error
	   status location
	   (make-soap-receiver-error location title msg args))
	  (make-html-error status location title msg args)))))

(define (condition-http-status condition)
  (cond
   ((http-effective-condition? condition)
    (http-effective-condition-status condition))
   ((agreement-timeout? condition) 503)
   ((timeout-object? condition) 408)
   (else 400)))

(define (condition->message logtag request condition)
  (if (xml-element? (document-element condition))
      (make-error-answer-message
       request 400
       (let ((title ((sxpath '(* title)) condition)))
	 (if (pair? title) title "Error")) '() condition)
      (receive
       (title msg args rest)
       (condition->fields+ condition)
       (if logtag (logcond logtag title msg args))
       (make-error-answer-message
	request (condition-http-status condition)
	title msg args))))
