;; (C) 2010 Jörg F. Wittenberger see http://www.askemos.org

;;** Helper to wait for messages on a channel.

(%early-once-only
 (define has-been-forwarded-nl
   (sxml `(body (p "Your request has been forwarded.")
		(p "Please note: Forwarding of write requests is asynchronous,
results are not yet available.")))))

(define (write-with-sync-answer-set-slots result text status)
  (if text
      (begin
	(set-slot! result 'mind-body #f)
	(set-slot! result 'content-type text/xml)
	(set-slot! result 'body/parsed-xml text)))

  ;; FIXME set error status if last answer is error and
  ;; dos'nt have 'success, 'error or 'actor 
  (and status (set-slot! result 'http-status status))
  result)

(define (legal-sync-answer-channel-message? obj)
  (or (xml-element? obj) (frame? obj) (condition? obj) (timeout-object? obj)))

(define (write-with-sync-answer answer-function request . args)
  (do ((args args (cddr args))
       (timeout (respond-timeout-interval))
       (actor #f)
       (status #f) 
       (text #f))
      ((null? args)
       (let* ((mbox (make-checked-mailbox 'sync-answer legal-sync-answer-channel-message?))
	      (end (and timeout
			(add-duration *system-time* (make-time 'time-duration 0 timeout))))
	      (tom (and timeout (register-timeout-message! timeout mbox))))
	 (set-slot! request 'reply-channel mbox)
	 (set-slot! request 'request-method 'write)
;;;      this way we would drop the initial synchron answer 
;;;      from (answer-function):
;;;      (answer-function request)
;;;      but we have to check if it is an error frame from a condition handler
;;;      askemos-web-user sends:
;;;      make-soap-receiver-error full-location condition~title ~msg ~args
;;;      xml: <env:Envelope xmlns:env="soap..." 
;;;                         xmlns:a="mind" xmlns:dc="dublincore">
;;;            <env:Body><env:Fault>....</env:Fault></env:Body></env:Envelop>
;;;      or
;;;      make-html-error 400 full-location condition~title ~msg ~args
;;;       ct:text/html
;;;       location:  full-location
;;;       dc-identifier: read-locator full-location
;;;       http-status: 400
;;;       body/parsed-xml: some html
;;;      (let loop ((result (receive-message! mbox))
	 (let loop ((result (answer-function request))
		    (actor actor) 
		    (status status) 
		    (text text))
	   (cond
	    ((timeout-object? result)
	     (if (and end (srfi19:time<? *system-time* end))
		 (begin
		   (set! tom (register-timeout-message! timeout mbox))
		   (loop (receive-message! mbox) actor status text))
		 (raise-service-unavailable-condition (literal (get-slot request 'destination)))))
	    ((agreement-timeout? result)
	     (if tom (cancel-timeout-message! tom))
	     result)
	    ((condition? result)
	     (if tom (cancel-timeout-message! tom))
	     result)
	    ((not (frame? result))
	     (logerr "E: sync-write: Response ~s is not a frame from ~s\n" result answer-function)
	     (loop (receive-message! mbox) actor status text))
	    #;((get-slot result 'http-status) result)
	    ((get-slot result 'http-status) =>
	     (lambda (status)
	       (if (eq? 102 status)
		   (begin
		     (cancel-timeout-message! tom)
		     (set! tom (register-timeout-message! timeout mbox))
		     (set! end (add-duration *system-time* (make-time 'time-duration 0 timeout)))
		     (loop (receive-message! mbox) actor status text))
		   result)
	       result))
	    ((not (get-slot result 'caller))
	     ;; FIXME: need recognisation of exception error messages
	     ;; (is-realy-error-response? result))
	     ;;(logerr "E: sync-write: got error response!\n")
	     result)
	    (else
	     (let* ((answer (document-element (message-body result)))
		    (actor
		     (or (string->oid (data ((sxpath '(final)) answer))) 
			 actor))
		    (status 
		     (or (string->number (data ((sxpath '(status)) answer)))
			 status))
		    (text
		     ;; Prefer SOAP over own protocol.
		     (or (let ((t ((sxpath '(Envelope)) answer))) 
			   (and (pair? t) t))
			 (let ((t ((sxpath '(text *)) answer))) 
			   (and (pair? t) t)) text)))
	       (cond
		((eq? (gi answer) 'actor)
		 ;;(logerr "I: sync-write: wait for ~a\n" actor)
		 (loop (receive-message! mbox) actor status text))
		((or (eq? (gi answer) 'success)
		     (eq? (gi answer) 'error)
		     (eq? (get-slot result 'caller) actor))
		 (if tom (cancel-timeout-message! tom))
		 ;;(logerr "I: sync-write: ~8a ~a\n"
		 ;;	 (or (gi answer) 'actor) (get-slot result 'caller))
		 (write-with-sync-answer-set-slots result text status))
		((eq? answer (document-element has-been-forwarded-nl))
		 ;;(logerr "I: sync-write: fwd from ~a\n"
		 ;; (get-slot result 'caller))
		 (loop (receive-message! mbox) actor status text))
		(else
		 (if tom (cancel-timeout-message! tom))
		 ;; 		 (logerr "E: sync-write: IGNORE ~a\n~a\n"
		 ;; 			 (get-slot result 'caller)
		 ;; 			 (xml-format answer))
		 ;; 		 (loop (receive-message! mbox) actor status text)
		 (write-with-sync-answer-set-slots result text status)))))))))
    (if (null? (cdr args))
	(error " E: sync-write: coding error key ~a needs an argument"
	       (car args)))
    (case (car args)
      ((to:)     (set-slot! request 'dc-identifier   (cadr args)))
      ((body:)
       (set-slot! request 'body/parsed-xml (cadr args))
       (set-slot! request 'mind-body #f)
       (set-slot! request 'content-type text/xml))
      ((actor:)  (set! actor  (cadr args)))
      ((status:) (set! status (cadr args)))
      ((text:)   (set! text   (cadr args)))
      ((timeout:) (set! timeout (cadr args)))
      (else      (set-slot! request (car args) (cadr args))))))
