;; (C) 2000 - 2002, 2013 Jörg F. Wittenberger see http://www.askemos.org

;;** XSLT

;;*** DSSSL

;; The extension language namespace "NameSpaceDSSSL" is for
;; experimental experimental syntax.  It implements a subset of DSSSL
;; (currently in a not completely conformant manner, which helps to
;; avoid quoting issues when embedding expressions in XML arguments)
;; to the extend as the features where available anyway or needed for
;; client code.  Because the XPath is not yet avail., xslt operations,
;; which have an attribute with an XPath expressions are "mirrored"
;; with the same semantics but the XPath replaced with dsssl.  In
;; addition to this language some experimental functions may be
;; avail. within some versions and silently disappear in further
;; releases.  For trusted code this means don't depend on anything,
;; which has neither a XSLT or DSSSL equvialent.

;; There is an own extention "dsssl:copy-of".  It's supposed to be
;; used to fetch values from the input document.
;;
;; It has a "select" attribute holding the DSSSL expression to be
;; delivered and a content, which serves as an example text.
;;
;; Usage:
;;(make element
;; gi: 'any ns: 'http://www.askemos.org/2000/NameSpaceDSSSL
;; attributes: '((select "<some DSSSL expression>"))
;; (literal "An example value of the DSSSL result."))

;; Forward definition, for eval-sane etc. see section evaluation later
;; on.

(define-transformer xsl-element-element "xsl:element"
  (apply-transformer
   use: xsl-element-default
   sosofos: (make-xml-element
	     (string->symbol (@! 'name))
	     (and-let* ((a (@? 'namespace))) (string->symbol a))
	     '()
	     (%children (sosofo)))))

(define-transformer xsl-comment-element "xsl:comment"
  (make-xml-comment (data (apply-transformer use: xsl-element-default))))

(define-transformer xsl-pi-element "xsl:pi"
  (make-xml-pi (string->symbol (@! 'name))
	       (data (transform sosofos: (%children (sosofo))))))

(define-transformer xsl-apply-templates "xsl:apply-templates"
  ;; TODO support select attribute
  (let ((selection (let ((select (@? 'select)))
		     (if select
			 ((make-xpath-match (xpath:parse-expr select) (namespaces))
			  (current-node) (root-node) (environment))
			 (%children (current-node)))))
	(rest (xslt-process-node-list (%%proc-chain))))
    (transform
     variables: (node-list-reduce
                 (%children (sosofo))
                 (lambda (init n)
                   (if (and (eq? (gi n) 'with-param)
                            (eq? (ns n) namespace-xsl)
                            )
                       (let ((name (required-attribute-string 'name n "with-param"))
			     (select (attribute-string 'select n)))
			 (if select
			     (bind-xsl-value/xpath-expr
			      init name
			      select (namespaces)
			      (current-node) (root-node) (environment)
			      'with-param)
			     (bind-xsl-value/walk
			      init name
			      (transform
			       sosofos: (%children n)
			       process: rest
			       variables: init)
			      'with-param)))
                       init))
                 (make-xsl-variables (xsl-parameters (environment))) ; would be '(environment') for dyn scope
                 )
     ;; das self-node hilf dem default von (rules->process) müsste aber raus
     current-node: selection
     sosofos: selection
     ancestors: (cons (current-node) (ancestors))
     process: (mode-choice (and-let* ((md (@? 'mode)))
				     ;; FIXME should evaluate ns prefix!?
				     (string->symbol md))))))

(define-transformer dsssl-apply-templates "dsssl:apply-templates"
  (let* ((att (@! 'select))
	 (str (if (string=? att "#CONTENT") (data (sosofo)) att))
         (expr (string->dsssl-proc2 str (ns (sosofo))))
         (selection (if (procedure? expr)
                        (guard (c (else (dsssl-exception-handler
					 "d:apply-templates" c str)))
			       (apply-transformer use: expr sosofos: (current-node)))
                        (raise-message "dsssl:apply-templates ~a in \"~a\""
				       expr str))))
    (transform
     ;; das self-node hilf dem default von (rules->process) müsste aber raus
     current-node: selection
     ancestors: (cons (current-node) (ancestors))
     sosofos: selection
     process: (mode-choice (and-let* ((md (@? 'mode)))
				     ;; FIXME should check ns prefix!?
				     (string->symbol md))))))

(define-transformer xsl-call-template "xsl:call-template"
  (transform
   variables: (node-list-reduce
               (%children (sosofo))
               (let ((rest (xslt-process-node-list (%%proc-chain))))
		 (lambda (init n)
                 (if (and (eq? (gi n) 'with-param)
                          (eq? (ns n) namespace-xsl)
                          )
                     (let ((select (attribute-string 'select n)))
		       (if select
			   (bind-xsl-value/xpath-expr
			    init (required-attribute-string 'name n "with-param")
			    select (namespaces)
			    (current-node) (root-node) (environment)
			    'with-param)
			   (bind-xsl-value/walk
			    init (required-attribute-string 'name n "with-param")
			    (transform
			     sosofos: (%children n)
			     process: rest
			     variables: (environment))
			    'with-param)))
                     init)))
               (make-xsl-variables (xsl-parameters (environment))))
   process: (mode-choice (@! 'name))))

(define-transformer dsssl-call-template "dsssl:call-template"
  (let* ((att (@! 'name))
         (expr (string->dsssl-proc2 att (ns (sosofo))))
         (name (if (procedure? expr)
                   (guard (c (else (dsssl-exception-handler
				    "d:call-template" c att)))
			  (apply-transformer use: expr sosofos: (current-node)))
                   (raise-message "dsssl:call-template ~a in \"~a\"" expr att))))
    (transform
     variables: (node-list-reduce
                 (%children (sosofo))
                 (lambda (init n)
                   (if (and (eq? (gi n) 'with-param)
                            (eq? (ns n) namespace-xsl)
                            )
                       (let ((select (attribute-string 'select n)))
			 (if select
			     (bind-xsl-value/xpath-expr
			      init (required-attribute-string 'name n "d:with-param")
			      select (namespaces)
			      (current-node) (root-node) (environment)
			      'with-param)
			     (bind-xsl-value/walk
			      init (required-attribute-string 'name n "d:with-param")
			      (transform
			       sosofos: (%children n)
			       process: (%%proc-chain) ; XXX is this required here?
			       variables: (environment))
			      'with-param)))
                       init))
                 (make-xsl-variables (xsl-parameters (environment))))
     process: (mode-choice (data name)))))

(define-transformer dsssl-for-each-transformer-serial "dsssl:for-each"
  (let* ((att (@! 'select))
         (sel (string->dsssl-proc2 att (ns (sosofo)))))
    (if (procedure? sel)
        (let ((rest (xslt-process-node-list (%%proc-chain)))
              (new-ancestors (cons (current-node) (ancestors))))
          (node-list-map
           (lambda (cn)
             (transform
              ;; variables: ...cn.. FIXME dsssl
              current-node: cn  ;??? FIXME
              ancestors: new-ancestors
              sosofos: (%children (sosofo))
              process: rest))
           (guard (c (else (dsssl-exception-handler "d:for-each" c sel)))
		  (apply-transformer use: sel))))
	(raise-message "dsssl:for-each ~a in ~a" sel att))))

(define-transformer dsssl-for-each-transformer-parallel "dsssl:for-each"
  (let* ((att (@! 'select))
         (sel (string->dsssl-proc2 att (ns (sosofo)))))
    (if (procedure? sel)
        (let ((rest (xslt-process-node-list (%%proc-chain)))
              (new-ancestors (cons (current-node) (ancestors))))
	  (node-list-map-parallel
	   thread-signal-timeout! raise #f #f
	   (lambda (cn)
	     (transform
	      ;; variables: ...cn.. FIXME dsssl
	      current-node: cn  ;??? FIXME
	      ancestors: new-ancestors
	      sosofos: (%children (sosofo))
	      process: rest))
	   (guard (c (else (dsssl-exception-handler "d:for-each" c sel)))
		  (apply-transformer use: sel))))
        (raise-message "dsssl:for-each ~a in ~a" sel att))))

(define dsssl-for-each-transformer dsssl-for-each-transformer-parallel)

(define-transformer xsl-for-each-transformer-parallel "xsl:for-each"
  ;; FIXME: I have no idea why `make-xpath-selection`appears to be
  ;; called with the wrong number of arguments here.
  (let ((selected (;; (make-xpath-selection (@! 'select) (namespaces))
		   (xpath-eval (xpath:parse-expr (@! 'select)))
		   (current-node) (root-node) (environment))))
    (if (node-list-empty? selected) (empty-node-list)
	(let ((rest (xslt-process-node-list (%%proc-chain)))
	      (new-ancestors (cons (current-node) (ancestors))))
	  (node-list-map-parallel
	   thread-signal-timeout! raise #f #f
	   (lambda (cn)
	     (transform
	      ;; variables: ...cn.. FIXME dsssl
	      current-node: cn  ;??? FIXME
	      ancestors: new-ancestors
	      sosofos: (%children (sosofo))
	      process: rest))
	   selected)))))

(define xsl-for-each-transformer xsl-for-each-transformer-parallel)

(define-transformer dsssl-if-transformer "dsssl:if"
  (let* ((att (@! 'test))
         (test (string->dsssl-proc2 att (ns (sosofo)))))
    (if (procedure? test)
        (if (guard (c (else (dsssl-exception-handler "d:if" c att)))
		   (apply-transformer use: test))
            (transform
             sosofos: (%children (sosofo))
             process: (xslt-process-node-list (%%proc-chain)))
            (empty-node-list))
	(raise-message "~a:if ~a in ~a" (ns (sosofo)) test att))))

(define-transformer dsssl-copy-of-transformer "dsssl:copy-of"
  (let* ((att (@! 'select))
	 (str (if (string=? att "#CONTENT")
		  (data (sosofo))
		  att))
         (expr (string->dsssl-proc2 str (ns (sosofo))))
         (result (if (procedure? expr)
                     (guard (c (else (dsssl-exception-handler
				      "d:copy-of" c str)))
			    (apply-transformer use: expr sosofos: (current-node)))
                     (format #f "~s" expr))))
    (if (node-list? result) result (make-xml-literal result))))

(define-transformer dsssl3-copy-of-transformer "dsssl:copy-of"
  (let* ((att (@? 'select))
	 (str (if att
		  att
		  (data (sosofo))))
         (expr (string->dsssl-proc2 str namespace-dsssl3))
         (result (if (procedure? expr)
                     (guard (c (else (dsssl-exception-handler
				      (string-append "d:" (symbol->string (ns (sosofo))))
				      c str)))
			    (apply-transformer use: expr sosofos: (current-node)))
                     (raise (format #f "~s" expr)))))
    (if (node-list? result) result (make-xml-literal result))))

(define-transformer dsssl2-form-transformer "dsssl:form"
  (let* ((methatt (@! 'method))
	 (methval (cond
		   ((string=? methatt "post") 'post)
		   ((string=? methatt "get") 'get)
		   ((string-ci=? methatt "post") 'post)
		   ((string-ci=? methatt "get") 'get)
		   (else (if (string-ci=?
			      (apply-transformer use: (string->dsssl-proc2 methatt (ns (sosofo))))
			      "post")
			     'post 'get))))
         (name-att (let ((a (find-first-attribute
			     (lambda (a) (eq? (xml-attribute-name a) 'name))
			     (sosofo))))
		     (if a (list a) '())))
         (namespace (xthe (or false symbol) #f))
	 (action-missing (xthe boolean #t)) ; KLUDGE, better rewrite functional style
         (result (cons*
                  (make-xml-attribute
                   'enctype #f
                   (or (attribute-string 'enctype (sosofo)) "x-urlform-encoded"))
                  (make-xml-attribute 'method #f (symbol->string methval))
                  name-att)))
    (do ((atts (or (attributes (sosofo)) (empty-node-list)) (cdr atts)))
        ((null? atts)
	 (if action-missing
	     (set! result (cons (make-xml-attribute
				 'action #f (current-message format-location methval))
				result)))
	 result)
      (let ((name (xml-attribute-name (car atts)))
            (nmsp (xml-attribute-ns (car atts)))
            (val (xml-attribute-value (car atts))))
	(if (eq? name 'action)
	    (set! action-missing #f))
	(case name
	 ((xmlns) (set! namespace (string->symbol val)))
	 ((method name enctype) #t)			;; prepared before
	 (else
	  (if (or (eq? nmsp namespace-dsssl2)
		  (eq? nmsp namespace-dsssl3))
	      (let* ((expr (string->dsssl-proc2 val nmsp))
		     (value (if (procedure? expr)
				(guard (c (else (dsssl-exception-handler
						 "d:form" c val)))
				       (apply-transformer use: expr sosofos: (current-node)))
				(raise-message "dsssl ~a in \"~a\""
					       expr val))))
		(set! result
		      (cons (make-xml-attribute
			     name #f
			     (cond 
			      ((string? value) value)
			      ((symbol? value) (symbol->string value))
			      ((node-list? value) (data value))
			      (else (format #f "~a" value))))
			    result)))
	      (set! result `(,(car atts) . ,result)))))))
      (make-xml-element (gi (sosofo)) namespace result
                        (transform
                         sosofos: (%children (sosofo))
                         process: (xslt-process-node-list (%%proc-chain))))))

(define-transformer dsssl-form-transformer "obsolete, old, ad hoc dsssl:form"
  (let* ((methatt (@! 'method))
         (action (find-first-attribute
                  (lambda (x) (eq? (xml-attribute-name x) 'action))
                  (sosofo)))
         (expr (string->dsssl-proc2
                (if action
                    (xml-attribute-value action)
                    (error "dsssl:form missing required attribute action"))
                (ns (sosofo))))
         (result (guard
                  (c (else (dsssl-exception-handler "d:form" c (xml-attribute-value action))))
                  (if (or (eq? (xml-attribute-ns action)
                               namespace-dsssl)
                          (eq? (xml-attribute-ns action) 'dsssl))
                      (apply-transformer use: expr)
		      (let ((type (if (string-ci=? methatt "post") 'post 'get))
			    (dst (apply-transformer use: expr)))
			(let ((loc (cond
				    ((pair? dst)
				     (append (reverse! dst) ((current-message) 'location)))
				    ((null? dst) ((current-message) 'location))
				    (else (cons dst ((current-message) 'location))))))
			  (current-message format-location type loc))))))
         (name-att (let ((a (find-first-attribute
			     (lambda (a) (eq? (xml-attribute-name a) 'name))
			     (sosofo))))
		     (if a (list a) '()))))
    (make-xml-element
     'form #f
     (cons*
      (make-xml-attribute 'action #f result)
      (make-xml-attribute
       'enctype #f
       (or (attribute-string 'enctype (sosofo)) "x-urlform-encoded"))
      (make-xml-attribute 'method #f methatt)
      name-att)
     (transform sosofos: (%children (sosofo))))))

(register-tag-transformer!
 *namespace-environment* namespace-dsssl2
 'apply-templates dsssl-apply-templates)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl3
 'apply-templates dsssl-apply-templates)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl2 'call-template dsssl-call-template)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl3 'call-template dsssl-call-template)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl2 'if dsssl-if-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl3 'if dsssl-if-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl2 'copy-of dsssl-copy-of-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl3 'copy-of dsssl3-copy-of-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl2 'for-each dsssl-for-each-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl3 'for-each dsssl-for-each-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl2 'form dsssl2-form-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl3 'form dsssl2-form-transformer)

;; For backward compatibility, to be removed.

(register-tag-transformer!
 *namespace-environment* namespace-dsssl
 'apply-templates dsssl-apply-templates)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl 'call-template dsssl-call-template)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl 'if dsssl-if-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl 'copy-of dsssl-copy-of-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl 'for-each dsssl-for-each-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-dsssl 'form dsssl-form-transformer)

(register-tag-transformer!
 *namespace-environment* 'dsssl 'copy-of dsssl-copy-of-transformer)

(register-tag-transformer!
 *namespace-environment* 'dsssl 'if dsssl-if-transformer)

(register-tag-transformer!
 *namespace-environment* 'dsssl 'for-each dsssl-for-each-transformer)

(register-tag-transformer!
 *namespace-environment* 'dsssl 'form dsssl-form-transformer)

;; XSLT

(define-transformer xsl-text-element "xsl:text"
  (transform sosofos: (%children (sosofo))))

(define-transformer xsl-choose-element "xsl:choose"
  ;; FIXME: doesn't yet care about name spaces.
  (let loop ((alt (select-elements (%children (sosofo)) 'when)))
    (if
     (node-list-empty? alt)
     (let ((other (select-elements (%children (sosofo)) 'otherwise)))
       (if
        (node-list-empty? other)
        other
        (transform
         sosofos: (%children (node-list-first other))
         process: (xslt-process-node-list (%%proc-chain)))))
     (let* ((nl (node-list-first alt))
            (att (required-attribute-string 'test nl (ns nl))))
       (if (eq? (ns nl) namespace-xsl)
	   (if ((make-xpath-match (xpath:parse-expr att) (namespaces))
		(current-node) (root-node) (environment))
	       (transform
		sosofos: (%children nl)
		process: (xslt-process-node-list (%%proc-chain)))
	       (loop (node-list-rest alt)))
	   (let ((test (string->dsssl-proc2 att (ns nl))))
	     (if (procedure? test)
		 (if (guard (c (else (dsssl-exception-handler "d:when" c att)))
			    (apply-transformer use: test))
		     (transform
		      sosofos: (%children nl)
		      process: (xslt-process-node-list (%%proc-chain)))
		     (loop (node-list-rest alt)))
		 (raise-message "dsssl:when ~a in ~a" test att))))))))

(define-transformer xsl-if-transformer "xsl:if"
  (if ((make-xpath-match (xpath:parse-expr (@! 'test)) (namespaces))
       (current-node) (root-node) (environment))
      (transform
       sosofos: (%children (sosofo))
       process: (xslt-process-node-list (%%proc-chain)))
      (empty-node-list)))

(define-transformer xsl-copy-of-transformer "xsl:copy-of"
  ((make-xpath-selection (@! 'select) (namespaces))
   (current-node) (root-node) (environment)))

(define-transformer xsl-value-of-transformer "xsl:value-of"
  (data
   ((make-xpath-selection (@! 'select) (namespaces))
    (current-node) (root-node) (environment))))

(define-transformer xsl-copy-transformer "xsl:copy"
  (let* ((select (@? 'select))
	 (selected (if select
		       ((make-xpath-selection select (namespaces))
			(current-node) (root-node) (environment))
		       (current-node))))
    (cond
     ((node-list-empty? selected) (empty-node-list))
     ((not (node-list-empty? (node-list-rest selected)))
      (error "xsl:copy selected more than one node"))
     (else
      (let ((nl (node-list-first selected)))
	(apply-transformer
	 use: xsl-element-default
	 sosofos: (make-xml-element
		   (gi nl)
		   (ns nl)
		   (filter
		    (lambda (a)
		      (or (eq? (xml-attribute-name a) 'xmlns) (eq? (xml-attribute-ns a) 'xmlns)))
		    (xml-element-attributes nl))
		   (%children (sosofo)))))))))

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'apply-templates xsl-apply-templates)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'call-template xsl-call-template)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'element xsl-element-element)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'comment xsl-comment-element)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'processing-instruction xsl-pi-element)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'text xsl-text-element)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'choose xsl-choose-element)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'if xsl-if-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'copy-of xsl-copy-of-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'value-of xsl-value-of-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'for-each xsl-for-each-transformer)

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'copy xsl-copy-transformer)
