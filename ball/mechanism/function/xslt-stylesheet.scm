;; (C) 2000 - 2002, 2013 Jörg F. Wittenberger see http://www.askemos.org

(define (raise-message fmt . args)
  (raise (make-condition &message 'message (apply format fmt args))))

(define (dsssl-exception-handler tag c str)
  (cond
   ((or (timeout-object? c) (message-condition? c)) (raise c))
   (else
    (receive
     (title msg args rest) (condition->fields+ c)
     (raise-message "~a ~a ~a ~a in \"~a\"" tag title msg args str)))))

;;** XSLT

;;*** DSSSL

;; The extension language namespace "NameSpaceDSSSL" is for
;; experimental experimental syntax.  It implements a subset of DSSSL
;; (currently in a not completely conformant manner, which helps to
;; avoid quoting issues when embedding expressions in XML arguments)
;; to the extend as the features where available anyway or needed for
;; client code.  Because the XPath is not yet avail., xslt operations,
;; which have an attribute with an XPath expressions are "mirrored"
;; with the same semantics but the XPath replaced with dsssl.  In
;; addition to this language some experimental functions may be
;; avail. within some versions and silently disappear in further
;; releases.  For trusted code this means don't depend on anything,
;; which has neither a XSLT or DSSSL equvialent.

;; There is an own extention "dsssl:copy-of".  It's supposed to be
;; used to fetch values from the input document.
;;
;; It has a "select" attribute holding the DSSSL expression to be
;; delivered and a content, which serves as an example text.
;;
;; Usage:
;;(make element
;; gi: 'any ns: 'http://www.askemos.org/2000/NameSpaceDSSSL
;; attributes: '((select "<some DSSSL expression>"))
;; (literal "An example value of the DSSSL result."))

;; Forward definition, for eval-sane etc. see section evaluation later
;; on.

(define dsssl-argument-list
  '(
    current-node
    ancestors
    process-children
    process-node-list
    grove-root
    xsl-variable
    service-level
    find
    fetch
    me msg                              ; ??? really exposed? can't
					; avoid it
    ))

;; (define -macro (define-dsssl-sosofo symbol body)
;;   `(define (,symbol . ,dsssl-argument-list) ,body))

;; (define -macro (lambda-dsssl-sosofo body)
;;   `(lambda ,dsssl-argument-list ,body))

;; Transform a procedure as created by the evaluation of a dsssl
;; expression into a transformer spec for the xml-walk.

(: cleanup-xslt-value (* --> *))
(define (cleanup-xslt-value r)
  (let cleanup-xslt-value ((r r))
    (if (and (pair? r) (node-list-empty? (cdr r)))
	(cleanup-xslt-value (car r))
	r)))

(define-syntax bind-xsl-value/expr
  (syntax-rules ()
    ((_ context name expr tag)
     (bind-xsl-value/string
      context name (delay* expr tag)))))

(define-syntax bind-xsl-value/walk
  (syntax-rules ()
    ((_ context name expr tag)
     (bind-xsl-value/string
      context name (delay* (cleanup-xslt-value expr) tag)))))

(define-syntax string-or-symbol-as-string
  (syntax-rules ()
    ((_ x)
     (cond
      ((string? x) x)
      ((symbol? x) (string->symbol x))
      (else (raise-message "xsl template application from dsssl: invalid parameter name ~a" x))))))

(define (make-dsssl-transformer proc)
  (lambda-transformer
   "annonymous dsssl transformer"
   (let ()
     (define (fetch-grove-root nl) ($< 'grove-root))

     (define (fetch-other dst . args)
       (let ((ans (apply function-fetch-other (current-place) dst args)))
	 (if (node-list? ans)
	     (document-element ans)
	     ans)))

     (define (xsl-template name . args)
       (transform
	variables: (bind-xsl-values
		    (make-xsl-variables (xsl-parameters (environment)))
		    (let loop ((args args)
			       (vars '()))
		      (cond
		       ((null? args) vars)
		       ((eq? (car args) param:)
			(loop (cdddr args)
			      (alist-cons (string-or-symbol-as-string (cadr args)) (caddr args) vars)))
		       ;; Secondary syntax (introduced 2011-07-08):
		       ;; list (param: <name> <value>) are accepted.
		       ;;
		       ;; This is especially useful with srfi-49 identiation (saves lines).
		       ((and (pair? (car args)) (eq? (caar args) param:))
			(loop (cdr args)
			      (alist-cons (string-or-symbol-as-string (cadar args)) (caddar args) vars)))
		       (else (raise-message "error in xsl-variable/template call ~a" (car args))))))
	process: (mode-choice name)))

     (let ((result (proc
		    (lambda () (current-node))     ; current-node (public)
		    (ancestors)	                   ; derive parents here (private)
		    (lambda ()                     ; process-children (public, broken)
		      (transform sosofos: (%children (sosofo))
				 ancestors: (cons (sosofo) (ancestors)))) ; really sosofo?
		    (lambda (snl)                  ; process-node-list (public)
		      (transform sosofos: snl
				 ancestors: (cons (sosofo) (ancestors)))) ;;??? evtl: '()
		    fetch-grove-root               ; grove-root (public)
		    (lambda (name . rest)          ; xsl-variable (public)
		      (if (eq? name apply:)
			  (cleanup-xslt-value (apply xsl-template rest))
			  ($ (data name))))
		    ;; TODO: this ought to be cached
		    ;; Or maybe it should be a parameter now that we have them?
		    ;; At least it might be better not pre-computed,
		    ;; since it's used rarely in practice.
		    ;;
		    ;; (make-service-level ((current-place) 'protection)
		    ;; 			(or ((current-message) 'capabilities)
		    ;; 			    (begin
		    ;; 			      (logerr "no capabilities found at ~a"
		    ;; 				      ((current-place) 'get 'id))
		    ;; 			      '())))
		    (lambda subset
		      (apply
		       (make-service-level ((current-place) 'protection)
					   (or ((current-message) 'capabilities)
					       (begin
						 (logerr "no capabilities found at ~a"
							 ((current-place) 'get 'id))
						 '())))
		       subset))
		    (lambda (name)                 ; find (public)
		      (function-mind-default-lookup (current-place) (current-message) name))
		    fetch-other                    ; fetch (public)
		    (current-place) (current-message))))
       (or (and (pair? result)
		(or (symbol? (car result))
		    (and (pair? (car result)) (symbol? (caar result))))
		(sxml result))
	   result)))))

(define (make-dsssl-process proc proc-chain)
  (let ((transformer (make-dsssl-transformer proc)))
    (lambda-process "dsssl process" (proc-chain) (apply-transformer use: transformer))))

;; FIXME use make-xslt-optimized-rules here.

(define (make-element-rules elements proc-chain)
  (define ssf
    (lambda-process
     "dsssl element rules" (proc-chain)
     (cond
      ((sxp:element? (sosofo) (root-node) (environment))
       (let ((entry (assq (gi (sosofo)) elements))) ; FIXME use match-element
         (if entry
             (apply-transformer use: (cdr entry) current-node: (sosofo))
             (transform
	      ancestors: (cons (sosofo) (ancestors))
	      sosofos: (%children (sosofo))
	      process: ssf))))
      (else (sosofo)))))
  ssf)

;; There is no real reason to use the uncached version except if you
;; want to provide an interactive interpreter, or something simillar,
;; which amost never evaluates the same expression again until next
;; sleep phase (when all short term memoizations are forgotten to
;; unload memory).

(define (syntax+string->dsssl-proc syntax eval str)
  (guard
   (condition
    ((or (timeout-object? condition) (message-condition? condition))
     (raise condition))
    (else (receive
	   (title msg args rest) (condition->fields condition)
	   (raise-message "~a ~a ~a in ~a" title msg args str))))
   (let ((listing (call-with-input-string str syntax)))
     (let loop ((listing listing)
		(elements '())
		(root '(process-children)))
       (cond
	((null? listing)
	 (lambda-transformer
	  "syntax+string->dsssl-proc"
	  ;; FIXME use make-xslt-optimized-rules here.
	  ;; evtl so um das (mode-choice #f) zu ermöglichen
	  ;;            (let* ((proc (make-dsssl-process
	  ;;			  (eval-sane `(lambda ,dsssl-argument-list ,root))
	  ;;			  (make-element-rules elements proc-chain))))
	  ;;              ;;use xml-walt*1 instead
	  ;;              (xml-walk-down 
	  ;;               sosofos: self-node        ; incoming request
	  ;;               process: proc
	  ;;	       continue: (lambda (m) proc)))))
	  (let ((p (make-element-rules elements (%%proc-chain))))
	    (transform
	     sosofos: (current-node)        ; incoming request
	     process: (make-dsssl-process
		       (eval `(lambda ,dsssl-argument-list ,root))
		       p)))))
	((not (pair? (car listing)))
	 (raise-message "dsssl ~a not valid: ~a in >>~a<<" syntax (car listing) str))
	((eq? (caar listing) 'element)
	 (loop (cdr listing)
	       `((,(cadar listing)
		  . ,(make-dsssl-transformer
		      (eval
		       `(lambda ,dsssl-argument-list ,(caddar listing)))))
		 . ,elements)
	       root))
	((eq? (caar listing) 'root)
	 (loop (cdr listing) elements (cadar listing)))
	((eq? (caar listing) 'default))
	(else 
	 (make-dsssl-transformer
	  (eval `(lambda ,dsssl-argument-list ,(car listing))))))))))

(define (syntax+string->dsssl-proc/2013 syntax eval str)
  (guard
   (condition
    ((or (timeout-object? condition) (message-condition? condition))
     (raise condition))
    (else (receive
	   (title msg args rest) (condition->fields condition)
	   (raise-message "~a ~a ~a in ~a" title msg args str))))
   (let ((listing (call-with-input-string str syntax)))
     (let loop ((listing listing)
		(elements '())
		(definitions '())
		(root '(process-children)))
       (cond
	((null? listing)
	 (receive
	  (elements root)
	  (eval
	   `(begin
	      ,@(reverse! definitions)
	      (values
	       (map
		(lambda (element)
		  `(,(car element) . `(lambda ,dsssl-argument-list ,(cdr element))))
		,elements)
	       (lambda ,dsssl-argument-list ,root))))
	  ;; Crazy enough: but should be changed anyway.
	  (for-each
	   (lambda (er)
	     (set-cdr! er (make-dsssl-transformer (cdr er))))
	   elements)
	  (lambda-transformer
	  "syntax+string->dsssl-proc"
	  ;; FIXME use make-xslt-optimized-rules here.
	  ;; evtl so um das (mode-choice #f) zu ermöglichen
	  ;;            (let* ((proc (make-dsssl-process
	  ;;			  (eval-sane `(lambda ,dsssl-argument-list ,root))
	  ;;			  (make-element-rules elements proc-chain))))
	  ;;              ;;use xml-walt*1 instead
	  ;;              (xml-walk-down 
	  ;;               sosofos: self-node        ; incoming request
	  ;;               process: proc
	  ;;	       continue: (lambda (m) proc)))))
	  (let ((p (make-element-rules elements (%%proc-chain))))
	    (transform
	     sosofos: (current-node)        ; incoming request
	     process: (make-dsssl-process root p))))))
	((not (pair? (car listing)))
	 (raise-message "dsssl not valid: ~a in ~a" (car listing) str))
	((eq? (caar listing) 'define)
	 (loop (cdr listing)
	       elements
	       (cons (car listing) definitions)
	       root))
	((eq? (caar listing) 'element)
	 (loop (cdr listing)
	       `((,(cadar listing)
		  . ,(caddar listing))
		 . ,elements)
	       definitions
	       root))
	((eq? (caar listing) 'root)
	 (loop (cdr listing) elements definitions (cadar listing)))
	((eq? (caar listing) 'default))
	(else
	 (make-dsssl-transformer
	  (eval `(lambda ,dsssl-argument-list ,@(reverse! definitions) . ,listing)))))))))

(define *string->dsssl-cache* (the (or false (struct <cache>)) #f))

(define *sugar-string->dsssl-cache* (the (or false (struct <cache>)) #f))

(define *sweet-string->dsssl-cache* (the (or false (struct <cache>)) #f))

(cond-expand
 (never

  (define (make-xslt-call-cache name)
    (make-cache
     name
     string=?
     #f ;; state
     ;; miss:
     (lambda (cs k) #t)
     ;; hit
     #f ;; (lambda (c es) #t)
     ;; fulfil
     #f ;; (lambda (c es results))
     ;; valid?
     (lambda (es) #t)
     ;; delete
     #f ;; (lambda (cs es) #f)
     ))
  (define (xslt-call-cache-ref cache key thunk)
    (cache-ref cache key thunk))

  )
 (else
  (define (make-xslt-call-cache name)
    (string-make-table))
  (define (xslt-call-cache-ref cache key thunk)
    (let* ((entry (string-table-ref
		   cache key
		   (lambda ()
		     (let ((entry (list (make-mutex key))))
		       (string-table-set! cache key entry)
		       entry))))
	   (mux (car entry)))
      (if (mutex? mux)
	  (with-mutex
	   mux
	   (if (eq? (car entry) mux)
	       (let ((value (thunk)))
		 (set-cdr! entry value)
		 (set-car! entry values)
		 value)
	       (cdr entry)))
	  (cdr entry))))
    ))

(define (dsssl-init)
  (set! *string->dsssl-cache* (make-xslt-call-cache "*string->dsssl-cache*"))
  (set! *sugar-string->dsssl-cache* (make-xslt-call-cache "make-xslt-call-cache"))
  (set! *sweet-string->dsssl-cache* (make-xslt-call-cache "string->bail")))

(define (string->dsssl-proc str)
  (xslt-call-cache-ref *string->dsssl-cache* str
	     (lambda () (syntax+string->dsssl-proc read-all-expressions eval-sane str))))

(define (sugar-string->dsssl-proc str)
  (xslt-call-cache-ref *sugar-string->dsssl-cache* str
	     (lambda () (syntax+string->dsssl-proc sugar-read-all eval-sane str))))

(define (sweet-string->dsssl-proc/2013 str)
  (xslt-call-cache-ref
   *sweet-string->dsssl-cache* str
   ;; FIXME: eval-sane is NOT correct here.  It's just a place holder.
   (lambda () (syntax+string->dsssl-proc/2013 sweet-read-all eval-sane str))))

(dsssl-init)

(define (string->dsssl-proc2 str ns)
  (case ns
    ((http://www.askemos.org/2000/NameSpaceDSSSL dsssl)
     (string->dsssl-proc str))
    ((http://www.askemos.org/2013/bail/)
     (sweet-string->dsssl-proc/2013 str))
    ((http://www.askemos.org/2005/NameSpaceDSSSL/)
     (sugar-string->dsssl-proc str))
    (else (raise-message "s->d2 \"~a\" ns not defined" ns))))


(define-syntax bind-xsl-value/xpath-expr
  (syntax-rules ()
    ((_ context name expr nmsp cn rn envt tag)
     (bind-xsl-value/string
      context name
      (delay*
       ((xpath-eval (xpath:parse-expr expr)) cn rn envt)
       tag)))))

(define-transformer dsssl-default-element "d:default"
  (let ((namespace (xthe (or false symbol) #f))
        (result (xthe (list-of :xml-attribute:) '())))
    (do ((atts (or (attributes (sosofo)) (empty-node-list)) (cdr atts)))
        ((null? atts) result)
      (let ((name (xml-attribute-name (car atts)))
            (nmsp (xml-attribute-ns (car atts)))
            (val (xml-attribute-value (car atts))))
        ;; TODO eliminate backward compatibility `(eq? nmsp 'dsssl)'.
        (if (or (eq? nmsp namespace-dsssl) (eq? nmsp 'dsssl)
                ;; yet another special case, folding in the historical
                ;; dsssl:a, which doesn't need a attribute namespace
                ;; for href.
                (and (not nmsp) (eq? name 'href) (eq? (gi (sosofo)) 'a)))
            (if (eq? name 'xmlns)
                (set! namespace (string->symbol val))
                (let* ((expr (string->dsssl-proc2 val (ns (sosofo))))
                       (value (if (procedure? expr)
                                  (guard
				   (c (else (dsssl-exception-handler
					     "d:default" c val)))
				   (apply-transformer use: expr sosofos: (current-node)))
                                  (raise-message "dsssl ~a in \"~a\""
						 expr val))))
                  (set! result
                        (cons (make-xml-attribute
                               name #f
                               (cond 
                                ((string? value) value)
                                ((symbol? value) (symbol->string value))
                                ((node-list? value) (data value))
                                (else (format #f "~a" value))))
                              result))))
                 (set! result `(,(car atts) . ,result)))))
      (make-xml-element (gi (sosofo)) namespace result
                        (transform
                         sosofos: (%children (sosofo))))))

(define-transformer dsssl2-default-element "d:default"
  (let ((namespace (xthe (or false symbol)  #f))
        (result (xthe (list-of :xml-attribute:) '())))
    (do ((atts (or (attributes (sosofo)) (empty-node-list)) (cdr atts)))
        ((null? atts) result)
      (let ((name (xml-attribute-name (car atts)))
            (nmsp (xml-attribute-ns (car atts)))
            (val (xml-attribute-value (car atts))))
        (if (or (eq? nmsp namespace-dsssl2)
		(eq? nmsp namespace-dsssl3)
		(eq? nmsp namespace-xsl))
            (if (eq? name 'xmlns)
                (set! namespace (string->symbol val))
		(let ((value
		       (cond
			((eq? nmsp namespace-xsl)
			 ((make-xpath-selection val (namespaces))
			  (current-node) (root-node) (environment)))
			((or (eq? nmsp namespace-dsssl2)
			     (eq? nmsp namespace-dsssl3))
			 (let ((expr (string->dsssl-proc2 val (ns (sosofo)))))
			   (if (procedure? expr)
			       (guard (c (else (dsssl-exception-handler
						"d:default" c val)))
				      (apply-transformer use: expr sosofos: (current-node)))
			       (raise-message "dsssl ~a in \"~a\""
					      expr val)))))))
		  (set! result
                        (cons (make-xml-attribute
                               name #f
                               (cond 
                                ((string? value) value)
                                ((symbol? value) (symbol->string value))
                                ((node-list? value) (data value))
                                (else (format #f "~a" value))))
                              result))))
                 (set! result `(,(car atts) . ,result)))))
      (make-xml-element (gi (sosofo)) namespace result
                        (transform
                         sosofos: (%children (sosofo))
;; Is this required?:                         process: (xslt-process-node-list (%%proc-chain))
			 ))))

(define (is-xsl-variable? node)
  (and (xml-element? node)
       (eq? (ns node) namespace-xsl) (eq? (gi node) 'variable)))

(define (is-xsl-param? node)
  (and (xml-element? node)
       (eq? (ns node) namespace-xsl) (eq? (gi node) 'param)))

(define  (xslt-process-node-list chained-xml-walks)
  (lambda-process
   "bind xsl variables" (chained-xml-walks)
   (transform
    ;; FIXME variables handling is too poor
    variables: (node-list-reduce
                (sosofo)		; here "sosofo" is actually
					; "sosofos" should be rename
					; it?
                (lambda (init n)
                  (cond
                   ((is-xsl-variable? n)
                    (let ((select (attribute-string 'select n))
			  (name (required-attribute-string 'name n "xsl:variable")))
		      (if select
			  (bind-xsl-value/xpath-expr
			   init name
			   select (namespaces)
			   (current-node) (root-node) (environment)
			   'xsl-variable)
			  (bind-xsl-value/walk
			   init name
			   (transform
			    sosofos: (%children n)
			    process: chained-xml-walks
			    variables: init)
			   'xsl-variable))))
                   ((is-xsl-param? n) init)
                   (else init)))
                (environment))
    sosofos: (node-list-filter
              ;; TODO this double test could really be somehow simplified!
              (lambda (node) (not (or (is-xsl-variable? node) 
                                      (is-xsl-param? node))))
              (sosofo))
    process: chained-xml-walks)))

(define (first-element-child-is-xsl-attribute? nl)
  (let loop ((nl nl))
    (and (not (node-list-empty? nl))
         (let ((first (node-list-first nl)))
           (if (xml-element? first)
               (and (eq? (ns first) namespace-xsl) (eq? (gi first) 'attribute)
                    nl)
               (loop (node-list-rest nl)))))))

(define-transformer xsl-element-default "xsl:element"
  ;; Accumulate xsl:attribute nodes
  (let loop ((atts (or (attributes (sosofo)) (empty-node-list)))
             (childs (%children (sosofo))))
    (cond
     ((and (pair? childs)
           (let ((c1 (node-list-first childs)))
             (or (xml-comment? c1) (xml-pi? c1))))
      (loop atts (node-list-rest childs)))
     ((first-element-child-is-xsl-attribute? childs) =>
      (lambda (childs)
        (let ((node (node-list-first childs)))
	  (let ((name-att (attribute-string 'name node))
		(ns-att (attribute-string 'namespace node))
		(select (attribute-string 'select node)))
	    (if (not name-att)
		(raise-message "xsl:attribute: missing \"name\" attribute"))
	    (if (and select (not (node-list-empty? (children node))))
		(raise-message "xsl:attribute: \"select\" attribute present and children not empty"))
	    (loop
	     (cons (make-xml-attribute
		    (string->symbol name-att)
		    (and ns-att (string->symbol ns-att))
		    (data
		     (if select
			 ((make-xpath-match (xpath:parse-expr select) (namespaces))
			  (current-node) (root-node) (environment))
			 (transform sosofos: node))))
		   atts)
	     (node-list-rest childs))))))
     (else

      ;; That's tricky, hm too bad.  Better don't put the rest
      ;; construction into the xml-walk-down macro.
      ;;
      ;; What do I do here?  Apparently I make an xml element with
      ;; the same ns:gi as the "nl" node (recreate the node), except
      ;; that the xsl:attributes are filled in.  Then there is the
      ;; "rest" process, which is executed with all children of nl.
      ;; This process consists of those actions from x-p-n-l at
      ;; highest priority and all the currently active rules, which
      ;; where passed in via proc-chain.

      (make-xml-element
       (gi (sosofo)) (xml-element-ns (sosofo)) atts
       (transform sosofos: childs
		  process: (xslt-process-node-list (%%proc-chain))))))))

(define rule-xslt5.8
  (list
   (cons
    sxp:element?
    (lambda-transformer
     "xslt 5.8"
     (transform
      ancestors: (cons (sosofo) (ancestors))
      sosofos: (%children (sosofo)))))
   ;; TODO what's about attribute rule?  do we need it?
   ;; Literal text is copied through by default.
   (cons sxp:comment? xml-drop)
   (cons sxp:pi? xml-drop)))

;; BEWARE

;; Did Donald Knuth ever say "premature optimization is the root of
;; all evil." - Yes, he did.  This here is the root of evil.  However
;; it's done intentionally to keep in mind that the code structure
;; should finally support some optimization.  Optimized is just upon
;; the name and name space.

;; Given a node and a list 'lst' of possible matches, find the one,
;; which applies to the node.  The list consists of pairs
;; (<match-function> . <template>)

(define (fulfilled-match root-node bindings node lst)
  (let loop ((root-node root-node) (bindings bindings) (node node) (lst lst))
    (if (null? lst) #f
	(or (and ((caar lst) node root-node bindings) (car lst))
	    (loop root-node bindings node (cdr lst))))))

(define (make-xslt-optimized-rules unused-root-node templates chain other-rules)
  (let ((rules (vector
                 (make-symbol-table)    ; ns prefix
                 (make-symbol-table)    ; element without prefix
                 )))
    (let ((proc-childs (xslt-process-node-list chain))
	  ;;??? tolleriert xsl:param, ist innerhalb von template nicht zulässig!
	  
	  (registered-element?
	   (lambda (x root binding)
	     (and (xml-element? x)
		  (let ((entry (hash-table-ref/default (vector-ref rules 1) (gi x) #f)))
		    (and entry (fulfilled-match root binding x entry))))))
	  (registered-namespace?
	   (lambda (x root binding)
	     (and (ns x)
		  (and-let* ((entry (hash-table-ref/default
				     (vector-ref rules 0)
				     (xml-element-ns x)
				     #f)))
			    (fulfilled-match root binding x entry))))))
      (for-each
       (lambda (t)
	 (let ((test (car t))
	       (namespaces (cadr t))
	       (template (cddr t)))
	   (match
	    test
	    (('child key . reduction)
	     (hash-table-set!
	      (vector-ref rules 1) key
	      (cons (cons (if (pair? reduction)
			      (make-xpath-match (car reduction) namespaces)
			      (node-typeof? '*any*))
			  template)
		    (hash-table-ref/default (vector-ref rules 1) key '()))))
	    (('ns-gi prefix name . reduction)
	     (let ((bound (find-namespace-by-prefix prefix namespaces)))
	       (if bound
		   (hash-table-set!
		    (vector-ref rules 0) (xml-namespace-uri bound)
		    (cons (cons (if (pair? reduction)
				    (make-xpath-match
				     `(node-reduce (node-typeof? ,name) . ,reduction)
				     namespaces)
				    (node-typeof? name))
				template)
			  (hash-table-ref/default (vector-ref rules 0) (xml-namespace-uri bound) '())))
		   (raise-message "xslt:match namespace prefix '~a' not bound" prefix))))
	    (('ns key . reduction)
	     (let ((bound (find-namespace-by-uri key namespaces)))
	       (if bound
		   (hash-table-set!
		    (vector-ref rules 0) (xml-namespace-uri bound)
		    (cons (cons (if (pair? reduction)
				    (make-xpath-match (car reduction) namespaces)
				    (node-typeof? '*any*))
				template)
			  (hash-table-ref/default (vector-ref rules 0) key '())))
		   (raise-message "xslt:match namespace uri '~a' not bound" key))))
	    (X (raise-message "can't make optimized rule for ~a" X)))))
       templates)
      `((,registered-namespace?
	 . ,(lambda-transformer
	     ;; XXX This looks suspicous.  Shouldn't the let sorround the lambda-transformer?
	     "xslt[5.1] namespace match"
	     (let ((entry (fulfilled-match
			   (root-node) (environment) (sosofo)
			   (hash-table-ref/default (vector-ref rules 0) (ns (sosofo)) #f))))
	       (transform current-node: (sosofo)
			  sosofos: (%children (cdr entry))
			  process: proc-childs))))
	(,registered-element?
	 . ,(lambda-transformer
	     ;; XXX same as above
	     "xslt[5.1] element name match"
	     (let ((entry (fulfilled-match
			   (root-node) (environment) (sosofo)
			   (hash-table-ref/default (vector-ref rules 1) (gi (sosofo)) #f))))
	       (transform current-node: (sosofo)
			  sosofos: (%children (cdr entry))
			  process: proc-childs))))
	. ,other-rules))))

(define (make-general-rule template chain)
  (cons
   (make-xpath-match (car template) (cadr template))
   ;; xslt[5.1] instantiate
   (lambda-transformer
    "xsl:general"
    (transform current-node: (sosofo)
	       sosofos: (%children (cddr template))
	       process: (xslt-process-node-list chain)))))

(define (make-general-rules templates chain)
  (let loop ((templates templates) (rules rule-xslt5.8))
    (if (null? templates)
        rules
        (loop (cdr templates)
              (cons (make-general-rule (car templates) chain) rules)))))

;; PURPOSE create a process (usable for xml-walk-down) implementing
;; the rules as defined by templates parameter.  Parameter "chain" is
;; the process, which will be used to evaluate the template content,
;; i.e., the xslt processor.

(define (rules->process recurse static-variables rules)
  (lambda-process
   "rules->process"
   (rules)
   ;; FIXME: this needs to be rewritten using the new macros!
   (xml-walk*1 (current-place) (current-message) (root-node)
	       (xsl-envt-union (environment) static-variables)
	       (namespaces)
	       (ancestors)
	       (current-node)
	       (current-node)
	       recurse rules)))

;; Rest of match, if xml-walk will hit that node, #f otherwise.
(define (xml-walk-match match)
  (and (pair? match) (eq? (car match) node-reduce)
       (pair? (cdr match)) (eq? (cadr match) sxp:child)
       (cons node-reduce (cddr match))))

;; register-xslt-match-template extends the list of analyzed
;; 'templates' by 'node', if 'node' has a 'match' attribute.  The
;; 'templates' parameter must always be initialized with
;;  '((#f . #(() ()))) .

(define empty-mode-data
  '(#f . #(()                           ; optimized-matchs
           ()))                         ; walk-matchs
  )

(define empty-mode-data-list (list empty-mode-data))

(define (make-xslt-named-template-register static-variables proc-chain)
  (lambda (p templates)
    (define node (car p))
    (define namespaces (cdr p))
    (let ((name (attribute-string 'name node)))
      (if (and name
	       (not (hash-table-ref/default templates name #f)))
	  (hash-table-set!
	   templates name
	   (receive
	    (params cache keys)
	    (let ((cached (attribute-string 'memorise node)))
	      (if cached
		  (let ((keys (string-split cached " ")))
		    (values
		     #f
		     (make-xslt-call-cache name)
		     (map
		      (lambda (expr)
			(cond
			 ((string=? expr ".")
			  (lambda-transformer "XPath \".\"" (data (current-node))))
			 (else
			  (lambda-transformer "named template" (data ($ expr))))))
		      keys)))
		  (values #f #f #f)))
	     (lambda-process
	      "with xsl templates" (proc-chain)
	      (if (not params)
		  ;; That's not only tricky, that stinks.
		  (set! params
			(fold
			 (lambda (n init)
			   (if
			    (and (xml-element? n)
				 (eq? (ns n) namespace-xsl)
				 (eq? (gi n) 'param))
			    (let ((select (attribute-string 'select n)))
			      (if select
				  (bind-xsl-value/xpath-expr
				   init (attribute-string 'name n)
				   select (namespaces)
				   (current-node) (root-node) (environment)
				   'var-or-param)
				  (let ((required (attribute-string 'required n)))
				    (if (equal? required "yes")
					(let ((name (attribute-string 'name n)))
					  (bind-xsl-value/expr
					   init name
					   (raise-message
					    "~a required xsl:param '~a' missing"
					    (attribute-string 'name node) name)
					   'var-or-param))
					(bind-xsl-value/walk
					 init (attribute-string 'name n)
					 (transform
					  sosofos: (%children n)
					  ;; continue: style-modi
					  process: proc-chain
					  variables: init)
					 'var-or-param)))))
			    init))
			 static-variables
			 (children node))))
	      (if cache
		  (let ((variables (xsl-envt-union (environment) params)))
		    (xslt-call-cache-ref
		     cache
		     (srfi:string-join
		      (map (lambda (var)
			     (apply-transformer use: var variables: variables))
			   keys)
		      (string #\0))
		     (lambda ()
		       (transform
			;;self-node: nl
			sosofos: (%children node)
			variables: variables
			process: (xslt-process-node-list proc-chain)
			;; continue: recurse
			))))
		  (transform
		   ;;self-node: nl
		   sosofos: (%children node)
		   variables: (xsl-envt-union (environment) params)
		   process: (xslt-process-node-list proc-chain)
		   ;; continue: recurse
		   )))))
          templates))
    templates))

(define (register-xslt-match-template p templates)
  (define node (car p))
  (define namespaces (cdr p))
  (let ((match (attribute-string 'match node)))
    (if match
	(let* ((parsed-match (xpath:parse match))
	       (o-m (xpath-optimizable-match? parsed-match))
	       (mode-str (attribute-string 'mode node))
	       (mode (and mode-str (string->symbol mode-str)))
	       (defined (assq mode templates))
	       (kind (cdr (or defined empty-mode-data)))
	       (entry
		(cons
		 mode
		 (let ((namespaces (xml-parse-register-xmlns
				    (xml-element-attributes node)
				    namespaces)))
		   (if o-m
		       (vector
			(cons (cons* o-m namespaces node) (vector-ref kind 0))
			(vector-ref kind 1))
		       (vector
			(vector-ref kind 0)
			(cons (cons*
			       (or (xml-walk-match parsed-match)
				   (raise-message "xslt match not implemented: ~a\n"
						  match))
			       namespaces
			       node)
			      (vector-ref kind 1)))))))
	       ;; FIXME priority is not yet used.
	       (priority (attribute-string 'priority node)))
	  (if defined
	      (map (lambda (x) (if (eq? (cdr x) kind) entry x)) templates)
	      ;; Keep the default mode in front position.
	      `(,(car templates) ,entry . ,(cdr templates))))
	templates)))

;; It's actually a poor chaice to put named templates as a special
;; case under templates.  They are *much* more similar to variables
;; than templates and should be treated together with those.  The only
;; open question would arise from templates bearing the same name as a
;; variable.

(define (make-mode-choice root-node templates vars proc-chain)
  (letrec
      ((modi (xthe (list-of (pair string procedure)) '()))
       (named (fold (make-xslt-named-template-register vars proc-chain)
		    (make-string-table) templates))
       (recurse
	(lambda (mode)
	  (if (string? mode)
	      (let ((e (hash-table-ref/default named mode #f)))
		(or e
		    (raise-message "template named \"~a\" not found" mode)))
	      (let ((e (assq mode modi)))
		(cdr (or e
			 (raise-message "mode ~a not defined" mode)
			 ;; Default not reached:
			 (car modi))))))))
    ;; Somehow it should be possible get that done without an
    ;; assignment, but I did not manage.
    (set! modi
	  (map
	   (lambda (mode)
	     (cons
	      (car mode)
	      (if (string? (car mode)) ; call a named template
		  (lambda-process
		   "named template" (proc-chain)
		   (transform
		    ;; self-node: nl
		    sosofos: (%children (cdr mode))
		    variables: vars
		    ;; war (xslt-process-node-list proc-chain) -- falsch
;		    process: proc-chain
		    continue: recurse))
		  (let ((optimized-templates (vector-ref (cdr mode) 0))
			(templates (vector-ref (cdr mode) 1)))
		    (if (null? optimized-templates)
			(rules->process
			 recurse vars
			 (make-general-rules templates proc-chain))
			(rules->process
			 recurse vars
			 (make-xslt-optimized-rules
			  root-node optimized-templates proc-chain
			  (make-general-rules templates proc-chain))))))))
	   (fold register-xslt-match-template
		 empty-mode-data-list
		 templates)))
    
    recurse))


;; Sort out xslt relevant elements from a style sheet tree.

(define-transformer xsl-stylesheet "xsl:stylesheet"
  (let loop ((childs (%children (sosofo)))
	     (namespaces (namespaces))
             (varelems '())
             (templates '())
	     (include-stack '())
             (output #f))
    (if (node-list-empty? childs)       ; whenever no more xsl topl-level
        (if (null? include-stack)
	    (let
		((result-childs
		  (let ((style-modi (the (or false procedure) #f)))
		    (let ((nvars
			   (if (null? varelems)
			       (environment)
			       (fold
				(lambda (np init)
				  (define namespaces (cdr np))
				  (define n (car np))
				  (let ((nm (attribute-string 'name n))
					(select (attribute-string 'select n)))
				    (if select
					(bind-xsl-value/xpath-expr
					 init nm
					 select namespaces
					 (current-node) (root-node) init
					 nm)
					(bind-xsl-value/walk
					 init nm
					 (transform
					  sosofos: (%children n)
					  continue: style-modi
					  process: (xslt-process-node-list (%%proc-chain))
					  variables: init)
					 nm))))
				(environment)
				(reverse! varelems)))))
		      (set! style-modi (make-mode-choice (root-node) templates nvars (%%proc-chain)))
		      (transform
		       ;; FIXME variables handling is too poor
		       variables: nvars
		       sosofos: (current-node)
		       process: (style-modi #f))))))
	      (if output
		  (make-xml-element (gi output) (ns output)
				    (xml-element-attributes output)
				    result-childs)
		  result-childs))
	    (loop (caddar include-stack) (cadar include-stack)
		  varelems templates (cdr include-stack) output))
        (let ((node (node-list-first childs)))
          (if (and (xml-element? node) (eq? (ns node) namespace-xsl))
              (case (gi node)
                ((template)
                 (loop (node-list-rest childs)
		       namespaces varelems
                       (cons (cons node namespaces) templates)
		       include-stack
                       output))
                ((variable) (loop (node-list-rest childs)
				  namespaces
                                  (cons (cons node namespaces) varelems)
                                  templates
				  include-stack
                                  output))
                ((include)
                 (let* ((href (attribute-string 'href node))
			;; (xpath:parse (attribute-string 'href node))
			(path (if (eqv? (string-ref href 0) #\/)
				  (string-split href "/")
				  (append (if (pair? include-stack)
					      (caar include-stack) '())
					  (string-split href "/"))))
			(answer
			 (document-element
			  (function-fetch-other
			   (current-place) path body: (empty-node-list)))))
                   (if (and (eq? (gi answer) 'stylesheet)
                            (eq? (ns answer) namespace-xsl))
                       (loop (%children answer)
                             (xml-parse-register-xmlns
			      (xml-element-attributes answer) namespaces)
			     varelems
                             templates
			     (cons (list
				    (drop-right path 1)
				    namespaces
				    (node-list-rest childs))
				   include-stack)
                             output)
		       (raise-message
			"~a:include: From ~a path ~a Unsupported response ~a ~a"
			(ns node) ((current-place) 'get 'id)
			(cons href (if (pair? include-stack) (car include-stack) '()))
			(and-let* ((d (document-element answer))
				   ((xml-element? d)))
				  (literal (ns d) ":" (gi d)))
			answer))))
                ((output)
                 (loop (node-list-rest childs)
                       namespaces varelems
                       templates
		       include-stack
                       (let ((method (attribute-string 'method node))
                             (media (attribute-string 'media-type node)))
                         (and (not (and (equal? method "xml")
                                        (equal? media "text/xml")))
                              node))))
                (else
                 (error
                  (format
                   #f "xsl:~a is not a (supported) xslt top level element."
                   (gi node)))))
              (loop (node-list-rest childs) ; ignored, see xslt[2.2]
                    namespaces varelems
                    templates
		    include-stack
                    output))))))

(register-tag-transformer!
 *namespace-environment* namespace-xsl 'stylesheet xsl-stylesheet)

(define-transformer xsl-element-handler "element dispatch"
  (apply-transformer
   use: (or (let ((nmsp (ns (sosofo))))
	      (and nmsp
		   (let ((table (hash-table-ref/default *namespace-environment* nmsp #f)))
		     (or (and table (hash-table-ref/default table (gi (sosofo)) #f))
			 (cond
			  ((eq? nmsp namespace-dsssl3)
			   dsssl2-default-element)
			  ((eq? nmsp namespace-dsssl2)
			   dsssl2-default-element)
			  ((eq? nmsp namespace-dsssl)
			   dsssl-default-element)
			  (else xsl-element-default))))))
	    xsl-element-default)))

(define make-xslt-transformer
  (let ((handler
         `((,sxp:element? . ,xsl-element-handler)
           (,sxp:comment? . ,xml-drop)
           (,sxp:pi?      . ,xml-drop))))
    ;; create a transformer, which operates in the right space of
    ;; place and message.
    (lambda (place message)
      (define (xslt-transformer node template)
        (apply
         (xml-walk
          place message
	  node
	  (make-xsl-variables
	   `((grove-root . ,(document-element (place 'body/parsed-xml)))))
	  (xml-parse-register-xmlns
	   (xml-element-attributes (document-element template))
	   empty-namespace-declaration)
          '()                           ; ancestor
          node
          template)
         handler))
      xslt-transformer)))

(define make-xslt-transformer*
  (let ((handler
         `((,sxp:element? . ,xsl-element-handler)
           (,sxp:comment? . ,xml-drop)
           (,sxp:pi?      . ,xml-drop))))
    ;; create a transformer, which operates in the right space of
    ;; place and message.
    (lambda (place message . args)
      (define (xslt-transformer node template)
        (apply
         (xml-walk
	  (or (and-let* ((key (memq context: args))) (cadr key)) place)
	  (or (and-let* ((key (memq message: args))) (cadr key)) message)
	  node
	  (make-xsl-variables
	   `((grove-root . ,(or (and-let* ((key (memq grove-root: args))) (cadr key))
				(document-element (place 'body/parsed-xml))))))
	  (xml-parse-register-xmlns
	   (xml-element-attributes (document-element template))
	   empty-namespace-declaration)
          '()                           ; ancestor
          node
          template)
         handler))
      xslt-transformer)))
