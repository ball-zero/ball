;; "metaview" access the system on a more fine granular way.

;; programmer would say "debug" level access.

;; This code implements a simple hook for an upper level of the code
;; to insert some function to display the current object at meta
;; level.

;; Access Control

;; Access at this level is critical.  Therefore we require a distinct
;; namespace declaration (thus use will be easily spotted.

(define namespace-meta-str "http://askemos.org/BALL/Meta/2012#")
(define namespace-meta (string->symbol namespace-meta-str))

;; Initialisation

;; Initially the hook is empty/invalid.

(define function-metaview #f)
(define function-metactrl #f)

(define (set-function-metaview! proc)	; EXPORT
  (set! function-metaview proc))

(define (set-function-metactrl! proc)
  (set! function-metactrl proc))

;; Implementation is deferred (in fact to yet another hook at this
;; moment).

(define-transformer ball-metaview "sys:metaview"
  (function-metaview (current-place) (current-message)))

(register-tag-transformer! *namespace-environment* namespace-meta 'view ball-metaview)

(define-transformer ball-metactrl "sys:metacntrl"
  (function-metactrl (current-place) (current-message)))

(register-tag-transformer! *namespace-environment* namespace-meta 'control ball-metactrl)
