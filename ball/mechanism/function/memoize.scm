;; (C) 1993, 2000, 2001, 2015 Jörg F. Wittenberger see http://www.askemos.org

;;** Memoization

;; A special case for the most common usage patter.  BEWARE the
;; implementation is actually not correct, as the function is
;; evaluated here even though it was before, if the result is #f.


(define (memoize-one-string-arg/hash f string=?)
  (let ((table (make-hash-table
		string=?
		(if (eq? string=? string-ci=?) string-ci-hash string-hash)))
	(mutex (make-mutex 'mutex)))
    (lambda (str)
      (hash-table-ref
       table str
       (lambda ()
	 (with-mutex
	  mutex
	  (hash-table-ref
	   table str
	   (lambda ()
	     (let ((result (f str)))
	       (hash-table-set! table str result)
	       result)))))))))

(define (memoize-one-string-arg f)
  (let ((table (make-table)))
    (lambda (str)
      (let loop ((entry (table-ref/default table str #f)))
	(if (not entry)
	    (let ((entry (list (make-mutex))))
	      (table-set! table str entry)
	      (loop entry))
	    (let ((hit (car entry)))
	      (if (mutex? hit)
		  (with-mutex
		   hit
		   (if (mutex? (car entry))
		       (let ((v (f str)))
			 (set-cdr! entry v)
			 (set-car! entry #f)
			 v)
		       (cdr entry)))
		  (cdr entry))))))))

(define (memoize-not-reentrant f . ac)
  (define (lookup pred arg where)
    (if (null? where)
	#f
	(if (pred arg (caar where))
	    (car where)
	    (lookup pred arg (cdr where)))))
  (define (insert where ac args val)
    (if (null? args)
	val
	(let ((sub (lookup (car ac) (car args) where)))
	  (if sub
	      (begin
		(set-cdr! sub (insert (cdr sub) (cdr ac) (cdr args) val))
		where)
	      `((,(car args) . ,(insert '() (cdr ac) (cdr args) val))
		. ,where)))))
  (let ((res (if (null? ac)
		 (list (f))
		 '())))
    (lambda args
      (let loop ((run-args args) (run-ac ac) (prev res))
	(if
	 (null? run-args)
	 (if (null? run-ac)
	     prev
	     (error "(memoized) too few arguments for" f run-ac))
	 (if
	  (null? run-ac)
	  (error "(memoized) too much arguments" f run-args)
	  (let ((possible-result (lookup (car run-ac) (car run-args) prev)))
	    (if possible-result
		(loop (cdr run-args) (cdr run-ac) (cdr possible-result))
		(let ((computed-result (apply f args)))
                      (set! res (insert res ac args computed-result))
                      computed-result)))))))))

;; FIXME RScheme problem:
;; (with-semaphore res-sema (loop args ac res #t))
;; Doesn't work due to a compiler bug.  Trying to get around it...


(define (%%RScheme-bug-with-semaphore res-sema loop args ac res
                                      islocked computed-result)
  (with-mutex res-sema (loop args ac res #t computed-result)))

(define (memoize-lazy f . ac)
  (define (lookup pred arg where)
    (if (null? where)
	#f
	(if (pred arg (caar where))
	    (car where)
	    (lookup pred arg (cdr where)))))
  (define (insert where ac args val)
    (if (null? args)
	val
	(let ((sub (lookup (car ac) (car args) where)))
	  (if sub
	      (begin
		(set-cdr! sub (insert (cdr sub) (cdr ac) (cdr args) val))
		where)
	      `((,(car args) . ,(insert '() (cdr ac) (cdr args) val))
		. ,where)))))
  (let ((res (if (null? ac)
		 (list (f))
		 '()))
        (res-sema (make-mutex f)))
    (lambda args
      (let loop ((run-args args) (run-ac ac) (prev res)
                 (locked #f) (computed-result #f))
	(if
	 (null? run-args)
	 (if (null? run-ac)
	     prev
	     (error "(memoized) too few arguments for" f run-ac))
	 (if
	  (null? run-ac)
	  (error "(memoized) too many arguments" f run-args)
	  (let ((possible-result (lookup (car run-ac) (car run-args) prev)))
	    (if possible-result
		(loop (cdr run-args) (cdr run-ac) (cdr possible-result)
                      locked computed-result)
		(if locked
                    (begin
                      (set! res (insert res ac args computed-result))
                      computed-result)
                    ;; We descant again in case someone inserted the
                    ;; result in the meantime.
                    (%%RScheme-bug-with-semaphore
                     res-sema loop args ac res #t
                     (apply f args)))))))))))

(define (memoize f . ac)
  (cond
   ((and (pair? ac)
	 (null? (cdr ac))
	 (eq? (car ac) string=?))
    (memoize-one-string-arg f))
   ((and (pair? ac)
	 (null? (cdr ac))
	 (eq? (car ac) string-ci=?))
    (memoize-one-string-arg/hash f string-ci=?))
   (else (apply memoize-lazy f ac))))
