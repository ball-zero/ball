;; (C) 2000, 2001, 2003, 2004 J�rg F. Wittenberger see http://www.askemos.org

;;; Tell the `match' macro to expand into slightly more efficient (but
;;; a little less safe) code:

(match-error-control 'unspecified)

;; The match macro is used in interp.scm, which must be compiled after
;; this setting.  That is, at least after this setting.  I'm not sure
;; that we don't have to call it even before, but when?  JFW
;; 2003-09-15

;;** Evaluation

;; We need a sandbox, i.e., a restricted environment to evaluate user
;; supplied expressions.

;; It's an unfortune fact of life that we have no standard way to
;; create environments.  We'll see this code heavily depend on the
;; underlying language implementation.

;; Create the initial environment for the client code.

(%early-once-only

(define user-envt (make-user-initial))
(eval-in-envt ',(use srfis) user-envt)
(eval-in-envt ',(use util) user-envt)	; only for raise and guard until those moved
(eval-in-envt ',(use tree sort) user-envt)
(eval-in-envt ',(use compiletime) user-envt) ; andmap, ormap etc.
(eval-in-envt ',(use notation) user-envt)

;; The pcre-lalr-parser is not yet ready for the prime time.  Defined
;; that way, it will compute the semantic actions within 'user-envt'
;; instead of dsssl-top-level.  If we where to use the latter for the
;; 'eval-in-envt' the generated parsers will not yet work, since they
;; use side effect (vector-set!) for parse stack manipulation...
;;
;; (eval-in-envt
;;  '(define-macro (pcre-lalr-parser scan tokens . rules)
;;     (let ((parser (with-module compiletime (gen-lalr-parser ,tokens . ,rules)))
;;           (scanner (make-pcre-tokeniser ,scan)))
;;       (lambda (str)
;;         (parser (scanner str) error))))
;;  user-envt)

(define dsssl-export! #f)

(define (dsssl-export-named! sym)
  (if (pair? sym)
      (dsssl-export! (eval-in-envt (car sym) user-envt) (cadr sym))
      (dsssl-export! (eval-in-envt sym user-envt) sym)))

(define (make-dsssl-initial)
  (receive
   (m dsssl-top-level) (make-module 'dsssl)

   (set! dsssl-export!
         (lambda (value name)
           ;; If in doubt while debuging, look what's being defined:
           ;; (logerr "binding ~s ~s\n" name value)
           (bind! dsssl-top-level
                  (make  <top-level-var> name: name value: value))))

   (let ((src (table (top-level-envt (make-r4rs-module))))
         (dst (table dsssl-top-level)))
     (hash-table-walk
      src
      (lambda (k v)
        (if (not (memq k r5rs-side-effecty-things))
            (hash-table-set! dst k v))))
     (bind! dsssl-top-level
            (make <top-level-var> 
              name: 'eval 
              value: (lambda 'eval (s-expr . r)
                             (if (null? r)
                                 (eval-in-envt s-expr dsssl-top-level)
                                 (eval-in-envt s-expr (car r))))))
     (for-each
      (lambda (lst) (for-each dsssl-export-named! lst))
      (list dsssl-forms srfi19-forms srfi43-forms))
     (cond-expand
      (rscheme (dsssl-export! srfi:cons* 'cons*)
	       (dsssl-export! srfi:vector-for-each 'vector-for-each)
	       (dsssl-export! srfi:member 'member)))
     (let ((src (table user-envt)))
       (table-insert! dst 'receive (table-lookup src 'receive-srfi))
       (table-insert! dst 'guard (table-lookup src 'dsssl-guard))
       (table-insert! dst 'shift (table-lookup src 'shift))
       (table-insert! dst 'reset (table-lookup src 'reset))
;;        (table-insert! dst 'pcre-lalr-parser
;;                       (table-lookup src 'pcre-lalr-parser))
       (for-each (lambda (k) (table-insert! dst k (table-lookup src k)))
                 '(and-let* values empty-node-list)))
;     (bind! dsssl-top-level (make  <top-level-var> 
;                              name: 'empty-node-list
;                              value: fun:empty-node-list))
;     (bind! dsssl-top-level (make  <top-level-var> 
;                              name: 'node-list-empty?
;                              value: fun:node-list-empty?))
;     (bind! dsssl-top-level (make  <top-level-var> 
;                              name: 'node-list-rest
;                              value: fun:node-list-rest))
     dsssl-top-level)))

(define *dsssl-environment* (make-dsssl-initial))

(define (dsssl-environment) *dsssl-environment*)

;; Refer to rschemes tests/all.scm suite for a way to allow isolated
;; side effects.  I don't do this here, because of the performance
;; issue accociated with copying a whole hash table,

(define (eval-sane expr)
  ((expr->thunk expr *dsssl-environment*))))

(define *eval-serialized* #f)

(define (init-evaluator)
  (if (not *eval-serialized*)
      (set! *eval-serialized* (make-serial-guard '*eval-serialized*))))

(define (eval-sane-string str)
  (guard (c (else (error "while evaluating \"~a\" ~a" str c)))
         ;; We put a let around the expression to support "define".
         ;; Otherwise top level defines could extend over the invocation.
         (let ((expr `(let () . ,(call-with-input-string str read))))
           (eval-sane expr))))

(define (eval-thread-safe expr)
  ;; (with-semaphore *eval-sema* (eval-in-envt expr user-envt))
  ;; (if (not *eval-serialized*) (init-evaluator))
  ;; (*eval-serialized* (lambda () (eval-in-envt expr user-envt)))
  (eval-in-envt expr user-envt)
  )

;;** Levenshtein

;; See http://www.merriampark.com/ld.htm

(define-glue (lev-init mx)
{
  int i,m = fx2int(mx), *p;
  REG0 = bvec_alloc( sizeof(int) * (m+1), byte_vector_class );
  p = (int*) PTR_TO_DATAPTR(REG0);
  for(i=0; i<=m; ++i) p[i]=i;
  RETURN1();
})

(define-glue (lev-dist d n)
{
  int *p=PTR_TO_DATAPTR(d);
  REG0 = int2fx( p[ fx2int(n) ] );
  RETURN1();
})

(define-glue (lev-step! matrix mx ix a b off)
{
#define min(a, b) (((a) < (b)) ? (a) : (b))
  int i=fx2int(ix), o=fx2int(off), m=fx2int(mx);
  int *d_i = (int*) PTR_TO_DATAPTR(matrix);
  unsigned char *s = string_text(a) + o;
  unsigned char *t = string_text(b) + o;
  int distance=d_i[0], j, left, cost;

  d_i[0]=i;
  for(j=1; j<=m; ++j) {               /* row loop */
    left = d_i[j];
    /* Step 5 */
    cost = s[i-1]==t[j-1] ? 0 : 1;
    /* Step 6 */
    d_i[j] =  min(min(d_i[j-1]+1, left+1), distance+cost);
    distance = left;
  }
  REG0 = int2fx(distance);
  RETURN1();
})

(define-glue (utf8-lev-step! matrix mx ix ixi a b off aoffi boffi)
{
#define min(a, b) (((a) < (b)) ? (a) : (b))
  int i=fx2int(ix), o=fx2int(off), m=fx2int(mx),
  aoi=fx2int(aoffi), boi=fx2int(boffi);
  int *d_i = (int*) PTR_TO_DATAPTR(matrix);
  unsigned char *s = string_text(a) + aoi;
  unsigned char *t = string_text(b) + boi;
  unsigned char *scan = s + fx2int(ixi);
  unsigned char *limit = s + string_length(a);
  int size, si, distance=d_i[0], j, left, cost;

  d_i[0]=i+1;
  for(j=0; j<=m-1; ++j) {               /* row loop */
    left = d_i[j+1];
    /* Step 5 */
    cost = 0;
    size = 1;
    if (*scan < 0x80) cost = *scan == t[j] ? 0 : 1;
    else if (*scan < 0xE0) {
      size=2;
      cost = (*scan & 0x1F) == (t[j] & 0x1F) ? 0 : 1;
    } else if (*scan < 0xF0) {
      size=3;
      cost = (*scan & 0x0F) == (t[j] & 0x0F) ? 0 : 1;
    } else if (*scan < 0xF8) {
      size=4;
      cost = (*scan & 0x07) == (t[j] & 0x07) ? 0 : 1;
    } else if (*scan < 0xFC) {
      size=5;
      cost = (*scan & 0x3) == (t[j] & 0x3) ? 0 : 1;
    } else if (*scan < 0xFE) {
      size=6;
      cost = (*scan & 0x1) == (t[j] & 0x1) ? 0 : 1;
    } else raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad character size"), int2fx(scan-s) ) );

    if( scan + size > limit )
      raise_error( make3( TLREF(0), NIL_OBJ, make_string("short character"), int2fx(scan-s) ) );
     for(si=1; si<size ; ++si) {
       if ((scan[si]<0x80) || (scan[si] >= 0xC0))
         raise_error( make3( TLREF(0), NIL_OBJ, make_string("bad byte"), int2fx(scan-s) ) );
       else {
         cost |= (scan[si] & 0x3F) == (t[j+si] & 0x3F) ? 0 : 1;
       }
     }

    /* Step 6 */
    d_i[j+1] =  min(min(d_i[j]+1, left+1), distance+cost);
    distance = left;
  }
  REG0 = int2fx(distance);
  RETURN1();
})

(define-safe-glue (lev-0 (a <string>) (b <string>))
{

  unsigned char *s = string_text(a);
  unsigned char *t = string_text(b);
  int i=0, n=string_length(b), m=string_length(a);

  /* skip common suffix */
  while( m>0 && n>0 && s[m-1]==t[n-1] ) --m, --n;
  /* skip common prefix */
  while( m>0 && n>0 && s[i] == t[i] ) ++i, --m, --n;
  
  REG0 = int2fx(i);
  REG1 = int2fx(m);
  REG2 = int2fx(n);
  RETURN(3);
})

(define (utf8-lev-0 (a <string>) (b <string>))
  (let ((m (utf8-string-length a)) (n (utf8-string-length b)))
    (define (skip-prefix i ai bi m n)
      (if (or (eqv? m 0) (eqv? n 0)
              (not (eqv? (utf8-string-getc a ai) (utf8-string-getc b bi))))
          (values i ai bi m n)
          (skip-prefix (add1 i)
                       (utf8-seek a ai i (add1 i))
                       (utf8-seek b bi i (add1 i))
                       (sub1 m) (sub1 n))))
    (let loop ((m m) (mi (utf8-seek a (string-length a) m (sub1 m)))
               (n n) (ni (utf8-seek b (string-length b) n (sub1 n))))
      (if (or (eqv? m 0) (eqv? n 0))
          (skip-prefix 0 0 0 m n)
          (let ((mi1 (utf8-seek a mi m (sub1 m)))
                (ni1 (utf8-seek a ni n (sub1 n))))
            (if (eqv? (utf8-string-getc a mi) (utf8-string-getc b ni))
                (loop (sub1 m) mi (sub1 n) ni)
                (skip-prefix 0 0 0 m n)))))))

(define (levenshtein-distance s t)
  (receive
   (off sl tl) (lev-0 s t)
   (cond
    ((eqv? sl 0) tl)
    ((eqv? tl 0) sl)
    (else
     (if (< tl sl)
         (lev-exec (lev-init sl) s t off sl tl)
         (lev-exec (lev-init tl) t s off tl sl))))))

(define (utf8-levenshtein-distance s t)
  (receive
   (off soi toi sl tl) (utf8-lev-0 s t)
   (cond
    ((eqv? sl 0) tl)
    ((eqv? tl 0) sl)
    (else
     (if (< tl sl)
         (utf8-lev-exec (lev-init sl) s t off soi toi sl tl)
         (utf8-lev-exec (lev-init tl) t s off toi soi tl sl))))))

(define (lev-exec matrix s t o m n)
  (do ((i 1 (add1 i)))
      ((> i n) (lev-dist matrix m))
    (lev-step! matrix m i s t o)))

(define (utf8-lev-exec matrix s t o soi toi m n)
  (do ((i 0 (add1 i))
       (ix 0 (utf8-seek s ix i (add1 i))))
      ((>= i n) (lev-dist matrix m))
    (utf8-lev-step! matrix m i ix s t o soi toi)))

(define (lev-exec< matrix s t o m n limit)
  (let loop ((i 1) (distance 0))
    (cond
     ((>= distance limit) #f)
     ((> i n) (>= (lev-dist matrix m) limit))
     (else (loop (add1 i) (lev-step! matrix m i s t o))))))

(define (utf8-lev-exec< matrix s t o soi toi m n limit)
  (let loop ((i 1) (ix 0) (distance 0))
    (cond
     ((>= distance limit) #f)
     ((> i n) (>= (lev-dist matrix m) limit))
     (else (loop (add1 i)
                 (utf8-seek s i ix (add1 i))
                 (utf8-lev-step! matrix m i ix s t o soi toi))))))

(define (levenshtein< s t limit)
  (receive
   (off sl tl) (lev-0 s t)
    (cond
     ((eqv? sl 0) (< tl limit))
     ((eqv? tl 0) (< sl limit))
     (else
      (if (< tl sl)
          (lev-exec< (lev-init sl) s t off sl tl limit)
          (lev-exec< (lev-init tl) t s off tl sl limit))))))

(define (utf8-levenshtein< s t limit)
  (receive
   (off soi toi sl tl) (utf8-lev-0 s t)
    (cond
     ((eqv? sl 0) (< tl limit))
     ((eqv? tl 0) (< sl limit))
     (else
      (if (< tl sl)
          (utf8-lev-exec< (lev-init sl) s t off soi toi sl tl limit)
          (utf8-lev-exec< (lev-init tl) t s off toi soi tl sl limit))))))

(define-class <out-of-mem> (<error>)
  (message type: <string>))

(define-safe-glue (levenshtein-distance-x (ss <string>) (ts <string>))
  literals: ((& <out-of-mem>))
{
/* #define min(a, b) (((a) < (b)) ? (a) : (b)) */
  /* Step 1 */
  int i,j,n,m,cost,*d_i,distance, left, start, end;

  unsigned char *s = string_text(ss);
  unsigned char *t = string_text(ts);
  n=string_length(ss);
  m=string_length(ts);

    /* we allocate only one column of the matrix, that's why we make
     t,m represent the smaller string to save space here. */

    if( n<m ) {
      unsigned char *hp = s;
      s = t;
      t = hp;
      i = m;
      m = n;
      n = i;
    }

  while( m>0 && s[m-1]==t[n-1] ) --m, --n;     /* skip common suffix */
  while( m>0 && *s == *t ) ++s, ++t, --m, --n; /* skip common prefix */

  if( m == 0 ) {
    REG0 = int2fx(n);
  } else if( n == 0 ) {
    REG0 = int2fx(m);
  } else {

    d_i=(int*)malloc((sizeof(int))*(m+1));
    if( !d_i ) {
      raise_error(make2(TLREF(1), NIL_OBJ,
                  make_string("out of memory in levenshtein-distance")));
      RETURN0();
    } else {

      /* Step 2(first half) initialise the arrays first column */
      for(j=0; j<=m; ++j) d_i[j]=j;

      /* Step 2(second half), 3 and 4 */

      for(i=1; i<=n; ++i) {                 /* column loop */
        distance=d_i[0];
        d_i[0]=i;
        for(j=1; j<=m; ++j) {               /* row loop */
          left = d_i[j];
	  /* Step 5 */
          cost = s[i-1]==t[j-1] ? 0 : 1;
	  /* Step 6 */
          d_i[j] =  min(min(d_i[j-1]+1, left+1), distance+cost);
          distance = left;
        }
      }
      distance=d_i[m];
      free(d_i);          
      REG0 = int2fx(distance);
    }
  }
  RETURN1();
})

(define-safe-glue (levenshtein-distance< (ss <string>) (ts <string>)
                                         (fence <integer>))
  literals: ((& <out-of-mem>))
{
#define min(a, b) (((a) < (b)) ? (a) : (b))
  /* Step 1 */
  int i,j,n,m,cost,*d_i,distance, left, start, end, stop;

  unsigned char *s = string_text(ss);
  unsigned char *t = string_text(ts);
  n=string_length(ss);
  m=string_length(ts);
  stop = fx2int(fence);

    /* we allocate only one column of the matrix, that's why we make
     t,m represent the smaller string to save space here. */

    if( n<m ) {
      unsigned char *hp = s;
      s = t;
      t = hp;
      i = m;
      m = n;
      n = i;
    }

  while( m>0 && s[m-1]==t[n-1] ) --m, --n;     /* skip common suffix */
  while( m>0 && *s == *t ) ++s, ++t, --m, --n; /* skip common prefix */

  if( m == 0 ) {
    REG0 = n >= stop ? FALSE_OBJ : TRUE_OBJ;
  } else if( n == 0 ) {
    REG0 = m >= stop ? FALSE_OBJ : TRUE_OBJ;
  } else {

    d_i=(int*)malloc((sizeof(int))*(m+1));
    if( !d_i ) {
      raise_error(make2(TLREF(1), NIL_OBJ,
                  make_string("out of memory in levenshtein-distance")));
      RETURN0();
    } else {

      /* Step 2(first half) initialise the arrays first column */
      for(j=0; j<=m; ++j) d_i[j]=j;

      /* Step 2(second half), 3 and 4 */

      for(distance=0, i=1; i<=n && distance < stop; ++i) { /* column loop */
        distance=d_i[0];
        d_i[0]=i;
        for(j=1; j<=m && distance < stop; ++j) {    /* row loop */
          left = d_i[j];
	  /* Step 5 */
          cost = s[i-1]==t[j-1] ? 0 : 1;
	  /* Step 6 */
          d_i[j] =  min(min(d_i[j-1]+1, left+1), distance+cost);
          distance = left;
        }
        distance=d_i[m];
      }
      free(d_i);          
      REG0 = distance >= stop ? FALSE_OBJ : TRUE_OBJ;
    }
  }
  RETURN1();
})
