;; (C) 2000-2003 Jörg F. Wittenberger see http://www.askemos.org

;;** Evaluation

;; We need a sandbox, i.e., a restricted environment to evaluate user
;; supplied expressions.

;; TODO There is a description at
;; http://www.rscheme.org/rs/pg1/isolate.scm
;; which should be used as a pattern for this code.  It even stops the
;; execution after a while.  But won't work with threads out of the box.

;; This environment SHOULD have all side effect free definitions of
;; R5RS or at least of R4RS bound. Additionally those DSSSL
;; definitions, which are not too expensive.  Other modules can use
;; dsssl-export! in their initialization (only! there) to export
;; additional bindings, but that MUST be kept to the required minimum
;; for name space control etc.

(define r5rs-side-effecty-things
  '( ;; define <- taken out, sourounded eval with proper let or lambda
     ;; forms to make sure that defines can't extend over the
     ;; evaluation.
    set! set-car! set-cdr! vector-set! vector-fill!
    string-set! string-fill!
    call-with-input-file call-with-output-file
    with-input-from-file with-output-to-file
    open-input-file open-output-file
    peek-char
    read-char
    transcript-on transcript-off
    close-input-port close-output-port
    ))

(define dsssl-forms
  '(attributes
    attribute-ns
    (dsssl-attribute-string attribute-string)
    children ; (dsssl-children children)
    data
    document-element
    (dsssl-element-key element)
    (dsssl-gi gi)
    (dsssl-ns ns)
    first-child-gi
    literal
    (dsssl-make make)
    (dsssl-read read)
    match-element?
    named-node-list-names
    node-list node-list?
    node-list-empty? ;; macro now: empty-node-list
    node-list-first node-list-rest
    node-list-map node-list-filter node-list-reduce
    node-list-length node-list-tail node-list-head
    ;; node-list->list BETTER NOT!  Too much temptation to abusive use.
    node-list->vector
    node-list=? ;; NOBODY wants that!!!
    (dsssl-select-elements select-elements)
    ;;
    copy-attributes
    xml-pi? xml-pi-tag xml-pi-data xml-comment? xml-comment-data
    ;; R5RS
    call-with-values
    ;; SRFI
    find find-tail any every
    append-reverse
    list=
    filter remove delete partition fold
    take take-right drop drop-right unzip2 alist-cons
    alist-ref alist-delete
    lset<= lset= lset-adjoin lset-union lset-intersection
    lset-difference lset-xor lset-diff+intersection
    ;;
    (srfi:string-join string-join)
    string-null?
    (identity string-copy)
    string-trim-both
    ;; SRFI 27
    (random random-integer)
    ;; SRFI 34
    raise
    ;; SRFI 35
    condition? message-condition? condition-message
    ;;
    sort
    ;;
    andmap ormap
    ))

(define srfi19-forms
  '(make-time
    time-type time-second time-nanosecond
    (srfi19:time? time?)
    (srfi19:current-time current-time) time-resolution
    (srfi19:time=? time=?)
    (srfi19:time>? time>?)
    (srfi19:time<? time<?)
    (srfi19:time>=? time>=?)
    (srfi19:time<=? time<=?)
    time-difference add-duration subtract-duration
    time-tai->time-utc time-utc->time-tai
    time-monotonic->time-utc time-monotonic->time-tai
    time-utc->date time-tai->date time-monotonic->date
    date->time-utc date->time-tai date->time-monotonic
    date-year-day
    (srfi19:date? date?)
    make-date
    date-zone-offset date-year date-month date-day date-hour
    date-minute date-second date-nanosecond
    leap-year?
    date-week-day date-week-number
    ;;
    date->julian-day date->modified-julian-day
    time-utc->julian-day time-utc->modified-julian-day
    time-tai->julian-day time-tai->modified-julian-day
    time-monotonic->julian-day time-monotonic->modified-julian-day
    julian-day->time-utc julian-day->time-tai julian-day->time-monotonic
    julian-day->date modified-julian-day->date
    modified-julian-day->time-utc modified-julian-day->time-tai
    modified-julian-day->time-monotonic
    current-date current-julian-day current-modified-julian-day
    (srfi19:date->string date->string)
    (srfi19:string->date string->date)))

(define srfi43-forms
  '(vector-unfold                   vector-unfold-right
    vector-copy                     vector-reverse-copy
    vector-append                   vector-concatenate
    ;;
    ;; * Predicates
    vector?
    vector-empty?
    vector=
    ;;
    ;; * Selectors
    ;; vector-ref
    ;; vector-length
    ;;
    ;; * Iteration
    vector-fold                     vector-fold-right
    vector-map
    vector-for-each
    vector-count
    ;;
    ;; * Searching
    vector-index                    vector-skip
    vector-index-right              vector-skip-right
    vector-binary-search
    vector-any                      vector-every
    ;;
    ;; * Mutators
    ;;
    ;; * Conversion
    (srfi:vector->list vector->list)
                                    reverse-vector->list
    (srfi:list->vector list->vector)
                                    reverse-list->vector
    ))
