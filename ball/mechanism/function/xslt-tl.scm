;; (C) 2000 - 2002, 2013, 2014 Jörg F. Wittenberger see http://www.askemos.org

(define (raise-message fmt . args)
  (raise (make-condition &message 'message (apply format fmt args))))

(define *namespace-environment* (make-symbol-table))

(define (register-tag-transformer! environment ns gi proc)
  (let ((table (xthe (or false (struct hash-table))
		     (hash-table-ref/default environment ns #f))))
    (if (not table)
        (begin
          (set! table (make-symbol-table))
          (hash-table-set! environment ns table)))
    (hash-table-set! table gi proc)))

(define *function-fetch-other*
  (make-shared-parameter
   (lambda (place dst . args)
     (error "internal: function-fetch-other not initilized"))))

(define (function-fetch-other place dst . args)
  (apply (*function-fetch-other*) place dst args))

(define (set-function-fetch-other! f)
  (or (procedure? f) (error "set-function-fetch-other! illegal argument"))
  (*function-fetch-other* f))

(define *function-mind-default-lookup*
  (lambda (place message name)
    (error "internal: function-mind-default-lookup not initilized")))

(define (function-mind-default-lookup place message name)
  (*function-mind-default-lookup* place message name))

(define (set-function-mind-default-lookup! f)
  (or (procedure? f) (error "set-function-mind-default-lookup! illegal argument"))
  (set! *function-mind-default-lookup* f))

(: xsl-parameters (:xsl-envt: --> :xsl-parameter-environment:))
(: xsl-environment-variables (:xsl-envt: --> :xpath-variable-environment:))
(define-record-type <xsl-parameters-and-variables>
  (make-parameters-and-variables parameters variables)
  xsl-environment?
  (parameters xsl-parameters)
  (variables xsl-environment-variables))

(: make-xsl-variables (:xsl-parameter-environment: --> :xsl-envt:))
(define (make-xsl-variables parameters)
  (make-parameters-and-variables parameters (make-wt-tree string-wt-type)))

(: bind-xsl-value ((or :xsl-envt: false) (or string symbol) * --> :xsl-envt:))
(: bind-xsl-value (deprecated parameter-type-specific-versions))
(define (bind-xsl-value variables name val)
  (let ((params (if variables
		    (xsl-parameters variables)
		    '()))
	(vars (if variables
		  (xsl-environment-variables variables)
		  (make-wt-tree string-wt-type))))
    (cond
     ((string? name)
      (make-parameters-and-variables params (wt-tree/add vars name val)))
     ((symbol? name)
      (make-parameters-and-variables (alist-cons name val params) vars))
     (else (raise-message "bind-xsl-value illegal key ~a" name)))))

(: bind-xsl-value/string (:xsl-envt: string * --> :xsl-envt:))
(define (bind-xsl-value/string variables name val)
  (let ((params (xsl-parameters variables))
	(vars (xsl-environment-variables variables)))
    (make-parameters-and-variables params (wt-tree/add vars name val))))

(: bind-xsl-values (:xsl-envt: (list-of (pair (or string symbol) *)) --> :xsl-envt:))
(define (bind-xsl-values variables args)
  (if (null? args)
      variables
      (let loop ((args args)
		 (params (xsl-parameters variables))
		 (vars (xsl-environment-variables variables)))
	(if (null? args)
	    (make-parameters-and-variables params vars)
	    (cond
	     ((string? (caar args))
	      (loop (cdr args) params
		    (wt-tree/add vars (caar args) (cdar args))))
	     ((symbol? (caar args))
	      (loop (cdr args) (cons (car args) params)
		    vars))
	     (else (raise-message "bind-xsl-values illegal key ~a" (car args))))))))

(: xsl-envt-union ((or :xsl-envt: false) (or :xsl-envt: false) --> :xsl-envt:))
(define (xsl-envt-union inner outer)
  (if outer
      (if inner
	  (make-parameters-and-variables
	   (xsl-parameters inner)
	   (wt-tree/union (xsl-environment-variables outer)
			  (xsl-environment-variables inner)))
	  (make-xsl-variables '()))
      inner))

(define xsl-variable-bound?
  (let ((none (list 'none)))
    (define (xsl-variable-bound? name variables)
      (and (string? name)
	   (not (eq?
		 (wt-tree/lookup (xsl-environment-variables variables) name none)
		 none))))
    xsl-variable-bound?))

(: fetch-xsl-variable:unbound (string * string string -> *))
(define (fetch-xsl-variable:unbound name variables path source)
  ;; BEWARE: this had already been refined to make-condition &message before.
  ;; But that supresses error message details one might need.
  (let ((msg (string-append "unbound XSL " path " \"" name "\" in " source)))
    ;; FIXME: there ought to be a parameter to support debugging here.
    ;; I just ran into a situation, where some variable is always
    ;; unbound and some unknown exception handler seems to "fix" this
    ;; magically.  You should know that this is plain waste… Would
    ;; need some better source location tracking.
    (logapperr 'UnKnownPlace "~a\n" msg)
    (raise msg)))

;; This old, generic version, works for both actual variables and
;; symbolic parameters.  We're in the process to replace it with
;; separate versions for both cases.
(: fetch-xsl-variable deprecated)
(define fetch-xsl-variable
  (let ((none (list 'none)))
    (define (fetch-xsl-variable name variables)
      (if (string? name)
	  (let ((a (wt-tree/lookup (xsl-environment-variables variables) name none)))
	    (if (eq? a none)
		(fetch-xsl-variable:unbound name variables "variable" "(unknown)")
		(force a)))
	  (let ((p (assq name (xsl-parameters variables))))
	    (if p (cdr p)
		(fetch-xsl-variable:unbound (symbol->string name) variables "parameter" "(unknown)")))))
    fetch-xsl-variable))

;; This version shall only resolve actual variables.  All it hides it
;; the implementation of the variable environment (which is here still
;; bound to be a wt-tree).
(define fetch-xsl-variable/string
  (let ((none (list 'none)))
    (define (fetch-xsl-variable name bindings source)
      (let ((a (wt-tree/lookup bindings name none)))
	(if (eq? a none)
	    (fetch-xsl-variable:unbound name bindings "variable" source)
	    (force a))))
    fetch-xsl-variable))

(: %node-variable-reference (string --> :sxpath-step:))
(define (%node-variable-reference name)
  (lambda (n r bindings)
    (fetch-xsl-variable/string name (xsl-environment-variables bindings) "XPath variable reference")))

(set-node-variable-reference! %node-variable-reference)

;; This version is to resolve values keyed by symbols, a.k.a
;; parameters.  (Those have usually dynamic scope.)
(define (fetch-xsl-variable/symbol name bindings source)
  (let ((p (assq name bindings)))
    (if p (cdr p)
	(fetch-xsl-variable:unbound (symbol->string name) bindings "parameter" source))))

;; Syntactic sugar par excellance.  Take a request object, which looks
;; as if the http protocol adaptor made it from a web form and return
;; a node list with all form fields matching <key>.

(define (make-form-element-register init n)
  (and (xml-element? n)
       (let ((x (hash-table-ref/default init (gi n) #f)))
         (hash-table-set! init (gi n)
			  (if x (%node-list-cons n x) (node-list n)))))
  init)

(define (make-form-element gi ns attributes content)
  (make-xml-element
   gi ns
   (let ((aa (delay*
	      (node-list-reduce content make-form-element-register (make-symbol-table))
	      'form)))
     (lambda args
       (cond
	((null? args) attributes)
	(else (hash-table-ref/default (force aa) (car args) (empty-node-list)))))) content))

(register-xml-element-constructor!
 'form #f make-form-element)
(register-xml-element-constructor!
 'form namespace-forms make-form-element)

(define (form-field key node)           ; EXPORT
  (let ((docel (document-element node)))
    (if docel
	(let ((form (or (and (eq? (gi docel) 'form) docel)
			(eq? (ns node) namespace-forms)
			(let ((form (document-element (%children docel))))
			  (and (or (eq? (gi form) 'form) (eq? (ns node) namespace-forms))
			       form))))
	      (key (if (symbol? key) key (string->symbol key))))
	  (if form
	      (let ((special (xml-element-special form)))
		(if special
		    (special key)
		    (select-elements (%children form) key)))
	      (empty-node-list)))
	(empty-node-list))))
