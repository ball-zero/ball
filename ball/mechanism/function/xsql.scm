;; (C) 2001 Jörg F. Wittenberger see http://www.askemos.org

(define sql-identifier
  (pcre->proc "^[[:alpha:]$#][[:alnum:]$#_\\.]*$"))

(%early-once-only
 (define sql-operators (make-symbol-table))
 (define (sql-operator? x) (hash-table-ref/default sql-operators x #f))
 (for-each
  (lambda (x) (hash-table-set! sql-operators x #t))
  (cons (string->symbol "||") '(= != <> < > >= <= - + * % / ))))

(define (sql-write expr)
  (call-with-output-string
   (lambda (port)
     (let recurse ((expr expr))
       (always-assert (pair? expr)
		      (format "sql-write: syntax error in ~a" expr))
       (do ((args expr (cdr args)))
	   ((null? args) #t)
	 (let ((obj (car args)))
	   (cond
	    ((xml-element? (document-element obj))
	     (display #\' port)
	     (display (sql-quote (data obj)) port)
	     (display #\' port))
	    ((pair? obj)
	     (if (and (eq? (car obj) 'unquote) (null? (cdr obj)))
		 (begin
		   (display #\, port)
		   (recurse (cdr obj)))
		 (begin (display #\( port)
			(recurse obj)
			(display #\) port))))
	    ((null? obj) (display obj port))
	    ((symbol? obj)
	     (cond
	      ((or (sql-operator? obj)
		   (sql-identifier (symbol->string obj)))
	       (display obj port))
	      ((eqv? obj ':) (display #\, port))
	      (else (raise-message "sql-write: illegal identifier ~a in ~a" obj expr))))
	    (else
	     (display #\' port)
	     (display (cond
		       ((srfi19:date? obj)
			(srfi19:date->string obj "~Y-~m-~d ~H:~M:~f"))
		       (else (sql-quote (literal obj))))
		      port)
	     (display #\' port)))
	   (display #\space port)))))))

(define (sql-exec-protected db arg)
  (sqlite3-exec db (if (string? arg) arg (sql-write arg))))

;;** XSQL

;; For the time being we only support the xsql-query element.  The
;; original Qracle specification allows the outermost element to
;; specify a connection attribute, which is obviously intented to be
;; reused through the page evaluation.  NYI We better do some smart
;; connection pooling under the hood.

;; access embedded, per place data base (sqlite3)

(define xsql-user-database-file
  (make-shared-parameter
   (lambda (identifier)
     (error "xsql-user-database not initialized"))))

;; connections to external data base servers

(define *xsql-connections-definitions* (xthe (or false (struct hash-table)) #f))
(define *xsql-connections-trusts* (xthe (or false (struct hash-table)) #f))

(define max-connection 10)

(define free-connections-mutex (make-mutex 'xsql-db-connections))

(define free-connections (xthe (list-of (pair string list)) '()))

(define connection-count
  (make-semaphore 'xsql-connection-count max-connection))

(define (find-connection-spec name)
  (or (hash-table-ref/default *xsql-connections-definitions* name #f)
      (raise-message "XSQL connection '~a' not defined." name)))

(define (init-xsql doc)
  (set! *xsql-connections-definitions* (make-string-table))
  (set! *xsql-connections-trusts* (make-oid-table))
  (let loop ((def ((sxpath '(connection)) doc)))
    (if (not (node-list-empty? def))
        (let ((row (node-list-first def)))
          (hash-table-set!
           *xsql-connections-definitions*
           (data ((sxpath '(name)) row))
           (list (if (node-list-empty? ((sxpath '(trusts)) row)) #t #f)
                 (data ((sxpath '(driver)) row))
                 (data ((sxpath '(database)) row))
                 (data ((sxpath '(host)) row))
                 (data ((sxpath '(user)) row))
                 (data ((sxpath '(password)) row))))
          (node-list-map
           (lambda (node) (hash-table-set! *xsql-connections-trusts*
					   (string->symbol (data node)) #t))
           ((sxpath '(trusts li)) row))
          (loop (node-list-rest def))))))

(define (allocate-connection name)
  (with-mutex
   free-connections-mutex
   (let ((free (assoc name free-connections)))
     (if free
         (begin (set! free-connections
                      (filter (lambda (value) (not (eq? value free)))
                              free-connections))
                free)
         (and
          (not (null? free-connections))
          (let ((con (car free-connections)))
            (set! free-connections (cdr free-connections))
            con))))))

(define (dispose-connection c)
  (with-mutex
   free-connections-mutex
   (set! free-connections (cons c free-connections))
   free-connections))

;; Find the named connection using oid as the identity of the agent
;; and pass it to 'proc', which runs the query.  Wrap 'proc' into an
;; exception handler and properly clean up.

;; Implementation detail: we could rather had the inner 'proc', which
;; processes the results of 'sql-with-tupels' catch all errors and
;; always leave a clean connection behind.  But sql-with-tupels could
;; also raise an exception (though rarely), hence we would have to
;; have two exception handlers installed for each query.  We avoid
;; that at the expense to close the connection is case of error.

;; While postgresql definiately needs to close the connection, I'm not
;; sure whether mysql might actually continue.  We ran comfortable
;; without problems from 2001-2003.

(define (with-connection name place proc)
  (if name
      (with-semaphore
       connection-count
       (let* ((conn-spec (find-connection-spec name))
	      (conn (allocate-connection name)))
	 (if (not (or (car conn-spec)
		      (hash-table-ref/default
		       *xsql-connections-trusts* (place 'get 'id) #f)))
	     (raise-message "no permissions for data base connection \"~a\"" name))
	 (if (and conn (not (string=? name (car conn))))
	     (begin (sql-close (cdr conn)) (set! conn #f)))
	 (if (not conn)
	     (set! conn (cons name (apply sql-connect (cdr conn-spec)))))
	 (guard
	  (e (else (sql-close (cdr conn)) (error e)))
	  (let ((result (proc (cdr conn))))
	    (dispose-connection conn)
	    result))))
      (proc (place 'get 'sqlite-read-connection))))

;; My 1st impression from the specification is, that those people did
;; not think for a long time before they wrote it.  Anyway even though
;; I start to hate it, standard is better than better.  XSQL is not
;; standard, but we'll have a better thing later.

(: oracle-defaulted ((or false string) symbol --> symbol))
(define (oracle-defaulted v default)
  (if v
      (and (not (string=? v "")) (string->symbol v))
      default))

(: numeric-defaulted (string number -> number))
(define (numeric-defaulted v default)
  (or (and-let* ((n (string->number v))) n) default))

(: symbolic-defaulted (string (or boolean symbol) --> (or boolean symbol)))
(define (symbolic-defaulted v default)
  (if (string-null? v) default (string->symbol v)))

(: xsql-find-attribute-pred (symbol --> (procedure (:xml-attribute:) boolean)))
(define xsql-find-attribute-pred
  (let ((max-rows (lambda (x) (eq? (xml-attribute-name x) 'max-rows)))
	(skip-rows (lambda (x) (eq? (xml-attribute-name x) 'skip-rows)))
	(id-attribute-column (lambda (x) (eq? (xml-attribute-name x) 'id-attribute-column)))
	(id-attribute (lambda (x) (eq? (xml-attribute-name x) 'id-attribute)))
	(row-element (lambda (x) (eq? (xml-attribute-name x) 'row-element)))
	(rowset-element (lambda (x) (eq? (xml-attribute-name x) 'rowset-element)))
	(transpose (lambda (x) (eq? (xml-attribute-name x) 'transpose))))
    (lambda (nm)
      (case nm
	((max-rows) max-rows)
	((skip-rows) skip-rows)
	((id-attribute-column) id-attribute-column)
	((id-attribute) id-attribute)
	((row-element) row-element)
	((rowset-element) rowset-element)
	((transpose) transpose)))))

(: attribute-name-is-select? (:xml-attribute: --> boolean))
(define (attribute-name-is-select? att) (eq? (xml-attribute-name att) 'select))

(define (xsql-with-param-post-process x)
  ;; XPath and XSLt transformations are likely to return "singleton
  ;; node lists" which MAY contain under circumstanced things which
  ;; are not text nodes but may fit as SQLite paramters to bind.  We
  ;; reduce them here for clarity.  This is currently
  ;; replicated/checked in the sqlite3 driver.
  (data x))

(define-transformer xsql-with-param "xsql:with-param"
  (let ((rest (xslt-process-node-list (%%proc-chain)))
	(new-ancestors (cons (current-node) (ancestors))))
    (node-list-map
     (lambda (cn)
       (let ((sel (find-first-attribute attribute-name-is-select? cn)))
	 (xsql-with-param-post-process
	  (if sel
	      (let ((nmsp (xml-attribute-ns sel)))
		(cond
		 ((or (eq? nmsp #f) (eq? nmsp namespace-xsl))
		  ((make-xpath-selection (xml-attribute-value sel) (namespaces))
		   (current-node) (root-node) (environment)))
		 ((eq? nmsp namespace-dsssl3)
		  (let ((expr (string->dsssl-proc2
			       (xml-attribute-value sel) namespace-dsssl3)))
		    (data (apply-transformer use: expr sosofos: (current-node)))))
		 (else (error "unhandled attribute ~a" sel))))
	      (transform
	       current-node: cn
	       ancestors: (cons cn (ancestors))
	       sosofos: (%children cn)
	       process: rest)))))
     (select-elements (%children (sosofo)) 'with-param))))

(define-transformer xsql-query "xsql:query"
  (let* ((nl (sosofo))
	 (xsql-attribute-value
	  (lambda (tag nm cnv default)
	    (cc/lambda-transformer
	     "xsl:attribute"		; tobe replaced by tag
	     (let ((att (find-first-attribute (xsql-find-attribute-pred nm) nl)))
	       (if att
		   (let ((nmsp (xml-attribute-ns att)))
		     (cond
		      ((eq? nmsp #f) (cnv (xml-attribute-value att) default))
		      ((eq? nmsp namespace-dsssl2)
		       (let ((expr (string->dsssl-proc2
				    (xml-attribute-value att) namespace-dsssl2)))
			 (if (procedure? expr)
			     (cnv (apply-transformer use: expr sosofos: (current-node)) default)
			     (cnv expr default))))
		      (else
		       (raise-message "xsql-attribute-value: unsupported namespace ~a " nmsp))))
		   default)))))
	 ;; No, we are not to follow the Oracle default here.
	 (max-rows (apply-transformer use: (xsql-attribute-value "max-rows" 'max-rows numeric-defaulted 25)))
	 (skip-rows (apply-transformer use: (xsql-attribute-value "skip-rows" 'skip-rows numeric-defaulted 0)))
	 (id-attribute-column
	  (apply-transformer use: (xsql-attribute-value "id-attribute-column" 'id-attribute-column symbolic-defaulted #f)))
	 (id-attribute (apply-transformer use: (xsql-attribute-value "id-attribute" 'id-attribute oracle-defaulted 'num)))
	 (mk-att (if id-attribute
		     (lambda (v) (list (make-xml-attribute id-attribute #f v)))
		     (lambda (v) (empty-node-list))))
	 (row-element (apply-transformer use: (xsql-attribute-value "row-element" 'row-element oracle-defaulted 'row)))
	 (mk-row (if row-element
		     (lambda (att r)
		       (make-xml-element row-element #f att r))
		     (lambda (att r) r)))
	 (rowset-element (apply-transformer use: (xsql-attribute-value "rowset-element" 'rowset-element oracle-defaulted 'rowset)))
	 (mk-set (if rowset-element
		     (lambda (r) (make-xml-element
				  rowset-element #f (empty-node-list) r))
		     identity))
	 ;; just an idea	 (transpose (member (xsql-attribute-value 'transpose nl #f) '("1" "y" "transpose")))
	 ;; TODO fix the mxsql-driver to actually return the value
	 (result-node-list (xthe :xml-nodelist: (empty-node-list)))
	 (has-params? (let ((att (attribute-string 'parameterized nl)))
			(and att ;; backbard compatible
			     (not (equal? att "no")))))
	 (template (if has-params?
		       (select-elements (%children nl) 'template)
		       nl))
	 (params (if has-params?
		     (apply-transformer use: xsql-with-param)
		     '()))
	 (query (data (transform sosofos: (children template)))))
    (with-connection
     (attribute-string 'connection nl)
     (current-place)
     (lambda (connection)
       (sql-with-tupels
	connection
	query params
	(lambda (result rows cols)
					;, WARNING sql-field and sql-value can be very slow.
	  (let* ((identifiers
		  (let loop ((i 0))
		    (if (eqv? i cols) '()
			(cons (cons (string->symbol (sql-field result i)) i)
			      (loop (add1 i))))))
		 (result-rows
		  (if id-attribute-column
		      (filter (lambda (r)
				(not (eq? id-attribute-column (car r))))
			      identifiers)
		      identifiers))
		 (result-id
		  (and-let* ((id-attribute-column))
			    (let ((r (filter (lambda (r)
					       (eq? id-attribute-column (car r)))
					     identifiers)))
			      (and (pair? r) (car r))))))
	    (let loop ((row (min rows (+ max-rows skip-rows)))
		       (res (empty-node-list)))
	      (if (eqv? row skip-rows)
		  (set! result-node-list (mk-set res))
		  (loop
		   (sub1 row)
		   (%node-list-cons
		    (mk-row
		     (mk-att (or (and result-id
				      (literal (sql-value result (sub1 row)
							  (cdr result-id))))
				 (number->string row)))
		     (map
		      (lambda (c)
			(make-xml-element
			 (car c) #f
			 (if (and result-id (not row-element))
			     (mk-att (literal (sql-value result (sub1 row) (cdr result-id))))
			     (empty-node-list))
			 (literal
			  (sql-value result (sub1 row) (cdr c)))))
		      result-rows))
		    res)))))))))
    result-node-list))
#|
(define-transformer new-xsql-query
  (let* ((skip-rows (let ((v (attribute-string 'skip-rows nl)))
                      (or (and v (string->number v)) 0)))
         ;; No, we are not to follow the Oracle default here.
         (last-row (let ((v (attribute-string 'max-rows nl)))
                     (+ skip-rows (or (and v (string->number v)) 25))))
         (id-attribute-column
          (let ((att (attribute-string 'id-attribute-column nl)))
            (and att (string->symbol att))))
         (id-attribute (oracle-defaulted
                        (attribute-string 'id-attribute nl)
                        'num))
         (mk-att (if id-attribute
                     (lambda (v) (list (make-xml-attribute id-attribute #f v)))
                     (lambda (v) (empty-node-list))))
         (row-element (oracle-defaulted (attribute-string 'row-element nl)
                                        'row))
         (mk-row (if row-element
                     (lambda (att r)
                       (make-xml-element row-element #f att r))
                     (lambda (att r) r)))
         (rowset-element (oracle-defaulted
                          (attribute-string 'rowset-element nl) 'rowset))
         (mk-set (if rowset-element
                     (lambda (r) (make-xml-element
                                  rowset-element #f (empty-node-list) r))
                     identity)))
    (with-connection
     (or (attribute-string 'connection nl)
         (error "XSQL missing required attribute 'connection'"))
     (place 'get 'id)
     (lambda (connection)
       (mk-set
        (sql-with-tupels%-fold-left
         connection
         (data (xml-walk-down sosofos: (children nl)))
         (lambda (result rows cols)
           (let* ((identifiers (let loop ((i 0))
				 (if (eqv? i cols) '()
				     (cons (cons (string->symbol (sql-field result i)) i)
					   (loop (add1 i))))))
                  (result-cols
                   (if id-attribute-column
                       (filter (lambda (r)
                                 (not (eq? id-attribute-column (car r))))
                               identifiers)
                       identifiers))
                  (result-id
                   (and id-attribute-column
                        (let ((r (filter (lambda (r)
                                           (eq? id-attribute-column (car r)))
                                         identifiers)))
                          (and (pair? r)  (car r))))))
             (list result-cols result-id)))
         (lambda (row-value row res result-cols result-id)
           (cond
            ((< row skip-rows)
             (values #t (add1 row) res result-cols result-id))
            ((<= row last-row)
             (values
              #t (add1 row)
              (mk-row
               (mk-att (or (and result-id
                                (list-ref row-value
                                          (cdr result-id)))
                           (number->string row)))
               (let loop ((cols row-value)
                          (col 0)
                          (template result-cols))
                 (cond
                  ((null? cols) (empty-node-list))
                  ((eqv? col (cdr result-id))
                   (loop (cdr cols) (add1 col) template))
                  (else (%node-list-cons
                         (make-xml-element
                          (caar template) #f (empty-node-list)
                          (make-xml-literal (car cols)))
                         (loop (cdr cols) (add1 col) (cdr template)))))))
              result-cols result-id))
            (else (values #f row res result-cols result-id))))))))))
|#

;; I found this one (akin to xsl:value-of) cheap and practical.

(define-transformer xsql-value-of "xsql:value-of"
  (with-connection
   (attribute-string 'connection (sosofo))
   (current-place)
   (lambda (connection)
     (let* ((has-params? (let ((att (attribute-string 'parameterized (sosofo))))
			   (and att ;; backbard compatible
				(not (equal? att "no")))))
	    (template (if has-params?
			  (select-elements (%children (sosofo)) 'template)
			  (%children (sosofo))))
	    (params (if has-params?
			(apply-transformer use: xsql-with-param)
			'()))
	    (query (data (transform sosofos: template))))
       (sql-with-tupels
	connection query params
	(lambda (result rows cols)
					;, WARNING sql-field and sql-value can be very slow.
	  (if (and (fx>= rows 1) (fx>= cols 1))
	      (literal (sql-ref result 0 0))
	      (let ((when-null (@? 'when-null)))
		(cond
		 ((not when-null) (empty-node-list))
		 ((string=? when-null "raise")
		  (raise (literal "no results from \"" query "\"")))
		 (else (literal "no results from \"" query "\"")))))))))))

(define-transformer xsql-for-each/tables "xsql:for-each (tables)"
  (let ((result (guard
		 (ex (else #f))
		 (sqlite3-exec ((current-place) 'get 'sqlite-read-connection) (current-node)))))
    (if (sql-result? result)
	(let loop ((x 1))
	  (if (eqv? x (vector-length result))
	      '()
	      (%node-list-cons
	       (transform current-node: (vector-ref
					 (vector-ref result x)
					 0)
			  sosofos: (%children (sosofo))
			  process: (xslt-process-node-list (%%proc-chain)))
	       (loop (add1 x)))))
	(empty-node-list))))

(define-transformer xsql-for-each/columns "xsql:for-each (columns)"
  (let* ((result (sqlite3-exec ((current-place) 'get 'sqlite-read-connection) (current-node)))
	 (cols (vector-ref result 0)))
    (let loop ((x 0))
      (if (eqv? x (vector-length cols))
	  '()
	  (%node-list-cons
	   (transform current-node: (vector-ref cols x)
		      sosofos: (%children (sosofo))
		      process: (xslt-process-node-list (%%proc-chain)))
	   (loop (add1 x)))))))

(define-transformer xsql-for-each/select "xsql:for-each (select)"
  (let* ((has-params? (let ((att (attribute-string 'parameterized (sosofo))))
			(and att ;; backbard compatible
			     (not (equal? att "no")))))
	 (template (if has-params?
		       (node-list-filter
			(lambda (n) (not (eq? (gi n) 'with-param)))
			(%children (sosofo)))
		       (%children (sosofo))))
	 (params (if has-params?
		     (apply-transformer use: xsql-with-param)
		     '()))
	 (result (if has-params?
		     (sqlite3-apply ((current-place) 'get 'sqlite-read-connection) (current-node) (debug (current-node) params))
		     (sqlite3-exec ((current-place) 'get 'sqlite-read-connection) (current-node))))
	 (rows (sql-ref result #f #f)))
    (let loop ((x 0))
      (if (eqv? x rows)
	  (empty-node-list)
	  (%node-list-cons
	   (let ((row (make-xml-element
		       'row namespace-xsql
		       (match-lambda
			(() (empty-node-list))
			((? symbol? key) (literal (sql-ref result x key)))
			((('context-position)) x)
			(x (raise-message "SQL row does not understand ~a" x)))
		       (empty-node-list))))
	     (transform current-node: row sosofos: template
			process: (xslt-process-node-list (%%proc-chain))))
	   (loop (add1 x)))))))

(define xsql-for-each-columns-regex
  (pcre/a->proc-uncached
   "^[[:space:]]*(?:\\.columns[[:space:]]+)?([[:alnum:]_]+)[[:space:]]*$"))

(define-transformer xsql-for-each "xsql:for-each"
  (let* ((att (or (find-first-attribute attribute-name-is-select? (sosofo))
		  (raise-message "Missing required attribute \"select\" in xsql:for-each")))
	 (tables (string=? (xml-attribute-value att) "tables"))
	 (cols (and-let* (((not tables))
			  (m (xsql-for-each-columns-regex (xml-attribute-value att))))
			 (cadr m)))
	 (select (cond
		  (tables
		   "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name")
		  (cols (sql-write `(select * from ,cols limit 0)))
		  ((eq? (xml-attribute-ns att) namespace-xsl)
		   (data
		    ((make-xpath-selection (xml-attribute-value att) (namespaces))
		     (current-node) (root-node) (environment))))
		  ((or (eq? (xml-attribute-ns att) namespace-dsssl3)
		       (eq? (xml-attribute-ns att) namespace-dsssl2))
		   (let* ((expr (string->dsssl-proc2
				 (xml-attribute-value att)
				 namespace-dsssl3 ;; (xml-attribute-ns att)
				 ))
			  (sel (data (apply-transformer use: expr sosofos: (current-node)))))
		     (cond
		      ((xsql-for-each-columns-regex sel)
		       =>
		       (lambda (table)
			 (cons 'cols (sql-write `(select * from ,(cadr table) limit 0)))))
		      (else sel))))
		  ((or (not (xml-attribute-ns att))
		       (eq? (xml-attribute-ns att) (ns (current-node))))
		   (xml-attribute-value att))
		  (else #f))))
    (if select
	(let* ((x (and (pair? select) (eq? (car select) 'cols))))
	  (apply-transformer
	   current-node: (if x (cdr select) select)
	   use: (cond
		 ((or cols x) xsql-for-each/columns)
		 (tables xsql-for-each/tables)
		 (else xsql-for-each/select))))
	(empty-node-list))))

;; register them

(register-tag-transformer!
 *namespace-environment* namespace-xsql 'query xsql-query)

(register-tag-transformer!
 *namespace-environment* namespace-xsql 'value-of xsql-value-of)

(register-tag-transformer!
 *namespace-environment* namespace-xsql 'for-each xsql-for-each)

(define-transformer xsql-literal "xsql:literal"
  (let* ((v (@! 'select))
	(att (find-first-attribute attribute-name-is-select? (sosofo))))
    (let ((v (cond
	      ((eq? (xml-attribute-ns att) namespace-xsl)
	       ((make-xpath-selection (xml-attribute-value att) (namespaces))
		 (current-node) (root-node) (environment)))
	      ((or (eq? (xml-attribute-ns att) namespace-dsssl3)
		   (eq? (xml-attribute-ns att) namespace-dsssl2))
	       (let ((expr (string->dsssl-proc2
			    (xml-attribute-value att)
			    namespace-dsssl3 ;; (xml-attribute-ns att)
			    )))
		 (apply-transformer use: expr sosofos: (current-node))))
	      (else ($ v)))))
      (cond
       ((number? v) (literal v))
       ((srfi19:date? v) (srfi19:date->string v "'~Y-~m-~dT~H:~M:~S.~N'"))
       (else (literal #\' (sql-quote (data v))  #\'))))))

(register-tag-transformer!
 *namespace-environment* namespace-xsql 'literal xsql-literal)

