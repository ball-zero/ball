  ;; Note the use of a 1-element vector to hold the value: this lets
  ;; us do the lookup at compile-time:

  ;; TODO: move to SRFI-111 (boxes) instead.

;;;; interpreter.scm - A simple interpreter core useful for evaluating
;;;; code in a "sandboxed" environment - felix

;; This interpreter code should be more understood as a proof of
;; concept than a practicaly useful interpreter.  At least under
;; rscheme it's horribly slow.  ~1/4th of the rscheme interpreter.
;; But the concept is nice and yields a concise and readable source.

;; /JFW

;; recommented reading

;; Dorai Sitaram, "handling Control"
;; http://www.cs.rice.edu/CS/PLT/Publications/Scheme/pldi93-s.ps.gz

;;; The closure-compiler:
;
; - Returns a procedure of one argument that compiles an s-expression
; into a closure, with the global-environment and special-forms given.
; - Environment-representation:
;   a) Compile-time: a list of lists, where each (inner) list contains
;   the names of the variables in that lexical environment. Newer
;   environments are positioned in front of older ones
;   b) Run-time: a list of vectors, where each vector contains the
;   values of the variables in that lexical environment

(cond-expand
 (chicken

  (define-type :i-global-envt: (struct hash-table))
  (define-type :i-envt0: vector)
  (define-type :i-envt: (list-of :i-envt0:))
  (define-type :i-val: (procedure (:i-envt:) :thunk:))
  (define-type :i-lookup: (symbol :i-envt: --> (or false (pair fixnum fixnum))))
  (define-type :i-compile: (* :i-envt: --> :i-val:))
  (define-type :i-expander: (list :i-compile: :i-lookup: :i-envt: -> :i-val:))

  (define interp-chicken-check-interrupts!
    (let ((n 1))
      (lambda ()
	(set! n  (if (fx<= n 0)
		     (begin
		       (chicken-check-interrupts!)
		       10000)
		     (sub1 n))))))

  (: allocate-envt (fixnum -> :i-envt0:))
  (define-inline (allocate-envt n) (make-vector n))

  (define-syntax interp-varref
    (syntax-rules ()
      ((_ bindings no) (##sys#slot bindings no))))

  (define (make-interp-varref i j)
    (case i
      ((0) (lambda (v) 
	     (##sys#slot v j)))
      ((1) (lambda (v) 
	     (##sys#slot (##sys#slot v 0) j)))
      ((2) (lambda (v) 
	     (##sys#slot 
	      (##sys#slot (##sys#slot v 0) 0)
	      j)))
      ((3) (lambda (v) 
	     (##sys#slot 
	      (##sys#slot (##sys#slot (##sys#slot v 0) 0) 0)
	      j)))
      (else
       #;(lambda (v)
	 (##sys#slot (##core#inline "C_u_i_list_ref" v i) j))
       (lambda (e)
	 (do ((k 4 (add1 k))
	      (e (##sys#slot (##sys#slot (##sys#slot (##sys#slot e 0) 0) 0) 0)
		 (##sys#slot e 0)) )
	     ((fx= k i) (##sys#slot e j)) ) ))))

  (define-syntax interp-setvar!
    (syntax-rules ()
      ((_ bindings no v) (##sys#setslot bindings no v))))

  )
 (else
  (define-macro (interp-chicken-check-interrupts!) '(begin))
  (define-macro (interp-varref bindings no)
    `(vector-ref ,bindings ,no))
  (define-macro (interp-setvar! bindings no v)
    `(vector-set! ,bindings ,no v))

  (: allocate-envt (fixnum -> :i-envt0:))
  (define-syntax allocate-envt
    (syntax-rules () ((_ n) (make-vector n))))

  (define (make-interp-varref i j)
    (lambda (e)
      (do ((k 0 (add1 k))
	   (e e (interp-varref e 0)) )
	  ((= k i) (interp-varref e j)) ) ))

  ))

(define-inline (combine-envt inner outer) (begin (interp-setvar! inner 0 outer) inner))

(: extend-envt (:i-envt: list -> :i-envt:))
(define-inline (extend-envt e n vals)
  (let ((v (allocate-envt (add1 n))))
    (do ((i 1 (add1 i))
	 (vals vals (cdr vals)))
	((null? vals)
	 (combine-envt v e))
      (interp-setvar! v i ((car vals) e)))))

(: extend-rec-envt (:i-envt: list -> :i-envt:))
(define-inline (extend-rec-envt e n vals)
  (let* ((v (allocate-envt (add1 n)))
	 (e (combine-envt v e)) )
    (do ((i 1 (add1 i))
	 (xs valsc (cdr xs)) )
	((fx> i n) e)
      (interp-setvar! v i ((car xs) e)) ) ))

(: make-interp-varref (fixnum fixnum --> :i-val:))

(: callof (:i-val: :i-envt: -> (procedure (&rest :i-val:) . *)))

(cond-expand
 (never
  (define (callof fnc e)

    (cond-expand
     (chicken (interp-chicken-check-interrupts!))
     (else (begin)))

    (let ((p (fnc e)))
      (if (procedure? p) p
	  ((interpreter-error-handler)
	   (make-condition
	    &message 'message (format "not a procedure: ~a" p))))))
  )
 (else
  (define-inline (callof fnc e)
    (cond-expand
     (chicken (interp-chicken-check-interrupts!))
     (else (begin )))    
    (fnc e))
  ))

;; CAUTION: the string internalisation currently assumes atomic
;; updates on *interp-interned-strings*

(define interp-flush-intern-cache! (lambda () #f))

(: interp-interned-string-constant (string -> string))
(define interp-interned-string-constant
  (let ((*interp-interned-strings* (make-string-ordered-llrbtree)))
    (set! interp-flush-intern-cache!
	  (lambda ()
	    (set! *interp-interned-strings* (make-string-ordered-llrbtree))))
    (lambda (str)
      (string-ordered-llrbtree-ref
       *interp-interned-strings*
       str
       (lambda ()
	 (string-ordered-llrbtree-set! *interp-interned-strings* str str)
	 str)))))

(: interp-unbound-symbol (symbol * -> . *))
(define (interp-unbound-symbol x exp)
  ((interpreter-error-handler)
   (make-condition
    &message 'message (format "unbound variable ~a in ~a" x exp))))

(: make-interp-unbound-symbol (symbol * -> :i-val:))
(define (make-interp-unbound-symbol x exp)
  (lambda (_)
    (interp-unbound-symbol x exp)))

(: interp-define-new-global (:i-global-envt: symbol * -> (vector :i-val:)))
(define (interp-define-new-global envt var exp)
  ;; Note the use of a 1-element vector to hold the value: this lets
  ;; us do the lookup at compile-time:
  (let ((ref (vector (make-interp-unbound-symbol var exp))))
    (hash-table-set! envt var ref)
    ref))

(: interp-global-ref (:i-global-envt: symbol --> (or false :i-val:)))
(define (interp-global-ref envt var)
  (hash-table-ref/default envt var #f))

(: interp-special-form-ref (:i-global-envt: symbol --> (or false :i-expander:)))
(define (interp-special-form-ref envt form)
  (hash-table-ref/default envt form #f))

;; Lookup variable in compile-time environment, if found return
;; environment- and variable index for the run-time environment. If
;; not found, return #f:

(: interp-lookup :i-lookup:)
(define (interp-lookup var e)
  (let loop ((e e) (i 0))
    (and (pair? e)
	 (let loop2 ((vs (car e)) (j 0))
	   (cond ((null? vs) (loop (cdr e) (add1 i)))
		 ((eq? (car vs) var) (cons i j))
		 (else (loop2 (cdr vs) (add1 j))) ) ) ) ) )

(: make-compiler
   (:i-global-envt:
    :i-global-envt:
    -> (procedure (list) :i-val:)))
(define (make-compiler global-environment special-form-table)
  (lambda (exp . envt)
    ;; Recursive compilation procedure:
    (: compile :i-compile:)
    (: compile-call (symbol list :i-envt: --> :i-val:))
    (define (compile x e)
      (match x
	     ;; constant string ("optimised" case; maybe disable?)
	     ((? string?) (let ((v (interp-interned-string-constant x))) (lambda (_) v)))
	     ((? boolean?) (if x (lambda (_) #t) (lambda (_) #f)))
	     ((? number?)
	      (case x
		((1) (lambda (_) 1))
		((2) (lambda (_) 2))
		((-1) (lambda (_) -1))
		((-2) (lambda (_) -2))
		(else (lambda (_) x))))
	     ((or (? keyword?) (? char?)) ; constant
	      (lambda (_) x) )
	     ((? symbol?)			; variable
	      (match (interp-lookup x e)
		     ((i . j)			; local or lexical...
		      (make-interp-varref i (add1 j)) )
		     ;; global
		     (_ (let ((s (or (interp-global-ref global-environment x)
				     ;; so far we don't support global definitions
				     (raise (interp-unbound-symbol x exp))
				     ;; else: not yet defined... add a new entry
				     (interp-define-new-global global-environment x exp)))) 
			  (lambda (_) (interp-varref s 0) ) ) ) ) )
	     (((? symbol? fn) args ...)	; special-form or function-call
	      (let ((h (interp-special-form-ref special-form-table fn)))
		(if h
		    (h x compile interp-lookup e)	; invoke special-form handler
		    (compile-call fn args e) ) ) )
	     ((fn args ...)
	      (compile-call fn args e) )
	     (_ ((syntax-error-handler) x)) ) )

    ;; Compile procedure call:
    (define (compile-call fn args e)
      (let ((fnc (compile fn e)))
	;; We use some special cases to speed things up:
	(match (map
		;; (cut compile <> e)
		(lambda (x) (compile x e))
		args)
	       (() (lambda (e) ((callof fnc e))))
	       ((a1) (lambda (e) ((callof fnc e) (a1 e))))
	       ((a1 a2) (lambda (e) ((callof fnc e) (a1 e) (a2 e))))
	       ((a1 a2 a3) (lambda (e) ((callof fnc e) (a1 e) (a2 e) (a3 e))))
	       ((a1 a2 a3 a4) (lambda (e) ((callof fnc e) (a1 e) (a2 e) (a3 e) (a4 e))))
	       (as (lambda (e) (apply (callof fnc e)
				      (map
				       ;; (cut <> e)
				       (lambda (f) (f e))
				       as)))) ) ) )
					;(trace interp-lookup compile)
    (compile exp envt) ) )


;;; Definition of special forms:
;
; - A special-form table is a hash-table (eq? predicate) that maps
; symbols to compilation-procedures - A special form is defined as a
; procedure taking four arguments:
;
;   1) The complete expression to be compiled
;   2) a compile-procedure that takes an expression and a compile-time
;   environment and returns a procedure
;   3) a lookup-procedure that takes a symbol and a compile-time
;   environment and returns either #f (if the variable is not defined)
;   or a pair of the form (<run-time environment-index>
;   . <variable-index>).
;   4) the current compile-time environment
;
; - The expander of a macro-special form is a simple one-argument
; procedure that gets an expression and returns another expression
; which is compiled recursively.

(define (macro-expander expander)
  (lambda (x c l e) (c (expander x) e)) )


;;; Helper routines:

;; Parses leading `(define ...) forms inside a body and returns a
;; single s-expression (probably transformed to a `letrec' form:

(: parse-body-definitions (list --> list))
(define (parse-body-definitions body)
  (let loop ((body body) (defs (xthe list '())))
    (match body
      (() ((syntax-error-handler) body))
      ((('define (? symbol? head) val) . more)
       (loop more (cons (list head val) defs)) )
      ((('define ((? symbol? head) . llist) body ...) . more)
       (loop more (cons (list head `(lambda ,llist ,@body)) defs)) )
      (_ (if (null? defs)
	     `(,begin0 ,@body)
	     ;; rerevese! since it's just consed up.
	     `(letrec ,(reverse! defs) ,@body) ) ) ) ) )


;;; A special-form table with some default entries:
;
; - Supports: quote, lambda, let, let*, letrec, begin, if
;             and-let*, guard

(define (interp:make-toplevel-environment) (make-symbol-table))

(define r4rs-special-form-table (make-symbol-table))

(define (interp:r4rs-environment) r4rs-special-form-table)

(: interp:define-special-form! (:i-global-envt: symbol :i-expander: -> undefined))
(define (interp:define-special-form! special-form-table operator compile)
  (hash-table-set! special-form-table operator compile))

(interp:define-special-form!
 r4rs-special-form-table 'quote
 (lambda (x c l e)
   (match x
     ((_ const) (lambda _ const))
     (_ ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'lambda
 (lambda (x c l e)
   ;; Decompose the lambda-list:
   (define (decomp llist0)
     (let loop ((llist llist0) (vars '()) (argc 0))
       ;; rerevese! since it's just consed up.
       (cond ((null? llist) (values (reverse! vars) argc #f))
	     ((symbol? llist) (values (reverse! (cons llist vars)) argc llist))
	     ((not (pair? llist)) ((syntax-error-handler) x))
	     (else (loop (cdr llist) (cons (car llist) vars) (add1 argc))) ) ) )
   (match x
     ((_ llist body ...)
      (receive
       (vars argc rest) (decomp llist)
       (let* ((e2 (if (null? vars) e (cons vars e)))
              (bodyc (c (parse-body-definitions body) e2)) )
         (if rest
	     ;; Lambda with rest-parameter:
	     (let ((vn (length vars)))
	       (lambda (e)
		 (lambda as
		   (let ((e2 (allocate-envt (add1 vn))))
		     (let loop ((as as) (i 1))
		       (cond ((fx> i argc) 
			      (interp-setvar! e2 i as)
			      (bodyc (combine-envt e2 e)) )
			     ((null? as)
			      ((interpreter-error-handler)
			       (make-condition
				&message 'message
				(format "too few arguments (~a) in function call ~a"
					(length as) vn))) )
			     (else
			      (interp-setvar! e2 i (car as))
			      (loop (cdr as) (add1 i)) ) ) ) ) ) ))
	     ;; ... without rest-parameter:
	     (case argc
	       ;; We handle 0-5 args specially (more efficient):
	       ((0) (lambda (e) (lambda () (bodyc e))))
	       ;; NO, we don't; at least unless we compile with argcount-checks
#|
	       ((1) (lambda (e) (lambda (a1) (bodyc (cons (vector a1) e)))))
	       ((2) (lambda (e) (lambda (a1 a2) (bodyc (cons (vector a1 a2) e)))))
	       ((3) (lambda (e) (lambda (a1 a2 a3) (bodyc (cons (vector a1 a2 a3) e)))))
	       ((4) (lambda (e) (lambda (a1 a2 a3 a4) (bodyc (cons (vector a1 a2 a3 a4) e)))))
|#
	       (else
		(let ((vn (length vars)))
		  (lambda (e)
		    (lambda as
		      (let ((e2 (allocate-envt (add1 vn))))
			(let loop ((as as) (i 1))
			  (cond ((fx> i vn)
				 (if (null? as)
				     (bodyc (combine-envt e2 e))
				     ((interpreter-error-handler)
				      (make-condition
				       &message 'message
				       (format "too many arguments (~a) in function call ~a"
					       (length as) vn))) ) )
				((null? as)
				 ((interpreter-error-handler)
				  (make-condition
				   &message 'message
				   (format "too few arguments (~a) in function call ~a"
					   (length as) argc))) )
				(else
				 (interp-setvar! e2 i (car as))
				 (loop (cdr as) (add1 i)) ) ) ) ) ) ) ) ) )) ) ) )
     (_ ((syntax-error-handler) x)) ) ) )

(define begin0 (string->symbol " begin0"))

(interp:define-special-form!
 r4rs-special-form-table begin0
 (lambda (x c l e)
   (match x
     ((_) (lambda _ (void)))
     ((_ x) (let ((xc (c x e))) (lambda (e) (xc e))))
     ((_ x1 . xs)
      (let ((xc (c x1 e))
	    (xsc (c `(,begin0 ,@xs) e)) )
	(lambda (e) (xc e) (xsc e)) ) )
     (_ ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'begin
 (macro-expander
  (match-lambda
   ((_ body ...) (parse-body-definitions body))
   (x ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
  r4rs-special-form-table 'let
  (lambda (x c l e)
    (match x
      ((_ ((vars vals) ...) body ...)
       (let ((valsc (map
		     ;; (cut c <> e)
		     (lambda (v) (c v e))
		     vals))
	     (bodyc (c (parse-body-definitions body) (cons vars e))) )
	(lambda (e)
	  (bodyc (extend-envt e (length valsc) valsc)) ) ) )
      ((_ (? symbol? n) ((vars vals) ...) body ...)
       (c `((letrec ((,n (lambda ,vars ,@body)))
	     ,n)
	   ,@vals)
	 e) )
      (_ ((syntax-error-handler) x)) ) ))

(interp:define-special-form!
 r4rs-special-form-table 'letrec
 (lambda (x c l e)
   (match x
     ((_ ((vars vals) ...) body ...)
      (let ((e2 (cons vars e))
	    (n (length vars)))
	(let ((valsc (map
		      ;; (cut c <> e2)
		      (lambda (e) (c e e2))
		      vals))
	      (bodyc (c (parse-body-definitions body) e2)))
	  (lambda (e) (bodyc (extend-rec-envt e n valsc)) )) ) )
     (_ ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'let*
 (macro-expander
  (match-lambda
    ((_ () body ...) (parse-body-definitions body))
    ((_ ((var val) . more) . body)
     `(let ((,var ,val)) (let* ,more . ,body)))
    (x ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'if
 (lambda (x c l e)
   (match x
     ((_ x y) 
      (let ((xc (c x e))
            (yc (c y e)) )
	(lambda (e) (if (xc e) (yc e))) ) )
     ((_ x y z) 
      (let ((xc (c x e))
            (yc (c y e)) 
            (zc (c z e)) )
	(lambda (e) (if (xc e) (yc e) (zc e))) ) )
     (_ ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'cond
 (macro-expander
  (match-lambda 
    ((_) #f)				; Oder was anderes...
    ((_ ('else . clause)) `(,begin0 ,@clause))
    ((_ (test '=> proc) . more)
     (let ((v (gensym)))
       `(let ((,v ,test))
	  (if ,v (,proc ,v) (cond ,@more)) ) ) )
    ((_ (test . body) . more)
     `(if ,test (,begin0 ,@body) (cond ,@more)) ) 
    (x ((syntax-error-handler) x)) ) ) )

;; The 'case0' is to avoid re-evaluation of the 'key' value.

(define case0 (string->symbol " case0"))

(interp:define-special-form!
 r4rs-special-form-table case0
 (macro-expander
  (match-lambda 
    ((_ key) #f)			; Oder was anderes...
    ((_ key ('else . clause)) `(,begin0 . ,clause))
    ((_ key (test . body) . more)
     `(if (memv ,key ',test) (,begin0 . ,body) (case ,key . ,more)) ) 
    (x ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'case
 (macro-expander
  (match-lambda 
    ((_ key) #f)			; Oder was anderes...
    ((_ key ('else . clause)) `(,begin0 . ,clause))
    ((_ key (test . body) . more)
     (let ((kv (gensym)))
       `(let ((,kv ,key))
	  (if (memv ,kv ',test) (,begin0 . ,body) (,case0 ,kv . ,more)))) ) 
    (x ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'and
 (macro-expander
  (match-lambda
    ((_) #t)
    ((_ x) x)
    ((_ x . more) `(if ,x (and ,@more) #f)) 
    (x ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'or
 (lambda (x c l e)
   (match x
     ((_) (lambda (e) #f))
     ((_ x) (let ((xc (c x e))) (lambda (e) (xc e))))
     ((_ x . more)
      (let ((xc (c x e))
            (rc (c `(or . ,more) e)))
        (lambda (e) (or (xc e) (rc e)))) ) )))

(interp:define-special-form!
 r4rs-special-form-table 'do
 (macro-expander
  (match-lambda
    ((_ bindings (test . result) . body)
     (let ((loop (gensym)))
       `(let ,loop 
	  ,(map (match-lambda
		  ((v i . _) (list v i))
		  (x ((syntax-error-handler) x)) )
		bindings)
	  (if ,test
	      (,begin0 ,@result)
	      (,loop ,@(map (match-lambda 
			      ((_ _ s) s)
			      ((v _) v)
			      (x ((syntax-error-handler) x)) )
			    bindings) ) ) ) ) )
    (x ((syntax-error-handler) x)) ) ) )

(interp:define-special-form!
 r4rs-special-form-table 'quasiquote
 (macro-expander
  (let ()
    (define (walk x n) (simplify (walk1 x n)))
    (define (walk1 x n)
      (match x
	((? vector?) `(list->vector ,(walk (vector->list x) n)))
	((head . tail)
	 (case head
	   ((unquote)
	    (if (pair? tail)
		(let ((hx (car tail)))
		  (if (zero? n)
		      hx
		      (list 'list '(quote unquote)
			    (walk hx (sub1 n)) ) ) )
		'(quote unquote) ) )
	   ((quasiquote)
	    (if (pair? tail)
		`(list (quote quasiquote) 
		       ,(walk (car tail) (add1 n)) ) 
		(list 'cons (list 'quote 'quasiquote) (walk tail n)) ) )
	   (else
	    (if (pair? head)
		(let ((hx (car head))
		      (tx (cdr head)) )
		  (if (and (eq? hx 'unquote-splicing) (pair? tx))
		      (let ((htx (car tx)))
			(if (zero? n)
			    `(append ,htx
				     ,(walk tail n) )
			    `(cons (list 'unquote-splicing
					 ,(walk htx (sub1 n)) )
				   ,(walk tail n) ) ) )
		      `(cons ,(walk head n) ,(walk tail n)) ) )
		`(cons ,(walk head n) ,(walk tail n)) ) ) ) )
	(_ `(quote ,x)) ) )
     (define simplify 
       (match-lambda
	 (('cons a ''()) (simplify `(list ,a)))
	 (('cons a ('list . b)) (simplify `(list ,a ,@b)))
	 (('append a ''()) a)
	 (x x) ) )
     ;(trace walk simplify)
     (match-lambda
       ((_ x) (walk x 0))
       (x ((syntax-error-handler) x)) ) ) ) )

;; SRFI 2

(interp:define-special-form!
 r4rs-special-form-table 'and-let*
 (macro-expander
  (lambda (x)
    ((lambda (claws body)
       (let* ((new-vars '())
              (result (cons 'and '()))
              (growth-point result))

         (define (andjoin! clause)
           (let ((prev-point growth-point)
                 (clause-cell (cons clause '())))
             (set-cdr! growth-point clause-cell)
             (set! growth-point clause-cell)))

         (if (not (list? claws))
             ((syntax-error-handler) "Bindings are not a list: ~s" claws))

         (for-each
          (lambda (claw)
            (cond
             ((symbol? claw)            ; BOUND-VARIABLE form
              (andjoin! claw))
             ((and (pair? claw) (null? (cdr claw)))  ; (EXPRESSION) form
              (andjoin! (car claw)))
             ((and (pair? claw) (symbol? (car claw)) ; (VARIABLE EXPRESSION) form
                   (pair? (cdr claw)) (null? (cddr claw)))
              (let* ((var (car claw))
                     (var-cell (cons var '())))
                (if (memq var new-vars)
                    ((syntax-error-handler)
		     "Duplicate variable in bindings: ~s" var))
                (set! new-vars (cons var new-vars))
                (set-cdr! growth-point `((let (,claw) (and . ,var-cell))))
                (set! growth-point var-cell)))
             (else
              ((syntax-error-handler) "Ill-formed binding: ~s" claw))))
          claws)
         (if (not (null? body))
             (if (null? (cdr body))
                 (andjoin! (car body))
                 (andjoin! `(,begin0 ,@body))))
;    (newline) (display result) (newline)  ; uncomment to show expansion
         result))
     (cadr x) (cddr x)))))

(interp:define-special-form!
 r4rs-special-form-table 'receive
 (macro-expander
  (lambda (x)
    ((lambda (bindings expression body)
       `(call-with-values (lambda () ,expression)
	  (lambda ,bindings . ,body)))
     (cadr x) (caddr x) (cdddr x)))))

;; SRFI 34

;; This guard definition looks slightly suspicious.  It's almost, but
;; not quite what the srfi-34 reference implementation does.

(interp:define-special-form!
 r4rs-special-form-table 'guard
 (macro-expander
  (lambda (x)
    ((lambda (clause body)
       (let ((return (gensym)))
	 `((call-with-current-continuation
	    (lambda (,return)
	      (with-exception-handler
	       (lambda (,(car clause))
		 (call-with-current-continuation
		  (lambda (reth)
		    (,return
		     (lambda ()
		       (cond . ,(if (assq 'else (cdr clause))
				    (cdr clause)
				    (append (cdr clause)
					    `((else (raise ,(car clause))))))))))))
	       (lambda ()
		 (call-with-values (lambda () . ,body)
		   (lambda args (,return (lambda () (apply values args))))))))))))
     (cadr x) (cddr x)))))

;; SXML

(interp:define-special-form!
 r4rs-special-form-table 'SXML
 (macro-expander
  (lambda (x) `(sxml (,'quasiquote (*TOP* . ,(cdr x)))))))

;;; Global environments:
;
; - A global environment is a hash-table (eq? predicate) that maps
; symbols to "boxes", i.e.  one-element vectors containing the actual
; value.
; - The following code is for testing purposes, only.

(define global-environment (make-symbol-table))

(define interpreter-error-handler
  (make-shared-parameter raise) )

(define syntax-error-handler
  (make-shared-parameter
   (lambda (f . args)
     (raise
      (call-with-output-string
       (lambda (ep)
	 (display "syntax error " ep)
	 (if (pair? args)
	     (apply format ep f args)
	     (begin (display "in " ep) (pretty-print f ep)))))))) )

; (define syntax-error-handler
;   (make-parameter
;    (cut error "syntax error" <...>)) )

;(define (expand x)
;  (pretty-print
;   ((hash-table-ref/default r4rs-special-form-table (car x) #f)
;    x
;    (lambda (x e) x)
;    #f
;    #f) ) )

;(expand '(quasiquote (1 (2 ,(99) 3) ,4 ,@5)))
