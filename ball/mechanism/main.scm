;; (C) 2000-2013 Jörg F. Wittenberger see http://www.askemos.org

(define $startup-verbose (make-shared-parameter #t))

;; This should be autogenerted at one point:
(define ball-software-version "0.9.5")

(define-macro (logstartup lvl fmt . args)
  (let ((v (gensym)))
    (or (number? lvl) (boolean? lvl)
	(error "logstartup wrong lvl argument"))
    `(let ((,v ($startup-verbose)))
       (if (and ,v ,(cond
		     ((eq? lvl #t) #t)
		     ((number? lvl) `(if (boolean? ,v) (eq? ,v #t) (> ,lvl ,v)))
		     (else #t)))
	   (logerr ,fmt . ,args)))))

;;** Read ACL file

(define (set-acl! tree)
  (define p-p
    ;; caution: (public-context) not yet valid
    (let ((p-c (make-context (my-oid))))
      (lambda (node)
        (let* ((d (data node))
               (p (mind-parse-protection d p-c)))
          (if (member #f p) (error "set-acl!: can't resolve ~a" p) p)))))
  (pool-fold! 1 (lambda (k v i) (fset! v 'capabilities '())) #f)
  (for-each
   (lambda (node)
     (let* ((id (attribute-string 'id node))
            (secret (attribute-string 'secret node))
            (oid (if id (entry-name->oid id) (error "set-acl! no id")))
            (frame (if oid (find-frames-by-id oid) #f)))
       (if frame
           (let ((capabilities
                  (map p-p (select-elements (children node) 'grant))))
             (respond!
              frame
              (lambda (me msg)
                (define setslot! (me 'get 'writer))
                (setslot! 'capabilities capabilities)
                (if secret (setslot! 'secret (msg 'secret))))
              respond-local #t
              (make-message (property 'capabilities capabilities))))
           (logerr "set-acl! no place ~a" id))))
   (select-elements (children (document-element tree)) 'user)))

(define *start-protection!
  (lambda ()
    (with-mind-access
     (lambda (tree)
       (with-a-ref (my-oid) (set-acl! tree)))
     (document-element (aggregate-body (find-frames-by-id (my-oid)))))))

(: set-start-protection! ((procedure () . *) -> . *))
(define (set-start-protection! proc) (set! *start-protection! proc))
(define (start-protection!) (*start-protection!))


;;* TrustedCode

;; Initialization here.  The base is currently implemented in util.scm.


(define (do-init-trustedcode from-frame)
  (let* ((public (find-local-frame-by-id (public-oid) 'do-init-trustedcode))
	 (reply (call-subject! from-frame (replicates-of public)
			       'read
			       (make-message
				(property 'capabilities (fget public 'capabilities))
				(property 'destination '())))))
    (reset-trustedcode!)
    (node-list-map
     (lambda (row)
       (let* ((cells ((sxpath '(td)) row))
              (oid (string->oid (data (node-list-first cells))))
              (name (data (node-list-first (node-list-rest cells))))
              (r/w (equal? (data (node-list-first
                                  (node-list-rest
                                   (node-list-rest cells))))
                           "read")))
	 (guard
	  (exception (else
		      (receive
		       (title msg args rest) (condition->fields exception)
		       (logerr "TrustedCode initialization ~a ~a ~a\n"
			       title msg args))))
	  (register-trustedcode! oid name r/w
				 (eval (call-with-input-string name read))))))
     ((sxpath '(// (table (@ (equal? (id "TrustedCode")))) // tr))
      (message-body reply)))))

(define (init-trustedcode from-oid)
  (reset-trustedcode!)
  (if from-oid
      (let ((frame (find-frames-by-id from-oid)))
        (if frame
            (do-init-trustedcode frame)
            (logerr "trusted code object ~a lost\n" from-oid))))
  (and
   (public-oid)
   (or
    ;; Old, backward compatible style. Requires "public" to be modified during setup.
    (and-let* ((public (aggregate-meta (find-local-frame-by-id (public-oid) 'init-trustedcode)))
	       (system (frame-resolve-link public "system" #f)))
	      (register-trustedcode! system "ball-control" #f ball-control)
	      (register-trustedcode! system "ball-info" #t ball-info))
    ;; Experimental.  Currently depends on control account with well known name "system".
    (and-let*
     ((sysuser (entry-name->oid "system"))
      (sysp (find-local-frame-by-id sysuser 'init-trustedcode))
      ;; If there is a "system" link, we use that one, otherwise the account itself.
      (system (or (frame-resolve-link (aggregate-meta sysp) "system" #f)
		  sysuser)))
     (register-trustedcode! system "ball-control" #f ball-control)
     (register-trustedcode! system "ball-info" #t ball-info)))))

;;* XSQL initialization

(define (init-xsql-from-config)
  (define fn #f)
  (guard
   (exception (else (receive
                     (title msg args rest) (condition->fields exception)
                     (logerr "XSQL initialization ~a ~a ~a (ignored)\n"
			     title msg args))))

   (if fn (init-xsql (document-element (xml-parse fn))))))

(define askemos-satelite (lambda () #f))

(define (set-satelite! proc) (set! askemos-satelite proc))

(define (askemos-switch request)
  (let ((action (get-slot request 'soapaction)))
    (if action
	(askemos-sync request action)
	(askemos-web-user request))))

(define (askemos-web request)
  (enter-front-court askemos-switch request))

;; TODO define clear structure to get this from:

(define $getput-oid (make-config-parameter #f))

(define (getput request)
  (if ($getput-oid)
      (enter-front-court (make-askemos-public-user ($getput-oid)) request)
      (begin
	(set-slot! request 'mind-body "get-as-put-receiver not configured")
	(set-slot! request 'http-status 500)
	request)))

;;* Proxy request handling

(define (kernel-extent-proxy-forward-implementation access request how)
  (http-response
   (apply make-message
          (property 'type (if (eq? how 'write) "POST" "GET"))
          (property 'dc-identifier
                    (let ((s (request 'dc-identifier)))
                      (substring
                       s
                       (add1 (string-index s #\/ (fx+ (string-index s #\/) 2)))
                       (string-length s))))
          (map (lambda (hdr) (property hdr (request hdr)))
               '(content-length
                 content-type
                 mind-body
                 host)))
   ))

;;** Startup and Wait

;; Fix this KLUDGE: we examine *the-registered-stores* and if we find
;; fsm-storage-adaptor, we commit into it.

(define (kludge-commit-to-safe-the-well-know-places)
  (for-each
   (lambda (storage-adaptor)
     (commit-mind-into (list storage-adaptor) 1))
   (filter
    (lambda (storage-adaptor)
      (eq? (car storage-adaptor) fsm-storage-adaptor))
    *the-registered-stores*)))

;; move this to mechanism/setup.scm
(define $acl-file-name (make-pathname '("mechanism") "ACL.xml"))
(define (basic-wake-up server)
  (define (with-defaults result)
    (kludge-commit-to-safe-the-well-know-places)
    (logerr "wake-up: Started up private ~a public ~a.\n"
            (oid->string (my-oid)) (oid->string (public-oid)))
    result)
  (if (not (public-oid))                ; very simple security scheme
      (lambda (message)
        (with-mind-access-now
         (lambda (message)
           (logerr "wake-up: Setting up default access: simple ACL.\n")
           (let ((id null-oid))
	     (with-a-ref
	      id
	      (let ((body (filedata $acl-file-name))
		    (protection (list id)))
		(let ((obj (make-place-for-id!
			    id
			    (property 'dc-date (current-date (timezone-offset)))
			    (property 'content-type text/xml)
			    (property 'mind-body body)
			    (property 'body/parsed-xml (xml-parse body))
			    (property 'protection protection)
			    (property 'capabilities '())
			    (property 'replicates *local-quorum*))))
		(commit-frame! #f obj (aggregate-meta obj) '())))))
           ;; Be careful.  The following let* would break, if it was
           ;; possible to restart after the first message.  (Which is
           ;; impossible for higher level reasons (can't learn half a
           ;; lesson).
           (let ((reply (server message)))
	     (with-a-ref
	      one-oid
	      (with-mutex
	       (respond!-mutex one-oid)
	       (let ((one-place (find-local-frame-by-id one-oid 'wake-up)))
		 (fset! one-place 'mind-action-document one-oid)
		 (fset! one-place 'protection (list one-oid))
		 (fset! one-place 'capabilities (list (list one-oid))))))
             (with-defaults reply)))
         message))
      (with-defaults server)))
(define wake-up basic-wake-up)
(define (set-wake-up! proc) (set! wake-up proc))

(define (install-wakeup-hooks! patch-in-zero-state with-defaults private)
(set-wake-up!
 (let ((with-defaults wake-up))
   (lambda (server)
     (lambda (message)
       ;; (define (with-mind-access-now f m) (f m))
       (logerr "Full setup\n")
       ($use-well-known-symbols #f)
       (if (public-oid)
	   (server message)
	   (with-mind-access-now
	    (lambda (message)
	      (let* ((host (make-place! (let ((message (message-clone message)))
					  ;; TBD: Maybe we do not really have to set these here?
					  (set-slot! message 'dc-creator null-oid)
					  (set-slot! message 'mind-action-document null-oid)
					  message)))
		     (who-can-be-found-at
		      (begin (bind-place! null-oid host #t)
			     (and-let* ((secret (fget host 'secret)))
				       (fset! host 'secret #f #;(secret-encode secret
									  (md5-digest (literal (current-date))))))
			     (fset! host 'replicates (make-quorum (list (aggregate-entity host))))
			     (set-my-place! host)
			     (commit-frame! #f host (aggregate-meta host) '())))
		     (replies (server message))
		     (public
		      (let* ((names (fget (find-frames-by-id (my-oid)) 'mind-links)))
			(find-frames-by-id
			 (fold-links (lambda (k v i) (or i v)) #f names))))
		     (e (list (aggregate-entity public)
			      (aggregate-entity host)))
		     (q (make-quorum (list (aggregate-entity public)))))
		(logit display "Virgin start set up host and public.\n")
		(fset! host 'protection (cdr e))
		(let ((v0 (fget host 'version))
		      (make-frame-version cons))
		  ;; Rarely it happens that the cache was flushed and
		  ;; then the zero-version of the private does not
		  ;; start.  This is the private space, not to be
		  ;; versioned anyway.
		  (fset! host 'version (make-frame-version (add1 (version-serial v0)) (version-chks v0))))
		(fset! host 'replicates q)
		(fset! host 'capabilities (map list e))

		(if patch-in-zero-state
		    (fset! public 'capabilities (list e))
		    (fset! public 'capabilities (list (list (car e)))))

		(fset! public 'protection (list (car e)))
		(fset! public 'dc-creator (aggregate-entity host))	; ?
		(if (and patch-in-zero-state (not (eq? patch-in-zero-state 'except-replicates)))
		    (fset! public 'replicates q))
		(set-public-place! public)
		(bind-place! one-oid public #t)
		(bind-place! (aggregate-entity public) public #t)
		(commit-frame! #f public (aggregate-meta public) '())
		(commit-frame! #f host (aggregate-meta host) '())
					;		     (frame-commit! (aggregate-entity public))
					;		     (frame-commit! (aggregate-entity host))
					;		     (kludge-commit-to-safe-the-well-know-places)
		(set-wake-up! with-defaults)
		(set-local-quorum! (make-quorum (list (aggregate-entity public))))
		(http-host-add! (aggregate-entity public) (local-id) #f #t)
		(if patch-in-zero-state
		    (assert (match
			     (fget (document (public-oid)) 'capabilities)
			     (((a b)) #t)
			     (_ #f)))
		    (assert (match
			     (fget (document (public-oid)) 'capabilities)
			     (((a)) #t)
			     (_ #f))))
		(assert (match
			 (fget (document (my-oid)) 'capabilities)
			 (((a) (b)) #t)
			 (_ #f)))
		replies))
	    message))))))
(set-start-protection!
 (lambda ()
   (i-call-you! (my-oid) (public-oid) (list (public-oid) (my-oid))) ;; NOT one-step setup
   ;; one-step setup Oh horror: for this questionable goal of having a
   ;; eternal "public" we need so much additional patching.
   (let ((public (find-frames-by-id (public-oid))))
     (fset! public 'replicates (make-quorum (list (aggregate-entity public))))
     (commit-frame! #f public (aggregate-meta public) '()))
   (use-mandatory-capabilities #t)
					;(kludge-commit-to-safe-the-well-know-places)
   )))

(define (run-initial-wakeup nl)
  (define handle
    (wake-up
     (lambda (request)
       (make-local-entry-point!
	(make-quorum (list one-oid))
	"public"
	((sxpath '(link new))
	 (or (get-slot request 'body/parsed-xml)
	     (xml-parse (get-slot request 'mind-body))))))))
  (if (string? nl) (set! nl (xml-parse nl)))
  (handle
   (make-message
    (property 'content-type "text/xml")
    (property 'dc-identifier "")
    (property 'dc-date (current-date (timezone-offset)))
    (property
     'mind-body
     (xml-format
      (sxml
       `(api:reply
	 (@ (@ (*NAMESPACES* (,namespace-mind ,namespace-mind-str api))))
	 (link
	  (@ (name "public"))
	  (new
	   (@ (secret ""))
	   #|
	   2017-12-11: FIXME For the time being we do not attach anything as
	   it seems to be the culprit why we still find objects without creator.

	   This is one of the reasons why I want a rewrite.
	   Once we figured out the culprit we likely remove the latter when we
	   simpify the consensus implementation.  Experiments with STM based on

	   the chicken egg "hopefully" look promising.
	   However this is going to be a major rewrite and unlikely to be
	   backward compatible ever.

	   2018-01-20: Issue seems to be fixed; even without a rewrite.
	   It works, but it is still a bad idea, since puts pressure on replication.

	   (link
	   (@ (name "favicon.ico"))
	   (new (@ (action ,(literal one-oid)))
	   ($$ ,make-output ,(filedata (make-pathname '("app") "favicon" "ico")) "image/x-icon")))
	   |#
	   (output (@ (method "xml") (media-type "text/xml")) ($$ ,nl))))))))
    )))

(define (sc-from-markdown-file file)
  (let* ((nl ((mime-converter "text/xml" "text/x-markdown; option=passive")
	      (filedata file)))
	 (title (data (node-list-first ((sxpath '((or@ title h1 h2 h3 h4 h5 h6 b))) nl)))))
    (sxml
     `(html
       (@ (@ (*NAMESPACES* (,namespace-mind ,namespace-mind-str CoreAPI))))
       (head
	(title ($$ ,title))
	(meta (@ (http-equiv "Content-Type") (content "text/html; charset=UTF-8")))
	(style (@ (type "text/css")) "programlisting { font-size: x-small; text-align: right; }")
	)
       (body
	($$ ,(node-list-filter (lambda (n) (not (eq? n title))) (children nl))))
       (CoreAPI:method (@ (type "accept")) (programlisting "public-place()"))))))

(define (install-initial-contract public #!key (private #f) (patch-in-zero-state 'except-replicates))
  ;; (define patch-in-zero-state 'except-replicates)  ;; #f #t 'except-replicates
  (assert (not (public-oid)) "initial contract already installed")
  (install-wakeup-hooks! patch-in-zero-state wake-up private)
  (unless (file-exists? public)
	  (logerr "file ~a does not exist" public)
	  (exit 1))
  (logerr "installing initial contract from ~a\n" public)
  (run-initial-wakeup
   (cond
    ((string-suffix? ".md" public) (sc-from-markdown-file public))
    (else (xml-parse (filedata public)))))
  (if (not patch-in-zero-state)
      (begin
	(logerr "patching initial contract\n")
	(start-protection!))))

(define (install-x509-default user password common-name #!key (days 365))
  ;; TBD: move into policy/trstc...
  ;;
  ;; user: name or #f -> mesh-cert-o
  ;; password: the password used for the new CA.
  (assert (public-oid) "must be initialized")
  (assert (string? common-name))
  (parameterize
   ((*set-child-uid* #f))
   (*set-child-uid* #f)
   (if (not (file-exists? (tc-ca-cert-file)))
       (begin
	 (logerr "Creating CA for ~a@~a\n" user common-name)
	 (tc-make-ca (oid->string (public-oid)) password 3000)))
   (let* ((serial 1)
	  (user (cond
		 ((string? user)
		  (let ((u (entry-name->oid user)))
		    (or (oid? u) (error "user does not exist" user))
		    (oid->string u)))
		 ((oid? user) (oid->string user))
		 (else #f)))
	  (key (begin
		 (logerr "Creating TLS cert request.\n")
		 (tc-genkey #f)))
	  (req (tc-make-certificate-request
		key
		subject: `(("CN" ,common-name)
			   ("L" ,(literal (public-oid)))
			   . ,(if user
				  `(("O" ,(literal user)))
				  '()))
		subjectAltName: common-name
		days: (literal days)))
	  (cert (begin
		  (logerr "Signing TLS cert.\n")
		  (x509-sign
		   req
		   serial
		   (filedata (tc-ca-key-file))
		   password
		   ca: (tc-ca-cert-file) days: days))))
     (tc-store-host-cert! cert)
     (tc-store-host-key! key)
     #t)))


(define (ball-flush-caches!)
  (nu-action-init read-only-plain-transformation transaction-private-plain-transformation!)
  (dsssl-init)
  (pool-init!))

;; TODO move the compacting logic out of the way here.  Module
;; "operation control"?

(define (drop-and-compact-database a b)
  ;; TODO it's a pity that we flush the function caches here.  Fix
  ;; that by pulling the data out of the pstore'd memory in the first
  ;; place (when using them as cache keys).
  (nu-action-init read-only-plain-transformation transaction-private-plain-transformation!)
  (dsssl-init)
  (compact-database a b))

;; FIXME: $pstore-access-strategie doesn't work.  When things get
;; copied into the store, while other places are in commit-frame! we
;; loose references.  A real fix will require close consideration, but
;; it should be feasable.

(define $pstore-access-strategie 'free) ; alternatives: 'late, 'early, 'free

;; badly named, we don't want that here.
(define (report-alive-state . dummy)
  (logerr "\n~a\nGot signal not send signal\n"
          (with-output-to-string (lambda () (thread-list))))
(cond-expand
 (chicken (check-watchdog!))
 (else ))
  (update-system-time! (srfi19:current-time 'time-utc) my-mind-exit))

(define (ball-signal-handler signame)
  (case signame
    ((SIGUSR1) (close-spool-db!) (mind-restart))
    ((SIGUSR2) (report-alive-state))
    (else (my-mind-exit))))

(define (@ parameter) (parameter))

(define *gatekeeper-thread* #f)

(define $storage-adaptors '(fsm))
(define $mind-file-name #f)

;; USAGE RESTRICTION set-working-mind! must never been called without
;; control over the pstore gatekeeper thread.  We need a priority lock
;; when changing the global variables, which is only guaranteed, when
;; gatekeeper holds back all low priority threads.

;; TODO find a way to factor the update of public/private-oid/context
;; out.

(define (set-working-mind! ps default-read default-write)
  (set-working-mind!/lolevel ps default-read default-write)
  ;; Flush the caches, which could have pointers into the persistant
  ;; store, which is just dropped.  Those pointers are not yet
  ;; invalid, but the rscheme pstore for instance will invalidate them
  ;; soon.  See also main.scm:drop-and-compact how we could avoid
  ;; cache flush here.
  (nu-action-init default-read default-write)
  (dsssl-init))

(define-values (startup-completed! startup-completed?) (expectable 'startup))

(define (ball-roll args #!key (offline #f) (init-control-socket #t))
  (define nufsm-base #f)
  (define $protocol-servers
    `(("webpub"
       ,(lambda ()
          (guard
           (e (else (logerr "webpub died ~a\n" e)))
           (if (not (equal? ($http-server-port) 0))
               (http-server ($server-listen-address) ($http-server-port)
                            askemos-satelite 'http-location-format
                            (lambda (message)
                              ((if (public-oid) askemos-web (wake-up askemos-web))
                               message)))))))
#|
      ("getput"
       ,(lambda ()
          (parameterize
	   (($http-magic-post-get #t)
	    (global-proxy-mode #t))
	   (guard
	    (e (else (logerr "getput died ~a\n" e)))
	    (http-server ($server-listen-address) 8245
			 (lambda () #f) 'http-location-format getput)))))
|#
      ("webauth"
       ,(lambda ()
          (guard
           (e (else (logerr "webauth died ~a\n" e)))
           (if (not (equal? ($http-server-port) 0))
               (http-server loopback-address (+ ($http-server-port) 1)
                            (lambda () "http://")
                            'http-location-format
                            askemos-web)))))
      ("authssl"
       ,(lambda ()
          (guard
           (e (else (logerr "authssl died ~a\n" e)))
           (https-server ($server-listen-address) ($https-server-port)
                        `((require . ,($https-server-require-cert))
			  (ca . ,(tc-ca-cert-file))
			  (cert . ,(tc-private-cert-file))
			  (key . ,(tc-private-key-file)))
			#t 'http-location-format
                        askemos-web))))
      ("lmtp"
       ,(lambda () (guard
                    (e (else (logerr "lmtp died ~a\n" e)))
                    (if (not (equal? ($lmtp-server-port) 0))
                        (lmtp-server ($server-listen-address) ($lmtp-server-port)
                                     askemos-web)))))))

  ;; Watch whether we still need this rscheme internal.
  ;; (with-module rs.sys.threads.shell (synchronize-with-finalization))
  ;; Initialization, which is not configuration dependant.
  (init-place-upcalls!)
  (set-function-fetch-other! fetch-other)
  (set-function-upcalls! mind-default-lookup my-oid right->string)
  (set-function-mind-default-lookup! mind-default-lookup)

  (askemos-register-client-bindings!)

  (quorum-lookup
   (lambda (id . init)
     (http-find-quorum (if (pair? init) (car init) *local-quorum*)
		       (lambda (id) (find-already-loaded-frame-by-id id)) id)))
  (forward-call http-forward-call)
  (replicate-call http-replicate-call)
  (replicate-reply http-replicate-reply)
  (echo-call http-echo-call)
  (send-ready http-send-ready)
  (send-one-shot http-send-one-shot)
  (rerequest-state http-get-state)
  (request-reply http-get-reply)
  (metainfo-remote (lambda (q n) (http-get-meta n q)))
  (privilege-info-remote
   (lambda (source quorum local target)
     (!mapfold
      (lambda (voter)
	(let ((addr ((host-lookup) voter)))
	  (if voter
	      (let ((reply (http-get-privileges source addr local target)))
		(parse-capability-statement (message-body reply)))
	      (raise-service-unavailable-condition voter))))
      (lambda (n result i)
	(values n (fold (lambda (e i1) (if (member e i) i1 (cons e i1))) i result)))
      '() quorum
      count: #t timeout: (respond-timeout-interval))))
  (resynchronize http-sync!)
  (invite-new-supporters! http-invite-new-supporters!)
  (message-body-fetch-blob-function http-replicate-blob!)
  (host-lookup askemos-host-lookup)
  (host-alive? http-host-alive?)
  (host-maybe-alive? http-host-maybe-alive?)
  (https-certification-for tc-certification-for)

  (kernel-extent-proxy-forward kernel-extent-proxy-forward-implementation)

  (xsql-user-database-file
   (lambda (identifier)
     (fsm-user-sqlite-file nufsm-base identifier)))

  ;; TODO make the interrupt signal handling portable.  Right now the
  ;; registration is define-macro'd away for chicken, only rscheme uses
  ;; it.  We need to come up with a startup file for each scheme.

  (cond-expand
   (rscheme
    (register-interrupt-handler! 'c-signal ball-signal-handler)
    (setup-c-signal-handler! 'SIGUSR1)
    (setup-c-signal-handler! 'SIGUSR2))
   (chicken
    (set-signal-handler! signal/usr1 (lambda (n) (mind-restart)))
    (set-signal-handler! signal/usr2 report-alive-state))
   (else (begin)))

  (enable-warnings #f)

  ;; Load configuration location, extensions etc.

  (if (and (pair? args)
	   (file-directory? (car args)))
      (let ((abs (if (absolute-pathname? (car args))
		     (car args) (make-absolute-pathname (cwd) (car args)))))
	(spool-directory abs)
	(set! nufsm-base abs)
	(set! args (cdr args))
	(if init-control-socket
	    (ball-control-socket-service-start!
	     abs ball-control-socket-connection-handler
	     serial: offline))
	(check-spool-db!)
	(logstartup 1 "I: running from ~a\n" nufsm-base))
      (begin
	(cond
	 ((null? args)
	  (logerr "E: no repository given.  Exiting.\n")
	  (exit 0))
	 ((file-exists? (car args))
	  (logerr "E: rep '~a' exists, but is not a directory. Exiting\n" (car args))
	  (exit 0))
	 (else
	  (logerr "E: rep '~a' does not exits.  Exiting\n" (car args))
	  (exit 0)))))

  (keydb-open! (make-pathname nufsm-base "key" "db"))

  (ssl-directory (list nufsm-base "ssl"))

  (tc-tofu-ssl-init! (make-pathname nufsm-base "tofu" "sqlite"))

  (xsql-user-database-file
   (lambda (identifier)
     (let ((fn (fsm-user-sqlite-file nufsm-base identifier)))
       (mkdirs (dirname fn))
       fn)))

  (for-each load-or-die args)
  (logstartup 2 "I: loaded ~a\n" args)

  (cond-expand
   (single-binary)
   (else
    (if (and (*set-child-uid*) (not (file-execute-access? (*set-child-uid*))))
        (guard
         (ex (else (logerr "CONFIG ERROR ~a not in path ~a\n"
                           (*set-child-uid*) (get-environment-variable "PATH"))
                   (exit 0)))
         (let ((f (find-in-path "asnobody")))
           (logstartup 2 "I: found annonymous wrapper ~a\n" f)
           (*set-child-uid* f))))))

  ;; configuration dependant setup

  (ball-config-file (make-pathname nufsm-base "config" "xml"))
  (ball-load-local-id-from-config)

  (logstartup 2 "I: local id ~a from config ~a\n" (local-id) (ball-config-file))

  (if (not (local-id)) (local-id "localhost"))

  (http-property-server
   (property 'server (string-append "Askemos/BALL " ball-software-version " " (local-id))))

  (init-http-voters! '())

  ;; Register the storage modules we want.
  (for-each
   (lambda (type)
     (case type
       ((pstore)
	(register-storage-adaptor!
	 local:
	 pstore-storage-adaptor
	 (let ((mind (pstore-open-mind
		      (or $mind-file-name
			  (string-append nufsm-base "pstore"))
		      initial-mind)))
	   (if mind
	       (set-working-mind! (root-object (underlying-pstore mind))
				  read-only-plain-transformation
				  transaction-private-plain-transformation!)
	       (error "Sorry, could not start from ~a"
		      (or $mind-file-name (string-append nufsm-base "pstore"))))
	   mind)))
       ((fsm) (register-storage-adaptor! local: fsm-storage-adaptor nufsm-base))
       (else (error "unknown storage adaptor ~a" type))))
   $storage-adaptors)

  ;; (if (not (memq 'pstore $storage-adaptors))
  ;;     (register-storage-adaptor! local: in-core-storage-adaptor #f))

  (or (and-let* ((cert (tc-private-cert))
		 (public (mesh-cert-l cert)))
		(logstartup 2 "I: ~a private subject ~a\n" (if public "used" "ignored") (mesh-cert-subject->string cert))
		(http-host-add! public (local-id) cert #t)
		(or (oid? public)
		    (local-id public))
		#t)
      (and-let* ((public (public-oid))
		 ((not (eq? public one-oid))))
		(http-host-add! public (local-id) #f #t)))

  (set-local-quorum! (make-quorum
		      (list (if (or (not (public-oid))
				    (eq? (public-oid) one-oid))
				(local-id) (public-oid)))))

  (register-storage-adaptor!
   remote:
   http-storage-adaptor
   (filter (lambda (x)
	     (not (equal? (local-id) x)))
	   (quorum-others  *local-quorum*)))

  ;; Load host configuration.

  ;; Loading config may NOT be moved before initialisation of public-oid!
  (logstartup 2 "I: load config\n")
  (ball-load-config)

#|
  ;; Usually to be left commented out.  Only applicable if known to
  ;; start from a pre-existing repository.
  (cond
   ((not (pair? (public-capabilities)))
    (logerr "No capabilities for public ~a ~a\n" (public-oid) (public-capabilities))
    (exit 1))
   ((not (document (my-oid)))
    (logerr "Private OID not found ~a ~a\n" (my-oid))
    (exit 1))
   )
|#

  (init-xsql-from-config)
  (init-protection $key-ring-type)
  (nu-action-init			; depends on mind
   read-only-plain-transformation
   transaction-private-plain-transformation!)
  ($use-well-known-symbols (not (public-oid)))
  (logstartup 2 "I: load virtual hosts\n")
  (reload-virtual-hosts! eval nufsm-base)
  (init-trustedcode $trustedcode-oid)

  (register-time-procedure! signal-clock)

  (letrec ((g (thread-start!
               (make-thread (make-mind-gatekeeper
                             (lambda (x) #f)
			     set-working-mind!
                             (case (car $storage-adaptors)
                               ((pstore) drop-and-compact-database)
                               (else (lambda (a b) #t)))
                             (or (and (memq 'pstore $storage-adaptors)
                                      $pstore-access-strategie)
                                 (and (memq 'fsm $storage-adaptors)
                                      'free)))
                            "gatekeeper")))
           (c (or offline
		  (let ((x ($control-port))) (or (not x) (eqv? x 0)))
		  (thread-start! (make-thread operation-control "opctrl"))))
           (w (make-watchdog! mind-exit))
           (o (or offline
		  (map (lambda (t) (thread-start! (make-thread (cadr t) (car t))))
		       $protocol-servers))))
    (update-*reverse-of-public-name-space*)
    (logstartup #t "Take off (pid ~a).\n" (current-process-id))
    (startup-completed! #t (current-process-id))
    (set! *gatekeeper-thread* g)
    ;;(for-each thread-join! o)
    g))

(define *oneshot-procs* '())

(define (##dev#run-cmd thunk) (if (procedure? thunk) (set! *oneshot-procs* (cons thunk *oneshot-procs*))))

(import openssl)
(define (ball-dispatch-channel-commands under error args)
  (match
   args
   (("ls")
    (lambda ()
      (assert (my-oid))
      (display "\nChannels:\n")
      (let ((links (fget (find-local-frame-by-id (my-oid) 'ball-kernel-info-handlers:channels) 'mind-links)))
	(for-each (lambda (p) (apply format (current-output-port) "~a\t~a\n" p)) (fold-links-sorted (lambda (k v i) `((,k ,v) . ,i)) '() links)))
      (display "---\n")))
   (((or "link" "-link") name val)
    (lambda ()
      (install-new-private-name!
       name
       (cond
	((equal? val "-") (xml-parse (read-string)))
	((string->oid val) (sxml `(id ,val)))
	((file-exists? val)
	 (logerr "Loading ~a using CoreAPI\n" val)
	 (xml-parse (filedata val)))
	(else (error "file not found" val))))))
   (((or "new" "-new") name action file . more)
    (define content-type (match more (() text/xml) ((x) x)))
    (or (file-exists? file) (error "file not found" file))
    (lambda ()
      (install-new-private-name!
       name
       (make-new-element
	(make-xml-element
	 'output #f
	 (list (make-xml-attribute 'space namespace-xml "preserve")
	       (make-xml-attribute 'method #f "text")
	       (make-xml-attribute 'media-type #f content-type))
	 (filedata file))
	(cons 'action action)
	(cons 'secret "none")))))
   (((or "load" "-load") name file)
    (logerr "Loading ~a as generator script.  (Use with care!)\n" file)
    (lambda () (install-new-private-name! name (load-or-die file))))
   (((or "secret" "-secret") "disable" name) (lambda () (disable-entry-point! name)))
   (((or "secret" "-secret") "set" name . more)
    (let ((sec (match more (("-") (read-line)) ((x) x))))
      (lambda () (disable-entry-point! name sec))))
   (((or "drop" "-drop") name)
    (cond
     ((member name '("public" "system")) (error "not removing essential parts" name))
     (else (lambda () (drop-entry-point! name)))))
   (("auth" (and (or "fixed" "tofu") mode))
    (begin
      ($tc-tofu (equal? mode "tofu"))
      (ball-save-config)))
   (_ (error "wrong command" (cons under args)))))

(define (ball-dispatch-offline-commands error args)
  (define str2via (match-lambda ("query" query-via-entry!) ("send" send-via-entry!)))
  (define ((onport d/w obj) port)
    (cond
     ((xml-element? (document-element obj)) (xml-format-at-output-port obj port))
     (else (d/w obj port))))
  (define (redir out proc)
    (if (equal? out "-")
	(proc (current-output-port))
	(call-with-output-file out proc)))
  (match
   args
   (("load" "-o" out file . files)
    (lambda ()
      (do ((files (cons file files) (cdr files)))
	  ((null? (cdr files))
	   (redir out (onport display (load-or-die (car files)))))
	(load-or-die (car files)))))
   (("load" file ...) (lambda () (for-each load-or-die file)))
   (("via" name kind . more)
    #;(query-via-entry! name #!optional nl #!key (to #t) (content-type "text/xml"))
    (lambda ()
      (let ((call (str2via kind)))
	(match
	 more
	 (() (call name))
	 (("-") (call name (xml-parse (read-string #f (current-input-port)))))
	 ((file) (call name (xml-parse (filedata file))))))))
   (("ps" oid)				; Print Signature
    (lambda ()
      (define frame (and-let* ((oid (string->oid oid)))
                              (or (find-local-frame-by-id oid 'ps) (error "not found" oid))))
      (display (xml-format (frame-signature frame)))))
   (("meta" "-public" oid)
    (lambda ()
      (and-let*
       ((oid (string->oid oid)) (frame (find-local-frame-by-id oid 'pm)))
       (display (xml-format (public-meta-info frame))))))
   (("channel" . more) (ball-dispatch-channel-commands "channel" error more))
   (((or "tofu" "auth-default") cn user pass) ;; pass last: TBD: optional
    (lambda () (install-x509-default user pass cn)))
   (_ (error "offine command not understood" args))))

(define (dispatch-bare-commands args)
  (define ((onport d/w obj) port)
    (cond
     ((xml-element? (document-element obj)) (xml-format-at-output-port obj port))
     (else (d/w obj port))))
  (define (redir out proc)
    (if (equal? out "-")
	(proc (current-output-port))
	(call-with-output-file out proc)))
  (match
   args
   (("load" "-o" out file . files)
    (do ((files (cons file files) (cdr files)))
	((null? (cdr files))
	 (redir out (onport display (load-or-die (car files)))))
      (load-or-die (car files))))
   (("load" . files) (for-each load-or-die files))
   (_ (error "-bare command not understood" args))))

(define (ball-dispatch-command-line args)
  (define (error msg . more) (logerr "Usage error: ~a ~a\n" msg more) (exit 1))
  (match
   args
   (("-init" . more)
    (match
     more
     ((rep precondition . xmore)
      (if (file-exists? rep) (error "file exists" rep))
      (mkdirs rep)
      (change-file-mode rep (+ perm/irusr perm/iwusr perm/ixusr))
      (##dev#run-cmd
       (lambda ()
	 (let ((private (and-let* ((x (member "-private" more))) (cadr x)))
	       (patch (and-let* ((x (member "-patch" more))) (cadr x))))
	   (install-initial-contract
	    precondition private: private
	    patch-in-zero-state: (cond ((equal? patch "0") #f) ((equal? patch "1") #t) (else 'except-replicates))))
	 (mind-exit)))
      (ball-roll (list rep) offline: #t))
     (_ (error "usage: -init representative precondition"))))
   (("-bare" . more)
    (dispatch-bare-commands more)
    (exit 0))
   (("-offline" dir . more)
    (or (file-exists? dir) (error "no directory" dir))
    (or (ball-control-socket-free dir)
	(error "-offline rep already running" dir))
    (##dev#run-cmd (ball-dispatch-offline-commands error more))
    (ball-roll (list dir) offline: #t))
   (("-online" dir . more)
    (ball-handle-online-connection
     (or (ball-control-socket-connect dir)
	 (error "-online socket connection failed" dir))
     more)
    (##dev#run-cmd (lambda () (exit 0)))
    (exit 0))
   (("-secret" . more) (error "reserved"))
   (("-" . _) (error "reading from stdin pending implementation"))
   (((? (lambda (x) (string-prefix? "-" x)) key) . _)
    (error "unknown command line option" key))
   (args (ball-roll args))))

(define ball-control-socket-command
  (match-lambda
   (("connect" url) (askemos-connect url))
   (("support" from name . remote)
    (match
     remote
     (() (frame? (support-entry-point! from name)))
     ((remote-name . ignored) (frame? (support-entry-point! from name remote-name)))))
   (("stop") (mind-exit) 'exiting)
   (("stop" "-f" . more) (exit (if (pair? more) 1 0)))
   (("via" name kind . more)
    ;;(query-via-entry! name #!optional nl #!key (to #t) (content-type "text/xml"))
    (let ((call (match kind ("query" query-via-entry!) ("send" send-via-entry!))))
      (message-body/plain
       (match
	more
	(() (call name))
	((data) (call name data)) ;; TBD: remove this case and the calling site.
	((data content-type accept destination)
	 (call name data content-type: content-type accept: accept to: (or (eq? destination #t) (parsed-locator destination))))))))
   (("print" prop)
    (match
     prop
     ("public" (and-let* ((oid (public-oid)) (f (find-local-frame-by-id oid 'print)))
			 (aggregate-entity f)))
     ("onion-route"
      (or (and-let*
	   ((id ($external-address)) (socks ($https-use-socks4a)) ((socks id)))
	   (let ((port ($external-port)))
	     (cond
	      ((not port) #f)
	      ((equal? port 443) (format "~a://~a" "https" id))
	      (else (format "~a://~a:~a" "https" id port)))))
	  ""))
     ("pid" (current-process-id))))
   (("channel" . more) (ball-dispatch-channel-commands "channel" error more))
   ((begin . exprs)
    (let ((results '()))
      (do ((exprs exprs (cdr exprs)))
	  ((null? exprs) (apply values results))
	(set! results (receive (eval (car exprs)))))))
   (other (error "ball-control-socket unknown command" other))))

(import srfi-106)
(define (ball-handle-online-connection socket command)
  ;; The client side.
  (define (error msg)
    (display msg (current-error-port)) (newline (current-error-port))
    (exit 1))
  (define (parse-request-options options)
    (let ((ct text/xml)
	  (accept application/rdf+xml)
	  (destination #t))
      (let loop ((options options))
	(match
	 options
	 (((or "-content-type" "-ct") val . more) (begin (set! ct val) (loop more)))
	 (("-accept" val . more) (begin (set! accept val) (loop more)))
	 (((or "-destination" "-d") val . more) (begin (set! destination val) (loop more)))
	 (() (list ct accept destination))))))
  (let ((port (socket-output-port socket)))
    (write
     (match
      command
      (("via" name kind . more)
       (match
	more
	(() `("via" ,name ,kind))
	(("-meta") `("via" ,name ,kind ,(xml-format metainfo-request-element)))
	(("-in" file . more)
	 (let ((literal (if (equal? file "-") (read-string) (filedata file))))
	   `("via" ,name ,kind ,literal . ,(parse-request-options more))))
	(("-literal" literal . more) `("via" ,name ,kind ,literal . ,(parse-request-options more)))
	((file) ;; TBD: remove this case and the called site.
	 (display "deprecated syntax, use '-in'\n" (current-error-port))
	 `("via" ,name ,kind ,(filedata file)))
	(_ (cond
	    (else (error "invalid \"via\" command syntax"))))))
      (_ command))
     port))
  (match
   (read (socket-input-port socket))
   (('D . more)
    (match
     more
     (other (display other) (newline))))
   (('E msg) (error msg))
   ((? eof-object?) (error "connection terminated")))
  (close-output-port (socket-output-port socket)))

(define (ball-control-socket-connection-handler s)
  (let ((in (socket-input-port s)) (out (socket-output-port s)))
    (guard
     (ex (else (log-condition "control socket connection handler" ex)))
     (force startup-completed?)         ;; sync with startup
     (do ((expr (read in) (read in)))
	 ((eof-object? expr))
       (write
	(guard
	 (ex (else `(E ,(literal ex))))
	 `(D . ,(receive (ball-control-socket-command expr))))
	out)))
    (close-input-port in)
    (close-output-port out)))

(cond-expand
 (chicken
  ;; FIXME: Chicken may spuriously wake up from thread-join!
  (define (primordial-thread-join! t)
    (let loop ()
      (let ((r (handle-exceptions
                ex (begin
                     (log-condition "Primordial thread join exception" ex)
                     ex)
                (thread-join! t))))
        (logerr "CHICKEN primordial thread woke up from thread-join! with ~a\n" r)
        (case (thread-state t)
          ((dead terminated) r)
          (else
           (logerr "CHICKEN primordial thread woke up from thread-join! to handle interrupt waiting again on ~a which is in state ~a\n" t (thread-state t))
           (loop)))))))
 (else (define primordial-thread-join! thread-join!)))

(define (main1 args)
  (let ((t (ball-dispatch-command-line args)))
    (if (pair? *oneshot-procs*)
	(begin
	  (for-each (lambda (f) (if (procedure? f) (f) (force f))) (reverse *oneshot-procs*))
	  (mind-exit))
	(if (null? args)
	    (error "Usage: -command ... OR (backward compatible) dir file ...")))
    (primordial-thread-join! t))
  (exit 0))

(define main2
  (let ((kernel #f)
	(ctrl #f))
    (define (start-ctrl! dir)
      (if (not ctrl)
	  (set! ctrl (ball-control-socket-service-start!
		      dir ball-control-socket-connection-handler
		      serial: #f))))
    (define (doit error args)
      (define (loop args)
	(match
	 args
	 (("-init" . more)
	  (match
	   more
	   ((rep precondition . xmore)
	    (if (file-exists? rep) (error "file exists" rep))
	    (mkdirs rep)
	    (change-file-mode rep (+ perm/irusr perm/iwusr perm/ixusr))
	    (set! kernel (ball-roll (list rep) offline: #t init-control-socket: #f))
	    (let ((private #f)
		  (patch #f))
	      (install-initial-contract
	       precondition private: private
	       patch-in-zero-state: (cond ((equal? patch "0") #f) ((equal? patch "1") #t) (else 'except-replicates))))
	    #;(start-ctrl! rep)
	    (loop xmore))
	   (_ (error "usage: -init representative precondition"))))
	 (("-start" rep)
	  (if kernel (thread-terminate! kernel))
	  (start-ctrl! rep)
	  (set! kernel (ball-roll (list rep) offline: #f init-control-socket: #f))
	  (primordial-thread-join! kernel))
	 (("load" "-o" out in . more)
	  ((ball-dispatch-offline-commands error (list "load" "-o" out in)))
	  (loop more))
	 (("channel" "-link" nm v . more)
	  ((ball-dispatch-channel-commands "channel" error (list "-link" nm v)))
	  (loop more))
	 (("channel" "secret" "set" nm pw . more)
	  ((ball-dispatch-channel-commands "channel" error (list "secret" "set" nm pw)))
	  (loop more))
	 (((or "tofu" "auth-default") cn user pass . more)
	  (install-x509-default user pass cn)
	  (loop more))
	 (() #t)
	 (_ (main1 args) #;(error "unhandled arguments" args))))
      (loop args))
    (lambda (args)
      (call-with-current-continuation
       (lambda (exit)
	 (define (error msg . more) (logerr "Usage error: ~a ~a\n" msg more) (exit 1))
	 (doit error args)
	 (exit 0))))))

(define main main2)
