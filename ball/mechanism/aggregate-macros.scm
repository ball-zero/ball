;; (C) 2008 Jörg F. Wittenberger see http://www.askemos.org

;;* Properties

(cond-expand
 (use-record+llrbtree-as-frame
  )
 (else
  (define-macro (property name value) `(cons ,name ,value))
  (define-macro (property? x) `(pair? ,x))
  (define-macro (property-name p) `(car ,p))
  (define-macro (property-value p) `(cdr ,p))
  ))

;;* Aggregates

(define-macro (aggregate? x) `(pair? ,x))
(define-macro (aggregate meta data)  `(cons ,meta ,data))

(cond-expand
 (never
  (: aggregate-meta (:place: -> :message:))
  (define (aggregate-meta x) (car x))
  (: aggregate-entity (:place: -> :oid:))
  (define (aggregate-entity x) (cdr x))
  )
 (else
  (define-macro (aggregate-meta aggregate) `(car ,aggregate))
  (define-macro (aggregate-entity aggregate) `(cdr ,aggregate))
  ))

(define-macro (set-aggregate-entity! aggregate value) `(set-cdr! ,aggregate ,value))
(define-macro (set-aggregate-meta! aggregate value) `(set-car! ,aggregate ,value))

(define-macro (aggregate-associate aggreg property)
  `(aggregate (properties-set (aggregate-meta ,aggreg) ,property)
              (aggregate-entity ,aggreg)))

(define-macro (set-slot! frame slot value)
  (cond
   ((equal? slot ''version) `(set-frame-version! ,frame ,value))
   ((equal? slot ''reply-channel) `(set-frame-reply-channel! ,frame ,value))
   ((equal? slot ''replicates) `(set-frame-replicates! ,frame ,value))
   ((equal? slot ''mind-links) `(set-frame-links! ,frame ,value))
   ((equal? slot ''content-type) `(set-frame-content-type! ,frame ,value))
   ((equal? slot ''mind-body) `(set-frame-body/plain! ,frame ,value))
   ((equal? slot ''body/parsed-xml) `(set-frame-body/xml! ,frame ,value))
   ((equal? slot ''protection) `(set-frame-protection! ,frame ,value))
   ((equal? slot ''capabilities) `(set-frame-capabilities! ,frame ,value))
   ((and (pair? slot) (eq? (car slot) 'quote) (symbol? (cadr slot)))
    `(set-frame-other!
      ,frame
      (let ((value ,value))
	(if value
	    (%replace-association (property ,slot value) (frame-other ,frame))
	    (%remove-association ,slot (frame-other ,frame))))))
   (else `(%set-slot! ,frame ,slot ,value))))

(define-macro (get-slot frame slot)
  (cond
   ((equal? slot ''version) `(frame-version ,frame))
   ((equal? slot ''reply-channel) `(frame-reply-channel ,frame))
   ((equal? slot ''replicates) `(frame-replicates ,frame))
   ((equal? slot ''mind-links) `(frame-links ,frame))
   ((equal? slot ''content-type) (list 'frame-content-type frame))
   ((equal? slot ''mind-body) `(frame-body/plain ,frame))
   ((equal? slot ''body/parsed-xml) `(frame-body/xml ,frame))
   ((equal? slot ''protection) `(frame-protection ,frame))
   ((equal? slot ''capabilities) `(frame-capabilities ,frame))
   ((and (pair? slot) (eq? (car slot) 'quote) (symbol? (cadr slot)))
    `(let ((v (properties-get ,slot (frame-other ,frame))))
       (and v (property-value v))))
   (else `(%get-slot ,frame ,slot))))

;; new-frame is internal to the "place" module.  It's there only to
;; force a compile time syntax check.

(cond-expand

((or use-record+llrbtree-as-frame use-vector+assoc-as-frame)

(define-macro
  (new-frame
   version
   replicates
   reply-channel
   links
   content-type
   body/plain
   body/xml
   protection
   capabilities
   other)
  (list 'allocate-frame
        version
        replicates
        reply-channel
        links
        content-type
        body/plain
        body/xml
	protection
	capabilities
        other))

)(use-env-as-frame

(define-macro
  (new-frame
   version
   replicates
   reply-channel
   links
   content-type
   body/plain
   body/xml
   protection
   capabilities
   other)
  (let ((envt (gensym)))
    `(let ((,envt (make-environment)))
       (environment-extend! ,envt 'version ,version)
       (environment-extend! ,envt 'replicates ,replicates)
       (environment-extend! ,envt 'reply-channel ,reply-channel)
       (environment-extend! ,envt 'links ,links)
       (environment-extend! ,envt 'content-type ,content-type)
       (environment-extend! ,envt 'body/plain ,body/plain)
       (environment-extend! ,envt 'body/xml ,body/xml)
       (environment-extend! ,envt 'protection ,protection)
       (environment-extend! ,envt 'capabilities ,capabilities)
       (for-each (lambda (p)
                   (environment-extend! ,envt (car p) (cdr p)))
                 ,other)
       ,envt)))

)(use-hashtable-as-frame

(define-macro
  (new-frame
   version
   replicates
   reply-channel
   links
   content-type
   body/plain
   body/xml
   protection
   capabilities
   other)
  (let ((envt (gensym)))
    `(let ((,envt (make-symbol-table)))
       (hash-table-set! ,envt 'version ,version)
       (hash-table-set! ,envt 'reply-channel ,reply-channel)
       (hash-table-set! ,envt 'replicates ,replicates)
       (hash-table-set! ,envt 'links ,links)
       (hash-table-set! ,envt 'content-type ,content-type)
       (hash-table-set! ,envt 'body/plain ,body/plain)
       (hash-table-set! ,envt 'body/xml ,body/xml)
       (hash-table-set! ,envt 'protection ,protection)
       (hash-table-set! ,envt 'capabilities ,capabilities)
       (for-each (lambda (p)
                   (hash-table-set! ,envt (car p) (cdr p)))
                 ,other)
       ,envt)))

)(else no-frame-representation-selected))

;;FIXME: FramerD emulation code, phase it out!  It's getting ugly.

(cond-expand
 (none ;(or chicken hygienic)
  (define-syntax fget
    (syntax-rules ()
      ((fget frame slot)
       (get-slot (aggregate-meta frame) slot)))))
 (else
  (define-macro (fget frame slot)
    `(get-slot (aggregate-meta ,frame) ,slot))))

;; I see two principle ways to implement updates for frames.  The side
;; effect way of life, which requires either unclassified locks for
;; both read and write operations or write priority locks.  Both are a
;; lot of hassle and cost time.

;; The second way is to have side effect free updates.  Read doesn't
;; need any locks.  But write operations suffer from wasting memory,
;; which is especially bad, if there is a persistent store.

;; Having the latter is a second argument for a garbage collection
;; mechanism, which doesn't simply commit the frame into a new store,
;; but copies it.

;; for demo purpose the ancient side effect code.  Maybe we reverse
;; the decision what to do...

;(define (fzap! frame slot value)
;  (let loop ((p frame))
;       (if (null? (cdr p))
;           #t
;           (let ((next (cdr p)))
;             (if (eq? (caar next) slot)
;                 (if (equal? value (cdar next))
;                     (set-cdr! p (cdr next)))
;                 (loop next))))))

;(define (fset! frame slot value)
;  (let loop ((p frame))
;       (if (null? (cdr p))
;           (if (not (eq? value #f)) (set-cdr! p `((,slot . ,value))))
;           (let ((next (cdr p)))
;             (if (eq? (caar next) slot)
;                 (if (eq? value #f)
;                     (set-cdr! p (cdr next))
;                     (set-cdr! (car next) value))
;                 (loop next))))))

(cond-expand
 ((or chicken hygienic)
  (define-syntax fset!
    (syntax-rules ()
      ((fset! frame slot value)
       (begin (set-slot! (aggregate-meta frame) slot value)
	      frame)))))
 (else
  (define-macro (fset! frame slot value)
    `(begin (set-slot! (aggregate-meta ,frame) ,slot ,value)
	    ,frame))))
