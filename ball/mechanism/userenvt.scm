(define (dsssl-timestamp)
  (srfi19:date->string (timestamp) "~a, ~d ~b ~Y ~H:~M:~S ~Z"))

(define (dsssl-error msg . args)
  (raise
   (cond
    ((string? msg)
     (make-condition
      &http-effective-condition 'status 400
      'message (apply format msg args)))
    ((node-list? msg)
     (make-condition
      &http-effective-condition
      'status (or (and-let* ((k (memq status: args))) (cadr k))
		  400)
      'message msg))
    (else msg))))

(define (dsssl-pcre regex)
  (letrec ((matcher (pcre->proc regex))
           (exec (lambda (obj start)
                   (cond
                    ((string? obj) (matcher obj start))
                    ((xml-element? obj) (exec (%children obj) start))
                    ((or (node-list-empty? obj)
                         (xml-pi? obj)
                         (xml-comment? obj)) #f)
                    ((node-list? obj)
                     (or (exec (node-list-first obj) start)
                         (exec (node-list-rest obj) start)))
                    (else (matcher (format #f "~a" obj) start))))))
    (lambda (obj . rest)
      (if (pair? rest) (apply exec obj rest) (exec obj 0)))))

(define (dsssl-make-load eval)
  (lambda (obj)
    (let ((listing
	   (cond
	    ((input-port? obj) (sweet-read-all obj))
	    ((string? obj) (call-with-input-string obj sweet-read-all))
	    (else (call-with-input-string (data obj) sweet-read-all)))))
      ((eval `(lambda (fetch) . ,listing))
       (lambda (dst . args)
	 (let ((ans (apply fetch-other (current-place) dst args)))
	   (if (node-list? ans)
	       (document-element ans)
	       ans)))))))

(cond-expand
 (chicken
  (define-syntax iconv-buflen (syntax-rules () ((_) 60))))
 (else
  (define-macro (iconv-buflen) 256)))

(define (iconv to from str)
  (let ((to (or to "UTF-8"))
        (from (or from "UTF-8"))
	(str (if (a:blob? str)
		 (fetch-blob-notation *the-registered-stores* str #f)
		 str)))
    (if (or (equal? to from)
            (and (member to '("UTF-8" "UTF8" "utf8" "utf-8"))
                 (member from '("UTF-8" "UTF8" "utf8" "utf-8"))))
        str
        (let ((cnv (or (iconv-open (if (string? to) to
				       (error "iconv ~a not a string" to))
				   (if (string? from) from
				       (error "iconv ~a not a string" from)))
		       (error "no converstion from ~a to ~a" from to)))
              (buf (make-string/uninit (iconv-buflen))))
          ;;
          (call-with-output-string
	   (lambda (o)
	     (let loop ((i 0)
			(n (string-length str)))

	       (cond-expand
		(chicken (chicken-check-interrupts/rarely!))
		(else ))

	       (if (eqv? n 0)
		   (iconv-close cnv)
		   (receive (nx sn dn)
			    (iconv-bytes cnv str i n buf 0 (iconv-buflen))
			    (call-write-bytes o buf 0 (- (iconv-buflen) dn))
			    (loop (+ i (- n sn)) sn))))))))))

(define (dsssl-normalise obj)
  (let ((obj (cond
	      ((node-list? obj) (data obj))
	      (else
	       ;; (body->string obj)
	       (raise 'normalise-does-not-work-on-blobs)))))
    (normalise-data obj #f #f "\n")))

(define (qrencode str . rest)
  (or (equal? rest '(inline))
      (error "Usage: qrencode <str> 'inline"))
  (string-append
   "data:image/png;base64,"
   (pem-encode
    #;(run-child-process
     (lambda (to) (display str to))
     (lambda (from) (read-string #f from))
     (list (find-in-path "qrencode") "-lQ" "-o-")
     #f)
    (receive
     (pixels size)
     (qrcodegen-string-8bit str 7 'high)
     (with-output-to-string
      (lambda () (write-png pixels size size 1 flip: #f compression: 8 stride: 0 filter: #f)))))))

(define (dsssl-make-locator orig)
  (define (full-locator format location . keywords)
    (if (pair? keywords)
	(if (and (eq? (car keywords) body:)
		 (pair? (cdr keywords)))
	    (orig format `(body: , (cadr keywords) . ,location))
	    (error "syntax error in ~a" keywords))
	(orig format location)))
  (lambda args
    (cond
     ((null? args)
      (let ((msg (current-message)))
	(full-locator (msg 'location-format) (append (reverse (msg 'destination)) (msg 'location)))))
     ((symbol? (car args))
      (case (car args)
	((body:)
	 (let ((msg (current-message)))
	   (apply 
	    full-locator
	    (msg 'location-format) (append (reverse (msg 'destination)) (msg 'location))
	    args)))
	(else (if (location-format? (car args))
		  (apply full-locator args)
		  (apply full-locator ((current-message) 'location-format) args)))))
     ((or (string? (car args)) (xml-element? (document-element (car args))))
      (let ((msg (current-message)))
	(full-locator
	 (msg 'location-format) (append (reverse (msg 'destination)) (msg 'location))
	 body: (car args))))
     (else (apply full-locator ((current-message) 'location-format) args)))))

(define dsssl-read-locator (dsssl-make-locator read-locator))
(define dsssl-write-locator (dsssl-make-locator write-locator))

(define dsssl-kernel-exception?
  (lambda (obj)
    (or
     (timeout-condition? obj)
     (missing-blob-condition? obj))))

(define dsssl-sxml
  (let ((basic-sxml sxml))
    (define (sxml body)
      (guard (ex ((dsssl-kernel-exception? ex) (raise ex))
		 ((message-condition? ex) (raise ex))
		 (else (raise (format "sxml error ~a in expression\n~s" ex body))))
	     (basic-sxml body)))
    sxml))

(define dsssl-with-exception-handler
    (let ((o with-exception-handler))
      (lambda (handler thunk)
	(let ((old-h (current-exception-handler)))
	  (o (lambda (ex)
	       (set! ex (do ((ex ex (uncaught-exception-reason ex)))
			    ((not (uncaught-exception? ex)) ex)))
	       (if (dsssl-kernel-exception? ex)
		   (old-h ex)
		   (handler ex)))
	     thunk)))))

(define (dsssl-x509-text t)
  (if (equal? t "")
      t
      (guard
       (ex (else t))
       (guard
	(ex (else (tc-request-text t)))
	(or (x509-text t) (tc-request-text t))))))

(cond-expand
 ((or chicken)
  (define-inline (dsssl-sql-ref/checked x . rest)
    (let* ((rp (and (pair? rest) rest))
	   (rv (and rp (car rp)))
	   (cp (and (pair? rp) (cdr rp)))
	   (cv (and cp (car cp))))
      (if cp (sql-ref x rv cv)
	  (raise
	   (make-condition
	    &message 'message
	    (format "sql-ref called with ~a indixes requiried 2"
		    (or (and rp 2) 1))))))))
 (else
  (define dsssl-sql-ref/checked sql-ref)))

(define (dsssl-sql-ref x . rest)
  (define (sql-re/1-1 result)
    (if (eqv? (sql-ref result #f #f) 1)
	(sql-ref result 0 0)
	(raise
	 (make-condition
	  &message 'message
	  (format "sql-ref: Expected exactly one result but got ~a from\n~a"
		  (sql-ref result #f #f) (sql-write x))))))
  (if (sql-result? x)
      (if (null? rest)
	  (sql-re/1-1 x)
	  (apply dsssl-sql-ref/checked x rest))
      (let ((result ((current-place) x 'sql-query)))
	(if (null? rest)
	    (sql-re/1-1 result)
	    (apply dsssl-sql-ref/checked result rest)))))

;; sql-ref/default is "kind of" deprecated right from the start.  But
;; it's so handsome when prototyping, that it will stay for some time.
;; However: don't rely on it's internals.  It will just never raise an
;; exception (and thus might hide real problems) but return default
;; instead.

;; TODO: RENAME sql-ref/default to sql-ref/guard before it's actually used!

(define (dsssl-sql-ref/default default x . rest)
  (if (sql-result? x)
      (guard
       (ex (else default))
       (apply sql-ref x rest))
      (let ((result ((current-place) x 'sql-query)))
	(if (null? rest)
	    (if (eqv? (sql-ref result #f #f) 1)
		(or (sql-ref result 0 0) default)
		default)
	    (guard
	     (ex (else default))
	     (apply sql-ref x rest))))))

(define (dsssl-sql-query x . args)
  (if (null? args)
      ((current-place) x 'sql-query)
      ((current-place) (vector x args) 'sql-query)))
(define (dsssl-sql-fold sqlv kons nil)
  (let ((sqlv (if (sql-result? sqlv) sqlv (dsssl-sql-query sqlv))))
    (if (sql-result? sqlv)
	(sql-fold sqlv kons nil)
	(error (format "sql-fold not a sql result ~a" sqlv)))))

(define-syntax define-msg-accessor
  (syntax-rules ()
    ((_ name key)
     (define (name msg)
       (cond
	((procedure? msg) (msg 'key))
	((frame? msg) (get-slot msg 'key))
	(else (raise (format "'~a' does not understand ~a" 'name msg))))))))

(define-msg-accessor dsssl-message-creator dc-creator)
(define-msg-accessor dsssl-message-date dc-date)
(define-msg-accessor dsssl-message-last-modified last-modified)
(define-msg-accessor dsssl-message-location location)
(define-msg-accessor dsssl-message-destination destination)
(define-msg-accessor dsssl-message-caller caller)

(define (%change-replicates replicates nli left)
  (make-xml-element
   (gi replicates) (ns replicates) (xml-element-attributes replicates)
   (make-xml-element 'Bag namespace-rdf '()
		     (node-list nli left))))

(define (replicates-toggle replicates nl)
  (let* ((data-trimed (lambda (x)
			(cond
			 ((oid? x) (oid->string x))
			 (else (string-trim-both (data x))))))
	 (li ((sxpath '(Bag li)) replicates))
	 (rdf namespace-rdf))
    (receive
     (deleteable insertable)
     (partition (lambda (name)
		  (ormap (lambda (li)
			   (equal? (attribute-string 'resource li) name))
			 li))
		(if (oid? nl) (list (oid->string nl))
		    (map data-trimed nl)))
     (let ((nli (fold
                 (lambda (name init)
                   (if (equal? name "") init
                       (cons (make-xml-element
			      'li rdf
			      (list (make-xml-attribute 'resource rdf name))
			      (empty-node-list))
			     init)))
                 (empty-node-list)
                 insertable))
	   (left (node-list-filter
                  (lambda (li) (not (member (attribute-string 'resource li) deleteable)))
		  li)))
       (if (and (node-list-empty? nli) (node-list-empty? left))
           (error "cowardly not removing the last supporter")
	   (%change-replicates replicates nli left))))))

(define (dsssl-message-replicates msg . kwds)
  (define (identity x) x)
  (define (arg x)
    (cond
     ((oid? x) x)
     ((and (string? x) (oid-parse x)) => identity)
     ((and-let* ((e (document-element x))) (oid-parse (data x))) => identity)
     (else (error (format "replicates: illegal spec ~a" x)))))
  (let ((val (cond
	       ((procedure? msg) (msg 'replicates))
	       ((frame? msg) (get-slot msg 'replicates))
	       ((quorum? msg) msg)
	       ((arg msg) => (lambda (msg)
			       (replicates-of
				(or (find-frame-by-id/quorum msg ((current-place) 'replicates))
				    (raise (make-object-not-available-condition
					    msg #f 'message-replicates))))))
	       (else (raise 'dsssl-replicates:NYI)))))
    (and
     val
     (let loop ((val val) (as quorum-xml) (args kwds))
       (if (null? args) (as val)
	   (case (car args)
	     ((as:)
	      (loop
	       val
	       (case (cadr args)
		 ((text/xml application/xml) quorum-xml)
		 ((#t) identity)
		 (else quorum-xml))
	       (cddr args)))
	     ((add:)
	      (loop
	       (let ((n (arg (cadr args))))
		 (if (or (member n (quorum-others val))
			 (and (quorum-local? val) (eq? n (public-oid))))
		     val
		     (let ((x (quorum-xml val)))
		       (make-quorum
			(%change-replicates
			 x
			 (make-xml-element
			  'li namespace-rdf
			  (list (make-xml-attribute 'resource namespace-rdf (oid->string n)))
			  (empty-node-list))
			 ((sxpath '(Bag li)) x))))))
	       as (cddr args)))
	     ((sub:)
	      (loop
	       (let ((n (arg (cadr args))))
		 (if (or (member n (quorum-others val))
			 (and (quorum-local? val) (eq? n (public-oid))))
		     (let ((x (quorum-xml val))
			   (d (oid->string n)))
		       (make-quorum
			(%change-replicates
			 x
			 (empty-node-list)
			 (filter
			  (lambda (li) (not (equal? (attribute-string 'resource li) d)))
			  ((sxpath '(Bag li)) x)))))
		     val))
	       as (cddr args)))
	     ((toggle:)
	      (loop (make-quorum (replicates-toggle (quorum-xml val) (cadr args)))
		    as (cddr args)))
	     (else (error (format "message-replicates ~a" args)))))))))

(define (sybil? a b)
  (let ((g.a (dsssl-message-replicates a as: #t))
	(g.b (dsssl-message-replicates b as: #t)))
    (let ((common
	   ;; (fold
	   ;;  (lambda (e i)
	   ;;    (if (member e (quorum-others g.a)) (add1 i) i))
	   ;;  (if (and (quorum-local? g.b) (quorum-local? g.b)) 1 0)
	   ;;  (quorum-others g.b))
	   (let loop ((i (if (and (quorum-local? g.b) (quorum-local? g.b)) 1 0))
		      (l (quorum-others g.b)))
	     (if (null? l) i
		 (loop (if (member (car l) (quorum-others g.a)) (+ i 1) i) (cdr l)))))
	  (n.a (quorum-size g.a))
	  (n.b (quorum-size g.b)))
      (or (> (- n.a common) (quotient n.a 2))
	  (> (- n.b common) (quotient n.b 2))))))

(define (replicates-join a b)
  (let ((g.a (dsssl-message-replicates a as: #t))
	(g.b (dsssl-message-replicates b as: #t)))
    (let ((ax (quorum-xml g.a)))
      (let ((seen (if (quorum-local? g.a)
		      (cons (public-oid) (quorum-others g.a))
		      (quorum-others g.a))))
      (dsssl-message-replicates
       g.a toggle:
       (filter
	(lambda (li) (not (member (attribute-string 'resource li) seen)))
	((sxpath '(Bag li)) (quorum-xml g.b))))))))

(define (dsssl-message-action obj)
  (cond
   ((procedure? obj) (obj 'mind-action-document))
   ((frame? obj) (get-slot obj 'mind-action-document))
   ((oid? obj) (secure-action-document obj))
   (else (raise (format "action-document illegal argument ~a" obj)))))

(define (dsssl-message-potentialities msg) (msg 'potentialities))

(define (dsssl-current-contract)
  ((current-place) 'mind-action-document))

(define (dsssl-source-contract oid)
  (action-document
   (fget (or (find-frame-by-id/quorum oid ((current-place) 'replicates))
	     (raise (make-object-not-available-condition
		     oid #f 'action-document)))
	 'dc-creator)))

(define (dsssl-link-ref name)
  (if (string? name)
      ((current-place) name)
      (raise (format "d:link-ref not a string ~a" name))))

(define (dsssl-fold-links kons init)
  (((current-place) 'fold-links) kons init))

(define (dsssl-fold-links/ascending kons init)
  (((current-place) 'fold-links-ascending) kons init))

(define (dsssl-message-identifier me) (me 'get 'id))
(define (dsssl-self-identifier) ((current-place) 'get 'id))

(define (dsssl-self-affirm-secret value) ((current-place) value 'affirm))

(define (dsssl-secret-encode value . salt)
  (secret-encode
   value
   (if (pair? salt) (car salt)
       (let* ((p (current-place))
	      (q (p 'replicates)))
	 (if (eqv? (quorum-size q) 1)
	     (md5-digest (literal (random 1000000)))
	     (cdr (p 'version))))))) ;; should be version-chks, not cdr

(define (anon-secret)
  (let ((msg (current-message)))
    (and (or
	  (eq? (dsssl-message-creator msg) one-oid)
	  (eq? (dsssl-message-creator msg) (public-oid)))
	 (msg 'secret))))

(define (dsssl-metainfo path)
  (cond
   ((eq? path #t)
    (let ((me (current-place)))
      (me (me 'get 'id) 'metainfo)))
   (else ((current-place) path 'metainfo))))

(define (dsssl-metainfo/adopt path)
  ((current-place) path 'metainfo-adopt))

(define (bail-current-sqlite-db)
  ((current-place) 'sqlite3))

(define (dsssl-apply-xslt-stylesheet template node)
  ((make-xslt-transformer (current-place) (current-message))
   node template))

(define (bail-current-message)
  (current-message))

(define (bail-current-place)
  (current-place))

(define (bail-form-field key . base)
  (form-field key (if (pair? base) (car base) (dsssl-message-body (current-message)))))

(define askemos-client-bindings
  '(
    (utf8-string-length string-length)
    (utf8-string-ref string-ref)
    (call-with-utf8-input-string call-with-input-string)
    (call-with-utf8-output-string call-with-output-string)
    (utf8-substring substring)
    ((lambda args (apply clformat #f args)) format)
    ;; FIXME: Don't export these anymore.  Move to SRFI compliance.
    iconv content-type-charset
    (mime-converter mime-cast)
    xml-element?
    ;;
    string-split
    (utf8-levenshtein-distance levenshtein-distance)
    (utf8-levenshtein< levenshtein<)
    ;; pcre is experimental, there should be a mechanism for private exports
    (dsssl-pcre pcre)
    (dsssl-error error)
    ((dsssl-make-load eval-sane) load)
    ; *shift *reset ;; experimental; these should be done diffeerent
    ; make-zipper zipper? zipper-cursor zipper-next
    raise-access-denied-condition
    ;; regex-case commented out: i's not yet available in the chicken version.
    ;; space and access control.
    acquire identity
    public-oid public-context user-context
    right->node-list node-list->right
    right->string string->right
    ;; The action-document indicates the type of the object.  As
    ;; explained elsewhere it's common sense to expect certain
    ;; behavior based on the type of the receipient of a message.
    ;; Actors might even want to abstain from interaction with certain
    ;; types at all.  Therefore we need to provide at least this
    ;; information.
    (dsssl-message-action action-document)
    (dsssl-current-contract current-contract)
    (dsssl-source-contract source-contract)
    (bail-current-message current-message)
    (bail-current-place current-place)
    (bail-current-sqlite-db current-sqlite)
    ;; I'd like to remove my-oid here. Questionable: It's the symbol
    ;; for the most valuable entity, that is also the most restrictive
    ;; right.  We would need a proxy right instead.  This could be
    ;; implemented as a well know document.  But that would be another
    ;; indirection and I'm not sure that we want that.
    (dsssl-message-identifier message-identifier)

    (dsssl-self-identifier self-reference)
    (dsssl-self-affirm-secret self-affirm-secret)
    anon-secret
    ;;(dsssl-self-identifier self) maybe?

    version-identifier

    (dsssl-message-replicates message-replicates)
    sybil?
    replicates-join
    ;;
    my-oid
    ;;
    entry-name->oid entry-name
    debug
    ;;
    (frame? message?) make-message
    (dsssl-message-body message-body)
    (dsssl-message-body/plain message-body/plain)
    (dsssl-body-size body-size)
    message-content-type
    message-protection message-capabilities
    (dsssl-message-potentialities message-potentialities)
    (dsssl-message-creator message-creator)
    (dsssl-message-date message-date)
    (dsssl-message-last-modified message-last-modified)
    (dsssl-message-location message-location)
    (dsssl-message-destination message-destination)
    (dsssl-message-caller message-caller)
    (cons property)
    ;;
    (dsssl-link-ref link-ref)
    (dsssl-fold-links fold-links)
    (dsssl-fold-links/ascending fold-links/ascending)
    ;;
    public-place
    (oid+quorum->string oid->string)
    (oid-parse string->oid)
    parsed-locator
    (dsssl-read-locator read-locator)
    (dsssl-write-locator write-locator)
    (dsssl-with-exception-handler with-exception-handler)
    ;;
    is-meta-form? is-metainfo-request?
    ;; Some indirection here to ease development of the metainterface moved towards config.
    metaview metactrl
    (dsssl-metainfo metainfo)
    (dsssl-metainfo/adopt metainfo/adopt)

    (dsssl-timestamp timestamp)         ; obsolete, deprecated
    (dsssl:time time)
    (dsssl-secret-encode secret-encode)
    secret-verify
    md5-digest sha256-digest
    (dsssl-sxml sxml)
    sxpath
    txpath
    (bail-form-field form-field)
    (dsssl-apply-xslt-stylesheet xdslt) ; HIGHLY experimental maybe be better do NOT export that one
    xml-parse uri-parse
    uri uri-scheme uri-authority uri-path uri-query uri-fragment
    (dsssl-xml-format xml-format)
    (dsssl-normalise normalise)
    qrencode
    read-only-plain-transformation
    transaction-private-plain-transformation!
    (reflexive-read-only-xslt-transformation read-only-xslt-transformation)

    ((lambda (m r) (bail/read m r)) bail/read)	     ; Experimental
    ((lambda (m r) (bail/confirm m r)) bail/confirm) ; Experimental
    ((lambda (m r) (bail/propose m r)) bail/propose) ; Experimental
    ((lambda (m r) (bail/submit m r)) bail/submit) ; Experimental
    ((lambda (m r p) (bail/accept m r p)) bail/accept)   ; Experimental

    ((is-propfind-request?) is-propfind-request?)
    (dav-propfind-element dav-propfind-reply)
    (propfind-request-element dav-propfind-request)
    is-options-request? dav-options
    collection-read custom-collection-read
    is-copy-request?  make-copy-element
    is-move-request?  make-move-element
    is-proppatch-request? make-proppatch-element
    is-put-request?   make-put-element
    is-mkcol-request? make-mkcol-element
    ;;is-delete-request? ;this is an empty link element
    is-collection-request?
    collection-propose custom-collection-propose
    collection-accept
    collection-write custom-collection-write
    collection-write-local custom-collection-write-local
    collection-update-tree collection-update-propose
    ;; this can be removed some day
    ((lambda (p r) (collection-read p r)) dav-action-read)
    ((lambda (p r) (collection-write p r)) dav-action-write)

    reflexive-xslt-transformation-propose
    xslt-transformation-accept
    (transaction-xslt-transformation!
     reflexive-transaction-xslt-transformation!)

    transaction-xslt-transformation!

    ;(mysql-escape-string sql-quote)
    sql-quote sql-write
    (dsssl-sql-ref sql-ref) (dsssl-sql-ref/default sql-ref/default)
    (dsssl-sql-query sql-query)
    (dsssl-sql-fold sql-fold)
    ;; These sql result accessors are deprecated.
    sql-field sql-index

    ;; pem encoding
    pem-encode pem-decode
    ;; certificate display
    (dsssl-x509-text x509-text)

    ennunu denunu nunubacklinks nunulinks nunu-fetch
    nunu-lock nunu-unlock nunu-change-draft
    nunu-action-get
    nunu-action-write))

(define (askemos-register-client-binding! name)
  (guard (exception
	  (else (logerr "trying dsssl-export ~a ~a\n" name exception)
		(raise exception)))
	 (if (pair? name)
	     (dsssl-export! (eval (car name))
			    (let ((x (cadr name)))
			      (if (string? x) (string->symbol x) x)))
	     (dsssl-export! (eval name) name))))

(define (askemos-register-client-bindings!)
  ;; WORK around a bug in rscheme
  (cond-expand
   (rscheme
    (dsssl-export!
     (lambda (x . b)
       (guard (ex (else #f)) (apply string->number x b)))
     'string->number))
   (else))
  (for-each askemos-register-client-binding! wttree-forms)
  (for-each askemos-register-client-binding! askemos-client-bindings))

(define loopback-address "127.0.0.1")

(define $dns-user #f)
(define $dns-password #f)

(define $kernel-debug #t)

(define (load-or-die file)
  (guard
   (exception (else (log-condition (format "Fatal load error in ~a\n" file) exception)
                    (exit 1)))
   (call-with-input-file file
     (lambda (port)
       (let loop ((expr (sweet-read port))
                  (last #f))
         (if (eof-object? expr)
             last
             (loop (sweet-read port) (eval expr))))))))

;; Some control over the operation.
;; TODO move this into another module; better listen on unix domain sockets.

(define (signal-via-entry! kind name nl to #!key (content-type "text/xml") (accept application/rdf+xml))
  (let ((user (entry-name->oid name))
	(destination '()))
    (assert user "user does not exist") ;; Be sure it exists.
    (let ((obj (find-local-frame-by-id user 'signal-via-entry!))
	  (to (cond
	       ((oid? to) to)
	       ((string? to)
		(match (parsed-locator to)
		       ((to . more)
			(begin
			  (set! destination more)
			  (or (oid->string to) (error "signal-via-entry! failed to convert destination"))))))
	       (else user))))
      (signal-subject!
       to
       kind
       (make-message
	(property 'dc-creator user)
	(property 'caller user)
	(property 'capabilities (fget obj 'capabilities))
	(property 'dc-date (current-date (timezone-offset)))
	(property 'destination destination)
	(property 'accept accept)
	(property
	 'location
	 (cond
	  ((oid? to) (list (oid->string to)))
	  ((eq? to #t) (list (oid->string user)))
	  (else '())))
	(property 'location-format 'http-location-format) ;; dunno any better
	(property 'content-type content-type)
	(if (string? nl)
	    (property 'mind-body nl)
	    (property 'body/parsed-xml nl)))))))

(define (send-via-entry! name #!optional nl #!key (to #t) (content-type "text/xml") (accept application/rdf+xml))
  (signal-via-entry! 'write name nl to content-type: content-type accept: accept))

(define (query-via-entry! name #!optional nl #!key (to #t) (content-type "text/xml") (accept application/rdf+xml))
  (signal-via-entry! 'read name nl to content-type: content-type accept: accept))

;; (: install-new-private-name (string :xml-element: -> *))
(define (install-new-private-name! name nl)
  (assert (public-oid) "not yet initialized")
  (assert (string? name))
  (let ((nl (document-element nl)))
    (assert (xml-element? nl))
    (make-local-entry-point! *local-quorum* name nl)))

(define (my-mind-exit)
  (logerr "\n~a\n"
          (with-output-to-string (lambda () (thread-list))))
  (close-spool-db!)
  (mind-exit))

(define (operation-control-command-loop in out peer return)
  (define (ev expr)
    (enter-front-court (lambda (expr) (eval expr)) expr))
  (display "\n" out)
  (let loop ()
    (with-exception-handler
     (lambda (c) (logerr "operation-control: ~a\n" c) (return c))
     (lambda ()
       (display "# " out)
       (flush-output-port out)
       (let* ((line0 (read-line in))
	      (line (if (eof-object? line0)
			(return (my-mind-exit))
			(string-trim-both line0))))
	 (cond
	  ((equal? line "logout") (return #f))
	  ((equal? line "exit")
	   (my-mind-exit)
	   (display "Waiting for system exit.\n" out)
	   (loop))
	  ((equal? line "force-exit") (return (my-mind-exit)))
	  ((equal? line "restart")
	   (mind-execute-synchron mind-restart)
	   (display "Waiting for system restart.\n" out)
	   (loop))
	  (((dsssl-pcre "connect (.*)") line) =>
	   (lambda (m) (askemos-connect (cadr m))))
	  (((dsssl-pcre "zip (.*)") line) =>
	   (lambda (m)
	     (zip-system-core
	      (or (and-let* ((fsm (assq fsm-storage-adaptor *the-registered-stores*)))
			    (cdr fsm))
		  (error "could not find default storage adaptor"))
	      (cadr m))))
	  ((equal? line "cert")
	   (let ((key (filedata (tc-private-cert-req-key-file))))
	     (if key
		 (begin
		   (display
		    "Enter cert data terminated by single dot at line or EOF.\n"
		    out)
		   (let* ((pem (lmtp-read-data in))
			  (text (x509-text pem)))
		     (tc-store-host-cert! pem)
		     (tc-store-host-key! key)
		     (remove-file (tc-private-cert-req-key-file))
		     (display text out)))
		 (display "There is no key.\n" out))))
	  (((dsssl-pcre "for (A[0-9a-f]+) tanfile (.*)") line) =>
	   (lambda (m) (store-tans! (cadr m) #f (filedata (caddr m)))))
	  ((equal? line "eval")
	   (if $kernel-debug
	       (return (eval-request-server
			in out peer
			`(eval . ,ev) `(return . ,return)))
	       (display "Debugging disabled.\n" out)))
	  ((and-let* (((and $dns-user $dns-password))
		      (ipup ((dsssl-pcre "ip-up: (.*)") line)))
		     (cadr ipup))
	   =>
	   (lambda (ip)
	     (https-response
	      (make-message
	       (property 'host "garkin.softeyes.net:443")
	       (property 'type 'post)
	       (property 'content-type text/xml)
	       (property 'body/parsed-xml
			 (make-xml-element
			  'form namespace-forms
			  form-element-attributes
			  (node-list
			   (make-xml-element
			    'action namespace-forms (empty-node-list)
			    (literal "register-node"))
			   (make-xml-element
			    'id namespace-mind (empty-node-list)
			    (literal (public-oid)))
			   (make-xml-element
			    'url namespace-forms (empty-node-list)
			    (literal (local-id)))
			   (make-xml-element
			    'ipaddr namespace-forms (empty-node-list)
			    (literal ip))))))
	      $dns-user $dns-password)))))))
    (loop)))

(define (operation-control)
  (askemos:run-tcp-server
   loopback-address ($control-port)
   (lambda (in out peer)
     (bind-exit
      (lambda (return)
        (display (with-output-to-string thread-list) out)
        ;; TODO disable local echo in telnet.
        (display "Password: " out)
        (flush-output-port out)
        (if (check-control-password (let ((line (read-line in)))
					   (or (eof-object? line)
					       (string-trim-both line))))
	    (operation-control-command-loop in out peer return)
            (return #f)))))
   (make-semaphore 'operation-control 1) #f "operation-control-connection"))
