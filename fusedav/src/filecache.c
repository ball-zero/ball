/* $Id: filecache.c 43 2008-06-02 16:40:07Z jfw $ */

/***
  This file is part of fusedav.

  fusedav is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  fusedav is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with fusedav; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define _XOPEN_SOURCE 500
#define _XOPEN_SOURCE_EXTENDED

#include <errno.h>
#include <string.h>
#include <limits.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <pthread.h>
#include <inttypes.h>
#include <linux/limits.h>

#include <ne_props.h>
#include <ne_uri.h>
#include <ne_session.h>
#include <ne_utils.h>
#include <ne_socket.h>
#include <ne_auth.h>
#include <ne_dates.h>
#include <ne_basic.h>

#include "filecache.h"
#include "statcache.h"
#include "fusedav.h"
#include "session.h"

#define ne_content_range64 ne_content_range
#define ne_get_range64 ne_get_range
#define ne_set_request_body_fd64 ne_set_request_body_fd

struct file_info {
    char *filename;
    int ref;

    pthread_mutex_t ro_mux;	/* protects the above, short operations */

    int fd;
    off_t server_length, length, present;
    
    int readable;
    int writable;

    int modified;

    int dead;

    pthread_mutex_t rw_mux;

    int state;
    pthread_t fixup;


    /* This field is locked by files_mutex, not by file_info->rw_mux */
    struct file_info *next;
};

static struct file_info *files = NULL;
static pthread_mutex_t files_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t exit_in_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct file_info *file_cache_done = NULL;
static pthread_mutex_t exit_ex_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t file_cache_maintainance_thread;

static int file_cache_sync_locked(struct file_info *fi);

static void file_cache_unlink(struct file_info *fi) {
    struct file_info *s, *prev;
    assert(fi);

    pthread_mutex_lock(&files_mutex);

    for (s = files, prev = NULL; s; s = s->next) {
        if (s == fi) {
            if (prev)
                prev->next = s->next;
            else
                files = s->next;

            break;
        }
        
        prev = s;
    }
    
    pthread_mutex_unlock(&files_mutex);
}

static void file_cache_free_locked(struct file_info *fi) {
    file_cache_unlink(fi);

    free(fi->filename);

    if (fi->fd >= 0)
        close(fi->fd);

    pthread_mutex_unlock(&fi->ro_mux);
    pthread_mutex_destroy(&fi->ro_mux);
    pthread_mutex_unlock(&fi->rw_mux);
    pthread_mutex_destroy(&fi->rw_mux);
    free(fi);
}

static void file_cache_free_unlocked(struct file_info *fi) {
    assert(fi && fi->dead>0 && fi->ref == 0);

    if(debug)
      fprintf(stderr, "file_cache_free %s\n", fi->filename);

    pthread_mutex_lock(&fi->ro_mux);
    pthread_mutex_lock(&fi->rw_mux);
    file_cache_free_locked(fi);
}

static void file_cache_done_with(struct file_info *fi) {
  pthread_mutex_lock(&exit_in_mutex);
  file_cache_done = fi;
  pthread_mutex_unlock(&exit_ex_mutex);
}

static void *file_cache_maintainance(__unused void *p) {
  pthread_mutex_lock(&exit_ex_mutex);
  while ( pthread_mutex_lock(&exit_ex_mutex) ) {
    struct file_info *fi = file_cache_done;
    pthread_mutex_unlock(&exit_in_mutex);
    if(fi->state) {
      void *result;
      if ( pthread_join(fi->fixup, &result) != 0 ) {
	fprintf(stderr, "pthread_join returned %s\n",
		strerror(errno));
      }
      fi->state=0;
    }
    file_cache_free_unlocked(fi);
  }
  return NULL;
}

void* file_cache_get(const char *path) {
  struct file_info *f, *r = NULL;

    pthread_mutex_lock(&files_mutex);
    
    for (f = files; f; f = f->next) {

        pthread_mutex_lock(&f->ro_mux);

        if (f->filename && !strcmp(path, f->filename)) {

	  f->ref++;
	  r = f;
        }

        pthread_mutex_unlock(&f->ro_mux);

        if (r)
            break;
    }
    
    pthread_mutex_unlock(&files_mutex);
    if(debug)
      fprintf(stderr, "file_cache %s %p\n", path, (void*) f);
    return f;
}

void* file_cache_get_error(const char *path, int *error) {
  struct file_info *fi = NULL;
  int retries=20;
  *error=0;
  while(1) {
    if ((fi = file_cache_get(path))) {
      /* busy */
      if( fi->dead>0 ) {
	--retries;
	sleep(1);
      } else {    /* usable */
	return fi;
      }
      file_cache_unref(fi);
      if(!retries) {
	*error = EBUSY;
	return NULL;
      }
    } else return NULL;
  }
}

static void *file_cache_fixup(void *p);

void file_cache_unref(void *f) {
    struct file_info *fi = f;
    int r=0;
    assert(fi);

    pthread_mutex_lock(&fi->ro_mux);
    pthread_mutex_lock(&fi->rw_mux);

    assert(fi->ref >= 1);
    if(fi->ref) fi->ref--;

    if (!fi->ref && fi->dead>0 && !fi->state) {
      if(!fi->modified) {
        file_cache_free_locked(fi);
	return;
      }
      if((r=file_cache_sync_locked(fi)) == 0) {
        file_cache_free_locked(fi);
	return;
      }
    }

    if(debug)
      fprintf(stderr, "file_cache_unref %s left %d cleanup %d\n", fi->filename, fi->ref, r);

    if((r == EBUSY) && !fi->state) {
      fi->ref++;
      if ((r = pthread_create(&(fi->fixup), NULL, file_cache_fixup, fi)) < 0) {
	fi->ref--;
	pthread_mutex_unlock(&fi->rw_mux);
      } else {
	fi->state = 1;
      }
    } else {
      pthread_mutex_unlock(&fi->rw_mux);
    }

    pthread_mutex_unlock(&fi->ro_mux);
}

int file_cache_close(void *f) {
  struct file_info *fi = f;
  int r = 0;
  assert(fi);

  pthread_mutex_lock(&fi->rw_mux);

  fi->dead++;

  if(fi->dead>0)
    r=file_cache_sync_locked(fi);

  if((r == EBUSY) && !fi->state) {
    if ((r = pthread_create(&(fi->fixup), NULL, file_cache_fixup, file_cache_get(fi->filename))) < 0) {
      pthread_mutex_unlock(&fi->rw_mux);
    } else {
      fi->state = 1;
    }
  } else {
    pthread_mutex_unlock(&fi->rw_mux);
  }
  return r;
}

int file_cache_rename(const char* from, const char *to) {
  struct file_info *fi;
  char *_to;
  if((fi=file_cache_get(from))) {
    _to = strdup(to);
    pthread_mutex_lock(&fi->ro_mux);
    free(fi->filename);
    fi->filename=_to;
    pthread_mutex_unlock(&fi->ro_mux);
  }
  return 0;
}

void* file_cache_create(const char *path, int flags) {
  struct file_info *fi = NULL;
  char tempfile[PATH_MAX];

  fi = malloc(sizeof(struct file_info));
  memset(fi, 0, sizeof(struct file_info));
  fi->fd = -1;

  fi->filename = strdup(path);

  snprintf(tempfile, sizeof(tempfile), "%s/fusedav-cache-XXXXXX", "/tmp");
  if ((fi->fd = mkstemp(tempfile)) < 0) {
    free(fi->filename);
    free(fi);
    return NULL;
  }
  unlink(tempfile);

  fi->server_length = fi->length = 0; 
  fi->modified = 1;
    
  if (flags & O_RDONLY || flags & O_RDWR) fi->readable = 1;
  if (flags & O_WRONLY || flags & O_RDWR) fi->writable = 1;

  pthread_mutex_init(&fi->rw_mux, NULL);
  pthread_mutex_init(&fi->ro_mux, NULL);

  fi->ref = 1;

  pthread_mutex_lock(&files_mutex);
  fi->next = files;
  files = fi;
  pthread_mutex_unlock(&files_mutex);

  return fi;
}

void* file_cache_open(const char *path, int flags) {
    struct file_info *fi = NULL;
    char tempfile[PATH_MAX];
    const char *length = NULL;
    ne_request *req = NULL;
    ne_session *session;
    int err;

    if (!(session = session_get(1))) {
        errno = EIO;
        goto fail;
    }

    if ((fi = file_cache_get_error(path, &err))) {
      /* usable */
      if (flags & O_RDONLY || flags & O_RDWR) fi->readable = 1;
      if (flags & O_WRONLY || flags & O_RDWR) fi->writable = 1;
      fi->dead--;
      return fi;
    } else if(err) {
      errno = err;
      return NULL;
    }

    fi = malloc(sizeof(struct file_info));
    memset(fi, 0, sizeof(struct file_info));
    fi->fd = -1;

    fi->filename = strdup(path);

    snprintf(tempfile, sizeof(tempfile), "%s/fusedav-cache-XXXXXX", "/tmp");
    if ((fi->fd = mkstemp(tempfile)) < 0)
        goto fail;
    unlink(tempfile);

    req = ne_request_create(session, "HEAD", path);
    assert(req);

    if (ne_request_dispatch(req) != NE_OK) {
        fprintf(stderr, "HEAD failed: %s\n", ne_get_error(session));
        errno = ENOENT;
        goto fail;
    }

    if (!(length = ne_get_response_header(req, "Content-Length")))
        /* dirty hack, since Apache doesn't send the file size if the file is empty */
        fi->server_length = fi->length = 0; 
    else
        fi->server_length = fi->length = atoi(length);

    ne_request_destroy(req);
    
    if (flags & O_RDONLY || flags & O_RDWR) fi->readable = 1;
    if (flags & O_WRONLY || flags & O_RDWR) fi->writable = 1;

    pthread_mutex_init(&fi->rw_mux, NULL);
    pthread_mutex_init(&fi->ro_mux, NULL);

    fi->ref = 1;

    pthread_mutex_lock(&files_mutex);
    fi->next = files;
    files = fi;
    pthread_mutex_unlock(&files_mutex);

    return fi;

fail:

    if (req)
        ne_request_destroy(req);

    if (fi) {
        if (fi->fd >= 0)
            close(fi->fd);
        free(fi->filename);
        free(fi);
    }
        
    return NULL;
}

static int load_up_to_unlocked(struct file_info *fi, off_t l) {
#ifndef ne_get_range64
#define NE_GET_RANGE ne_get_range
    ne_content_range range;
#else
#define NE_GET_RANGE ne_get_range64
    ne_content_range64 range;
#endif
    ne_session *session;

    assert(fi);

    if (!(session = session_get(1))) {
        errno = EIO;
        return -1;
    }

    if (l > fi->server_length)
        l = fi->server_length;
    
    if (l <= fi->present)
        return 0;

    if (lseek(fi->fd, fi->present, SEEK_SET) != fi->present)
        return -1;
    
    range.start = fi->present;
    range.end = l-1;
    range.total = 0;
    
    if (NE_GET_RANGE(session, fi->filename, &range, fi->fd) != NE_OK) {
        fprintf(stderr, "GET failed: %s\n", ne_get_error(session));
        errno = ENOENT;
        return -1;
    }

    fi->present = l;
    return 0;
#undef NE_GET_RANGE
}

#define FILECACHE_BLOCK_SIZE 16384
#define min(a,b) ((a) < (b) ? (a) : (b))

static int file_cache_compare_blockwise(struct file_info *fi) {
  char tempfile[PATH_MAX];
  char *bl[FILECACHE_BLOCK_SIZE],*br[FILECACHE_BLOCK_SIZE];
  off_t off;
  ssize_t r = -1, turn;
  int fdr = 0;
  ne_content_range64 range;
  ne_session *session;

  if (!(session = session_get(1))) {
    errno = EIO;
    return -1;
  }

  snprintf(tempfile, sizeof(tempfile), "%s/fusedav-cache-XXXXXX", "/tmp");
  if ((fdr = mkstemp(tempfile)) < 0)
    return 1;
  unlink(tempfile);

  for( off=0 ; off<fi->length ; off+=FILECACHE_BLOCK_SIZE ) {
    turn = min(FILECACHE_BLOCK_SIZE,fi->length-off);

    if((r = pread(fi->fd, bl, turn, off)) < 0) break;

    if((r=lseek(fdr, 0, SEEK_SET)) != 0) break;

    range.start = off;
    range.end = off+turn-1;
    range.total = 0;

    /* FIXME: we need to use a more low level call to get a hand on
       the http status.  408 for instance should get retries.  The
       better alterenative seems to be to fix neon. */
    if (ne_get_range64(session, fi->filename, &range, fdr) != NE_OK) {
        fprintf(stderr, "GET failed: %s\n", ne_get_error(session));
        errno = ENOENT;
	r=1;
        break;
    }

    if((r = pread(fdr, br, turn, 0)) < 0) break;

    if((r = memcmp(bl, br, turn)) != 0) break;

  }

  if(fdr) close(fdr);

  return r;
}

int file_cache_read(void *f, char *buf, size_t size, off_t offset) {
    struct file_info *fi = f;
    ssize_t r = -1;
    
    assert(fi && buf && size);

    pthread_mutex_lock(&fi->rw_mux);

    if (load_up_to_unlocked(fi, offset+size) < 0)
        goto finish;

    if ((r = pread(fi->fd, buf, size, offset)) < 0)
        goto finish;

finish:
    
    pthread_mutex_unlock(&fi->rw_mux);

    return r;
}

int file_cache_write(void *f, const char *buf, size_t size, off_t offset) {
    struct file_info *fi = f;
    ssize_t r = -1;

    assert (fi);

    pthread_mutex_lock(&fi->rw_mux);

    if (!fi->writable) {
        errno = EBADF;
        goto finish;
    }

    if (load_up_to_unlocked(fi, offset) < 0)
        goto finish;
        
    if ((r = pwrite(fi->fd, buf, size, offset)) < 0)
        goto finish;

    if (offset+size > fi->present)
        fi->present = offset+size;

    if (offset+size > fi->length)
        fi->length = offset+size;

    fi->modified = 1;

finish:
    pthread_mutex_unlock(&fi->rw_mux);
    
    return r;
}

int file_cache_truncate(void *f, off_t s) {
    struct file_info *fi = f;
    int r;

    assert(fi);

    pthread_mutex_lock(&fi->rw_mux);

    if(fi->length != s) fi->modified = 1;
    fi->length = s;
    r = ftruncate(fi->fd, fi->length);

    pthread_mutex_unlock(&fi->rw_mux);

    return r;
}


/* lifted from neon to add support for status codes; */

/* PUT's from fd to URI */
#define NE_HAVE_DAV 1

static int file_cache_put(ne_session *sess, const char *urip, int fd, off_t size)
{
    ne_request *req;
    int ret;

    req = ne_request_create(sess, "PUT", urip);

#ifdef NE_HAVE_DAV
    ne_lock_using_resource(req, urip, 0);
    ne_lock_using_parent(req, urip);
#endif

    ne_set_request_body_fd64(req, fd, (off_t) 0, (off_t) size);

    ret = ne_request_dispatch(req);
    
    if (ne_get_status(req)->klass != 2)
      switch(ne_get_status(req)->code) {
      case 503: case 408: case 412:
	ret = EBUSY;
	break;
      default:
	ret = NE_ERROR;
      }

    ne_request_destroy(req);

    return ret;
}

int file_cache_sync_locked(struct file_info *fi) {
    int r = -1;
    ne_session *session;

    assert(fi);
    if (!fi->writable) {
        errno = EBADF;
        goto finish;
    }

    if (!fi->modified) {
        r = 0;
        goto finish;
    }

    if (load_up_to_unlocked(fi, (off_t) -1) < 0)
        goto finish;

    if (lseek(fi->fd, 0, SEEK_SET) == (off_t)-1)
        goto finish;

    if (!(session = session_get(1))) {
        errno = EIO;
        goto finish;
    }
    if ((r=file_cache_put(session, fi->filename, fi->fd, fi->length)) != NE_OK) {
      fprintf(stderr, "PUT failed: %s\n", ne_get_error(session));
      if( r != EBUSY ) {
	r = ENOENT;
      }
      goto finish;
    }

    fi->modified = 0;
    stat_cache_invalidate(fi->filename);
    dir_cache_invalidate_parent(fi->filename);

    r = 0;

finish:
    
    return r;
}

static void *file_cache_fixup(void *p) {
  struct file_info *fi = p;
  struct stat st;
  int i=10, r;

  assert(fi);

  do {

    if(!fi->modified) break;

    sleep(5);

    if ((r=get_stat_fetch(fi->filename, &st)) == NE_OK) {
      /* if file size && data && check content */
      if( (st.st_size == fi->length && !file_cache_compare_blockwise(fi))
	  || !(r=file_cache_sync_locked(fi))) {
	fi->modified = 0;
	stat_cache_set(fi->filename, &st);
	break;
      }
      stat_cache_invalidate(fi->filename);
    }

  } while(i-- && (r==EBUSY));

  if(fi->modified)
    fprintf(stderr, "giving up on %s\n", fi->filename);

  fi->state = fi->modified = 0;
  pthread_mutex_unlock(&fi->rw_mux); /*  balanced by caller */

  file_cache_unref(fi);
  return NULL;
}

int file_cache_sync(void *f) {
    struct file_info *fi = f;
    int r = -1;
    assert(fi);

    pthread_mutex_lock(&fi->rw_mux);
    r = file_cache_sync_locked(fi);
    if((r == EBUSY) && !fi->state) {
      if ((r = pthread_create(&(fi->fixup), NULL, file_cache_fixup, file_cache_get(fi->filename))) < 0) {
	pthread_mutex_unlock(&fi->rw_mux);
      } else {
	fi->state = 1;
	r = EBUSY;
      }
    } else {
      pthread_mutex_unlock(&fi->rw_mux);
    }

    return r;
}

int file_cache_close_all(void) {
    int r = 0;

    pthread_mutex_lock(&files_mutex);

    while (files) {
        struct file_info *fi = files;
        
        pthread_mutex_lock(&fi->ro_mux);
        fi->ref++;
        pthread_mutex_unlock(&fi->ro_mux);

        pthread_mutex_unlock(&files_mutex);
        file_cache_close(fi);
        file_cache_unref(fi);
        pthread_mutex_lock(&files_mutex);
    }

    pthread_mutex_unlock(&files_mutex);

    pthread_kill(file_cache_maintainance_thread, SIGUSR1);
    pthread_join(file_cache_maintainance_thread, NULL);

    return r;
}

off_t file_cache_get_size(void *f) {
    struct file_info *fi = f;

    assert(fi);

    return fi->length;
}

int file_cache_dead(void *f) {
    struct file_info *fi = f;

    return fi == NULL || fi->dead>0;
}

int file_cache_dirty(void *f) {
    struct file_info *fi = f;

    return fi != NULL && fi->modified;
}

void file_cache_start(void) {
  int r=0;
  if ((r = pthread_create(&file_cache_maintainance_thread, NULL, file_cache_maintainance, NULL)) < 0) {
    fprintf(stderr, "could not start file_cache_maintainance_thread, exiting");
    exit(1);
  }
}
