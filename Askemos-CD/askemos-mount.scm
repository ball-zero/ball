#! /usr/bin/csi

(require-extension srfi-1 srfi-18 posix pstk)

(declare
 (uses posix srfi-18 files pstk)
 (foreign-declare #<<EOF
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
EOF
))

(define *log* #t)

(define webbrowser-executable "firefox")

;; BEGIN defaults and parameters

(define-syntax defrecord
  (lambda (x r c)
    (define rec (cadr x))
    (define ts (symbol->string rec))
    (define fields (cddr x))
    `(define-record-type ,rec
       (,(string->symbol (string-append "make-" ts)) . ,fields)
       ,(string->symbol (string-append ts "?"))
       . ,(map
	   (lambda (f)
	     (list f
		   (string->symbol (string-append (symbol->string rec) "-" (symbol->string f)))))
	   fields))))


#|
(define-macro (defrecord rec . fields)
  (let ((ts (symbol->string rec)))
    `(define-record-type ,rec
       (,(string->symbol (string-append "make-" ts)) . ,fields)
       ,(string->symbol (string-append ts "?"))
       . ,(map
	   (lambda (f)
	     (list f
		   (string->symbol (string-append (symbol->string rec) "-" (symbol->string f)))))
	   fields))))
|#

(defrecord am
  pkgname
  runuser desktopuser
  encdir
  username password url mountdir
  daemon libdir
  repository datadir)

;;; TODO: read defaults from ~/.askemos

(define (ormap f first . rest)
  (cond
   ((null? first) (or))
   ((null? rest) (let loop ((first first))
                   (and (pair? first)
                        (or (f (car first))
                            (loop (cdr first))))))
   (else (let loop ((lists (cons first rest)))
           (and (pair? (car lists))
                (or (apply f (map car lists))
                    (loop (map cdr lists))))))))

(define params
  (let* ((pkgname "askemos")
	 (repository (cadr (system-information))) ; hostname
	 (runuser (if (eqv? (current-effective-user-id) 0) "askemos"
		      (car (user-information (current-user-id))))))
    (make-am
     pkgname runuser
     (if (eqv? (current-effective-user-id) 0)
	 (or (get-environment-variable "SUDO_USER") "ubuntu")
	 runuser)
     (ormap (lambda (d)
	      (or (let ((d2 (string-append "/" d)))
		    (and (directory? d2) d2))
		  (and (directory? d) d)))
	    `("/media/Private/crypt" "/media/Private" "/cdrom"
	      (get-environment-variable "HOME")))
     ;;  Fusedav defaults
     "" "" "https://localhost:2143/"
     (string-append (get-environment-variable "HOME") "/Desktop/Askemos")
     (make-absolute-pathname '("usr" "sbin") pkgname)
     (make-absolute-pathname '("usr" "lib") pkgname)
     ;; repository
     (cadr (system-information))		; hostname
     (make-absolute-pathname
      (or (and-let* ((ui (user-information runuser #t)))
		    (list (vector-ref ui 5) repository))
	  (list "var" "state" runuser repository))
      #f))))

;;

;; END defaults and parameters

(define current-language
  (let ((translations '(en de)))
    (make-parameter
     (let ((x (get-environment-variable "LANG")))
       (or
	(and-let* (((string? x))
		   ((>= (string-length x) 2))
		   (tag (string->symbol (substring x 0 2)))
		   ((memq tag translations)))
		  tag)
	'en))
     (lambda (s) (if (memq s translations) s
		     (error "language ~a undefined" s))))))

(define current-country (make-parameter 'de))
;; The association list in which bundles will be stored
(define *localization-bundles* '())

;; The load-bundle! and store-bundle! both return #f in this
;; reference implementation.  A compliant implementation need
;; not rewrite these procedures.
(define load-bundle!
  (lambda (bundle-specifier)
    #f))

(define store-bundle!
  (lambda (bundle-specifier)
    #f))

;; Declare a bundle of templates with a given bundle specifier
(define declare-bundle!
  (letrec ((remove-old-bundle
            (lambda (specifier bundle)
              (cond ((null? bundle) '())
                    ((equal? (caar bundle) specifier)
                     (cdr bundle))
                    (else (cons (car bundle)
                                (remove-old-bundle specifier
                                                   (cdr bundle))))))))
    (lambda (bundle-specifier bundle-assoc-list)
      (set! *localization-bundles*
            (cons (cons bundle-specifier bundle-assoc-list)
                  (remove-old-bundle bundle-specifier
                                     *localization-bundles*))))))

;;Retrieve a localized template given its package name and a template name
(define localized-template
  (letrec ((rdc
            (lambda (ls)
              (if (null? (cdr ls))
                  '()
                  (cons (car ls) (rdc (cdr ls))))))
           (find-bundle
            (lambda (specifier template-name)
              (cond ((assoc specifier *localization-bundles*) =>
                     (lambda (bundle) bundle))
                    ((null? specifier) #f)
                    (else (find-bundle (rdc specifier)
                                       template-name))))))
    (lambda (package-name template-name)
      (let loop ((specifier (cons package-name
                                  (list (current-language)
                                        (current-country)))))
        (and (not (null? specifier))
             (let ((bundle (find-bundle specifier template-name)))
               (and bundle
                    (cond ((assq template-name bundle) => cdr)
                          ((null? (cdr specifier)) #f)
                          (else (loop (rdc specifier)))))))))))

(define (_ key . args)
  (apply format (localized-template 'askemos-mount key) args))

 ;; BEGIN util-chicken.scm --

(define (ormap f first . rest)
  (cond
   ((null? first) (or))
   ((null? rest) (let loop ((first first))
                   (and (pair? first)
                        (or (f (car first))
                            (loop (cdr first))))))
   (else (let loop ((lists (cons first rest)))
           (and (pair? (car lists))
                (or (apply f (map car lists))
                    (loop (map cdr lists))))))))

;; REPLACE THE EXCEPTION HANDLER

;; unsure, shouldn't we wrap the handler with dynamic-wind ?

(define chicken-exception-handler (current-exception-handler))

(define srfi34:*current-exception-handlers*
  (make-parameter (list chicken-exception-handler)))

(define (srfi34:with-exception-handler handler thunk)
  (srfi34:with-exception-handlers
   (cons handler (srfi34:*current-exception-handlers*))
   thunk))

(define (srfi34:with-exception-handlers new-handlers thunk)
  (let ((previous-handlers (srfi34:*current-exception-handlers*)))
    (dynamic-wind
      (lambda ()
        (srfi34:*current-exception-handlers* new-handlers))
      thunk
      (lambda ()
        (srfi34:*current-exception-handlers* previous-handlers)))))

(define (srfi34:raise obj)
  (let ((handlers (srfi34:*current-exception-handlers*)))
    (srfi34:with-exception-handlers (cdr handlers)
      (lambda ()
        ((car handlers) obj)
        ;; (error "handler returned" (car handlers) obj)
        (chicken-exception-handler
         (make-property-condition
          'exn 'message "exception handler returned"))))))

(set! with-exception-handler srfi34:with-exception-handler)

(set! current-exception-handler
      (lambda () (car (srfi34:*current-exception-handlers*))))

; (define (##sys#escape x)
;   (let ((handler-chain ##sys#current-exception-handler))
;     (set! ##sys#current-exception-handler
;           (cdr ##sys#current-exception-handler))
;     ((car handler-chain) x)
;     (set! ##sys#current-exception-handler handler-chain)))

;(define srfi-34:raise ##sys#escape)

(set! ##sys#abort
      (lambda (x)
        ((current-exception-handler) x)
        (##sys#abort (make-property-condition
                      'exn 'message "exception handler returned")) ))

(set! ##sys#signal
      (lambda (x)
        ((current-exception-handler) x)) )

(define raise srfi34:raise)
(set! abort ##sys#abort)
(set! signal ##sys#signal)
(set! error
      (lambda (msg . args)
        (raise (if (pair? args) (format #f "~a ~s" msg args) msg))))

(set! ##sys#error error)

; (set! ##sys#current-exception-handler
;       (list ##sys#current-exception-handler))

; (set! with-exception-handler
;       (lambda (handler thunk)
;         (let ((handler-chain ##sys#current-exception-handler))
;           (##sys#dynamic-wind
;            (lambda () (set! ##sys#current-exception-handler
;                             (cons handler handler-chain)))
;            thunk
;            (lambda () (set! ##sys#current-exception-handler handler-chain)) ))) )

;; SRFI 34 support

(define (with-exception-guard handler thunk)
  ((call-with-current-continuation
    (lambda (return)
      (srfi34:with-exception-handler
       (lambda (condition)
         ((call-with-current-continuation
           (lambda (reraise)
             (return (lambda ()
                       (srfi34:with-exception-handler
                        reraise (lambda () (handler condition)))))))))
       (lambda ()
         (##sys#call-with-values
          thunk
          (lambda args
            (return (lambda () (##sys#apply ##sys#values args)))) ) ) ) ) )) )

(define (yield)
  (##sys#call-with-current-continuation
   (lambda (return)
     (let ((ct ##sys#current-thread))
       (##sys#setslot ct 1 (lambda () (return (##core#undefined))))
       (##sys#schedule) ) ) ) )

(set-signal-handler! signal/pipe #f)

(define (buffer-size) 4096)

(define-syntax end-of-file
  (syntax-rules () ((_) #!eof)))

(define ##process#io-ports
   (let ([make-input-port make-input-port]
 	[make-output-port make-output-port] 
 	[make-string make-string] 
 	[substring substring] )
     (lambda (pid fdr fdw)
       (let* ([buf (make-string (buffer-size))]
 	     ;; [data (vector #f #f #f)]
 	     [buflen 0]
 	     [bufindex 0]
 	     [iclosed #f] 
 	     [oclosed #f]
 	     [in
 	      (and
	       fdr
	       (make-input-port
		(lambda ()
		  (when (fx>= bufindex buflen)
			(##sys#thread-block-for-i/o! ##sys#current-thread fdr #t)
			(yield)
			(let ([n ((foreign-lambda*
				   int
				   ((int fd) (scheme-pointer buf) (int s))
				   "return(read(fd, buf, s));")
				  fdr buf (buffer-size))])
			  (when (eq? -1 n)
				(##sys#update-errno)
				(##sys#signal-hook #:process-error "can not read from fd" fdr) )
			  ;; (print "[rd: " n "]")
			  (set! buflen n)
			  (set! bufindex 0) ) )
		  (if (fx>= bufindex buflen)
		      (end-of-file)
		      (let ([c (##core#inline "C_subchar" buf bufindex)])
			(set! bufindex (fx+ bufindex 1))
			c) ) )
		(lambda ()
                  (when iclosed
			(##sys#signal-hook #:process-error "input port is closed" fdr))
		  #t )
		(lambda ()
		  (unless iclosed
			  (set! iclosed #t)
			  (when (eq? -1 (file-close fdr))
				(##sys#update-errno)
				(##sys#signal-hook #:process-error
						   "can not close fd input port" fdr) )
			  (when oclosed
				(receive (p f s) (process-wait pid #f) s))
			  ) ) )) ]
 	     [out
 	      (make-output-port
 	       (lambda (s)
 		 (let ([len (##sys#size s)])
 		   (let loop ()
 		     (let ([n ((foreign-lambda*
                                 int
                                 ((int fd) (scheme-pointer buf) (int count))
                                 "return(write(fd, buf, count));") fdw s len)])
 		       (cond [(eq? -1 n)
 			      (##sys#update-errno)
 			      (##sys#signal-hook
                                #:process-error "can not write to fd" fdw len) ]
 			     [(fx< n len)
 			      (set! s (substring s 0 n))
 			      (set! len (fx- len n))
 			      (loop) ] ) ) ) ) )
 	       (lambda ()
 		 (unless oclosed
 		   (set! oclosed #t)
 		   (when (eq? -1 (file-close fdw))
 		     (##sys#update-errno)
 		     (##sys#signal-hook #:process-error
                                         "can not close fd output port" fdw) )
                    (when iclosed
                          (receive (p f s) (process-wait pid #f) s))) ) ) ] )
; 	(##sys#setslot (##sys#port-data in) 0 data)
; 	(##sys#setslot (##sys#port-data out) 0 data)
         (set-finalizer! in (lambda (port) (close-input-port port)))
         (set-finalizer! out (lambda (port) (close-output-port port)))
 	(values in out) ) ) ) )

 (define (close-all-ports-except . ports)
   (do ((i 0 (add1 i)))
       ((eqv? i 1024) #t)
     (if (not (memv i ports))
         ((foreign-lambda* int ((int fd)) "return(close(fd));") i))))

(define (logmsg msg . args)
  (if *log*
      (begin
	(display msg (current-error-port))
	(for-each (lambda (a) (display #\space (current-error-port)) (display a (current-error-port))) args)
	(newline (current-error-port)))))

(define (logdebug l v)
  (logmsg l v)
  v)

(define (debug l v)
  (display l (current-error-port))
  (display ": " (current-error-port))
  (display v (current-error-port))
  (newline (current-error-port))
  v)

(define (sudo! runuser)
  (and-let* ((runuser)
	     (ui (user-information runuser))
	     (user-id (list-ref ui 2))
	     (cu (current-user-id))
	     ((not (eqv? cu user-id)))
	     (g (list-ref ui 3)))
	    (if (eqv? cu 0)
		(initialize-groups runuser g))
	    (set! (current-group-id) g)
	    (set! (current-user-id) user-id)
	    #t))

 (define (make-user-process-stub runuser thunk dir . env)
   ;; FIXME 'env' and 'dir' not yet used
   (receive (tr tw) (create-pipe)
            (receive (fr fw) (create-pipe)
                     (let ((pid (process-fork)))
                       (if (eqv? pid 0)
                           (begin
			     (or (sudo! runuser) (logmsg "su failed\n"))
                             ;; (file-close cr) (file-close cw)
			     (duplicate-fileno tr 0)
                             (duplicate-fileno fw 1)
                             (close-all-ports-except 0 1 2)
                             (handle-exceptions ex (_exit 1) (thunk))
			     (_exit 0))
                           (begin
                             (file-close tr) (file-close fw)
                             (receive (in out) (##process#io-ports pid fr tw)
                                      (values pid out in))))))))

(define (make-process-stub args dir . env)
  (apply make-user-process-stub #f
	 (lambda ()
	   (or (process-execute (car args) (cdr args))
	       (logmsg "failed to execute" args)))
	 dir env))

;;** future

(define-record-type <future>
  (internal-make-future mutex condition has-result result)
  future?
  (mutex future-mutex)
  (condition future-condition)
  (has-result future-has-result future-has-result-set!)
  (result future-result future-result-set!))

(define (future-force f)
  (if (future-has-result f)
      (future-result f)
      (begin
        (mutex-lock! (future-mutex f))
        (if (future-has-result f)
            (begin
              (mutex-unlock! (future-mutex f))
              (future-result f))
            (begin
              (mutex-unlock! (future-mutex f) (future-condition f))
              (future-result f))))))

(define (thunk->future thunk name)
  (let ((f (internal-make-future (make-mutex)
                                 (make-condition-variable)
                                 #f 'invalid)))
    (thread-start!
     (make-thread
      (lambda ()
	(future-result-set! f (with-exception-guard identity thunk))
	(future-has-result-set! f #t)
	(condition-variable-broadcast! (future-condition f)))
      name))
    (delay (future-force f))))

 ;; now actually util.scm ;-)

(define *set-child-uid* (make-parameter #f))

(define (run-child-process producer consumer cmd dir . env)
  (if (and dir (*set-child-uid*)) (change-file-mode dir #o777))
  (receive (pid to-cmd from-cmd)
	   (apply make-process-stub
		  (if (*set-child-uid*) (cons (*set-child-uid*) cmd) cmd)
		  dir env)
	   (if pid
	       (begin
		 (thunk->future
		  (lambda ()
		    (dynamic-wind
			(lambda () #f)
			(lambda () (producer to-cmd))
			(lambda () (close-output-port to-cmd))))
		  cmd)
		 (dynamic-wind
		     (lambda () #f)
		     (lambda () (consumer from-cmd))
		     (lambda () (close-input-port from-cmd))))
	       (error (_ 'process-start-failed) cmd))))

(define (within-directory dir thunk)
  (let ((here #f))
    (dynamic-wind
	(lambda ()
	  (set! here (current-directory))
	  (change-directory dir))
	thunk
	(lambda () (change-directory here)))))

;; END util-chicken.scm --

(define (run-as-user runuser thunk)
  (receive (pid to-cmd from-cmd)
	   (make-user-process-stub
	    runuser
	    (lambda ()
	      ;; (current-output-port (open-output-file* 1))
	      (handle-exceptions ex #f (write (thunk)))
	      (flush-output (current-output-port))
	      ;; (close-output-port (current-output-port))
	      (_exit 1))
	    #f)
	   (if pid
	       (dynamic-wind
		   (lambda () (close-output-port to-cmd))
		   (lambda () (read from-cmd))
		   (lambda () (close-input-port from-cmd)))
	       (error (_ 'process-start-failed) thunk))))

(define (fd-reopen target fn)
  ((foreign-lambda*
    int
    ((int desired_fd) (c-string file)) #<<END
 int flags = O_CREAT | O_WRONLY | O_APPEND;
 int mode = S_IRUSR | S_IWUSR;

 int fd;

 fd = open (file, flags, mode);
 if (fd == desired_fd || fd < 0)
   return fd;
 else {
   int saved_errno;
   dup2(fd, desired_fd);
   saved_errno = errno;
   close (fd);
   errno = saved_errno;
   return desired_fd;
 }
END
)
   target fn))

;; No idea what I dig wrong about the "file-open/duplicate-fileno"
;; (native chicken) way.
(define (run-with-logfiles user cmd outlog errlog)
  (receive
   (sr cw) (create-pipe)
   (let ((pid (process-fork)))
     (if (eqv? pid 0)
	 (let (;;(outlog (file-open outlog (+ open/sync open/creat open/append)))
	      ;;  (errlog (file-open errlog (+ open/sync open/creat open/append)))
	       )
	   ;; (file-close cr)
	   (file-close cw)
	   (duplicate-fileno sr 0)
	   ;;	   (file-close 1)
	   ;; (duplicate-fileno (debug cmd outlog) 1)
	   ;; (duplicate-fileno (debug 'errlog errlog) 2)
	   (fd-reopen 1 outlog)
	   (fd-reopen 2 errlog)
	   (close-all-ports-except 0 1 2)
	   (sudo! user)
	   (handle-exceptions
	    ex (_exit 1)
	    (if (pair? cmd)
		(process-execute (car cmd) (cdr cmd))
		(cmd)))
	   (_exit 0))
	 (begin
	   (file-close sr)
	   (receive (in out) (##process#io-ports pid #f cw)
		    (values pid out in)))))))

;(is-running? (make-absolute-pathname rundir pkgname "pid"))
(define (is-running? pid-file)
  (and-let* ((pid (handle-exceptions
		   _ #f (with-input-from-file pid-file read-line)))
	     ((directory? (make-absolute-pathname "proc" pid))))
	    pid))

(define (kill-from-pid-file pid-file)
  (and-let* ((pids (handle-exceptions
		   _ #f (with-input-from-file pid-file read-line)))
	     (pp (make-absolute-pathname "proc" pids))
	     ((directory? pp))
	     (pid (string->number pids)))
	    (process-signal pid)
	    (do ((i 0.1 (+ i 0.01)))
		((or (directory? pp)
		     (> i 0.35)))
	      (thread-sleep! i))
	    (if (directory? pp)
		(begin
		  ; (debug "can't kill process" pid)
		  (process-signal pid 9)
		  (do ((i 0.1 (+ i 0.05)))
		      ((or (not (directory? pp))
			   (> i 1)))
		    (thread-sleep! i))))))

(define (with-pid-file pid-file thunk wait)
  (call-with-values thunk
    (lambda (pid . rest)
      (call-with-output-file (pid-file (make-dir 'run))
	(lambda (port) (display pid port) (newline port)))
      (if wait (thread-sleep! (logdebug 'wait wait)))
      (apply values pid rest))))

(define (with-threads thunk)
  (let ((pid (process-fork)))
     (if (eqv? pid 0)
	 (begin (thunk) (exit 0))
	 (process-wait pid))))

(let ((translations
       '(((en) . ((process-start-failed . "starting ~a failed")
		  (data-dir-not-empty . "data directory not empty.\n")
		  (need-askemos-user . "need askemos user")
		  (select-mount-point . "Select Mount Point")
		  (select-askemos-repository . "Select askemos repository (clear text)")
		  (select-encrypted-repository . "Select encrypted repository")
		  (askemos-user . "askemos user")
		  (repository . "repository (clear text)")
		  (mount-user . "username")
		  (mount-password . "password")
		  (url . "URL")
		  (mount-point . "mount point")
		  (encryption . "encryption")
		  (private-space . "private space")
		  (encryption-password . "password")
		  (old-process-running . "There are already Askemos related processes running:")
		  (kill-them . "Kill Them")
		  (start . "START")
		  (cancel . "CANCEL")
		  (Y . "Y")
		  (N . "N")))
         ((de) . ((process-start-failed . "Programm ~a startet nicht.")
		  (data-dir-not-empty . "Dataverzeichnis nicht leer.\n")
		  (need-askemos-user . "Askemosbenutzer fehlt")
		  (select-mount-point . "Zielverzeichnis aussuchen")
		  (select-askemos-repository . "Askemos Arbeitsverzeichnis (Klartext) einstellen")
		  (select-encrypted-repository . "Verschlossener Askemos Datenbereich")
		  (askemos-user . "Systembenutzer 'Askemos'")
		  (repository . "Arbeitsverzeichnis (Klartext)")
		  (mount-user . "Benutzername")
		  (mount-password . "Mantra")
		  (url . "URL")
		  (mount-point . "Verbundverzeichnis")
		  (encryption . "Kryptoverfahren")
		  (private-space . "Privater Bereich")
		  (encryption-password . "Mantra")
		  (old-process-running . "Es laufen bereits Askemos-relevante Prozesse:")
		  (kill-them . "Anhalten")
		  (start . "STARTEN")
		  (cancel . "ABBRUCH")
		  (Y . "Ja")
		  (N . "Nein"))))))
  (for-each (lambda (translation)
              (let ((bundle-name (cons 'askemos-mount (car translation))))
                (if (not (load-bundle! bundle-name))
                    (begin
		      (declare-bundle! bundle-name (cdr translation))
		      (store-bundle! bundle-name)))))
	    translations))

(define (enc-startup)
  (let ((enc (string->symbol (get-property 'cryptkind)))
	(encloc (get-property 'encloc))
	(datadir (get-property 'datadir))
	(runuser (get-property 'runuser))
	(uids (and-let* ((ui (user-information (am-runuser params))))
			(sprintf "uid=~A,gid=~A"
				 (caddr ui) (cadddr ui)))))
    (cond ((eq? enc 'none) -1)
	  ((pair? (directory datadir))
	   (display (_ 'data-dir-not-empty) (current-error-port))
	   #f)
	  ((eq? enc 'encfs)
	   ;;encfs
	   (receive
	    (pid to from)
	    (make-user-process-stub
	     runuser
	     (let ((args `("-f" "-S" ,encloc ,datadir
			   . ,(if uids (list "--" "-o" uids) '()))))
	       (lambda () (process-execute "encfs" args)))
	     #f)
	    (force
	     (thunk->future
	      (lambda ()
		(dynamic-wind
		    (lambda () #f)
		    (lambda ()
		      (display (get-property 'encpasswd) to)
		      (newline to))
		    (lambda () (close-output-port to))))
	      "encfs-pw"))
	    ;; (close-input-port from) <= FIXME: hangs, but why?
	    pid))
	   ((eqv? enc 'truecrypt)
	    ;;truecrypt
	    (receive
	     (pid to from)
 	     (make-process-stub
 	      `("truecrypt" "-t" ,encloc ,datadir)
	      ;; "--protect-hidden=yes"
 	      ; `("tee" "/tmp/noda")
 	      #f)
	     (force
	      (thunk->future
	       (lambda ()
		 (dynamic-wind
		     (lambda () #f)
		     (lambda ()
		       (display (get-property 'encpasswd) to)
		       (newline to))
		     (lambda () (close-output-port to))))
	       "truecrypt-pw"))
	     (close-input-port from)
	     pid))
	   (else #f))))

(define (make-dir type)
  (if (eqv? (current-effective-user-id) 0)
      (case type
	((run)"/var/run")
	((log)"/var/log"))
      (string-append "/tmp/" (am-runuser params))))

(define rundir (make-dir 'run))

(or (file-exists? rundir) (directory? rundir) (create-directory rundir))

(define (fusedav-startup)
  (define mountdir (get-property 'mountdir))
  (define logdir (make-dir 'log))
  (if (or (directory? mountdir)
	  (run-as-user (am-desktopuser params) (lambda () (create-directory mountdir))))
      (receive
       (pid to in)
       (run-with-logfiles
	(am-desktopuser params)
	`("fusedav" "-l" ,(get-property 'url) ,mountdir
	  . ,(or '() (and-let* ((ui (user-information (am-desktopuser params))))
			       (list
				(sprintf "-ouid=~A,gid=~A~A"
					 (caddr ui) (cadddr ui)
					 (if (eqv? (current-effective-user-id) 0)
					     ",allow_other" "")))) '()))
	(make-absolute-pathname logdir "fusedav" "log")
	(make-absolute-pathname logdir "fusedav-error" "log"))
       (let ((pw (get-property 'password)))
	 (if (not (equal? pw ""))
	     (force
	      (thunk->future
	       (lambda ()
		 (dynamic-wind
		     (lambda () #f)
		     (lambda ()
		       (display (get-property 'username) to)
		       (newline to)
		       (display pw to)
		       (newline to))
		     (lambda () (close-output-port to))))
	       "fusedav-pw"))
	     (close-output-port to)))
       (if (input-port? in) (close-input-port in))
       pid)
      (begin
	;; (raise (format "no access to mount directory ~a" mountdir))
	-1)))

(define (run-askemos)
  (define pkgname (am-pkgname params))
  (define runuser (get-property 'runuser))
  (define dd (get-property 'datadir))
  (define logdir (make-dir 'log))
  ;; (define config.scm (make-absolute-pathname dd "config" "scm"))
  (define ld (am-libdir params))
  (setenv "PATH" (string-append ld ":" (get-environment-variable "PATH")))
  (receive
   (pid out in)
   (run-with-logfiles
    #f
    (logdebug
     'run-askemos
     `(,(am-daemon params) ,runuser ,pkgname ,ld ,dd
       . ,(let ((config.scm (make-absolute-pathname dd "config.scm")))
	    (if (run-as-user
		 runuser
		 (lambda ()
		   (let loop ((i 0))
		     (file-exists? config.scm))))
		(list config.scm) '()))))
    (make-absolute-pathname logdir pkgname "log")
    (make-absolute-pathname logdir (string-append pkgname "-error") "log"))
   (if (input-port? in) (close-input-port in))
   ;; might hang; why?
   ;;(close-output-port out)
   pid))

(define pid-files
  `((,(lambda (rundir) (make-absolute-pathname rundir "askemos-crypt" "pid")) ,enc-startup 2)
    (,(lambda (rundir) (make-absolute-pathname rundir (am-pkgname params)  "pid")) ,run-askemos 5)
    (,(lambda (rundir) (make-absolute-pathname rundir "askemos-fusedav" "pid")) ,fusedav-startup #f)))

(define (startup)
  (define url (get-property 'url))
  (do ((lst pid-files (cdr lst)))
      ((null? lst))
    (apply with-pid-file (car lst)))
  (run-as-user
   (am-desktopuser params)
   (lambda ()
     (logmsg "starting" webbrowser-executable)
     (process-execute webbrowser-executable (list url))))
  (close-output-port (current-output-port))
  (tk-end)
  (exit 0))

(define (x-startup)
  (with-threads startup)
  (exit 0))

;;; -------  tk UI

(define find-tk-var
  (let ((variables '()))
    (lambda (name dflt)
      (let ((entry (assq name variables)))
	(if entry
	    (begin
	      (tk-set-var! name dflt)
	      (cdr entry))
	    (let ((v (tk-var name)))
	      (tk-set-var! name dflt)
	      (set! variables `((,name . ,v) . ,variables)) v))))))

(define (sx-required-attribute l n)
  (define atts (and (pair? (cdr l)) (pair? (cadr l)) (eq? (caadr l) '@) (cdadr l)))
  (define entry (assq n atts))
  (if entry
      (cadr entry)
      (error (format "sq-required-attribute ~a ~a" n l))))

(define (sxch l)
  (if (and (pair? (cdr l)) (pair? (cadr l)) (eq? (caadr l) '@) (cdadr l))
      (cddr l) (cdr l)))

(define (tk/render tki l)
  (handle-exceptions
   ex (error (debug 'render-exception ex))
   (if (pair? l)
       (let ((x (car l)))
	 (cond
	  ((pair? x) (append (tk/render tki x) (tk/render tki (cdr l))))
	  ((procedure? x) (cons x (tk/render tki (cdr l))))
	  ((symbol? x)
	   (case (car l)
	     ((window)
	      (if (and (pair? (cdr l)) (pair? (cadr l)) (eq? (caadr l) '@))
		  (begin
		    (for-each
		     (lambda (att)
		       (case (car att)
			 ((nix-width) (apply tk/place 'configure "."  #:width (cdr att)))))
		     (cdadr l))
		    (tk/render tki (cddr l)))
		  (tk/render tki (cdr l))))
	     ((button)
	      (cons
	       (tki 'create-widget 'button
		    #:text (let* ((a (assq 'text (cdadr l)))) (if a (cadr a) ""))
		    #:width (let* ((a (assq 'width (cdadr l)))) (if a (cadr a) 10))
		    #:command (let* ((a (assq 'callback (cdadr l)))) (if a (cadr a) (lambda () #f))))
	       (tk/render tki (cddr l))))
	     ((choice-button)
	      (list
	       (let* ((id (sx-required-attribute l 'id))
		      (v (find-tk-var id #f))
		      (b (apply
			  tki 'create-widget "tk_optionMenu" v
			  (map (lambda (x)
				 (if (and (pair? (cdr x)) (pair? (cadr x)) (eq? (caadr x) '@))
				     (caddr x) (cadr x)))
			       (cddr l)))))
		 (b 'configure #:width (let* ((a (assq 'width (cdadr l)))) (if a (cadr a) 10)))
		 (let ((a (find (lambda (o)
				  (and (pair? o)
				       (eq? (car o) 'option)
				       (pair? (cdr o))
				       (pair? (cadr o))
				       (eq? (caadr l) '@)
				       (let ((a (assq 'selected (cdadr o))))
					 (and a (eq? (cadr a) 'selected)))))
				(cddr l))))
		   (if a (tk-set-var! id (caddr a))))
		 b)))
	     ((entry secret-entry)
	      (list
	       (apply
		tki 'create-widget 'entry
		#:width (let* ((a (assq 'width (cdadr l)))) (if a (cadr a) 10))
		#:textvariable
		(let ((a (sx-required-attribute l 'id)))
		  (find-tk-var a (let ((a (cddr l))) (if (pair? a) (car a) ""))))
		(if (eq? (car l) 'secret-entry)
		    '(#:show #\*)
		    '()))))
	     ((label)
	      (list
	       (tki 'create-widget 'label
		    #:text (let ((c (sxch l))) (if (pair? c) (car c) ""))
		    #:width (let* ((a (assq 'width (cdadr l)))) (if a (cadr a) 10))
		    #:anchor (let* ((a (assq 'align (cdadr l))))
			       (if (not a) 'w
				   (case (cadr a)
				     ((left) 'w)
				     ((right) 'e)
				     (else (cadr a)))))
		    )))
	     ((pack)
	      (let* ((panel (tki 'create-widget 'frame
				 #:width (let* ((a (assq 'width (cdadr l)))) (if a (cadr a) 10))
				 ;; #:heigth (let* ((a (assq 'height (cdadr l)))) (if a (cadr a) 10))
				 ;; #:borderwidth 1 #:relief 'solid
				 ))
		     (content (tk/render panel (cddr l))))
		(if (pair? content)
		    (apply
		     tk/pack
		     (append content
			     (list #:side
				   (let ((d (assq 'type (cdadr l)))
					 (a (assq 'align (cdadr l))))
				     (cond
				      ((or (not d) (eq? (cadr d) 'horizontal))
				       (if (or (not a) (eq? (cadr a) 'left)) 'left 'right))
				      (else
				       (if (or (not a) (eq? (cadr a) 'top)) 'top 'bottom))))))))
		(list panel)))
	     (else (error (format "tk/render: ~a" (car l))))))))
       '())))

(define tk/switch
  (let ((content '()))
    (lambda s
      (if (pair? content) (apply tk/pack 'forget content))
      (set! content (tk/render tk s))
      (if (pair? content) (apply tk/pack content))
      content)))

(define (kill-from-all-pid-files)
  (for-each
   (lambda (i) (kill-from-pid-file ((car i) rundir)))
   (reverse pid-files))
  (run-as-user
   (am-desktopuser params)
   (lambda ()
     ;; Quick HACK trying to get the normal case working.
     (process-execute "fusermount" (list "-q" "-u" "-z" (am-datadir params)))))
  (ask-for-cont))

(define (ask-for-kill)
  (tk/switch
   `(window
     (@ (callback ,exit)
	(width 460) (height 360))
     (pack
      (@ (height 80) (type vertical) (spacing 5))
      (label (@ (width 50) (align center)) ,(_ 'old-process-running))
      ,@(map
	 (lambda (pid-f-entry)
	   `(pack
	     (@ (height 20) (type horizontal) (spacing 5))
	     (label (@ (width 30) (align right)) ,((car pid-f-entry) rundir))
	     (label (@ (width 5) (align left))
		    ,(if (is-running? ((car pid-f-entry) rundir))
			 (_ 'Y) (_ 'N)))))
	 pid-files)
      (button (@ (width 20) (text  ,(_ 'kill-them))
		 (callback ,kill-from-all-pid-files)))))
   ))

(define get-property tk-get-var)

(define (existing-pathname-part path)
  (let loop ((path path))
    (cond ((not path) "/")
	  ((directory? path)
	   (make-absolute-pathname path #f))
	  (else (loop (pathname-directory path))))))

(define (select-directory variable label)
  (tk-set-var! variable
	       (tk/choose-directory
		#:initialdir (get-property variable)
		#:title label)))

(define (select-file widget-id label)
  (tk-set-var! variable
	       (tk/get-save-file
		#:initialfilename (get-property variable)
		#:title label)))

;;   (let ((w (bb:find-widget widget-id)))
;;     (set! (bb:property w 'text)
;; 	  (let ((old (bb:property w 'text)))
;; 	    (within-directory old (lambda () (bb:select-file label old))))))

(define select-mountdir (cut select-directory 'mountdir (_ 'select-mount-point)))
(define select-datadir  (cut select-directory 'datadir (_ 'select-askemos-repository)))

(define (select-encloc)
  (case (string->symbol (get-property 'cryptkind))
    ((none) (select-directory 'datadir (_ 'select-askemos-repository)))
    ((encfs) (select-directory 'encloc (_ 'select-encrypted-repository)))
    ((truecrypt) (select-file 'encloc (_ 'select-encrypted-repository)))
    (else #f)))

(define (ask-for-options)
  (tk/switch
   (let* ((lw 25)
	  (ew 30)
	  (tw (+ lw ew 5))
	  (daemon-user-selection
	   `((pack
	      (@ (height 1) (width ,tw) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'askemos-user))
	      (entry (@ (width ,ew) (id runuser)) ,(am-runuser params))
	      (pack (@ (type vertical) (width 35))))
	     (pack
	      (@ (height 1) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'repository))
	      (entry (@ (width ,ew) (id datadir)) ,(existing-pathname-part (am-datadir params)))
	      (button (@ (width 1) (callback ,select-datadir) (text  "..."))))
	     (pack (@ (height 20) (type horizontal) (spacing 5)))))
	  (mount-point-selection
	   `((pack
	      (@ (height 1) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'url))
	      (entry (@ (width ,ew) (id url)) ,(am-url params))
	      (pack (@ (type vertical) (width 35))))
	     (pack
	      (@ (height 1) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'mount-point))
	      (entry (@ (width ,ew) (id mountdir)) ,(am-mountdir params))
	      (button (@ (width 1) (callback ,select-mountdir) (text  "..."))))
	     (pack 
	      (@ (height 1) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'mount-user))
	      (entry (@ (width ,ew) (id username)  (text ,(am-username params))))
	      (pack (@ (type vertical) (width 35))))
	     (pack
	      (@ (height 1) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'mount-password))
	      (secret-entry (@ (width ,ew) (id password)  (text ,(am-password params))))
	      (pack (@ (type vertical) (width 35))))
	     )))
     `(window
       (@ (callback ,exit)
	  (width 100) (height 370)
	  )
					;   (tabs 
					;    (@  (x 10) (y 10) (width 400) (height 180))
					;    (group (@ (x 10) (y 30) (width 400) (height 150) (text "truecrypt"))
       (pack (@ (type vertical))
	     (pack 
	      (@ (height 1) (width ,tw) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'encryption))
	      (choice-button (@ (width ,ew) (id cryptkind))
			     (option "none")
			     (option (@ (selected selected)) "encfs")
			     (option "truecrypt")))
	     (pack
	      (@ (height 20) (width ,tw) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'private-space))
	      (entry (@ (width ,ew) (id encloc)) ,(existing-pathname-part (am-encdir params)))
	      (button (@ (width 1) (callback ,select-encloc) (text  "..."))))
	     (pack
	      (@ (height 20) (width ,tw) (type horizontal) (spacing 5))
	      (label (@ (width ,lw) (align right)) ,(_ 'encryption-password))
	      (secret-entry (@ (width ,ew) (id encpasswd)))
	      (pack (@ (type vertical) (width 35))))
	     (pack (@ (height 20) (width ,tw) (type horizontal) (spacing 5)))
					; (pack
					; 		  (@ (height 20) (type horizontal) (spacing 5))
					; 		  (label (@ (width 230)))
					; 		  (button (@ (width 60) (callback ,exit) (text "CANCEL")))
					; 		  (button (@ (width 60) (id start-encfs) (callback ,enc-startup) (text "START"))))
					;)
					;)

					;    (group (@ (x 10) (y 30) (width 400) (height 150) (text "askemos"))
					;	   (pack (@ (x 10) (y ,lw) (spacing 5))

	     ,@daemon-user-selection

					; (pack
					; 		  (@ (height 20) (type horizontal) (spacing 5))
					; 		  (label (@ (width 230)))
					; 		  (button (@ (width 60) (id cancel-askemos) (callback ,exit) (text "CANCEL")))
					; 		  (button (@ (width 60) (id start-askemos) (callback ,run-askemos) (text "START"))))
					;)
					;)
					;    (group (@ (x 10) (y 30) (width 400) (height 150) (text "fusedav"))
					;	   (pack (@ (x 10) (y 230) (spacing 5))
	     ,@mount-point-selection
	     (pack (@ (height 10) (type horizontal) (spacing 5)))
	     (pack
	      (@ (height 1)  (width ,tw) (type horizontal) (spacing 5))
	      (label (@ (width ,(+ lw 3))))
	      (button (@ (width 10) (id start-fusedav) (callback ,startup) (text ,(_ 'start))))
	      (button (@ (width 10) (callback ,(lambda () (tk-end) (exit 0))) (text ,(_ 'cancel))))
	      (pack (@ (type vertical) (width 35))))
					;)
					;)
	     
	     )
       ))
   ))

(define (ask-for-cont)
  (tk/switch
   (if (ormap (lambda (p) (is-running? ((car p) rundir))) pid-files)
       (ask-for-kill)
       (ask-for-options))))

#|

; (set-property! 'start-askemos 'text 
; 	       (if (is-running? (make-absolute-pathname rundir pkgname "pid"))
; 		   "RESTART" "START"))
; (set-property! 'start-fusedav 'text	       
; 	       (if (is-running? (make-absolute-pathname rundir "fusedav" "pid"))
; 	

|#

(define (with-tk proc)
  (handle-exceptions
   ex (begin (tk-end) (raise ex))
   (tk-start)
   (tk/appname "Askemos Mount")
   ;; (tk/console)
   (proc)
   (tk-event-loop)
   (tk-end)))

(with-tk ask-for-cont)

(exit)
